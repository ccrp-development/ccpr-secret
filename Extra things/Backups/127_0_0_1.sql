-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2019 at 03:59 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ccrpadmin`
--
CREATE DATABASE IF NOT EXISTS `ccrpadmin` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ccrpadmin`;
--
-- Database: `devserver`
--
CREATE DATABASE IF NOT EXISTS `devserver` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `devserver`;

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1),
(33, 'society_admin', 'admin', 1),
(34, 'society_biker', 'Biker', 1),
(35, 'society_cardealer', 'Car Dealer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_ambulance', 0, NULL),
(2, 'society_police', 0, NULL),
(3, 'society_mecano', 0, NULL),
(4, 'society_taxi', 0, NULL),
(5, 'society_fire', 0, NULL),
(6, 'society_airlines', 0, NULL),
(7, 'society_mafia', 0, NULL),
(8, 'society_rebel', 0, NULL),
(9, 'society_unicorn', 0, NULL),
(10, 'society_dock', 0, NULL),
(11, 'society_avocat', 0, NULL),
(12, 'society_irish', 0, NULL),
(13, 'society_rodriguez', 0, NULL),
(14, 'society_bishops', 0, NULL),
(15, 'society_bountyhunter', 0, NULL),
(16, 'society_dismay', 0, NULL),
(17, 'society_grove', 0, NULL),
(18, 'society_foodtruck', 0, NULL),
(19, 'society_vagos', 0, NULL),
(20, 'society_ballas', 0, NULL),
(21, 'society_carthief', 0, NULL),
(22, 'society_realestateagent', 0, NULL),
(23, 'society_admin', 0, NULL),
(24, 'society_biker', 0, NULL),
(25, 'society_cardealer', 0, NULL),
(44, 'caution', 0, 'steam:11000010a01bdb9'),
(45, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(48, 'property_black_money', 0, 'steam:11000013ca7f6bf'),
(49, 'caution', 0, 'steam:11000013ca7f6bf'),
(52, 'caution', 0, 'steam:110000132580eb0'),
(53, 'property_black_money', 0, 'steam:110000132580eb0');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_mecano', 'Mechanic', 1),
(3, 'society_taxi', 'Taxi', 1),
(5, 'property', 'Property', 0),
(6, 'society_fire', 'fire', 1),
(7, 'society_airlines', 'Airlines', 1),
(8, 'society_mafia', 'Mafia', 1),
(9, 'society_citizen', 'Mafia', 1),
(10, 'society_rebel', 'Rebel', 1),
(11, 'society_unicorn', 'Unicorn', 1),
(12, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(13, 'society_unicorn', 'Unicorn', 1),
(14, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(15, 'society_dock', 'Marina', 1),
(16, 'society_avocat', 'Avocat', 1),
(17, 'society_irish', 'Irish', 1),
(18, 'society_rodriguez', 'Rodriguez', 1),
(19, 'society_avocat', 'Avocat', 1),
(20, 'society_bishops', 'Bishops', 1),
(21, 'society_irish', 'Irish', 1),
(22, 'society_bountyhunter', 'Bountyhunter', 1),
(23, 'society_dismay', 'Dismay', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_grove', 'Grove', 1),
(26, 'society_vagos', 'Vagos', 1),
(27, 'society_ballas', 'Ballas', 1),
(28, 'society_carthief', 'Car Thief', 1),
(29, 'society_admin', 'admin', 1),
(30, 'society_biker', 'Biker', 1),
(31, 'society_cardealer', 'Car Dealer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `ban_issued` varchar(50) NOT NULL,
  `banned_until` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `Column 9` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `admin_rank` int(11) DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `admin_rank`) VALUES
('steam:110000112969e8f', 'Steven', 'Super', '05/08/1998', 'M', '80', -1, -1, -1, -1),
('steam:11000010a01bdb9', 'T', 'P', '12/28/1988', 'M', '62', -1, -1, -1, -1),
('steam:1100001068ef13c', 'William', 'Woodard', '7/27/1989', 'M', '72', -1, -1, -1, -1),
('steam:110000132580eb0', 'Jak', 'Fulton', '10/10/1988', 'M', '74', -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commend`
--

CREATE TABLE `commend` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `community_name` varchar(50) NOT NULL,
  `discord_webhook` varchar(50) NOT NULL,
  `joinmessage` enum('T','F') NOT NULL,
  `chatcommands` enum('T','F') NOT NULL,
  `checktimeout` int(11) NOT NULL,
  `trustscore` int(11) NOT NULL,
  `tswarn` int(11) NOT NULL,
  `tskick` int(11) NOT NULL,
  `tsban` int(11) NOT NULL,
  `tscommend` int(11) NOT NULL,
  `tstime` int(11) NOT NULL,
  `recent_time` int(11) NOT NULL,
  `permissions` varchar(50) NOT NULL,
  `serveractions` varchar(50) NOT NULL,
  `debug` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'user_mask', 'Masque', 0),
(3, 'property', 'Property', 0),
(4, 'society_fire', 'fire', 1),
(5, 'society_mafia', 'Mafia', 1),
(7, 'society_rebel', 'Rebel', 1),
(8, 'society_unicorn', 'Unicorn', 1),
(9, 'society_unicorn', 'Unicorn', 1),
(10, 'society_avocat', 'Avocat', 1),
(11, 'society_irish', 'Irish', 1),
(12, 'society_rodriguez', 'Rodriguez', 1),
(13, 'society_avocat', 'Avocat', 1),
(14, 'society_bishops', 'Bishops', 1),
(15, 'society_irish', 'Irish', 1),
(16, 'society_bountyhunter', 'Bountyhunter', 1),
(17, 'society_dismay', 'Dismay', 1),
(18, 'society_bountyhunter', 'Bountyhunter', 1),
(19, 'society_grove', 'Grove', 1),
(20, 'society_vagos', 'Vagos', 1),
(21, 'society_ballas', 'Ballas', 1),
(22, 'society_carthief', 'Car Thief', 1),
(23, 'society_admin', 'admin', 1),
(24, 'society_biker', 'Biker', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_police', NULL, '{}'),
(2, 'society_fire', NULL, '{}'),
(3, 'society_mafia', NULL, '{}'),
(4, 'society_rebel', NULL, '{}'),
(5, 'society_unicorn', NULL, '{}'),
(6, 'society_avocat', NULL, '{}'),
(7, 'society_irish', NULL, '{}'),
(8, 'society_rodriguez', NULL, '{}'),
(9, 'society_bishops', NULL, '{}'),
(10, 'society_bountyhunter', NULL, '{}'),
(11, 'society_dismay', NULL, '{}'),
(12, 'society_grove', NULL, '{}'),
(13, 'society_vagos', NULL, '{}'),
(14, 'society_ballas', NULL, '{}'),
(15, 'society_carthief', NULL, '{}'),
(18, 'property', 'steam:110000100023daf', '{}'),
(19, 'user_mask', 'steam:110000100023daf', '{}'),
(26, 'property', 'steam:1100001138168a0', '{}'),
(27, 'user_mask', 'steam:1100001138168a0', '{}'),
(28, 'property', 'steam:1100001320dfb72', '{}'),
(29, 'user_mask', 'steam:1100001320dfb72', '{}'),
(30, 'user_mask', 'steam:1100001159dff06', '{}'),
(31, 'property', 'steam:1100001159dff06', '{}'),
(32, 'property', 'steam:11000010dc84b6d', '{}'),
(33, 'user_mask', 'steam:11000010dc84b6d', '{}'),
(34, 'society_admin', NULL, '{}'),
(35, 'property', 'steam:11000010c87fe96', '{}'),
(36, 'user_mask', 'steam:11000010c87fe96', '{}'),
(37, 'user_mask', 'steam:1100001372437de', '{}'),
(38, 'property', 'steam:1100001372437de', '{}'),
(39, 'property', 'steam:11000010b15a7d4', '{}'),
(40, 'user_mask', 'steam:11000010b15a7d4', '{}'),
(41, 'society_biker', NULL, '{}'),
(64, 'property', 'steam:11000010a01bdb9', '{}'),
(65, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(66, 'property', 'steam:11000013ca7f6bf', '{}'),
(67, 'user_mask', 'steam:11000013ca7f6bf', '{}'),
(68, 'property', 'steam:110000132580eb0', '{}'),
(69, 'user_mask', 'steam:110000132580eb0', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Toro', 'toro', 2000, 'dock'),
(3, 'Dinghy3', 'dinghy3', 2000, 'dock'),
(4, 'Seashark', 'seashark', 1000, 'dock'),
(5, 'Submarine', 'submersible2', 4000, 'dock');

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ballas`
--

CREATE TABLE `fine_types_ballas` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_ballas`
--

INSERT INTO `fine_types_ballas` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bishops`
--

CREATE TABLE `fine_types_bishops` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bishops`
--

INSERT INTO `fine_types_bishops` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_dismay`
--

CREATE TABLE `fine_types_dismay` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_dismay`
--

INSERT INTO `fine_types_dismay` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_grove`
--

CREATE TABLE `fine_types_grove` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_grove`
--

INSERT INTO `fine_types_grove` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_irish`
--

CREATE TABLE `fine_types_irish` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_irish`
--

INSERT INTO `fine_types_irish` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_mafia`
--

CREATE TABLE `fine_types_mafia` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_mafia`
--

INSERT INTO `fine_types_mafia` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rebel`
--

CREATE TABLE `fine_types_rebel` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rebel`
--

INSERT INTO `fine_types_rebel` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rodriguez`
--

CREATE TABLE `fine_types_rodriguez` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rodriguez`
--

INSERT INTO `fine_types_rodriguez` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_vagos`
--

CREATE TABLE `fine_types_vagos` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_vagos`
--

INSERT INTO `fine_types_vagos` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `glovebox_inventory`
--

INSERT INTO `glovebox_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(1, '00JLE838', '{}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'bread', 'Bread', -1, 0, 1),
(2, 'water', 'Water', -1, 0, 1),
(3, 'weed', 'Weed', 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1),
(14, 'fish', 'Fish', -1, 0, 1),
(15, 'stone', 'Stone', -1, 0, 1),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1),
(17, 'copper', 'Copper', -1, 0, 1),
(18, 'iron', 'Iron', -1, 0, 1),
(19, 'gold', 'Gold', -1, 0, 1),
(20, 'diamond', 'Diamond', -1, 0, 1),
(21, 'wood', 'Wood', -1, 0, 1),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1),
(24, 'petrol', 'Gas', -1, 0, 1),
(25, 'petrol_raffin', 'Refined Oil', -1, 0, 1),
(26, 'essence', 'Essence', -1, 0, 1),
(27, 'whool', 'Whool', -1, 0, 1),
(28, 'fabric', 'Fabric', -1, 0, 1),
(29, 'clothe', 'Clothe', -1, 0, 1),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1),
(31, 'fixtool', 'Repair Tools', -1, 0, 1),
(32, 'carotool', 'Tools', -1, 0, 1),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1),
(34, 'fixkit', 'Repair Kit', -1, 0, 1),
(35, 'carokit', 'Body Kit', -1, 0, 1),
(36, 'beer', 'Beer', -1, 0, 1),
(37, 'bandage', 'Bandage', 20, 0, 1),
(38, 'medikit', 'Medikit', 100, 0, 1),
(39, 'pills', 'Pills', 10, 0, 1),
(40, 'lockpick', 'Lockpick', -1, 0, 1),
(41, 'vodka', 'Vodka', -1, 0, 1),
(42, 'coffee', 'Coffee', -1, 0, 1),
(49, 'clip', 'Chargeur', -1, 0, 1),
(50, 'jager', 'Jägermeister', 5, 0, 1),
(51, 'vodka', 'Vodka', 5, 0, 1),
(52, 'rhum', 'Rhum', 5, 0, 1),
(53, 'whisky', 'Whisky', 5, 0, 1),
(54, 'tequila', 'Tequila', 5, 0, 1),
(55, 'martini', 'Martini blanc', 5, 0, 1),
(56, 'soda', 'Soda', 5, 0, 1),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1),
(58, 'icetea', 'Ice Tea', 5, 0, 1),
(59, 'energy', 'Energy Drink', 5, 0, 1),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(61, 'limonade', 'Limonade', 5, 0, 1),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1),
(63, 'bolnoixcajou', 'Bowl of Cashews', 5, 0, 1),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1),
(66, 'saucisson', 'Sausage', 5, 0, 1),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(69, 'golem', 'Golem', 5, 0, 1),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(76, 'mojito', 'Mojito', 5, 0, 1),
(77, 'ice', 'Glaçon', 5, 0, 1),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1),
(81, 'menthe', 'Mint leaf', 10, 0, 1),
(82, 'cola', 'Coke', -1, 0, 1),
(105, 'vegetables', 'Vegetables', 20, 0, 1),
(117, 'turtle', 'Turtle', -1, 0, 1),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1),
(119, 'lsd', 'Lsd', -1, 0, 1),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(121, 'pearl', 'Pearl', -1, 0, 1),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1),
(123, 'litter', 'Litter', -1, 0, 1),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1),
(128, 'meat', 'Meat', 20, 0, 1),
(129, 'tacos', 'Tacos', 20, 0, 1),
(130, 'burger', 'Burger', 20, 0, 1),
(131, 'silencieux', 'Siliencer', -1, 0, 1),
(132, 'flashlight', 'Flashlight', -1, 0, 1),
(133, 'grip', 'Grip', -1, 0, 1),
(134, 'yusuf', 'Skin', -1, 0, 1),
(135, 'binoculars', 'Binoculars', 1, 0, 1),
(136, 'croquettes', 'Croquettes', -1, 0, 1),
(137, 'blackberry', 'blackberry', -1, 0, 1),
(138, 'lighter', 'Bic', -1, 0, 1),
(139, 'cigarett', 'Cigarette', -1, 0, 1),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1),
(2010, 'armor', 'Armor', -1, 0, 1),
(2011, 'contrat', '📃 Facture', 100, 0, 1),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1),
(2013, 'powerade', 'Powerade', -1, 0, 1),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1),
(2016, 'plongee1', 'Short Dive', -1, 0, 1),
(2017, 'plongee2', 'Long Dive', -1, 0, 1),
(2018, 'contrat', 'Salvage', 15, 0, 1),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1),
(2021, 'meat', 'Meat', -1, 0, 1),
(2022, 'leather', 'Leather', -1, 0, 1),
(2023, 'cannabis', 'Cannabis', 50, 0, 1),
(2024, 'marijuana', 'Marijuana', 250, 0, 1),
(2025, 'coca', 'CocaPlant', 150, 0, 1),
(2026, 'cocaine', 'Coke', 50, 0, 1),
(2027, 'ephedra', 'Ephedra', 100, 0, 1),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1),
(2029, 'poppy', 'Poppy', 100, 0, 1),
(2030, 'opium', 'Opium', 50, 0, 1),
(2031, 'meth', 'Meth', 25, 0, 1),
(2032, 'heroine', 'Heroine', 10, 0, 1),
(2033, 'beer', 'Beer', 30, 0, 1),
(2034, 'tequila', 'Tequila', 10, 0, 1),
(2035, 'vodka', 'Vodka', 10, 0, 1),
(2036, 'whiskey', 'Whiskey', 10, 0, 1),
(2037, 'crack', 'Crack', 25, 0, 1),
(2038, 'drugtest', 'DrugTest', 10, 0, 1),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1),
(2041, 'pcp', 'PCP', 25, 0, 1),
(2042, 'dabs', 'Dabs', 50, 0, 1),
(2043, 'painkiller', 'Painkiller', 10, 0, 1),
(2044, 'narcan', 'Narcan', 10, 0, 1),
(2045, 'boitier', 'Darknet', -1, 0, 1),
(2046, 'cocacola', 'Coca Cola', -1, 0, 1),
(2047, 'fanta', 'Fanta Exotic', -1, 0, 1),
(2048, 'sprite', 'Sprite', -1, 0, 1),
(2049, 'loka', 'Loka Crush', -1, 0, 1),
(2050, 'cheesebows', 'Cheese Doodles', -1, 0, 1),
(2051, 'chips', 'Chips', -1, 0, 1),
(2052, 'marabou', 'Milk Chocolate ', -1, 0, 1),
(2053, 'pizza', 'Kebab Pizza', -1, 0, 1),
(2054, 'baconburger', 'Bacon Burger', -1, 0, 1),
(2055, 'pastacarbonara', 'Pasta Carbonara', -1, 0, 1),
(2056, 'macka', 'Ham sammy', -1, 0, 1),
(2059, 'lotteryticket', 'Trisslott', -1, 0, 1),
(2060, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1),
(2061, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1),
(2062, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1),
(2063, 'hunting_license', 'Hunting License', -1, 0, 1),
(2064, 'fishing_license', 'Fishing License', -1, 0, 1),
(2065, 'diving_license', 'Diving License', -1, 0, 1),
(2066, 'marriage_license', 'Marriage License', -1, 0, 1),
(2067, 'pilot_license', 'Pilot License', -1, 0, 1),
(2068, 'taxi_license', 'Taxi License', -1, 0, 1),
(2069, 'commercial_license', 'Commerical Drivers License', -1, 0, 1),
(2070, 'motorcycle_license', 'Motorcycle License', -1, 0, 1),
(2071, 'drivers_license', 'Drivers License', -1, 0, 1),
(2072, 'boating_license', 'Boating License', -1, 0, 1),
(2073, 'firstaidpass', 'First Aid Pass', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'Unemployed', 0),
(3, 'police', 'LEO', 1),
(4, 'slaughterer', 'Slaughterer', 0),
(5, 'fisherman', 'Fisherman', 0),
(6, 'miner', 'Miner', 0),
(7, 'lumberjack', 'Woodcutter', 0),
(8, 'fuel', 'Refiner', 0),
(9, 'reporter', 'Journalist', 0),
(10, 'textil', 'Couturier', 0),
(11, 'mecano', 'Mechanic', 0),
(12, 'taxi', 'Taxi', 0),
(14, 'trucker', 'Trucker', 0),
(16, 'fire', 'LFD', 1),
(17, 'deliverer', 'Deliverer', 0),
(18, 'brinks', 'Transport', 0),
(19, 'gopostal', 'GoPostal', 0),
(20, 'airlines', 'Airlines', 0),
(21, 'ambulance', 'AMR', 1),
(22, 'mafia', 'Mafia', 1),
(24, 'rebel', 'Rebel', 1),
(25, 'security', 'Security', 0),
(28, 'garbage', 'Garbage Driver', 0),
(29, 'pizza', 'Pizzadelivery', 0),
(30, 'ranger', 'parkranger', 0),
(33, 'unicorn', 'Unicorn', 0),
(34, 'bus', 'busdriver', 0),
(36, 'coastguard', 'CoastGuard', 0),
(38, 'irish', 'Irish', 1),
(39, 'rodriguez', 'Rodriguez', 1),
(41, 'bishops', 'Bishops', 1),
(42, 'irish', 'Irish', 1),
(43, 'lawyer', 'lawyer', 1),
(45, 'dismay', 'Dismay', 1),
(46, 'bountyhunter', 'Bountyhunter', 1),
(47, 'grove', 'Grove Street Family', 1),
(49, 'vagos', 'Vagos', 1),
(50, 'ballas', 'Ballas', 1),
(51, 'traffic', 'trafficofficer', 0),
(52, 'poolcleaner', 'PoolCleaner', 0),
(53, 'carthief', 'Car Thief', 0),
(54, 'Salvage', 'Salvage', 0),
(55, 'realestateagent', 'Real Estate Agent', 1),
(56, 'admin', 'Admin', 1),
(57, 'biker', 'HHMC', 1),
(58, 'cardealer', 'Car Dealer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'lumberjack', 0, 'interim', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'interim', 'Employee', 250, '{}', '{}'),
(3, 'fuel', 0, 'interim', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'textil', 0, 'interim', 'Employee', 250, '{}', '{}'),
(6, 'miner', 0, 'interim', 'Employee', 250, '{}', '{}'),
(7, 'slaughterer', 0, 'interim', 'Employee', 250, '{}', '{}'),
(8, 'mecano', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mecano', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mecano', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mecano', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mecano', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'recruit', 'Recruit', 800, '{}', '{}'),
(23, 'airlines', 1, 'firstofficer', 'Firstofficer', 800, '{}', '{}'),
(24, 'airlines', 0, 'pilote', 'Pilote', 800, '{}', '{}'),
(25, 'airlines', 0, 'gerant', 'Gerant', 800, '{}', '{}'),
(26, 'airlines', 0, 'boss', 'Patron', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'interim', 'Interimaire', 400, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'realestateagent', 0, 'agnt', 'Agent', 550, '{}', '{}'),
(78, 'realestateagent', 1, 'seller', 'Senior Agent', 965, '{}', '{}'),
(79, 'realestateagent', 2, 'mgmtn', 'Management', 1750, '{}', '{}'),
(80, 'realestateagent', 3, 'boss', 'Boss', 3000, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(120, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(147, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(148, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(149, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(150, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(156, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(157, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(158, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'soldato', 'Prospect', 1500, '{}', '{}'),
(164, 'biker', 1, 'capo', 'Chapter member', 1800, '{}', '{}'),
(165, 'biker', 2, 'consigliere', 'Nomad', 2100, '{}', '{}'),
(166, 'biker', 3, 'boss', 'President', 10000, '{}', '{}'),
(167, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(168, 'cardealer', 0, 'recruit', 'Recruit', 100, '{}', '{}'),
(169, 'cardealer', 1, 'novice', 'Novice', 250, '{}', '{}'),
(170, 'cardealer', 2, 'experienced', 'Experienced', 350, '{}', '{}'),
(171, 'cardealer', 3, 'boss', 'Boss', 500, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kicks`
--

CREATE TABLE `kicks` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicles`
--

CREATE TABLE `old_vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicles`
--

INSERT INTO `old_vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 15000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
(4, 'Chino', 'chino', 15000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 250000, 'muscle'),
(7, 'Dominator', 'dominator', 60000, 'muscle'),
(8, 'Dukes', 'dukes', 28000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
(11, 'Faction', 'faction', 20000, 'muscle'),
(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
(13, 'Faction XL', 'faction3', 40000, 'muscle'),
(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
(16, 'Picador', 'picador', 18000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
(20, 'Tampa', 'tampa', 16000, 'muscle'),
(21, 'Virgo', 'virgo', 14000, 'muscle'),
(22, 'Vigero', 'vigero', 12500, 'muscle'),
(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
(24, 'Blista', 'blista', 42958, 'compacts'),
(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
(26, 'Issi', 'issi2', 10000, 'compacts'),
(27, 'Panto', 'panto', 10000, 'compacts'),
(28, 'Prairie', 'prairie', 12000, 'compacts'),
(29, 'Bison', 'bison', 45000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
(31, 'Burrito', 'burrito3', 19000, 'work'),
(32, 'Burrito', 'gburrito2', 29000, 'vans'),
(33, 'Camper', 'camper', 42000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
(35, 'Journey', 'journey', 6500, 'vans'),
(36, 'Minivan', 'minivan', 13000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
(39, 'Paradise', 'paradise', 19000, 'vans'),
(40, 'Rumpo', 'rumpo', 30000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
(42, 'Surfer', 'surfer', 12000, 'vans'),
(43, 'Youga', 'youga', 10800, 'vans'),
(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
(45, 'Asea', 'asea', 5500, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
(47, 'Emperor', 'emperor', 8500, 'sedans'),
(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
(49, 'Glendale', 'glendale', 6500, 'sedans'),
(50, 'Intruder', 'intruder', 7500, 'sedans'),
(51, 'Premier', 'premier', 8000, 'sedans'),
(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
(53, 'Regina', 'regina', 5000, 'sedans'),
(54, 'Schafter', 'schafter2', 25000, 'sedans'),
(55, 'Stretch', 'stretch', 90000, 'sedans'),
(56, 'Phantom', 'superd', 250000, 'sedans'),
(57, 'MercedezAMG', 'tailgater', 130000, 'modded'),
(58, 'Warrener', 'warrener', 120000, 'sedans'),
(59, 'Washington', 'washington', 90000, 'sedans'),
(60, 'Baller', 'baller2', 40000, 'suvs'),
(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
(63, 'Contender', 'contender', 70000, 'suvs'),
(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
(66, 'Fhantom', 'fq2', 17000, 'suvs'),
(67, 'Grabger', 'granger', 50000, 'suvs'),
(68, 'Gresley', 'gresley', 47500, 'suvs'),
(69, 'Huntley S', 'huntley', 40000, 'suvs'),
(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
(71, 'Mesa', 'mesa', 16000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
(73, 'Patriot', 'patriot', 55000, 'suvs'),
(74, 'Radius', 'radi', 29000, 'suvs'),
(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
(76, 'Seminole', 'seminole', 25000, 'suvs'),
(77, 'XLS', 'xls', 70000, 'suvs'),
(78, 'Btype', 'btype', 62000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
(81, 'Casco', 'casco', 350000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
(83, 'Manana', 'manana', 12800, 'sportsclassics'),
(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 220000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
(89, 'Porsche Classic', 'ztype', 160000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 12000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
(92, 'Blazer', 'blazer', 6500, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
(94, 'Brawler', 'brawler', 75000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
(97, 'Guardian', 'guardian', 45000, 'offroad'),
(98, 'Rebel', 'rebel2', 35000, 'offroad'),
(99, 'Sandking', 'sandking', 55000, 'offroad'),
(100, 'The Liberator', 'monster', 210000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
(104, 'Exemplar', 'exemplar', 132000, 'modded'),
(105, 'Silvia', 'f620', 80000, 'modded'),
(106, 'Felon', 'felon', 42000, 'coupes'),
(107, 'Felon GT', 'felon2', 55000, 'coupes'),
(108, 'Jackal', 'jackal', 38000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
(112, 'Windsor', 'windsor', 350000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 180000, 'coupes'),
(114, 'Zion', 'zion', 40000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 70000, 'coupes'),
(116, '9F', 'ninef', 65000, 'sports'),
(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 125000, 'modded'),
(120, 'Bestia GTS', 'bestiagts', 400000, 'sports'),
(121, 'Buffalo', 'buffalo', 120000, 'modded'),
(122, 'wide body charger', 'buffalo2', 175000, 'modded'),
(123, 'Carbonizzare', 'carbonizzare', 110000, 'modded'),
(124, 'Comet', 'comet2', 200000, 'modded'),
(125, 'Coquette', 'coquette', 65000, 'sports'),
(126, 'Vantage', 'tampa2', 230000, 'sports'),
(127, 'Elegy', 'elegy2', 175000, 'sports'),
(128, 'Feltzer', 'feltzer2', 125000, 'sports'),
(129, 'Furore GT', 'furoregt', 45000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
(133, 'Khamelion', 'khamelion', 38000, 'sports'),
(134, 'Kuruma', 'kuruma', 75000, 'sports'),
(135, 'Lynx', 'lynx', 40000, 'sports'),
(136, 'Mamba', 'mamba', 70000, 'sports'),
(137, 'Massacro', 'massacro', 65000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
(139, 'Omnis', 'omnis', 35000, 'sports'),
(140, 'Penumbra', 'penumbra', 28000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
(143, 'Schafter V12', 'schafter3', 130000, 'sports'),
(144, 'Seven 70', 'seven70', 290000, 'sports'),
(145, 'Sultan', 'sultan', 55000, 'sports'),
(146, 'Surano', 'surano', 50000, 'sports'),
(147, 'Tropos', 'tropos', 95000, 'sports'),
(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
(149, 'Adder', 'adder', 1000000, 'modded'),
(150, 'Banshee 900R', 'banshee2', 350000, 'super'),
(151, 'Bullet', 'bullet', 250000, 'super'),
(152, 'Cheetah', 'cheetah', 500000, 'modded'),
(153, 'Entity XF', 'entityxf', 425000, 'super'),
(154, 'Maserati ET1', 'sheava', 500000, 'super'),
(155, 'FMJ', 'fmj', 185000, 'super'),
(156, 'Infernus', 'infernus', 240000, 'modded'),
(157, 'Osiris', 'osiris', 160000, 'super'),
(158, 'Pfister', 'pfister811', 85000, 'super'),
(159, 'RE-7B', 'le7b', 325000, 'super'),
(160, 'Reaper', 'reaper', 150000, 'super'),
(161, 'Sultan RS', 'sultanrs', 130000, 'super'),
(162, 'T20', 't20', 300000, 'super'),
(163, 'Turismo R', 'turismor', 350000, 'super'),
(164, 'Tyrus', 'tyrus', 600000, 'super'),
(165, 'Vacca', 'vacca', 120000, 'super'),
(166, 'Voltic', 'voltic', 900000, 'super'),
(167, 'X80 Proto', 'prototipo', 950000, 'super'),
(168, 'Zentorno', 'zentorno', 700000, 'super'),
(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
(174, 'BF400', 'bf400', 6500, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
(183, 'Double T', 'double', 28000, 'motorcycles'),
(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 7500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 7500, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 80000, 'motorcycles'),
(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
(207, 'Vader', 'vader', 7200, 'motorcycles'),
(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
(213, 'Ruiner', 'ruiner', 90000, 'muscle'),
(214, 'TunedComet', 'comet3', 300000, 'super'),
(215, 'TunedslamVan', 'slamvan3', 270000, 'muscle'),
(216, 'TunedVirgo', 'virgo3', 220000, 'muscle'),
(217, 'Nero', 'nero', 400000, 'super'),
(218, 'TunedBucaneer', 'buccaneer2', 200000, 'muscle'),
(219, 'TunedChino', 'chino2', 190000, 'muscle'),
(220, 'TunedElegy', 'elegy', 220000, 'sports'),
(221, 'TunedFaction', 'faction2', 190000, 'muscle'),
(222, 'PegassiFCR', 'fcr', 8000, 'motorcycles'),
(223, 'TunedPegassi', 'fcr2', 15000, 'motorcycles'),
(224, 'ProgenItaliGTB', 'italigtb', 260000, 'super'),
(225, 'TunedItaliGTB', 'italigtb2', 320000, 'super'),
(226, 'Tunedminivan', 'minivan2', 160000, 'suvs'),
(227, 'TunedNero', 'nero2', 500000, 'super'),
(228, 'Primo', 'primo', 35000, 'coupes'),
(229, 'DewbaucheeSpecter', 'specter', 220000, 'sports'),
(230, 'Tunedspecter', 'specter2', 290000, 'sports'),
(231, 'TunedVan', 'slamvan2', 25000, 'muscle'),
(232, 'EMSCar', 'emscar', 1000, 'work'),
(233, 'EMSCar2', 'emscar2', 1000, 'work'),
(234, 'EMSVan', 'emsvan', 1000, 'work'),
(235, 'EMSSuv', 'emssuv', 1000, 'work'),
(236, 'Rat Loader', 'ratloader', 10000, 'work'),
(237, 'Sand King2', 'sandking2', 60000, 'offroad'),
(238, 'Sadler', 'sadler', 75000, 'offroad'),
(239, 'Taxi', 'taxi', 15000, 'work'),
(240, 'Rubble', 'rubble', 100000, 'work'),
(241, 'Tour Bus', 'tourbus', 30000, 'work'),
(242, 'Tow Truck', 'towtruck', 25000, 'work'),
(243, 'Flat Bed', 'flatbed', 27000, 'work'),
(244, 'Clown', 'speedo2', 16000, 'work'),
(246, 'Stratum', 'stratum', 80000, 'sports'),
(249, 'Mazda', 'blista3', 40000, 'compacts'),
(250, 'Honda Civic', 'blista2', 30000, 'compacts'),
(251, 'Caddilac', 'buffalo3', 50000, 'sedans'),
(252, 'Golf Green', 'surge', 70000, 'compacts'),
(253, 'Stalion', 'stalion', 60000, 'muscle'),
(254, 'SRT', 'stalion2', 160000, 'muscle'),
(255, 'Mercedez RI', 'serrano', 90000, 'suvs'),
(256, 'Mercedes AMG', 'schafter4', 140000, 'sedans'),
(257, 'BMW M3', 'schafter5', 135000, 'sedans'),
(258, 'BMW M3', 'schwarzer', 150000, 'sedans'),
(259, 'gt500', 'gt500', 45000, 'doomsday'),
(260, 'comet4', 'comet4', 180000, 'doomsday'),
(261, 'comet5', 'comet5', 200000, 'doomsday'),
(263, 'hermes', 'hermes', 60000, 'doomsday'),
(264, 'hustler', 'hustler', 60000, 'doomsday'),
(265, 'kamacho', 'kamacho', 50000, 'doomsday'),
(266, 'neon', 'neon', 100000, 'doomsday'),
(267, 'pariah', 'pariah', 100000, 'doomsday'),
(268, 'raiden', 'raiden', 75000, 'doomsday'),
(269, 'revolter', 'revolter', 75000, 'doomsday'),
(270, 'riata', 'riata', 75000, 'doomsday'),
(271, 'savestra', 'savestra', 75000, 'doomsday'),
(272, 'sc1', 'sc1', 75000, 'doomsday'),
(273, 'streiter', 'streiter', 75000, 'doomsday'),
(274, 'stromberg', 'stromberg', 75000, 'doomsday'),
(275, 'sentinel3', 'sentinel3', 75000, 'doomsday'),
(276, 'viseris', 'viseris', 75000, 'doomsday'),
(277, 'yosemite', 'yosemite', 75000, 'doomsday'),
(278, 'z190', 'z190', 75000, 'doomsday'),
(279, 'Mustang', 'musty5', 90000, 'modded'),
(280, 'Infiniti G37', 'g37cs', 50000, 'modded'),
(281, 'Peugeot 107', 'p107', 30000, 'modded'),
(282, 'Renault Megane', 'renmeg', 70000, 'modded'),
(283, 'Lamborghini Hurricane', 'lh610', 230000, 'modded'),
(284, 'Aston Cygnet', 'cygnet11', 40000, 'modded'),
(285, 'Cadillac CTS', 'cadicts', 55000, 'modded'),
(286, 'Mini John Cooper', 'miniub', 45000, 'modded'),
(287, 'Lotus Espirit V8', 'lev8', 130000, 'modded'),
(288, 'Lambo Veneno', 'lamven', 500000, 'modded'),
(289, 'Nissan GTR SpecV', 'gtrublu', 230000, 'modded'),
(290, 'Genesis', 'genublu', 60000, 'modded'),
(291, 'Porsche Cayman R', 'caymanub', 190000, 'modded'),
(292, 'Porsche 911 GT', '911ublu', 700000, 'modded'),
(293, 'Ferrari Laferrari', 'laferublu', 450000, 'modded'),
(294, 'Mclaren 12c', 'mcublu', 400000, 'modded'),
(295, 'Merc SLR', 'slrublu', 200000, 'modded'),
(297, 'Merc SLS AMG Electric', 'slsublue', 150000, 'modded'),
(298, 'Dodge Charger', 'charublu', 90000, 'modded'),
(299, 'subaru 22b', '22bbublu', 80000, 'modded'),
(300, 'Focus RS', 'focusublu', 70000, 'modded'),
(301, 'Mazda furai', 'furaiub', 350000, 'modded'),
(302, 'Ferrari F50', 'f50ub', 350000, 'modded'),
(303, 'Porsche 550a', 'p550a', 250000, 'modded'),
(304, 'Porsche 959', 'p959', 250000, 'modded'),
(305, 'Porsche 944', 'p944', 180000, 'modded'),
(306, 'dodge Viper', 'vip99', 450000, 'modded'),
(307, 'Mazda Rx8', 'rx8', 50000, 'modded'),
(308, 'Ferrari 599', 'gtbf', 290000, 'modded'),
(309, 'Tesla Roadster', 'tesla11', 55000, 'modded'),
(310, 'Mazda Mx5a', 'mx5a', 45000, 'modded'),
(311, 'toyota Celica', 'celicassi', 48000, 'modded'),
(312, 'Toyota celica T', 'celicassi2', 55000, 'modded'),
(313, 'Aston Martin Vanquish', 'amv12', 280000, 'modded'),
(314, 'Subari WRX STI', 'sti05', 80000, 'modded'),
(315, 'Porsche Panamera', 'panamera', 200000, 'modded'),
(316, 'Ferrari 360', 'f360', 250000, 'modded'),
(317, 'Lambo Mura', 'miura', 230000, 'modded'),
(318, 'chevrolet Corvette', 'zr1c3', 180000, 'modded'),
(319, 'Lambo Gallardo', 'gallardo', 300000, 'modded'),
(320, 'Corvette Stingray', 'vc7', 230000, 'modded'),
(321, 'Ferrari Cali', '2fiftygt', 260000, 'modded'),
(322, 'Mercedz Gullwing', '300gsl', 200000, 'modded'),
(323, 'Aston Martin vantage', 'db700', 140000, 'modded'),
(324, 'shelby cobra', 'cobra', 130000, 'modded'),
(325, 'BMW Z4i', 'z4i', 100000, 'modded'),
(326, 'Lambo Huracan', 'huracan', 240000, 'modded'),
(327, 'Ferrari 812', 'ferrari812', 250000, 'modded'),
(328, 'Lambo Veneno', 'veneno', 350000, 'modded'),
(329, 'Ferrari XXK', 'fxxk16', 300000, 'modded'),
(330, 'LaFerrari 15', 'laferrari15', 400000, 'modded'),
(331, 'Italia 458 LW', 'lw458s', 320000, 'modded'),
(332, 'Lykan', 'lykan', 350000, 'modded'),
(333, 'iTalia 458', 'italia458', 290000, 'modded'),
(334, 'Diablous', 'Diablous', 15000, 'motorcycles'),
(335, 'Diablous 2', 'Diablous2', 17000, 'motorcycles'),
(336, 'Raptor', 'Raptor', 20000, 'motorcycles'),
(337, 'Ratbike', 'Ratbike', 16000, 'motorcycles'),
(338, 'Xa21', 'XA21', 340000, 'super'),
(339, 'Penetrator', 'Penetrator', 300000, 'super'),
(340, 'Gp1', 'GP1', 270000, 'super'),
(341, 'Tempestra', 'Tempesta', 260000, 'super'),
(342, 'Toreo', 'Torero', 250000, 'sportsclassics'),
(343, 'Infernus2', 'Infernus2', 240000, 'sportsclassics'),
(344, 'Savestra', 'Savestra', 240000, 'sportsclassics'),
(345, 'Cheetah2', 'Cheetah2', 200000, 'sportsclassics'),
(346, 'Turismo2', 'Turismo2', 180000, 'sportsclassics'),
(347, 'Viceris', 'Viceris', 190000, 'sportsclassics'),
(348, 'JB700', 'JB700', 190000, 'sportsclassics'),
(349, 'Peyote', 'Peyote', 175000, 'sportsclassics'),
(350, 'Ruston', 'Ruston', 150000, 'sports'),
(351, 'Surge', 'Surge', 45000, 'sedans'),
(353, 'Voltic', 'Voltic2', 4000000, 'super'),
(354, 'Dilettante', 'Dilettante', 50000, 'compacts'),
(355, 'Tornado6', 'Tornado6', 150000, 'muscle'),
(356, 'Gauntlet2', 'Gauntlet2', 100000, 'muscle'),
(357, 'Dominator2', 'Dominator2', 100000, 'muscle'),
(358, 'Hurse', 'Lurcher', 60000, 'muscle'),
(359, 'Vagner', 'Vagner', 250000, 'super'),
(360, 'Austarch', 'Autarch', 230000, 'super'),
(361, 'Tornado5', 'tornado5', 75000, 'muscle'),
(362, 'audi a4', 'asterope', 110000, 'sports'),
(363, 'Merc AMG', 'rmodamgc63', 165000, 'sports'),
(364, 'dodgeCharger', '69charger', 140000, 'muscle'),
(365, 'R35', 'r35', 200000, 'modded'),
(367, 'Mustang', 'mgt', 180000, 'modded'),
(368, 'Cheburek', 'cheburek', 20000, 'assault'),
(369, 'Ellie', 'ellie', 100000, 'assault'),
(370, 'Dommy3', 'dominator3', 110000, 'assault'),
(371, 'Enity2', 'entity2', 200000, 'assault'),
(372, 'Fagi', 'fagaloa', 22000, 'assault'),
(373, 'Flash', 'flashgt', 105000, 'assault'),
(374, 'RS200', 'gb200', 135000, 'assault'),
(375, 'Hotring', 'hotring', 140000, 'assault'),
(376, 'Mini', 'issi3', 18000, 'assault'),
(377, 'Jester3', 'jester3', 85000, 'assault'),
(378, 'Michelle', 'michelli', 41000, 'assault'),
(379, 'Tai', 'taipan', 220000, 'assault'),
(380, 'Tezeract', 'tezeract', 180000, 'assault'),
(381, 'Tyrant', 'tyrant', 220000, 'assault'),
(382, 'Impreza', 'ySbrImpS11', 100000, 'modded'),
(383, 'R35 Skyline', 'r35', 200000, 'sports'),
(384, 'Insurgent', 'insurgent3', 200000, 'modded'),
(385, 'Halftrack', 'halftrack', 200000, 'modded'),
(386, 'Tempa', 'tampa3', 200000, 'modded'),
(387, 'Technical', 'technical3', 200000, 'modded'),
(388, 'Tech2', 'technical2', 200000, 'modded'),
(389, 'Barrage', 'barrage', 200000, 'modded'),
(390, 'Boxville', 'boxville5', 200000, 'modded'),
(391, 'Road Glide', 'foxharley2', 3000, 'motorcycles'),
(392, 'Harley Bobber', 'foxharley1', 3000, 'motorcycles');

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicle_categories`
--

CREATE TABLE `old_vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicle_categories`
--

INSERT INTO `old_vehicle_categories` (`id`, `name`, `label`) VALUES
(1, 'compacts', 'Compacts'),
(2, 'coupes', 'Coupés'),
(3, 'sedans', 'Sedans'),
(4, 'sports', 'Sports'),
(5, 'sportsclassics', 'Sports Classics'),
(6, 'super', 'Super'),
(7, 'muscle', 'Muscle'),
(8, 'offroad', 'Off Road'),
(9, 'suvs', 'SUVs'),
(10, 'vans', 'Vans'),
(11, 'motorcycles', 'Motos'),
(12, 'work', 'Work'),
(13, 'doomsday', 'doomsday'),
(14, 'modded', 'Modded'),
(15, 'assault', 'assault');

-- --------------------------------------------------------

--
-- Table structure for table `outfits`
--

CREATE TABLE `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) NOT NULL COMMENT 'mp_m_freemode_01',
  `face` int(11) NOT NULL COMMENT '0',
  `face_text` int(11) NOT NULL COMMENT '0',
  `hair` int(11) NOT NULL COMMENT '0',
  `pants` int(11) NOT NULL COMMENT '0',
  `pants_text` int(11) NOT NULL COMMENT '0',
  `shoes` int(11) NOT NULL COMMENT '0',
  `shoes_text` int(11) NOT NULL COMMENT '0',
  `torso` int(11) NOT NULL COMMENT '0',
  `torso_text` int(11) NOT NULL COMMENT '0',
  `shirt` int(11) NOT NULL COMMENT '0',
  `shirt_text` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `state of the car` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'State of the car',
  `finance` int(32) NOT NULL DEFAULT '0',
  `financetimer` int(32) NOT NULL DEFAULT '0',
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `stored` tinyint(1) NOT NULL DEFAULT '0',
  `jamstate` tinyint(11) NOT NULL DEFAULT '0',
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT '0',
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isRead` int(11) NOT NULL DEFAULT '0',
  `owner` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `steam` varchar(50) NOT NULL,
  `playtime` int(11) NOT NULL,
  `firstjoined` varchar(50) NOT NULL,
  `lastplayed` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Apartment de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modyrn1Apartment', 'Apartment Modern 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modyrn2Apartment', 'Apartment Modern 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modyrn3Apartment', 'Apartment Modern 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Apartment Mody 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Apartment Mody 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Apartment Mody 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Apartment Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Apartment Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Apartment Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Apartment Persian 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Apartment Persian 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Apartment Persian 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Apartment Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Apartment Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Apartment Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Apartment Seductive 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Apartment Seductive 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Apartment Seductive 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Apartment Regal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Apartment Regal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Apartment Regal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Apartment Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Apartment Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Apartment Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `received_bans`
--

CREATE TABLE `received_bans` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `banned_by` varchar(255) DEFAULT NULL,
  `banned_on` varchar(255) DEFAULT NULL,
  `ban_expires` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `ID` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `connection` int(11) NOT NULL,
  `rcon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `server_actions`
--

CREATE TABLE `server_actions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_do` varchar(255) DEFAULT NULL,
  `action_ammount` varchar(255) DEFAULT NULL,
  `byadmin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'PDShop', 'coffee', 1),
(2, 'PDShop', 'donut', 1),
(3, 'PDShop', 'clip', 1),
(4, 'PDShop', 'armor', 1),
(5, 'PDShop', 'medikit', 0),
(6, 'CityHall', 'weapons_license1', 100),
(7, 'CityHall', 'weapons_license2', 200),
(8, 'CityHall', 'weapons_license3', 300),
(62, 'CityHall', 'hunting_license', 100),
(63, 'CityHall', 'fishing_license', 100),
(64, 'CityHall', 'diving_license', 50),
(65, 'CityHall', 'marriage_license', 5000),
(66, 'DMV', 'drivers_license', 80),
(67, 'DMV', 'motorcycle_license', 80),
(68, 'DMV', 'commercial_license', 175),
(69, 'DMV', 'boating_license', 40),
(70, 'DMV', 'taxi_license', 175),
(71, 'DMV', 'pilot_license', 500);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(38, 'Jak Fulton', 'fuck4tick', NULL),
(39, 'tpickles', 'stoner420', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 39, 'steam:11000010a01bdb9', 'New city who dis?', '2019-08-09 03:24:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `rank` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `steamid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeplayed` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `online` int(10) NOT NULL DEFAULT '0',
  `server` int(10) NOT NULL DEFAULT '1',
  `is_dead` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `ID`, `rank`, `steamid`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `last_property`, `animal`, `timeplayed`, `online`, `server`, `is_dead`) VALUES
('steam:110000112969e8f', 36, '', '', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 0, 'SuperSteve902', NULL, 'unemployed', 0, '[]', '{\"y\":0.0,\"z\":0.0,\"x\":0.0}', 0, 0, 'user', '[{\"name\":\"hunger\",\"val\":952400,\"percent\":95.24},{\"name\":\"thirst\",\"val\":940500,\"percent\":94.05},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', 'Steven', 'Super', '05/08/1998', 'M', '80', '415-8959', NULL, NULL, '0', 0, 1, 0),
('steam:11000010a01bdb9', 38, '', '', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', -7500, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"y\":0.0,\"z\":0.0,\"x\":0.0}', 99987040, 0, 'user', '[{\"val\":345800,\"percent\":34.58,\"name\":\"hunger\"},{\"val\":307250,\"percent\":30.725,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'T', 'P', '12/28/1988', 'M', '62', '626-4798', NULL, NULL, '0', 0, 1, 0),
('steam:1100001068ef13c', 40, '', '', 'license:a4979e4221783962685bb8a6105e2b93fc364e77', 0, 'Soft-Hearted Devil', NULL, 'unemployed', 0, '[]', '{\"z\":0.0,\"y\":0.0,\"x\":0.0}', 0, 0, 'user', '[{\"val\":492200,\"percent\":49.22,\"name\":\"hunger\"},{\"val\":490250,\"percent\":49.025,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'William', 'Woodard', '7/27/1989', 'M', '72', '840-7465', NULL, NULL, '0', 0, 1, 0),
('steam:11000013ca7f6bf', 41, '', '', 'license:e9bd935234b86be8ff3bbe5c5a949e6d2fb3adc9', 0, 'jrende17', NULL, 'unemployed', 0, '[]', '{\"y\":0.0,\"x\":0.0,\"z\":0.0}', 100, 0, 'user', '[{\"percent\":92.86,\"name\":\"hunger\",\"val\":928600},{\"percent\":91.075,\"name\":\"thirst\",\"val\":910750},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', '', '', '', '', '', '273-8087', NULL, NULL, '0', 0, 1, 0),
('steam:110000132580eb0', 43, '', '', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 0, 'K9Marine', NULL, 'biker', 3, '[]', '{\"x\":0.0,\"z\":0.0,\"y\":0.0}', 120400, 0, 'user', '[{\"percent\":32.6,\"name\":\"hunger\",\"val\":326000},{\"percent\":28.25,\"name\":\"thirst\",\"val\":282500},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Jak', 'Fulton', '10/10/1988', 'M', '74', '739-5135', NULL, NULL, '0', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(35, 'steam:110000112969e8f', 'black_money', 0),
(37, 'steam:11000010a01bdb9', 'black_money', 0),
(39, 'steam:1100001068ef13c', 'black_money', 0),
(40, 'steam:11000013ca7f6bf', 'black_money', 0),
(42, 'steam:110000132580eb0', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin_notes`
--

CREATE TABLE `user_admin_notes` (
  `id` int(11) NOT NULL,
  `note` longblob,
  `admin` varchar(255) DEFAULT NULL,
  `note_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1196, 'steam:110000112969e8f', 'tequila', 0),
(1197, 'steam:110000112969e8f', 'petrol', 0),
(1198, 'steam:110000112969e8f', 'crack', 0),
(1199, 'steam:110000112969e8f', 'croquettes', 0),
(1200, 'steam:110000112969e8f', 'litter', 0),
(1201, 'steam:110000112969e8f', 'grip', 0),
(1202, 'steam:110000112969e8f', 'bolpistache', 0),
(1203, 'steam:110000112969e8f', 'weapons_license2', 0),
(1204, 'steam:110000112969e8f', 'turtle_pooch', 0),
(1205, 'steam:110000112969e8f', 'plongee2', 0),
(1206, 'steam:110000112969e8f', 'whiskycoca', 0),
(1207, 'steam:110000112969e8f', 'poppy', 0),
(1208, 'steam:110000112969e8f', 'opium_pooch', 0),
(1209, 'steam:110000112969e8f', 'water', 0),
(1210, 'steam:110000112969e8f', 'donut', 0),
(1211, 'steam:110000112969e8f', 'scratchoff', 0),
(1212, 'steam:110000112969e8f', 'limonade', 0),
(1213, 'steam:110000112969e8f', 'carotool', 0),
(1214, 'steam:110000112969e8f', 'silencieux', 0),
(1215, 'steam:110000112969e8f', 'meat', 0),
(1216, 'steam:110000112969e8f', 'flashlight', 0),
(1217, 'steam:110000112969e8f', 'grapperaisin', 0),
(1218, 'steam:110000112969e8f', 'plongee1', 0),
(1219, 'steam:110000112969e8f', 'chips', 0),
(1220, 'steam:110000112969e8f', 'tacos', 0),
(1221, 'steam:110000112969e8f', 'lighter', 0),
(1222, 'steam:110000112969e8f', 'boating_license', 0),
(1223, 'steam:110000112969e8f', 'diamond', 0),
(1224, 'steam:110000112969e8f', 'litter_pooch', 0),
(1225, 'steam:110000112969e8f', 'turtle', 0),
(1226, 'steam:110000112969e8f', 'cutted_wood', 0),
(1227, 'steam:110000112969e8f', 'narcan', 0),
(1228, 'steam:110000112969e8f', 'pizza', 0),
(1229, 'steam:110000112969e8f', 'sportlunch', 0),
(1230, 'steam:110000112969e8f', 'boitier', 0),
(1231, 'steam:110000112969e8f', 'packaged_plank', 0),
(1232, 'steam:110000112969e8f', 'jager', 0),
(1233, 'steam:110000112969e8f', 'cigarett', 0),
(1234, 'steam:110000112969e8f', 'menthe', 0),
(1235, 'steam:110000112969e8f', 'petrol_raffin', 0),
(1236, 'steam:110000112969e8f', 'meth_pooch', 0),
(1237, 'steam:110000112969e8f', 'coffee', 0),
(1238, 'steam:110000112969e8f', 'blackberry', 0),
(1239, 'steam:110000112969e8f', 'sprite', 0),
(1240, 'steam:110000112969e8f', 'fixtool', 0),
(1241, 'steam:110000112969e8f', 'contrat', 0),
(1242, 'steam:110000112969e8f', 'saucisson', 0),
(1243, 'steam:110000112969e8f', 'pilot_license', 0),
(1244, 'steam:110000112969e8f', 'cocaine', 0),
(1245, 'steam:110000112969e8f', 'blowpipe', 0),
(1246, 'steam:110000112969e8f', 'wood', 0),
(1247, 'steam:110000112969e8f', 'breathalyzer', 0),
(1248, 'steam:110000112969e8f', 'teqpaf', 0),
(1249, 'steam:110000112969e8f', 'stone', 0),
(1250, 'steam:110000112969e8f', 'vodkafruit', 0),
(1251, 'steam:110000112969e8f', 'fabric', 0),
(1252, 'steam:110000112969e8f', 'lsd_pooch', 0),
(1253, 'steam:110000112969e8f', 'pills', 0),
(1254, 'steam:110000112969e8f', 'vodkaenergy', 0),
(1255, 'steam:110000112969e8f', 'burger', 0),
(1256, 'steam:110000112969e8f', 'taxi_license', 0),
(1257, 'steam:110000112969e8f', 'jusfruit', 0),
(1258, 'steam:110000112969e8f', 'opium', 0),
(1259, 'steam:110000112969e8f', 'energy', 0),
(1260, 'steam:110000112969e8f', 'drugtest', 0),
(1261, 'steam:110000112969e8f', 'martini', 0),
(1262, 'steam:110000112969e8f', 'macka', 0),
(1263, 'steam:110000112969e8f', 'whisky', 0),
(1264, 'steam:110000112969e8f', 'weed_pooch', 0),
(1265, 'steam:110000112969e8f', 'metreshooter', 0),
(1266, 'steam:110000112969e8f', 'lsd', 0),
(1267, 'steam:110000112969e8f', 'bolnoixcajou', 0),
(1268, 'steam:110000112969e8f', 'packaged_chicken', 0),
(1269, 'steam:110000112969e8f', 'ephedrine', 0),
(1270, 'steam:110000112969e8f', 'whiskey', 0),
(1271, 'steam:110000112969e8f', 'leather', 0),
(1272, 'steam:110000112969e8f', 'iron', 0),
(1273, 'steam:110000112969e8f', 'alive_chicken', 0),
(1274, 'steam:110000112969e8f', 'rhum', 0),
(1275, 'steam:110000112969e8f', 'gym_membership', 0),
(1276, 'steam:110000112969e8f', 'pearl_pooch', 0),
(1277, 'steam:110000112969e8f', 'essence', 0),
(1278, 'steam:110000112969e8f', 'beer', 0),
(1279, 'steam:110000112969e8f', 'drivers_license', 0),
(1280, 'steam:110000112969e8f', 'motorcycle_license', 0),
(1281, 'steam:110000112969e8f', 'bread', 0),
(1282, 'steam:110000112969e8f', 'marijuana', 0),
(1283, 'steam:110000112969e8f', 'weapons_license1', 0),
(1284, 'steam:110000112969e8f', 'vegetables', 0),
(1285, 'steam:110000112969e8f', 'marriage_license', 0),
(1286, 'steam:110000112969e8f', 'fishing_license', 0),
(1287, 'steam:110000112969e8f', 'diving_license', 0),
(1288, 'steam:110000112969e8f', 'protein_shake', 0),
(1289, 'steam:110000112969e8f', 'hunting_license', 0),
(1290, 'steam:110000112969e8f', 'jagerbomb', 0),
(1291, 'steam:110000112969e8f', 'coke', 0),
(1292, 'steam:110000112969e8f', 'carokit', 0),
(1293, 'steam:110000112969e8f', 'weapons_license3', 0),
(1294, 'steam:110000112969e8f', 'lotteryticket', 0),
(1295, 'steam:110000112969e8f', 'fanta', 0),
(1296, 'steam:110000112969e8f', 'pastacarbonara', 0),
(1297, 'steam:110000112969e8f', 'slaughtered_chicken', 0),
(1298, 'steam:110000112969e8f', 'rhumfruit', 0),
(1299, 'steam:110000112969e8f', 'coca', 0),
(1300, 'steam:110000112969e8f', 'armor', 0),
(1301, 'steam:110000112969e8f', 'dabs', 0),
(1302, 'steam:110000112969e8f', 'loka', 0),
(1303, 'steam:110000112969e8f', 'cola', 0),
(1304, 'steam:110000112969e8f', 'painkiller', 0),
(1305, 'steam:110000112969e8f', 'cocacola', 0),
(1306, 'steam:110000112969e8f', 'fixkit', 0),
(1307, 'steam:110000112969e8f', 'fakepee', 0),
(1308, 'steam:110000112969e8f', 'scratchoff_used', 0),
(1309, 'steam:110000112969e8f', 'heroine', 0),
(1310, 'steam:110000112969e8f', 'ephedra', 0),
(1311, 'steam:110000112969e8f', 'bolcacahuetes', 0),
(1312, 'steam:110000112969e8f', 'bolchips', 0),
(1313, 'steam:110000112969e8f', 'golem', 0),
(1314, 'steam:110000112969e8f', 'cheesebows', 0),
(1315, 'steam:110000112969e8f', 'vodka', 0),
(1316, 'steam:110000112969e8f', 'binoculars', 0),
(1317, 'steam:110000112969e8f', 'cannabis', 0),
(1318, 'steam:110000112969e8f', 'commercial_license', 0),
(1319, 'steam:110000112969e8f', 'drpepper', 0),
(1320, 'steam:110000112969e8f', 'soda', 0),
(1321, 'steam:110000112969e8f', 'copper', 0),
(1322, 'steam:110000112969e8f', 'rhumcoca', 0),
(1323, 'steam:110000112969e8f', 'lockpick', 0),
(1324, 'steam:110000112969e8f', 'powerade', 0),
(1325, 'steam:110000112969e8f', 'clothe', 0),
(1326, 'steam:110000112969e8f', 'icetea', 0),
(1327, 'steam:110000112969e8f', 'mojito', 0),
(1328, 'steam:110000112969e8f', 'gazbottle', 0),
(1329, 'steam:110000112969e8f', 'fish', 0),
(1330, 'steam:110000112969e8f', 'yusuf', 0),
(1331, 'steam:110000112969e8f', 'marabou', 0),
(1332, 'steam:110000112969e8f', 'ice', 0),
(1333, 'steam:110000112969e8f', 'medikit', 0),
(1334, 'steam:110000112969e8f', 'clip', 0),
(1335, 'steam:110000112969e8f', 'pearl', 0),
(1336, 'steam:110000112969e8f', 'weed', 0),
(1337, 'steam:110000112969e8f', 'mixapero', 0),
(1338, 'steam:110000112969e8f', 'pcp', 0),
(1339, 'steam:110000112969e8f', 'washed_stone', 0),
(1340, 'steam:110000112969e8f', 'meth', 0),
(1341, 'steam:110000112969e8f', 'bandage', 0),
(1342, 'steam:110000112969e8f', 'gold', 0),
(1343, 'steam:110000112969e8f', 'whool', 0),
(1344, 'steam:110000112969e8f', 'coke_pooch', 0),
(1494, 'steam:11000010a01bdb9', 'coke', 0),
(1495, 'steam:11000010a01bdb9', 'gold', 0),
(1496, 'steam:11000010a01bdb9', 'lighter', 0),
(1497, 'steam:11000010a01bdb9', 'gazbottle', 0),
(1498, 'steam:11000010a01bdb9', 'whisky', 0),
(1499, 'steam:11000010a01bdb9', 'diamond', 0),
(1500, 'steam:11000010a01bdb9', 'tacos', 0),
(1501, 'steam:11000010a01bdb9', 'opium', 0),
(1502, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(1503, 'steam:11000010a01bdb9', 'donut', 0),
(1504, 'steam:11000010a01bdb9', 'pearl_pooch', 0),
(1505, 'steam:11000010a01bdb9', 'medikit', 0),
(1506, 'steam:11000010a01bdb9', 'martini', 0),
(1507, 'steam:11000010a01bdb9', 'petrol', 0),
(1508, 'steam:11000010a01bdb9', 'cheesebows', 0),
(1509, 'steam:11000010a01bdb9', 'ice', 0),
(1510, 'steam:11000010a01bdb9', 'scratchoff_used', 0),
(1511, 'steam:11000010a01bdb9', 'copper', 0),
(1512, 'steam:11000010a01bdb9', 'vegetables', 0),
(1513, 'steam:11000010a01bdb9', 'clothe', 0),
(1514, 'steam:11000010a01bdb9', 'binoculars', 0),
(1515, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(1516, 'steam:11000010a01bdb9', 'leather', 0),
(1517, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(1518, 'steam:11000010a01bdb9', 'teqpaf', 0),
(1519, 'steam:11000010a01bdb9', 'saucisson', 0),
(1520, 'steam:11000010a01bdb9', 'beer', 0),
(1521, 'steam:11000010a01bdb9', 'litter', 0),
(1522, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(1523, 'steam:11000010a01bdb9', 'flashlight', 0),
(1524, 'steam:11000010a01bdb9', 'carokit', 0),
(1525, 'steam:11000010a01bdb9', 'cocacola', 0),
(1526, 'steam:11000010a01bdb9', 'cocaine', 0),
(1527, 'steam:11000010a01bdb9', 'meth', 0),
(1528, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(1529, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(1530, 'steam:11000010a01bdb9', 'rhum', 0),
(1531, 'steam:11000010a01bdb9', 'heroine', 0),
(1532, 'steam:11000010a01bdb9', 'fanta', 0),
(1533, 'steam:11000010a01bdb9', 'plongee1', 0),
(1534, 'steam:11000010a01bdb9', 'bread', 0),
(1535, 'steam:11000010a01bdb9', 'silencieux', 0),
(1536, 'steam:11000010a01bdb9', 'metreshooter', 0),
(1537, 'steam:11000010a01bdb9', 'croquettes', 0),
(1538, 'steam:11000010a01bdb9', 'pills', 0),
(1539, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(1540, 'steam:11000010a01bdb9', 'painkiller', 0),
(1541, 'steam:11000010a01bdb9', 'ephedra', 0),
(1542, 'steam:11000010a01bdb9', 'fixtool', 0),
(1543, 'steam:11000010a01bdb9', 'armor', 0),
(1544, 'steam:11000010a01bdb9', 'icetea', 0),
(1545, 'steam:11000010a01bdb9', 'macka', 0),
(1546, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(1547, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(1548, 'steam:11000010a01bdb9', 'wood', 0),
(1549, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(1550, 'steam:11000010a01bdb9', 'menthe', 0),
(1551, 'steam:11000010a01bdb9', 'pcp', 0),
(1552, 'steam:11000010a01bdb9', 'lsd', 0),
(1553, 'steam:11000010a01bdb9', 'drivers_license', 0),
(1554, 'steam:11000010a01bdb9', 'coca', 0),
(1555, 'steam:11000010a01bdb9', 'pastacarbonara', 0),
(1556, 'steam:11000010a01bdb9', 'blackberry', 0),
(1557, 'steam:11000010a01bdb9', 'meat', 0),
(1558, 'steam:11000010a01bdb9', 'lockpick', 0),
(1559, 'steam:11000010a01bdb9', 'commercial_license', 0),
(1560, 'steam:11000010a01bdb9', 'taxi_license', 0),
(1561, 'steam:11000010a01bdb9', 'cannabis', 0),
(1562, 'steam:11000010a01bdb9', 'bolpistache', 0),
(1563, 'steam:11000010a01bdb9', 'ephedrine', 0),
(1564, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(1565, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(1566, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(1567, 'steam:11000010a01bdb9', 'pizza', 0),
(1568, 'steam:11000010a01bdb9', 'grip', 0),
(1569, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(1570, 'steam:11000010a01bdb9', 'contrat', 0),
(1571, 'steam:11000010a01bdb9', 'fishing_license', 0),
(1572, 'steam:11000010a01bdb9', 'stone', 0),
(1573, 'steam:11000010a01bdb9', 'jager', 0),
(1574, 'steam:11000010a01bdb9', 'boating_license', 0),
(1575, 'steam:11000010a01bdb9', 'pearl', 0),
(1576, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(1577, 'steam:11000010a01bdb9', 'chips', 0),
(1578, 'steam:11000010a01bdb9', 'pilot_license', 0),
(1579, 'steam:11000010a01bdb9', 'gym_membership', 0),
(1580, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(1581, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(1582, 'steam:11000010a01bdb9', 'washed_stone', 0),
(1583, 'steam:11000010a01bdb9', 'fabric', 0),
(1584, 'steam:11000010a01bdb9', 'diving_license', 0),
(1585, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(1586, 'steam:11000010a01bdb9', 'limonade', 0),
(1587, 'steam:11000010a01bdb9', 'hunting_license', 0),
(1588, 'steam:11000010a01bdb9', 'lotteryticket', 0),
(1589, 'steam:11000010a01bdb9', 'protein_shake', 0),
(1590, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(1591, 'steam:11000010a01bdb9', 'blowpipe', 0),
(1592, 'steam:11000010a01bdb9', 'loka', 0),
(1593, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(1594, 'steam:11000010a01bdb9', 'bandage', 0),
(1595, 'steam:11000010a01bdb9', 'turtle', 0),
(1596, 'steam:11000010a01bdb9', 'energy', 0),
(1597, 'steam:11000010a01bdb9', 'weed', 0),
(1598, 'steam:11000010a01bdb9', 'sprite', 0),
(1599, 'steam:11000010a01bdb9', 'boitier', 0),
(1600, 'steam:11000010a01bdb9', 'fixkit', 0),
(1601, 'steam:11000010a01bdb9', 'marriage_license', 0),
(1602, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(1603, 'steam:11000010a01bdb9', 'marabou', 0),
(1604, 'steam:11000010a01bdb9', 'narcan', 0),
(1605, 'steam:11000010a01bdb9', 'bolchips', 0),
(1606, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(1607, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(1608, 'steam:11000010a01bdb9', 'litter_pooch', 0),
(1609, 'steam:11000010a01bdb9', 'dabs', 0),
(1610, 'steam:11000010a01bdb9', 'fakepee', 0),
(1611, 'steam:11000010a01bdb9', 'sportlunch', 0),
(1612, 'steam:11000010a01bdb9', 'jusfruit', 0),
(1613, 'steam:11000010a01bdb9', 'cola', 0),
(1614, 'steam:11000010a01bdb9', 'water', 0),
(1615, 'steam:11000010a01bdb9', 'drugtest', 0),
(1616, 'steam:11000010a01bdb9', 'crack', 0),
(1617, 'steam:11000010a01bdb9', 'whool', 0),
(1618, 'steam:11000010a01bdb9', 'whiskey', 0),
(1619, 'steam:11000010a01bdb9', 'poppy', 0),
(1620, 'steam:11000010a01bdb9', 'yusuf', 0),
(1621, 'steam:11000010a01bdb9', 'marijuana', 0),
(1622, 'steam:11000010a01bdb9', 'vodka', 0),
(1623, 'steam:11000010a01bdb9', 'essence', 0),
(1624, 'steam:11000010a01bdb9', 'fish', 0),
(1625, 'steam:11000010a01bdb9', 'coffee', 0),
(1626, 'steam:11000010a01bdb9', 'iron', 0),
(1627, 'steam:11000010a01bdb9', 'golem', 0),
(1628, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(1629, 'steam:11000010a01bdb9', 'scratchoff', 0),
(1630, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(1631, 'steam:11000010a01bdb9', 'powerade', 0),
(1632, 'steam:11000010a01bdb9', 'plongee2', 0),
(1633, 'steam:11000010a01bdb9', 'drpepper', 0),
(1634, 'steam:11000010a01bdb9', 'carotool', 0),
(1635, 'steam:11000010a01bdb9', 'tequila', 0),
(1636, 'steam:11000010a01bdb9', 'cigarett', 2),
(1637, 'steam:11000010a01bdb9', 'soda', 0),
(1638, 'steam:11000010a01bdb9', 'mojito', 0),
(1639, 'steam:11000010a01bdb9', 'burger', 0),
(1640, 'steam:11000010a01bdb9', 'mixapero', 0),
(1641, 'steam:11000010a01bdb9', 'clip', 0),
(1642, 'steam:11000010a01bdb9', 'turtle_pooch', 0),
(1643, 'steam:11000010a01bdb9', 'baconburger', 0),
(1644, 'steam:11000010a01bdb9', 'firstaidpass', 1),
(1796, 'steam:1100001068ef13c', 'plongee1', 0),
(1797, 'steam:1100001068ef13c', 'carotool', 0),
(1798, 'steam:1100001068ef13c', 'crack', 0),
(1799, 'steam:1100001068ef13c', 'pearl', 0),
(1800, 'steam:1100001068ef13c', 'bolnoixcajou', 0),
(1801, 'steam:1100001068ef13c', 'cannabis', 0),
(1802, 'steam:1100001068ef13c', 'cola', 0),
(1803, 'steam:1100001068ef13c', 'iron', 0),
(1804, 'steam:1100001068ef13c', 'loka', 0),
(1805, 'steam:1100001068ef13c', 'boitier', 0),
(1806, 'steam:1100001068ef13c', 'diving_license', 0),
(1807, 'steam:1100001068ef13c', 'weed_pooch', 0),
(1808, 'steam:1100001068ef13c', 'coffee', 0),
(1809, 'steam:1100001068ef13c', 'coca', 0),
(1810, 'steam:1100001068ef13c', 'donut', 0),
(1811, 'steam:1100001068ef13c', 'rhumcoca', 0),
(1812, 'steam:1100001068ef13c', 'commercial_license', 0),
(1813, 'steam:1100001068ef13c', 'stone', 0),
(1814, 'steam:1100001068ef13c', 'sprite', 0),
(1815, 'steam:1100001068ef13c', 'turtle_pooch', 0),
(1816, 'steam:1100001068ef13c', 'petrol_raffin', 0),
(1817, 'steam:1100001068ef13c', 'martini', 0),
(1818, 'steam:1100001068ef13c', 'mojito', 0),
(1819, 'steam:1100001068ef13c', 'scratchoff', 0),
(1820, 'steam:1100001068ef13c', 'marijuana', 0),
(1821, 'steam:1100001068ef13c', 'grip', 0),
(1822, 'steam:1100001068ef13c', 'opium_pooch', 0),
(1823, 'steam:1100001068ef13c', 'clothe', 0),
(1824, 'steam:1100001068ef13c', 'baconburger', 0),
(1825, 'steam:1100001068ef13c', 'jager', 0),
(1826, 'steam:1100001068ef13c', 'icetea', 0),
(1827, 'steam:1100001068ef13c', 'washed_stone', 0),
(1828, 'steam:1100001068ef13c', 'lsd_pooch', 0),
(1829, 'steam:1100001068ef13c', 'yusuf', 0),
(1830, 'steam:1100001068ef13c', 'metreshooter', 0),
(1831, 'steam:1100001068ef13c', 'dabs', 0),
(1832, 'steam:1100001068ef13c', 'powerade', 0),
(1833, 'steam:1100001068ef13c', 'lighter', 0),
(1834, 'steam:1100001068ef13c', 'poppy', 0),
(1835, 'steam:1100001068ef13c', 'cocaine', 0),
(1836, 'steam:1100001068ef13c', 'armor', 0),
(1837, 'steam:1100001068ef13c', 'rhum', 0),
(1838, 'steam:1100001068ef13c', 'bolchips', 0),
(1839, 'steam:1100001068ef13c', 'bread', 0),
(1840, 'steam:1100001068ef13c', 'opium', 0),
(1841, 'steam:1100001068ef13c', 'cigarett', 0),
(1842, 'steam:1100001068ef13c', 'fanta', 0),
(1843, 'steam:1100001068ef13c', 'burger', 0),
(1844, 'steam:1100001068ef13c', 'scratchoff_used', 0),
(1845, 'steam:1100001068ef13c', 'copper', 0),
(1846, 'steam:1100001068ef13c', 'vodkaenergy', 0),
(1847, 'steam:1100001068ef13c', 'contrat', 0),
(1848, 'steam:1100001068ef13c', 'carokit', 0),
(1849, 'steam:1100001068ef13c', 'drugtest', 0),
(1850, 'steam:1100001068ef13c', 'gym_membership', 0),
(1851, 'steam:1100001068ef13c', 'weed', 0),
(1852, 'steam:1100001068ef13c', 'alive_chicken', 0),
(1853, 'steam:1100001068ef13c', 'taxi_license', 0),
(1854, 'steam:1100001068ef13c', 'water', 0),
(1855, 'steam:1100001068ef13c', 'medikit', 0),
(1856, 'steam:1100001068ef13c', 'blowpipe', 0),
(1857, 'steam:1100001068ef13c', 'bolpistache', 0),
(1858, 'steam:1100001068ef13c', 'sportlunch', 0),
(1859, 'steam:1100001068ef13c', 'saucisson', 0),
(1860, 'steam:1100001068ef13c', 'bandage', 0),
(1861, 'steam:1100001068ef13c', 'croquettes', 0),
(1862, 'steam:1100001068ef13c', 'limonade', 0),
(1863, 'steam:1100001068ef13c', 'lockpick', 0),
(1864, 'steam:1100001068ef13c', 'blackberry', 0),
(1865, 'steam:1100001068ef13c', 'fish', 0),
(1866, 'steam:1100001068ef13c', 'wood', 0),
(1867, 'steam:1100001068ef13c', 'tequila', 0),
(1868, 'steam:1100001068ef13c', 'golem', 0),
(1869, 'steam:1100001068ef13c', 'silencieux', 0),
(1870, 'steam:1100001068ef13c', 'cheesebows', 0),
(1871, 'steam:1100001068ef13c', 'pearl_pooch', 0),
(1872, 'steam:1100001068ef13c', 'boating_license', 0),
(1873, 'steam:1100001068ef13c', 'litter', 0),
(1874, 'steam:1100001068ef13c', 'drivers_license', 0),
(1875, 'steam:1100001068ef13c', 'firstaidpass', 0),
(1876, 'steam:1100001068ef13c', 'pilot_license', 0),
(1877, 'steam:1100001068ef13c', 'coke_pooch', 0),
(1878, 'steam:1100001068ef13c', 'ephedrine', 0),
(1879, 'steam:1100001068ef13c', 'fakepee', 0),
(1880, 'steam:1100001068ef13c', 'motorcycle_license', 0),
(1881, 'steam:1100001068ef13c', 'grapperaisin', 0),
(1882, 'steam:1100001068ef13c', 'slaughtered_chicken', 0),
(1883, 'steam:1100001068ef13c', 'fishing_license', 0),
(1884, 'steam:1100001068ef13c', 'coke', 0),
(1885, 'steam:1100001068ef13c', 'hunting_license', 0),
(1886, 'steam:1100001068ef13c', 'packaged_plank', 0),
(1887, 'steam:1100001068ef13c', 'cutted_wood', 0),
(1888, 'steam:1100001068ef13c', 'weapons_license3', 0),
(1889, 'steam:1100001068ef13c', 'diamond', 0),
(1890, 'steam:1100001068ef13c', 'pills', 0),
(1891, 'steam:1100001068ef13c', 'weapons_license2', 0),
(1892, 'steam:1100001068ef13c', 'lotteryticket', 0),
(1893, 'steam:1100001068ef13c', 'weapons_license1', 0),
(1894, 'steam:1100001068ef13c', 'meat', 0),
(1895, 'steam:1100001068ef13c', 'macka', 0),
(1896, 'steam:1100001068ef13c', 'pastacarbonara', 0),
(1897, 'steam:1100001068ef13c', 'breathalyzer', 0),
(1898, 'steam:1100001068ef13c', 'marabou', 0),
(1899, 'steam:1100001068ef13c', 'protein_shake', 0),
(1900, 'steam:1100001068ef13c', 'jagerbomb', 0),
(1901, 'steam:1100001068ef13c', 'chips', 0),
(1902, 'steam:1100001068ef13c', 'whool', 0),
(1903, 'steam:1100001068ef13c', 'rhumfruit', 0),
(1904, 'steam:1100001068ef13c', 'meth', 0),
(1905, 'steam:1100001068ef13c', 'menthe', 0),
(1906, 'steam:1100001068ef13c', 'drpepper', 0),
(1907, 'steam:1100001068ef13c', 'ice', 0),
(1908, 'steam:1100001068ef13c', 'ephedra', 0),
(1909, 'steam:1100001068ef13c', 'meth_pooch', 0),
(1910, 'steam:1100001068ef13c', 'litter_pooch', 0),
(1911, 'steam:1100001068ef13c', 'cocacola', 0),
(1912, 'steam:1100001068ef13c', 'painkiller', 0),
(1913, 'steam:1100001068ef13c', 'whisky', 0),
(1914, 'steam:1100001068ef13c', 'narcan', 0),
(1915, 'steam:1100001068ef13c', 'soda', 0),
(1916, 'steam:1100001068ef13c', 'lsd', 0),
(1917, 'steam:1100001068ef13c', 'gazbottle', 0),
(1918, 'steam:1100001068ef13c', 'vegetables', 0),
(1919, 'steam:1100001068ef13c', 'pcp', 0),
(1920, 'steam:1100001068ef13c', 'marriage_license', 0),
(1921, 'steam:1100001068ef13c', 'packaged_chicken', 0),
(1922, 'steam:1100001068ef13c', 'pizza', 0),
(1923, 'steam:1100001068ef13c', 'fixtool', 0),
(1924, 'steam:1100001068ef13c', 'bolcacahuetes', 0),
(1925, 'steam:1100001068ef13c', 'petrol', 0),
(1926, 'steam:1100001068ef13c', 'essence', 0),
(1927, 'steam:1100001068ef13c', 'whiskey', 0),
(1928, 'steam:1100001068ef13c', 'heroine', 0),
(1929, 'steam:1100001068ef13c', 'plongee2', 0),
(1930, 'steam:1100001068ef13c', 'gold', 0),
(1931, 'steam:1100001068ef13c', 'vodka', 0),
(1932, 'steam:1100001068ef13c', 'binoculars', 0),
(1933, 'steam:1100001068ef13c', 'energy', 0),
(1934, 'steam:1100001068ef13c', 'beer', 0),
(1935, 'steam:1100001068ef13c', 'jusfruit', 0),
(1936, 'steam:1100001068ef13c', 'leather', 0),
(1937, 'steam:1100001068ef13c', 'mixapero', 0),
(1938, 'steam:1100001068ef13c', 'vodkafruit', 0),
(1939, 'steam:1100001068ef13c', 'clip', 0),
(1940, 'steam:1100001068ef13c', 'flashlight', 0),
(1941, 'steam:1100001068ef13c', 'fixkit', 0),
(1942, 'steam:1100001068ef13c', 'teqpaf', 0),
(1943, 'steam:1100001068ef13c', 'fabric', 0),
(1944, 'steam:1100001068ef13c', 'whiskycoca', 0),
(1945, 'steam:1100001068ef13c', 'turtle', 0),
(1946, 'steam:1100001068ef13c', 'tacos', 0),
(1947, 'steam:11000013ca7f6bf', 'cocaine', 0),
(1948, 'steam:11000013ca7f6bf', 'whool', 0),
(1949, 'steam:11000013ca7f6bf', 'burger', 0),
(1950, 'steam:11000013ca7f6bf', 'iron', 0),
(1951, 'steam:11000013ca7f6bf', 'rhum', 0),
(1952, 'steam:11000013ca7f6bf', 'washed_stone', 0),
(1953, 'steam:11000013ca7f6bf', 'blowpipe', 0),
(1954, 'steam:11000013ca7f6bf', 'coke', 0),
(1955, 'steam:11000013ca7f6bf', 'teqpaf', 0),
(1956, 'steam:11000013ca7f6bf', 'carotool', 0),
(1957, 'steam:11000013ca7f6bf', 'ephedrine', 0),
(1958, 'steam:11000013ca7f6bf', 'metreshooter', 0),
(1959, 'steam:11000013ca7f6bf', 'pizza', 0),
(1960, 'steam:11000013ca7f6bf', 'mojito', 0),
(1961, 'steam:11000013ca7f6bf', 'armor', 0),
(1962, 'steam:11000013ca7f6bf', 'protein_shake', 0),
(1963, 'steam:11000013ca7f6bf', 'firstaidpass', 0),
(1964, 'steam:11000013ca7f6bf', 'coca', 0),
(1965, 'steam:11000013ca7f6bf', 'grapperaisin', 0),
(1966, 'steam:11000013ca7f6bf', 'packaged_plank', 0),
(1967, 'steam:11000013ca7f6bf', 'binoculars', 0),
(1968, 'steam:11000013ca7f6bf', 'leather', 0),
(1969, 'steam:11000013ca7f6bf', 'petrol_raffin', 0),
(1970, 'steam:11000013ca7f6bf', 'cannabis', 0),
(1971, 'steam:11000013ca7f6bf', 'macka', 0),
(1972, 'steam:11000013ca7f6bf', 'lsd', 0),
(1973, 'steam:11000013ca7f6bf', 'ice', 0),
(1974, 'steam:11000013ca7f6bf', 'diving_license', 0),
(1975, 'steam:11000013ca7f6bf', 'lighter', 0),
(1976, 'steam:11000013ca7f6bf', 'limonade', 0),
(1977, 'steam:11000013ca7f6bf', 'cutted_wood', 0),
(1978, 'steam:11000013ca7f6bf', 'sportlunch', 0),
(1979, 'steam:11000013ca7f6bf', 'tequila', 0),
(1980, 'steam:11000013ca7f6bf', 'gold', 0),
(1981, 'steam:11000013ca7f6bf', 'pills', 0),
(1982, 'steam:11000013ca7f6bf', 'pcp', 0),
(1983, 'steam:11000013ca7f6bf', 'lotteryticket', 0),
(1984, 'steam:11000013ca7f6bf', 'marriage_license', 0),
(1985, 'steam:11000013ca7f6bf', 'plongee1', 0),
(1986, 'steam:11000013ca7f6bf', 'bolpistache', 0),
(1987, 'steam:11000013ca7f6bf', 'grip', 0),
(1988, 'steam:11000013ca7f6bf', 'diamond', 0),
(1989, 'steam:11000013ca7f6bf', 'gym_membership', 0),
(1990, 'steam:11000013ca7f6bf', 'alive_chicken', 0),
(1991, 'steam:11000013ca7f6bf', 'beer', 0),
(1992, 'steam:11000013ca7f6bf', 'energy', 0),
(1993, 'steam:11000013ca7f6bf', 'vodkafruit', 0),
(1994, 'steam:11000013ca7f6bf', 'saucisson', 0),
(1995, 'steam:11000013ca7f6bf', 'gazbottle', 0),
(1996, 'steam:11000013ca7f6bf', 'fakepee', 0),
(1997, 'steam:11000013ca7f6bf', 'packaged_chicken', 0),
(1998, 'steam:11000013ca7f6bf', 'soda', 0),
(1999, 'steam:11000013ca7f6bf', 'fixkit', 0),
(2000, 'steam:11000013ca7f6bf', 'flashlight', 0),
(2001, 'steam:11000013ca7f6bf', 'meat', 0),
(2002, 'steam:11000013ca7f6bf', 'cigarett', 0),
(2003, 'steam:11000013ca7f6bf', 'weapons_license3', 0),
(2004, 'steam:11000013ca7f6bf', 'boitier', 0),
(2005, 'steam:11000013ca7f6bf', 'whiskycoca', 0),
(2006, 'steam:11000013ca7f6bf', 'lsd_pooch', 0),
(2007, 'steam:11000013ca7f6bf', 'rhumcoca', 0),
(2008, 'steam:11000013ca7f6bf', 'meth_pooch', 0),
(2009, 'steam:11000013ca7f6bf', 'water', 0),
(2010, 'steam:11000013ca7f6bf', 'litter', 0),
(2011, 'steam:11000013ca7f6bf', 'fanta', 0),
(2012, 'steam:11000013ca7f6bf', 'boating_license', 0),
(2013, 'steam:11000013ca7f6bf', 'fabric', 0),
(2014, 'steam:11000013ca7f6bf', 'drivers_license', 0),
(2015, 'steam:11000013ca7f6bf', 'loka', 0),
(2016, 'steam:11000013ca7f6bf', 'motorcycle_license', 0),
(2017, 'steam:11000013ca7f6bf', 'taxi_license', 0),
(2018, 'steam:11000013ca7f6bf', 'jusfruit', 0),
(2019, 'steam:11000013ca7f6bf', 'weapons_license2', 0),
(2020, 'steam:11000013ca7f6bf', 'fishing_license', 0),
(2021, 'steam:11000013ca7f6bf', 'pilot_license', 0),
(2022, 'steam:11000013ca7f6bf', 'pearl_pooch', 0),
(2023, 'steam:11000013ca7f6bf', 'jagerbomb', 0),
(2024, 'steam:11000013ca7f6bf', 'croquettes', 0),
(2025, 'steam:11000013ca7f6bf', 'weapons_license1', 0),
(2026, 'steam:11000013ca7f6bf', 'crack', 0),
(2027, 'steam:11000013ca7f6bf', 'jager', 0),
(2028, 'steam:11000013ca7f6bf', 'mixapero', 0),
(2029, 'steam:11000013ca7f6bf', 'vodkaenergy', 0),
(2030, 'steam:11000013ca7f6bf', 'silencieux', 0),
(2031, 'steam:11000013ca7f6bf', 'pastacarbonara', 0),
(2032, 'steam:11000013ca7f6bf', 'clip', 0),
(2033, 'steam:11000013ca7f6bf', 'baconburger', 0),
(2034, 'steam:11000013ca7f6bf', 'narcan', 0),
(2035, 'steam:11000013ca7f6bf', 'bread', 0),
(2036, 'steam:11000013ca7f6bf', 'slaughtered_chicken', 0),
(2037, 'steam:11000013ca7f6bf', 'marabou', 0),
(2038, 'steam:11000013ca7f6bf', 'copper', 0),
(2039, 'steam:11000013ca7f6bf', 'lockpick', 0),
(2040, 'steam:11000013ca7f6bf', 'vodka', 0),
(2041, 'steam:11000013ca7f6bf', 'bolcacahuetes', 0),
(2042, 'steam:11000013ca7f6bf', 'sprite', 0),
(2043, 'steam:11000013ca7f6bf', 'fish', 0),
(2044, 'steam:11000013ca7f6bf', 'cheesebows', 0),
(2045, 'steam:11000013ca7f6bf', 'litter_pooch', 0),
(2046, 'steam:11000013ca7f6bf', 'cocacola', 0),
(2047, 'steam:11000013ca7f6bf', 'painkiller', 0),
(2048, 'steam:11000013ca7f6bf', 'dabs', 0),
(2049, 'steam:11000013ca7f6bf', 'fixtool', 0),
(2050, 'steam:11000013ca7f6bf', 'whisky', 0),
(2051, 'steam:11000013ca7f6bf', 'bandage', 0),
(2052, 'steam:11000013ca7f6bf', 'powerade', 0),
(2053, 'steam:11000013ca7f6bf', 'rhumfruit', 0),
(2054, 'steam:11000013ca7f6bf', 'poppy', 0),
(2055, 'steam:11000013ca7f6bf', 'tacos', 0),
(2056, 'steam:11000013ca7f6bf', 'breathalyzer', 0),
(2057, 'steam:11000013ca7f6bf', 'opium_pooch', 0),
(2058, 'steam:11000013ca7f6bf', 'stone', 0),
(2059, 'steam:11000013ca7f6bf', 'drpepper', 0),
(2060, 'steam:11000013ca7f6bf', 'icetea', 0),
(2061, 'steam:11000013ca7f6bf', 'whiskey', 0),
(2062, 'steam:11000013ca7f6bf', 'heroine', 0),
(2063, 'steam:11000013ca7f6bf', 'bolnoixcajou', 0),
(2064, 'steam:11000013ca7f6bf', 'pearl', 0),
(2065, 'steam:11000013ca7f6bf', 'weed_pooch', 0),
(2066, 'steam:11000013ca7f6bf', 'ephedra', 0),
(2067, 'steam:11000013ca7f6bf', 'carokit', 0),
(2068, 'steam:11000013ca7f6bf', 'turtle', 0),
(2069, 'steam:11000013ca7f6bf', 'meth', 0),
(2070, 'steam:11000013ca7f6bf', 'coffee', 0),
(2071, 'steam:11000013ca7f6bf', 'marijuana', 0),
(2072, 'steam:11000013ca7f6bf', 'scratchoff_used', 0),
(2073, 'steam:11000013ca7f6bf', 'blackberry', 0),
(2074, 'steam:11000013ca7f6bf', 'clothe', 0),
(2075, 'steam:11000013ca7f6bf', 'weed', 0),
(2076, 'steam:11000013ca7f6bf', 'medikit', 0),
(2077, 'steam:11000013ca7f6bf', 'donut', 0),
(2078, 'steam:11000013ca7f6bf', 'coke_pooch', 0),
(2079, 'steam:11000013ca7f6bf', 'essence', 0),
(2080, 'steam:11000013ca7f6bf', 'plongee2', 0),
(2081, 'steam:11000013ca7f6bf', 'turtle_pooch', 0),
(2082, 'steam:11000013ca7f6bf', 'scratchoff', 0),
(2083, 'steam:11000013ca7f6bf', 'golem', 0),
(2084, 'steam:11000013ca7f6bf', 'bolchips', 0),
(2085, 'steam:11000013ca7f6bf', 'opium', 0),
(2086, 'steam:11000013ca7f6bf', 'martini', 0),
(2087, 'steam:11000013ca7f6bf', 'contrat', 0),
(2088, 'steam:11000013ca7f6bf', 'yusuf', 0),
(2089, 'steam:11000013ca7f6bf', 'commercial_license', 0),
(2090, 'steam:11000013ca7f6bf', 'petrol', 0),
(2091, 'steam:11000013ca7f6bf', 'menthe', 0),
(2092, 'steam:11000013ca7f6bf', 'vegetables', 0),
(2093, 'steam:11000013ca7f6bf', 'hunting_license', 0),
(2094, 'steam:11000013ca7f6bf', 'cola', 0),
(2095, 'steam:11000013ca7f6bf', 'drugtest', 0),
(2096, 'steam:11000013ca7f6bf', 'wood', 0),
(2097, 'steam:11000013ca7f6bf', 'chips', 0),
(2249, 'steam:110000132580eb0', 'teqpaf', 0),
(2250, 'steam:110000132580eb0', 'litter_pooch', 0),
(2251, 'steam:110000132580eb0', 'essence', 0),
(2252, 'steam:110000132580eb0', 'medikit', 0),
(2253, 'steam:110000132580eb0', 'cola', 0),
(2254, 'steam:110000132580eb0', 'armor', 0),
(2255, 'steam:110000132580eb0', 'scratchoff_used', 0),
(2256, 'steam:110000132580eb0', 'vodkaenergy', 0),
(2257, 'steam:110000132580eb0', 'beer', 0),
(2258, 'steam:110000132580eb0', 'gym_membership', 0),
(2259, 'steam:110000132580eb0', 'packaged_chicken', 0),
(2260, 'steam:110000132580eb0', 'drugtest', 0),
(2261, 'steam:110000132580eb0', 'diamond', 0),
(2262, 'steam:110000132580eb0', 'breathalyzer', 0),
(2263, 'steam:110000132580eb0', 'bandage', 0),
(2264, 'steam:110000132580eb0', 'poppy', 0),
(2265, 'steam:110000132580eb0', 'grip', 0),
(2266, 'steam:110000132580eb0', 'jusfruit', 0),
(2267, 'steam:110000132580eb0', 'gazbottle', 0),
(2268, 'steam:110000132580eb0', 'fixkit', 0),
(2269, 'steam:110000132580eb0', 'drivers_license', 0),
(2270, 'steam:110000132580eb0', 'whisky', 0),
(2271, 'steam:110000132580eb0', 'protein_shake', 0),
(2272, 'steam:110000132580eb0', 'painkiller', 0),
(2273, 'steam:110000132580eb0', 'bolnoixcajou', 0),
(2274, 'steam:110000132580eb0', 'fish', 0),
(2275, 'steam:110000132580eb0', 'contrat', 0),
(2276, 'steam:110000132580eb0', 'mixapero', 0),
(2277, 'steam:110000132580eb0', 'washed_stone', 0),
(2278, 'steam:110000132580eb0', 'weapons_license1', 0),
(2279, 'steam:110000132580eb0', 'loka', 0),
(2280, 'steam:110000132580eb0', 'turtle', 0),
(2281, 'steam:110000132580eb0', 'firstaidpass', 0),
(2282, 'steam:110000132580eb0', 'whiskycoca', 0),
(2283, 'steam:110000132580eb0', 'tacos', 0),
(2284, 'steam:110000132580eb0', 'vegetables', 0),
(2285, 'steam:110000132580eb0', 'rhum', 0),
(2286, 'steam:110000132580eb0', 'packaged_plank', 0),
(2287, 'steam:110000132580eb0', 'lighter', 0),
(2288, 'steam:110000132580eb0', 'pastacarbonara', 0),
(2289, 'steam:110000132580eb0', 'hunting_license', 0),
(2290, 'steam:110000132580eb0', 'carotool', 0),
(2291, 'steam:110000132580eb0', 'fabric', 0),
(2292, 'steam:110000132580eb0', 'dabs', 0),
(2293, 'steam:110000132580eb0', 'fanta', 0),
(2294, 'steam:110000132580eb0', 'leather', 0),
(2295, 'steam:110000132580eb0', 'sportlunch', 0),
(2296, 'steam:110000132580eb0', 'water', 0),
(2297, 'steam:110000132580eb0', 'gold', 0),
(2298, 'steam:110000132580eb0', 'coke', 0),
(2299, 'steam:110000132580eb0', 'pills', 0),
(2300, 'steam:110000132580eb0', 'stone', 0),
(2301, 'steam:110000132580eb0', 'jager', 0),
(2302, 'steam:110000132580eb0', 'lsd', 0),
(2303, 'steam:110000132580eb0', 'commercial_license', 0),
(2304, 'steam:110000132580eb0', 'mojito', 0),
(2305, 'steam:110000132580eb0', 'ephedrine', 0),
(2306, 'steam:110000132580eb0', 'menthe', 0),
(2307, 'steam:110000132580eb0', 'powerade', 0),
(2308, 'steam:110000132580eb0', 'tequila', 0),
(2309, 'steam:110000132580eb0', 'rhumfruit', 0),
(2310, 'steam:110000132580eb0', 'cocaine', 0),
(2311, 'steam:110000132580eb0', 'marijuana', 0),
(2312, 'steam:110000132580eb0', 'grapperaisin', 0),
(2313, 'steam:110000132580eb0', 'diving_license', 0),
(2314, 'steam:110000132580eb0', 'bolchips', 0),
(2315, 'steam:110000132580eb0', 'croquettes', 0),
(2316, 'steam:110000132580eb0', 'vodka', 0),
(2317, 'steam:110000132580eb0', 'martini', 0),
(2318, 'steam:110000132580eb0', 'meth_pooch', 0),
(2319, 'steam:110000132580eb0', 'pcp', 0),
(2320, 'steam:110000132580eb0', 'boating_license', 0),
(2321, 'steam:110000132580eb0', 'fakepee', 0),
(2322, 'steam:110000132580eb0', 'cheesebows', 0),
(2323, 'steam:110000132580eb0', 'alive_chicken', 0),
(2324, 'steam:110000132580eb0', 'petrol', 0),
(2325, 'steam:110000132580eb0', 'cannabis', 0),
(2326, 'steam:110000132580eb0', 'sprite', 0),
(2327, 'steam:110000132580eb0', 'meat', 0),
(2328, 'steam:110000132580eb0', 'motorcycle_license', 0),
(2329, 'steam:110000132580eb0', 'lsd_pooch', 0),
(2330, 'steam:110000132580eb0', 'pilot_license', 0),
(2331, 'steam:110000132580eb0', 'burger', 0),
(2332, 'steam:110000132580eb0', 'taxi_license', 0),
(2333, 'steam:110000132580eb0', 'plongee1', 0),
(2334, 'steam:110000132580eb0', 'ephedra', 0),
(2335, 'steam:110000132580eb0', 'weapons_license3', 0),
(2336, 'steam:110000132580eb0', 'fishing_license', 0),
(2337, 'steam:110000132580eb0', 'slaughtered_chicken', 0),
(2338, 'steam:110000132580eb0', 'blowpipe', 0),
(2339, 'steam:110000132580eb0', 'macka', 0),
(2340, 'steam:110000132580eb0', 'weapons_license2', 0),
(2341, 'steam:110000132580eb0', 'baconburger', 0),
(2342, 'steam:110000132580eb0', 'lotteryticket', 0),
(2343, 'steam:110000132580eb0', 'copper', 0),
(2344, 'steam:110000132580eb0', 'pizza', 0),
(2345, 'steam:110000132580eb0', 'coca', 0),
(2346, 'steam:110000132580eb0', 'chips', 0),
(2347, 'steam:110000132580eb0', 'marabou', 0),
(2348, 'steam:110000132580eb0', 'drpepper', 0),
(2349, 'steam:110000132580eb0', 'bolcacahuetes', 0),
(2350, 'steam:110000132580eb0', 'weed', 0),
(2351, 'steam:110000132580eb0', 'crack', 0),
(2352, 'steam:110000132580eb0', 'ice', 0),
(2353, 'steam:110000132580eb0', 'lockpick', 0),
(2354, 'steam:110000132580eb0', 'iron', 0),
(2355, 'steam:110000132580eb0', 'coffee', 0),
(2356, 'steam:110000132580eb0', 'coke_pooch', 0),
(2357, 'steam:110000132580eb0', 'boitier', 0),
(2358, 'steam:110000132580eb0', 'cigarett', 0),
(2359, 'steam:110000132580eb0', 'vodkafruit', 0),
(2360, 'steam:110000132580eb0', 'soda', 0),
(2361, 'steam:110000132580eb0', 'meth', 0),
(2362, 'steam:110000132580eb0', 'bread', 0),
(2363, 'steam:110000132580eb0', 'narcan', 0),
(2364, 'steam:110000132580eb0', 'fixtool', 0),
(2365, 'steam:110000132580eb0', 'heroine', 0),
(2366, 'steam:110000132580eb0', 'limonade', 0),
(2367, 'steam:110000132580eb0', 'golem', 0),
(2368, 'steam:110000132580eb0', 'silencieux', 0),
(2369, 'steam:110000132580eb0', 'cutted_wood', 0),
(2370, 'steam:110000132580eb0', 'opium_pooch', 0),
(2371, 'steam:110000132580eb0', 'plongee2', 0),
(2372, 'steam:110000132580eb0', 'whiskey', 0),
(2373, 'steam:110000132580eb0', 'flashlight', 0),
(2374, 'steam:110000132580eb0', 'icetea', 0),
(2375, 'steam:110000132580eb0', 'clothe', 0),
(2376, 'steam:110000132580eb0', 'blackberry', 0),
(2377, 'steam:110000132580eb0', 'opium', 0),
(2378, 'steam:110000132580eb0', 'jagerbomb', 0),
(2379, 'steam:110000132580eb0', 'turtle_pooch', 0),
(2380, 'steam:110000132580eb0', 'saucisson', 0),
(2381, 'steam:110000132580eb0', 'clip', 0),
(2382, 'steam:110000132580eb0', 'scratchoff', 0),
(2383, 'steam:110000132580eb0', 'bolpistache', 0),
(2384, 'steam:110000132580eb0', 'whool', 0),
(2385, 'steam:110000132580eb0', 'weed_pooch', 0),
(2386, 'steam:110000132580eb0', 'binoculars', 0),
(2387, 'steam:110000132580eb0', 'marriage_license', 0),
(2388, 'steam:110000132580eb0', 'yusuf', 0),
(2389, 'steam:110000132580eb0', 'carokit', 0),
(2390, 'steam:110000132580eb0', 'pearl_pooch', 0),
(2391, 'steam:110000132580eb0', 'pearl', 0),
(2392, 'steam:110000132580eb0', 'litter', 0),
(2393, 'steam:110000132580eb0', 'rhumcoca', 0),
(2394, 'steam:110000132580eb0', 'cocacola', 0),
(2395, 'steam:110000132580eb0', 'petrol_raffin', 0),
(2396, 'steam:110000132580eb0', 'metreshooter', 0),
(2397, 'steam:110000132580eb0', 'energy', 0),
(2398, 'steam:110000132580eb0', 'donut', 0),
(2399, 'steam:110000132580eb0', 'wood', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_lastcharacter`
--

INSERT INTO `user_lastcharacter` (`steamid`, `charid`) VALUES
('steam:11000010a01bdb9', 1),
('steam:110000132580eb0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_licenses`
--

INSERT INTO `user_licenses` (`id`, `type`, `owner`) VALUES
(1, 'dmv', 'steam:11000010a01bdb9'),
(2, 'drive', 'steam:11000010a01bdb9');

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(11) NOT NULL,
  `reported_by` varchar(80) DEFAULT NULL,
  `report_type` varchar(255) DEFAULT NULL,
  `report_comment` varchar(255) DEFAULT NULL,
  `report_admin` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_warnings`
--

CREATE TABLE `user_warnings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `warning` longtext,
  `time_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `inshop` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `inshop`) VALUES
('Adder', 'adder', 900000, 'super', 1),
('Akuma', 'AKUMA', 7500, 'motorcycles', 1),
('Alpha', 'alpha', 60000, 'sports', 1),
('Ardent', 'ardent', 1150000, 'sportsclassics', 1),
('Asea', 'asea', 5500, 'sedans', 1),
('Autarch', 'autarch', 1955000, 'super', 1),
('Avarus', 'avarus', 18000, 'motorcycles', 1),
('Bagger', 'bagger', 13500, 'motorcycles', 1),
('Baller', 'baller2', 40000, 'suvs', 1),
('Baller Sport', 'baller3', 60000, 'suvs', 1),
('Banshee', 'banshee', 70000, 'sports', 1),
('Banshee 900R', 'banshee2', 255000, 'importcars', 0),
('Bati 801', 'bati', 12000, 'motorcycles', 1),
('Bati 801RR', 'bati2', 19000, 'motorcycles', 1),
('Bestia GTS', 'bestiagts', 55000, 'sports', 1),
('BF400', 'bf400', 6500, 'motorcycles', 1),
('Bf Injection', 'bfinjection', 16000, 'offroad', 1),
('Bifta', 'bifta', 12000, 'offroad', 1),
('Bison', 'bison', 45000, 'vans', 1),
('Blade', 'blade', 15000, 'muscle', 1),
('Blazer', 'blazer', 6500, 'offroad', 1),
('Blazer Sport', 'blazer4', 8500, 'offroad', 1),
('blazer5', 'blazer5', 1755600, 'offroad', 1),
('Blista', 'blista', 8000, 'compacts', 1),
('BMX (velo)', 'bmx', 160, 'motorcycles', 1),
('Bobcat XL', 'bobcatxl', 32000, 'vans', 1),
('Brawler', 'brawler', 45000, 'offroad', 1),
('Brioso R/A', 'brioso', 18000, 'compacts', 1),
('Btype', 'btype', 62000, 'sportsclassics', 1),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 1),
('Btype Luxe', 'btype3', 85000, 'sportsclassics', 1),
('Buccaneer', 'buccaneer', 18000, 'muscle', 1),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 1),
('Buffalo', 'buffalo', 12000, 'sports', 1),
('Buffalo S', 'buffalo2', 20000, 'sports', 1),
('Bullet', 'bullet', 90000, 'super', 1),
('Burrito', 'burrito3', 19000, 'vans', 1),
('Camper', 'camper', 42000, 'vans', 1),
('Carbonizzare', 'carbonizzare', 75000, 'sports', 1),
('Carbon RS', 'carbonrs', 18000, 'motorcycles', 1),
('Casco', 'casco', 30000, 'sportsclassics', 1),
('Cavalcade', 'cavalcade2', 55000, 'suvs', 1),
('Cheetah', 'cheetah', 375000, 'super', 1),
('Chimera', 'chimera', 38000, 'motorcycles', 1),
('Chino', 'chino', 15000, 'muscle', 1),
('Chino Luxe', 'chino2', 19000, 'muscle', 1),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 1),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 1),
('Cognoscenti', 'cognoscenti', 55000, 'sedans', 1),
('Comet', 'comet2', 65000, 'sports', 1),
('Comet 5', 'comet5', 1145000, 'sports', 1),
('Contender', 'contender', 70000, 'suvs', 1),
('Coquette', 'coquette', 65000, 'sports', 1),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 1),
('Coquette BlackFin', 'coquette3', 55000, 'muscle', 1),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles', 1),
('Cyclone', 'cyclone', 1890000, 'super', 1),
('Daemon', 'daemon', 11500, 'motorcycles', 1),
('Daemon High', 'daemon2', 13500, 'motorcycles', 1),
('Defiler', 'defiler', 9800, 'motorcycles', 1),
('Deluxo', 'deluxo', 4721500, 'sportsclassics', 1),
('Dominator', 'dominator', 35000, 'muscle', 1),
('Double T', 'double', 28000, 'motorcycles', 1),
('Dubsta', 'dubsta', 45000, 'suvs', 1),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 1),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 1),
('Dukes', 'dukes', 28000, 'muscle', 1),
('Dune Buggy', 'dune', 8000, 'offroad', 1),
('Elegy', 'elegy2', 38500, 'sports', 1),
('Emperor', 'emperor', 8500, 'sedans', 1),
('Enduro', 'enduro', 5500, 'motorcycles', 1),
('Entity XF', 'entityxf', 425000, 'importcars', 0),
('Esskey', 'esskey', 4200, 'motorcycles', 1),
('Exemplar', 'exemplar', 32000, 'coupes', 1),
('F620', 'f620', 40000, 'coupes', 1),
('Faction', 'faction', 20000, 'muscle', 1),
('Faction Rider', 'faction2', 30000, 'muscle', 1),
('Faction XL', 'faction3', 40000, 'muscle', 1),
('Faggio', 'faggio', 1900, 'motorcycles', 1),
('Vespa', 'faggio2', 2800, 'motorcycles', 1),
('Felon', 'felon', 42000, 'coupes', 1),
('Felon GT', 'felon2', 55000, 'coupes', 1),
('Feltzer', 'feltzer2', 55000, 'sports', 1),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 1),
('Fixter (velo)', 'fixter', 225, 'motorcycles', 1),
('Flat Bed', 'flatbed', 15000, 'utility', 1),
('FMJ', 'fmj', 185000, 'super', 1),
('Road King', 'foxharley1', 23000, 'importbikes', 0),
('Road Glide', 'foxharley2', 30000, 'importbikes', 0),
('Fhantom', 'fq2', 17000, 'suvs', 1),
('Fugitive', 'fugitive', 12000, 'sedans', 1),
('Furore GT', 'furoregt', 45000, 'sports', 1),
('Fusilade', 'fusilade', 40000, 'sports', 1),
('Gargoyle', 'gargoyle', 16500, 'motorcycles', 1),
('Gauntlet', 'gauntlet', 30000, 'muscle', 1),
('Gang Burrito', 'gburrito', 45000, 'vans', 1),
('Burrito', 'gburrito2', 29000, 'vans', 1),
('Glendale', 'glendale', 6500, 'sedans', 1),
('Grabger', 'granger', 50000, 'suvs', 1),
('Gresley', 'gresley', 47500, 'suvs', 1),
('GT 500', 'gt500', 785000, 'sportsclassics', 1),
('Guardian', 'guardian', 45000, 'offroad', 1),
('Hakuchou', 'hakuchou', 31000, 'motorcycles', 1),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 1),
('Hermes', 'hermes', 535000, 'muscle', 1),
('Hexer', 'hexer', 12000, 'motorcycles', 1),
('Hotknife', 'hotknife', 125000, 'muscle', 1),
('Huntley S', 'huntley', 40000, 'suvs', 1),
('Hustler', 'hustler', 625000, 'muscle', 1),
('Infernus', 'infernus', 180000, 'super', 1),
('Innovation', 'innovation', 23500, 'motorcycles', 1),
('Intruder', 'intruder', 7500, 'sedans', 1),
('Issi', 'issi2', 10000, 'compacts', 1),
('Jackal', 'jackal', 38000, 'coupes', 1),
('Jester', 'jester', 65000, 'sports', 1),
('Jester(Racecar)', 'jester2', 135000, 'sports', 1),
('Journey', 'journey', 6500, 'vans', 1),
('Kamacho', 'kamacho', 345000, 'offroad', 1),
('Khamelion', 'khamelion', 38000, 'sports', 1),
('Kuruma', 'kuruma', 30000, 'sports', 1),
('Landstalker', 'landstalker', 35000, 'suvs', 1),
('RE-7B', 'le7b', 325000, 'super', 1),
('Lynx', 'lynx', 40000, 'sports', 1),
('Mamba', 'mamba', 70000, 'sports', 1),
('Manana', 'manana', 12800, 'sportsclassics', 1),
('Manchez', 'manchez', 5300, 'motorcycles', 1),
('Massacro', 'massacro', 65000, 'sports', 1),
('Massacro(Racecar)', 'massacro2', 130000, 'sports', 1),
('Mesa', 'mesa', 16000, 'suvs', 1),
('Mesa Trail', 'mesa3', 40000, 'suvs', 1),
('Minivan', 'minivan', 13000, 'vans', 1),
('Monroe', 'monroe', 55000, 'sportsclassics', 1),
('The Liberator', 'monster', 210000, 'offroad', 1),
('Moonbeam', 'moonbeam', 18000, 'vans', 1),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 1),
('Nemesis', 'nemesis', 5800, 'motorcycles', 1),
('Neon', 'neon', 1500000, 'sports', 1),
('Nightblade', 'nightblade', 35000, 'importbikes', 0),
('Nightshade', 'nightshade', 65000, 'muscle', 1),
('9F', 'ninef', 65000, 'sports', 1),
('9F Cabrio', 'ninef2', 80000, 'sports', 1),
('Omnis', 'omnis', 35000, 'sports', 1),
('Oppressor', 'oppressor', 3524500, 'super', 1),
('Oracle XS', 'oracle2', 35000, 'coupes', 1),
('Osiris', 'osiris', 160000, 'super', 1),
('Panto', 'panto', 10000, 'compacts', 1),
('Paradise', 'paradise', 19000, 'vans', 1),
('Pariah', 'pariah', 1420000, 'sports', 1),
('Patriot', 'patriot', 55000, 'suvs', 1),
('PCJ-600', 'pcj', 6200, 'motorcycles', 1),
('Penumbra', 'penumbra', 28000, 'sports', 1),
('Pfister', 'pfister811', 85000, 'super', 1),
('Phoenix', 'phoenix', 12500, 'muscle', 1),
('Picador', 'picador', 18000, 'muscle', 1),
('Pigalle', 'pigalle', 20000, 'sportsclassics', 1),
('Prairie', 'prairie', 12000, 'compacts', 1),
('Premier', 'premier', 8000, 'sedans', 1),
('Primo Custom', 'primo2', 14000, 'sedans', 1),
('X80 Proto', 'prototipo', 2500000, 'super', 1),
('Radius', 'radi', 29000, 'suvs', 1),
('raiden', 'raiden', 1375000, 'sports', 1),
('Rapid GT', 'rapidgt', 35000, 'sports', 1),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 1),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 1),
('Reaper', 'reaper', 150000, 'importcars', 0),
('Rebel', 'rebel2', 35000, 'offroad', 1),
('Regina', 'regina', 5000, 'sedans', 1),
('Retinue', 'retinue', 615000, 'sportsclassics', 1),
('Revolter', 'revolter', 1610000, 'sports', 1),
('riata', 'riata', 380000, 'offroad', 1),
('Rocoto', 'rocoto', 45000, 'suvs', 1),
('Ruffian', 'ruffian', 6800, 'motorcycles', 1),
('Ruiner 2', 'ruiner2', 5745600, 'muscle', 1),
('Rumpo', 'rumpo', 15000, 'vans', 1),
('Rumpo Trail', 'rumpo3', 19500, 'vans', 1),
('Sabre Turbo', 'sabregt', 20000, 'muscle', 1),
('Sabre GT', 'sabregt2', 25000, 'muscle', 1),
('Sanchez', 'sanchez', 5300, 'motorcycles', 1),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 1),
('Sanctus', 'sanctus', 25000, 'motorcycles', 1),
('Sandking', 'sandking', 55000, 'offroad', 1),
('Savestra', 'savestra', 990000, 'sportsclassics', 1),
('SC 1', 'sc1', 1603000, 'super', 1),
('Schafter', 'schafter2', 25000, 'sedans', 1),
('Schafter V12', 'schafter3', 50000, 'sports', 1),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles', 1),
('Seminole', 'seminole', 25000, 'suvs', 1),
('Sentinel', 'sentinel', 32000, 'coupes', 1),
('Sentinel XS', 'sentinel2', 40000, 'coupes', 1),
('Sentinel3', 'sentinel3', 650000, 'sports', 1),
('Seven 70', 'seven70', 39500, 'sports', 1),
('ETR1', 'sheava', 220000, 'importcars', 0),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 1),
('Slam Van', 'slamvan3', 11500, 'muscle', 1),
('Sovereign', 'sovereign', 22000, 'motorcycles', 1),
('Stinger', 'stinger', 80000, 'sportsclassics', 1),
('Stinger GT', 'stingergt', 75000, 'sportsclassics', 1),
('Streiter', 'streiter', 500000, 'sports', 1),
('Stretch', 'stretch', 90000, 'sedans', 1),
('Stromberg', 'stromberg', 3185350, 'sports', 1),
('Sultan', 'sultan', 15000, 'sports', 1),
('Sultan RS', 'sultanrs', 65000, 'importcars', 0),
('Super Diamond', 'superd', 130000, 'sedans', 1),
('Surano', 'surano', 50000, 'sports', 1),
('Surfer', 'surfer', 12000, 'vans', 1),
('T20', 't20', 300000, 'super', 1),
('Tailgater', 'tailgater', 30000, 'sedans', 1),
('Tampa', 'tampa', 16000, 'muscle', 1),
('Drift Tampa', 'tampa2', 80000, 'sports', 1),
('Thrust', 'thrust', 24000, 'motorcycles', 1),
('Tow Truck', 'towtruck', 15000, 'utility', 1),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles', 1),
('Trophy Truck', 'trophytruck', 60000, 'offroad', 1),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 1),
('Tropos', 'tropos', 40000, 'sports', 1),
('Turismo R', 'turismor', 350000, 'super', 1),
('Tyrus', 'tyrus', 600000, 'super', 1),
('Vacca', 'vacca', 120000, 'super', 1),
('Vader', 'vader', 7200, 'motorcycles', 1),
('Verlierer', 'verlierer2', 70000, 'sports', 1),
('Vigero', 'vigero', 12500, 'muscle', 1),
('Virgo', 'virgo', 14000, 'muscle', 1),
('Viseris', 'viseris', 875000, 'sportsclassics', 1),
('Visione', 'visione', 2250000, 'super', 1),
('Voltic', 'voltic', 90000, 'super', 1),
('Voltic 2', 'voltic2', 3830400, 'super', 1),
('Voodoo', 'voodoo', 7200, 'muscle', 1),
('Vortex', 'vortex', 9800, 'motorcycles', 1),
('Warrener', 'warrener', 4000, 'sedans', 1),
('Washington', 'washington', 9000, 'sedans', 1),
('Windsor', 'windsor', 95000, 'coupes', 1),
('Windsor Drop', 'windsor2', 125000, 'importcars', 0),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 1),
('XLS', 'xls', 32000, 'suvs', 1),
('Yosemite', 'yosemite', 485000, 'muscle', 1),
('Youga', 'youga', 10800, 'vans', 1),
('Youga Luxuary', 'youga2', 14500, 'vans', 1),
('Z190', 'z190', 900000, 'sportsclassics', 1),
('Zentorno', 'zentorno', 1500000, 'super', 1),
('Zion', 'zion', 36000, 'coupes', 1),
('Zion Cabrio', 'zion2', 45000, 'coupes', 1),
('Zombie', 'zombiea', 9500, 'motorcycles', 1),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 1),
('Z-Type', 'ztype', 220000, 'sportsclassics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_display`
--

CREATE TABLE `vehicles_display` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT '10',
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles_display`
--

INSERT INTO `vehicles_display` (`ID`, `name`, `model`, `profit`, `price`) VALUES
(1, 'Windsor Drop', 'windsor2', 10, 125000),
(2, 'Reaper', 'reaper', 10, 150000),
(3, 'Sultan RS', 'sultanrs', 10, 65000),
(4, 'Road Glide', 'foxharley2', 10, 30000),
(5, 'Road King', 'foxharley1', 10, 23000),
(6, 'Nightblade', 'nightblade', 10, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('importbikes', 'Import Bikes'),
('importcars', 'Import Cars'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('utility', 'Utility'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `fullname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `crimes` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 3500),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 45000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 25000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 85000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 95000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(25, 'GunShop', 'WEAPON_BALL', 50),
(26, 'BlackWeashop', 'WEAPON_BALL', 50),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(28, 'GunShop', 'WEAPON_PISTOL50', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commend`
--
ALTER TABLE `commend`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `gsr`
--
ALTER TABLE `gsr`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kicks`
--
ALTER TABLE `kicks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `index_owned_vehicles_owner` (`owner`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `license` (`license`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_bans`
--
ALTER TABLE `received_bans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `server_actions`
--
ALTER TABLE `server_actions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin_notes`
--
ALTER TABLE `user_admin_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_warnings`
--
ALTER TABLE `user_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commend`
--
ALTER TABLE `commend`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2074;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=338;

--
-- AUTO_INCREMENT for table `kicks`
--
ALTER TABLE `kicks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;

--
-- AUTO_INCREMENT for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `received_bans`
--
ALTER TABLE `received_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_actions`
--
ALTER TABLE `server_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2400;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_warnings`
--
ALTER TABLE `user_warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
--
-- Database: `essentialmode`
--
CREATE DATABASE IF NOT EXISTS `essentialmode` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `essentialmode`;

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1),
(33, 'society_admin', 'admin', 1),
(34, 'society_biker', 'Biker', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_ambulance', 0, NULL),
(2, 'society_police', 3000, NULL),
(3, 'society_mecano', 0, NULL),
(4, 'society_taxi', 112, NULL),
(5, 'society_fire', 0, NULL),
(6, 'society_airlines', 0, NULL),
(7, 'society_mafia', 0, NULL),
(8, 'society_rebel', 0, NULL),
(9, 'society_unicorn', 0, NULL),
(10, 'society_dock', 0, NULL),
(11, 'society_avocat', 0, NULL),
(12, 'society_irish', 0, NULL),
(13, 'society_rodriguez', 0, NULL),
(14, 'society_bishops', 0, NULL),
(15, 'society_bountyhunter', 0, NULL),
(16, 'society_dismay', 0, NULL),
(17, 'society_grove', 0, NULL),
(18, 'society_foodtruck', 0, NULL),
(19, 'society_vagos', 0, NULL),
(20, 'society_ballas', 0, NULL),
(21, 'society_carthief', 0, NULL),
(22, 'society_realestateagent', 0, NULL),
(23, 'property_black_money', 0, 'steam:110000132580eb0'),
(24, 'caution', 0, 'steam:110000132580eb0'),
(25, 'property_black_money', 0, 'steam:110000100023daf'),
(26, 'caution', 0, 'steam:110000100023daf'),
(27, 'caution', 0, 'steam:11000010a01bdb9'),
(28, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(29, 'property_black_money', 0, 'steam:110000112969e8f'),
(30, 'caution', 0, 'steam:110000112969e8f'),
(31, 'caution', 0, 'steam:1100001068ef13c'),
(32, 'property_black_money', 0, 'steam:1100001068ef13c'),
(33, 'caution', 0, 'steam:1100001138168a0'),
(34, 'property_black_money', 0, 'steam:1100001138168a0'),
(35, 'property_black_money', 0, 'steam:1100001320dfb72'),
(36, 'caution', 0, 'steam:1100001320dfb72'),
(37, 'caution', 0, 'steam:1100001159dff06'),
(38, 'property_black_money', 0, 'steam:1100001159dff06'),
(39, 'property_black_money', 0, 'steam:11000010dc84b6d'),
(40, 'caution', 0, 'steam:11000010dc84b6d'),
(41, 'society_admin', 0, NULL),
(42, 'caution', 0, 'steam:11000010c87fe96'),
(43, 'property_black_money', 0, 'steam:11000010c87fe96'),
(44, 'caution', 0, 'steam:1100001372437de'),
(45, 'property_black_money', 0, 'steam:1100001372437de'),
(46, 'property_black_money', 0, 'steam:11000010b15a7d4'),
(47, 'caution', 0, 'steam:11000010b15a7d4'),
(48, 'society_biker', 0, NULL),
(49, 'property_black_money', 0, 'steam:110000136d9eea0'),
(50, 'caution', 0, 'steam:110000136d9eea0'),
(51, 'caution', 0, 'steam:110000133989c6d'),
(52, 'property_black_money', 0, 'steam:110000133989c6d'),
(53, 'caution', 0, 'steam:11000013ca51b76'),
(54, 'property_black_money', 0, 'steam:11000013ca51b76'),
(55, 'caution', 0, 'steam:11000010bf29a8c'),
(56, 'property_black_money', 0, 'steam:11000010bf29a8c');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_mecano', 'Mechanic', 1),
(3, 'society_taxi', 'Taxi', 1),
(5, 'property', 'Property', 0),
(6, 'society_fire', 'fire', 1),
(7, 'society_airlines', 'Airlines', 1),
(8, 'society_mafia', 'Mafia', 1),
(9, 'society_citizen', 'Mafia', 1),
(10, 'society_rebel', 'Rebel', 1),
(11, 'society_unicorn', 'Unicorn', 1),
(12, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(13, 'society_unicorn', 'Unicorn', 1),
(14, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(15, 'society_dock', 'Marina', 1),
(16, 'society_avocat', 'Avocat', 1),
(17, 'society_irish', 'Irish', 1),
(18, 'society_rodriguez', 'Rodriguez', 1),
(19, 'society_avocat', 'Avocat', 1),
(20, 'society_bishops', 'Bishops', 1),
(21, 'society_irish', 'Irish', 1),
(22, 'society_bountyhunter', 'Bountyhunter', 1),
(23, 'society_dismay', 'Dismay', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_grove', 'Grove', 1),
(26, 'society_vagos', 'Vagos', 1),
(27, 'society_ballas', 'Ballas', 1),
(28, 'society_carthief', 'Car Thief', 1),
(29, 'society_admin', 'admin', 1),
(30, 'society_biker', 'Biker', 1),
(31, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `ban_issued` varchar(50) NOT NULL,
  `banned_until` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `Column 9` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(1, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Class 2 Weapon trafficking', 7500),
(2, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(3, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(4, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(5, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(6, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(7, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(8, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(9, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(10, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(11, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(12, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(13, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(14, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(15, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(16, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(17, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(18, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(19, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(20, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(21, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(22, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(23, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(24, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(25, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000),
(26, 'steam:1100001372437de', 'steam:11000010a01bdb9', 'society', 'society_police', 'Fine: Murder of an LEO', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `admin_rank` int(11) DEFAULT '-1',
  `biker_rank` int(11) DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `admin_rank`, `biker_rank`) VALUES
('steam:11000010a01bdb9', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', -1, -1, -1, 1, -1),
('steam:110000132580eb0', 'Jak', 'Fulton', '10/10/1988', 'M', '74', -1, -1, -1, -1, -1),
('steam:1100001372437de', 'Shawn', 'Vargas', '010/9/1990', 'M', '60', -1, -1, -1, -1, -1),
('steam:1100001068ef13c', 'William', 'Woodard', '7/27/1979', '', '', -1, -1, -1, -1, -1),
('steam:110000112969e8f', 'Steven', 'Super', '05/08/03', 'M', '86', -1, -1, -1, -1, -1),
('steam:11000010b15a7d4', 'Bob', 'Saget', '06/07/1200', 'M', '90', -1, -1, -1, -1, -1),
('steam:110000136d9eea0', 'Billy', 'Hamilton', '03/14/1990', 'm', '76', -1, -1, -1, -1, -1),
('steam:110000133989c6d', 'Lance', 'Vance', '02/25/1992', 'M', '69', -1, -1, -1, -1, -1),
('steam:11000010bf29a8c', 'Chayne', 'Fisher', '02251996', 'm', '96', -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commend`
--

CREATE TABLE `commend` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `community_name` varchar(50) NOT NULL,
  `discord_webhook` varchar(50) NOT NULL,
  `joinmessage` enum('T','F') NOT NULL,
  `chatcommands` enum('T','F') NOT NULL,
  `checktimeout` int(11) NOT NULL,
  `trustscore` int(11) NOT NULL,
  `tswarn` int(11) NOT NULL,
  `tskick` int(11) NOT NULL,
  `tsban` int(11) NOT NULL,
  `tscommend` int(11) NOT NULL,
  `tstime` int(11) NOT NULL,
  `recent_time` int(11) NOT NULL,
  `permissions` varchar(50) NOT NULL,
  `serveractions` varchar(50) NOT NULL,
  `debug` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'user_mask', 'Masque', 0),
(3, 'property', 'Property', 0),
(4, 'society_fire', 'fire', 1),
(5, 'society_mafia', 'Mafia', 1),
(7, 'society_rebel', 'Rebel', 1),
(8, 'society_unicorn', 'Unicorn', 1),
(9, 'society_unicorn', 'Unicorn', 1),
(10, 'society_avocat', 'Avocat', 1),
(11, 'society_irish', 'Irish', 1),
(12, 'society_rodriguez', 'Rodriguez', 1),
(13, 'society_avocat', 'Avocat', 1),
(14, 'society_bishops', 'Bishops', 1),
(15, 'society_irish', 'Irish', 1),
(16, 'society_bountyhunter', 'Bountyhunter', 1),
(17, 'society_dismay', 'Dismay', 1),
(18, 'society_bountyhunter', 'Bountyhunter', 1),
(19, 'society_grove', 'Grove', 1),
(20, 'society_vagos', 'Vagos', 1),
(21, 'society_ballas', 'Ballas', 1),
(22, 'society_carthief', 'Car Thief', 1),
(23, 'society_admin', 'admin', 1),
(24, 'society_biker', 'Biker', 1),
(25, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_police', NULL, '{}'),
(2, 'society_fire', NULL, '{}'),
(3, 'society_mafia', NULL, '{}'),
(4, 'society_rebel', NULL, '{}'),
(5, 'society_unicorn', NULL, '{}'),
(6, 'society_avocat', NULL, '{}'),
(7, 'society_irish', NULL, '{}'),
(8, 'society_rodriguez', NULL, '{}'),
(9, 'society_bishops', NULL, '{}'),
(10, 'society_bountyhunter', NULL, '{}'),
(11, 'society_dismay', NULL, '{}'),
(12, 'society_grove', NULL, '{}'),
(13, 'society_vagos', NULL, '{}'),
(14, 'society_ballas', NULL, '{}'),
(15, 'society_carthief', NULL, '{}'),
(16, 'user_mask', 'steam:110000132580eb0', '{}'),
(17, 'property', 'steam:110000132580eb0', '{}'),
(18, 'property', 'steam:110000100023daf', '{}'),
(19, 'user_mask', 'steam:110000100023daf', '{}'),
(20, 'property', 'steam:11000010a01bdb9', '{}'),
(21, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(22, 'user_mask', 'steam:110000112969e8f', '{}'),
(23, 'property', 'steam:110000112969e8f', '{}'),
(24, 'user_mask', 'steam:1100001068ef13c', '{}'),
(25, 'property', 'steam:1100001068ef13c', '{}'),
(26, 'property', 'steam:1100001138168a0', '{}'),
(27, 'user_mask', 'steam:1100001138168a0', '{}'),
(28, 'property', 'steam:1100001320dfb72', '{}'),
(29, 'user_mask', 'steam:1100001320dfb72', '{}'),
(30, 'user_mask', 'steam:1100001159dff06', '{}'),
(31, 'property', 'steam:1100001159dff06', '{}'),
(32, 'property', 'steam:11000010dc84b6d', '{}'),
(33, 'user_mask', 'steam:11000010dc84b6d', '{}'),
(34, 'society_admin', NULL, '{}'),
(35, 'property', 'steam:11000010c87fe96', '{}'),
(36, 'user_mask', 'steam:11000010c87fe96', '{}'),
(37, 'user_mask', 'steam:1100001372437de', '{}'),
(38, 'property', 'steam:1100001372437de', '{}'),
(39, 'property', 'steam:11000010b15a7d4', '{}'),
(40, 'user_mask', 'steam:11000010b15a7d4', '{}'),
(41, 'society_biker', NULL, '{}'),
(42, 'society_ambulance', NULL, '{}'),
(43, 'user_mask', 'steam:110000136d9eea0', '{}'),
(44, 'property', 'steam:110000136d9eea0', '{}'),
(45, 'property', 'steam:110000133989c6d', '{}'),
(46, 'user_mask', 'steam:110000133989c6d', '{}'),
(47, 'user_mask', 'steam:11000013ca51b76', '{}'),
(48, 'property', 'steam:11000013ca51b76', '{}'),
(49, 'property', 'steam:11000010bf29a8c', '{}'),
(50, 'user_mask', 'steam:11000010bf29a8c', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Toro', 'toro', 2000, 'dock'),
(3, 'Dinghy3', 'dinghy3', 2000, 'dock'),
(4, 'Seashark', 'seashark', 1000, 'dock'),
(5, 'Submarine', 'submersible2', 4000, 'dock');

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ballas`
--

CREATE TABLE `fine_types_ballas` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_ballas`
--

INSERT INTO `fine_types_ballas` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bishops`
--

CREATE TABLE `fine_types_bishops` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bishops`
--

INSERT INTO `fine_types_bishops` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_dismay`
--

CREATE TABLE `fine_types_dismay` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_dismay`
--

INSERT INTO `fine_types_dismay` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_grove`
--

CREATE TABLE `fine_types_grove` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_grove`
--

INSERT INTO `fine_types_grove` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_irish`
--

CREATE TABLE `fine_types_irish` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_irish`
--

INSERT INTO `fine_types_irish` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_mafia`
--

CREATE TABLE `fine_types_mafia` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_mafia`
--

INSERT INTO `fine_types_mafia` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rebel`
--

CREATE TABLE `fine_types_rebel` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rebel`
--

INSERT INTO `fine_types_rebel` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rodriguez`
--

CREATE TABLE `fine_types_rodriguez` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rodriguez`
--

INSERT INTO `fine_types_rodriguez` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_vagos`
--

CREATE TABLE `fine_types_vagos` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_vagos`
--

INSERT INTO `fine_types_vagos` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'bread', 'Bread', -1, 0, 1),
(2, 'water', 'Water', -1, 0, 1),
(3, 'weed', 'Weed', 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1),
(14, 'fish', 'Fish', -1, 0, 1),
(15, 'stone', 'Stone', -1, 0, 1),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1),
(17, 'copper', 'Copper', -1, 0, 1),
(18, 'iron', 'Iron', -1, 0, 1),
(19, 'gold', 'Gold', -1, 0, 1),
(20, 'diamond', 'Diamond', -1, 0, 1),
(21, 'wood', 'Wood', -1, 0, 1),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1),
(24, 'petrol', 'Gas', -1, 0, 1),
(25, 'petrol_raffin', 'Refined Oil', -1, 0, 1),
(26, 'essence', 'Essence', -1, 0, 1),
(27, 'whool', 'Whool', -1, 0, 1),
(28, 'fabric', 'Fabric', -1, 0, 1),
(29, 'clothe', 'Clothe', -1, 0, 1),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1),
(31, 'fixtool', 'Repair Tools', -1, 0, 1),
(32, 'carotool', 'Tools', -1, 0, 1),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1),
(34, 'fixkit', 'Repair Kit', -1, 0, 1),
(35, 'carokit', 'Body Kit', -1, 0, 1),
(36, 'beer', 'Beer', -1, 0, 1),
(37, 'bandage', 'Bandage', 20, 0, 1),
(38, 'medikit', 'Medikit', 100, 0, 1),
(39, 'pills', 'Pills', 10, 0, 1),
(40, 'lockpick', 'Lockpick', -1, 0, 1),
(41, 'vodka', 'Vodka', -1, 0, 1),
(42, 'coffee', 'Coffee', -1, 0, 1),
(49, 'clip', 'Chargeur', -1, 0, 1),
(50, 'jager', 'Jägermeister', 5, 0, 1),
(51, 'vodka', 'Vodka', 5, 0, 1),
(52, 'rhum', 'Rhum', 5, 0, 1),
(53, 'whisky', 'Whisky', 5, 0, 1),
(54, 'tequila', 'Tequila', 5, 0, 1),
(55, 'martini', 'Martini blanc', 5, 0, 1),
(56, 'soda', 'Soda', 5, 0, 1),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1),
(58, 'icetea', 'Ice Tea', 5, 0, 1),
(59, 'energy', 'Energy Drink', 5, 0, 1),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(61, 'limonade', 'Limonade', 5, 0, 1),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1),
(63, 'bolnoixcajou', 'Bowl of Cachews', 5, 0, 1),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1),
(66, 'saucisson', 'Sausage', 5, 0, 1),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(69, 'golem', 'Golem', 5, 0, 1),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(76, 'mojito', 'Mojito', 5, 0, 1),
(77, 'ice', 'Glaçon', 5, 0, 1),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1),
(81, 'menthe', 'Mint leaf', 10, 0, 1),
(82, 'cola', 'Coke', -1, 0, 1),
(117, 'turtle', 'Turtle', -1, 0, 1),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1),
(119, 'lsd', 'Lsd', -1, 0, 1),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(121, 'pearl', 'Pearl', -1, 0, 1),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1),
(123, 'litter', 'Litter', -1, 0, 1),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1),
(125, 'nitro', 'Nitroso', -1, 0, 1),
(127, 'vegetables', 'Vegetables', 20, 0, 1),
(128, 'meat', 'Meat', 20, 0, 1),
(129, 'tacos', 'Tacos', 20, 0, 1),
(130, 'burger', 'Burger', 20, 0, 1),
(131, 'silencieux', 'Siliencer', -1, 0, 1),
(132, 'flashlight', 'Flashlight', -1, 0, 1),
(133, 'grip', 'Grip', -1, 0, 1),
(134, 'yusuf', 'Skin', -1, 0, 1),
(135, 'binoculars', 'Binoculars', 1, 0, 1),
(136, 'croquettes', 'Croquettes', -1, 0, 1),
(137, 'blackberry', 'blackberry', -1, 0, 1),
(138, 'lighter', 'Bic', -1, 0, 1),
(139, 'cigarett', 'Cigarette', -1, 0, 1),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1),
(2010, 'armor', 'Armor', -1, 0, 1),
(2011, 'contrat', '📃 Facture', 100, 0, 1),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1),
(2013, 'powerade', 'Powerade', -1, 0, 1),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1),
(2016, 'plongee1', 'Short Dive', -1, 0, 1),
(2017, 'plongee2', 'Long Dive', -1, 0, 1),
(2018, 'contrat', 'Salvage', 15, 0, 1),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1),
(2021, 'meat', 'Meat', -1, 0, 1),
(2022, 'leather', 'Leather', -1, 0, 1),
(2023, 'cannabis', 'Cannabis', 50, 0, 1),
(2024, 'marijuana', 'Marijuana', 250, 0, 1),
(2025, 'coca', 'CocaPlant', 150, 0, 1),
(2026, 'cocaine', 'Coke', 50, 0, 1),
(2027, 'ephedra', 'Ephedra', 100, 0, 1),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1),
(2029, 'poppy', 'Poppy', 100, 0, 1),
(2030, 'opium', 'Opium', 50, 0, 1),
(2031, 'meth', 'Meth', 25, 0, 1),
(2032, 'heroine', 'Heroine', 10, 0, 1),
(2033, 'beer', 'Beer', 30, 0, 1),
(2034, 'tequila', 'Tequila', 10, 0, 1),
(2035, 'vodka', 'Vodka', 10, 0, 1),
(2036, 'whiskey', 'Whiskey', 10, 0, 1),
(2037, 'crack', 'Crack', 25, 0, 1),
(2038, 'drugtest', 'DrugTest', 10, 0, 1),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1),
(2041, 'pcp', 'PCP', 25, 0, 1),
(2042, 'dabs', 'Dabs', 50, 0, 1),
(2043, 'painkiller', 'Painkiller', 10, 0, 1),
(2044, 'narcan', 'Narcan', 10, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'Unemployed', 0),
(3, 'police', 'LEO', 1),
(4, 'slaughterer', 'Slaughterer', 0),
(5, 'fisherman', 'Fisherman', 0),
(6, 'miner', 'Miner', 0),
(7, 'lumberjack', 'Woodcutter', 0),
(8, 'fuel', 'Refiner', 0),
(9, 'reporter', 'Journalist', 0),
(10, 'textil', 'Couturier', 0),
(11, 'mecano', 'Mechanic', 0),
(12, 'taxi', 'Taxi', 0),
(14, 'trucker', 'Trucker', 0),
(16, 'fire', 'LFD', 1),
(17, 'deliverer', 'Deliverer', 0),
(18, 'brinks', 'Transport', 0),
(19, 'gopostal', 'GoPostal', 0),
(20, 'airlines', 'Airlines', 0),
(21, 'ambulance', 'AMR', 1),
(22, 'mafia', 'Mafia', 1),
(24, 'rebel', 'Rebel', 1),
(25, 'security', 'Security', 0),
(28, 'garbage', 'Garbage Driver', 0),
(29, 'pizza', 'Pizzadelivery', 0),
(30, 'ranger', 'parkranger', 0),
(33, 'unicorn', 'Unicorn', 0),
(34, 'bus', 'busdriver', 0),
(36, 'coastguard', 'CoastGuard', 0),
(38, 'irish', 'Irish', 1),
(39, 'rodriguez', 'Rodriguez', 1),
(41, 'bishops', 'Bishops', 1),
(42, 'irish', 'Irish', 1),
(43, 'lawyer', 'lawyer', 0),
(45, 'dismay', 'Dismay', 1),
(46, 'bountyhunter', 'Bountyhunter', 1),
(47, 'grove', 'Grove Street Family', 1),
(49, 'vagos', 'Vagos', 1),
(50, 'ballas', 'Ballas', 1),
(51, 'traffic', 'trafficofficer', 0),
(52, 'poolcleaner', 'PoolCleaner', 0),
(53, 'carthief', 'Car Thief', 0),
(54, 'Salvage', 'Salvage', 0),
(55, 'realestateagent', 'Real Estate Agent', 1),
(56, 'admin', 'Admin', 1),
(57, 'biker', 'HHMC', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(0, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(1, 'lumberjack', 0, 'interim', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'interim', 'Employee', 250, '{}', '{}'),
(3, 'fuel', 0, 'interim', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'textil', 0, 'interim', 'Employee', 250, '{}', '{}'),
(6, 'miner', 0, 'interim', 'Employee', 250, '{}', '{}'),
(7, 'slaughterer', 0, 'interim', 'Employee', 250, '{}', '{}'),
(8, 'mecano', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mecano', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mecano', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mecano', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mecano', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'recruit', 'Recruit', 800, '{}', '{}'),
(23, 'airlines', 1, 'firstofficer', 'Firstofficer', 800, '{}', '{}'),
(24, 'airlines', 0, 'pilote', 'Pilote', 800, '{}', '{}'),
(25, 'airlines', 0, 'gerant', 'Gerant', 800, '{}', '{}'),
(26, 'airlines', 0, 'boss', 'Patron', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'interim', 'Interimaire', 400, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'realestateagent', 0, 'agnt', 'Agent', 550, '{}', '{}'),
(78, 'realestateagent', 1, 'seller', 'Senior Agent', 965, '{}', '{}'),
(79, 'realestateagent', 2, 'mgmtn', 'Management', 1750, '{}', '{}'),
(80, 'realestateagent', 3, 'boss', 'Boss', 3000, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(120, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(147, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(148, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(149, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(150, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(156, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(157, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(158, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'hangaround', 'Hangaround', 100, '{}', '{}'),
(164, 'biker', 1, 'prospect', 'Prospect', 200, '{}', '{}'),
(165, 'biker', 2, 'chaptermember', 'Chapter Member', 500, '{}', '{}'),
(166, 'biker', 3, 'nomad', 'Nomad', 750, '{}', '{}'),
(167, 'biker', 4, 'executiveboard', 'Executive Board', 1000, '{}', '{}'),
(168, 'biker', 5, 'roadcapitan', 'Road Capitan', 1000, '{}', '{}'),
(169, 'biker', 6, 'treasurer', 'Treasurer', 1000, '{}', '{}'),
(170, 'biker', 7, 'secretary', 'Secretary', 1000, '{}', '{}'),
(171, 'biker', 8, 'enforcer', 'Enforcer', 1000, '{}', '{}'),
(172, 'biker', 9, 'boss', 'Vice President', 1000, '{}', '{}'),
(173, 'biker', 10, 'boss', 'President', 5000, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kicks`
--

CREATE TABLE `kicks` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `outfits`
--

CREATE TABLE `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) NOT NULL COMMENT 'mp_m_freemode_01',
  `face` int(11) NOT NULL COMMENT '0',
  `face_text` int(11) NOT NULL COMMENT '0',
  `hair` int(11) NOT NULL COMMENT '0',
  `pants` int(11) NOT NULL COMMENT '0',
  `pants_text` int(11) NOT NULL COMMENT '0',
  `shoes` int(11) NOT NULL COMMENT '0',
  `shoes_text` int(11) NOT NULL COMMENT '0',
  `torso` int(11) NOT NULL COMMENT '0',
  `torso_text` int(11) NOT NULL COMMENT '0',
  `shirt` int(11) NOT NULL COMMENT '0',
  `shirt_text` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `state of the car` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'State of the car'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`id`, `vehicle`, `owner`, `plate`, `state of the car`, `state`) VALUES
(1, '{\"modSuspension\":-1,\"color1\":74,\"modTrimA\":-1,\"modTurbo\":false,\"modRearBumper\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"modTransmission\":-1,\"plate\":\"00PSQ855\",\"wheels\":1,\"modFrontBumper\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modTank\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"modFender\":-1,\"modWindows\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modDashboard\":-1,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"health\":1000,\"pearlescentColor\":5,\"modSpoilers\":-1,\"modSideSkirt\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"color2\":0,\"modEngineBlock\":-1,\"modExhaust\":-1,\"tyreSmokeColor\":[255,255,255],\"modDoorSpeaker\":-1,\"plateIndex\":0,\"modPlateHolder\":-1,\"modBackWheels\":-1,\"modRoof\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modEngine\":-1,\"windowTint\":-1,\"modFrontWheels\":-1,\"modSmokeEnabled\":false,\"modSpeakers\":-1,\"modHorns\":-1,\"modArmor\":-1,\"wheelColor\":1,\"modAerials\":-1,\"extras\":[],\"modFrame\":-1,\"modXenon\":false,\"model\":1115909093,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"dirtLevel\":4.0,\"neonEnabled\":[false,false,false,false],\"modRightFender\":-1,\"modLivery\":-1}', 'steam:110000132580eb0', '', 0, 1),
(2, '{\"modRightFender\":-1,\"modArchCover\":-1,\"modTrunk\":-1,\"modFrame\":-1,\"modTank\":-1,\"modStruts\":-1,\"wheels\":0,\"modSmokeEnabled\":1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"health\":1000,\"modGrille\":-1,\"modSteeringWheel\":-1,\"modTransmission\":-1,\"plateIndex\":3,\"modLivery\":-1,\"neonColor\":[255,0,255],\"model\":1353720154,\"modFrontBumper\":-1,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"dirtLevel\":0.24828369915485,\"modTrimA\":-1,\"modDashboard\":-1,\"modFrontWheels\":-1,\"modArmor\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"plate\":\"00WEY909\",\"windowTint\":-1,\"modWindows\":-1,\"modXenon\":false,\"modEngine\":-1,\"color2\":70,\"modRoof\":-1,\"wheelColor\":156,\"modPlateHolder\":-1,\"modHydrolic\":-1,\"modSuspension\":-1,\"neonEnabled\":[false,false,false,false],\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modExhaust\":-1,\"extras\":[],\"modEngineBlock\":-1,\"modAerials\":-1,\"modBrakes\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modTrimB\":-1,\"pearlescentColor\":63,\"modFender\":-1,\"color1\":73,\"modDial\":-1,\"modTurbo\":false,\"modBackWheels\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255]}', 'steam:110000132580eb0', '', 0, 1),
(3, '{\"model\":1353720154,\"plate\":\"05SKH032\",\"neonEnabled\":[false,false,false,false],\"wheels\":0,\"modBackWheels\":-1,\"modBrakes\":-1,\"modTurbo\":false,\"modTank\":-1,\"extras\":[],\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"health\":1000,\"modSpeakers\":-1,\"modFrontBumper\":-1,\"modDoorSpeaker\":-1,\"modGrille\":-1,\"modExhaust\":-1,\"modLivery\":-1,\"color1\":73,\"modFrame\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"modXenon\":false,\"color2\":70,\"modTrimA\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modEngine\":-1,\"modRearBumper\":-1,\"modArmor\":-1,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modFender\":-1,\"modHood\":-1,\"plateIndex\":3,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modWindows\":-1,\"modOrnaments\":-1,\"windowTint\":-1,\"dirtLevel\":8.0,\"modAerials\":-1,\"modPlateHolder\":-1,\"modSteeringWheel\":-1,\"modVanityPlate\":-1,\"modEngineBlock\":-1,\"modTransmission\":-1,\"wheelColor\":156,\"pearlescentColor\":63,\"modSmokeEnabled\":false,\"modAirFilter\":-1,\"modStruts\":-1,\"modRoof\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modTrimB\":-1,\"modShifterLeavers\":-1,\"modHorns\":-1,\"modAPlate\":-1}', 'steam:1100001372437de', '', 0, 1),
(4, '{\"modRearBumper\":-1,\"modTrunk\":-1,\"modEngine\":3,\"extras\":{\"2\":false,\"1\":true},\"modTrimB\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modVanityPlate\":-1,\"modWindows\":-1,\"modRightFender\":-1,\"wheelColor\":0,\"modExhaust\":-1,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":0,\"modFrontWheels\":-1,\"modDial\":-1,\"modAirFilter\":-1,\"modSteeringWheel\":-1,\"health\":1000,\"modBrakes\":2,\"tyreSmokeColor\":[0,255,0],\"modTrimA\":-1,\"neonColor\":[255,0,255],\"modFrontBumper\":-1,\"modSuspension\":-1,\"modSpoilers\":0,\"modHydrolic\":-1,\"dirtLevel\":5.2288327217102,\"modEngineBlock\":-1,\"modArchCover\":-1,\"model\":305501667,\"modSideSkirt\":0,\"modTurbo\":1,\"modSpeakers\":-1,\"plate\":\"46VAU284\",\"modBackWheels\":-1,\"modPlateHolder\":-1,\"color1\":0,\"modAerials\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modFender\":-1,\"modSeats\":-1,\"modShifterLeavers\":-1,\"modXenon\":1,\"modDashboard\":-1,\"modFrame\":-1,\"plateIndex\":4,\"modTransmission\":2,\"color2\":118,\"modLivery\":-1,\"modTank\":-1,\"modOrnaments\":-1,\"windowTint\":5,\"modRoof\":-1,\"modStruts\":-1,\"modSmokeEnabled\":1,\"modHorns\":-1,\"wheels\":6,\"modGrille\":-1}', 'steam:110000132580eb0', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `steam` varchar(50) NOT NULL,
  `playtime` int(11) NOT NULL,
  `firstjoined` varchar(50) NOT NULL,
  `lastplayed` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playerstattoos`
--

INSERT INTO `playerstattoos` (`id`, `identifier`, `tattoos`) VALUES
(1, 'steam:110000132580eb0', '[]'),
(2, 'steam:110000100023daf', '[]'),
(3, 'steam:11000010a01bdb9', '[]'),
(4, 'steam:110000112969e8f', '[]'),
(5, 'steam:1100001068ef13c', '[]'),
(6, 'steam:1100001138168a0', '[]'),
(7, 'steam:1100001320dfb72', '[]'),
(8, 'steam:1100001159dff06', '[]'),
(9, 'steam:11000010dc84b6d', '[]'),
(10, 'steam:11000010c87fe96', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Apartment de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modyrn1Apartment', 'Apartment Modern 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modyrn2Apartment', 'Apartment Modern 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modyrn3Apartment', 'Apartment Modern 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Apartment Mody 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Apartment Mody 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Apartment Mody 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Apartment Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Apartment Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Apartment Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Apartment Persian 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Apartment Persian 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Apartment Persian 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Apartment Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Apartment Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Apartment Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Apartment Seductive 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Apartment Seductive 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Apartment Seductive 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Apartment Regal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Apartment Regal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Apartment Regal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Apartment Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Apartment Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Apartment Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `received_bans`
--

CREATE TABLE `received_bans` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `banned_by` varchar(255) DEFAULT NULL,
  `banned_on` varchar(255) DEFAULT NULL,
  `ban_expires` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `ID` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `connection` int(11) NOT NULL,
  `rcon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `server_actions`
--

CREATE TABLE `server_actions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_do` varchar(255) DEFAULT NULL,
  `action_ammount` varchar(255) DEFAULT NULL,
  `byadmin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 15),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 15),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 15),
(6, 'LTDgasoline', 'water', 15),
(7, 'TwentyFourSeven', 'scratchoff', 20),
(16, 'LTDgasoline', 'fixkit', 50),
(19, 'RobsLiquor', 'fixkit', 50),
(22, 'TwentyFourSeven', 'fixkit', 50),
(24, 'RobsLiquor', 'beer', 45),
(25, 'TwentyFourSeven', 'lockpick', 100),
(26, 'LTDgasoline', 'lockpick', 100),
(27, 'RobsLiquor', 'vodka', 50),
(28, 'LTDgasoline', 'vodka', 50),
(29, 'TwentyFourSeven', 'coffee', 30),
(30, 'LTDgasoline', 'coffee', 30),
(31, 'RobsLiquor', 'coffee', 30),
(32, 'RobsLiquor', 'nitro', 500),
(33, 'RobsLiquor', 'cola', 100),
(34, 'RobsLiquor', 'vegetables', 100),
(35, 'RobsLiquor', 'meat', 100),
(36, 'RobsLiquor', 'silencieux', 500),
(37, 'RobsLiquor', 'flashlight', 500),
(38, 'RobsLiquor', 'grip', 500),
(39, 'RobsLiquor', 'yusuf', 500),
(40, 'TwentyFourSeven', 'binoculars', 1000),
(41, 'RobsLiquor', 'binoculars', 1000),
(42, 'LTDgasoline', 'binoculars', 1000),
(43, 'LTDgasoline', 'binoculars', 1000),
(44, 'RobsLiquor', 'blackberry', 50),
(45, 'LTDgasoline', 'lighter', 10),
(46, 'LTDgasoline', 'cigarett', 35),
(47, 'RobsLiquor', 'armor', 500),
(48, 'LTDgasoline', 'plongee1', 250),
(49, 'RobsLiquor', 'plongee1', 250),
(50, 'TwentyFourSeven', 'plongee1', 250),
(51, 'LTDgasoline', 'plongee2', 350),
(52, 'RobsLiquor', 'plongee2', 350),
(53, 'TwentyFourSeven', 'plongee2', 350),
(54, 'LTDgasoline', 'scratchoff', 20),
(55, 'PDShop', 'coffee', 1),
(56, 'PDShop', 'donut', 1),
(57, 'PDShop', 'clip', 1),
(58, 'PDShop', 'armor', 1),
(59, 'PDShop', 'medikit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `rank` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `steamid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` int(11) DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeplayed` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `online` int(10) NOT NULL DEFAULT '0',
  `server` int(10) NOT NULL DEFAULT '1',
  `is_dead` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `ID`, `rank`, `steamid`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `last_property`, `animal`, `timeplayed`, `online`, `server`, `is_dead`) VALUES
('steam:11000010a01bdb9', 5, '', '', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 4856, 'stickybombz', NULL, 'police', 36, '[]', '{\"z\":30.6,\"y\":-1392.5,\"x\":284.5}', 386300, 0, 'user', '[{\"val\":448400,\"name\":\"hunger\",\"percent\":44.84},{\"val\":435500,\"name\":\"thirst\",\"percent\":43.55},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', 56312, NULL, NULL, '0', 0, 1, 0),
('steam:110000132580eb0', 6, '', '', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 357, 'K9Marine', NULL, 'police', 37, '[{\"components\":[\"clip_default\"],\"ammo\":42,\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\"}]', '{\"z\":30.7,\"y\":-806.5,\"x\":218.4}', 5108091, 0, 'user', '[{\"val\":609400,\"name\":\"hunger\",\"percent\":60.94},{\"val\":410500,\"name\":\"thirst\",\"percent\":41.05},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'Jak', 'Fulton', '10/10/1988', 'M', '74', 85569, NULL, NULL, '0', 0, 1, 0),
('steam:1100001372437de', 7, '', '', 'license:89c2710265a4729f5623af0ebe267216b81935b0', 0, 'Shawny V', NULL, 'police', 12, '[]', '{\"x\":272.8,\"y\":-1358.8,\"z\":24.5}', 12200, 0, 'user', '[{\"name\":\"hunger\",\"percent\":50.0,\"val\":500000},{\"name\":\"thirst\",\"percent\":50.0,\"val\":500000},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', 'Shawn', 'Vargas', '010/9/1990', 'M', '60', 35298, NULL, NULL, '0', 0, 1, 0),
('steam:1100001068ef13c', 8, '', '', 'license:a4979e4221783962685bb8a6105e2b93fc364e77', 472, 'Soft-Hearted Devil', NULL, 'police', 1, '[]', '{\"z\":93.0,\"y\":-377.7,\"x\":2563.8}', 8800, 0, 'user', '[{\"val\":448600,\"name\":\"hunger\",\"percent\":44.86},{\"val\":119500,\"name\":\"thirst\",\"percent\":11.95},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'William', 'Woodard', '7/27/1979', '', '', 57878, NULL, NULL, '0', 0, 1, 0),
('steam:110000112969e8f', 9, '', '', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 458, 'SuperSteve902', NULL, 'police', 11, '[{\"label\":\"Nightstick\",\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[]},{\"label\":\"Combat pistol\",\"ammo\":283,\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\"]},{\"label\":\"Pump shotgun\",\"ammo\":336,\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[]},{\"label\":\"Special carbine\",\"ammo\":238,\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"]},{\"label\":\"Fire extinguisher\",\"ammo\":84,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[]},{\"label\":\"Taser\",\"ammo\":168,\"name\":\"WEAPON_STUNGUN\",\"components\":[]},{\"label\":\"Flaregun\",\"ammo\":20,\"name\":\"WEAPON_FLAREGUN\",\"components\":[]},{\"label\":\"Flashlight\",\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]}]', '{\"z\":33.4,\"y\":3674.1,\"x\":1860.7}', 25900, 0, 'user', '[{\"val\":311400,\"name\":\"hunger\",\"percent\":31.14},{\"val\":264250,\"name\":\"thirst\",\"percent\":26.425},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'Steven', 'Super', '05/08/03', 'M', '86', 69787, NULL, NULL, '0', 0, 1, 0),
('steam:11000010b15a7d4', 10, '', '', 'license:d994e5ab0451d1015819093a31bec6006f915865', 0, 'MrFunBeard', NULL, 'police', 5, '[]', '{\"z\":29.0,\"y\":-1046.7,\"x\":79.1}', -200, 0, 'user', '[{\"name\":\"hunger\",\"val\":307800,\"percent\":30.78},{\"name\":\"thirst\",\"val\":259750,\"percent\":25.975},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', 'Bob', 'Saget', '06/07/1200', 'M', '90', 69298, NULL, NULL, '0', 0, 1, 0),
('steam:110000136d9eea0', 11, '', '', 'license:5ea51ac4f5afcd0887f13db91597d9ab8c4d7fcd', 0, 'Gronkafied', NULL, 'unemployed', 0, '[]', '{\"x\":-1053.6,\"y\":-2541.7,\"z\":13.8}', 100, 0, 'user', '[{\"percent\":98.8,\"name\":\"hunger\",\"val\":988000},{\"percent\":98.5,\"name\":\"thirst\",\"val\":985000},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Billy', 'Hamilton', '03/14/1990', 'm', '76', 27641, NULL, NULL, '0', 0, 1, 0),
('steam:110000133989c6d', 12, '', '', 'license:ce648691913789b0a9848b814b19ca1ea371a982', 0, 'lamario9002', NULL, 'unemployed', 0, '[]', '{\"z\":52.9,\"y\":-1025.5,\"x\":145.9}', 200, 0, 'user', '[{\"val\":875200,\"percent\":87.52,\"name\":\"hunger\"},{\"val\":844000,\"percent\":84.4,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'Lance', 'Vance', '02/25/1992', 'M', '69', 45573, NULL, NULL, '0', 0, 1, 1),
('steam:11000013ca51b76', 13, '', '', 'license:b1644426b59f325bf80a8a1de9289714f80bb90f', 0, 'dorseydotson', NULL, 'unemployed', 0, '[]', '{\"x\":-1057.0,\"y\":-2539.8,\"z\":13.9}', 100, 0, 'user', '[{\"name\":\"hunger\",\"percent\":95.82,\"val\":958200},{\"name\":\"thirst\",\"percent\":94.775,\"val\":947750},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', '', '', '', '', '', 39064, NULL, NULL, '0', 0, 1, 0),
('steam:11000010bf29a8c', 14, '', '', 'license:d48beaffd3aa41001e4102aa4a946c0d009bbeef', 0, 'skrrtlord', NULL, 'unemployed', 0, '[]', '{\"z\":28.9,\"y\":-1013.3,\"x\":189.7}', 300, 0, 'user', '[{\"val\":788200,\"name\":\"hunger\",\"percent\":78.82},{\"val\":735250,\"name\":\"thirst\",\"percent\":73.525},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'Chayne', 'Fisher', '02251996', 'm', '96', 76041, NULL, NULL, '0', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(1, 'steam:110000132580eb0', 'black_money', 0),
(2, 'steam:110000100023daf', 'black_money', 0),
(3, 'steam:11000010a01bdb9', 'black_money', 625),
(4, 'steam:110000112969e8f', 'black_money', 0),
(5, 'steam:1100001068ef13c', 'black_money', 0),
(6, 'steam:1100001138168a0', 'black_money', 0),
(7, 'steam:1100001320dfb72', 'black_money', 0),
(8, 'steam:1100001159dff06', 'black_money', 0),
(9, 'steam:11000010dc84b6d', 'black_money', 0),
(10, 'steam:11000010c87fe96', 'black_money', 0),
(11, 'steam:1100001372437de', 'black_money', 0),
(12, 'steam:11000010b15a7d4', 'black_money', 0),
(13, 'steam:110000136d9eea0', 'black_money', 0),
(14, 'steam:110000133989c6d', 'black_money', 0),
(15, 'steam:11000013ca51b76', 'black_money', 0),
(16, 'steam:11000010bf29a8c', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin_notes`
--

CREATE TABLE `user_admin_notes` (
  `id` int(11) NOT NULL,
  `note` longblob,
  `admin` varchar(255) DEFAULT NULL,
  `note_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_contacts`
--

INSERT INTO `user_contacts` (`id`, `identifier`, `name`, `number`) VALUES
(1, 'steam:110000112969e8f', 'Sticky', 90634),
(2, 'steam:11000010a01bdb9', 'Grant', 77192);

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_documents`
--

INSERT INTO `user_documents` (`id`, `owner`, `data`) VALUES
(9, 'steam:11000010a01bdb9', '{\"headerDateCreated\":\"04/06/2019 0:1:12\",\"headerTitle\":\"MEDICAL REPORT - REFUSAL OF TREATMENT\",\"submittable\":true,\"signed\":true,\"headerSubtitle\":\"Official medical report of refusal to be treated by medical team.\",\"headerDateOfBirth\":\"12/28/1988\",\"headerJobGrade\":\"Cadet\",\"headerJobLabel\":\"AMR\",\"headerLastName\":\"Pickles\",\"elements\":[{\"value\":\"dfgh\",\"elementid\":\"_m4\",\"type\":\"input\",\"label\":\"INSURED FIRSTNAME\"},{\"value\":\"dfhbsd\",\"elementid\":\"_m5\",\"type\":\"input\",\"label\":\"INSURED LASTNAME\"},{\"elementid\":\"_m6\",\"label\":\"DATE OF REPORT\",\"can_be_empty\":false,\"type\":\"input\",\"value\":\"dghn\"},{\"value\":\"THE AFOREMENTIONED HAS REFUSED TREATMENT BY MEDICAL PROFESSIONALS. CITIZEN WAS GIVEN THE RECOMMENDATION TO VISIT AN ER AT EARLIEST CONVIENCE.\",\"elementid\":\"_m7\",\"type\":\"textarea\",\"label\":\"MEDICAL NOTES\"}],\"headerFirstName\":\"Tommie\"}'),
(10, 'steam:110000132580eb0', '{\"headerDateCreated\":\"04/06/2019 0:1:12\",\"headerLastName\":\"Pickles\",\"submittable\":true,\"signed\":true,\"headerSubtitle\":\"Official medical report of refusal to be treated by medical team.\",\"headerDateOfBirth\":\"12/28/1988\",\"headerJobGrade\":\"Cadet\",\"headerJobLabel\":\"AMR\",\"headerTitle\":\"MEDICAL REPORT - REFUSAL OF TREATMENT\",\"elements\":[{\"value\":\"dfgh\",\"elementid\":\"_m4\",\"type\":\"input\",\"label\":\"INSURED FIRSTNAME\"},{\"value\":\"dfhbsd\",\"elementid\":\"_m5\",\"type\":\"input\",\"label\":\"INSURED LASTNAME\"},{\"elementid\":\"_m6\",\"label\":\"DATE OF REPORT\",\"value\":\"dghn\",\"type\":\"input\",\"can_be_empty\":false},{\"value\":\"THE AFOREMENTIONED HAS REFUSED TREATMENT BY MEDICAL PROFESSIONALS. CITIZEN WAS GIVEN THE RECOMMENDATION TO VISIT AN ER AT EARLIEST CONVIENCE.\",\"elementid\":\"_m7\",\"type\":\"textarea\",\"label\":\"MEDICAL NOTES\"}],\"headerFirstName\":\"Tommie\"}');

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'essence', 0),
(2, 'steam:11000010a01bdb9', 'pearl_pooch', 0),
(3, 'steam:11000010a01bdb9', 'carotool', 0),
(4, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(5, 'steam:11000010a01bdb9', 'rhum', 0),
(6, 'steam:11000010a01bdb9', 'croquettes', 0),
(7, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(8, 'steam:11000010a01bdb9', 'silencieux', 0),
(9, 'steam:11000010a01bdb9', 'fabric', 0),
(10, 'steam:11000010a01bdb9', 'mojito', 0),
(11, 'steam:11000010a01bdb9', 'gold', 0),
(12, 'steam:11000010a01bdb9', 'bandage', 0),
(13, 'steam:11000010a01bdb9', 'vegetables', 0),
(14, 'steam:11000010a01bdb9', 'plongee2', 0),
(15, 'steam:11000010a01bdb9', 'icetea', 0),
(16, 'steam:11000010a01bdb9', 'litter', 0),
(17, 'steam:11000010a01bdb9', 'cigarett', 0),
(18, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(19, 'steam:11000010a01bdb9', 'coffee', 0),
(20, 'steam:11000010a01bdb9', 'scratchoff_used', 0),
(21, 'steam:11000010a01bdb9', 'washed_stone', 0),
(22, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(23, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(24, 'steam:11000010a01bdb9', 'teqpaf', 0),
(25, 'steam:11000010a01bdb9', 'carokit', 0),
(26, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(27, 'steam:11000010a01bdb9', 'plongee1', 0),
(28, 'steam:11000010a01bdb9', 'powerade', 0),
(29, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(30, 'steam:11000010a01bdb9', 'soda', 0),
(31, 'steam:11000010a01bdb9', 'blowpipe', 0),
(32, 'steam:11000010a01bdb9', 'petrol', 0),
(33, 'steam:11000010a01bdb9', 'gym_membership', 0),
(34, 'steam:11000010a01bdb9', 'turtle', 0),
(35, 'steam:11000010a01bdb9', 'fish', 0),
(36, 'steam:11000010a01bdb9', 'whool', 0),
(37, 'steam:11000010a01bdb9', 'water', 0),
(38, 'steam:11000010a01bdb9', 'contrat', 0),
(39, 'steam:11000010a01bdb9', 'stone', 0),
(40, 'steam:11000010a01bdb9', 'pills', 0),
(41, 'steam:11000010a01bdb9', 'cola', 0),
(42, 'steam:11000010a01bdb9', 'whisky', 0),
(43, 'steam:11000010a01bdb9', 'saucisson', 0),
(44, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(45, 'steam:11000010a01bdb9', 'armor', 0),
(46, 'steam:11000010a01bdb9', 'grip', 0),
(47, 'steam:11000010a01bdb9', 'leather', 0),
(48, 'steam:11000010a01bdb9', 'nitro', 0),
(49, 'steam:11000010a01bdb9', 'lighter', 0),
(50, 'steam:11000010a01bdb9', 'vodka', 0),
(51, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(52, 'steam:11000010a01bdb9', 'clip', 0),
(53, 'steam:11000010a01bdb9', 'scratchoff', 0),
(54, 'steam:11000010a01bdb9', 'binoculars', 0),
(55, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(56, 'steam:11000010a01bdb9', 'blackberry', 0),
(57, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(58, 'steam:11000010a01bdb9', 'flashlight', 0),
(59, 'steam:11000010a01bdb9', 'bolchips', 0),
(60, 'steam:11000010a01bdb9', 'burger', 0),
(61, 'steam:11000010a01bdb9', 'yusuf', 0),
(62, 'steam:11000010a01bdb9', 'pearl', 0),
(63, 'steam:11000010a01bdb9', 'litter_pooch', 0),
(64, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(65, 'steam:11000010a01bdb9', 'jager', 0),
(66, 'steam:11000010a01bdb9', 'opium', 0),
(67, 'steam:11000010a01bdb9', 'donut', 0),
(68, 'steam:11000010a01bdb9', 'tacos', 0),
(69, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(70, 'steam:11000010a01bdb9', 'wood', 0),
(71, 'steam:11000010a01bdb9', 'weed', 0),
(72, 'steam:11000010a01bdb9', 'lockpick', 0),
(73, 'steam:11000010a01bdb9', 'medikit', 0),
(74, 'steam:11000010a01bdb9', 'metreshooter', 0),
(75, 'steam:11000010a01bdb9', 'menthe', 0),
(76, 'steam:11000010a01bdb9', 'golem', 0),
(77, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(78, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(79, 'steam:11000010a01bdb9', 'mixapero', 0),
(80, 'steam:11000010a01bdb9', 'diamond', 0),
(81, 'steam:11000010a01bdb9', 'bread', 0),
(82, 'steam:11000010a01bdb9', 'gazbottle', 0),
(83, 'steam:11000010a01bdb9', 'martini', 0),
(84, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(85, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(86, 'steam:11000010a01bdb9', 'turtle_pooch', 0),
(87, 'steam:11000010a01bdb9', 'jusfruit', 0),
(88, 'steam:11000010a01bdb9', 'meat', 0),
(89, 'steam:11000010a01bdb9', 'clothe', 0),
(90, 'steam:11000010a01bdb9', 'iron', 0),
(91, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(92, 'steam:11000010a01bdb9', 'fixtool', 0),
(93, 'steam:11000010a01bdb9', 'lsd', 0),
(94, 'steam:11000010a01bdb9', 'coke', 0),
(95, 'steam:11000010a01bdb9', 'energy', 0),
(96, 'steam:11000010a01bdb9', 'copper', 0),
(97, 'steam:11000010a01bdb9', 'protein_shake', 0),
(98, 'steam:11000010a01bdb9', 'sportlunch', 0),
(99, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(100, 'steam:11000010a01bdb9', 'beer', 0),
(101, 'steam:11000010a01bdb9', 'bolpistache', 0),
(102, 'steam:11000010a01bdb9', 'ice', 0),
(103, 'steam:11000010a01bdb9', 'drpepper', 0),
(104, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(105, 'steam:11000010a01bdb9', 'limonade', 0),
(106, 'steam:11000010a01bdb9', 'meth', 0),
(107, 'steam:11000010a01bdb9', 'tequila', 0),
(108, 'steam:11000010a01bdb9', 'fixkit', 0),
(109, 'steam:11000010dc84b6d', 'mixapero', 0),
(110, 'steam:11000010dc84b6d', 'vodka', 0),
(111, 'steam:11000010dc84b6d', 'litter', 0),
(112, 'steam:11000010dc84b6d', 'vegetables', 0),
(113, 'steam:11000010dc84b6d', 'alive_chicken', 0),
(114, 'steam:11000010dc84b6d', 'mojito', 0),
(115, 'steam:11000010dc84b6d', 'flashlight', 0),
(116, 'steam:11000010dc84b6d', 'turtle_pooch', 0),
(117, 'steam:11000010dc84b6d', 'yusuf', 0),
(118, 'steam:11000010dc84b6d', 'turtle', 0),
(119, 'steam:11000010dc84b6d', 'martini', 0),
(120, 'steam:11000010dc84b6d', 'weed_pooch', 0),
(121, 'steam:11000010dc84b6d', 'gym_membership', 0),
(122, 'steam:11000010dc84b6d', 'medikit', 0),
(123, 'steam:11000010dc84b6d', 'washed_stone', 0),
(124, 'steam:11000010dc84b6d', 'jusfruit', 0),
(125, 'steam:11000010dc84b6d', 'grapperaisin', 0),
(126, 'steam:11000010dc84b6d', 'fixtool', 0),
(127, 'steam:11000010dc84b6d', 'soda', 0),
(128, 'steam:11000010dc84b6d', 'fabric', 0),
(129, 'steam:11000010dc84b6d', 'drpepper', 0),
(130, 'steam:11000010dc84b6d', 'limonade', 0),
(131, 'steam:11000010dc84b6d', 'carokit', 0),
(132, 'steam:11000010dc84b6d', 'rhumfruit', 0),
(133, 'steam:11000010dc84b6d', 'leather', 0),
(134, 'steam:11000010dc84b6d', 'golem', 0),
(135, 'steam:11000010dc84b6d', 'lighter', 0),
(136, 'steam:11000010dc84b6d', 'scratchoff', 0),
(137, 'steam:11000010dc84b6d', 'blowpipe', 0),
(138, 'steam:11000010dc84b6d', 'donut', 0),
(139, 'steam:11000010dc84b6d', 'bolcacahuetes', 0),
(140, 'steam:11000010dc84b6d', 'saucisson', 0),
(141, 'steam:11000010dc84b6d', 'jager', 0),
(142, 'steam:11000010dc84b6d', 'sportlunch', 0),
(143, 'steam:11000010dc84b6d', 'protein_shake', 0),
(144, 'steam:11000010dc84b6d', 'lsd', 0),
(145, 'steam:11000010dc84b6d', 'croquettes', 0),
(146, 'steam:11000010dc84b6d', 'plongee1', 0),
(147, 'steam:11000010dc84b6d', 'fixkit', 0),
(148, 'steam:11000010dc84b6d', 'energy', 0),
(149, 'steam:11000010dc84b6d', 'powerade', 0),
(150, 'steam:11000010dc84b6d', 'armor', 0),
(151, 'steam:11000010dc84b6d', 'coffee', 0),
(152, 'steam:11000010dc84b6d', 'plongee2', 0),
(153, 'steam:11000010dc84b6d', 'water', 0),
(154, 'steam:11000010dc84b6d', 'contrat', 0),
(155, 'steam:11000010dc84b6d', 'whool', 0),
(156, 'steam:11000010dc84b6d', 'cigarett', 0),
(157, 'steam:11000010dc84b6d', 'scratchoff_used', 0),
(158, 'steam:11000010dc84b6d', 'icetea', 0),
(159, 'steam:11000010dc84b6d', 'teqpaf', 0),
(160, 'steam:11000010dc84b6d', 'binoculars', 0),
(161, 'steam:11000010dc84b6d', 'grip', 0),
(162, 'steam:11000010dc84b6d', 'packaged_plank', 0),
(163, 'steam:11000010dc84b6d', 'burger', 0),
(164, 'steam:11000010dc84b6d', 'metreshooter', 0),
(165, 'steam:11000010dc84b6d', 'pearl', 0),
(166, 'steam:11000010dc84b6d', 'nitro', 0),
(167, 'steam:11000010dc84b6d', 'fish', 0),
(168, 'steam:11000010dc84b6d', 'pearl_pooch', 0),
(169, 'steam:11000010dc84b6d', 'meat', 0),
(170, 'steam:11000010dc84b6d', 'meth_pooch', 0),
(171, 'steam:11000010dc84b6d', 'lsd_pooch', 0),
(172, 'steam:11000010dc84b6d', 'cola', 0),
(173, 'steam:11000010dc84b6d', 'menthe', 0),
(174, 'steam:11000010dc84b6d', 'silencieux', 0),
(175, 'steam:11000010dc84b6d', 'ice', 0),
(176, 'steam:11000010dc84b6d', 'gazbottle', 0),
(177, 'steam:11000010dc84b6d', 'whisky', 0),
(178, 'steam:11000010dc84b6d', 'blackberry', 0),
(179, 'steam:11000010dc84b6d', 'rhumcoca', 0),
(180, 'steam:11000010dc84b6d', 'petrol', 0),
(181, 'steam:11000010dc84b6d', 'cutted_wood', 0),
(182, 'steam:11000010dc84b6d', 'stone', 0),
(183, 'steam:11000010dc84b6d', 'beer', 0),
(184, 'steam:11000010dc84b6d', 'vodkafruit', 0),
(185, 'steam:11000010dc84b6d', 'vodkaenergy', 0),
(186, 'steam:11000010dc84b6d', 'whiskycoca', 0),
(187, 'steam:11000010dc84b6d', 'essence', 0),
(188, 'steam:11000010dc84b6d', 'meth', 0),
(189, 'steam:11000010dc84b6d', 'opium', 0),
(190, 'steam:11000010dc84b6d', 'gold', 0),
(191, 'steam:11000010dc84b6d', 'jagerbomb', 0),
(192, 'steam:11000010dc84b6d', 'litter_pooch', 0),
(193, 'steam:11000010dc84b6d', 'copper', 0),
(194, 'steam:11000010dc84b6d', 'weed', 0),
(195, 'steam:11000010dc84b6d', 'pills', 0),
(196, 'steam:11000010dc84b6d', 'bread', 0),
(197, 'steam:11000010dc84b6d', 'coke', 0),
(198, 'steam:11000010dc84b6d', 'coke_pooch', 0),
(199, 'steam:11000010dc84b6d', 'tacos', 0),
(200, 'steam:11000010dc84b6d', 'clip', 0),
(201, 'steam:11000010dc84b6d', 'bolchips', 0),
(202, 'steam:11000010dc84b6d', 'carotool', 0),
(203, 'steam:11000010dc84b6d', 'bandage', 0),
(204, 'steam:11000010dc84b6d', 'bolnoixcajou', 0),
(205, 'steam:11000010dc84b6d', 'clothe', 0),
(206, 'steam:11000010dc84b6d', 'bolpistache', 0),
(207, 'steam:11000010dc84b6d', 'wood', 0),
(208, 'steam:11000010dc84b6d', 'rhum', 0),
(209, 'steam:11000010dc84b6d', 'lockpick', 0),
(210, 'steam:11000010dc84b6d', 'tequila', 0),
(211, 'steam:11000010dc84b6d', 'iron', 0),
(212, 'steam:11000010dc84b6d', 'petrol_raffin', 0),
(213, 'steam:11000010dc84b6d', 'opium_pooch', 0),
(214, 'steam:11000010dc84b6d', 'slaughtered_chicken', 0),
(215, 'steam:11000010dc84b6d', 'packaged_chicken', 0),
(216, 'steam:11000010dc84b6d', 'diamond', 0),
(217, 'steam:1100001068ef13c', 'petrol_raffin', 0),
(218, 'steam:1100001068ef13c', 'vodka', 0),
(219, 'steam:1100001068ef13c', 'fixtool', 0),
(220, 'steam:1100001068ef13c', 'bandage', 0),
(221, 'steam:1100001068ef13c', 'slaughtered_chicken', 0),
(222, 'steam:1100001068ef13c', 'clip', 0),
(223, 'steam:1100001068ef13c', 'metreshooter', 0),
(224, 'steam:1100001068ef13c', 'martini', 0),
(225, 'steam:1100001068ef13c', 'fabric', 0),
(226, 'steam:1100001068ef13c', 'coke_pooch', 0),
(227, 'steam:1100001068ef13c', 'carokit', 0),
(228, 'steam:1100001068ef13c', 'croquettes', 0),
(229, 'steam:1100001068ef13c', 'golem', 0),
(230, 'steam:1100001068ef13c', 'icetea', 0),
(231, 'steam:1100001068ef13c', 'gold', 0),
(232, 'steam:1100001068ef13c', 'leather', 0),
(233, 'steam:1100001068ef13c', 'rhum', 0),
(234, 'steam:1100001068ef13c', 'scratchoff_used', 0),
(235, 'steam:1100001068ef13c', 'jager', 0),
(236, 'steam:1100001068ef13c', 'washed_stone', 0),
(237, 'steam:1100001068ef13c', 'nitro', 0),
(238, 'steam:1100001068ef13c', 'jagerbomb', 0),
(239, 'steam:1100001068ef13c', 'scratchoff', 0),
(240, 'steam:1100001068ef13c', 'turtle', 0),
(241, 'steam:1100001068ef13c', 'blackberry', 0),
(242, 'steam:1100001068ef13c', 'energy', 0),
(243, 'steam:1100001068ef13c', 'essence', 0),
(244, 'steam:1100001068ef13c', 'medikit', 0),
(245, 'steam:1100001068ef13c', 'plongee1', 0),
(246, 'steam:1100001068ef13c', 'fixkit', 10),
(247, 'steam:1100001068ef13c', 'teqpaf', 0),
(248, 'steam:1100001068ef13c', 'meth_pooch', 0),
(249, 'steam:1100001068ef13c', 'powerade', 0),
(250, 'steam:1100001068ef13c', 'drpepper', 0),
(251, 'steam:1100001068ef13c', 'sportlunch', 0),
(252, 'steam:1100001068ef13c', 'contrat', 0),
(253, 'steam:1100001068ef13c', 'donut', 0),
(254, 'steam:1100001068ef13c', 'copper', 0),
(255, 'steam:1100001068ef13c', 'yusuf', 0),
(256, 'steam:1100001068ef13c', 'vodkaenergy', 0),
(257, 'steam:1100001068ef13c', 'cigarett', 0),
(258, 'steam:1100001068ef13c', 'litter_pooch', 0),
(259, 'steam:1100001068ef13c', 'cutted_wood', 0),
(260, 'steam:1100001068ef13c', 'lighter', 0),
(261, 'steam:1100001068ef13c', 'beer', 0),
(262, 'steam:1100001068ef13c', 'armor', 0),
(263, 'steam:1100001068ef13c', 'flashlight', 0),
(264, 'steam:1100001068ef13c', 'turtle_pooch', 0),
(265, 'steam:1100001068ef13c', 'binoculars', 0),
(266, 'steam:1100001068ef13c', 'bolpistache', 0),
(267, 'steam:1100001068ef13c', 'iron', 0),
(268, 'steam:1100001068ef13c', 'pearl', 0),
(269, 'steam:1100001068ef13c', 'silencieux', 0),
(270, 'steam:1100001068ef13c', 'opium', 0),
(271, 'steam:1100001068ef13c', 'burger', 0),
(272, 'steam:1100001068ef13c', 'clothe', 0),
(273, 'steam:1100001068ef13c', 'meat', 0),
(274, 'steam:1100001068ef13c', 'tacos', 0),
(275, 'steam:1100001068ef13c', 'vegetables', 0),
(276, 'steam:1100001068ef13c', 'carotool', 0),
(277, 'steam:1100001068ef13c', 'pearl_pooch', 0),
(278, 'steam:1100001068ef13c', 'menthe', 0),
(279, 'steam:1100001068ef13c', 'limonade', 0),
(280, 'steam:1100001068ef13c', 'litter', 0),
(281, 'steam:1100001068ef13c', 'bolcacahuetes', 0),
(282, 'steam:1100001068ef13c', 'opium_pooch', 0),
(283, 'steam:1100001068ef13c', 'saucisson', 0),
(284, 'steam:1100001068ef13c', 'packaged_chicken', 0),
(285, 'steam:1100001068ef13c', 'grip', 0),
(286, 'steam:1100001068ef13c', 'lsd', 0),
(287, 'steam:1100001068ef13c', 'gazbottle', 0),
(288, 'steam:1100001068ef13c', 'cola', 0),
(289, 'steam:1100001068ef13c', 'mixapero', 0),
(290, 'steam:1100001068ef13c', 'plongee2', 0),
(291, 'steam:1100001068ef13c', 'soda', 0),
(292, 'steam:1100001068ef13c', 'ice', 0),
(293, 'steam:1100001068ef13c', 'petrol', 0),
(294, 'steam:1100001068ef13c', 'rhumcoca', 0),
(295, 'steam:1100001068ef13c', 'fish', 0),
(296, 'steam:1100001068ef13c', 'meth', 0),
(297, 'steam:1100001068ef13c', 'lockpick', 0),
(298, 'steam:1100001068ef13c', 'gym_membership', 0),
(299, 'steam:1100001068ef13c', 'vodkafruit', 0),
(300, 'steam:1100001068ef13c', 'blowpipe', 0),
(301, 'steam:1100001068ef13c', 'coke', 0),
(302, 'steam:1100001068ef13c', 'coffee', 0),
(303, 'steam:1100001068ef13c', 'jusfruit', 0),
(304, 'steam:1100001068ef13c', 'alive_chicken', 0),
(305, 'steam:1100001068ef13c', 'whiskycoca', 0),
(306, 'steam:1100001068ef13c', 'mojito', 0),
(307, 'steam:1100001068ef13c', 'weed', 0),
(308, 'steam:1100001068ef13c', 'grapperaisin', 0),
(309, 'steam:1100001068ef13c', 'weed_pooch', 0),
(310, 'steam:1100001068ef13c', 'bread', 23),
(311, 'steam:1100001068ef13c', 'lsd_pooch', 0),
(312, 'steam:1100001068ef13c', 'packaged_plank', 0),
(313, 'steam:1100001068ef13c', 'wood', 0),
(314, 'steam:1100001068ef13c', 'bolnoixcajou', 0),
(315, 'steam:1100001068ef13c', 'tequila', 0),
(316, 'steam:1100001068ef13c', 'bolchips', 0),
(317, 'steam:1100001068ef13c', 'stone', 0),
(318, 'steam:1100001068ef13c', 'whool', 0),
(319, 'steam:1100001068ef13c', 'water', 11),
(320, 'steam:1100001068ef13c', 'protein_shake', 0),
(321, 'steam:1100001068ef13c', 'diamond', 0),
(322, 'steam:1100001068ef13c', 'whisky', 0),
(323, 'steam:1100001068ef13c', 'rhumfruit', 0),
(324, 'steam:1100001068ef13c', 'pills', 0),
(325, 'steam:110000132580eb0', 'jusfruit', 0),
(326, 'steam:110000132580eb0', 'rhumcoca', 0),
(327, 'steam:110000132580eb0', 'lockpick', 0),
(328, 'steam:110000132580eb0', 'medikit', 0),
(329, 'steam:110000132580eb0', 'saucisson', 0),
(330, 'steam:110000132580eb0', 'washed_stone', 0),
(331, 'steam:110000132580eb0', 'scratchoff_used', 0),
(332, 'steam:110000132580eb0', 'meth_pooch', 0),
(333, 'steam:110000132580eb0', 'beer', 0),
(334, 'steam:110000132580eb0', 'litter', 0),
(335, 'steam:110000132580eb0', 'ice', 0),
(336, 'steam:110000132580eb0', 'flashlight', 0),
(337, 'steam:110000132580eb0', 'bread', 17),
(338, 'steam:110000132580eb0', 'bandage', 0),
(339, 'steam:110000132580eb0', 'gold', 0),
(340, 'steam:110000132580eb0', 'plongee1', 0),
(341, 'steam:110000132580eb0', 'binoculars', 0),
(342, 'steam:110000132580eb0', 'pills', 0),
(343, 'steam:110000132580eb0', 'tequila', 0),
(344, 'steam:110000132580eb0', 'wood', 0),
(345, 'steam:110000132580eb0', 'jager', 0),
(346, 'steam:110000132580eb0', 'limonade', 0),
(347, 'steam:110000132580eb0', 'litter_pooch', 0),
(348, 'steam:110000132580eb0', 'opium_pooch', 0),
(349, 'steam:110000132580eb0', 'blackberry', 0),
(350, 'steam:110000132580eb0', 'leather', 0),
(351, 'steam:110000132580eb0', 'sportlunch', 0),
(352, 'steam:110000132580eb0', 'coke_pooch', 0),
(353, 'steam:110000132580eb0', 'cutted_wood', 0),
(354, 'steam:110000132580eb0', 'diamond', 0),
(355, 'steam:110000132580eb0', 'whiskycoca', 0),
(356, 'steam:110000132580eb0', 'scratchoff', 0),
(357, 'steam:110000132580eb0', 'turtle_pooch', 0),
(358, 'steam:110000132580eb0', 'protein_shake', 0),
(359, 'steam:110000132580eb0', 'plongee2', 0),
(360, 'steam:110000132580eb0', 'powerade', 0),
(361, 'steam:110000132580eb0', 'golem', 0),
(362, 'steam:110000132580eb0', 'coffee', 0),
(363, 'steam:110000132580eb0', 'gym_membership', 0),
(364, 'steam:110000132580eb0', 'packaged_plank', 0),
(365, 'steam:110000132580eb0', 'carotool', 0),
(366, 'steam:110000132580eb0', 'soda', 0),
(367, 'steam:110000132580eb0', 'contrat', 0),
(368, 'steam:110000132580eb0', 'martini', 0),
(369, 'steam:110000132580eb0', 'fixtool', 0),
(370, 'steam:110000132580eb0', 'packaged_chicken', 0),
(371, 'steam:110000132580eb0', 'burger', 0),
(372, 'steam:110000132580eb0', 'menthe', 0),
(373, 'steam:110000132580eb0', 'jagerbomb', 0),
(374, 'steam:110000132580eb0', 'armor', 0),
(375, 'steam:110000132580eb0', 'croquettes', 0),
(376, 'steam:110000132580eb0', 'weed', 0),
(377, 'steam:110000132580eb0', 'bolnoixcajou', 0),
(378, 'steam:110000132580eb0', 'lighter', 0),
(379, 'steam:110000132580eb0', 'yusuf', 0),
(380, 'steam:110000132580eb0', 'grip', 0),
(381, 'steam:110000132580eb0', 'silencieux', 0),
(382, 'steam:110000132580eb0', 'gazbottle', 0),
(383, 'steam:110000132580eb0', 'tacos', 0),
(384, 'steam:110000132580eb0', 'drpepper', 0),
(385, 'steam:110000132580eb0', 'coke', 0),
(386, 'steam:110000132580eb0', 'petrol', 0),
(387, 'steam:110000132580eb0', 'whisky', 0),
(388, 'steam:110000132580eb0', 'donut', 0),
(389, 'steam:110000132580eb0', 'alive_chicken', 0),
(390, 'steam:110000132580eb0', 'stone', 0),
(391, 'steam:110000132580eb0', 'vodkafruit', 0),
(392, 'steam:110000132580eb0', 'slaughtered_chicken', 0),
(393, 'steam:110000132580eb0', 'vodkaenergy', 0),
(394, 'steam:110000132580eb0', 'vegetables', 0),
(395, 'steam:110000132580eb0', 'icetea', 0),
(396, 'steam:110000132580eb0', 'fixkit', 0),
(397, 'steam:110000132580eb0', 'pearl_pooch', 0),
(398, 'steam:110000132580eb0', 'nitro', 0),
(399, 'steam:110000132580eb0', 'energy', 0),
(400, 'steam:110000132580eb0', 'mixapero', 0),
(401, 'steam:110000132580eb0', 'bolcacahuetes', 0),
(402, 'steam:110000132580eb0', 'pearl', 0),
(403, 'steam:110000132580eb0', 'fabric', 0),
(404, 'steam:110000132580eb0', 'blowpipe', 0),
(405, 'steam:110000132580eb0', 'mojito', 0),
(406, 'steam:110000132580eb0', 'vodka', 0),
(407, 'steam:110000132580eb0', 'petrol_raffin', 0),
(408, 'steam:110000132580eb0', 'lsd', 0),
(409, 'steam:110000132580eb0', 'rhum', 0),
(410, 'steam:110000132580eb0', 'cigarett', 0),
(411, 'steam:110000132580eb0', 'cola', 0),
(412, 'steam:110000132580eb0', 'opium', 0),
(413, 'steam:110000132580eb0', 'meat', 0),
(414, 'steam:110000132580eb0', 'teqpaf', 0),
(415, 'steam:110000132580eb0', 'metreshooter', 0),
(416, 'steam:110000132580eb0', 'whool', 0),
(417, 'steam:110000132580eb0', 'clothe', 0),
(418, 'steam:110000132580eb0', 'rhumfruit', 0),
(419, 'steam:110000132580eb0', 'grapperaisin', 0),
(420, 'steam:110000132580eb0', 'bolchips', 0),
(421, 'steam:110000132580eb0', 'turtle', 0),
(422, 'steam:110000132580eb0', 'bolpistache', 0),
(423, 'steam:110000132580eb0', 'fish', 0),
(424, 'steam:110000132580eb0', 'water', 6),
(425, 'steam:110000132580eb0', 'copper', 0),
(426, 'steam:110000132580eb0', 'essence', 0),
(427, 'steam:110000132580eb0', 'iron', 0),
(428, 'steam:110000132580eb0', 'meth', 0),
(429, 'steam:110000132580eb0', 'clip', 0),
(430, 'steam:110000132580eb0', 'lsd_pooch', 0),
(431, 'steam:110000132580eb0', 'carokit', 0),
(432, 'steam:110000132580eb0', 'weed_pooch', 0),
(433, 'steam:11000010c87fe96', 'coke_pooch', 0),
(434, 'steam:11000010c87fe96', 'grapperaisin', 0),
(435, 'steam:11000010c87fe96', 'contrat', 0),
(436, 'steam:11000010c87fe96', 'bandage', 0),
(437, 'steam:11000010c87fe96', 'tequila', 0),
(438, 'steam:11000010c87fe96', 'coffee', 0),
(439, 'steam:11000010c87fe96', 'iron', 0),
(440, 'steam:11000010c87fe96', 'soda', 0),
(441, 'steam:11000010c87fe96', 'bolchips', 0),
(442, 'steam:11000010c87fe96', 'water', 0),
(443, 'steam:11000010c87fe96', 'wood', 0),
(444, 'steam:11000010c87fe96', 'opium_pooch', 0),
(445, 'steam:11000010c87fe96', 'lsd_pooch', 0),
(446, 'steam:11000010c87fe96', 'gold', 0),
(447, 'steam:11000010c87fe96', 'lockpick', 0),
(448, 'steam:11000010c87fe96', 'yusuf', 0),
(449, 'steam:11000010c87fe96', 'plongee1', 0),
(450, 'steam:11000010c87fe96', 'jusfruit', 0),
(451, 'steam:11000010c87fe96', 'essence', 0),
(452, 'steam:11000010c87fe96', 'clip', 0),
(453, 'steam:11000010c87fe96', 'armor', 0),
(454, 'steam:11000010c87fe96', 'lighter', 0),
(455, 'steam:11000010c87fe96', 'leather', 0),
(456, 'steam:11000010c87fe96', 'packaged_plank', 0),
(457, 'steam:11000010c87fe96', 'fixkit', 0),
(458, 'steam:11000010c87fe96', 'plongee2', 0),
(459, 'steam:11000010c87fe96', 'scratchoff', 0),
(460, 'steam:11000010c87fe96', 'protein_shake', 0),
(461, 'steam:11000010c87fe96', 'meth_pooch', 0),
(462, 'steam:11000010c87fe96', 'petrol', 0),
(463, 'steam:11000010c87fe96', 'petrol_raffin', 0),
(464, 'steam:11000010c87fe96', 'powerade', 0),
(465, 'steam:11000010c87fe96', 'rhum', 0),
(466, 'steam:11000010c87fe96', 'silencieux', 0),
(467, 'steam:11000010c87fe96', 'limonade', 0),
(468, 'steam:11000010c87fe96', 'stone', 0),
(469, 'steam:11000010c87fe96', 'gym_membership', 0),
(470, 'steam:11000010c87fe96', 'donut', 0),
(471, 'steam:11000010c87fe96', 'litter_pooch', 0),
(472, 'steam:11000010c87fe96', 'binoculars', 0),
(473, 'steam:11000010c87fe96', 'cigarett', 0),
(474, 'steam:11000010c87fe96', 'scratchoff_used', 0),
(475, 'steam:11000010c87fe96', 'slaughtered_chicken', 0),
(476, 'steam:11000010c87fe96', 'blackberry', 0),
(477, 'steam:11000010c87fe96', 'pills', 0),
(478, 'steam:11000010c87fe96', 'teqpaf', 0),
(479, 'steam:11000010c87fe96', 'croquettes', 0),
(480, 'steam:11000010c87fe96', 'litter', 0),
(481, 'steam:11000010c87fe96', 'grip', 0),
(482, 'steam:11000010c87fe96', 'medikit', 0),
(483, 'steam:11000010c87fe96', 'vodka', 0),
(484, 'steam:11000010c87fe96', 'sportlunch', 0),
(485, 'steam:11000010c87fe96', 'alive_chicken', 0),
(486, 'steam:11000010c87fe96', 'tacos', 0),
(487, 'steam:11000010c87fe96', 'burger', 0),
(488, 'steam:11000010c87fe96', 'meat', 0),
(489, 'steam:11000010c87fe96', 'blowpipe', 0),
(490, 'steam:11000010c87fe96', 'vegetables', 0),
(491, 'steam:11000010c87fe96', 'nitro', 0),
(492, 'steam:11000010c87fe96', 'flashlight', 0),
(493, 'steam:11000010c87fe96', 'pearl_pooch', 0),
(494, 'steam:11000010c87fe96', 'golem', 0),
(495, 'steam:11000010c87fe96', 'bolpistache', 0),
(496, 'steam:11000010c87fe96', 'packaged_chicken', 0),
(497, 'steam:11000010c87fe96', 'lsd', 0),
(498, 'steam:11000010c87fe96', 'menthe', 0),
(499, 'steam:11000010c87fe96', 'mixapero', 0),
(500, 'steam:11000010c87fe96', 'coke', 0),
(501, 'steam:11000010c87fe96', 'turtle', 0),
(502, 'steam:11000010c87fe96', 'rhumfruit', 0),
(503, 'steam:11000010c87fe96', 'rhumcoca', 0),
(504, 'steam:11000010c87fe96', 'cola', 0),
(505, 'steam:11000010c87fe96', 'weed_pooch', 0),
(506, 'steam:11000010c87fe96', 'whisky', 0),
(507, 'steam:11000010c87fe96', 'whool', 0),
(508, 'steam:11000010c87fe96', 'carokit', 0),
(509, 'steam:11000010c87fe96', 'icetea', 0),
(510, 'steam:11000010c87fe96', 'whiskycoca', 0),
(511, 'steam:11000010c87fe96', 'metreshooter', 0),
(512, 'steam:11000010c87fe96', 'saucisson', 0),
(513, 'steam:11000010c87fe96', 'ice', 0),
(514, 'steam:11000010c87fe96', 'mojito', 0),
(515, 'steam:11000010c87fe96', 'bread', 0),
(516, 'steam:11000010c87fe96', 'turtle_pooch', 0),
(517, 'steam:11000010c87fe96', 'fish', 0),
(518, 'steam:11000010c87fe96', 'vodkafruit', 0),
(519, 'steam:11000010c87fe96', 'beer', 0),
(520, 'steam:11000010c87fe96', 'jager', 0),
(521, 'steam:11000010c87fe96', 'opium', 0),
(522, 'steam:11000010c87fe96', 'vodkaenergy', 0),
(523, 'steam:11000010c87fe96', 'jagerbomb', 0),
(524, 'steam:11000010c87fe96', 'carotool', 0),
(525, 'steam:11000010c87fe96', 'martini', 0),
(526, 'steam:11000010c87fe96', 'pearl', 0),
(527, 'steam:11000010c87fe96', 'weed', 0),
(528, 'steam:11000010c87fe96', 'washed_stone', 0),
(529, 'steam:11000010c87fe96', 'gazbottle', 0),
(530, 'steam:11000010c87fe96', 'fixtool', 0),
(531, 'steam:11000010c87fe96', 'fabric', 0),
(532, 'steam:11000010c87fe96', 'cutted_wood', 0),
(533, 'steam:11000010c87fe96', 'bolcacahuetes', 0),
(534, 'steam:11000010c87fe96', 'copper', 0),
(535, 'steam:11000010c87fe96', 'energy', 0),
(536, 'steam:11000010c87fe96', 'diamond', 0),
(537, 'steam:11000010c87fe96', 'bolnoixcajou', 0),
(538, 'steam:11000010c87fe96', 'clothe', 0),
(539, 'steam:11000010c87fe96', 'meth', 0),
(540, 'steam:11000010c87fe96', 'drpepper', 0),
(541, 'steam:1100001372437de', 'bandage', 0),
(542, 'steam:1100001372437de', 'opium_pooch', 0),
(543, 'steam:1100001372437de', 'lsd_pooch', 0),
(544, 'steam:1100001372437de', 'litter_pooch', 0),
(545, 'steam:1100001372437de', 'rhumcoca', 0),
(546, 'steam:1100001372437de', 'water', 0),
(547, 'steam:1100001372437de', 'sportlunch', 0),
(548, 'steam:1100001372437de', 'meth', 0),
(549, 'steam:1100001372437de', 'contrat', 0),
(550, 'steam:1100001372437de', 'powerade', 0),
(551, 'steam:1100001372437de', 'weed', 0),
(552, 'steam:1100001372437de', 'cigarett', 0),
(553, 'steam:1100001372437de', 'pills', 0),
(554, 'steam:1100001372437de', 'clothe', 0),
(555, 'steam:1100001372437de', 'packaged_chicken', 0),
(556, 'steam:1100001372437de', 'fixkit', 0),
(557, 'steam:1100001372437de', 'burger', 0),
(558, 'steam:1100001372437de', 'leather', 0),
(559, 'steam:1100001372437de', 'scratchoff_used', 0),
(560, 'steam:1100001372437de', 'whisky', 0),
(561, 'steam:1100001372437de', 'washed_stone', 0),
(562, 'steam:1100001372437de', 'teqpaf', 0),
(563, 'steam:1100001372437de', 'petrol_raffin', 0),
(564, 'steam:1100001372437de', 'golem', 0),
(565, 'steam:1100001372437de', 'clip', 0),
(566, 'steam:1100001372437de', 'fish', 0),
(567, 'steam:1100001372437de', 'menthe', 0),
(568, 'steam:1100001372437de', 'yusuf', 0),
(569, 'steam:1100001372437de', 'slaughtered_chicken', 0),
(570, 'steam:1100001372437de', 'petrol', 0),
(571, 'steam:1100001372437de', 'plongee2', 0),
(572, 'steam:1100001372437de', 'bread', 0),
(573, 'steam:1100001372437de', 'packaged_plank', 0),
(574, 'steam:1100001372437de', 'jager', 0),
(575, 'steam:1100001372437de', 'croquettes', 0),
(576, 'steam:1100001372437de', 'vodka', 0),
(577, 'steam:1100001372437de', 'rhumfruit', 0),
(578, 'steam:1100001372437de', 'essence', 0),
(579, 'steam:1100001372437de', 'tequila', 0),
(580, 'steam:1100001372437de', 'jusfruit', 0),
(581, 'steam:1100001372437de', 'plongee1', 0),
(582, 'steam:1100001372437de', 'nitro', 0),
(583, 'steam:1100001372437de', 'flashlight', 0),
(584, 'steam:1100001372437de', 'whiskycoca', 0),
(585, 'steam:1100001372437de', 'bolchips', 0),
(586, 'steam:1100001372437de', 'fixtool', 0),
(587, 'steam:1100001372437de', 'protein_shake', 0),
(588, 'steam:1100001372437de', 'medikit', 0),
(589, 'steam:1100001372437de', 'donut', 0),
(590, 'steam:1100001372437de', 'binoculars', 0),
(591, 'steam:1100001372437de', 'limonade', 0),
(592, 'steam:1100001372437de', 'gym_membership', 0),
(593, 'steam:1100001372437de', 'lighter', 0),
(594, 'steam:1100001372437de', 'energy', 0),
(595, 'steam:1100001372437de', 'gazbottle', 0),
(596, 'steam:1100001372437de', 'bolpistache', 0),
(597, 'steam:1100001372437de', 'tacos', 0),
(598, 'steam:1100001372437de', 'silencieux', 0),
(599, 'steam:1100001372437de', 'meat', 0),
(600, 'steam:1100001372437de', 'vodkaenergy', 0),
(601, 'steam:1100001372437de', 'vegetables', 0),
(602, 'steam:1100001372437de', 'pearl_pooch', 0),
(603, 'steam:1100001372437de', 'pearl', 0),
(604, 'steam:1100001372437de', 'carotool', 0),
(605, 'steam:1100001372437de', 'lsd', 0),
(606, 'steam:1100001372437de', 'coke_pooch', 0),
(607, 'steam:1100001372437de', 'vodkafruit', 0),
(608, 'steam:1100001372437de', 'turtle', 0),
(609, 'steam:1100001372437de', 'turtle_pooch', 0),
(610, 'steam:1100001372437de', 'saucisson', 0),
(611, 'steam:1100001372437de', 'metreshooter', 0),
(612, 'steam:1100001372437de', 'grapperaisin', 0),
(613, 'steam:1100001372437de', 'ice', 0),
(614, 'steam:1100001372437de', 'mixapero', 0),
(615, 'steam:1100001372437de', 'bolcacahuetes', 0),
(616, 'steam:1100001372437de', 'cutted_wood', 0),
(617, 'steam:1100001372437de', 'scratchoff', 0),
(618, 'steam:1100001372437de', 'opium', 0),
(619, 'steam:1100001372437de', 'cola', 0),
(620, 'steam:1100001372437de', 'diamond', 0),
(621, 'steam:1100001372437de', 'mojito', 0),
(622, 'steam:1100001372437de', 'fabric', 0),
(623, 'steam:1100001372437de', 'litter', 0),
(624, 'steam:1100001372437de', 'coffee', 0),
(625, 'steam:1100001372437de', 'lockpick', 0),
(626, 'steam:1100001372437de', 'gold', 0),
(627, 'steam:1100001372437de', 'icetea', 0),
(628, 'steam:1100001372437de', 'jagerbomb', 0),
(629, 'steam:1100001372437de', 'wood', 0),
(630, 'steam:1100001372437de', 'blackberry', 0),
(631, 'steam:1100001372437de', 'grip', 0),
(632, 'steam:1100001372437de', 'drpepper', 0),
(633, 'steam:1100001372437de', 'soda', 0),
(634, 'steam:1100001372437de', 'coke', 0),
(635, 'steam:1100001372437de', 'meth_pooch', 0),
(636, 'steam:1100001372437de', 'alive_chicken', 0),
(637, 'steam:1100001372437de', 'martini', 0),
(638, 'steam:1100001372437de', 'carokit', 0),
(639, 'steam:1100001372437de', 'iron', 0),
(640, 'steam:1100001372437de', 'rhum', 0),
(641, 'steam:1100001372437de', 'copper', 0),
(642, 'steam:1100001372437de', 'bolnoixcajou', 0),
(643, 'steam:1100001372437de', 'armor', 0),
(644, 'steam:1100001372437de', 'blowpipe', 0),
(645, 'steam:1100001372437de', 'beer', 0),
(646, 'steam:1100001372437de', 'weed_pooch', 0),
(647, 'steam:1100001372437de', 'stone', 0),
(648, 'steam:1100001372437de', 'whool', 0),
(649, 'steam:110000112969e8f', 'wood', 0),
(650, 'steam:110000112969e8f', 'weed', 0),
(651, 'steam:110000112969e8f', 'lsd', 0),
(652, 'steam:110000112969e8f', 'medikit', 0),
(653, 'steam:110000112969e8f', 'blowpipe', 0),
(654, 'steam:110000112969e8f', 'rhum', 0),
(655, 'steam:110000112969e8f', 'whool', 0),
(656, 'steam:110000112969e8f', 'gold', 0),
(657, 'steam:110000112969e8f', 'teqpaf', 0),
(658, 'steam:110000112969e8f', 'fixkit', 0),
(659, 'steam:110000112969e8f', 'bandage', 0),
(660, 'steam:110000112969e8f', 'soda', 0),
(661, 'steam:110000112969e8f', 'turtle_pooch', 0),
(662, 'steam:110000112969e8f', 'energy', 0),
(663, 'steam:110000112969e8f', 'opium_pooch', 0),
(664, 'steam:110000112969e8f', 'rhumfruit', 0),
(665, 'steam:110000112969e8f', 'slaughtered_chicken', 0),
(666, 'steam:110000112969e8f', 'copper', 0),
(667, 'steam:110000112969e8f', 'protein_shake', 0),
(668, 'steam:110000112969e8f', 'donut', 0),
(669, 'steam:110000112969e8f', 'plongee2', 0),
(670, 'steam:110000112969e8f', 'binoculars', 0),
(671, 'steam:110000112969e8f', 'meat', 0),
(672, 'steam:110000112969e8f', 'martini', 0),
(673, 'steam:110000112969e8f', 'petrol_raffin', 0),
(674, 'steam:110000112969e8f', 'pearl', 0),
(675, 'steam:110000112969e8f', 'meth_pooch', 0),
(676, 'steam:110000112969e8f', 'whisky', 0),
(677, 'steam:110000112969e8f', 'fixtool', 0),
(678, 'steam:110000112969e8f', 'jagerbomb', 0),
(679, 'steam:110000112969e8f', 'stone', 0),
(680, 'steam:110000112969e8f', 'packaged_plank', 0),
(681, 'steam:110000112969e8f', 'fabric', 0),
(682, 'steam:110000112969e8f', 'lighter', 0),
(683, 'steam:110000112969e8f', 'drpepper', 0),
(684, 'steam:110000112969e8f', 'weed_pooch', 0),
(685, 'steam:110000112969e8f', 'armor', 0),
(686, 'steam:110000112969e8f', 'litter_pooch', 0),
(687, 'steam:110000112969e8f', 'coffee', 0),
(688, 'steam:110000112969e8f', 'vodkaenergy', 0),
(689, 'steam:110000112969e8f', 'mojito', 0),
(690, 'steam:110000112969e8f', 'scratchoff', 0),
(691, 'steam:110000112969e8f', 'plongee1', 0),
(692, 'steam:110000112969e8f', 'leather', 0),
(693, 'steam:110000112969e8f', 'scratchoff_used', 0),
(694, 'steam:110000112969e8f', 'powerade', 0),
(695, 'steam:110000112969e8f', 'tequila', 0),
(696, 'steam:110000112969e8f', 'gym_membership', 0),
(697, 'steam:110000112969e8f', 'silencieux', 0),
(698, 'steam:110000112969e8f', 'sportlunch', 0),
(699, 'steam:110000112969e8f', 'cutted_wood', 0),
(700, 'steam:110000112969e8f', 'cigarett', 0),
(701, 'steam:110000112969e8f', 'lockpick', 0),
(702, 'steam:110000112969e8f', 'carokit', 0),
(703, 'steam:110000112969e8f', 'contrat', 0),
(704, 'steam:110000112969e8f', 'bolpistache', 0),
(705, 'steam:110000112969e8f', 'packaged_chicken', 0),
(706, 'steam:110000112969e8f', 'grip', 0),
(707, 'steam:110000112969e8f', 'jager', 0),
(708, 'steam:110000112969e8f', 'clothe', 0),
(709, 'steam:110000112969e8f', 'golem', 0),
(710, 'steam:110000112969e8f', 'limonade', 0),
(711, 'steam:110000112969e8f', 'essence', 0),
(712, 'steam:110000112969e8f', 'burger', 0),
(713, 'steam:110000112969e8f', 'icetea', 0),
(714, 'steam:110000112969e8f', 'tacos', 0),
(715, 'steam:110000112969e8f', 'coke_pooch', 0),
(716, 'steam:110000112969e8f', 'mixapero', 0),
(717, 'steam:110000112969e8f', 'vegetables', 0),
(718, 'steam:110000112969e8f', 'pearl_pooch', 0),
(719, 'steam:110000112969e8f', 'vodkafruit', 0),
(720, 'steam:110000112969e8f', 'nitro', 0),
(721, 'steam:110000112969e8f', 'litter', 0),
(722, 'steam:110000112969e8f', 'lsd_pooch', 0),
(723, 'steam:110000112969e8f', 'gazbottle', 0),
(724, 'steam:110000112969e8f', 'turtle', 0),
(725, 'steam:110000112969e8f', 'cola', 0),
(726, 'steam:110000112969e8f', 'metreshooter', 0),
(727, 'steam:110000112969e8f', 'menthe', 0),
(728, 'steam:110000112969e8f', 'saucisson', 0),
(729, 'steam:110000112969e8f', 'bolcacahuetes', 0),
(730, 'steam:110000112969e8f', 'yusuf', 0),
(731, 'steam:110000112969e8f', 'pills', 0),
(732, 'steam:110000112969e8f', 'bread', 6),
(733, 'steam:110000112969e8f', 'iron', 0),
(734, 'steam:110000112969e8f', 'coke', 0),
(735, 'steam:110000112969e8f', 'jusfruit', 0),
(736, 'steam:110000112969e8f', 'alive_chicken', 0),
(737, 'steam:110000112969e8f', 'meth', 0),
(738, 'steam:110000112969e8f', 'whiskycoca', 0),
(739, 'steam:110000112969e8f', 'ice', 0),
(740, 'steam:110000112969e8f', 'clip', 0),
(741, 'steam:110000112969e8f', 'water', 0),
(742, 'steam:110000112969e8f', 'croquettes', 0),
(743, 'steam:110000112969e8f', 'grapperaisin', 0),
(744, 'steam:110000112969e8f', 'washed_stone', 0),
(745, 'steam:110000112969e8f', 'bolchips', 0),
(746, 'steam:110000112969e8f', 'carotool', 0),
(747, 'steam:110000112969e8f', 'opium', 0),
(748, 'steam:110000112969e8f', 'rhumcoca', 0),
(749, 'steam:110000112969e8f', 'beer', 0),
(750, 'steam:110000112969e8f', 'flashlight', 0),
(751, 'steam:110000112969e8f', 'diamond', 0),
(752, 'steam:110000112969e8f', 'bolnoixcajou', 0),
(753, 'steam:110000112969e8f', 'vodka', 0),
(754, 'steam:110000112969e8f', 'blackberry', 0),
(755, 'steam:110000112969e8f', 'fish', 0),
(756, 'steam:110000112969e8f', 'petrol', 0),
(757, 'steam:11000010a01bdb9', 'pcp', 0),
(758, 'steam:11000010a01bdb9', 'fakepee', 0),
(759, 'steam:11000010a01bdb9', 'painkiller', 0),
(760, 'steam:11000010a01bdb9', 'narcan', 0),
(761, 'steam:11000010a01bdb9', 'dabs', 0),
(762, 'steam:11000010a01bdb9', 'whiskey', 0),
(763, 'steam:11000010a01bdb9', 'crack', 0),
(764, 'steam:11000010a01bdb9', 'drugtest', 0),
(765, 'steam:11000010a01bdb9', 'coca', -1),
(766, 'steam:11000010a01bdb9', 'heroine', 0),
(767, 'steam:11000010a01bdb9', 'poppy', 0),
(768, 'steam:11000010a01bdb9', 'cocaine', 0),
(769, 'steam:11000010a01bdb9', 'ephedra', 0),
(770, 'steam:11000010a01bdb9', 'cannabis', 0),
(771, 'steam:11000010a01bdb9', 'marijuana', 0),
(772, 'steam:11000010a01bdb9', 'ephedrine', 0),
(773, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(774, 'steam:110000132580eb0', 'coca', 0),
(775, 'steam:110000132580eb0', 'painkiller', 0),
(776, 'steam:110000132580eb0', 'narcan', 0),
(777, 'steam:110000132580eb0', 'heroine', 0),
(778, 'steam:110000132580eb0', 'dabs', 0),
(779, 'steam:110000132580eb0', 'whiskey', 0),
(780, 'steam:110000132580eb0', 'crack', 0),
(781, 'steam:110000132580eb0', 'pcp', 0),
(782, 'steam:110000132580eb0', 'ephedra', 0),
(783, 'steam:110000132580eb0', 'poppy', 0),
(784, 'steam:110000132580eb0', 'cocaine', 0),
(785, 'steam:110000132580eb0', 'cannabis', 0),
(786, 'steam:110000132580eb0', 'ephedrine', 0),
(787, 'steam:110000132580eb0', 'marijuana', 0),
(788, 'steam:110000132580eb0', 'breathalyzer', 0),
(789, 'steam:110000132580eb0', 'drugtest', 0),
(790, 'steam:110000132580eb0', 'fakepee', 0),
(791, 'steam:11000010b15a7d4', 'bread', 0),
(792, 'steam:11000010b15a7d4', 'painkiller', 0),
(793, 'steam:11000010b15a7d4', 'narcan', 0),
(794, 'steam:11000010b15a7d4', 'cola', 0),
(795, 'steam:11000010b15a7d4', 'flashlight', 0),
(796, 'steam:11000010b15a7d4', 'gold', 0),
(797, 'steam:11000010b15a7d4', 'ephedrine', 0),
(798, 'steam:11000010b15a7d4', 'pcp', 0),
(799, 'steam:11000010b15a7d4', 'protein_shake', 0),
(800, 'steam:11000010b15a7d4', 'coca', 0),
(801, 'steam:11000010b15a7d4', 'drugtest', 0),
(802, 'steam:11000010b15a7d4', 'armor', 0),
(803, 'steam:11000010b15a7d4', 'leather', 0),
(804, 'steam:11000010b15a7d4', 'packaged_plank', 0),
(805, 'steam:11000010b15a7d4', 'grip', 0),
(806, 'steam:11000010b15a7d4', 'sportlunch', 0),
(807, 'steam:11000010b15a7d4', 'scratchoff_used', 0),
(808, 'steam:11000010b15a7d4', 'whisky', 0),
(809, 'steam:11000010b15a7d4', 'carokit', 0),
(810, 'steam:11000010b15a7d4', 'whiskey', 0),
(811, 'steam:11000010b15a7d4', 'copper', 0),
(812, 'steam:11000010b15a7d4', 'menthe', 0),
(813, 'steam:11000010b15a7d4', 'metreshooter', 0),
(814, 'steam:11000010b15a7d4', 'iron', 0),
(815, 'steam:11000010b15a7d4', 'saucisson', 0),
(816, 'steam:11000010b15a7d4', 'ephedra', 0),
(817, 'steam:11000010b15a7d4', 'icetea', 0),
(818, 'steam:11000010b15a7d4', 'fakepee', 0),
(819, 'steam:11000010b15a7d4', 'breathalyzer', 0),
(820, 'steam:11000010b15a7d4', 'pearl', 0),
(821, 'steam:11000010b15a7d4', 'marijuana', 0),
(822, 'steam:11000010b15a7d4', 'rhumcoca', 0),
(823, 'steam:11000010b15a7d4', 'donut', 0),
(824, 'steam:11000010b15a7d4', 'jager', 0),
(825, 'steam:11000010b15a7d4', 'vodkaenergy', 0),
(826, 'steam:11000010b15a7d4', 'fixkit', 0),
(827, 'steam:11000010b15a7d4', 'blackberry', 0),
(828, 'steam:11000010b15a7d4', 'bolchips', 0),
(829, 'steam:11000010b15a7d4', 'powerade', 0),
(830, 'steam:11000010b15a7d4', 'tacos', 0),
(831, 'steam:11000010b15a7d4', 'plongee1', 0),
(832, 'steam:11000010b15a7d4', 'cannabis', 0),
(833, 'steam:11000010b15a7d4', 'meth_pooch', 0),
(834, 'steam:11000010b15a7d4', 'cocaine', 0),
(835, 'steam:11000010b15a7d4', 'cigarett', 0),
(836, 'steam:11000010b15a7d4', 'lighter', 0),
(837, 'steam:11000010b15a7d4', 'vegetables', 0),
(838, 'steam:11000010b15a7d4', 'crack', 0),
(839, 'steam:11000010b15a7d4', 'contrat', 0),
(840, 'steam:11000010b15a7d4', 'meat', 0),
(841, 'steam:11000010b15a7d4', 'yusuf', 0),
(842, 'steam:11000010b15a7d4', 'croquettes', 0),
(843, 'steam:11000010b15a7d4', 'soda', 0),
(844, 'steam:11000010b15a7d4', 'whool', 0),
(845, 'steam:11000010b15a7d4', 'slaughtered_chicken', 0),
(846, 'steam:11000010b15a7d4', 'diamond', 0),
(847, 'steam:11000010b15a7d4', 'gym_membership', 0),
(848, 'steam:11000010b15a7d4', 'plongee2', 0),
(849, 'steam:11000010b15a7d4', 'silencieux', 0),
(850, 'steam:11000010b15a7d4', 'dabs', 0),
(851, 'steam:11000010b15a7d4', 'poppy', 0),
(852, 'steam:11000010b15a7d4', 'jagerbomb', 0),
(853, 'steam:11000010b15a7d4', 'essence', 0),
(854, 'steam:11000010b15a7d4', 'bandage', 0),
(855, 'steam:11000010b15a7d4', 'binoculars', 0),
(856, 'steam:11000010b15a7d4', 'opium_pooch', 0),
(857, 'steam:11000010b15a7d4', 'lsd_pooch', 0),
(858, 'steam:11000010b15a7d4', 'rhum', 0),
(859, 'steam:11000010b15a7d4', 'stone', 0),
(860, 'steam:11000010b15a7d4', 'clothe', 0),
(861, 'steam:11000010b15a7d4', 'litter', 0),
(862, 'steam:11000010b15a7d4', 'clip', 0),
(863, 'steam:11000010b15a7d4', 'rhumfruit', 0),
(864, 'steam:11000010b15a7d4', 'pearl_pooch', 0),
(865, 'steam:11000010b15a7d4', 'meth', 0),
(866, 'steam:11000010b15a7d4', 'lsd', 0),
(867, 'steam:11000010b15a7d4', 'mixapero', 0),
(868, 'steam:11000010b15a7d4', 'nitro', 0),
(869, 'steam:11000010b15a7d4', 'teqpaf', 0),
(870, 'steam:11000010b15a7d4', 'beer', 0),
(871, 'steam:11000010b15a7d4', 'limonade', 0),
(872, 'steam:11000010b15a7d4', 'vodka', 0),
(873, 'steam:11000010b15a7d4', 'pills', 0),
(874, 'steam:11000010b15a7d4', 'burger', 0),
(875, 'steam:11000010b15a7d4', 'mojito', 0),
(876, 'steam:11000010b15a7d4', 'fish', 0),
(877, 'steam:11000010b15a7d4', 'washed_stone', 0),
(878, 'steam:11000010b15a7d4', 'martini', 0),
(879, 'steam:11000010b15a7d4', 'weed_pooch', 0),
(880, 'steam:11000010b15a7d4', 'turtle_pooch', 0),
(881, 'steam:11000010b15a7d4', 'bolpistache', 0),
(882, 'steam:11000010b15a7d4', 'scratchoff', 0),
(883, 'steam:11000010b15a7d4', 'alive_chicken', 0),
(884, 'steam:11000010b15a7d4', 'medikit', 0),
(885, 'steam:11000010b15a7d4', 'gazbottle', 0),
(886, 'steam:11000010b15a7d4', 'blowpipe', 0),
(887, 'steam:11000010b15a7d4', 'wood', 0),
(888, 'steam:11000010b15a7d4', 'petrol_raffin', 0),
(889, 'steam:11000010b15a7d4', 'grapperaisin', 0),
(890, 'steam:11000010b15a7d4', 'ice', 0),
(891, 'steam:11000010b15a7d4', 'water', 0),
(892, 'steam:11000010b15a7d4', 'coffee', 0),
(893, 'steam:11000010b15a7d4', 'fabric', 0),
(894, 'steam:11000010b15a7d4', 'bolcacahuetes', 0),
(895, 'steam:11000010b15a7d4', 'energy', 0),
(896, 'steam:11000010b15a7d4', 'turtle', 0),
(897, 'steam:11000010b15a7d4', 'bolnoixcajou', 0),
(898, 'steam:11000010b15a7d4', 'tequila', 0),
(899, 'steam:11000010b15a7d4', 'whiskycoca', 0),
(900, 'steam:11000010b15a7d4', 'weed', 0),
(901, 'steam:11000010b15a7d4', 'golem', 0),
(902, 'steam:11000010b15a7d4', 'carotool', 0),
(903, 'steam:11000010b15a7d4', 'fixtool', 0),
(904, 'steam:11000010b15a7d4', 'opium', 0),
(905, 'steam:11000010b15a7d4', 'vodkafruit', 0),
(906, 'steam:11000010b15a7d4', 'coke', 0),
(907, 'steam:11000010b15a7d4', 'coke_pooch', 0),
(908, 'steam:11000010b15a7d4', 'heroine', 0),
(909, 'steam:11000010b15a7d4', 'drpepper', 0),
(910, 'steam:11000010b15a7d4', 'cutted_wood', 0),
(911, 'steam:11000010b15a7d4', 'litter_pooch', 0),
(912, 'steam:11000010b15a7d4', 'packaged_chicken', 0),
(913, 'steam:11000010b15a7d4', 'lockpick', 0),
(914, 'steam:11000010b15a7d4', 'jusfruit', 0),
(915, 'steam:11000010b15a7d4', 'petrol', 0),
(916, 'steam:1100001068ef13c', 'fakepee', 0),
(917, 'steam:1100001068ef13c', 'breathalyzer', 0),
(918, 'steam:1100001068ef13c', 'painkiller', 0),
(919, 'steam:1100001068ef13c', 'narcan', 0),
(920, 'steam:1100001068ef13c', 'drugtest', 0),
(921, 'steam:1100001068ef13c', 'marijuana', 0),
(922, 'steam:1100001068ef13c', 'whiskey', 0),
(923, 'steam:1100001068ef13c', 'heroine', 0),
(924, 'steam:1100001068ef13c', 'poppy', 0),
(925, 'steam:1100001068ef13c', 'coca', 0),
(926, 'steam:1100001068ef13c', 'ephedrine', 0),
(927, 'steam:1100001068ef13c', 'pcp', 0),
(928, 'steam:1100001068ef13c', 'cannabis', 0),
(929, 'steam:1100001068ef13c', 'cocaine', 0),
(930, 'steam:1100001068ef13c', 'ephedra', 0),
(931, 'steam:1100001068ef13c', 'crack', 0),
(932, 'steam:1100001068ef13c', 'dabs', 0),
(933, 'steam:110000136d9eea0', 'washed_stone', 0),
(934, 'steam:110000136d9eea0', 'pearl_pooch', 0),
(935, 'steam:110000136d9eea0', 'jagerbomb', 0),
(936, 'steam:110000136d9eea0', 'pearl', 0),
(937, 'steam:110000136d9eea0', 'mojito', 0),
(938, 'steam:110000136d9eea0', 'meth_pooch', 0),
(939, 'steam:110000136d9eea0', 'coffee', 0),
(940, 'steam:110000136d9eea0', 'opium_pooch', 0),
(941, 'steam:110000136d9eea0', 'drpepper', 0),
(942, 'steam:110000136d9eea0', 'cannabis', 0),
(943, 'steam:110000136d9eea0', 'marijuana', 0),
(944, 'steam:110000136d9eea0', 'painkiller', 0),
(945, 'steam:110000136d9eea0', 'drugtest', 0),
(946, 'steam:110000136d9eea0', 'whiskycoca', 0),
(947, 'steam:110000136d9eea0', 'carokit', 0),
(948, 'steam:110000136d9eea0', 'tequila', 0),
(949, 'steam:110000136d9eea0', 'protein_shake', 0),
(950, 'steam:110000136d9eea0', 'fakepee', 0),
(951, 'steam:110000136d9eea0', 'turtle_pooch', 0),
(952, 'steam:110000136d9eea0', 'clip', 0),
(953, 'steam:110000136d9eea0', 'dabs', 0),
(954, 'steam:110000136d9eea0', 'bolcacahuetes', 0),
(955, 'steam:110000136d9eea0', 'cocaine', 0),
(956, 'steam:110000136d9eea0', 'petrol_raffin', 0),
(957, 'steam:110000136d9eea0', 'whisky', 0),
(958, 'steam:110000136d9eea0', 'litter', 0),
(959, 'steam:110000136d9eea0', 'crack', 0),
(960, 'steam:110000136d9eea0', 'gold', 0),
(961, 'steam:110000136d9eea0', 'lockpick', 0),
(962, 'steam:110000136d9eea0', 'gazbottle', 0),
(963, 'steam:110000136d9eea0', 'whiskey', 0),
(964, 'steam:110000136d9eea0', 'fixtool', 0),
(965, 'steam:110000136d9eea0', 'medikit', 0),
(966, 'steam:110000136d9eea0', 'fabric', 0),
(967, 'steam:110000136d9eea0', 'burger', 0),
(968, 'steam:110000136d9eea0', 'poppy', 0),
(969, 'steam:110000136d9eea0', 'vodkaenergy', 0),
(970, 'steam:110000136d9eea0', 'ephedrine', 0),
(971, 'steam:110000136d9eea0', 'slaughtered_chicken', 0),
(972, 'steam:110000136d9eea0', 'energy', 0),
(973, 'steam:110000136d9eea0', 'essence', 0),
(974, 'steam:110000136d9eea0', 'blowpipe', 0),
(975, 'steam:110000136d9eea0', 'ephedra', 0),
(976, 'steam:110000136d9eea0', 'clothe', 0),
(977, 'steam:110000136d9eea0', 'narcan', 0),
(978, 'steam:110000136d9eea0', 'scratchoff_used', 0),
(979, 'steam:110000136d9eea0', 'leather', 0),
(980, 'steam:110000136d9eea0', 'scratchoff', 0),
(981, 'steam:110000136d9eea0', 'grapperaisin', 0),
(982, 'steam:110000136d9eea0', 'jager', 0),
(983, 'steam:110000136d9eea0', 'weed', 0),
(984, 'steam:110000136d9eea0', 'armor', 0),
(985, 'steam:110000136d9eea0', 'binoculars', 0),
(986, 'steam:110000136d9eea0', 'sportlunch', 0),
(987, 'steam:110000136d9eea0', 'contrat', 0),
(988, 'steam:110000136d9eea0', 'plongee1', 0),
(989, 'steam:110000136d9eea0', 'pcp', 0),
(990, 'steam:110000136d9eea0', 'copper', 0),
(991, 'steam:110000136d9eea0', 'powerade', 0),
(992, 'steam:110000136d9eea0', 'packaged_plank', 0),
(993, 'steam:110000136d9eea0', 'gym_membership', 0),
(994, 'steam:110000136d9eea0', 'plongee2', 0),
(995, 'steam:110000136d9eea0', 'turtle', 0),
(996, 'steam:110000136d9eea0', 'lighter', 0),
(997, 'steam:110000136d9eea0', 'cigarett', 0),
(998, 'steam:110000136d9eea0', 'fish', 0),
(999, 'steam:110000136d9eea0', 'icetea', 0),
(1000, 'steam:110000136d9eea0', 'mixapero', 0),
(1001, 'steam:110000136d9eea0', 'tacos', 0),
(1002, 'steam:110000136d9eea0', 'donut', 0),
(1003, 'steam:110000136d9eea0', 'coke_pooch', 0),
(1004, 'steam:110000136d9eea0', 'blackberry', 0),
(1005, 'steam:110000136d9eea0', 'croquettes', 0),
(1006, 'steam:110000136d9eea0', 'silencieux', 0),
(1007, 'steam:110000136d9eea0', 'yusuf', 0),
(1008, 'steam:110000136d9eea0', 'stone', 0),
(1009, 'steam:110000136d9eea0', 'rhumfruit', 0),
(1010, 'steam:110000136d9eea0', 'meth', 0),
(1011, 'steam:110000136d9eea0', 'flashlight', 0),
(1012, 'steam:110000136d9eea0', 'fixkit', 0),
(1013, 'steam:110000136d9eea0', 'petrol', 0),
(1014, 'steam:110000136d9eea0', 'golem', 0),
(1015, 'steam:110000136d9eea0', 'martini', 0),
(1016, 'steam:110000136d9eea0', 'heroine', 0),
(1017, 'steam:110000136d9eea0', 'whool', 0),
(1018, 'steam:110000136d9eea0', 'vodkafruit', 0),
(1019, 'steam:110000136d9eea0', 'lsd', 0),
(1020, 'steam:110000136d9eea0', 'bandage', 0),
(1021, 'steam:110000136d9eea0', 'vegetables', 0),
(1022, 'steam:110000136d9eea0', 'menthe', 0),
(1023, 'steam:110000136d9eea0', 'cutted_wood', 0),
(1024, 'steam:110000136d9eea0', 'diamond', 0),
(1025, 'steam:110000136d9eea0', 'bread', 0),
(1026, 'steam:110000136d9eea0', 'limonade', 0),
(1027, 'steam:110000136d9eea0', 'nitro', 0),
(1028, 'steam:110000136d9eea0', 'litter_pooch', 0),
(1029, 'steam:110000136d9eea0', 'opium', 0),
(1030, 'steam:110000136d9eea0', 'carotool', 0),
(1031, 'steam:110000136d9eea0', 'meat', 0),
(1032, 'steam:110000136d9eea0', 'coke', 0),
(1033, 'steam:110000136d9eea0', 'breathalyzer', 0),
(1034, 'steam:110000136d9eea0', 'bolnoixcajou', 0),
(1035, 'steam:110000136d9eea0', 'cola', 0),
(1036, 'steam:110000136d9eea0', 'ice', 0),
(1037, 'steam:110000136d9eea0', 'rhumcoca', 0),
(1038, 'steam:110000136d9eea0', 'teqpaf', 0),
(1039, 'steam:110000136d9eea0', 'lsd_pooch', 0),
(1040, 'steam:110000136d9eea0', 'rhum', 0),
(1041, 'steam:110000136d9eea0', 'bolpistache', 0),
(1042, 'steam:110000136d9eea0', 'jusfruit', 0),
(1043, 'steam:110000136d9eea0', 'metreshooter', 0),
(1044, 'steam:110000136d9eea0', 'saucisson', 0),
(1045, 'steam:110000136d9eea0', 'wood', 0),
(1046, 'steam:110000136d9eea0', 'bolchips', 0),
(1047, 'steam:110000136d9eea0', 'beer', 0),
(1048, 'steam:110000136d9eea0', 'coca', 0),
(1049, 'steam:110000136d9eea0', 'alive_chicken', 0),
(1050, 'steam:110000136d9eea0', 'packaged_chicken', 0),
(1051, 'steam:110000136d9eea0', 'iron', 0),
(1052, 'steam:110000136d9eea0', 'grip', 0),
(1053, 'steam:110000136d9eea0', 'vodka', 0),
(1054, 'steam:110000136d9eea0', 'pills', 0),
(1055, 'steam:110000136d9eea0', 'water', 0),
(1056, 'steam:110000136d9eea0', 'weed_pooch', 0),
(1057, 'steam:110000136d9eea0', 'soda', 0),
(1058, 'steam:110000133989c6d', 'opium', 0),
(1059, 'steam:110000133989c6d', 'fish', 0),
(1060, 'steam:110000133989c6d', 'meth', 0),
(1061, 'steam:110000133989c6d', 'bolnoixcajou', 0),
(1062, 'steam:110000133989c6d', 'narcan', 0),
(1063, 'steam:110000133989c6d', 'turtle', 0),
(1064, 'steam:110000133989c6d', 'cannabis', 0),
(1065, 'steam:110000133989c6d', 'pcp', 0),
(1066, 'steam:110000133989c6d', 'alive_chicken', 0),
(1067, 'steam:110000133989c6d', 'dabs', 0),
(1068, 'steam:110000133989c6d', 'cocaine', 0),
(1069, 'steam:110000133989c6d', 'ephedrine', 0),
(1070, 'steam:110000133989c6d', 'drpepper', 0),
(1071, 'steam:110000133989c6d', 'whiskycoca', 0),
(1072, 'steam:110000133989c6d', 'fakepee', 0),
(1073, 'steam:110000133989c6d', 'petrol_raffin', 0),
(1074, 'steam:110000133989c6d', 'jagerbomb', 0),
(1075, 'steam:110000133989c6d', 'rhumfruit', 0),
(1076, 'steam:110000133989c6d', 'protein_shake', 0),
(1077, 'steam:110000133989c6d', 'burger', 0),
(1078, 'steam:110000133989c6d', 'coke', 0),
(1079, 'steam:110000133989c6d', 'armor', 0),
(1080, 'steam:110000133989c6d', 'limonade', 0),
(1081, 'steam:110000133989c6d', 'contrat', 0),
(1082, 'steam:110000133989c6d', 'cola', 0),
(1083, 'steam:110000133989c6d', 'copper', 0),
(1084, 'steam:110000133989c6d', 'clothe', 0),
(1085, 'steam:110000133989c6d', 'rhumcoca', 0),
(1086, 'steam:110000133989c6d', 'poppy', 0),
(1087, 'steam:110000133989c6d', 'fabric', 0),
(1088, 'steam:110000133989c6d', 'stone', 0),
(1089, 'steam:110000133989c6d', 'carokit', 0),
(1090, 'steam:110000133989c6d', 'jager', 0),
(1091, 'steam:110000133989c6d', 'opium_pooch', 0),
(1092, 'steam:110000133989c6d', 'ephedra', 0),
(1093, 'steam:110000133989c6d', 'binoculars', 0),
(1094, 'steam:110000133989c6d', 'energy', 0),
(1095, 'steam:110000133989c6d', 'slaughtered_chicken', 0),
(1096, 'steam:110000133989c6d', 'tequila', 0),
(1097, 'steam:110000133989c6d', 'pearl', 0),
(1098, 'steam:110000133989c6d', 'clip', 0),
(1099, 'steam:110000133989c6d', 'leather', 0),
(1100, 'steam:110000133989c6d', 'sportlunch', 0),
(1101, 'steam:110000133989c6d', 'coke_pooch', 0),
(1102, 'steam:110000133989c6d', 'coffee', 0),
(1103, 'steam:110000133989c6d', 'golem', 0),
(1104, 'steam:110000133989c6d', 'weed_pooch', 0),
(1105, 'steam:110000133989c6d', 'marijuana', 0),
(1106, 'steam:110000133989c6d', 'tacos', 0),
(1107, 'steam:110000133989c6d', 'crack', 0),
(1108, 'steam:110000133989c6d', 'essence', 0),
(1109, 'steam:110000133989c6d', 'diamond', 0),
(1110, 'steam:110000133989c6d', 'litter_pooch', 0),
(1111, 'steam:110000133989c6d', 'weed', 0),
(1112, 'steam:110000133989c6d', 'bread', 0),
(1113, 'steam:110000133989c6d', 'grip', 0),
(1114, 'steam:110000133989c6d', 'menthe', 0),
(1115, 'steam:110000133989c6d', 'gym_membership', 0),
(1116, 'steam:110000133989c6d', 'teqpaf', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1117, 'steam:110000133989c6d', 'water', 0),
(1118, 'steam:110000133989c6d', 'whiskey', 0),
(1119, 'steam:110000133989c6d', 'soda', 0),
(1120, 'steam:110000133989c6d', 'whisky', 0),
(1121, 'steam:110000133989c6d', 'heroine', 0),
(1122, 'steam:110000133989c6d', 'cigarett', 0),
(1123, 'steam:110000133989c6d', 'lighter', 0),
(1124, 'steam:110000133989c6d', 'iron', 0),
(1125, 'steam:110000133989c6d', 'bolpistache', 0),
(1126, 'steam:110000133989c6d', 'blackberry', 0),
(1127, 'steam:110000133989c6d', 'painkiller', 0),
(1128, 'steam:110000133989c6d', 'coca', 0),
(1129, 'steam:110000133989c6d', 'yusuf', 0),
(1130, 'steam:110000133989c6d', 'flashlight', 0),
(1131, 'steam:110000133989c6d', 'breathalyzer', 0),
(1132, 'steam:110000133989c6d', 'rhum', 0),
(1133, 'steam:110000133989c6d', 'plongee2', 0),
(1134, 'steam:110000133989c6d', 'lsd_pooch', 0),
(1135, 'steam:110000133989c6d', 'meat', 0),
(1136, 'steam:110000133989c6d', 'vegetables', 0),
(1137, 'steam:110000133989c6d', 'nitro', 0),
(1138, 'steam:110000133989c6d', 'turtle_pooch', 0),
(1139, 'steam:110000133989c6d', 'washed_stone', 0),
(1140, 'steam:110000133989c6d', 'croquettes', 0),
(1141, 'steam:110000133989c6d', 'ice', 0),
(1142, 'steam:110000133989c6d', 'grapperaisin', 0),
(1143, 'steam:110000133989c6d', 'bolcacahuetes', 0),
(1144, 'steam:110000133989c6d', 'packaged_plank', 0),
(1145, 'steam:110000133989c6d', 'whool', 0),
(1146, 'steam:110000133989c6d', 'cutted_wood', 0),
(1147, 'steam:110000133989c6d', 'pearl_pooch', 0),
(1148, 'steam:110000133989c6d', 'pills', 0),
(1149, 'steam:110000133989c6d', 'donut', 0),
(1150, 'steam:110000133989c6d', 'beer', 0),
(1151, 'steam:110000133989c6d', 'powerade', 0),
(1152, 'steam:110000133989c6d', 'petrol', 0),
(1153, 'steam:110000133989c6d', 'metreshooter', 0),
(1154, 'steam:110000133989c6d', 'mixapero', 0),
(1155, 'steam:110000133989c6d', 'scratchoff', 0),
(1156, 'steam:110000133989c6d', 'vodkafruit', 0),
(1157, 'steam:110000133989c6d', 'vodkaenergy', 0),
(1158, 'steam:110000133989c6d', 'packaged_chicken', 0),
(1159, 'steam:110000133989c6d', 'wood', 0),
(1160, 'steam:110000133989c6d', 'jusfruit', 0),
(1161, 'steam:110000133989c6d', 'lockpick', 0),
(1162, 'steam:110000133989c6d', 'litter', 0),
(1163, 'steam:110000133989c6d', 'icetea', 0),
(1164, 'steam:110000133989c6d', 'bandage', 0),
(1165, 'steam:110000133989c6d', 'drugtest', 0),
(1166, 'steam:110000133989c6d', 'gazbottle', 0),
(1167, 'steam:110000133989c6d', 'blowpipe', 0),
(1168, 'steam:110000133989c6d', 'lsd', 0),
(1169, 'steam:110000133989c6d', 'saucisson', 0),
(1170, 'steam:110000133989c6d', 'vodka', 0),
(1171, 'steam:110000133989c6d', 'fixtool', 0),
(1172, 'steam:110000133989c6d', 'mojito', 0),
(1173, 'steam:110000133989c6d', 'bolchips', 0),
(1174, 'steam:110000133989c6d', 'martini', 0),
(1175, 'steam:110000133989c6d', 'silencieux', 0),
(1176, 'steam:110000133989c6d', 'plongee1', 0),
(1177, 'steam:110000133989c6d', 'medikit', 0),
(1178, 'steam:110000133989c6d', 'meth_pooch', 0),
(1179, 'steam:110000133989c6d', 'scratchoff_used', 0),
(1180, 'steam:110000133989c6d', 'gold', 0),
(1181, 'steam:110000133989c6d', 'carotool', 0),
(1182, 'steam:110000133989c6d', 'fixkit', 0),
(1183, 'steam:110000112969e8f', 'cocaine', 0),
(1184, 'steam:110000112969e8f', 'heroine', 0),
(1185, 'steam:110000112969e8f', 'painkiller', 0),
(1186, 'steam:110000112969e8f', 'narcan', 0),
(1187, 'steam:110000112969e8f', 'fakepee', 0),
(1188, 'steam:110000112969e8f', 'whiskey', 0),
(1189, 'steam:110000112969e8f', 'breathalyzer', 0),
(1190, 'steam:110000112969e8f', 'poppy', 0),
(1191, 'steam:110000112969e8f', 'ephedrine', 0),
(1192, 'steam:110000112969e8f', 'marijuana', 0),
(1193, 'steam:110000112969e8f', 'cannabis', 0),
(1194, 'steam:110000112969e8f', 'ephedra', 0),
(1195, 'steam:110000112969e8f', 'crack', 0),
(1196, 'steam:110000112969e8f', 'drugtest', 0),
(1197, 'steam:110000112969e8f', 'coca', 0),
(1198, 'steam:110000112969e8f', 'pcp', 0),
(1199, 'steam:110000112969e8f', 'dabs', 0),
(1200, 'steam:11000013ca51b76', 'meth_pooch', 0),
(1201, 'steam:11000013ca51b76', 'bolpistache', 0),
(1202, 'steam:11000013ca51b76', 'fish', 0),
(1203, 'steam:11000013ca51b76', 'narcan', 0),
(1204, 'steam:11000013ca51b76', 'drpepper', 0),
(1205, 'steam:11000013ca51b76', 'painkiller', 0),
(1206, 'steam:11000013ca51b76', 'pearl_pooch', 0),
(1207, 'steam:11000013ca51b76', 'water', 0),
(1208, 'steam:11000013ca51b76', 'cannabis', 0),
(1209, 'steam:11000013ca51b76', 'jusfruit', 0),
(1210, 'steam:11000013ca51b76', 'dabs', 0),
(1211, 'steam:11000013ca51b76', 'nitro', 0),
(1212, 'steam:11000013ca51b76', 'donut', 0),
(1213, 'steam:11000013ca51b76', 'fakepee', 0),
(1214, 'steam:11000013ca51b76', 'grapperaisin', 0),
(1215, 'steam:11000013ca51b76', 'blowpipe', 0),
(1216, 'steam:11000013ca51b76', 'pcp', 0),
(1217, 'steam:11000013ca51b76', 'rhumfruit', 0),
(1218, 'steam:11000013ca51b76', 'whiskycoca', 0),
(1219, 'steam:11000013ca51b76', 'breathalyzer', 0),
(1220, 'steam:11000013ca51b76', 'meth', 0),
(1221, 'steam:11000013ca51b76', 'whiskey', 0),
(1222, 'steam:11000013ca51b76', 'crack', 0),
(1223, 'steam:11000013ca51b76', 'heroine', 0),
(1224, 'steam:11000013ca51b76', 'drugtest', 0),
(1225, 'steam:11000013ca51b76', 'ephedrine', 0),
(1226, 'steam:11000013ca51b76', 'poppy', 0),
(1227, 'steam:11000013ca51b76', 'mixapero', 0),
(1228, 'steam:11000013ca51b76', 'stone', 0),
(1229, 'steam:11000013ca51b76', 'menthe', 0),
(1230, 'steam:11000013ca51b76', 'opium', 0),
(1231, 'steam:11000013ca51b76', 'yusuf', 0),
(1232, 'steam:11000013ca51b76', 'coca', 0),
(1233, 'steam:11000013ca51b76', 'teqpaf', 0),
(1234, 'steam:11000013ca51b76', 'marijuana', 0),
(1235, 'steam:11000013ca51b76', 'golem', 0),
(1236, 'steam:11000013ca51b76', 'bandage', 0),
(1237, 'steam:11000013ca51b76', 'jagerbomb', 0),
(1238, 'steam:11000013ca51b76', 'leather', 0),
(1239, 'steam:11000013ca51b76', 'soda', 0),
(1240, 'steam:11000013ca51b76', 'scratchoff_used', 0),
(1241, 'steam:11000013ca51b76', 'weed_pooch', 0),
(1242, 'steam:11000013ca51b76', 'scratchoff', 0),
(1243, 'steam:11000013ca51b76', 'gazbottle', 0),
(1244, 'steam:11000013ca51b76', 'plongee2', 0),
(1245, 'steam:11000013ca51b76', 'vodkafruit', 0),
(1246, 'steam:11000013ca51b76', 'turtle_pooch', 0),
(1247, 'steam:11000013ca51b76', 'metreshooter', 0),
(1248, 'steam:11000013ca51b76', 'protein_shake', 0),
(1249, 'steam:11000013ca51b76', 'petrol_raffin', 0),
(1250, 'steam:11000013ca51b76', 'fixtool', 0),
(1251, 'steam:11000013ca51b76', 'weed', 0),
(1252, 'steam:11000013ca51b76', 'iron', 0),
(1253, 'steam:11000013ca51b76', 'gold', 0),
(1254, 'steam:11000013ca51b76', 'clothe', 0),
(1255, 'steam:11000013ca51b76', 'saucisson', 0),
(1256, 'steam:11000013ca51b76', 'rhum', 0),
(1257, 'steam:11000013ca51b76', 'lighter', 0),
(1258, 'steam:11000013ca51b76', 'beer', 0),
(1259, 'steam:11000013ca51b76', 'rhumcoca', 0),
(1260, 'steam:11000013ca51b76', 'powerade', 0),
(1261, 'steam:11000013ca51b76', 'vodka', 0),
(1262, 'steam:11000013ca51b76', 'cutted_wood', 0),
(1263, 'steam:11000013ca51b76', 'contrat', 0),
(1264, 'steam:11000013ca51b76', 'turtle', 0),
(1265, 'steam:11000013ca51b76', 'armor', 0),
(1266, 'steam:11000013ca51b76', 'energy', 0),
(1267, 'steam:11000013ca51b76', 'blackberry', 0),
(1268, 'steam:11000013ca51b76', 'bolchips', 0),
(1269, 'steam:11000013ca51b76', 'cigarett', 0),
(1270, 'steam:11000013ca51b76', 'croquettes', 0),
(1271, 'steam:11000013ca51b76', 'binoculars', 0),
(1272, 'steam:11000013ca51b76', 'packaged_chicken', 0),
(1273, 'steam:11000013ca51b76', 'cocaine', 0),
(1274, 'steam:11000013ca51b76', 'fabric', 0),
(1275, 'steam:11000013ca51b76', 'martini', 0),
(1276, 'steam:11000013ca51b76', 'flashlight', 0),
(1277, 'steam:11000013ca51b76', 'silencieux', 0),
(1278, 'steam:11000013ca51b76', 'grip', 0),
(1279, 'steam:11000013ca51b76', 'icetea', 0),
(1280, 'steam:11000013ca51b76', 'burger', 0),
(1281, 'steam:11000013ca51b76', 'diamond', 0),
(1282, 'steam:11000013ca51b76', 'tacos', 0),
(1283, 'steam:11000013ca51b76', 'coke_pooch', 0),
(1284, 'steam:11000013ca51b76', 'meat', 0),
(1285, 'steam:11000013ca51b76', 'litter', 0),
(1286, 'steam:11000013ca51b76', 'coffee', 0),
(1287, 'steam:11000013ca51b76', 'litter_pooch', 0),
(1288, 'steam:11000013ca51b76', 'tequila', 0),
(1289, 'steam:11000013ca51b76', 'fixkit', 0),
(1290, 'steam:11000013ca51b76', 'pills', 0),
(1291, 'steam:11000013ca51b76', 'slaughtered_chicken', 0),
(1292, 'steam:11000013ca51b76', 'pearl', 0),
(1293, 'steam:11000013ca51b76', 'whool', 0),
(1294, 'steam:11000013ca51b76', 'petrol', 0),
(1295, 'steam:11000013ca51b76', 'lsd', 0),
(1296, 'steam:11000013ca51b76', 'copper', 0),
(1297, 'steam:11000013ca51b76', 'lsd_pooch', 0),
(1298, 'steam:11000013ca51b76', 'bolcacahuetes', 0),
(1299, 'steam:11000013ca51b76', 'ephedra', 0),
(1300, 'steam:11000013ca51b76', 'ice', 0),
(1301, 'steam:11000013ca51b76', 'opium_pooch', 0),
(1302, 'steam:11000013ca51b76', 'medikit', 0),
(1303, 'steam:11000013ca51b76', 'essence', 0),
(1304, 'steam:11000013ca51b76', 'gym_membership', 0),
(1305, 'steam:11000013ca51b76', 'plongee1', 0),
(1306, 'steam:11000013ca51b76', 'limonade', 0),
(1307, 'steam:11000013ca51b76', 'coke', 0),
(1308, 'steam:11000013ca51b76', 'packaged_plank', 0),
(1309, 'steam:11000013ca51b76', 'sportlunch', 0),
(1310, 'steam:11000013ca51b76', 'mojito', 0),
(1311, 'steam:11000013ca51b76', 'clip', 0),
(1312, 'steam:11000013ca51b76', 'bolnoixcajou', 0),
(1313, 'steam:11000013ca51b76', 'wood', 0),
(1314, 'steam:11000013ca51b76', 'vodkaenergy', 0),
(1315, 'steam:11000013ca51b76', 'cola', 0),
(1316, 'steam:11000013ca51b76', 'jager', 0),
(1317, 'steam:11000013ca51b76', 'bread', 0),
(1318, 'steam:11000013ca51b76', 'vegetables', 0),
(1319, 'steam:11000013ca51b76', 'whisky', 0),
(1320, 'steam:11000013ca51b76', 'carotool', 0),
(1321, 'steam:11000013ca51b76', 'washed_stone', 0),
(1322, 'steam:11000013ca51b76', 'lockpick', 0),
(1323, 'steam:11000013ca51b76', 'carokit', 0),
(1324, 'steam:11000013ca51b76', 'alive_chicken', 0),
(1325, 'steam:11000010bf29a8c', 'packaged_chicken', 0),
(1326, 'steam:11000010bf29a8c', 'whisky', 0),
(1327, 'steam:11000010bf29a8c', 'carokit', 0),
(1328, 'steam:11000010bf29a8c', 'whiskey', 0),
(1329, 'steam:11000010bf29a8c', 'energy', 0),
(1330, 'steam:11000010bf29a8c', 'golem', 0),
(1331, 'steam:11000010bf29a8c', 'drugtest', 0),
(1332, 'steam:11000010bf29a8c', 'cannabis', 0),
(1333, 'steam:11000010bf29a8c', 'cutted_wood', 0),
(1334, 'steam:11000010bf29a8c', 'contrat', 0),
(1335, 'steam:11000010bf29a8c', 'saucisson', 0),
(1336, 'steam:11000010bf29a8c', 'wood', 0),
(1337, 'steam:11000010bf29a8c', 'fixtool', 0),
(1338, 'steam:11000010bf29a8c', 'fakepee', 0),
(1339, 'steam:11000010bf29a8c', 'breathalyzer', 0),
(1340, 'steam:11000010bf29a8c', 'lighter', 0),
(1341, 'steam:11000010bf29a8c', 'narcan', 0),
(1342, 'steam:11000010bf29a8c', 'poppy', 0),
(1343, 'steam:11000010bf29a8c', 'mojito', 0),
(1344, 'steam:11000010bf29a8c', 'vodka', 0),
(1345, 'steam:11000010bf29a8c', 'crack', 0),
(1346, 'steam:11000010bf29a8c', 'ephedrine', 0),
(1347, 'steam:11000010bf29a8c', 'copper', 0),
(1348, 'steam:11000010bf29a8c', 'teqpaf', 0),
(1349, 'steam:11000010bf29a8c', 'drpepper', 0),
(1350, 'steam:11000010bf29a8c', 'turtle', 0),
(1351, 'steam:11000010bf29a8c', 'gazbottle', 0),
(1352, 'steam:11000010bf29a8c', 'pearl', 0),
(1353, 'steam:11000010bf29a8c', 'croquettes', 0),
(1354, 'steam:11000010bf29a8c', 'litter_pooch', 0),
(1355, 'steam:11000010bf29a8c', 'clip', 0),
(1356, 'steam:11000010bf29a8c', 'scratchoff_used', 0),
(1357, 'steam:11000010bf29a8c', 'blackberry', 0),
(1358, 'steam:11000010bf29a8c', 'binoculars', 0),
(1359, 'steam:11000010bf29a8c', 'yusuf', 0),
(1360, 'steam:11000010bf29a8c', 'lockpick', 0),
(1361, 'steam:11000010bf29a8c', 'coca', 0),
(1362, 'steam:11000010bf29a8c', 'scratchoff', 0),
(1363, 'steam:11000010bf29a8c', 'coke', 0),
(1364, 'steam:11000010bf29a8c', 'vodkaenergy', 0),
(1365, 'steam:11000010bf29a8c', 'medikit', 0),
(1366, 'steam:11000010bf29a8c', 'litter', 0),
(1367, 'steam:11000010bf29a8c', 'rhum', 0),
(1368, 'steam:11000010bf29a8c', 'martini', 0),
(1369, 'steam:11000010bf29a8c', 'ephedra', 0),
(1370, 'steam:11000010bf29a8c', 'nitro', 0),
(1371, 'steam:11000010bf29a8c', 'gym_membership', 0),
(1372, 'steam:11000010bf29a8c', 'menthe', 0),
(1373, 'steam:11000010bf29a8c', 'lsd', 0),
(1374, 'steam:11000010bf29a8c', 'painkiller', 0),
(1375, 'steam:11000010bf29a8c', 'slaughtered_chicken', 0),
(1376, 'steam:11000010bf29a8c', 'armor', 0),
(1377, 'steam:11000010bf29a8c', 'jusfruit', 0),
(1378, 'steam:11000010bf29a8c', 'donut', 0),
(1379, 'steam:11000010bf29a8c', 'plongee1', 0),
(1380, 'steam:11000010bf29a8c', 'ice', 0),
(1381, 'steam:11000010bf29a8c', 'coke_pooch', 0),
(1382, 'steam:11000010bf29a8c', 'blowpipe', 0),
(1383, 'steam:11000010bf29a8c', 'opium', 0),
(1384, 'steam:11000010bf29a8c', 'marijuana', 0),
(1385, 'steam:11000010bf29a8c', 'alive_chicken', 0),
(1386, 'steam:11000010bf29a8c', 'leather', 0),
(1387, 'steam:11000010bf29a8c', 'icetea', 0),
(1388, 'steam:11000010bf29a8c', 'grip', 0),
(1389, 'steam:11000010bf29a8c', 'pills', 0),
(1390, 'steam:11000010bf29a8c', 'washed_stone', 0),
(1391, 'steam:11000010bf29a8c', 'tacos', 0),
(1392, 'steam:11000010bf29a8c', 'weed', 0),
(1393, 'steam:11000010bf29a8c', 'powerade', 0),
(1394, 'steam:11000010bf29a8c', 'meat', 0),
(1395, 'steam:11000010bf29a8c', 'stone', 0),
(1396, 'steam:11000010bf29a8c', 'burger', 0),
(1397, 'steam:11000010bf29a8c', 'sportlunch', 0),
(1398, 'steam:11000010bf29a8c', 'vegetables', 0),
(1399, 'steam:11000010bf29a8c', 'cocaine', 0),
(1400, 'steam:11000010bf29a8c', 'lsd_pooch', 0),
(1401, 'steam:11000010bf29a8c', 'mixapero', 0),
(1402, 'steam:11000010bf29a8c', 'cola', 0),
(1403, 'steam:11000010bf29a8c', 'diamond', 0),
(1404, 'steam:11000010bf29a8c', 'turtle_pooch', 0),
(1405, 'steam:11000010bf29a8c', 'whool', 0),
(1406, 'steam:11000010bf29a8c', 'metreshooter', 0),
(1407, 'steam:11000010bf29a8c', 'fabric', 0),
(1408, 'steam:11000010bf29a8c', 'rhumfruit', 0),
(1409, 'steam:11000010bf29a8c', 'rhumcoca', 0),
(1410, 'steam:11000010bf29a8c', 'grapperaisin', 0),
(1411, 'steam:11000010bf29a8c', 'vodkafruit', 0),
(1412, 'steam:11000010bf29a8c', 'heroine', 0),
(1413, 'steam:11000010bf29a8c', 'essence', 0),
(1414, 'steam:11000010bf29a8c', 'jager', 0),
(1415, 'steam:11000010bf29a8c', 'opium_pooch', 0),
(1416, 'steam:11000010bf29a8c', 'whiskycoca', 0),
(1417, 'steam:11000010bf29a8c', 'tequila', 0),
(1418, 'steam:11000010bf29a8c', 'silencieux', 0),
(1419, 'steam:11000010bf29a8c', 'packaged_plank', 0),
(1420, 'steam:11000010bf29a8c', 'petrol', 0),
(1421, 'steam:11000010bf29a8c', 'dabs', 0),
(1422, 'steam:11000010bf29a8c', 'petrol_raffin', 0),
(1423, 'steam:11000010bf29a8c', 'meth_pooch', 0),
(1424, 'steam:11000010bf29a8c', 'iron', 0),
(1425, 'steam:11000010bf29a8c', 'plongee2', 0),
(1426, 'steam:11000010bf29a8c', 'bolchips', 0),
(1427, 'steam:11000010bf29a8c', 'carotool', 0),
(1428, 'steam:11000010bf29a8c', 'meth', 0),
(1429, 'steam:11000010bf29a8c', 'bolpistache', 0),
(1430, 'steam:11000010bf29a8c', 'bread', 0),
(1431, 'steam:11000010bf29a8c', 'soda', 0),
(1432, 'steam:11000010bf29a8c', 'weed_pooch', 0),
(1433, 'steam:11000010bf29a8c', 'bolnoixcajou', 0),
(1434, 'steam:11000010bf29a8c', 'bolcacahuetes', 0),
(1435, 'steam:11000010bf29a8c', 'jagerbomb', 0),
(1436, 'steam:11000010bf29a8c', 'limonade', 0),
(1437, 'steam:11000010bf29a8c', 'beer', 0),
(1438, 'steam:11000010bf29a8c', 'protein_shake', 0),
(1439, 'steam:11000010bf29a8c', 'coffee', 0),
(1440, 'steam:11000010bf29a8c', 'pearl_pooch', 0),
(1441, 'steam:11000010bf29a8c', 'fish', 0),
(1442, 'steam:11000010bf29a8c', 'water', 0),
(1443, 'steam:11000010bf29a8c', 'fixkit', 0),
(1444, 'steam:11000010bf29a8c', 'clothe', 0),
(1445, 'steam:11000010bf29a8c', 'cigarett', 0),
(1446, 'steam:11000010bf29a8c', 'bandage', 0),
(1447, 'steam:11000010bf29a8c', 'pcp', 0),
(1448, 'steam:11000010bf29a8c', 'flashlight', 0),
(1449, 'steam:11000010bf29a8c', 'gold', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_licenses`
--

INSERT INTO `user_licenses` (`id`, `type`, `owner`) VALUES
(3, 'weapon', 'steam:110000132580eb0');

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(11) NOT NULL,
  `reported_by` varchar(80) DEFAULT NULL,
  `report_type` varchar(255) DEFAULT NULL,
  `report_comment` varchar(255) DEFAULT NULL,
  `report_admin` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_warnings`
--

CREATE TABLE `user_warnings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `warning` longtext,
  `time_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 15000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
(4, 'Chino', 'chino', 15000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 250000, 'muscle'),
(7, 'Dominator', 'dominator', 60000, 'muscle'),
(8, 'Dukes', 'dukes', 28000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
(11, 'Faction', 'faction', 20000, 'muscle'),
(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
(13, 'Faction XL', 'faction3', 40000, 'muscle'),
(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
(16, 'Picador', 'picador', 18000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
(20, 'Tampa', 'tampa', 16000, 'muscle'),
(21, 'Virgo', 'virgo', 14000, 'muscle'),
(22, 'Vigero', 'vigero', 12500, 'muscle'),
(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
(24, 'Blista', 'blista', 42958, 'compacts'),
(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
(26, 'Issi', 'issi2', 10000, 'compacts'),
(27, 'Panto', 'panto', 10000, 'compacts'),
(28, 'Prairie', 'prairie', 12000, 'compacts'),
(29, 'Bison', 'bison', 45000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
(31, 'Burrito', 'burrito3', 19000, 'work'),
(32, 'Burrito', 'gburrito2', 29000, 'vans'),
(33, 'Camper', 'camper', 42000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
(35, 'Journey', 'journey', 6500, 'vans'),
(36, 'Minivan', 'minivan', 13000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
(39, 'Paradise', 'paradise', 19000, 'vans'),
(40, 'Rumpo', 'rumpo', 30000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
(42, 'Surfer', 'surfer', 12000, 'vans'),
(43, 'Youga', 'youga', 10800, 'vans'),
(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
(45, 'Asea', 'asea', 5500, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
(47, 'Emperor', 'emperor', 8500, 'sedans'),
(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
(49, 'Glendale', 'glendale', 6500, 'sedans'),
(50, 'Intruder', 'intruder', 7500, 'sedans'),
(51, 'Premier', 'premier', 8000, 'sedans'),
(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
(53, 'Regina', 'regina', 5000, 'sedans'),
(54, 'Schafter', 'schafter2', 25000, 'sedans'),
(55, 'Stretch', 'stretch', 90000, 'sedans'),
(56, 'Phantom', 'superd', 250000, 'sedans'),
(57, 'MercedezAMG', 'tailgater', 130000, 'modded'),
(58, 'Warrener', 'warrener', 120000, 'sedans'),
(59, 'Washington', 'washington', 90000, 'sedans'),
(60, 'Baller', 'baller2', 40000, 'suvs'),
(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
(63, 'Contender', 'contender', 70000, 'suvs'),
(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
(66, 'Fhantom', 'fq2', 17000, 'suvs'),
(67, 'Grabger', 'granger', 50000, 'suvs'),
(68, 'Gresley', 'gresley', 47500, 'suvs'),
(69, 'Huntley S', 'huntley', 40000, 'suvs'),
(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
(71, 'Mesa', 'mesa', 16000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
(73, 'Patriot', 'patriot', 55000, 'suvs'),
(74, 'Radius', 'radi', 29000, 'suvs'),
(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
(76, 'Seminole', 'seminole', 25000, 'suvs'),
(77, 'XLS', 'xls', 70000, 'suvs'),
(78, 'Btype', 'btype', 62000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
(81, 'Casco', 'casco', 350000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
(83, 'Manana', 'manana', 12800, 'sportsclassics'),
(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 220000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
(89, 'Porsche Classic', 'ztype', 160000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 12000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
(92, 'Blazer', 'blazer', 6500, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
(94, 'Brawler', 'brawler', 75000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
(97, 'Guardian', 'guardian', 45000, 'offroad'),
(98, 'Rebel', 'rebel2', 35000, 'offroad'),
(99, 'Sandking', 'sandking', 55000, 'offroad'),
(100, 'The Liberator', 'monster', 210000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
(104, 'Exemplar', 'exemplar', 132000, 'modded'),
(105, 'Silvia', 'f620', 80000, 'modded'),
(106, 'Felon', 'felon', 42000, 'coupes'),
(107, 'Felon GT', 'felon2', 55000, 'coupes'),
(108, 'Jackal', 'jackal', 38000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
(112, 'Windsor', 'windsor', 350000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 180000, 'coupes'),
(114, 'Zion', 'zion', 40000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 70000, 'coupes'),
(116, '9F', 'ninef', 65000, 'sports'),
(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 125000, 'modded'),
(120, 'Bestia GTS', 'bestiagts', 400000, 'sports'),
(121, 'Buffalo', 'buffalo', 120000, 'modded'),
(122, 'wide body charger', 'buffalo2', 175000, 'modded'),
(123, 'Carbonizzare', 'carbonizzare', 110000, 'modded'),
(124, 'Comet', 'comet2', 200000, 'modded'),
(125, 'Coquette', 'coquette', 65000, 'sports'),
(126, 'Vantage', 'tampa2', 230000, 'sports'),
(127, 'Elegy', 'elegy2', 175000, 'sports'),
(128, 'Feltzer', 'feltzer2', 125000, 'sports'),
(129, 'Furore GT', 'furoregt', 45000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
(133, 'Khamelion', 'khamelion', 38000, 'sports'),
(134, 'Kuruma', 'kuruma', 75000, 'sports'),
(135, 'Lynx', 'lynx', 40000, 'sports'),
(136, 'Mamba', 'mamba', 70000, 'sports'),
(137, 'Massacro', 'massacro', 65000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
(139, 'Omnis', 'omnis', 35000, 'sports'),
(140, 'Penumbra', 'penumbra', 28000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
(143, 'Schafter V12', 'schafter3', 130000, 'sports'),
(144, 'Seven 70', 'seven70', 290000, 'sports'),
(145, 'Sultan', 'sultan', 55000, 'sports'),
(146, 'Surano', 'surano', 50000, 'sports'),
(147, 'Tropos', 'tropos', 95000, 'sports'),
(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
(149, 'Adder', 'adder', 1000000, 'modded'),
(150, 'Banshee 900R', 'banshee2', 350000, 'super'),
(151, 'Bullet', 'bullet', 250000, 'super'),
(152, 'Cheetah', 'cheetah', 500000, 'modded'),
(153, 'Entity XF', 'entityxf', 425000, 'super'),
(154, 'Maserati ET1', 'sheava', 500000, 'super'),
(155, 'FMJ', 'fmj', 185000, 'super'),
(156, 'Infernus', 'infernus', 240000, 'modded'),
(157, 'Osiris', 'osiris', 160000, 'super'),
(158, 'Pfister', 'pfister811', 85000, 'super'),
(159, 'RE-7B', 'le7b', 325000, 'super'),
(160, 'Reaper', 'reaper', 150000, 'super'),
(161, 'Sultan RS', 'sultanrs', 130000, 'super'),
(162, 'T20', 't20', 300000, 'super'),
(163, 'Turismo R', 'turismor', 350000, 'super'),
(164, 'Tyrus', 'tyrus', 600000, 'super'),
(165, 'Vacca', 'vacca', 120000, 'super'),
(166, 'Voltic', 'voltic', 900000, 'super'),
(167, 'X80 Proto', 'prototipo', 950000, 'super'),
(168, 'Zentorno', 'zentorno', 700000, 'super'),
(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
(174, 'BF400', 'bf400', 6500, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
(183, 'Double T', 'double', 28000, 'motorcycles'),
(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 7500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 7500, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 80000, 'motorcycles'),
(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
(207, 'Vader', 'vader', 7200, 'motorcycles'),
(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
(213, 'Ruiner', 'ruiner', 90000, 'muscle'),
(214, 'TunedComet', 'comet3', 300000, 'super'),
(215, 'TunedslamVan', 'slamvan3', 270000, 'muscle'),
(216, 'TunedVirgo', 'virgo3', 220000, 'muscle'),
(217, 'Nero', 'nero', 400000, 'super'),
(218, 'TunedBucaneer', 'buccaneer2', 200000, 'muscle'),
(219, 'TunedChino', 'chino2', 190000, 'muscle'),
(220, 'TunedElegy', 'elegy', 220000, 'sports'),
(221, 'TunedFaction', 'faction2', 190000, 'muscle'),
(222, 'PegassiFCR', 'fcr', 8000, 'motorcycles'),
(223, 'TunedPegassi', 'fcr2', 15000, 'motorcycles'),
(224, 'ProgenItaliGTB', 'italigtb', 260000, 'super'),
(225, 'TunedItaliGTB', 'italigtb2', 320000, 'super'),
(226, 'Tunedminivan', 'minivan2', 160000, 'suvs'),
(227, 'TunedNero', 'nero2', 500000, 'super'),
(228, 'Primo', 'primo', 35000, 'coupes'),
(229, 'DewbaucheeSpecter', 'specter', 220000, 'sports'),
(230, 'Tunedspecter', 'specter2', 290000, 'sports'),
(231, 'TunedVan', 'slamvan2', 25000, 'muscle'),
(232, 'EMSCar', 'emscar', 1000, 'work'),
(233, 'EMSCar2', 'emscar2', 1000, 'work'),
(234, 'EMSVan', 'emsvan', 1000, 'work'),
(235, 'EMSSuv', 'emssuv', 1000, 'work'),
(236, 'Rat Loader', 'ratloader', 10000, 'work'),
(237, 'Sand King2', 'sandking2', 60000, 'offroad'),
(238, 'Sadler', 'sadler', 75000, 'offroad'),
(239, 'Taxi', 'taxi', 15000, 'work'),
(240, 'Rubble', 'rubble', 100000, 'work'),
(241, 'Tour Bus', 'tourbus', 30000, 'work'),
(242, 'Tow Truck', 'towtruck', 25000, 'work'),
(243, 'Flat Bed', 'flatbed', 27000, 'work'),
(244, 'Clown', 'speedo2', 16000, 'work'),
(246, 'Stratum', 'stratum', 80000, 'sports'),
(249, 'Mazda', 'blista3', 40000, 'compacts'),
(250, 'Honda Civic', 'blista2', 30000, 'compacts'),
(251, 'Caddilac', 'buffalo3', 50000, 'sedans'),
(252, 'Golf Green', 'surge', 70000, 'compacts'),
(253, 'Stalion', 'stalion', 60000, 'muscle'),
(254, 'SRT', 'stalion2', 160000, 'muscle'),
(255, 'Mercedez RI', 'serrano', 90000, 'suvs'),
(256, 'Mercedes AMG', 'schafter4', 140000, 'sedans'),
(257, 'BMW M3', 'schafter5', 135000, 'sedans'),
(258, 'BMW M3', 'schwarzer', 150000, 'sedans'),
(259, 'gt500', 'gt500', 45000, 'doomsday'),
(260, 'comet4', 'comet4', 180000, 'doomsday'),
(261, 'comet5', 'comet5', 200000, 'doomsday'),
(263, 'hermes', 'hermes', 60000, 'doomsday'),
(264, 'hustler', 'hustler', 60000, 'doomsday'),
(265, 'kamacho', 'kamacho', 50000, 'doomsday'),
(266, 'neon', 'neon', 100000, 'doomsday'),
(267, 'pariah', 'pariah', 100000, 'doomsday'),
(268, 'raiden', 'raiden', 75000, 'doomsday'),
(269, 'revolter', 'revolter', 75000, 'doomsday'),
(270, 'riata', 'riata', 75000, 'doomsday'),
(271, 'savestra', 'savestra', 75000, 'doomsday'),
(272, 'sc1', 'sc1', 75000, 'doomsday'),
(273, 'streiter', 'streiter', 75000, 'doomsday'),
(274, 'stromberg', 'stromberg', 75000, 'doomsday'),
(275, 'sentinel3', 'sentinel3', 75000, 'doomsday'),
(276, 'viseris', 'viseris', 75000, 'doomsday'),
(277, 'yosemite', 'yosemite', 75000, 'doomsday'),
(278, 'z190', 'z190', 75000, 'doomsday'),
(279, 'Mustang', 'musty5', 90000, 'modded'),
(280, 'Infiniti G37', 'g37cs', 50000, 'modded'),
(281, 'Peugeot 107', 'p107', 30000, 'modded'),
(282, 'Renault Megane', 'renmeg', 70000, 'modded'),
(283, 'Lamborghini Hurricane', 'lh610', 230000, 'modded'),
(284, 'Aston Cygnet', 'cygnet11', 40000, 'modded'),
(285, 'Cadillac CTS', 'cadicts', 55000, 'modded'),
(286, 'Mini John Cooper', 'miniub', 45000, 'modded'),
(287, 'Lotus Espirit V8', 'lev8', 130000, 'modded'),
(288, 'Lambo Veneno', 'lamven', 500000, 'modded'),
(289, 'Nissan GTR SpecV', 'gtrublu', 230000, 'modded'),
(290, 'Genesis', 'genublu', 60000, 'modded'),
(291, 'Porsche Cayman R', 'caymanub', 190000, 'modded'),
(292, 'Porsche 911 GT', '911ublu', 700000, 'modded'),
(293, 'Ferrari Laferrari', 'laferublu', 450000, 'modded'),
(294, 'Mclaren 12c', 'mcublu', 400000, 'modded'),
(295, 'Merc SLR', 'slrublu', 200000, 'modded'),
(297, 'Merc SLS AMG Electric', 'slsublue', 150000, 'modded'),
(298, 'Dodge Charger', 'charublu', 90000, 'modded'),
(299, 'subaru 22b', '22bbublu', 80000, 'modded'),
(300, 'Focus RS', 'focusublu', 70000, 'modded'),
(301, 'Mazda furai', 'furaiub', 350000, 'modded'),
(302, 'Ferrari F50', 'f50ub', 350000, 'modded'),
(303, 'Porsche 550a', 'p550a', 250000, 'modded'),
(304, 'Porsche 959', 'p959', 250000, 'modded'),
(305, 'Porsche 944', 'p944', 180000, 'modded'),
(306, 'dodge Viper', 'vip99', 450000, 'modded'),
(307, 'Mazda Rx8', 'rx8', 50000, 'modded'),
(308, 'Ferrari 599', 'gtbf', 290000, 'modded'),
(309, 'Tesla Roadster', 'tesla11', 55000, 'modded'),
(310, 'Mazda Mx5a', 'mx5a', 45000, 'modded'),
(311, 'toyota Celica', 'celicassi', 48000, 'modded'),
(312, 'Toyota celica T', 'celicassi2', 55000, 'modded'),
(313, 'Aston Martin Vanquish', 'amv12', 280000, 'modded'),
(314, 'Subari WRX STI', 'sti05', 80000, 'modded'),
(315, 'Porsche Panamera', 'panamera', 200000, 'modded'),
(316, 'Ferrari 360', 'f360', 250000, 'modded'),
(317, 'Lambo Mura', 'miura', 230000, 'modded'),
(318, 'chevrolet Corvette', 'zr1c3', 180000, 'modded'),
(319, 'Lambo Gallardo', 'gallardo', 300000, 'modded'),
(320, 'Corvette Stingray', 'vc7', 230000, 'modded'),
(321, 'Ferrari Cali', '2fiftygt', 260000, 'modded'),
(322, 'Mercedz Gullwing', '300gsl', 200000, 'modded'),
(323, 'Aston Martin vantage', 'db700', 140000, 'modded'),
(324, 'shelby cobra', 'cobra', 130000, 'modded'),
(325, 'BMW Z4i', 'z4i', 100000, 'modded'),
(326, 'Lambo Huracan', 'huracan', 240000, 'modded'),
(327, 'Ferrari 812', 'ferrari812', 250000, 'modded'),
(328, 'Lambo Veneno', 'veneno', 350000, 'modded'),
(329, 'Ferrari XXK', 'fxxk16', 300000, 'modded'),
(330, 'LaFerrari 15', 'laferrari15', 400000, 'modded'),
(331, 'Italia 458 LW', 'lw458s', 320000, 'modded'),
(332, 'Lykan', 'lykan', 350000, 'modded'),
(333, 'iTalia 458', 'italia458', 290000, 'modded'),
(334, 'Diablous', 'Diablous', 15000, 'motorcycles'),
(335, 'Diablous 2', 'Diablous2', 17000, 'motorcycles'),
(336, 'Raptor', 'Raptor', 20000, 'motorcycles'),
(337, 'Ratbike', 'Ratbike', 16000, 'motorcycles'),
(338, 'Xa21', 'XA21', 340000, 'super'),
(339, 'Penetrator', 'Penetrator', 300000, 'super'),
(340, 'Gp1', 'GP1', 270000, 'super'),
(341, 'Tempestra', 'Tempesta', 260000, 'super'),
(342, 'Toreo', 'Torero', 250000, 'sportsclassics'),
(343, 'Infernus2', 'Infernus2', 240000, 'sportsclassics'),
(344, 'Savestra', 'Savestra', 240000, 'sportsclassics'),
(345, 'Cheetah2', 'Cheetah2', 200000, 'sportsclassics'),
(346, 'Turismo2', 'Turismo2', 180000, 'sportsclassics'),
(347, 'Viceris', 'Viceris', 190000, 'sportsclassics'),
(348, 'JB700', 'JB700', 190000, 'sportsclassics'),
(349, 'Peyote', 'Peyote', 175000, 'sportsclassics'),
(350, 'Ruston', 'Ruston', 150000, 'sports'),
(351, 'Surge', 'Surge', 45000, 'sedans'),
(353, 'Voltic', 'Voltic2', 4000000, 'super'),
(354, 'Dilettante', 'Dilettante', 50000, 'compacts'),
(355, 'Tornado6', 'Tornado6', 150000, 'muscle'),
(356, 'Gauntlet2', 'Gauntlet2', 100000, 'muscle'),
(357, 'Dominator2', 'Dominator2', 100000, 'muscle'),
(358, 'Hurse', 'Lurcher', 60000, 'muscle'),
(359, 'Vagner', 'Vagner', 250000, 'super'),
(360, 'Austarch', 'Autarch', 230000, 'super'),
(361, 'Tornado5', 'tornado5', 75000, 'muscle'),
(362, 'audi a4', 'asterope', 110000, 'sports'),
(363, 'Merc AMG', 'rmodamgc63', 165000, 'sports'),
(364, 'dodgeCharger', '69charger', 140000, 'muscle'),
(365, 'R35', 'r35', 200000, 'modded'),
(367, 'Mustang', 'mgt', 180000, 'modded'),
(368, 'Cheburek', 'cheburek', 20000, 'assault'),
(369, 'Ellie', 'ellie', 100000, 'assault'),
(370, 'Dommy3', 'dominator3', 110000, 'assault'),
(371, 'Enity2', 'entity2', 200000, 'assault'),
(372, 'Fagi', 'fagaloa', 22000, 'assault'),
(373, 'Flash', 'flashgt', 105000, 'assault'),
(374, 'RS200', 'gb200', 135000, 'assault'),
(375, 'Hotring', 'hotring', 140000, 'assault'),
(376, 'Mini', 'issi3', 18000, 'assault'),
(377, 'Jester3', 'jester3', 85000, 'assault'),
(378, 'Michelle', 'michelli', 41000, 'assault'),
(379, 'Tai', 'taipan', 220000, 'assault'),
(380, 'Tezeract', 'tezeract', 180000, 'assault'),
(381, 'Tyrant', 'tyrant', 220000, 'assault'),
(382, 'Impreza', 'ySbrImpS11', 100000, 'modded'),
(383, 'R35 Skyline', 'r35', 200000, 'sports'),
(384, 'Insurgent', 'insurgent3', 200000, 'modded'),
(385, 'Halftrack', 'halftrack', 200000, 'modded'),
(386, 'Tempa', 'tampa3', 200000, 'modded'),
(387, 'Technical', 'technical3', 200000, 'modded'),
(388, 'Tech2', 'technical2', 200000, 'modded'),
(389, 'Barrage', 'barrage', 200000, 'modded'),
(390, 'Boxville', 'boxville5', 200000, 'modded'),
(391, 'Road King', 'foxharley2', 50000, 'motorcycles');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`id`, `name`, `label`) VALUES
(1, 'compacts', 'Compacts'),
(2, 'coupes', 'Coupés'),
(3, 'sedans', 'Sedans'),
(4, 'sports', 'Sports'),
(5, 'sportsclassics', 'Sports Classics'),
(6, 'super', 'Super'),
(7, 'muscle', 'Muscle'),
(8, 'offroad', 'Off Road'),
(9, 'suvs', 'SUVs'),
(10, 'vans', 'Vans'),
(11, 'motorcycles', 'Motos'),
(12, 'work', 'Work'),
(13, 'doomsday', 'doomsday'),
(14, 'modded', 'Modded'),
(15, 'assault', 'assault');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `fullname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `crimes` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 3500),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 45000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 25000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 85000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 95000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(25, 'GunShop', 'WEAPON_BALL', 50),
(26, 'BlackWeashop', 'WEAPON_BALL', 50),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(28, 'GunShop', 'WEAPON_PISTOL50', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commend`
--
ALTER TABLE `commend`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gsr`
--
ALTER TABLE `gsr`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kicks`
--
ALTER TABLE `kicks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `license` (`license`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_bans`
--
ALTER TABLE `received_bans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `server_actions`
--
ALTER TABLE `server_actions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin_notes`
--
ALTER TABLE `user_admin_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_warnings`
--
ALTER TABLE `user_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commend`
--
ALTER TABLE `commend`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2045;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `kicks`
--
ALTER TABLE `kicks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `received_bans`
--
ALTER TABLE `received_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_actions`
--
ALTER TABLE `server_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1450;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_warnings`
--
ALTER TABLE `user_warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=392;

--
-- AUTO_INCREMENT for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Database: `fuckoff`
--
CREATE DATABASE IF NOT EXISTS `fuckoff` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `fuckoff`;

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(10, 'society_police', 'Police', 1),
(11, 'property_black_money', 'Argent Sale Propriété', 0),
(12, 'society_realestateagent', 'Agent immobilier', 1),
(13, 'caution', 'Caution', 0),
(14, 'society_taxi', 'Taxi', 1),
(15, 'society_cardealer', 'Concessionnaire', 1),
(16, 'society_banker', 'Banque', 1),
(17, 'bank_savings', 'Livret Bleu', 0),
(18, 'society_mecano', 'Mécano', 1),
(19, 'society_ambulance', 'Ambulance', 1),
(20, 'society_cardealer', 'Concessionnaire', 1),
(21, 'society_foodtruck', 'Foodtruck', 1),
(22, 'society_fire', 'fire', 1),
(23, 'society_mecano', 'Mécano', 1),
(24, 'society_fire', 'fire', 1),
(25, 'society_unicorn', 'Unicorn', 1),
(26, 'society_airlines', 'Airlines', 1),
(27, 'society_foodtruck', 'Foodtruck', 1),
(28, 'society_avocat', 'Advokat', 1),
(29, 'society_journaliste', 'journaliste', 1),
(30, 'society_karting', 'Karing', 1),
(31, 'society_parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(420, 'society_police', 145, NULL),
(421, 'society_realestateagent', 0, NULL),
(422, 'society_taxi', 0, NULL),
(423, 'society_cardealer', 0, NULL),
(424, 'society_banker', 0, NULL),
(425, 'society_mecano', 0, NULL),
(426, 'society_ambulance', 0, NULL),
(427, 'society_foodtruck', 0, NULL),
(497, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(498, 'caution', 0, 'steam:11000010a01bdb9'),
(499, 'bank_savings', 0, 'steam:11000010a01bdb9'),
(500, 'bank_savings', 0, 'steam:1100001068ef13c'),
(501, 'caution', 0, 'steam:1100001068ef13c'),
(502, 'property_black_money', 0, 'steam:1100001068ef13c'),
(503, 'property_black_money', 0, 'steam:110000132580eb0'),
(504, 'bank_savings', 0, 'steam:110000132580eb0'),
(505, 'caution', 0, 'steam:110000132580eb0'),
(506, 'society_fire', 0, NULL),
(507, 'property_black_money', 0, 'steam:110000112f07694'),
(508, 'caution', 0, 'steam:110000112f07694'),
(509, 'bank_savings', 0, 'steam:110000112f07694'),
(510, 'property_black_money', 0, 'steam:1100001042b46e1'),
(511, 'caution', 0, 'steam:1100001042b46e1'),
(512, 'bank_savings', 0, 'steam:1100001042b46e1'),
(513, 'property_black_money', 0, 'steam:11000011820eeac'),
(514, 'bank_savings', 0, 'steam:11000011820eeac'),
(515, 'caution', 0, 'steam:11000011820eeac'),
(516, 'property_black_money', 0, 'steam:11000011258948d'),
(517, 'caution', 0, 'steam:11000011258948d'),
(518, 'bank_savings', 0, 'steam:11000011258948d'),
(519, 'society_unicorn', 9070, NULL),
(520, 'society_airlines', 0, NULL),
(521, 'society_avocat', 0, NULL),
(522, 'property_black_money', 0, 'steam:110000114590956'),
(523, 'bank_savings', 0, 'steam:110000114590956'),
(524, 'caution', 0, 'steam:110000114590956'),
(525, 'caution', 0, 'steam:110000115c5ddc9'),
(526, 'property_black_money', 0, 'steam:110000115c5ddc9'),
(527, 'bank_savings', 0, 'steam:110000115c5ddc9'),
(528, 'society_journaliste', 100000, NULL),
(529, 'society_karting', 100, NULL),
(530, 'property_black_money', 0, 'steam:110000109660ef9'),
(531, 'caution', 0, 'steam:110000109660ef9'),
(532, 'bank_savings', 0, 'steam:110000109660ef9'),
(533, 'bank_savings', 0, 'steam:11000010c6acdce'),
(534, 'property_black_money', 0, 'steam:11000010c6acdce'),
(535, 'caution', 0, 'steam:11000010c6acdce'),
(536, 'bank_savings', 0, 'steam:110000106a5790d'),
(537, 'caution', 0, 'steam:110000106a5790d'),
(538, 'property_black_money', 0, 'steam:110000106a5790d'),
(539, 'property_black_money', 0, 'steam:11000010b0a1bc3'),
(540, 'bank_savings', 0, 'steam:11000010b0a1bc3'),
(541, 'caution', 0, 'steam:11000010b0a1bc3'),
(542, 'bank_savings', 0, 'steam:11000011b78df95'),
(543, 'caution', 0, 'steam:11000011b78df95'),
(544, 'property_black_money', 0, 'steam:11000011b78df95'),
(545, 'property_black_money', 0, 'steam:110000135c88cca'),
(546, 'caution', 0, 'steam:110000135c88cca'),
(547, 'bank_savings', 0, 'steam:110000135c88cca'),
(548, 'society_parking', 10000, NULL),
(549, 'property_black_money', 0, 'steam:11000010473daf1'),
(550, 'bank_savings', 0, 'steam:11000010473daf1'),
(551, 'caution', 0, 'steam:11000010473daf1'),
(552, 'bank_savings', 0, 'steam:110000103b294e0'),
(553, 'property_black_money', 0, 'steam:110000103b294e0'),
(554, 'caution', 0, 'steam:110000103b294e0'),
(555, 'property_black_money', 0, 'steam:110000104e0ed21'),
(556, 'caution', 0, 'steam:110000104e0ed21'),
(557, 'bank_savings', 0, 'steam:110000104e0ed21'),
(558, 'property_black_money', 0, 'steam:1100001238ac476'),
(559, 'bank_savings', 0, 'steam:1100001238ac476'),
(560, 'caution', 0, 'steam:1100001238ac476'),
(561, 'property_black_money', 0, 'steam:11000011742efb1'),
(562, 'caution', 0, 'steam:11000011742efb1'),
(563, 'bank_savings', 0, 'steam:11000011742efb1'),
(564, 'caution', 0, 'steam:110000112969e8f'),
(565, 'property_black_money', 0, 'steam:110000112969e8f'),
(566, 'bank_savings', 0, 'steam:110000112969e8f'),
(567, 'bank_savings', 0, 'steam:110000134857568'),
(568, 'property_black_money', 0, 'steam:110000134857568'),
(569, 'caution', 0, 'steam:110000134857568'),
(570, 'caution', 0, 'steam:110000131f0d83c'),
(571, 'property_black_money', 0, 'steam:110000131f0d83c'),
(572, 'bank_savings', 0, 'steam:110000131f0d83c'),
(573, 'property_black_money', 0, 'steam:110000110931da8'),
(574, 'bank_savings', 0, 'steam:110000110931da8'),
(575, 'caution', 0, 'steam:110000110931da8'),
(576, 'property_black_money', 0, 'steam:1100001185894b3'),
(577, 'bank_savings', 0, 'steam:1100001185894b3'),
(578, 'caution', 0, 'steam:1100001185894b3'),
(579, 'bank_savings', 0, 'steam:11000011a15fea8'),
(580, 'property_black_money', 0, 'steam:11000011a15fea8'),
(581, 'caution', 0, 'steam:11000011a15fea8'),
(582, 'caution', 0, 'steam:1100001183ded9e'),
(583, 'property_black_money', 0, 'steam:1100001183ded9e'),
(584, 'bank_savings', 0, 'steam:1100001183ded9e'),
(585, 'bank_savings', 0, 'steam:110000115815ad6'),
(586, 'caution', 0, 'steam:110000115815ad6'),
(587, 'property_black_money', 0, 'steam:110000115815ad6');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_admin', 'Admin', 1),
(4, 'property', 'Propriété', 0),
(5, 'society_taxi', 'Taxi', 1),
(6, 'society_cardealer', 'Dealer', 1),
(7, 'society_mecano', 'Mechanic', 1),
(8, 'society_cardealer', 'Concesionnaire', 1),
(9, 'society_fire', 'fire', 1),
(10, 'society_mecano', 'Mécano', 1),
(11, 'society_fire', 'fire', 1),
(12, 'society_unicorn', 'Unicorn', 1),
(13, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(14, 'society_airlines', 'Airlines', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(1, 'steam:110000134857568', 'steam:110000132580eb0', 'society', 'society_police', 'Fine: Shooting a Civilian', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'f',
  `height` varchar(128) NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `fire_rank` int(11) DEFAULT '-1',
  `staff_rank` int(11) DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coffees`
--

INSERT INTO `coffees` (`id`, `name`, `item`, `price`) VALUES
(1, 'Coffee', 'coffee', 30);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(11, 'property', 'Property', 0),
(12, 'society_fire', 'fire', 1),
(14, 'society_unicorn', 'Unicorn', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(139, 'society_police', NULL, '{}'),
(163, 'property', 'steam:11000010a01bdb9', '{\"dressing\":[]}'),
(164, 'property', 'steam:1100001068ef13c', '{\"dressing\":[{\"label\":\"idk\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":0,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"lipstick_1\":0,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":0,\"tshirt_1\":14,\"beard_4\":0,\"bags_2\":0,\"shoes_1\":3,\"lipstick_4\":0,\"makeup_4\":0,\"mask_1\":0,\"chain_1\":2,\"eyebrows_2\":8,\"eyebrows_4\":0,\"lipstick_2\":0,\"helmet_1\":-1,\"arms\":0,\"eyebrows_1\":4,\"tshirt_2\":0,\"beard_2\":3,\"sex\":1,\"makeup_3\":0,\"beard_3\":11,\"makeup_2\":0,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":8,\"shoes_2\":2,\"decals_2\":0,\"face\":31,\"mask_2\":0,\"hair_color_1\":11,\"torso_1\":34,\"age_2\":0,\"bproof_2\":0,\"pants_1\":0,\"glasses_1\":4,\"hair_1\":19,\"ears_1\":1,\"bproof_1\":0}},{\"label\":\"Cop girl\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":1,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"hair_color_1\":11,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":13,\"lipstick_1\":0,\"beard_2\":3,\"glasses_1\":5,\"shoes_1\":14,\"lipstick_4\":0,\"makeup_3\":6,\"tshirt_1\":152,\"mask_1\":0,\"eyebrows_2\":8,\"eyebrows_4\":0,\"chain_1\":2,\"helmet_1\":-1,\"arms\":14,\"makeup_4\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"sex\":1,\"eyebrows_1\":4,\"beard_3\":11,\"makeup_2\":9,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":0,\"shoes_2\":15,\"bproof_2\":9,\"face\":31,\"mask_2\":0,\"age_2\":0,\"torso_1\":73,\"decals_2\":0,\"beard_4\":0,\"pants_1\":3,\"bags_2\":0,\"hair_1\":17,\"ears_1\":1,\"bproof_1\":27}}]}'),
(165, 'property', 'steam:110000132580eb0', '{\"dressing\":[{\"skin\":{\"makeup_2\":0,\"arms\":0,\"eyebrows_3\":0,\"eyebrows_4\":0,\"torso_2\":0,\"glasses_2\":0,\"face\":0,\"hair_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"lipstick_3\":0,\"sex\":0,\"beard_3\":0,\"glasses_1\":0,\"bags_1\":0,\"ears_2\":0,\"beard_2\":0,\"chain_1\":0,\"skin\":0,\"makeup_4\":0,\"bproof_1\":0,\"lipstick_1\":0,\"pants_2\":7,\"tshirt_1\":0,\"mask_2\":0,\"ears_1\":-1,\"eyebrows_1\":0,\"lipstick_4\":0,\"shoes_2\":0,\"makeup_1\":0,\"shoes_1\":0,\"lipstick_2\":0,\"makeup_3\":0,\"decals_2\":0,\"helmet_2\":0,\"chain_2\":0,\"beard_1\":0,\"mask_1\":0,\"age_2\":0,\"eyebrows_2\":0,\"hair_1\":0,\"hair_color_2\":0,\"bproof_2\":0,\"torso_1\":0,\"decals_1\":0,\"hair_color_1\":0,\"beard_4\":0,\"pants_1\":9,\"tshirt_2\":0,\"age_1\":0},\"label\":\"test\"}]}'),
(166, 'society_fire', NULL, '{}'),
(167, 'property', 'steam:110000112f07694', '{}'),
(168, 'property', 'steam:1100001042b46e1', '{}'),
(169, 'property', 'steam:11000011820eeac', '{}'),
(170, 'property', 'steam:11000011258948d', '{}'),
(171, 'society_unicorn', NULL, '{}'),
(172, 'society_avocat', NULL, '{}'),
(173, 'property', 'steam:110000114590956', '{}'),
(174, 'property', 'steam:110000115c5ddc9', '{}'),
(175, 'society_journaliste', NULL, '{}'),
(176, 'property', 'steam:110000109660ef9', '{}'),
(177, 'property', 'steam:11000010c6acdce', '{}'),
(178, 'property', 'steam:110000106a5790d', '{}'),
(179, 'property', 'steam:11000010b0a1bc3', '{}'),
(180, 'property', 'steam:11000011b78df95', '{}'),
(181, 'property', 'steam:110000135c88cca', '{}'),
(182, 'property', 'steam:11000010473daf1', '{}'),
(183, 'property', 'steam:110000103b294e0', '{}'),
(184, 'property', 'steam:110000104e0ed21', '{}'),
(185, 'property', 'steam:1100001238ac476', '{}'),
(186, 'property', 'steam:11000011742efb1', '{}'),
(187, 'society_ambulance', NULL, '{}'),
(188, 'property', 'steam:110000112969e8f', '{}'),
(189, 'property', 'steam:110000134857568', '{}'),
(190, 'property', 'steam:110000131f0d83c', '{}'),
(191, 'property', 'steam:110000110931da8', '{}'),
(192, 'property', 'steam:1100001185894b3', '{}'),
(193, 'property', 'steam:11000011a15fea8', '{}'),
(194, 'property', 'steam:1100001183ded9e', '{}'),
(195, 'property', 'steam:110000115815ad6', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Excessive Horn Use', 125, 0),
(2, 'Crossing Lines', 150, 0),
(3, 'Cont', 250, 0),
(4, 'Illegal U-Turn', 250, 0),
(5, 'Illegal Offroading', 375, 0),
(6, 'Illegal Safety Distance', 125, 0),
(7, 'Dangerous / forbidden stop', 450, 0),
(8, 'Illegal Parking', 110, 0),
(9, 'Not respecting the priority on the right', 225, 0),
(10, 'Non-compliance with a priority vehicle', 225, 0),
(11, 'Failure to stop', 350, 0),
(12, 'Failure to comply with a red light', 225, 0),
(13, 'Dangerous Driving', 1000, 0),
(14, 'Vehicle out of state', 100, 0),
(15, 'Driving without a license', 1500, 0),
(16, 'Hit and run', 1125, 0),
(17, 'Speeding <5 km / h', 135, 0),
(18, 'Speeding 5-15 kmh', 175, 0),
(19, 'Speeding 15-30 kmh', 375, 0),
(20, 'Speeding > 30 kmh', 1250, 0),
(21, 'Traffic obstruction', 110, 1),
(22, 'Degradation of the public road', 90, 1),
(23, 'Trouble with public order', 90, 1),
(24, 'Obstructing police operation', 225, 1),
(25, 'Insulting civilians', 135, 1),
(26, 'Insulting a police officer', 550, 1),
(27, 'Verbal threat or intimidation towards civil', 750, 1),
(28, 'Verbal threat or intimidation of a police officer', 1000, 1),
(29, 'Illegal protest', 250, 1),
(30, 'Attempted bribery', 1500, 1),
(31, 'Weapon out in town', 375, 2),
(32, 'Flashing a lethal wepon', 300, 2),
(33, 'Have a gun with no permit', 600, 2),
(34, 'Porting illegal wepons', 700, 2),
(35, 'Using a lockpickk', 300, 2),
(36, 'Car Theft', 3500, 2),
(37, 'Sale OF Drugs', 1500, 2),
(38, 'Drug Manifacturing', 1500, 2),
(39, 'Possetion Of Drugs', 650, 2),
(40, 'Civil Despute', 1500, 2),
(41, 'Takeover agent of the state', 2000, 2),
(42, 'Special deflection', 650, 2),
(43, 'Robbing Store', 4500, 2),
(44, 'Attempt to rob', 1500, 2),
(45, 'Shooting a Civilian', 2000, 3),
(46, 'Shooting a State Agent', 7500, 3),
(47, 'Attempt to Kill CIV', 3000, 3),
(48, 'Attempted murder of LEO', 5000, 3),
(49, 'Murder on civilian', 10000, 3),
(50, 'Murder on STate Agent', 30000, 3),
(51, 'Murder involuntarily', 1800, 3),
(52, 'Business scam', 2000, 2),
(53, 'After Market Parts', 1000, 0),
(54, 'DWI', 3500, 0),
(55, 'avoid and allude ', 4500, 3);

-- --------------------------------------------------------

--
-- Table structure for table `interiors`
--

CREATE TABLE `interiors` (
  `id` int(11) NOT NULL COMMENT 'key id',
  `enter` text NOT NULL COMMENT 'enter coords',
  `exit` text NOT NULL COMMENT 'destination coords',
  `iname` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interiors`
--

INSERT INTO `interiors` (`id`, `enter`, `exit`, `iname`) VALUES
(1, '{-1045.888,-2751.017,21.3634,321.7075}', '{-1055.37,-2698.47,13.82,234.62}', 'first int');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'weed', 'Weed', -1, 0, 1),
(2, 'weed_pooch', 'Pochon de weed', -1, 0, 1),
(3, 'coke', 'Coke', -1, 0, 1),
(4, 'coke_pooch', 'Pochon de coke', -1, 0, 1),
(5, 'meth', 'Meth', -1, 0, 1),
(6, 'meth_pooch', 'Pochon de meth', -1, 0, 1),
(7, 'opium', 'Opium', -1, 0, 1),
(8, 'opium_pooch', 'Pochon de opium', -1, 0, 1),
(9, 'alive_chicken', 'Poulet vivant', -1, 0, 1),
(10, 'slaughtered_chicken', 'Poulet abattu', -1, 0, 1),
(11, 'packaged_chicken', 'Poulet en barquette', -1, 0, 1),
(12, 'fish', 'Poisson', -1, 0, 1),
(13, 'stone', 'Pierre', -1, 0, 1),
(14, 'washed_stone', 'Pierre Lavée', -1, 0, 1),
(15, 'copper', 'Cuivre', -1, 0, 1),
(16, 'iron', 'Fer', -1, 0, 1),
(17, 'gold', 'Or', -1, 0, 1),
(18, 'diamond', 'Diamant', -1, 0, 1),
(19, 'wood', 'Bois', -1, 0, 1),
(20, 'cutted_wood', 'Bois coupé', -1, 0, 1),
(21, 'packaged_plank', 'Paquet de planches', -1, 0, 1),
(22, 'petrol', 'Pétrole', -1, 0, 1),
(23, 'petrol_raffin', 'Pétrole Raffiné', -1, 0, 1),
(24, 'essence', 'Essence', -1, 0, 1),
(25, 'whool', 'Laine', -1, 0, 1),
(26, 'fabric', 'Tissu', -1, 0, 1),
(27, 'clothe', 'Vêtement', -1, 0, 1),
(28, 'bread', 'Bread', -1, 0, 1),
(29, 'water', 'Water', -1, 0, 1),
(30, 'gazbottle', 'Gas', -1, 0, 1),
(31, 'fixtool', 'FixTool', -1, 0, 1),
(32, 'carotool', 'CarTool', -1, 0, 1),
(33, 'blowpipe', 'BlowPipe', -1, 0, 1),
(34, 'fixkit', 'FixKit', -1, 0, 1),
(35, 'carokit', 'Kit carosserie', -1, 0, 1),
(52, 'bandage', 'Bandage', 50, 0, 1),
(53, 'medikit', 'Medikit', 50, 0, 1),
(55, 'cola', 'Coke', 20, 0, 1),
(56, 'vegetables', 'Vegetables', 20, 0, 1),
(57, 'meat', 'Meat', 20, 0, 1),
(58, 'tacos', 'Tacos', 20, 0, 1),
(59, 'burger', 'Burger', 20, 0, 1),
(62, 'coffee', 'Cafe', 10, 1, 1),
(63, 'donut', 'Donut', 10, 1, 1),
(74, 'black_chip', 'Puce cryptée', 1, 0, 1),
(75, 'chocolate', 'Chocolate', -1, 0, 1),
(76, 'silent', 'Silenced', -1, 0, 1),
(77, 'flashlight', 'Flashlight', -1, 0, 1),
(78, 'grip', 'Grip', -1, 0, 1),
(79, 'nightvision_scope', 'Night Vision Scope', -1, 0, 1),
(80, 'thermal_scope', 'Thermal Vision Scope', -1, 0, 1),
(81, 'extended_magazine', 'Extended Magazine', -1, 0, 1),
(82, 'very_extended_magazine', 'Very Extended Magazine', -1, 0, 1),
(83, 'scope', 'Scope', -1, 0, 1),
(84, 'advanced_scope', 'Advanced Scope', -1, 0, 1),
(85, 'yusuf', 'Luxury Skin', -1, 0, 1),
(86, 'lowrider', 'Lowrider Skin', -1, 0, 1),
(87, 'incendiary', 'Incendiary Bullets', -1, 0, 1),
(88, 'tracer_clip', 'Trackers Bullets', -1, 0, 1),
(89, 'hollow', 'Hollow Bullets', -1, 0, 1),
(90, 'fmj', 'Perforating Bullets', -1, 0, 1),
(91, 'lazer_scope', 'Lazer Scope', -1, 0, 1),
(92, 'compansator', 'Compensator', -1, 0, 1),
(93, 'barrel', 'Barrel', -1, 0, 1),
(100, 'drivers_license', 'Drivers License', -1, 0, 1),
(101, 'motorcycle_license', 'Motorcycle License', -1, 0, 1),
(102, 'commercial_license', 'Commerical Drivers License', -1, 0, 1),
(103, 'boating_license', 'Boating License', -1, 0, 1),
(104, 'taxi_license', 'Taxi License', -1, 0, 1),
(105, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1),
(106, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1),
(107, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1),
(108, 'hunting_license', 'Hunting License', -1, 0, 1),
(109, 'fishing_license', 'Fishing License', -1, 0, 1),
(110, 'diving_license', 'diving_license', -1, 0, 1),
(111, 'marriage_license', 'Marriage License', -1, 0, 1),
(112, 'pilot_license', 'Pilot License', -1, 0, 1),
(2010, 'armor', 'Bulletproof Vest', -1, 0, 1),
(2017, 'clip', 'Clip', -1, 0, 1),
(2018, 'binoculars', 'Binoculars', 1, 0, 1),
(2019, 'lsd', 'Lsd', -1, 0, 1),
(2020, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2021, 'lsd', 'Lsd', -1, 0, 1),
(2022, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2023, 'jager', 'Jägermeister', 5, 0, 1),
(2024, 'vodka', 'Vodka', 5, 0, 1),
(2025, 'rhum', 'Rhum', 5, 0, 1),
(2026, 'whisky', 'Whisky', 5, 0, 1),
(2027, 'tequila', 'Tequila', 5, 0, 1),
(2028, 'martini', 'Martini blanc', 5, 0, 1),
(2029, 'soda', 'Soda', 5, 0, 1),
(2030, 'jusfruit', 'Jus de fruits', 5, 0, 1),
(2031, 'icetea', 'Ice Tea', 5, 0, 1),
(2032, 'energy', 'Energy Drink', 5, 0, 1),
(2033, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(2034, 'limonade', 'Limonade', 5, 0, 1),
(2035, 'bolcacahuetes', 'Bol de cacahuètes', 5, 0, 1),
(2036, 'bolnoixcajou', 'Bol de noix de cajou', 5, 0, 1),
(2037, 'bolpistache', 'Bol de pistaches', 5, 0, 1),
(2038, 'bolchips', 'Bol de chips', 5, 0, 1),
(2039, 'saucisson', 'Saucisson', 5, 0, 1),
(2040, 'grapperaisin', 'Grappe de raisin', 5, 0, 1),
(2041, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(2042, 'golem', 'Golem', 5, 0, 1),
(2043, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(2044, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(2045, 'vodkafruit', 'Vodka-jus de fruits', 5, 0, 1),
(2046, 'rhumfruit', 'Rhum-jus de fruits', 5, 0, 1),
(2047, 'teqpaf', 'Teq\'paf', 5, 0, 1),
(2048, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(2049, 'mojito', 'Mojito', 5, 0, 1),
(2050, 'ice', 'Glaçon', 5, 0, 1),
(2051, 'mixapero', 'Mix Apéritif', 3, 0, 1),
(2052, 'metreshooter', 'Mètre de shooter', 3, 0, 1),
(2053, 'jagercerbere', 'Jäger Cerbère', 3, 0, 1),
(2054, 'menthe', 'Feuille de menthe', 10, 0, 1),
(2055, 'nitro', 'Nitroso', -1, 0, 1),
(2056, 'fish', 'Fish', -1, 0, 1),
(2057, 'fishingrod', 'Fishing rod', 2, 0, 1),
(2058, 'bait', 'Bait', 20, 0, 1),
(2064, 'coffee', 'Café', -1, 0, 1),
(2069, 'plongee1', 'Plongee courte', -1, 0, 1),
(2070, 'plongee2', 'Plongee longue', -1, 0, 1),
(2073, 'contrat', 'Salvage', 15, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `J_Time` int(10) NOT NULL,
  `J_Cell` varchar(5) COLLATE utf8mb4_bin NOT NULL,
  `Jailer` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `Jailer_ID` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'unemployed', 0),
(5, 'realestateagent', 'Real Estate', 1),
(6, 'slaughterer', 'Butcher', 0),
(7, 'fisherman', 'Fisherman', 0),
(8, 'miner', 'Miner', 0),
(9, 'lumberjack', 'Lumberjack', 0),
(10, 'fuel', 'Refiner', 0),
(11, 'reporter', '9 News', 1),
(12, 'textil', 'Cloth Maker', 0),
(13, 'taxi', 'Taxi/Uber', 0),
(14, 'cardealer', 'Car Dealer', 1),
(15, 'banker', 'Banker', 1),
(20, 'security', 'Commercial City Security', 0),
(21, 'balla', 'balla', 1),
(22, 'family', 'family', 1),
(23, 'LOST MC', 'LOST MC', 1),
(24, 'vagos', 'vagos', 1),
(25, 'bus', 'BlueHound', 0),
(29, 'pizza', 'Pizza Sluts', 0),
(30, 'fire', 'Littleton Fire', 1),
(31, 'mecano', 'Mechanic', 1),
(32, 'brinks', 'Brinks', 0),
(33, 'coastguard', 'coastguard', 1),
(34, 'deliverer', 'Amazon', 0),
(36, 'gopostal', 'CSPS', 0),
(37, 'trucker', 'Trucker', 0),
(38, 'unicorn', 'Unicorn', 0),
(39, 'airlines', 'DIA', 0),
(40, 'banksecurity', 'banksecurity', 0),
(41, 'garbage', 'CC Sanitation', 0),
(42, 'foodtruck', 'Foodtruck', 1),
(43, 'avocat', 'Advokat', 0),
(44, 'police', 'LEO', 1),
(45, 'ambulance', 'AMR', 1),
(46, 'staff', 'Staff', 1),
(47, 'fork', 'Forklift', 1),
(50, 'journaliste', '9News', 0),
(51, 'Salvage', 'Salvage', 0),
(52, 'parking', 'Parking Enforcement', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'rsa', 'RSA', 100, '{}', '{}'),
(16, 'realestateagent', 0, 'location', 'Location', 350, '{}', '{}'),
(17, 'realestateagent', 1, 'vendeur', 'Seller', 550, '{}', '{}'),
(18, 'realestateagent', 2, 'gestion', 'Management', 40, '{}', '{}'),
(19, 'realestateagent', 3, 'boss', 'Boss', 0, '{}', '{}'),
(20, 'lumberjack', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(21, 'fisherman', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(22, 'fuel', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(23, 'reporter', 0, 'employee', 'Employee', 1000, '{}', '{}'),
(24, 'textil', 0, 'interim', 'Employee', 1000, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(25, 'miner', 0, 'interim', 'Employee', 1000, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(26, 'slaughterer', 0, 'interim', 'Employee', 1000, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(27, 'taxi', 0, 'driver', 'Driver', 500, '{}', '{}'),
(32, 'cardealer', 0, 'recruit', 'Recruit', 750, '{}', '{}'),
(33, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
(34, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
(35, 'cardealer', 3, 'boss', 'Boss', 100, '{}', '{}'),
(36, 'banker', 0, 'advisor', 'Adviser', 350, '{}', '{}'),
(37, 'banker', 1, 'banker', 'Banker', 550, '{}', '{}'),
(38, 'banker', 2, 'business_banker', 'Business Banker', 750, '{}', '{}'),
(39, 'banker', 3, 'trader', 'Trader', 800, '{}', '{}'),
(40, 'banker', 4, 'boss', 'Boss', 1000, '{}', '{}'),
(54, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(55, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(63, 'security', 0, 'Basic noob', 'Basic Guard', 200, '{}', '{}'),
(64, 'balla', 0, 'balla', 'balla', 600, '{}', '{}'),
(65, 'family', 0, 'family', 'family', 600, '{}', '{}'),
(66, 'LOST MC', 0, 'LOST MC', 'LOST MC', 600, '{}', '{}'),
(67, 'vagos', 0, 'vagos', 'vagos', 600, '{}', '{}'),
(70, 'bus', 0, 'employee', 'Bus Driver', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(74, 'pizza', 0, 'employee', 'Pizza Bitch', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,jobs\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(78, 'fire', 3, 'boss', 'Commandant', 80, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":41,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":8,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":21,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"hair_color_1\":10,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"shoes\":24,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(79, 'mecano', 0, 'recruit', 'Recruit', 12, '{}', '{}'),
(80, 'mecano', 1, 'novice', 'Novice', 24, '{}', '{}'),
(81, 'mecano', 2, 'experimente', 'Experimente', 36, '{}', '{}'),
(82, 'mecano', 3, 'chief', 'Chef d\'équipe', 48, '{}', '{}'),
(83, 'mecano', 4, 'boss', 'Patron', 0, '{}', '{}'),
(84, 'brinks', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(85, 'coastguard', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(86, 'deliverer', 0, 'employee', 'Employee', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(91, 'gopostal', 0, 'employee', 'Sedex', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(92, 'trucker', 0, 'employee', 'Employé', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(93, 'unicorn', 0, 'barman', 'Barman', 300, '{}', '{}'),
(94, 'unicorn', 1, 'dancer', 'Danseur', 300, '{}', '{}'),
(95, 'unicorn', 2, 'viceboss', 'Co-gérant', 500, '{}', '{}'),
(96, 'unicorn', 3, 'boss', 'Gérant', 600, '{}', '{}'),
(97, 'airlines', 0, 'recrue', 'Recrue', 30, '{}', '{}'),
(98, 'airlines', 1, 'chauffeur', 'Chauffeur', 40, '{}', '{}'),
(99, 'airlines', 2, 'pilote', 'Pilote', 50, '{}', '{}'),
(100, 'airlines', 3, 'gerant', 'Gerant', 60, '{}', '{}'),
(101, 'airlines', 4, 'boss', 'Patron', 0, '{}', '{}'),
(102, 'banksecurity', 0, 'employee', 'banksecurity', 200, '{\"tshirt_1\":60,\"torso_1\":130,\"arms\":31,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}', '{\"tshirt_1\":60,\"torso_1\":0,\"arms\":68,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}'),
(103, 'garbage', 0, 'employee', 'Employee', 750, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(104, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(105, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(106, 'avocat', 0, 'boss', 'Patron', 500, '', ''),
(107, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(108, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(109, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(110, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(111, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(112, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(113, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(114, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(115, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(116, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(117, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(118, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(119, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(120, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(121, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(122, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(123, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(124, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(125, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(126, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(127, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(128, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(129, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(130, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(131, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(132, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(133, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(134, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(135, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(136, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(137, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(138, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(139, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(140, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(141, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(142, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(143, 'police', 36, 'boss', 'Deputy Commissioner', 2000, '{}', '{}'),
(144, 'police', 37, 'boss', 'Commissioner', 2000, '{}', '{}'),
(145, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(146, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(147, 'fire', 0, 'cadet', 'Cadet', 100, '{\"tshirt_1\":57,\"torso_1\":55,\"arms\":0,\"pants_1\":35,\"glasses\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":5,\"face\":19,\"glasses_2\":1,\"torso_2\":0,\"shoes\":24,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":8}', '{\"tshirt_1\":34,\"torso_1\":48,\"shoes\":24,\"pants_1\":34,\"torso_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"glasses\":0,\"helmet_2\":0,\"hair_2\":3,\"face\":21,\"decals_1\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"arms\":14,\"hair_color_1\":10,\"tshirt_2\":0,\"helmet_1\":57}'),
(148, 'fire', 1, 'probationary', 'Probationary', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":0,\"decals_1\":8,\"torso_2\":0,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"hair_color_1\":5,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":14,\"pants_1\":34,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"shoes\":24,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"face\":21,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"hair_color_1\":10,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(159, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(160, 'fire', 3, 'firefighter', 'Firefighter', 200, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(161, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(162, 'fire', 5, 'captain', 'Captain', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(163, 'fire', 6, 'sharkone', 'Shark One', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(164, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(165, 'fire', 8, 'fto', 'FTO', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(166, 'fire', 9, 'hr', 'HR', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(167, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(168, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(169, 'fire', 12, 'boss', 'Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(170, 'ambulance', 0, 'cadet', 'Cadet', 100, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(171, 'ambulance', 1, 'probationary', 'Probationary', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(172, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(173, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(174, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(175, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(176, 'ambulance', 6, 'medone', 'Med One', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(178, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(179, 'ambulance', 8, 'fto', 'FTO', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(181, 'ambulance', 10, 'medicalchief', 'Medical Chief', 400, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(182, 'ambulance', 11, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(183, 'ambulance', 12, 'boss', 'Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(184, 'ambulance', 13, 'boss', 'Medical Examiner', 0, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(185, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(186, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(187, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(188, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(189, 'fork', 0, 'employee', 'Operator', 20, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(192, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(193, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(194, 'journaliste', 2, 'investigator', 'Enquêteur', 400, '{}', '{}'),
(195, 'journaliste', 3, 'boss', 'Rédac\' chef', 450, '{}', '{}'),
(196, 'Salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(197, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(198, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(199, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(200, 'ambulance', 9, 'phrn', 'PreHospital Registered Nurse', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Weapon license'),
(6, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('Char1:11000010a01bdb9', 'FMJ 423', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"FMJ 423\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":4.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'GSV 723', '{\"modArchCover\":-1,\"modLivery\":0,\"model\":353883353,\"modRearBumper\":-1,\"windowTint\":-1,\"dirtLevel\":1.0,\"modTransmission\":-1,\"color2\":0,\"modSpeakers\":-1,\"pearlescentColor\":112,\"health\":1000,\"modFrontWheels\":-1,\"modFender\":-1,\"modAirFilter\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modAerials\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"modExhaust\":-1,\"modSideSkirt\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"plate\":\"GSV 723\",\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modBrakes\":-1,\"modGrille\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"modHorns\":-1,\"modSeats\":-1,\"modDoorSpeaker\":-1,\"extras\":[],\"modEngineBlock\":-1,\"modAPlate\":-1,\"modXenon\":false,\"color1\":134,\"modPlateHolder\":-1,\"modOrnaments\":-1,\"modTrimA\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"modDial\":-1,\"modFrame\":-1,\"wheels\":0,\"modSmokeEnabled\":false,\"modVanityPlate\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDashboard\":-1,\"wheelColor\":156,\"modHood\":-1,\"modShifterLeavers\":-1,\"modEngine\":-1}', 'helicopter', 'police', 1),
('Char1:11000010a01bdb9', 'OLQ 222', '{\"pearlescentColor\":134,\"modRoof\":-1,\"modAirFilter\":-1,\"modSeats\":-1,\"modFender\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"modAerials\":-1,\"modHood\":-1,\"windowTint\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTank\":-1,\"dirtLevel\":1.0,\"color2\":134,\"modArchCover\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"extras\":[],\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modArmor\":-1,\"modSideSkirt\":-1,\"modXenon\":false,\"wheelColor\":134,\"modSpeakers\":-1,\"color1\":134,\"modTurbo\":false,\"modFrontBumper\":-1,\"neonColor\":[255,0,255],\"modAPlate\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"model\":-576293386,\"modGrille\":-1,\"modPlateHolder\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"health\":1000,\"modWindows\":-1,\"plateIndex\":4,\"wheels\":3,\"modStruts\":-1,\"modDial\":-1,\"modTransmission\":-1,\"modTrimB\":-1,\"plate\":\"OLQ 222\",\"modShifterLeavers\":-1,\"modLivery\":7,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'QVU 418', '{\"health\":0,\"modXenon\":false,\"modSpoilers\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modExhaust\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"extras\":[],\"modRightFender\":-1,\"modTrimB\":-1,\"wheels\":0,\"modShifterLeavers\":-1,\"neonColor\":[0,0,0],\"modTurbo\":false,\"color1\":0,\"modRoof\":-1,\"plateIndex\":-1,\"tyreSmokeColor\":[0,0,0],\"modHydrolic\":-1,\"modTransmission\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modSmokeEnabled\":false,\"plate\":\"QVU 418\",\"modTrunk\":-1,\"modOrnaments\":-1,\"modStruts\":-1,\"modAerials\":-1,\"model\":0,\"dirtLevel\":0.0,\"modHood\":-1,\"color2\":0,\"modFender\":-1,\"modRearBumper\":-1,\"pearlescentColor\":0,\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modHorns\":-1,\"modArchCover\":-1,\"modFrame\":-1,\"modFrontBumper\":-1,\"windowTint\":-1,\"wheelColor\":0,\"modEngine\":-1,\"modLivery\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modSeats\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false]}', 'car', 'ambulance', 1),
('Char1:11000010a01bdb9', 'TQP 485', '{\"modFender\":-1,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modGrille\":-1,\"modFrame\":-1,\"modXenon\":false,\"modArmor\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"dirtLevel\":0.0,\"modSteeringWheel\":-1,\"modDial\":-1,\"model\":178383100,\"modEngine\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"health\":1000,\"extras\":[],\"modBrakes\":-1,\"modHydrolic\":-1,\"modLivery\":0,\"tyreSmokeColor\":[255,255,255],\"modSpeakers\":-1,\"modRoof\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":false,\"pearlescentColor\":134,\"modExhaust\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modHood\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modHorns\":-1,\"color1\":111,\"modSideSkirt\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"color2\":1,\"modTurbo\":false,\"modTransmission\":-1,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"plate\":\"TQP 485\",\"modSuspension\":-1,\"modTank\":-1,\"wheels\":6,\"modRightFender\":-1,\"plateIndex\":4,\"modAPlate\":-1}', 'car', 'police', 0),
('Char1:11000010a01bdb9', 'UGA 061', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"UGA 061\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":3.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'UGU 071', '{\"modBackWheels\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"modAPlate\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modRoof\":-1,\"color1\":111,\"health\":1000,\"modGrille\":-1,\"modAirFilter\":-1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modFender\":-1,\"modTransmission\":-1,\"modSeats\":-1,\"modSuspension\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modEngine\":-1,\"wheelColor\":156,\"modWindows\":-1,\"dirtLevel\":11.0,\"modDashboard\":-1,\"neonEnabled\":[false,false,false,false],\"modVanityPlate\":-1,\"modXenon\":false,\"modShifterLeavers\":-1,\"modDial\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modAerials\":-1,\"color2\":1,\"modFrontBumper\":-1,\"wheels\":6,\"extras\":[],\"pearlescentColor\":134,\"modBrakes\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modTank\":-1,\"modHorns\":-1,\"plate\":\"UGU 071\",\"model\":178383100,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modHood\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'VFQ 240', '{\"modRightFender\":-1,\"modDial\":-1,\"plateIndex\":4,\"modAPlate\":-1,\"dirtLevel\":7.0,\"model\":-576293386,\"modWindows\":-1,\"modTrunk\":-1,\"modBrakes\":-1,\"modFender\":-1,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"modTransmission\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modHood\":-1,\"modExhaust\":-1,\"modRearBumper\":-1,\"modLivery\":4,\"modBackWheels\":-1,\"color1\":134,\"wheelColor\":134,\"modSeats\":-1,\"modRoof\":-1,\"modPlateHolder\":-1,\"modShifterLeavers\":-1,\"wheels\":3,\"modArchCover\":-1,\"modAerials\":-1,\"color2\":134,\"extras\":[],\"modEngineBlock\":-1,\"tyreSmokeColor\":[255,255,255],\"modHydrolic\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"modSpeakers\":-1,\"modFrame\":-1,\"modTank\":-1,\"plate\":\"VFQ 240\",\"modAirFilter\":-1,\"modOrnaments\":-1,\"modGrille\":-1,\"modTurbo\":false,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":134,\"modSmokeEnabled\":false,\"modSideSkirt\":-1,\"modHorns\":-1,\"modDashboard\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1}', 'car', 'ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15),
(7, 'LTDgasoline', 'fixkit', 0),
(8, 'TwentyFourSeven', 'binoculars', 1000),
(9, 'RobsLiquor', 'binoculars', 1000),
(10, 'LTDgasoline', 'binoculars', 1000),
(11, 'LTDgasoline', 'plongee1', 250),
(12, 'RobsLiquor', 'plongee1', 250),
(13, 'TwentyFourSeven', 'plongee1', 250),
(14, 'LTDgasoline', 'plongee2', 350),
(15, 'RobsLiquor', 'plongee2', 350),
(16, 'TwentyFourSeven', 'plongee2', 350),
(17, 'LTDgasoline', 'chocolate', 0),
(18, 'PDStation', 'coffee', 1),
(19, 'PDStation', 'donut', 1),
(20, 'PDStation', 'clip', 1),
(21, 'PDStation', 'armor', 1),
(22, 'DMV', 'drivers_license', 80),
(23, 'DMV', 'motorcycle_license', 80),
(24, 'DMV', 'commercial_license', 175),
(25, 'DMV', 'boating_license', 40),
(26, 'DMV', 'taxi_license', 175),
(27, 'City_Hall', 'weapons_license1', 100),
(28, 'City_Hall', 'weapons_license2', 200),
(29, 'City_Hall', 'weapons_license3', 300),
(30, 'City_Hall', 'hunting_license', 100),
(31, 'City_Hall', 'fishing_license', 100),
(32, 'City_Hall', 'diving_license', 50),
(33, 'City_Hall', 'marrage_license', 5000),
(34, 'DMV', 'pilot_license', 500),
(35, 'PDStation', 'medikit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `society` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `ID` int(255) DEFAULT NULL,
  `steamid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8mb4_bin DEFAULT 'user',
  `is_dead` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `last_property`, `phone_number`, `status`, `ID`, `steamid`, `rank`, `is_dead`) VALUES
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(138, 'steam:11000010a256910', 'black_money', 0),
(139, 'steam:110000117d5e2b7', 'black_money', 0),
(140, 'steam:1100001123cd531', 'black_money', 0),
(141, 'steam:1100001326230ef', 'black_money', 0),
(142, 'steam:11000010a266dc7', 'black_money', 0),
(143, 'steam:11000010e80a075', 'black_money', 0),
(144, 'steam:110000132b7eb1f', 'black_money', 0),
(145, 'steam:11000010b16012a', 'black_money', 0),
(146, 'steam:1100001184e4440', 'black_money', 0),
(147, 'steam:11000010f75c618', 'black_money', 0),
(148, 'steam:110000104ec6862', 'black_money', 0),
(149, 'steam:11000010c991cb2', 'black_money', 0),
(150, 'steam:11000010cd0b522', 'black_money', 0),
(151, 'steam:11000010b7982b5', 'black_money', 0),
(152, 'steam:11000010548afc9', 'black_money', 0),
(153, 'steam:1100001145029fc', 'black_money', 0),
(154, 'steam:110000116e20aa8', 'black_money', 0),
(155, 'steam:110000105318505', 'black_money', 0),
(156, 'steam:110000106def124', 'black_money', 0),
(157, 'steam:110000112206eef', 'black_money', 0),
(158, 'steam:110000106141cfc', 'black_money', 0),
(159, 'steam:110000119f838fa', 'black_money', 0),
(160, 'steam:110000107f1f2d6', 'black_money', 0),
(161, 'Char1:11000010a01bdb9', 'black_money', 49990000),
(162, 'steam:1100001068ef13c', 'black_money', 0),
(163, 'steam:110000132580eb0', 'black_money', 5000000),
(164, 'steam:110000112f07694', 'black_money', 0),
(165, 'steam:1100001042b46e1', 'black_money', 0),
(166, 'steam:11000011820eeac', 'black_money', 0),
(167, 'steam:11000011258948d', 'black_money', 0),
(168, 'steam:110000114590956', 'black_money', 0),
(169, 'steam:110000115c5ddc9', 'black_money', 0),
(170, 'steam:110000109660ef9', 'black_money', 0),
(171, 'steam:11000010c6acdce', 'black_money', 0),
(172, 'steam:110000106a5790d', 'black_money', 0),
(173, 'steam:11000010b0a1bc3', 'black_money', 0),
(174, 'steam:11000011b78df95', 'black_money', 0),
(175, 'steam:110000135c88cca', 'black_money', 0),
(176, 'steam:11000010473daf1', 'black_money', 0),
(177, 'steam:110000103b294e0', 'black_money', 0),
(178, 'steam:110000104e0ed21', 'black_money', 0),
(179, 'steam:1100001238ac476', 'black_money', 0),
(180, 'steam:11000011742efb1', 'black_money', 0),
(181, 'steam:110000112969e8f', 'black_money', 0),
(182, 'steam:110000134857568', 'black_money', 0),
(183, 'steam:110000131f0d83c', 'black_money', 0),
(184, 'steam:110000110931da8', 'black_money', 0),
(185, 'steam:1100001185894b3', 'black_money', 0),
(186, 'steam:11000011a15fea8', 'black_money', 0),
(187, 'steam:1100001183ded9e', 'black_money', 0),
(188, 'steam:1100001183ded9e', 'black_money', 0),
(189, 'steam:1100001183ded9e', 'black_money', 0),
(190, 'steam:1100001183ded9e', 'black_money', 0),
(191, 'steam:1100001183ded9e', 'black_money', 0),
(192, 'steam:1100001183ded9e', 'black_money', 0),
(193, 'steam:1100001183ded9e', 'black_money', 0),
(194, 'steam:1100001183ded9e', 'black_money', 0),
(195, 'steam:110000115815ad6', 'black_money', 0),
(196, 'steam:110000115815ad6', 'black_money', 0),
(197, 'steam:110000115815ad6', 'black_money', 0),
(198, 'steam:110000115815ad6', 'black_money', 0),
(199, 'steam:110000115815ad6', 'black_money', 0),
(200, 'steam:110000115815ad6', 'black_money', 0),
(201, 'steam:110000115815ad6', 'black_money', 0),
(202, 'steam:110000115815ad6', 'black_money', 0),
(203, 'Char2:11000010a01bdb9', 'black_money', 0),
(204, 'Char2:11000010a01bdb9', 'black_money', 0),
(205, 'Char2:11000010a01bdb9', 'black_money', 0),
(206, 'Char2:11000010a01bdb9', 'black_money', 0),
(207, 'Char2:11000010a01bdb9', 'black_money', 0),
(208, 'Char2:11000010a01bdb9', 'black_money', 0),
(209, 'Char2:11000010a01bdb9', 'black_money', 0),
(210, 'Char2:11000010a01bdb9', 'black_money', 0),
(211, 'steam:11000010a01bdb9', 'black_money', 0),
(212, 'steam:11000010a01bdb9', 'black_money', 0),
(213, 'steam:11000010a01bdb9', 'black_money', 0),
(214, 'steam:11000010a01bdb9', 'black_money', 0),
(215, 'steam:11000010a01bdb9', 'black_money', 0),
(216, 'steam:11000010a01bdb9', 'black_money', 0),
(217, 'steam:11000010a01bdb9', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_contacts`
--

INSERT INTO `user_contacts` (`id`, `identifier`, `name`, `number`) VALUES
(1, 'steam:110000112969e8f', 'K9', 84053),
(2, 'steam:110000132580eb0', 'J-bomb', 56216);

-- --------------------------------------------------------

--
-- Table structure for table `user_heli`
--

CREATE TABLE `user_heli` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `heli_name` varchar(255) DEFAULT NULL,
  `heli_model` varchar(255) DEFAULT NULL,
  `heli_price` int(60) DEFAULT NULL,
  `heli_plate` varchar(255) DEFAULT NULL,
  `heli_state` varchar(255) DEFAULT NULL,
  `heli_colorprimary` varchar(255) DEFAULT NULL,
  `heli_colorsecondary` varchar(255) DEFAULT NULL,
  `heli_pearlescentcolor` varchar(255) DEFAULT NULL,
  `heli_wheelcolor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(2, 'steam:11000010a01bdb9', 'vodka', 0),
(3, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(4, 'steam:11000010a01bdb9', 'clothe', 0),
(5, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(6, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(7, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(8, 'steam:11000010a01bdb9', 'clothe', 0),
(9, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(10, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(11, 'steam:11000010a01bdb9', 'clothe', 0),
(12, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(13, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(14, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(15, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(16, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(17, 'steam:11000010a01bdb9', 'clothe', 0),
(18, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(19, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(20, 'steam:11000010a01bdb9', 'clothe', 0),
(21, 'steam:11000010a01bdb9', 'vodka', 0),
(22, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(23, 'steam:11000010a01bdb9', 'vodka', 0),
(24, 'steam:11000010a01bdb9', 'vodka', 0),
(25, 'steam:11000010a01bdb9', 'clothe', 0),
(26, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(27, 'steam:11000010a01bdb9', 'vodka', 0),
(28, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(29, 'steam:11000010a01bdb9', 'vodka', 0),
(30, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(31, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(32, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(33, 'steam:11000010a01bdb9', 'silent', 0),
(34, 'steam:11000010a01bdb9', 'jusfruit', 0),
(35, 'steam:11000010a01bdb9', 'coffee', 0),
(36, 'steam:11000010a01bdb9', 'silent', 0),
(37, 'steam:11000010a01bdb9', 'water', 0),
(38, 'steam:11000010a01bdb9', 'jusfruit', 0),
(39, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(40, 'steam:11000010a01bdb9', 'water', 0),
(41, 'steam:11000010a01bdb9', 'silent', 0),
(42, 'steam:11000010a01bdb9', 'water', 0),
(43, 'steam:11000010a01bdb9', 'silent', 0),
(44, 'steam:11000010a01bdb9', 'coffee', 0),
(45, 'steam:11000010a01bdb9', 'coffee', 0),
(46, 'steam:11000010a01bdb9', 'silent', 0),
(47, 'steam:11000010a01bdb9', 'coffee', 0),
(48, 'steam:11000010a01bdb9', 'silent', 0),
(49, 'steam:11000010a01bdb9', 'water', 0),
(50, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(51, 'steam:11000010a01bdb9', 'coffee', 0),
(52, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(53, 'steam:11000010a01bdb9', 'jusfruit', 0),
(54, 'steam:11000010a01bdb9', 'jusfruit', 0),
(55, 'steam:11000010a01bdb9', 'jusfruit', 0),
(56, 'steam:11000010a01bdb9', 'water', 0),
(57, 'steam:11000010a01bdb9', 'coffee', 0),
(58, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(59, 'steam:11000010a01bdb9', 'water', 0),
(60, 'steam:11000010a01bdb9', 'jusfruit', 0),
(61, 'steam:11000010a01bdb9', 'yusuf', 0),
(62, 'steam:11000010a01bdb9', 'binoculars', 0),
(63, 'steam:11000010a01bdb9', 'yusuf', 0),
(64, 'steam:11000010a01bdb9', 'yusuf', 0),
(65, 'steam:11000010a01bdb9', 'binoculars', 0),
(66, 'steam:11000010a01bdb9', 'yusuf', 0),
(67, 'steam:11000010a01bdb9', 'yusuf', 0),
(68, 'steam:11000010a01bdb9', 'lowrider', 0),
(69, 'steam:11000010a01bdb9', 'jusfruit', 0),
(70, 'steam:11000010a01bdb9', 'jusfruit', 0),
(71, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(72, 'steam:11000010a01bdb9', 'silent', 0),
(73, 'steam:11000010a01bdb9', 'coffee', 0),
(74, 'steam:11000010a01bdb9', 'water', 0),
(75, 'steam:11000010a01bdb9', 'water', 0),
(76, 'steam:11000010a01bdb9', 'yusuf', 0),
(77, 'steam:11000010a01bdb9', 'coffee', 0),
(78, 'steam:11000010a01bdb9', 'silent', 0),
(79, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(80, 'steam:11000010a01bdb9', 'lowrider', 0),
(81, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(82, 'steam:11000010a01bdb9', 'tequila', 0),
(83, 'steam:11000010a01bdb9', 'lowrider', 0),
(84, 'steam:11000010a01bdb9', 'carotool', 0),
(85, 'steam:11000010a01bdb9', 'binoculars', 0),
(86, 'steam:11000010a01bdb9', 'fish', 0),
(87, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(88, 'steam:11000010a01bdb9', 'binoculars', 0),
(89, 'steam:11000010a01bdb9', 'tequila', 0),
(90, 'steam:11000010a01bdb9', 'lowrider', 0),
(91, 'steam:11000010a01bdb9', 'binoculars', 0),
(92, 'steam:11000010a01bdb9', 'tequila', 0),
(93, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(94, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(95, 'steam:11000010a01bdb9', 'fish', 0),
(96, 'steam:11000010a01bdb9', 'fish', 0),
(97, 'steam:11000010a01bdb9', 'binoculars', 0),
(98, 'steam:11000010a01bdb9', 'binoculars', 0),
(99, 'steam:11000010a01bdb9', 'fish', 0),
(100, 'steam:11000010a01bdb9', 'fish', 0),
(101, 'steam:11000010a01bdb9', 'tequila', 0),
(102, 'steam:11000010a01bdb9', 'yusuf', 0),
(103, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(104, 'steam:11000010a01bdb9', 'lowrider', 0),
(105, 'steam:11000010a01bdb9', 'lowrider', 0),
(106, 'steam:11000010a01bdb9', 'yusuf', 0),
(107, 'steam:11000010a01bdb9', 'fish', 0),
(108, 'steam:11000010a01bdb9', 'tequila', 0),
(109, 'steam:11000010a01bdb9', 'tequila', 0),
(110, 'steam:11000010a01bdb9', 'fish', 0),
(111, 'steam:11000010a01bdb9', 'tequila', 0),
(112, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(113, 'steam:11000010a01bdb9', 'binoculars', 0),
(114, 'steam:11000010a01bdb9', 'lowrider', 0),
(115, 'steam:11000010a01bdb9', 'fish', 0),
(116, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(117, 'steam:11000010a01bdb9', 'lowrider', 0),
(118, 'steam:11000010a01bdb9', 'tequila', 0),
(119, 'steam:11000010a01bdb9', 'carotool', 0),
(120, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(121, 'steam:11000010a01bdb9', 'contrat', 0),
(122, 'steam:11000010a01bdb9', 'carotool', 0),
(123, 'steam:11000010a01bdb9', 'contrat', 0),
(124, 'steam:11000010a01bdb9', 'martini', 0),
(125, 'steam:11000010a01bdb9', 'carotool', 0),
(126, 'steam:11000010a01bdb9', 'contrat', 0),
(127, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(128, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(129, 'steam:11000010a01bdb9', 'contrat', 0),
(130, 'steam:11000010a01bdb9', 'carotool', 0),
(131, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(132, 'steam:11000010a01bdb9', 'menthe', 0),
(133, 'steam:11000010a01bdb9', 'martini', 0),
(134, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(135, 'steam:11000010a01bdb9', 'contrat', 0),
(136, 'steam:11000010a01bdb9', 'contrat', 0),
(137, 'steam:11000010a01bdb9', 'menthe', 0),
(138, 'steam:11000010a01bdb9', 'martini', 0),
(139, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(140, 'steam:11000010a01bdb9', 'menthe', 0),
(141, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(142, 'steam:11000010a01bdb9', 'martini', 0),
(143, 'steam:11000010a01bdb9', 'martini', 0),
(144, 'steam:11000010a01bdb9', 'tacos', 0),
(145, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(146, 'steam:11000010a01bdb9', 'contrat', 0),
(147, 'steam:11000010a01bdb9', 'menthe', 0),
(148, 'steam:11000010a01bdb9', 'flashlight', 0),
(149, 'steam:11000010a01bdb9', 'carotool', 0),
(150, 'steam:11000010a01bdb9', 'martini', 0),
(151, 'steam:11000010a01bdb9', 'menthe', 0),
(152, 'steam:11000010a01bdb9', 'menthe', 0),
(153, 'steam:11000010a01bdb9', 'carotool', 0),
(154, 'steam:11000010a01bdb9', 'carotool', 0),
(155, 'steam:11000010a01bdb9', 'contrat', 0),
(156, 'steam:11000010a01bdb9', 'martini', 0),
(157, 'steam:11000010a01bdb9', 'martini', 0),
(158, 'steam:11000010a01bdb9', 'tacos', 0),
(159, 'steam:11000010a01bdb9', 'tacos', 0),
(160, 'steam:11000010a01bdb9', 'menthe', 0),
(161, 'steam:11000010a01bdb9', 'bolpistache', 0),
(162, 'steam:11000010a01bdb9', 'tacos', 0),
(163, 'steam:11000010a01bdb9', 'flashlight', 0),
(164, 'steam:11000010a01bdb9', 'boating_license', 0),
(165, 'steam:11000010a01bdb9', 'flashlight', 0),
(166, 'steam:11000010a01bdb9', 'tacos', 0),
(167, 'steam:11000010a01bdb9', 'bolpistache', 0),
(168, 'steam:11000010a01bdb9', 'tacos', 0),
(169, 'steam:11000010a01bdb9', 'boating_license', 0),
(170, 'steam:11000010a01bdb9', 'bolpistache', 0),
(171, 'steam:11000010a01bdb9', 'bolpistache', 0),
(172, 'steam:11000010a01bdb9', 'menthe', 0),
(173, 'steam:11000010a01bdb9', 'flashlight', 0),
(174, 'steam:11000010a01bdb9', 'flashlight', 0),
(175, 'steam:11000010a01bdb9', 'clip', 0),
(176, 'steam:11000010a01bdb9', 'clip', 0),
(177, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(178, 'steam:11000010a01bdb9', 'clip', 0),
(179, 'steam:11000010a01bdb9', 'bolpistache', 0),
(180, 'steam:11000010a01bdb9', 'bolpistache', 0),
(181, 'steam:11000010a01bdb9', 'bolpistache', 0),
(182, 'steam:11000010a01bdb9', 'clip', 0),
(183, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(184, 'steam:11000010a01bdb9', 'clip', 0),
(185, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(186, 'steam:11000010a01bdb9', 'boating_license', 0),
(187, 'steam:11000010a01bdb9', 'boating_license', 0),
(188, 'steam:11000010a01bdb9', 'tacos', 0),
(189, 'steam:11000010a01bdb9', 'boating_license', 0),
(190, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(191, 'steam:11000010a01bdb9', 'boating_license', 0),
(192, 'steam:11000010a01bdb9', 'boating_license', 0),
(193, 'steam:11000010a01bdb9', 'flashlight', 0),
(194, 'steam:11000010a01bdb9', 'clip', 0),
(195, 'steam:11000010a01bdb9', 'bolpistache', 0),
(196, 'steam:11000010a01bdb9', 'tacos', 0),
(197, 'steam:11000010a01bdb9', 'flashlight', 0),
(198, 'steam:11000010a01bdb9', 'flashlight', 0),
(199, 'steam:11000010a01bdb9', 'clip', 0),
(200, 'steam:11000010a01bdb9', 'boating_license', 0),
(201, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(202, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(203, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(204, 'steam:11000010a01bdb9', 'cola', 0),
(205, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(206, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(207, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(208, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(209, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(210, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(211, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(212, 'steam:11000010a01bdb9', 'bait', 0),
(213, 'steam:11000010a01bdb9', 'cola', 0),
(214, 'steam:11000010a01bdb9', 'clip', 0),
(215, 'steam:11000010a01bdb9', 'cola', 0),
(216, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(217, 'steam:11000010a01bdb9', 'bait', 0),
(218, 'steam:11000010a01bdb9', 'cola', 0),
(219, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(220, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(221, 'steam:11000010a01bdb9', 'cola', 0),
(222, 'steam:11000010a01bdb9', 'bait', 0),
(223, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(224, 'steam:11000010a01bdb9', 'bait', 0),
(225, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(226, 'steam:11000010a01bdb9', 'weed', 0),
(227, 'steam:11000010a01bdb9', 'weed', 0),
(228, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(229, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(230, 'steam:11000010a01bdb9', 'cola', 0),
(231, 'steam:11000010a01bdb9', 'bait', 0),
(232, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(233, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(234, 'steam:11000010a01bdb9', 'fishingrod', 0),
(235, 'steam:11000010a01bdb9', 'fmj', 0),
(236, 'steam:11000010a01bdb9', 'weed', 0),
(237, 'steam:11000010a01bdb9', 'weed', 0),
(238, 'steam:11000010a01bdb9', 'bait', 0),
(239, 'steam:11000010a01bdb9', 'fmj', 0),
(240, 'steam:11000010a01bdb9', 'bait', 0),
(241, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(242, 'steam:11000010a01bdb9', 'weed', 0),
(243, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(244, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(245, 'steam:11000010a01bdb9', 'bait', 0),
(246, 'steam:11000010a01bdb9', 'fishingrod', 0),
(247, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(248, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(249, 'steam:11000010a01bdb9', 'fishingrod', 0),
(250, 'steam:11000010a01bdb9', 'fishingrod', 0),
(251, 'steam:11000010a01bdb9', 'fmj', 0),
(252, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(253, 'steam:11000010a01bdb9', 'cola', 0),
(254, 'steam:11000010a01bdb9', 'weed', 0),
(255, 'steam:11000010a01bdb9', 'fmj', 0),
(256, 'steam:11000010a01bdb9', 'cola', 0),
(257, 'steam:11000010a01bdb9', 'fishingrod', 0),
(258, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(259, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(260, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(261, 'steam:11000010a01bdb9', 'fishingrod', 0),
(262, 'steam:11000010a01bdb9', 'fmj', 0),
(263, 'steam:11000010a01bdb9', 'weed', 0),
(264, 'steam:11000010a01bdb9', 'fmj', 0),
(265, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(266, 'steam:11000010a01bdb9', 'weed', 0),
(267, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(268, 'steam:11000010a01bdb9', 'fabric', 0),
(269, 'steam:11000010a01bdb9', 'fishingrod', 0),
(270, 'steam:11000010a01bdb9', 'fabric', 0),
(271, 'steam:11000010a01bdb9', 'fishingrod', 0),
(272, 'steam:11000010a01bdb9', 'plongee2', 0),
(273, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(274, 'steam:11000010a01bdb9', 'fmj', 0),
(275, 'steam:11000010a01bdb9', 'donut', 0),
(276, 'steam:11000010a01bdb9', 'incendiary', 0),
(277, 'steam:11000010a01bdb9', 'incendiary', 0),
(278, 'steam:11000010a01bdb9', 'fabric', 0),
(279, 'steam:11000010a01bdb9', 'incendiary', 0),
(280, 'steam:11000010a01bdb9', 'donut', 0),
(281, 'steam:11000010a01bdb9', 'incendiary', 0),
(282, 'steam:11000010a01bdb9', 'burger', 0),
(283, 'steam:11000010a01bdb9', 'donut', 0),
(284, 'steam:11000010a01bdb9', 'fabric', 0),
(285, 'steam:11000010a01bdb9', 'burger', 0),
(286, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(287, 'steam:11000010a01bdb9', 'fabric', 0),
(288, 'steam:11000010a01bdb9', 'fabric', 0),
(289, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(290, 'steam:11000010a01bdb9', 'fmj', 0),
(291, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(292, 'steam:11000010a01bdb9', 'burger', 0),
(293, 'steam:11000010a01bdb9', 'incendiary', 0),
(294, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(295, 'steam:11000010a01bdb9', 'plongee2', 0),
(296, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(297, 'steam:11000010a01bdb9', 'donut', 0),
(298, 'steam:11000010a01bdb9', 'burger', 0),
(299, 'steam:11000010a01bdb9', 'incendiary', 0),
(300, 'steam:11000010a01bdb9', 'fabric', 0),
(301, 'steam:11000010a01bdb9', 'plongee2', 0),
(302, 'steam:11000010a01bdb9', 'fabric', 0),
(303, 'steam:11000010a01bdb9', 'donut', 0),
(304, 'steam:11000010a01bdb9', 'donut', 0),
(305, 'steam:11000010a01bdb9', 'burger', 0),
(306, 'steam:11000010a01bdb9', 'incendiary', 0),
(307, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(308, 'steam:11000010a01bdb9', 'plongee2', 0),
(309, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(310, 'steam:11000010a01bdb9', 'burger', 0),
(311, 'steam:11000010a01bdb9', 'incendiary', 0),
(312, 'steam:11000010a01bdb9', 'plongee2', 0),
(313, 'steam:11000010a01bdb9', 'essence', 0),
(314, 'steam:11000010a01bdb9', 'iron', 0),
(315, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(316, 'steam:11000010a01bdb9', 'plongee1', 0),
(317, 'steam:11000010a01bdb9', 'plongee1', 0),
(318, 'steam:11000010a01bdb9', 'donut', 0),
(319, 'steam:11000010a01bdb9', 'mixapero', 0),
(320, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(321, 'steam:11000010a01bdb9', 'iron', 0),
(322, 'steam:11000010a01bdb9', 'burger', 0),
(323, 'steam:11000010a01bdb9', 'essence', 0),
(324, 'steam:11000010a01bdb9', 'burger', 0),
(325, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(326, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(327, 'steam:11000010a01bdb9', 'plongee1', 0),
(328, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(329, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(330, 'steam:11000010a01bdb9', 'plongee2', 0),
(331, 'steam:11000010a01bdb9', 'iron', 0),
(332, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(333, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(334, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(335, 'steam:11000010a01bdb9', 'donut', 0),
(336, 'steam:11000010a01bdb9', 'essence', 0),
(337, 'steam:11000010a01bdb9', 'essence', 0),
(338, 'steam:11000010a01bdb9', 'essence', 0),
(339, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(340, 'steam:11000010a01bdb9', 'mixapero', 0),
(341, 'steam:11000010a01bdb9', 'plongee2', 0),
(342, 'steam:11000010a01bdb9', 'iron', 0),
(343, 'steam:11000010a01bdb9', 'plongee1', 0),
(344, 'steam:11000010a01bdb9', 'essence', 0),
(345, 'steam:11000010a01bdb9', 'plongee2', 0),
(346, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(347, 'steam:11000010a01bdb9', 'plongee1', 0),
(348, 'steam:11000010a01bdb9', 'plongee1', 0),
(349, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(350, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(351, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(352, 'steam:11000010a01bdb9', 'mixapero', 0),
(353, 'steam:11000010a01bdb9', 'mojito', 0),
(354, 'steam:11000010a01bdb9', 'diamond', 0),
(355, 'steam:11000010a01bdb9', 'drpepper', 0),
(356, 'steam:11000010a01bdb9', 'icetea', 0),
(357, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(358, 'steam:11000010a01bdb9', 'icetea', 0),
(359, 'steam:11000010a01bdb9', 'essence', 0),
(360, 'steam:11000010a01bdb9', 'mixapero', 0),
(361, 'steam:11000010a01bdb9', 'mojito', 0),
(362, 'steam:11000010a01bdb9', 'mixapero', 0),
(363, 'steam:11000010a01bdb9', 'essence', 0),
(364, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(365, 'steam:11000010a01bdb9', 'drpepper', 0),
(366, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(367, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(368, 'steam:11000010a01bdb9', 'mixapero', 0),
(369, 'steam:11000010a01bdb9', 'diamond', 0),
(370, 'steam:11000010a01bdb9', 'icetea', 0),
(371, 'steam:11000010a01bdb9', 'mixapero', 0),
(372, 'steam:11000010a01bdb9', 'mixapero', 0),
(373, 'steam:11000010a01bdb9', 'drpepper', 0),
(374, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(375, 'steam:11000010a01bdb9', 'iron', 0),
(376, 'steam:11000010a01bdb9', 'drpepper', 0),
(377, 'steam:11000010a01bdb9', 'drpepper', 0),
(378, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(379, 'steam:11000010a01bdb9', 'iron', 0),
(380, 'steam:11000010a01bdb9', 'plongee1', 0),
(381, 'steam:11000010a01bdb9', 'plongee1', 0),
(382, 'steam:11000010a01bdb9', 'iron', 0),
(383, 'steam:11000010a01bdb9', 'icetea', 0),
(384, 'steam:11000010a01bdb9', 'icetea', 0),
(385, 'steam:11000010a01bdb9', 'iron', 0),
(386, 'steam:11000010a01bdb9', 'drpepper', 0),
(387, 'steam:11000010a01bdb9', 'diamond', 0),
(388, 'steam:11000010a01bdb9', 'icetea', 0),
(389, 'steam:11000010a01bdb9', 'icetea', 0),
(390, 'steam:11000010a01bdb9', 'diamond', 0),
(391, 'steam:11000010a01bdb9', 'icetea', 0),
(392, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(393, 'steam:11000010a01bdb9', 'jager', 0),
(394, 'steam:11000010a01bdb9', 'jager', 0),
(395, 'steam:11000010a01bdb9', 'whool', 0),
(396, 'steam:11000010a01bdb9', 'jager', 0),
(397, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(398, 'steam:11000010a01bdb9', 'mojito', 0),
(399, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(400, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(401, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(402, 'steam:11000010a01bdb9', 'jager', 0),
(403, 'steam:11000010a01bdb9', 'mojito', 0),
(404, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(405, 'steam:11000010a01bdb9', 'mojito', 0),
(406, 'steam:11000010a01bdb9', 'whool', 0),
(407, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(408, 'steam:11000010a01bdb9', 'drpepper', 0),
(409, 'steam:11000010a01bdb9', 'mojito', 0),
(410, 'steam:11000010a01bdb9', 'mojito', 0),
(411, 'steam:11000010a01bdb9', 'whool', 0),
(412, 'steam:11000010a01bdb9', 'diamond', 0),
(413, 'steam:11000010a01bdb9', 'drpepper', 0),
(414, 'steam:11000010a01bdb9', 'whool', 0),
(415, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(416, 'steam:11000010a01bdb9', 'mojito', 0),
(417, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(418, 'steam:11000010a01bdb9', 'jager', 0),
(419, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(420, 'steam:11000010a01bdb9', 'diamond', 0),
(421, 'steam:11000010a01bdb9', 'diamond', 0),
(422, 'steam:11000010a01bdb9', 'whool', 0),
(423, 'steam:11000010a01bdb9', 'jager', 0),
(424, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(425, 'steam:11000010a01bdb9', 'diamond', 0),
(426, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(427, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(428, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(429, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(430, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(431, 'steam:11000010a01bdb9', 'whool', 0),
(432, 'steam:11000010a01bdb9', 'bread', 0),
(433, 'steam:11000010a01bdb9', 'coke', 0),
(434, 'steam:11000010a01bdb9', 'golem', 0),
(435, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(436, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(437, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(438, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(439, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(440, 'steam:11000010a01bdb9', 'whool', 0),
(441, 'steam:11000010a01bdb9', 'bread', 0),
(442, 'steam:11000010a01bdb9', 'coke', 0),
(443, 'steam:11000010a01bdb9', 'coke', 0),
(444, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(445, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(446, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(447, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(448, 'steam:11000010a01bdb9', 'bread', 0),
(449, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(450, 'steam:11000010a01bdb9', 'bread', 0),
(451, 'steam:11000010a01bdb9', 'coke', 0),
(452, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(453, 'steam:11000010a01bdb9', 'golem', 0),
(454, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(455, 'steam:11000010a01bdb9', 'coke', 0),
(456, 'steam:11000010a01bdb9', 'bread', 0),
(457, 'steam:11000010a01bdb9', 'whool', 0),
(458, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(459, 'steam:11000010a01bdb9', 'jager', 0),
(460, 'steam:11000010a01bdb9', 'jager', 0),
(461, 'steam:11000010a01bdb9', 'bread', 0),
(462, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(463, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(464, 'steam:11000010a01bdb9', 'bread', 0),
(465, 'steam:11000010a01bdb9', 'coke', 0),
(466, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(467, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(468, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(469, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(470, 'steam:11000010a01bdb9', 'bread', 0),
(471, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(472, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(473, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(474, 'steam:11000010a01bdb9', 'bolchips', 0),
(475, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(476, 'steam:11000010a01bdb9', 'whisky', 0),
(477, 'steam:11000010a01bdb9', 'golem', 0),
(478, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(479, 'steam:11000010a01bdb9', 'golem', 0),
(480, 'steam:11000010a01bdb9', 'bolchips', 0),
(481, 'steam:11000010a01bdb9', 'commercial_license', 0),
(482, 'steam:11000010a01bdb9', 'bolchips', 0),
(483, 'steam:11000010a01bdb9', 'golem', 0),
(484, 'steam:11000010a01bdb9', 'golem', 0),
(485, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(486, 'steam:11000010a01bdb9', 'bolchips', 0),
(487, 'steam:11000010a01bdb9', 'whisky', 0),
(488, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(489, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(490, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(491, 'steam:11000010a01bdb9', 'bolchips', 0),
(492, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(493, 'steam:11000010a01bdb9', 'whisky', 0),
(494, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(495, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(496, 'steam:11000010a01bdb9', 'bolchips', 0),
(497, 'steam:11000010a01bdb9', 'lsd', 0),
(498, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(499, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(500, 'steam:11000010a01bdb9', 'coke', 0),
(501, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(502, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(503, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(504, 'steam:11000010a01bdb9', 'coke', 0),
(505, 'steam:11000010a01bdb9', 'golem', 0),
(506, 'steam:11000010a01bdb9', 'whisky', 0),
(507, 'steam:11000010a01bdb9', 'lsd', 0),
(508, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(509, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(510, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(511, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(512, 'steam:11000010a01bdb9', 'golem', 0),
(513, 'steam:11000010a01bdb9', 'energy', 0),
(514, 'steam:11000010a01bdb9', 'lsd', 0),
(515, 'steam:11000010a01bdb9', 'whisky', 0),
(516, 'steam:11000010a01bdb9', 'commercial_license', 0),
(517, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(518, 'steam:11000010a01bdb9', 'soda', 0),
(519, 'steam:11000010a01bdb9', 'energy', 0),
(520, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(521, 'steam:11000010a01bdb9', 'lsd', 0),
(522, 'steam:11000010a01bdb9', 'lsd', 0),
(523, 'steam:11000010a01bdb9', 'lsd', 0),
(524, 'steam:11000010a01bdb9', 'compansator', 0),
(525, 'steam:11000010a01bdb9', 'bolchips', 0),
(526, 'steam:11000010a01bdb9', 'commercial_license', 0),
(527, 'steam:11000010a01bdb9', 'commercial_license', 0),
(528, 'steam:11000010a01bdb9', 'soda', 0),
(529, 'steam:11000010a01bdb9', 'energy', 0),
(530, 'steam:11000010a01bdb9', 'compansator', 0),
(531, 'steam:11000010a01bdb9', 'whisky', 0),
(532, 'steam:11000010a01bdb9', 'commercial_license', 0),
(533, 'steam:11000010a01bdb9', 'whisky', 0),
(534, 'steam:11000010a01bdb9', 'compansator', 0),
(535, 'steam:11000010a01bdb9', 'compansator', 0),
(536, 'steam:11000010a01bdb9', 'soda', 0),
(537, 'steam:11000010a01bdb9', 'soda', 0),
(538, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(539, 'steam:11000010a01bdb9', 'bolchips', 0),
(540, 'steam:11000010a01bdb9', 'energy', 0),
(541, 'steam:11000010a01bdb9', 'commercial_license', 0),
(542, 'steam:11000010a01bdb9', 'ice', 0),
(543, 'steam:11000010a01bdb9', 'commercial_license', 0),
(544, 'steam:11000010a01bdb9', 'energy', 0),
(545, 'steam:11000010a01bdb9', 'whisky', 0),
(546, 'steam:11000010a01bdb9', 'compansator', 0),
(547, 'steam:11000010a01bdb9', 'lsd', 0),
(548, 'steam:11000010a01bdb9', 'energy', 0),
(549, 'steam:11000010a01bdb9', 'commercial_license', 0),
(550, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(551, 'steam:11000010a01bdb9', 'compansator', 0),
(552, 'steam:11000010a01bdb9', 'lsd', 0),
(553, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(554, 'steam:11000010a01bdb9', 'ice', 0),
(555, 'steam:11000010a01bdb9', 'soda', 0),
(556, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(557, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(558, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(559, 'steam:11000010a01bdb9', 'ice', 0),
(560, 'steam:11000010a01bdb9', 'ice', 0),
(561, 'steam:11000010a01bdb9', 'teqpaf', 0),
(562, 'steam:11000010a01bdb9', 'teqpaf', 0),
(563, 'steam:11000010a01bdb9', 'teqpaf', 0),
(564, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(565, 'steam:11000010a01bdb9', 'saucisson', 0),
(566, 'steam:11000010a01bdb9', 'wood', 0),
(567, 'steam:11000010a01bdb9', 'teqpaf', 0),
(568, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(569, 'steam:11000010a01bdb9', 'soda', 0),
(570, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(571, 'steam:11000010a01bdb9', 'wood', 0),
(572, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(573, 'steam:11000010a01bdb9', 'energy', 0),
(574, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(575, 'steam:11000010a01bdb9', 'wood', 0),
(576, 'steam:11000010a01bdb9', 'ice', 0),
(577, 'steam:11000010a01bdb9', 'ice', 0),
(578, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(579, 'steam:11000010a01bdb9', 'compansator', 0),
(580, 'steam:11000010a01bdb9', 'wood', 0),
(581, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(582, 'steam:11000010a01bdb9', 'energy', 0),
(583, 'steam:11000010a01bdb9', 'teqpaf', 0),
(584, 'steam:11000010a01bdb9', 'teqpaf', 0),
(585, 'steam:11000010a01bdb9', 'compansator', 0),
(586, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(587, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(588, 'steam:11000010a01bdb9', 'ice', 0),
(589, 'steam:11000010a01bdb9', 'soda', 0),
(590, 'steam:11000010a01bdb9', 'soda', 0),
(591, 'steam:11000010a01bdb9', 'ice', 0),
(592, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(593, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(594, 'steam:11000010a01bdb9', 'wood', 0),
(595, 'steam:11000010a01bdb9', 'saucisson', 0),
(596, 'steam:11000010a01bdb9', 'teqpaf', 0),
(597, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(598, 'steam:11000010a01bdb9', 'saucisson', 0),
(599, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(600, 'steam:11000010a01bdb9', 'vegetables', 0),
(601, 'steam:11000010a01bdb9', 'wood', 0),
(602, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(603, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(604, 'steam:11000010a01bdb9', 'wood', 0),
(605, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(606, 'steam:11000010a01bdb9', 'saucisson', 0),
(607, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(608, 'steam:11000010a01bdb9', 'pilot_license', 0),
(609, 'steam:11000010a01bdb9', 'meth', 0),
(610, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(611, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(612, 'steam:11000010a01bdb9', 'vegetables', 0),
(613, 'steam:11000010a01bdb9', 'vegetables', 0),
(614, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(615, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(616, 'steam:11000010a01bdb9', 'saucisson', 0),
(617, 'steam:11000010a01bdb9', 'meth', 0),
(618, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(619, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(620, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(621, 'steam:11000010a01bdb9', 'vegetables', 0),
(622, 'steam:11000010a01bdb9', 'teqpaf', 0),
(623, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(624, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(625, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(626, 'steam:11000010a01bdb9', 'saucisson', 0),
(627, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(628, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(629, 'steam:11000010a01bdb9', 'wood', 0),
(630, 'steam:11000010a01bdb9', 'saucisson', 0),
(631, 'steam:11000010a01bdb9', 'saucisson', 0),
(632, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(633, 'steam:11000010a01bdb9', 'pilot_license', 0),
(634, 'steam:11000010a01bdb9', 'marrage_license', 0),
(635, 'steam:11000010a01bdb9', 'marrage_license', 0),
(636, 'steam:11000010a01bdb9', 'gazbottle', 0),
(637, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(638, 'steam:11000010a01bdb9', 'meth', 0),
(639, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(640, 'steam:11000010a01bdb9', 'hollow', 0),
(641, 'steam:11000010a01bdb9', 'pilot_license', 0),
(642, 'steam:11000010a01bdb9', 'gazbottle', 0),
(643, 'steam:11000010a01bdb9', 'meth', 0),
(644, 'steam:11000010a01bdb9', 'hollow', 0),
(645, 'steam:11000010a01bdb9', 'vegetables', 0),
(646, 'steam:11000010a01bdb9', 'marrage_license', 0),
(647, 'steam:11000010a01bdb9', 'gazbottle', 0),
(648, 'steam:11000010a01bdb9', 'vegetables', 0),
(649, 'steam:11000010a01bdb9', 'pilot_license', 0),
(650, 'steam:11000010a01bdb9', 'meth', 0),
(651, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(652, 'steam:11000010a01bdb9', 'diving_license', 0),
(653, 'steam:11000010a01bdb9', 'hollow', 0),
(654, 'steam:11000010a01bdb9', 'vegetables', 0),
(655, 'steam:11000010a01bdb9', 'vegetables', 0),
(656, 'steam:11000010a01bdb9', 'pilot_license', 0),
(657, 'steam:11000010a01bdb9', 'meth', 0),
(658, 'steam:11000010a01bdb9', 'meth', 0),
(659, 'steam:11000010a01bdb9', 'marrage_license', 0),
(660, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(661, 'steam:11000010a01bdb9', 'hollow', 0),
(662, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(663, 'steam:11000010a01bdb9', 'marrage_license', 0),
(664, 'steam:11000010a01bdb9', 'pilot_license', 0),
(665, 'steam:11000010a01bdb9', 'gazbottle', 0),
(666, 'steam:11000010a01bdb9', 'pilot_license', 0),
(667, 'steam:11000010a01bdb9', 'hollow', 0),
(668, 'steam:11000010a01bdb9', 'pilot_license', 0),
(669, 'steam:11000010a01bdb9', 'meth', 0),
(670, 'steam:11000010a01bdb9', 'hollow', 0),
(671, 'steam:11000010a01bdb9', 'marrage_license', 0),
(672, 'steam:11000010a01bdb9', 'diving_license', 0),
(673, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(674, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(675, 'steam:11000010a01bdb9', 'diving_license', 0),
(676, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(677, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(678, 'steam:11000010a01bdb9', 'gazbottle', 0),
(679, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(680, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(681, 'steam:11000010a01bdb9', 'scope', 0),
(682, 'steam:11000010a01bdb9', 'gazbottle', 0),
(683, 'steam:11000010a01bdb9', 'marrage_license', 0),
(684, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(685, 'steam:11000010a01bdb9', 'fishing_license', 0),
(686, 'steam:11000010a01bdb9', 'scope', 0),
(687, 'steam:11000010a01bdb9', 'gazbottle', 0),
(688, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(689, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(690, 'steam:11000010a01bdb9', 'diving_license', 0),
(691, 'steam:11000010a01bdb9', 'fishing_license', 0),
(692, 'steam:11000010a01bdb9', 'scope', 0),
(693, 'steam:11000010a01bdb9', 'hollow', 0),
(694, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(695, 'steam:11000010a01bdb9', 'fishing_license', 0),
(696, 'steam:11000010a01bdb9', 'diving_license', 0),
(697, 'steam:11000010a01bdb9', 'fishing_license', 0),
(698, 'steam:11000010a01bdb9', 'diving_license', 0),
(699, 'steam:11000010a01bdb9', 'scope', 0),
(700, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(701, 'steam:11000010a01bdb9', 'gazbottle', 0),
(702, 'steam:11000010a01bdb9', 'scope', 0),
(703, 'steam:11000010a01bdb9', 'diving_license', 0),
(704, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(705, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(706, 'steam:11000010a01bdb9', 'scope', 0),
(707, 'steam:11000010a01bdb9', 'hollow', 0),
(708, 'steam:11000010a01bdb9', 'marrage_license', 0),
(709, 'steam:11000010a01bdb9', 'diving_license', 0),
(710, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(711, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(712, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(713, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(714, 'steam:11000010a01bdb9', 'fixkit', 0),
(715, 'steam:11000010a01bdb9', 'washed_stone', 0),
(716, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(717, 'steam:11000010a01bdb9', 'petrol', 0),
(718, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(719, 'steam:11000010a01bdb9', 'taxi_license', 0),
(720, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(721, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(722, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(723, 'steam:11000010a01bdb9', 'fishing_license', 0),
(724, 'steam:11000010a01bdb9', 'fishing_license', 0),
(725, 'steam:11000010a01bdb9', 'fishing_license', 0),
(726, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(727, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(728, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(729, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(730, 'steam:11000010a01bdb9', 'fixkit', 0),
(731, 'steam:11000010a01bdb9', 'fishing_license', 0),
(732, 'steam:11000010a01bdb9', 'taxi_license', 0),
(733, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(734, 'steam:11000010a01bdb9', 'scope', 0),
(735, 'steam:11000010a01bdb9', 'taxi_license', 0),
(736, 'steam:11000010a01bdb9', 'taxi_license', 0),
(737, 'steam:11000010a01bdb9', 'barrel', 0),
(738, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(739, 'steam:11000010a01bdb9', 'barrel', 0),
(740, 'steam:11000010a01bdb9', 'washed_stone', 0),
(741, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(742, 'steam:11000010a01bdb9', 'washed_stone', 0),
(743, 'steam:11000010a01bdb9', 'fixkit', 0),
(744, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(745, 'steam:11000010a01bdb9', 'taxi_license', 0),
(746, 'steam:11000010a01bdb9', 'scope', 0),
(747, 'steam:11000010a01bdb9', 'washed_stone', 0),
(748, 'steam:11000010a01bdb9', 'washed_stone', 0),
(749, 'steam:11000010a01bdb9', 'fixkit', 0),
(750, 'steam:11000010a01bdb9', 'washed_stone', 0),
(751, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(752, 'steam:11000010a01bdb9', 'taxi_license', 0),
(753, 'steam:11000010a01bdb9', 'petrol', 0),
(754, 'steam:11000010a01bdb9', 'limonade', 0),
(755, 'steam:11000010a01bdb9', 'metreshooter', 0),
(756, 'steam:11000010a01bdb9', 'petrol', 0),
(757, 'steam:11000010a01bdb9', 'barrel', 0),
(758, 'steam:11000010a01bdb9', 'petrol', 0),
(759, 'steam:11000010a01bdb9', 'metreshooter', 0),
(760, 'steam:11000010a01bdb9', 'taxi_license', 0),
(761, 'steam:11000010a01bdb9', 'barrel', 0),
(762, 'steam:11000010a01bdb9', 'chocolate', 0),
(763, 'steam:11000010a01bdb9', 'fixkit', 0),
(764, 'steam:11000010a01bdb9', 'barrel', 0),
(765, 'steam:11000010a01bdb9', 'metreshooter', 0),
(766, 'steam:11000010a01bdb9', 'fixkit', 0),
(767, 'steam:11000010a01bdb9', 'petrol', 0),
(768, 'steam:11000010a01bdb9', 'limonade', 0),
(769, 'steam:11000010a01bdb9', 'washed_stone', 0),
(770, 'steam:11000010a01bdb9', 'fixkit', 0),
(771, 'steam:11000010a01bdb9', 'gold', 0),
(772, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(773, 'steam:11000010a01bdb9', 'washed_stone', 0),
(774, 'steam:11000010a01bdb9', 'gold', 0),
(775, 'steam:11000010a01bdb9', 'limonade', 0),
(776, 'steam:11000010a01bdb9', 'petrol', 0),
(777, 'steam:11000010a01bdb9', 'metreshooter', 0),
(778, 'steam:11000010a01bdb9', 'gold', 0),
(779, 'steam:11000010a01bdb9', 'metreshooter', 0),
(780, 'steam:11000010a01bdb9', 'barrel', 0),
(781, 'steam:11000010a01bdb9', 'barrel', 0),
(782, 'steam:11000010a01bdb9', 'chocolate', 0),
(783, 'steam:11000010a01bdb9', 'gold', 0),
(784, 'steam:11000010a01bdb9', 'limonade', 0),
(785, 'steam:11000010a01bdb9', 'limonade', 0),
(786, 'steam:11000010a01bdb9', 'limonade', 0),
(787, 'steam:11000010a01bdb9', 'metreshooter', 0),
(788, 'steam:11000010a01bdb9', 'barrel', 0),
(789, 'steam:11000010a01bdb9', 'taxi_license', 0),
(790, 'steam:11000010a01bdb9', 'petrol', 0),
(791, 'steam:11000010a01bdb9', 'fixkit', 0),
(792, 'steam:11000010a01bdb9', 'petrol', 0),
(793, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(794, 'steam:11000010a01bdb9', 'blowpipe', 0),
(795, 'steam:11000010a01bdb9', 'drivers_license', 0),
(796, 'steam:11000010a01bdb9', 'stone', 0),
(797, 'steam:11000010a01bdb9', 'opium', 0),
(798, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(799, 'steam:11000010a01bdb9', 'copper', 0),
(800, 'steam:11000010a01bdb9', 'chocolate', 0),
(801, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(802, 'steam:11000010a01bdb9', 'limonade', 0),
(803, 'steam:11000010a01bdb9', 'blowpipe', 0),
(804, 'steam:11000010a01bdb9', 'drivers_license', 0),
(805, 'steam:11000010a01bdb9', 'chocolate', 0),
(806, 'steam:11000010a01bdb9', 'blowpipe', 0),
(807, 'steam:11000010a01bdb9', 'gold', 0),
(808, 'steam:11000010a01bdb9', 'drivers_license', 0),
(809, 'steam:11000010a01bdb9', 'gold', 0),
(810, 'steam:11000010a01bdb9', 'stone', 0),
(811, 'steam:11000010a01bdb9', 'chocolate', 0),
(812, 'steam:11000010a01bdb9', 'blowpipe', 0),
(813, 'steam:11000010a01bdb9', 'metreshooter', 0),
(814, 'steam:11000010a01bdb9', 'limonade', 0),
(815, 'steam:11000010a01bdb9', 'blowpipe', 0),
(816, 'steam:11000010a01bdb9', 'drivers_license', 0),
(817, 'steam:11000010a01bdb9', 'metreshooter', 0),
(818, 'steam:11000010a01bdb9', 'chocolate', 0),
(819, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(820, 'steam:11000010a01bdb9', 'opium', 0),
(821, 'steam:11000010a01bdb9', 'stone', 0),
(822, 'steam:11000010a01bdb9', 'stone', 0),
(823, 'steam:11000010a01bdb9', 'gold', 0),
(824, 'steam:11000010a01bdb9', 'gold', 0),
(825, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(826, 'steam:11000010a01bdb9', 'stone', 0),
(827, 'steam:11000010a01bdb9', 'chocolate', 0),
(828, 'steam:11000010a01bdb9', 'chocolate', 0),
(829, 'steam:11000010a01bdb9', 'stone', 0),
(830, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(831, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(832, 'steam:11000010a01bdb9', 'blowpipe', 0),
(833, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(834, 'steam:11000010a01bdb9', 'copper', 0),
(835, 'steam:11000010a01bdb9', 'armor', 0),
(836, 'steam:11000010a01bdb9', 'copper', 0),
(837, 'steam:11000010a01bdb9', 'opium', 0),
(838, 'steam:11000010a01bdb9', 'grip', 0),
(839, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(840, 'steam:11000010a01bdb9', 'copper', 0),
(841, 'steam:11000010a01bdb9', 'armor', 0),
(842, 'steam:11000010a01bdb9', 'copper', 0),
(843, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(844, 'steam:11000010a01bdb9', 'opium', 0),
(845, 'steam:11000010a01bdb9', 'grip', 0),
(846, 'steam:11000010a01bdb9', 'grip', 0),
(847, 'steam:11000010a01bdb9', 'copper', 0),
(848, 'steam:11000010a01bdb9', 'stone', 0),
(849, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(850, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(851, 'steam:11000010a01bdb9', 'bandage', 0),
(852, 'steam:11000010a01bdb9', 'grip', 0),
(853, 'steam:11000010a01bdb9', 'blowpipe', 0),
(854, 'steam:11000010a01bdb9', 'copper', 0),
(855, 'steam:11000010a01bdb9', 'opium', 0),
(856, 'steam:11000010a01bdb9', 'grip', 0),
(857, 'steam:11000010a01bdb9', 'stone', 0),
(858, 'steam:11000010a01bdb9', 'opium', 0),
(859, 'steam:11000010a01bdb9', 'copper', 0),
(860, 'steam:11000010a01bdb9', 'drivers_license', 0),
(861, 'steam:11000010a01bdb9', 'drivers_license', 0),
(862, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(863, 'steam:11000010a01bdb9', 'drivers_license', 0),
(864, 'steam:11000010a01bdb9', 'rhum', 0),
(865, 'steam:11000010a01bdb9', 'rhum', 0),
(866, 'steam:11000010a01bdb9', 'drivers_license', 0),
(867, 'steam:11000010a01bdb9', 'armor', 0),
(868, 'steam:11000010a01bdb9', 'opium', 0),
(869, 'steam:11000010a01bdb9', 'armor', 0),
(870, 'steam:11000010a01bdb9', 'grip', 0),
(871, 'steam:11000010a01bdb9', 'blowpipe', 0),
(872, 'steam:11000010a01bdb9', 'opium', 0),
(873, 'steam:11000010a01bdb9', 'medikit', 0),
(874, 'steam:11000010a01bdb9', 'bandage', 0),
(875, 'steam:11000010a01bdb9', 'medikit', 0),
(876, 'steam:11000010a01bdb9', 'nitro', 0),
(877, 'steam:11000010a01bdb9', 'armor', 0),
(878, 'steam:11000010a01bdb9', 'bandage', 0),
(879, 'steam:11000010a01bdb9', 'rhum', 0),
(880, 'steam:11000010a01bdb9', 'grip', 0),
(881, 'steam:11000010a01bdb9', 'hunting_license', 0),
(882, 'steam:11000010a01bdb9', 'bandage', 0),
(883, 'steam:11000010a01bdb9', 'meat', 0),
(884, 'steam:11000010a01bdb9', 'rhum', 0),
(885, 'steam:11000010a01bdb9', 'medikit', 0),
(886, 'steam:11000010a01bdb9', 'rhum', 0),
(887, 'steam:11000010a01bdb9', 'armor', 0),
(888, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(889, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(890, 'steam:11000010a01bdb9', 'armor', 0),
(891, 'steam:11000010a01bdb9', 'meat', 0),
(892, 'steam:11000010a01bdb9', 'rhum', 0),
(893, 'steam:11000010a01bdb9', 'carokit', 0),
(894, 'steam:11000010a01bdb9', 'rhum', 0),
(895, 'steam:11000010a01bdb9', 'grip', 0),
(896, 'steam:11000010a01bdb9', 'nitro', 0),
(897, 'steam:11000010a01bdb9', 'nitro', 0),
(898, 'steam:11000010a01bdb9', 'bandage', 0),
(899, 'steam:11000010a01bdb9', 'armor', 0),
(900, 'steam:11000010a01bdb9', 'nitro', 0),
(901, 'steam:11000010a01bdb9', 'carokit', 0),
(902, 'steam:11000010a01bdb9', 'bandage', 0),
(903, 'steam:11000010a01bdb9', 'fixtool', 0),
(904, 'steam:11000010a01bdb9', 'bandage', 0),
(905, 'steam:11000010a01bdb9', 'meat', 0),
(906, 'steam:11000010a01bdb9', 'rhum', 0),
(907, 'steam:11000010a01bdb9', 'hunting_license', 0),
(908, 'steam:11000010a01bdb9', 'medikit', 0),
(909, 'steam:11000010a01bdb9', 'nitro', 0),
(910, 'steam:11000010a01bdb9', 'medikit', 0),
(911, 'steam:11000010a01bdb9', 'bandage', 0),
(912, 'steam:11000010a01bdb9', 'meat', 0),
(913, 'steam:11000010a01bdb9', 'nitro', 0),
(914, 'steam:11000010a01bdb9', 'medikit', 0),
(915, 'steam:11000010a01bdb9', 'fixtool', 0),
(916, 'steam:11000010a01bdb9', 'meat', 0),
(917, 'steam:11000010a01bdb9', 'carokit', 0),
(918, 'steam:11000010a01bdb9', 'nitro', 0),
(919, 'steam:11000010a01bdb9', 'nitro', 0),
(920, 'steam:11000010a01bdb9', 'carokit', 0),
(921, 'steam:11000010a01bdb9', 'hunting_license', 0),
(922, 'steam:11000010a01bdb9', 'meat', 0),
(923, 'steam:11000010a01bdb9', 'fixtool', 0),
(924, 'steam:11000010a01bdb9', 'black_chip', 0),
(925, 'steam:11000010a01bdb9', 'meat', 0),
(926, 'steam:11000010a01bdb9', 'meat', 0),
(927, 'steam:11000010a01bdb9', 'medikit', 0),
(928, 'steam:11000010a01bdb9', 'black_chip', 0),
(929, 'steam:11000010a01bdb9', 'black_chip', 0),
(930, 'steam:11000010a01bdb9', 'hunting_license', 0),
(931, 'steam:11000010a01bdb9', 'fixtool', 0),
(932, 'steam:11000010a01bdb9', 'carokit', 0),
(933, 'steam:11000010a01bdb9', 'carokit', 0),
(934, 'steam:11000010a01bdb9', 'hunting_license', 0),
(935, 'steam:11000010a01bdb9', 'fixtool', 0),
(936, 'steam:11000010a01bdb9', 'hunting_license', 0),
(937, 'steam:11000010a01bdb9', 'black_chip', 0),
(938, 'steam:11000010a01bdb9', 'fixtool', 0),
(939, 'steam:11000010a01bdb9', 'hunting_license', 0),
(940, 'steam:11000010a01bdb9', 'hunting_license', 0),
(941, 'steam:11000010a01bdb9', 'carokit', 0),
(942, 'steam:11000010a01bdb9', 'carokit', 0),
(943, 'steam:11000010a01bdb9', 'black_chip', 0),
(944, 'steam:11000010a01bdb9', 'medikit', 0),
(945, 'steam:11000010a01bdb9', 'black_chip', 0),
(946, 'steam:11000010a01bdb9', 'black_chip', 0),
(947, 'steam:11000010a01bdb9', 'black_chip', 0),
(948, 'steam:11000010a01bdb9', 'fixtool', 0),
(949, 'steam:11000010a01bdb9', 'fixtool', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Adder', 'adder', 900000, 'super'),
('Akuma', 'AKUMA', 7500, 'motorcycles'),
('Alpha', 'alpha', 60000, 'sports'),
('Ardent', 'ardent', 1150000, 'sportsclassics'),
('Asea', 'asea', 5500, 'sedans'),
('Autarch', 'autarch', 1955000, 'super'),
('Avarus', 'avarus', 18000, 'motorcycles'),
('Bagger', 'bagger', 13500, 'motorcycles'),
('Baller', 'baller2', 40000, 'suvs'),
('Baller Sport', 'baller3', 60000, 'suvs'),
('Banshee', 'banshee', 70000, 'sports'),
('Banshee 900R', 'banshee2', 255000, 'super'),
('Bati 801', 'bati', 12000, 'motorcycles'),
('Bati 801RR', 'bati2', 19000, 'motorcycles'),
('Bestia GTS', 'bestiagts', 55000, 'sports'),
('BF400', 'bf400', 6500, 'motorcycles'),
('Bf Injection', 'bfinjection', 16000, 'offroad'),
('Bifta', 'bifta', 12000, 'offroad'),
('Bison', 'bison', 45000, 'vans'),
('Blade', 'blade', 15000, 'muscle'),
('Blazer', 'blazer', 6500, 'offroad'),
('Blazer Sport', 'blazer4', 8500, 'offroad'),
('blazer5', 'blazer5', 1755600, 'offroad'),
('Blista', 'blista', 8000, 'compacts'),
('BMX (velo)', 'bmx', 160, 'motorcycles'),
('Bobcat XL', 'bobcatxl', 32000, 'vans'),
('Brawler', 'brawler', 45000, 'offroad'),
('Brioso R/A', 'brioso', 18000, 'compacts'),
('Btype', 'btype', 62000, 'sportsclassics'),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
('Buccaneer', 'buccaneer', 18000, 'muscle'),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
('Buffalo', 'buffalo', 12000, 'sports'),
('Buffalo S', 'buffalo2', 20000, 'sports'),
('Bullet', 'bullet', 90000, 'super'),
('Burrito', 'burrito3', 19000, 'vans'),
('Camper', 'camper', 42000, 'vans'),
('Carbonizzare', 'carbonizzare', 75000, 'sports'),
('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
('Casco', 'casco', 30000, 'sportsclassics'),
('Cavalcade', 'cavalcade2', 55000, 'suvs'),
('Cheetah', 'cheetah', 375000, 'super'),
('Chimera', 'chimera', 38000, 'motorcycles'),
('Chino', 'chino', 15000, 'muscle'),
('Chino Luxe', 'chino2', 19000, 'muscle'),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
('Comet', 'comet2', 65000, 'sports'),
('Comet 5', 'comet5', 1145000, 'sports'),
('Contender', 'contender', 70000, 'suvs'),
('Coquette', 'coquette', 65000, 'sports'),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
('Cyclone', 'cyclone', 1890000, 'super'),
('Daemon', 'daemon', 11500, 'motorcycles'),
('Daemon High', 'daemon2', 13500, 'motorcycles'),
('Defiler', 'defiler', 9800, 'motorcycles'),
('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
('Dominator', 'dominator', 35000, 'muscle'),
('Double T', 'double', 28000, 'motorcycles'),
('Dubsta', 'dubsta', 45000, 'suvs'),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
('Dukes', 'dukes', 28000, 'muscle'),
('Dune Buggy', 'dune', 8000, 'offroad'),
('Elegy', 'elegy2', 38500, 'sports'),
('Emperor', 'emperor', 8500, 'sedans'),
('Enduro', 'enduro', 5500, 'motorcycles'),
('Entity XF', 'entityxf', 425000, 'super'),
('Esskey', 'esskey', 4200, 'motorcycles'),
('Exemplar', 'exemplar', 32000, 'coupes'),
('F620', 'f620', 40000, 'coupes'),
('Faction', 'faction', 20000, 'muscle'),
('Faction Rider', 'faction2', 30000, 'muscle'),
('Faction XL', 'faction3', 40000, 'muscle'),
('Faggio', 'faggio', 1900, 'motorcycles'),
('Vespa', 'faggio2', 2800, 'motorcycles'),
('Felon', 'felon', 42000, 'coupes'),
('Felon GT', 'felon2', 55000, 'coupes'),
('Feltzer', 'feltzer2', 55000, 'sports'),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
('Fixter (velo)', 'fixter', 225, 'motorcycles'),
('FMJ', 'fmj', 185000, 'super'),
('Fhantom', 'fq2', 17000, 'suvs'),
('Fugitive', 'fugitive', 12000, 'sedans'),
('Furore GT', 'furoregt', 45000, 'sports'),
('Fusilade', 'fusilade', 40000, 'sports'),
('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
('Gauntlet', 'gauntlet', 30000, 'muscle'),
('Gang Burrito', 'gburrito', 45000, 'vans'),
('Burrito', 'gburrito2', 29000, 'vans'),
('Glendale', 'glendale', 6500, 'sedans'),
('Grabger', 'granger', 50000, 'suvs'),
('Gresley', 'gresley', 47500, 'suvs'),
('GT 500', 'gt500', 785000, 'sportsclassics'),
('Guardian', 'guardian', 45000, 'offroad'),
('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
('Hermes', 'hermes', 535000, 'muscle'),
('Hexer', 'hexer', 12000, 'motorcycles'),
('Hotknife', 'hotknife', 125000, 'muscle'),
('Huntley S', 'huntley', 40000, 'suvs'),
('Hustler', 'hustler', 625000, 'muscle'),
('Infernus', 'infernus', 180000, 'super'),
('Innovation', 'innovation', 23500, 'motorcycles'),
('Intruder', 'intruder', 7500, 'sedans'),
('Issi', 'issi2', 10000, 'compacts'),
('Jackal', 'jackal', 38000, 'coupes'),
('Jester', 'jester', 65000, 'sports'),
('Jester(Racecar)', 'jester2', 135000, 'sports'),
('Journey', 'journey', 6500, 'vans'),
('Kamacho', 'kamacho', 345000, 'offroad'),
('Khamelion', 'khamelion', 38000, 'sports'),
('Kuruma', 'kuruma', 30000, 'sports'),
('Landstalker', 'landstalker', 35000, 'suvs'),
('RE-7B', 'le7b', 325000, 'super'),
('Lynx', 'lynx', 40000, 'sports'),
('Mamba', 'mamba', 70000, 'sports'),
('Manana', 'manana', 12800, 'sportsclassics'),
('Manchez', 'manchez', 5300, 'motorcycles'),
('Massacro', 'massacro', 65000, 'sports'),
('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
('Mesa', 'mesa', 16000, 'suvs'),
('Mesa Trail', 'mesa3', 40000, 'suvs'),
('Minivan', 'minivan', 13000, 'vans'),
('Monroe', 'monroe', 55000, 'sportsclassics'),
('The Liberator', 'monster', 210000, 'offroad'),
('Moonbeam', 'moonbeam', 18000, 'vans'),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
('Nemesis', 'nemesis', 5800, 'motorcycles'),
('Neon', 'neon', 1500000, 'sports'),
('Nightblade', 'nightblade', 35000, 'motorcycles'),
('Nightshade', 'nightshade', 65000, 'muscle'),
('9F', 'ninef', 65000, 'sports'),
('9F Cabrio', 'ninef2', 80000, 'sports'),
('Omnis', 'omnis', 35000, 'sports'),
('Oppressor', 'oppressor', 3524500, 'super'),
('Oracle XS', 'oracle2', 35000, 'coupes'),
('Osiris', 'osiris', 160000, 'super'),
('Panto', 'panto', 10000, 'compacts'),
('Paradise', 'paradise', 19000, 'vans'),
('Pariah', 'pariah', 1420000, 'sports'),
('Patriot', 'patriot', 55000, 'suvs'),
('PCJ-600', 'pcj', 6200, 'motorcycles'),
('Penumbra', 'penumbra', 28000, 'sports'),
('Pfister', 'pfister811', 85000, 'super'),
('Phoenix', 'phoenix', 12500, 'muscle'),
('Picador', 'picador', 18000, 'muscle'),
('Pigalle', 'pigalle', 20000, 'sportsclassics'),
('06 Tahoe', 'police8', 0, 'emergency'),
('Prairie', 'prairie', 12000, 'compacts'),
('Premier', 'premier', 8000, 'sedans'),
('Primo Custom', 'primo2', 14000, 'sedans'),
('X80 Proto', 'prototipo', 2500000, 'super'),
('Radius', 'radi', 29000, 'suvs'),
('raiden', 'raiden', 1375000, 'sports'),
('Rapid GT', 'rapidgt', 35000, 'sports'),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
('Reaper', 'reaper', 150000, 'super'),
('Rebel', 'rebel2', 35000, 'offroad'),
('Regina', 'regina', 5000, 'sedans'),
('Retinue', 'retinue', 615000, 'sportsclassics'),
('Revolter', 'revolter', 1610000, 'sports'),
('riata', 'riata', 380000, 'offroad'),
('Rocoto', 'rocoto', 45000, 'suvs'),
('Ruffian', 'ruffian', 6800, 'motorcycles'),
('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
('Rumpo', 'rumpo', 15000, 'vans'),
('Rumpo Trail', 'rumpo3', 19500, 'vans'),
('Sabre Turbo', 'sabregt', 20000, 'muscle'),
('Sabre GT', 'sabregt2', 25000, 'muscle'),
('Sanchez', 'sanchez', 5300, 'motorcycles'),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
('Sanctus', 'sanctus', 25000, 'motorcycles'),
('Sandking', 'sandking', 55000, 'offroad'),
('Savestra', 'savestra', 990000, 'sportsclassics'),
('SC 1', 'sc1', 1603000, 'super'),
('Schafter', 'schafter2', 25000, 'sedans'),
('Schafter V12', 'schafter3', 50000, 'sports'),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
('Seminole', 'seminole', 25000, 'suvs'),
('Sentinel', 'sentinel', 32000, 'coupes'),
('Sentinel XS', 'sentinel2', 40000, 'coupes'),
('Sentinel3', 'sentinel3', 650000, 'sports'),
('Seven 70', 'seven70', 39500, 'sports'),
('ETR1', 'sheava', 220000, 'super'),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
('Slam Van', 'slamvan3', 11500, 'muscle'),
('Sovereign', 'sovereign', 22000, 'motorcycles'),
('Stinger', 'stinger', 80000, 'sportsclassics'),
('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
('Streiter', 'streiter', 500000, 'sports'),
('Stretch', 'stretch', 90000, 'sedans'),
('Stromberg', 'stromberg', 3185350, 'sports'),
('Sultan', 'sultan', 15000, 'sports'),
('Sultan RS', 'sultanrs', 65000, 'super'),
('Super Diamond', 'superd', 130000, 'sedans'),
('Surano', 'surano', 50000, 'sports'),
('Surfer', 'surfer', 12000, 'vans'),
('T20', 't20', 300000, 'super'),
('Tailgater', 'tailgater', 30000, 'sedans'),
('Tampa', 'tampa', 16000, 'muscle'),
('Drift Tampa', 'tampa2', 80000, 'sports'),
('Thrust', 'thrust', 24000, 'motorcycles'),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
('Trophy Truck', 'trophytruck', 60000, 'offroad'),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
('Tropos', 'tropos', 40000, 'sports'),
('Turismo R', 'turismor', 350000, 'super'),
('Tyrus', 'tyrus', 600000, 'super'),
('Vacca', 'vacca', 120000, 'super'),
('Vader', 'vader', 7200, 'motorcycles'),
('Verlierer', 'verlierer2', 70000, 'sports'),
('Vigero', 'vigero', 12500, 'muscle'),
('Virgo', 'virgo', 14000, 'muscle'),
('Viseris', 'viseris', 875000, 'sportsclassics'),
('Visione', 'visione', 2250000, 'super'),
('Voltic', 'voltic', 90000, 'super'),
('Voltic 2', 'voltic2', 3830400, 'super'),
('Voodoo', 'voodoo', 7200, 'muscle'),
('Vortex', 'vortex', 9800, 'motorcycles'),
('Warrener', 'warrener', 4000, 'sedans'),
('Washington', 'washington', 9000, 'sedans'),
('Windsor', 'windsor', 95000, 'coupes'),
('Windsor Drop', 'windsor2', 125000, 'coupes'),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
('XLS', 'xls', 32000, 'suvs'),
('Yosemite', 'yosemite', 485000, 'muscle'),
('Youga', 'youga', 10800, 'vans'),
('Youga Luxuary', 'youga2', 14500, 'vans'),
('Z190', 'z190', 900000, 'sportsclassics'),
('Zentorno', 'zentorno', 1500000, 'super'),
('Zion', 'zion', 36000, 'coupes'),
('Zion Cabrio', 'zion2', 45000, 'coupes'),
('Zombie', 'zombiea', 9500, 'motorcycles'),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
('Z-Type', 'ztype', 220000, 'sportsclassics');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'Police'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 300),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(11, 'GunShop', 'WEAPON_STUNGUN', 50),
(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
(25, 'GunShop', 'WEAPON_GRENADE', 500),
(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
(27, 'GunShop', 'WEAPON_BZGAS', 200),
(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(31, 'GunShop', 'WEAPON_BALL', 50),
(32, 'BlackWeashop', 'WEAPON_BALL', 50),
(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist`
--

CREATE TABLE `whitelist` (
  `identifier` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interiors`
--
ALTER TABLE `interiors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_heli`
--
ALTER TABLE `user_heli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `heli_plate` (`heli_plate`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist`
--
ALTER TABLE `whitelist`
  ADD PRIMARY KEY (`identifier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `interiors`
--
ALTER TABLE `interiors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'key id', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2074;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_heli`
--
ALTER TABLE `user_heli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=950;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Database: `old server`
--
CREATE DATABASE IF NOT EXISTS `old server` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `old server`;

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(10, 'society_police', 'Police', 1),
(11, 'property_black_money', 'Argent Sale Propriété', 0),
(12, 'society_realestateagent', 'Agent immobilier', 1),
(13, 'caution', 'Caution', 0),
(14, 'society_taxi', 'Taxi', 1),
(15, 'society_cardealer', 'Concessionnaire', 1),
(16, 'society_banker', 'Banque', 1),
(17, 'bank_savings', 'Livret Bleu', 0),
(18, 'society_mecano', 'Mécano', 1),
(19, 'society_ambulance', 'Ambulance', 1),
(20, 'society_cardealer', 'Concessionnaire', 1),
(21, 'society_foodtruck', 'Foodtruck', 1),
(22, 'society_fire', 'fire', 1),
(23, 'society_mecano', 'Mécano', 1),
(24, 'society_fire', 'fire', 1),
(25, 'society_unicorn', 'Unicorn', 1),
(26, 'society_airlines', 'Airlines', 1),
(27, 'society_foodtruck', 'Foodtruck', 1),
(28, 'society_avocat', 'Advokat', 1),
(29, 'society_journaliste', 'journaliste', 1),
(30, 'society_karting', 'Karing', 1),
(31, 'society_parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(420, 'society_police', 145, NULL),
(421, 'society_realestateagent', 0, NULL),
(422, 'society_taxi', 0, NULL),
(423, 'society_cardealer', 0, NULL),
(424, 'society_banker', 0, NULL),
(425, 'society_mecano', 0, NULL),
(426, 'society_ambulance', 0, NULL),
(427, 'society_foodtruck', 0, NULL),
(497, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(498, 'caution', 0, 'steam:11000010a01bdb9'),
(499, 'bank_savings', 0, 'steam:11000010a01bdb9'),
(500, 'bank_savings', 0, 'steam:1100001068ef13c'),
(501, 'caution', 0, 'steam:1100001068ef13c'),
(502, 'property_black_money', 0, 'steam:1100001068ef13c'),
(503, 'property_black_money', 0, 'steam:110000132580eb0'),
(504, 'bank_savings', 0, 'steam:110000132580eb0'),
(505, 'caution', 0, 'steam:110000132580eb0'),
(506, 'society_fire', 0, NULL),
(507, 'property_black_money', 0, 'steam:110000112f07694'),
(508, 'caution', 0, 'steam:110000112f07694'),
(509, 'bank_savings', 0, 'steam:110000112f07694'),
(510, 'property_black_money', 0, 'steam:1100001042b46e1'),
(511, 'caution', 0, 'steam:1100001042b46e1'),
(512, 'bank_savings', 0, 'steam:1100001042b46e1'),
(513, 'property_black_money', 0, 'steam:11000011820eeac'),
(514, 'bank_savings', 0, 'steam:11000011820eeac'),
(515, 'caution', 0, 'steam:11000011820eeac'),
(516, 'property_black_money', 0, 'steam:11000011258948d'),
(517, 'caution', 0, 'steam:11000011258948d'),
(518, 'bank_savings', 0, 'steam:11000011258948d'),
(519, 'society_unicorn', 9070, NULL),
(520, 'society_airlines', 0, NULL),
(521, 'society_avocat', 0, NULL),
(522, 'property_black_money', 0, 'steam:110000114590956'),
(523, 'bank_savings', 0, 'steam:110000114590956'),
(524, 'caution', 0, 'steam:110000114590956'),
(525, 'caution', 0, 'steam:110000115c5ddc9'),
(526, 'property_black_money', 0, 'steam:110000115c5ddc9'),
(527, 'bank_savings', 0, 'steam:110000115c5ddc9'),
(528, 'society_journaliste', 100000, NULL),
(529, 'society_karting', 100, NULL),
(530, 'property_black_money', 0, 'steam:110000109660ef9'),
(531, 'caution', 0, 'steam:110000109660ef9'),
(532, 'bank_savings', 0, 'steam:110000109660ef9'),
(533, 'bank_savings', 0, 'steam:11000010c6acdce'),
(534, 'property_black_money', 0, 'steam:11000010c6acdce'),
(535, 'caution', 0, 'steam:11000010c6acdce'),
(536, 'bank_savings', 0, 'steam:110000106a5790d'),
(537, 'caution', 0, 'steam:110000106a5790d'),
(538, 'property_black_money', 0, 'steam:110000106a5790d'),
(539, 'property_black_money', 0, 'steam:11000010b0a1bc3'),
(540, 'bank_savings', 0, 'steam:11000010b0a1bc3'),
(541, 'caution', 0, 'steam:11000010b0a1bc3'),
(542, 'bank_savings', 0, 'steam:11000011b78df95'),
(543, 'caution', 0, 'steam:11000011b78df95'),
(544, 'property_black_money', 0, 'steam:11000011b78df95'),
(545, 'property_black_money', 0, 'steam:110000135c88cca'),
(546, 'caution', 0, 'steam:110000135c88cca'),
(547, 'bank_savings', 0, 'steam:110000135c88cca'),
(548, 'society_parking', 10000, NULL),
(549, 'property_black_money', 0, 'steam:11000010473daf1'),
(550, 'bank_savings', 0, 'steam:11000010473daf1'),
(551, 'caution', 0, 'steam:11000010473daf1'),
(552, 'bank_savings', 0, 'steam:110000103b294e0'),
(553, 'property_black_money', 0, 'steam:110000103b294e0'),
(554, 'caution', 0, 'steam:110000103b294e0'),
(555, 'property_black_money', 0, 'steam:110000104e0ed21'),
(556, 'caution', 0, 'steam:110000104e0ed21'),
(557, 'bank_savings', 0, 'steam:110000104e0ed21'),
(558, 'property_black_money', 0, 'steam:1100001238ac476'),
(559, 'bank_savings', 0, 'steam:1100001238ac476'),
(560, 'caution', 0, 'steam:1100001238ac476'),
(561, 'property_black_money', 0, 'steam:11000011742efb1'),
(562, 'caution', 0, 'steam:11000011742efb1'),
(563, 'bank_savings', 0, 'steam:11000011742efb1'),
(564, 'caution', 0, 'steam:110000112969e8f'),
(565, 'property_black_money', 0, 'steam:110000112969e8f'),
(566, 'bank_savings', 0, 'steam:110000112969e8f'),
(567, 'bank_savings', 0, 'steam:110000134857568'),
(568, 'property_black_money', 0, 'steam:110000134857568'),
(569, 'caution', 0, 'steam:110000134857568'),
(570, 'caution', 0, 'steam:110000131f0d83c'),
(571, 'property_black_money', 0, 'steam:110000131f0d83c'),
(572, 'bank_savings', 0, 'steam:110000131f0d83c'),
(573, 'property_black_money', 0, 'steam:110000110931da8'),
(574, 'bank_savings', 0, 'steam:110000110931da8'),
(575, 'caution', 0, 'steam:110000110931da8'),
(576, 'property_black_money', 0, 'steam:1100001185894b3'),
(577, 'bank_savings', 0, 'steam:1100001185894b3'),
(578, 'caution', 0, 'steam:1100001185894b3'),
(579, 'bank_savings', 0, 'steam:11000011a15fea8'),
(580, 'property_black_money', 0, 'steam:11000011a15fea8'),
(581, 'caution', 0, 'steam:11000011a15fea8'),
(582, 'caution', 0, 'steam:1100001183ded9e'),
(583, 'property_black_money', 0, 'steam:1100001183ded9e'),
(584, 'bank_savings', 0, 'steam:1100001183ded9e'),
(585, 'bank_savings', 0, 'steam:110000115815ad6'),
(586, 'caution', 0, 'steam:110000115815ad6'),
(587, 'property_black_money', 0, 'steam:110000115815ad6');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_admin', 'Admin', 1),
(4, 'property', 'Propriété', 0),
(5, 'society_taxi', 'Taxi', 1),
(6, 'society_cardealer', 'Dealer', 1),
(7, 'society_mecano', 'Mechanic', 1),
(8, 'society_cardealer', 'Concesionnaire', 1),
(9, 'society_fire', 'fire', 1),
(10, 'society_mecano', 'Mécano', 1),
(11, 'society_fire', 'fire', 1),
(12, 'society_unicorn', 'Unicorn', 1),
(13, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(14, 'society_airlines', 'Airlines', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(1, 'steam:110000134857568', 'steam:110000132580eb0', 'society', 'society_police', 'Fine: Shooting a Civilian', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'f',
  `height` varchar(128) NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `fire_rank` int(11) DEFAULT '-1',
  `staff_rank` int(11) DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `fire_rank`, `staff_rank`) VALUES
(4, 'steam:11000010a01bdb9', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coffees`
--

INSERT INTO `coffees` (`id`, `name`, `item`, `price`) VALUES
(1, 'Coffee', 'coffee', 30);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(11, 'property', 'Property', 0),
(12, 'society_fire', 'fire', 1),
(14, 'society_unicorn', 'Unicorn', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(139, 'society_police', NULL, '{}'),
(163, 'property', 'steam:11000010a01bdb9', '{\"dressing\":[]}'),
(164, 'property', 'steam:1100001068ef13c', '{\"dressing\":[{\"label\":\"idk\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":0,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"lipstick_1\":0,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":0,\"tshirt_1\":14,\"beard_4\":0,\"bags_2\":0,\"shoes_1\":3,\"lipstick_4\":0,\"makeup_4\":0,\"mask_1\":0,\"chain_1\":2,\"eyebrows_2\":8,\"eyebrows_4\":0,\"lipstick_2\":0,\"helmet_1\":-1,\"arms\":0,\"eyebrows_1\":4,\"tshirt_2\":0,\"beard_2\":3,\"sex\":1,\"makeup_3\":0,\"beard_3\":11,\"makeup_2\":0,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":8,\"shoes_2\":2,\"decals_2\":0,\"face\":31,\"mask_2\":0,\"hair_color_1\":11,\"torso_1\":34,\"age_2\":0,\"bproof_2\":0,\"pants_1\":0,\"glasses_1\":4,\"hair_1\":19,\"ears_1\":1,\"bproof_1\":0}},{\"label\":\"Cop girl\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":1,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"hair_color_1\":11,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":13,\"lipstick_1\":0,\"beard_2\":3,\"glasses_1\":5,\"shoes_1\":14,\"lipstick_4\":0,\"makeup_3\":6,\"tshirt_1\":152,\"mask_1\":0,\"eyebrows_2\":8,\"eyebrows_4\":0,\"chain_1\":2,\"helmet_1\":-1,\"arms\":14,\"makeup_4\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"sex\":1,\"eyebrows_1\":4,\"beard_3\":11,\"makeup_2\":9,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":0,\"shoes_2\":15,\"bproof_2\":9,\"face\":31,\"mask_2\":0,\"age_2\":0,\"torso_1\":73,\"decals_2\":0,\"beard_4\":0,\"pants_1\":3,\"bags_2\":0,\"hair_1\":17,\"ears_1\":1,\"bproof_1\":27}}]}'),
(165, 'property', 'steam:110000132580eb0', '{\"dressing\":[{\"skin\":{\"makeup_2\":0,\"arms\":0,\"eyebrows_3\":0,\"eyebrows_4\":0,\"torso_2\":0,\"glasses_2\":0,\"face\":0,\"hair_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"lipstick_3\":0,\"sex\":0,\"beard_3\":0,\"glasses_1\":0,\"bags_1\":0,\"ears_2\":0,\"beard_2\":0,\"chain_1\":0,\"skin\":0,\"makeup_4\":0,\"bproof_1\":0,\"lipstick_1\":0,\"pants_2\":7,\"tshirt_1\":0,\"mask_2\":0,\"ears_1\":-1,\"eyebrows_1\":0,\"lipstick_4\":0,\"shoes_2\":0,\"makeup_1\":0,\"shoes_1\":0,\"lipstick_2\":0,\"makeup_3\":0,\"decals_2\":0,\"helmet_2\":0,\"chain_2\":0,\"beard_1\":0,\"mask_1\":0,\"age_2\":0,\"eyebrows_2\":0,\"hair_1\":0,\"hair_color_2\":0,\"bproof_2\":0,\"torso_1\":0,\"decals_1\":0,\"hair_color_1\":0,\"beard_4\":0,\"pants_1\":9,\"tshirt_2\":0,\"age_1\":0},\"label\":\"test\"}]}'),
(166, 'society_fire', NULL, '{}'),
(167, 'property', 'steam:110000112f07694', '{}'),
(168, 'property', 'steam:1100001042b46e1', '{}'),
(169, 'property', 'steam:11000011820eeac', '{}'),
(170, 'property', 'steam:11000011258948d', '{}'),
(171, 'society_unicorn', NULL, '{}'),
(172, 'society_avocat', NULL, '{}'),
(173, 'property', 'steam:110000114590956', '{}'),
(174, 'property', 'steam:110000115c5ddc9', '{}'),
(175, 'society_journaliste', NULL, '{}'),
(176, 'property', 'steam:110000109660ef9', '{}'),
(177, 'property', 'steam:11000010c6acdce', '{}'),
(178, 'property', 'steam:110000106a5790d', '{}'),
(179, 'property', 'steam:11000010b0a1bc3', '{}'),
(180, 'property', 'steam:11000011b78df95', '{}'),
(181, 'property', 'steam:110000135c88cca', '{}'),
(182, 'property', 'steam:11000010473daf1', '{}'),
(183, 'property', 'steam:110000103b294e0', '{}'),
(184, 'property', 'steam:110000104e0ed21', '{}'),
(185, 'property', 'steam:1100001238ac476', '{}'),
(186, 'property', 'steam:11000011742efb1', '{}'),
(187, 'society_ambulance', NULL, '{}'),
(188, 'property', 'steam:110000112969e8f', '{}'),
(189, 'property', 'steam:110000134857568', '{}'),
(190, 'property', 'steam:110000131f0d83c', '{}'),
(191, 'property', 'steam:110000110931da8', '{}'),
(192, 'property', 'steam:1100001185894b3', '{}'),
(193, 'property', 'steam:11000011a15fea8', '{}'),
(194, 'property', 'steam:1100001183ded9e', '{}'),
(195, 'property', 'steam:110000115815ad6', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Excessive Horn Use', 125, 0),
(2, 'Crossing Lines', 150, 0),
(3, 'Cont', 250, 0),
(4, 'Illegal U-Turn', 250, 0),
(5, 'Illegal Offroading', 375, 0),
(6, 'Illegal Safety Distance', 125, 0),
(7, 'Dangerous / forbidden stop', 450, 0),
(8, 'Illegal Parking', 110, 0),
(9, 'Not respecting the priority on the right', 225, 0),
(10, 'Non-compliance with a priority vehicle', 225, 0),
(11, 'Failure to stop', 350, 0),
(12, 'Failure to comply with a red light', 225, 0),
(13, 'Dangerous Driving', 1000, 0),
(14, 'Vehicle out of state', 100, 0),
(15, 'Driving without a license', 1500, 0),
(16, 'Hit and run', 1125, 0),
(17, 'Speeding <5 km / h', 135, 0),
(18, 'Speeding 5-15 kmh', 175, 0),
(19, 'Speeding 15-30 kmh', 375, 0),
(20, 'Speeding > 30 kmh', 1250, 0),
(21, 'Traffic obstruction', 110, 1),
(22, 'Degradation of the public road', 90, 1),
(23, 'Trouble with public order', 90, 1),
(24, 'Obstructing police operation', 225, 1),
(25, 'Insulting civilians', 135, 1),
(26, 'Insulting a police officer', 550, 1),
(27, 'Verbal threat or intimidation towards civil', 750, 1),
(28, 'Verbal threat or intimidation of a police officer', 1000, 1),
(29, 'Illegal protest', 250, 1),
(30, 'Attempted bribery', 1500, 1),
(31, 'Weapon out in town', 375, 2),
(32, 'Flashing a lethal wepon', 300, 2),
(33, 'Have a gun with no permit', 600, 2),
(34, 'Porting illegal wepons', 700, 2),
(35, 'Using a lockpickk', 300, 2),
(36, 'Car Theft', 3500, 2),
(37, 'Sale OF Drugs', 1500, 2),
(38, 'Drug Manifacturing', 1500, 2),
(39, 'Possetion Of Drugs', 650, 2),
(40, 'Civil Despute', 1500, 2),
(41, 'Takeover agent of the state', 2000, 2),
(42, 'Special deflection', 650, 2),
(43, 'Robbing Store', 4500, 2),
(44, 'Attempt to rob', 1500, 2),
(45, 'Shooting a Civilian', 2000, 3),
(46, 'Shooting a State Agent', 7500, 3),
(47, 'Attempt to Kill CIV', 3000, 3),
(48, 'Attempted murder of LEO', 5000, 3),
(49, 'Murder on civilian', 10000, 3),
(50, 'Murder on STate Agent', 30000, 3),
(51, 'Murder involuntarily', 1800, 3),
(52, 'Business scam', 2000, 2),
(53, 'After Market Parts', 1000, 0),
(54, 'DWI', 3500, 0),
(55, 'avoid and allude ', 4500, 3);

-- --------------------------------------------------------

--
-- Table structure for table `interiors`
--

CREATE TABLE `interiors` (
  `id` int(11) NOT NULL COMMENT 'key id',
  `enter` text NOT NULL COMMENT 'enter coords',
  `exit` text NOT NULL COMMENT 'destination coords',
  `iname` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interiors`
--

INSERT INTO `interiors` (`id`, `enter`, `exit`, `iname`) VALUES
(1, '{-1045.888,-2751.017,21.3634,321.7075}', '{-1055.37,-2698.47,13.82,234.62}', 'first int');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'weed', 'Weed', -1, 0, 1),
(2, 'weed_pooch', 'Pochon de weed', -1, 0, 1),
(3, 'coke', 'Coke', -1, 0, 1),
(4, 'coke_pooch', 'Pochon de coke', -1, 0, 1),
(5, 'meth', 'Meth', -1, 0, 1),
(6, 'meth_pooch', 'Pochon de meth', -1, 0, 1),
(7, 'opium', 'Opium', -1, 0, 1),
(8, 'opium_pooch', 'Pochon de opium', -1, 0, 1),
(9, 'alive_chicken', 'Poulet vivant', -1, 0, 1),
(10, 'slaughtered_chicken', 'Poulet abattu', -1, 0, 1),
(11, 'packaged_chicken', 'Poulet en barquette', -1, 0, 1),
(12, 'fish', 'Poisson', -1, 0, 1),
(13, 'stone', 'Pierre', -1, 0, 1),
(14, 'washed_stone', 'Pierre Lavée', -1, 0, 1),
(15, 'copper', 'Cuivre', -1, 0, 1),
(16, 'iron', 'Fer', -1, 0, 1),
(17, 'gold', 'Or', -1, 0, 1),
(18, 'diamond', 'Diamant', -1, 0, 1),
(19, 'wood', 'Bois', -1, 0, 1),
(20, 'cutted_wood', 'Bois coupé', -1, 0, 1),
(21, 'packaged_plank', 'Paquet de planches', -1, 0, 1),
(22, 'petrol', 'Pétrole', -1, 0, 1),
(23, 'petrol_raffin', 'Pétrole Raffiné', -1, 0, 1),
(24, 'essence', 'Essence', -1, 0, 1),
(25, 'whool', 'Laine', -1, 0, 1),
(26, 'fabric', 'Tissu', -1, 0, 1),
(27, 'clothe', 'Vêtement', -1, 0, 1),
(28, 'bread', 'Bread', -1, 0, 1),
(29, 'water', 'Water', -1, 0, 1),
(30, 'gazbottle', 'Gas', -1, 0, 1),
(31, 'fixtool', 'FixTool', -1, 0, 1),
(32, 'carotool', 'CarTool', -1, 0, 1),
(33, 'blowpipe', 'BlowPipe', -1, 0, 1),
(34, 'fixkit', 'FixKit', -1, 0, 1),
(35, 'carokit', 'Kit carosserie', -1, 0, 1),
(52, 'bandage', 'Bandage', 50, 0, 1),
(53, 'medikit', 'Medikit', 50, 0, 1),
(55, 'cola', 'Coke', 20, 0, 1),
(56, 'vegetables', 'Vegetables', 20, 0, 1),
(57, 'meat', 'Meat', 20, 0, 1),
(58, 'tacos', 'Tacos', 20, 0, 1),
(59, 'burger', 'Burger', 20, 0, 1),
(62, 'coffee', 'Cafe', 10, 1, 1),
(63, 'donut', 'Donut', 10, 1, 1),
(74, 'black_chip', 'Puce cryptée', 1, 0, 1),
(75, 'chocolate', 'Chocolate', -1, 0, 1),
(76, 'silent', 'Silenced', -1, 0, 1),
(77, 'flashlight', 'Flashlight', -1, 0, 1),
(78, 'grip', 'Grip', -1, 0, 1),
(79, 'nightvision_scope', 'Night Vision Scope', -1, 0, 1),
(80, 'thermal_scope', 'Thermal Vision Scope', -1, 0, 1),
(81, 'extended_magazine', 'Extended Magazine', -1, 0, 1),
(82, 'very_extended_magazine', 'Very Extended Magazine', -1, 0, 1),
(83, 'scope', 'Scope', -1, 0, 1),
(84, 'advanced_scope', 'Advanced Scope', -1, 0, 1),
(85, 'yusuf', 'Luxury Skin', -1, 0, 1),
(86, 'lowrider', 'Lowrider Skin', -1, 0, 1),
(87, 'incendiary', 'Incendiary Bullets', -1, 0, 1),
(88, 'tracer_clip', 'Trackers Bullets', -1, 0, 1),
(89, 'hollow', 'Hollow Bullets', -1, 0, 1),
(90, 'fmj', 'Perforating Bullets', -1, 0, 1),
(91, 'lazer_scope', 'Lazer Scope', -1, 0, 1),
(92, 'compansator', 'Compensator', -1, 0, 1),
(93, 'barrel', 'Barrel', -1, 0, 1),
(100, 'drivers_license', 'Drivers License', -1, 0, 1),
(101, 'motorcycle_license', 'Motorcycle License', -1, 0, 1),
(102, 'commercial_license', 'Commerical Drivers License', -1, 0, 1),
(103, 'boating_license', 'Boating License', -1, 0, 1),
(104, 'taxi_license', 'Taxi License', -1, 0, 1),
(105, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1),
(106, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1),
(107, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1),
(108, 'hunting_license', 'Hunting License', -1, 0, 1),
(109, 'fishing_license', 'Fishing License', -1, 0, 1),
(110, 'diving_license', 'Diving License', -1, 0, 1),
(111, 'marrage_license', 'Marriage License', -1, 0, 1),
(112, 'pilot_license', 'Pilot License', -1, 0, 1),
(2010, 'armor', 'Bulletproof Vest', -1, 0, 1),
(2017, 'clip', 'Clip', -1, 0, 1),
(2018, 'binoculars', 'Binoculars', 1, 0, 1),
(2019, 'lsd', 'Lsd', -1, 0, 1),
(2020, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2021, 'lsd', 'Lsd', -1, 0, 1),
(2022, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2023, 'jager', 'Jägermeister', 5, 0, 1),
(2024, 'vodka', 'Vodka', 5, 0, 1),
(2025, 'rhum', 'Rhum', 5, 0, 1),
(2026, 'whisky', 'Whisky', 5, 0, 1),
(2027, 'tequila', 'Tequila', 5, 0, 1),
(2028, 'martini', 'Martini blanc', 5, 0, 1),
(2029, 'soda', 'Soda', 5, 0, 1),
(2030, 'jusfruit', 'Jus de fruits', 5, 0, 1),
(2031, 'icetea', 'Ice Tea', 5, 0, 1),
(2032, 'energy', 'Energy Drink', 5, 0, 1),
(2033, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(2034, 'limonade', 'Limonade', 5, 0, 1),
(2035, 'bolcacahuetes', 'Bol de cacahuètes', 5, 0, 1),
(2036, 'bolnoixcajou', 'Bol de noix de cajou', 5, 0, 1),
(2037, 'bolpistache', 'Bol de pistaches', 5, 0, 1),
(2038, 'bolchips', 'Bol de chips', 5, 0, 1),
(2039, 'saucisson', 'Saucisson', 5, 0, 1),
(2040, 'grapperaisin', 'Grappe de raisin', 5, 0, 1),
(2041, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(2042, 'golem', 'Golem', 5, 0, 1),
(2043, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(2044, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(2045, 'vodkafruit', 'Vodka-jus de fruits', 5, 0, 1),
(2046, 'rhumfruit', 'Rhum-jus de fruits', 5, 0, 1),
(2047, 'teqpaf', 'Teq\'paf', 5, 0, 1),
(2048, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(2049, 'mojito', 'Mojito', 5, 0, 1),
(2050, 'ice', 'Glaçon', 5, 0, 1),
(2051, 'mixapero', 'Mix Apéritif', 3, 0, 1),
(2052, 'metreshooter', 'Mètre de shooter', 3, 0, 1),
(2053, 'jagercerbere', 'Jäger Cerbère', 3, 0, 1),
(2054, 'menthe', 'Feuille de menthe', 10, 0, 1),
(2055, 'nitro', 'Nitroso', -1, 0, 1),
(2056, 'fish', 'Fish', -1, 0, 1),
(2057, 'fishingrod', 'Fishing rod', 2, 0, 1),
(2058, 'bait', 'Bait', 20, 0, 1),
(2064, 'coffee', 'Café', -1, 0, 1),
(2069, 'plongee1', 'Plongee courte', -1, 0, 1),
(2070, 'plongee2', 'Plongee longue', -1, 0, 1),
(2073, 'contrat', 'Salvage', 15, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `J_Time` int(10) NOT NULL,
  `J_Cell` varchar(5) COLLATE utf8mb4_bin NOT NULL,
  `Jailer` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `Jailer_ID` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'unemployed', 0),
(5, 'realestateagent', 'Real Estate', 1),
(6, 'slaughterer', 'Butcher', 0),
(7, 'fisherman', 'Fisherman', 0),
(8, 'miner', 'Miner', 0),
(9, 'lumberjack', 'Lumberjack', 0),
(10, 'fuel', 'Refiner', 0),
(11, 'reporter', '9 News', 1),
(12, 'textil', 'Cloth Maker', 0),
(13, 'taxi', 'Taxi/Uber', 0),
(14, 'cardealer', 'Car Dealer', 1),
(15, 'banker', 'Banker', 1),
(20, 'security', 'Commercial City Security', 0),
(21, 'balla', 'balla', 1),
(22, 'family', 'family', 1),
(23, 'LOST MC', 'LOST MC', 1),
(24, 'vagos', 'vagos', 1),
(25, 'bus', 'BlueHound', 0),
(29, 'pizza', 'Pizza Sluts', 0),
(30, 'fire', 'Littleton Fire', 1),
(31, 'mecano', 'Mechanic', 1),
(32, 'brinks', 'Brinks', 0),
(33, 'coastguard', 'coastguard', 1),
(34, 'deliverer', 'Amazon', 0),
(36, 'gopostal', 'CSPS', 0),
(37, 'trucker', 'Trucker', 0),
(38, 'unicorn', 'Unicorn', 0),
(39, 'airlines', 'DIA', 0),
(40, 'banksecurity', 'banksecurity', 0),
(41, 'garbage', 'CC Sanitation', 0),
(42, 'foodtruck', 'Foodtruck', 1),
(43, 'avocat', 'Advokat', 0),
(44, 'police', 'LEO', 1),
(45, 'ambulance', 'AMR', 1),
(46, 'staff', 'Staff', 1),
(47, 'fork', 'Forklift', 1),
(50, 'journaliste', '9News', 0),
(51, 'Salvage', 'Salvage', 0),
(52, 'parking', 'Parking Enforcement', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'rsa', 'RSA', 100, '{}', '{}'),
(16, 'realestateagent', 0, 'location', 'Location', 350, '{}', '{}'),
(17, 'realestateagent', 1, 'vendeur', 'Seller', 550, '{}', '{}'),
(18, 'realestateagent', 2, 'gestion', 'Management', 40, '{}', '{}'),
(19, 'realestateagent', 3, 'boss', 'Boss', 0, '{}', '{}'),
(20, 'lumberjack', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(21, 'fisherman', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(22, 'fuel', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(23, 'reporter', 0, 'employee', 'Employee', 1000, '{}', '{}'),
(24, 'textil', 0, 'interim', 'Employee', 1000, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(25, 'miner', 0, 'interim', 'Employee', 1000, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(26, 'slaughterer', 0, 'interim', 'Employee', 1000, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(27, 'taxi', 0, 'driver', 'Driver', 500, '{}', '{}'),
(32, 'cardealer', 0, 'recruit', 'Recruit', 750, '{}', '{}'),
(33, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
(34, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
(35, 'cardealer', 3, 'boss', 'Boss', 100, '{}', '{}'),
(36, 'banker', 0, 'advisor', 'Adviser', 350, '{}', '{}'),
(37, 'banker', 1, 'banker', 'Banker', 550, '{}', '{}'),
(38, 'banker', 2, 'business_banker', 'Business Banker', 750, '{}', '{}'),
(39, 'banker', 3, 'trader', 'Trader', 800, '{}', '{}'),
(40, 'banker', 4, 'boss', 'Boss', 1000, '{}', '{}'),
(54, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(55, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(63, 'security', 0, 'Basic noob', 'Basic Guard', 200, '{}', '{}'),
(64, 'balla', 0, 'balla', 'balla', 600, '{}', '{}'),
(65, 'family', 0, 'family', 'family', 600, '{}', '{}'),
(66, 'LOST MC', 0, 'LOST MC', 'LOST MC', 600, '{}', '{}'),
(67, 'vagos', 0, 'vagos', 'vagos', 600, '{}', '{}'),
(70, 'bus', 0, 'employee', 'Bus Driver', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(74, 'pizza', 0, 'employee', 'Pizza Bitch', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,jobs\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(78, 'fire', 3, 'boss', 'Commandant', 80, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":41,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":8,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":21,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"hair_color_1\":10,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"shoes\":24,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(79, 'mecano', 0, 'recruit', 'Recruit', 12, '{}', '{}'),
(80, 'mecano', 1, 'novice', 'Novice', 24, '{}', '{}'),
(81, 'mecano', 2, 'experimente', 'Experimente', 36, '{}', '{}'),
(82, 'mecano', 3, 'chief', 'Chef d\'équipe', 48, '{}', '{}'),
(83, 'mecano', 4, 'boss', 'Patron', 0, '{}', '{}'),
(84, 'brinks', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(85, 'coastguard', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(86, 'deliverer', 0, 'employee', 'Employee', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(91, 'gopostal', 0, 'employee', 'Sedex', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(92, 'trucker', 0, 'employee', 'Employé', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(93, 'unicorn', 0, 'barman', 'Barman', 300, '{}', '{}'),
(94, 'unicorn', 1, 'dancer', 'Danseur', 300, '{}', '{}'),
(95, 'unicorn', 2, 'viceboss', 'Co-gérant', 500, '{}', '{}'),
(96, 'unicorn', 3, 'boss', 'Gérant', 600, '{}', '{}'),
(97, 'airlines', 0, 'recrue', 'Recrue', 30, '{}', '{}'),
(98, 'airlines', 1, 'chauffeur', 'Chauffeur', 40, '{}', '{}'),
(99, 'airlines', 2, 'pilote', 'Pilote', 50, '{}', '{}'),
(100, 'airlines', 3, 'gerant', 'Gerant', 60, '{}', '{}'),
(101, 'airlines', 4, 'boss', 'Patron', 0, '{}', '{}'),
(102, 'banksecurity', 0, 'employee', 'banksecurity', 200, '{\"tshirt_1\":60,\"torso_1\":130,\"arms\":31,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}', '{\"tshirt_1\":60,\"torso_1\":0,\"arms\":68,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}'),
(103, 'garbage', 0, 'employee', 'Employee', 750, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(104, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(105, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(106, 'avocat', 0, 'boss', 'Patron', 500, '', ''),
(107, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(108, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(109, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(110, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(111, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(112, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(113, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(114, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(115, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(116, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(117, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(118, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(119, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(120, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(121, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(122, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(123, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(124, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(125, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(126, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(127, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(128, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(129, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(130, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(131, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(132, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(133, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(134, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(135, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(136, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(137, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(138, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(139, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(140, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(141, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(142, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(143, 'police', 36, 'boss', 'Deputy Commissioner', 2000, '{}', '{}'),
(144, 'police', 37, 'boss', 'Commissioner', 2000, '{}', '{}'),
(145, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(146, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(147, 'fire', 0, 'cadet', 'Cadet', 100, '{\"tshirt_1\":57,\"torso_1\":55,\"arms\":0,\"pants_1\":35,\"glasses\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":5,\"face\":19,\"glasses_2\":1,\"torso_2\":0,\"shoes\":24,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":8}', '{\"tshirt_1\":34,\"torso_1\":48,\"shoes\":24,\"pants_1\":34,\"torso_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"glasses\":0,\"helmet_2\":0,\"hair_2\":3,\"face\":21,\"decals_1\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"arms\":14,\"hair_color_1\":10,\"tshirt_2\":0,\"helmet_1\":57}'),
(148, 'fire', 1, 'probationary', 'Probationary', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":0,\"decals_1\":8,\"torso_2\":0,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"hair_color_1\":5,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":14,\"pants_1\":34,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"shoes\":24,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"face\":21,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"hair_color_1\":10,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(159, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(160, 'fire', 3, 'firefighter', 'Firefighter', 200, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(161, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(162, 'fire', 5, 'captain', 'Captain', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(163, 'fire', 6, 'sharkone', 'Shark One', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(164, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(165, 'fire', 8, 'fto', 'FTO', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(166, 'fire', 9, 'hr', 'HR', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(167, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(168, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(169, 'fire', 12, 'boss', 'Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(170, 'ambulance', 0, 'cadet', 'Cadet', 100, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(171, 'ambulance', 1, 'probationary', 'Probationary', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(172, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(173, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(174, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(175, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(176, 'ambulance', 6, 'medone', 'Med One', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(178, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(179, 'ambulance', 8, 'fto', 'FTO', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(181, 'ambulance', 10, 'medicalchief', 'Medical Chief', 400, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(182, 'ambulance', 11, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(183, 'ambulance', 12, 'boss', 'Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(184, 'ambulance', 13, 'boss', 'Medical Examiner', 0, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(185, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(186, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(187, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(188, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(189, 'fork', 0, 'employee', 'Operator', 20, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(192, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(193, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(194, 'journaliste', 2, 'investigator', 'Enquêteur', 400, '{}', '{}'),
(195, 'journaliste', 3, 'boss', 'Rédac\' chef', 450, '{}', '{}'),
(196, 'Salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(197, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(198, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(199, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(200, 'ambulance', 9, 'phrn', 'PreHospital Registered Nurse', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Weapon license'),
(6, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('Char1:11000010a01bdb9', 'FMJ 423', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"FMJ 423\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":4.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'GSV 723', '{\"modArchCover\":-1,\"modLivery\":0,\"model\":353883353,\"modRearBumper\":-1,\"windowTint\":-1,\"dirtLevel\":1.0,\"modTransmission\":-1,\"color2\":0,\"modSpeakers\":-1,\"pearlescentColor\":112,\"health\":1000,\"modFrontWheels\":-1,\"modFender\":-1,\"modAirFilter\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modAerials\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"modExhaust\":-1,\"modSideSkirt\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"plate\":\"GSV 723\",\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modBrakes\":-1,\"modGrille\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"modHorns\":-1,\"modSeats\":-1,\"modDoorSpeaker\":-1,\"extras\":[],\"modEngineBlock\":-1,\"modAPlate\":-1,\"modXenon\":false,\"color1\":134,\"modPlateHolder\":-1,\"modOrnaments\":-1,\"modTrimA\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"modDial\":-1,\"modFrame\":-1,\"wheels\":0,\"modSmokeEnabled\":false,\"modVanityPlate\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDashboard\":-1,\"wheelColor\":156,\"modHood\":-1,\"modShifterLeavers\":-1,\"modEngine\":-1}', 'helicopter', 'police', 1),
('Char1:11000010a01bdb9', 'OLQ 222', '{\"pearlescentColor\":134,\"modRoof\":-1,\"modAirFilter\":-1,\"modSeats\":-1,\"modFender\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"modAerials\":-1,\"modHood\":-1,\"windowTint\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTank\":-1,\"dirtLevel\":1.0,\"color2\":134,\"modArchCover\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"extras\":[],\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modArmor\":-1,\"modSideSkirt\":-1,\"modXenon\":false,\"wheelColor\":134,\"modSpeakers\":-1,\"color1\":134,\"modTurbo\":false,\"modFrontBumper\":-1,\"neonColor\":[255,0,255],\"modAPlate\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"model\":-576293386,\"modGrille\":-1,\"modPlateHolder\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"health\":1000,\"modWindows\":-1,\"plateIndex\":4,\"wheels\":3,\"modStruts\":-1,\"modDial\":-1,\"modTransmission\":-1,\"modTrimB\":-1,\"plate\":\"OLQ 222\",\"modShifterLeavers\":-1,\"modLivery\":7,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'QVU 418', '{\"health\":0,\"modXenon\":false,\"modSpoilers\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modExhaust\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"extras\":[],\"modRightFender\":-1,\"modTrimB\":-1,\"wheels\":0,\"modShifterLeavers\":-1,\"neonColor\":[0,0,0],\"modTurbo\":false,\"color1\":0,\"modRoof\":-1,\"plateIndex\":-1,\"tyreSmokeColor\":[0,0,0],\"modHydrolic\":-1,\"modTransmission\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modSmokeEnabled\":false,\"plate\":\"QVU 418\",\"modTrunk\":-1,\"modOrnaments\":-1,\"modStruts\":-1,\"modAerials\":-1,\"model\":0,\"dirtLevel\":0.0,\"modHood\":-1,\"color2\":0,\"modFender\":-1,\"modRearBumper\":-1,\"pearlescentColor\":0,\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modHorns\":-1,\"modArchCover\":-1,\"modFrame\":-1,\"modFrontBumper\":-1,\"windowTint\":-1,\"wheelColor\":0,\"modEngine\":-1,\"modLivery\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modSeats\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false]}', 'car', 'ambulance', 1),
('Char1:11000010a01bdb9', 'TQP 485', '{\"modFender\":-1,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modGrille\":-1,\"modFrame\":-1,\"modXenon\":false,\"modArmor\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"dirtLevel\":0.0,\"modSteeringWheel\":-1,\"modDial\":-1,\"model\":178383100,\"modEngine\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"health\":1000,\"extras\":[],\"modBrakes\":-1,\"modHydrolic\":-1,\"modLivery\":0,\"tyreSmokeColor\":[255,255,255],\"modSpeakers\":-1,\"modRoof\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":false,\"pearlescentColor\":134,\"modExhaust\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modHood\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modHorns\":-1,\"color1\":111,\"modSideSkirt\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"color2\":1,\"modTurbo\":false,\"modTransmission\":-1,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"plate\":\"TQP 485\",\"modSuspension\":-1,\"modTank\":-1,\"wheels\":6,\"modRightFender\":-1,\"plateIndex\":4,\"modAPlate\":-1}', 'car', 'police', 0),
('Char1:11000010a01bdb9', 'UGA 061', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"UGA 061\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":3.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'UGU 071', '{\"modBackWheels\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"modAPlate\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modRoof\":-1,\"color1\":111,\"health\":1000,\"modGrille\":-1,\"modAirFilter\":-1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modFender\":-1,\"modTransmission\":-1,\"modSeats\":-1,\"modSuspension\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modEngine\":-1,\"wheelColor\":156,\"modWindows\":-1,\"dirtLevel\":11.0,\"modDashboard\":-1,\"neonEnabled\":[false,false,false,false],\"modVanityPlate\":-1,\"modXenon\":false,\"modShifterLeavers\":-1,\"modDial\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modAerials\":-1,\"color2\":1,\"modFrontBumper\":-1,\"wheels\":6,\"extras\":[],\"pearlescentColor\":134,\"modBrakes\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modTank\":-1,\"modHorns\":-1,\"plate\":\"UGU 071\",\"model\":178383100,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modHood\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'VFQ 240', '{\"modRightFender\":-1,\"modDial\":-1,\"plateIndex\":4,\"modAPlate\":-1,\"dirtLevel\":7.0,\"model\":-576293386,\"modWindows\":-1,\"modTrunk\":-1,\"modBrakes\":-1,\"modFender\":-1,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"modTransmission\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modHood\":-1,\"modExhaust\":-1,\"modRearBumper\":-1,\"modLivery\":4,\"modBackWheels\":-1,\"color1\":134,\"wheelColor\":134,\"modSeats\":-1,\"modRoof\":-1,\"modPlateHolder\":-1,\"modShifterLeavers\":-1,\"wheels\":3,\"modArchCover\":-1,\"modAerials\":-1,\"color2\":134,\"extras\":[],\"modEngineBlock\":-1,\"tyreSmokeColor\":[255,255,255],\"modHydrolic\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"modSpeakers\":-1,\"modFrame\":-1,\"modTank\":-1,\"plate\":\"VFQ 240\",\"modAirFilter\":-1,\"modOrnaments\":-1,\"modGrille\":-1,\"modTurbo\":false,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":134,\"modSmokeEnabled\":false,\"modSideSkirt\":-1,\"modHorns\":-1,\"modDashboard\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1}', 'car', 'ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15),
(7, 'LTDgasoline', 'fixkit', 0),
(8, 'TwentyFourSeven', 'binoculars', 1000),
(9, 'RobsLiquor', 'binoculars', 1000),
(10, 'LTDgasoline', 'binoculars', 1000),
(11, 'LTDgasoline', 'plongee1', 250),
(12, 'RobsLiquor', 'plongee1', 250),
(13, 'TwentyFourSeven', 'plongee1', 250),
(14, 'LTDgasoline', 'plongee2', 350),
(15, 'RobsLiquor', 'plongee2', 350),
(16, 'TwentyFourSeven', 'plongee2', 350),
(17, 'LTDgasoline', 'chocolate', 0),
(18, 'PDStation', 'coffee', 1),
(19, 'PDStation', 'donut', 1),
(20, 'PDStation', 'clip', 1),
(21, 'PDStation', 'armor', 1),
(22, 'DMV', 'drivers_license', 80),
(23, 'DMV', 'motorcycle_license', 80),
(24, 'DMV', 'commercial_license', 175),
(25, 'DMV', 'boating_license', 40),
(26, 'DMV', 'taxi_license', 175),
(27, 'City_Hall', 'weapons_license1', 100),
(28, 'City_Hall', 'weapons_license2', 200),
(29, 'City_Hall', 'weapons_license3', 300),
(30, 'City_Hall', 'hunting_license', 100),
(31, 'City_Hall', 'fishing_license', 100),
(32, 'City_Hall', 'diving_license', 50),
(33, 'City_Hall', 'marrage_license', 5000),
(34, 'DMV', 'pilot_license', 500),
(35, 'PDStation', 'medikit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `society` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `ID` int(255) DEFAULT NULL,
  `steamid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8mb4_bin DEFAULT 'user',
  `is_dead` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `last_property`, `phone_number`, `status`, `ID`, `steamid`, `rank`, `is_dead`) VALUES
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(138, 'steam:11000010a256910', 'black_money', 0),
(139, 'steam:110000117d5e2b7', 'black_money', 0),
(140, 'steam:1100001123cd531', 'black_money', 0),
(141, 'steam:1100001326230ef', 'black_money', 0),
(142, 'steam:11000010a266dc7', 'black_money', 0),
(143, 'steam:11000010e80a075', 'black_money', 0),
(144, 'steam:110000132b7eb1f', 'black_money', 0),
(145, 'steam:11000010b16012a', 'black_money', 0),
(146, 'steam:1100001184e4440', 'black_money', 0),
(147, 'steam:11000010f75c618', 'black_money', 0),
(148, 'steam:110000104ec6862', 'black_money', 0),
(149, 'steam:11000010c991cb2', 'black_money', 0),
(150, 'steam:11000010cd0b522', 'black_money', 0),
(151, 'steam:11000010b7982b5', 'black_money', 0),
(152, 'steam:11000010548afc9', 'black_money', 0),
(153, 'steam:1100001145029fc', 'black_money', 0),
(154, 'steam:110000116e20aa8', 'black_money', 0),
(155, 'steam:110000105318505', 'black_money', 0),
(156, 'steam:110000106def124', 'black_money', 0),
(157, 'steam:110000112206eef', 'black_money', 0),
(158, 'steam:110000106141cfc', 'black_money', 0),
(159, 'steam:110000119f838fa', 'black_money', 0),
(160, 'steam:110000107f1f2d6', 'black_money', 0),
(161, 'Char1:11000010a01bdb9', 'black_money', 49990000),
(162, 'steam:1100001068ef13c', 'black_money', 0),
(163, 'steam:110000132580eb0', 'black_money', 5000000),
(164, 'steam:110000112f07694', 'black_money', 0),
(165, 'steam:1100001042b46e1', 'black_money', 0),
(166, 'steam:11000011820eeac', 'black_money', 0),
(167, 'steam:11000011258948d', 'black_money', 0),
(168, 'steam:110000114590956', 'black_money', 0),
(169, 'steam:110000115c5ddc9', 'black_money', 0),
(170, 'steam:110000109660ef9', 'black_money', 0),
(171, 'steam:11000010c6acdce', 'black_money', 0),
(172, 'steam:110000106a5790d', 'black_money', 0),
(173, 'steam:11000010b0a1bc3', 'black_money', 0),
(174, 'steam:11000011b78df95', 'black_money', 0),
(175, 'steam:110000135c88cca', 'black_money', 0),
(176, 'steam:11000010473daf1', 'black_money', 0),
(177, 'steam:110000103b294e0', 'black_money', 0),
(178, 'steam:110000104e0ed21', 'black_money', 0),
(179, 'steam:1100001238ac476', 'black_money', 0),
(180, 'steam:11000011742efb1', 'black_money', 0),
(181, 'steam:110000112969e8f', 'black_money', 0),
(182, 'steam:110000134857568', 'black_money', 0),
(183, 'steam:110000131f0d83c', 'black_money', 0),
(184, 'steam:110000110931da8', 'black_money', 0),
(185, 'steam:1100001185894b3', 'black_money', 0),
(186, 'steam:11000011a15fea8', 'black_money', 0),
(187, 'steam:1100001183ded9e', 'black_money', 0),
(188, 'steam:1100001183ded9e', 'black_money', 0),
(189, 'steam:1100001183ded9e', 'black_money', 0),
(190, 'steam:1100001183ded9e', 'black_money', 0),
(191, 'steam:1100001183ded9e', 'black_money', 0),
(192, 'steam:1100001183ded9e', 'black_money', 0),
(193, 'steam:1100001183ded9e', 'black_money', 0),
(194, 'steam:1100001183ded9e', 'black_money', 0),
(195, 'steam:110000115815ad6', 'black_money', 0),
(196, 'steam:110000115815ad6', 'black_money', 0),
(197, 'steam:110000115815ad6', 'black_money', 0),
(198, 'steam:110000115815ad6', 'black_money', 0),
(199, 'steam:110000115815ad6', 'black_money', 0),
(200, 'steam:110000115815ad6', 'black_money', 0),
(201, 'steam:110000115815ad6', 'black_money', 0),
(202, 'steam:110000115815ad6', 'black_money', 0),
(203, 'Char2:11000010a01bdb9', 'black_money', 0),
(204, 'Char2:11000010a01bdb9', 'black_money', 0),
(205, 'Char2:11000010a01bdb9', 'black_money', 0),
(206, 'Char2:11000010a01bdb9', 'black_money', 0),
(207, 'Char2:11000010a01bdb9', 'black_money', 0),
(208, 'Char2:11000010a01bdb9', 'black_money', 0),
(209, 'Char2:11000010a01bdb9', 'black_money', 0),
(210, 'Char2:11000010a01bdb9', 'black_money', 0),
(211, 'steam:11000010a01bdb9', 'black_money', 0),
(212, 'steam:11000010a01bdb9', 'black_money', 0),
(213, 'steam:11000010a01bdb9', 'black_money', 0),
(214, 'steam:11000010a01bdb9', 'black_money', 0),
(215, 'steam:11000010a01bdb9', 'black_money', 0),
(216, 'steam:11000010a01bdb9', 'black_money', 0),
(217, 'steam:11000010a01bdb9', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_contacts`
--

INSERT INTO `user_contacts` (`id`, `identifier`, `name`, `number`) VALUES
(1, 'steam:110000112969e8f', 'K9', 84053),
(2, 'steam:110000132580eb0', 'J-bomb', 56216);

-- --------------------------------------------------------

--
-- Table structure for table `user_heli`
--

CREATE TABLE `user_heli` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `heli_name` varchar(255) DEFAULT NULL,
  `heli_model` varchar(255) DEFAULT NULL,
  `heli_price` int(60) DEFAULT NULL,
  `heli_plate` varchar(255) DEFAULT NULL,
  `heli_state` varchar(255) DEFAULT NULL,
  `heli_colorprimary` varchar(255) DEFAULT NULL,
  `heli_colorsecondary` varchar(255) DEFAULT NULL,
  `heli_pearlescentcolor` varchar(255) DEFAULT NULL,
  `heli_wheelcolor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(2, 'steam:11000010a01bdb9', 'vodka', 0),
(3, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(4, 'steam:11000010a01bdb9', 'clothe', 0),
(5, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(6, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(7, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(8, 'steam:11000010a01bdb9', 'clothe', 0),
(9, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(10, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(11, 'steam:11000010a01bdb9', 'clothe', 0),
(12, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(13, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(14, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(15, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(16, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(17, 'steam:11000010a01bdb9', 'clothe', 0),
(18, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(19, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(20, 'steam:11000010a01bdb9', 'clothe', 0),
(21, 'steam:11000010a01bdb9', 'vodka', 0),
(22, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(23, 'steam:11000010a01bdb9', 'vodka', 0),
(24, 'steam:11000010a01bdb9', 'vodka', 0),
(25, 'steam:11000010a01bdb9', 'clothe', 0),
(26, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(27, 'steam:11000010a01bdb9', 'vodka', 0),
(28, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(29, 'steam:11000010a01bdb9', 'vodka', 0),
(30, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(31, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(32, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(33, 'steam:11000010a01bdb9', 'silent', 0),
(34, 'steam:11000010a01bdb9', 'jusfruit', 0),
(35, 'steam:11000010a01bdb9', 'coffee', 0),
(36, 'steam:11000010a01bdb9', 'silent', 0),
(37, 'steam:11000010a01bdb9', 'water', 0),
(38, 'steam:11000010a01bdb9', 'jusfruit', 0),
(39, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(40, 'steam:11000010a01bdb9', 'water', 0),
(41, 'steam:11000010a01bdb9', 'silent', 0),
(42, 'steam:11000010a01bdb9', 'water', 0),
(43, 'steam:11000010a01bdb9', 'silent', 0),
(44, 'steam:11000010a01bdb9', 'coffee', 0),
(45, 'steam:11000010a01bdb9', 'coffee', 0),
(46, 'steam:11000010a01bdb9', 'silent', 0),
(47, 'steam:11000010a01bdb9', 'coffee', 0),
(48, 'steam:11000010a01bdb9', 'silent', 0),
(49, 'steam:11000010a01bdb9', 'water', 0),
(50, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(51, 'steam:11000010a01bdb9', 'coffee', 0),
(52, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(53, 'steam:11000010a01bdb9', 'jusfruit', 0),
(54, 'steam:11000010a01bdb9', 'jusfruit', 0),
(55, 'steam:11000010a01bdb9', 'jusfruit', 0),
(56, 'steam:11000010a01bdb9', 'water', 0),
(57, 'steam:11000010a01bdb9', 'coffee', 0),
(58, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(59, 'steam:11000010a01bdb9', 'water', 0),
(60, 'steam:11000010a01bdb9', 'jusfruit', 0),
(61, 'steam:11000010a01bdb9', 'yusuf', 0),
(62, 'steam:11000010a01bdb9', 'binoculars', 0),
(63, 'steam:11000010a01bdb9', 'yusuf', 0),
(64, 'steam:11000010a01bdb9', 'yusuf', 0),
(65, 'steam:11000010a01bdb9', 'binoculars', 0),
(66, 'steam:11000010a01bdb9', 'yusuf', 0),
(67, 'steam:11000010a01bdb9', 'yusuf', 0),
(68, 'steam:11000010a01bdb9', 'lowrider', 0),
(69, 'steam:11000010a01bdb9', 'jusfruit', 0),
(70, 'steam:11000010a01bdb9', 'jusfruit', 0),
(71, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(72, 'steam:11000010a01bdb9', 'silent', 0),
(73, 'steam:11000010a01bdb9', 'coffee', 0),
(74, 'steam:11000010a01bdb9', 'water', 0),
(75, 'steam:11000010a01bdb9', 'water', 0),
(76, 'steam:11000010a01bdb9', 'yusuf', 0),
(77, 'steam:11000010a01bdb9', 'coffee', 0),
(78, 'steam:11000010a01bdb9', 'silent', 0),
(79, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(80, 'steam:11000010a01bdb9', 'lowrider', 0),
(81, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(82, 'steam:11000010a01bdb9', 'tequila', 0),
(83, 'steam:11000010a01bdb9', 'lowrider', 0),
(84, 'steam:11000010a01bdb9', 'carotool', 0),
(85, 'steam:11000010a01bdb9', 'binoculars', 0),
(86, 'steam:11000010a01bdb9', 'fish', 0),
(87, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(88, 'steam:11000010a01bdb9', 'binoculars', 0),
(89, 'steam:11000010a01bdb9', 'tequila', 0),
(90, 'steam:11000010a01bdb9', 'lowrider', 0),
(91, 'steam:11000010a01bdb9', 'binoculars', 0),
(92, 'steam:11000010a01bdb9', 'tequila', 0),
(93, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(94, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(95, 'steam:11000010a01bdb9', 'fish', 0),
(96, 'steam:11000010a01bdb9', 'fish', 0),
(97, 'steam:11000010a01bdb9', 'binoculars', 0),
(98, 'steam:11000010a01bdb9', 'binoculars', 0),
(99, 'steam:11000010a01bdb9', 'fish', 0),
(100, 'steam:11000010a01bdb9', 'fish', 0),
(101, 'steam:11000010a01bdb9', 'tequila', 0),
(102, 'steam:11000010a01bdb9', 'yusuf', 0),
(103, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(104, 'steam:11000010a01bdb9', 'lowrider', 0),
(105, 'steam:11000010a01bdb9', 'lowrider', 0),
(106, 'steam:11000010a01bdb9', 'yusuf', 0),
(107, 'steam:11000010a01bdb9', 'fish', 0),
(108, 'steam:11000010a01bdb9', 'tequila', 0),
(109, 'steam:11000010a01bdb9', 'tequila', 0),
(110, 'steam:11000010a01bdb9', 'fish', 0),
(111, 'steam:11000010a01bdb9', 'tequila', 0),
(112, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(113, 'steam:11000010a01bdb9', 'binoculars', 0),
(114, 'steam:11000010a01bdb9', 'lowrider', 0),
(115, 'steam:11000010a01bdb9', 'fish', 0),
(116, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(117, 'steam:11000010a01bdb9', 'lowrider', 0),
(118, 'steam:11000010a01bdb9', 'tequila', 0),
(119, 'steam:11000010a01bdb9', 'carotool', 0),
(120, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(121, 'steam:11000010a01bdb9', 'contrat', 0),
(122, 'steam:11000010a01bdb9', 'carotool', 0),
(123, 'steam:11000010a01bdb9', 'contrat', 0),
(124, 'steam:11000010a01bdb9', 'martini', 0),
(125, 'steam:11000010a01bdb9', 'carotool', 0),
(126, 'steam:11000010a01bdb9', 'contrat', 0),
(127, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(128, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(129, 'steam:11000010a01bdb9', 'contrat', 0),
(130, 'steam:11000010a01bdb9', 'carotool', 0),
(131, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(132, 'steam:11000010a01bdb9', 'menthe', 0),
(133, 'steam:11000010a01bdb9', 'martini', 0),
(134, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(135, 'steam:11000010a01bdb9', 'contrat', 0),
(136, 'steam:11000010a01bdb9', 'contrat', 0),
(137, 'steam:11000010a01bdb9', 'menthe', 0),
(138, 'steam:11000010a01bdb9', 'martini', 0),
(139, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(140, 'steam:11000010a01bdb9', 'menthe', 0),
(141, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(142, 'steam:11000010a01bdb9', 'martini', 0),
(143, 'steam:11000010a01bdb9', 'martini', 0),
(144, 'steam:11000010a01bdb9', 'tacos', 0),
(145, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(146, 'steam:11000010a01bdb9', 'contrat', 0),
(147, 'steam:11000010a01bdb9', 'menthe', 0),
(148, 'steam:11000010a01bdb9', 'flashlight', 0),
(149, 'steam:11000010a01bdb9', 'carotool', 0),
(150, 'steam:11000010a01bdb9', 'martini', 0),
(151, 'steam:11000010a01bdb9', 'menthe', 0),
(152, 'steam:11000010a01bdb9', 'menthe', 0),
(153, 'steam:11000010a01bdb9', 'carotool', 0),
(154, 'steam:11000010a01bdb9', 'carotool', 0),
(155, 'steam:11000010a01bdb9', 'contrat', 0),
(156, 'steam:11000010a01bdb9', 'martini', 0),
(157, 'steam:11000010a01bdb9', 'martini', 0),
(158, 'steam:11000010a01bdb9', 'tacos', 0),
(159, 'steam:11000010a01bdb9', 'tacos', 0),
(160, 'steam:11000010a01bdb9', 'menthe', 0),
(161, 'steam:11000010a01bdb9', 'bolpistache', 0),
(162, 'steam:11000010a01bdb9', 'tacos', 0),
(163, 'steam:11000010a01bdb9', 'flashlight', 0),
(164, 'steam:11000010a01bdb9', 'boating_license', 0),
(165, 'steam:11000010a01bdb9', 'flashlight', 0),
(166, 'steam:11000010a01bdb9', 'tacos', 0),
(167, 'steam:11000010a01bdb9', 'bolpistache', 0),
(168, 'steam:11000010a01bdb9', 'tacos', 0),
(169, 'steam:11000010a01bdb9', 'boating_license', 0),
(170, 'steam:11000010a01bdb9', 'bolpistache', 0),
(171, 'steam:11000010a01bdb9', 'bolpistache', 0),
(172, 'steam:11000010a01bdb9', 'menthe', 0),
(173, 'steam:11000010a01bdb9', 'flashlight', 0),
(174, 'steam:11000010a01bdb9', 'flashlight', 0),
(175, 'steam:11000010a01bdb9', 'clip', 0),
(176, 'steam:11000010a01bdb9', 'clip', 0),
(177, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(178, 'steam:11000010a01bdb9', 'clip', 0),
(179, 'steam:11000010a01bdb9', 'bolpistache', 0),
(180, 'steam:11000010a01bdb9', 'bolpistache', 0),
(181, 'steam:11000010a01bdb9', 'bolpistache', 0),
(182, 'steam:11000010a01bdb9', 'clip', 0),
(183, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(184, 'steam:11000010a01bdb9', 'clip', 0),
(185, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(186, 'steam:11000010a01bdb9', 'boating_license', 0),
(187, 'steam:11000010a01bdb9', 'boating_license', 0),
(188, 'steam:11000010a01bdb9', 'tacos', 0),
(189, 'steam:11000010a01bdb9', 'boating_license', 0),
(190, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(191, 'steam:11000010a01bdb9', 'boating_license', 0),
(192, 'steam:11000010a01bdb9', 'boating_license', 0),
(193, 'steam:11000010a01bdb9', 'flashlight', 0),
(194, 'steam:11000010a01bdb9', 'clip', 0),
(195, 'steam:11000010a01bdb9', 'bolpistache', 0),
(196, 'steam:11000010a01bdb9', 'tacos', 0),
(197, 'steam:11000010a01bdb9', 'flashlight', 0),
(198, 'steam:11000010a01bdb9', 'flashlight', 0),
(199, 'steam:11000010a01bdb9', 'clip', 0),
(200, 'steam:11000010a01bdb9', 'boating_license', 0),
(201, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(202, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(203, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(204, 'steam:11000010a01bdb9', 'cola', 0),
(205, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(206, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(207, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(208, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(209, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(210, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(211, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(212, 'steam:11000010a01bdb9', 'bait', 0),
(213, 'steam:11000010a01bdb9', 'cola', 0),
(214, 'steam:11000010a01bdb9', 'clip', 0),
(215, 'steam:11000010a01bdb9', 'cola', 0),
(216, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(217, 'steam:11000010a01bdb9', 'bait', 0),
(218, 'steam:11000010a01bdb9', 'cola', 0),
(219, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(220, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(221, 'steam:11000010a01bdb9', 'cola', 0),
(222, 'steam:11000010a01bdb9', 'bait', 0),
(223, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(224, 'steam:11000010a01bdb9', 'bait', 0),
(225, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(226, 'steam:11000010a01bdb9', 'weed', 0),
(227, 'steam:11000010a01bdb9', 'weed', 0),
(228, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(229, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(230, 'steam:11000010a01bdb9', 'cola', 0),
(231, 'steam:11000010a01bdb9', 'bait', 0),
(232, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(233, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(234, 'steam:11000010a01bdb9', 'fishingrod', 0),
(235, 'steam:11000010a01bdb9', 'fmj', 0),
(236, 'steam:11000010a01bdb9', 'weed', 0),
(237, 'steam:11000010a01bdb9', 'weed', 0),
(238, 'steam:11000010a01bdb9', 'bait', 0),
(239, 'steam:11000010a01bdb9', 'fmj', 0),
(240, 'steam:11000010a01bdb9', 'bait', 0),
(241, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(242, 'steam:11000010a01bdb9', 'weed', 0),
(243, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(244, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(245, 'steam:11000010a01bdb9', 'bait', 0),
(246, 'steam:11000010a01bdb9', 'fishingrod', 0),
(247, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(248, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(249, 'steam:11000010a01bdb9', 'fishingrod', 0),
(250, 'steam:11000010a01bdb9', 'fishingrod', 0),
(251, 'steam:11000010a01bdb9', 'fmj', 0),
(252, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(253, 'steam:11000010a01bdb9', 'cola', 0),
(254, 'steam:11000010a01bdb9', 'weed', 0),
(255, 'steam:11000010a01bdb9', 'fmj', 0),
(256, 'steam:11000010a01bdb9', 'cola', 0),
(257, 'steam:11000010a01bdb9', 'fishingrod', 0),
(258, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(259, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(260, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(261, 'steam:11000010a01bdb9', 'fishingrod', 0),
(262, 'steam:11000010a01bdb9', 'fmj', 0),
(263, 'steam:11000010a01bdb9', 'weed', 0),
(264, 'steam:11000010a01bdb9', 'fmj', 0),
(265, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(266, 'steam:11000010a01bdb9', 'weed', 0),
(267, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(268, 'steam:11000010a01bdb9', 'fabric', 0),
(269, 'steam:11000010a01bdb9', 'fishingrod', 0),
(270, 'steam:11000010a01bdb9', 'fabric', 0),
(271, 'steam:11000010a01bdb9', 'fishingrod', 0),
(272, 'steam:11000010a01bdb9', 'plongee2', 0),
(273, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(274, 'steam:11000010a01bdb9', 'fmj', 0),
(275, 'steam:11000010a01bdb9', 'donut', 0),
(276, 'steam:11000010a01bdb9', 'incendiary', 0),
(277, 'steam:11000010a01bdb9', 'incendiary', 0),
(278, 'steam:11000010a01bdb9', 'fabric', 0),
(279, 'steam:11000010a01bdb9', 'incendiary', 0),
(280, 'steam:11000010a01bdb9', 'donut', 0),
(281, 'steam:11000010a01bdb9', 'incendiary', 0),
(282, 'steam:11000010a01bdb9', 'burger', 0),
(283, 'steam:11000010a01bdb9', 'donut', 0),
(284, 'steam:11000010a01bdb9', 'fabric', 0),
(285, 'steam:11000010a01bdb9', 'burger', 0),
(286, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(287, 'steam:11000010a01bdb9', 'fabric', 0),
(288, 'steam:11000010a01bdb9', 'fabric', 0),
(289, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(290, 'steam:11000010a01bdb9', 'fmj', 0),
(291, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(292, 'steam:11000010a01bdb9', 'burger', 0),
(293, 'steam:11000010a01bdb9', 'incendiary', 0),
(294, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(295, 'steam:11000010a01bdb9', 'plongee2', 0),
(296, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(297, 'steam:11000010a01bdb9', 'donut', 0),
(298, 'steam:11000010a01bdb9', 'burger', 0),
(299, 'steam:11000010a01bdb9', 'incendiary', 0),
(300, 'steam:11000010a01bdb9', 'fabric', 0),
(301, 'steam:11000010a01bdb9', 'plongee2', 0),
(302, 'steam:11000010a01bdb9', 'fabric', 0),
(303, 'steam:11000010a01bdb9', 'donut', 0),
(304, 'steam:11000010a01bdb9', 'donut', 0),
(305, 'steam:11000010a01bdb9', 'burger', 0),
(306, 'steam:11000010a01bdb9', 'incendiary', 0),
(307, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(308, 'steam:11000010a01bdb9', 'plongee2', 0),
(309, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(310, 'steam:11000010a01bdb9', 'burger', 0),
(311, 'steam:11000010a01bdb9', 'incendiary', 0),
(312, 'steam:11000010a01bdb9', 'plongee2', 0),
(313, 'steam:11000010a01bdb9', 'essence', 0),
(314, 'steam:11000010a01bdb9', 'iron', 0),
(315, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(316, 'steam:11000010a01bdb9', 'plongee1', 0),
(317, 'steam:11000010a01bdb9', 'plongee1', 0),
(318, 'steam:11000010a01bdb9', 'donut', 0),
(319, 'steam:11000010a01bdb9', 'mixapero', 0),
(320, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(321, 'steam:11000010a01bdb9', 'iron', 0),
(322, 'steam:11000010a01bdb9', 'burger', 0),
(323, 'steam:11000010a01bdb9', 'essence', 0),
(324, 'steam:11000010a01bdb9', 'burger', 0),
(325, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(326, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(327, 'steam:11000010a01bdb9', 'plongee1', 0),
(328, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(329, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(330, 'steam:11000010a01bdb9', 'plongee2', 0),
(331, 'steam:11000010a01bdb9', 'iron', 0),
(332, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(333, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(334, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(335, 'steam:11000010a01bdb9', 'donut', 0),
(336, 'steam:11000010a01bdb9', 'essence', 0),
(337, 'steam:11000010a01bdb9', 'essence', 0),
(338, 'steam:11000010a01bdb9', 'essence', 0),
(339, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(340, 'steam:11000010a01bdb9', 'mixapero', 0),
(341, 'steam:11000010a01bdb9', 'plongee2', 0),
(342, 'steam:11000010a01bdb9', 'iron', 0),
(343, 'steam:11000010a01bdb9', 'plongee1', 0),
(344, 'steam:11000010a01bdb9', 'essence', 0),
(345, 'steam:11000010a01bdb9', 'plongee2', 0),
(346, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(347, 'steam:11000010a01bdb9', 'plongee1', 0),
(348, 'steam:11000010a01bdb9', 'plongee1', 0),
(349, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(350, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(351, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(352, 'steam:11000010a01bdb9', 'mixapero', 0),
(353, 'steam:11000010a01bdb9', 'mojito', 0),
(354, 'steam:11000010a01bdb9', 'diamond', 0),
(355, 'steam:11000010a01bdb9', 'drpepper', 0),
(356, 'steam:11000010a01bdb9', 'icetea', 0),
(357, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(358, 'steam:11000010a01bdb9', 'icetea', 0),
(359, 'steam:11000010a01bdb9', 'essence', 0),
(360, 'steam:11000010a01bdb9', 'mixapero', 0),
(361, 'steam:11000010a01bdb9', 'mojito', 0),
(362, 'steam:11000010a01bdb9', 'mixapero', 0),
(363, 'steam:11000010a01bdb9', 'essence', 0),
(364, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(365, 'steam:11000010a01bdb9', 'drpepper', 0),
(366, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(367, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(368, 'steam:11000010a01bdb9', 'mixapero', 0),
(369, 'steam:11000010a01bdb9', 'diamond', 0),
(370, 'steam:11000010a01bdb9', 'icetea', 0),
(371, 'steam:11000010a01bdb9', 'mixapero', 0),
(372, 'steam:11000010a01bdb9', 'mixapero', 0),
(373, 'steam:11000010a01bdb9', 'drpepper', 0),
(374, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(375, 'steam:11000010a01bdb9', 'iron', 0),
(376, 'steam:11000010a01bdb9', 'drpepper', 0),
(377, 'steam:11000010a01bdb9', 'drpepper', 0),
(378, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(379, 'steam:11000010a01bdb9', 'iron', 0),
(380, 'steam:11000010a01bdb9', 'plongee1', 0),
(381, 'steam:11000010a01bdb9', 'plongee1', 0),
(382, 'steam:11000010a01bdb9', 'iron', 0),
(383, 'steam:11000010a01bdb9', 'icetea', 0),
(384, 'steam:11000010a01bdb9', 'icetea', 0),
(385, 'steam:11000010a01bdb9', 'iron', 0),
(386, 'steam:11000010a01bdb9', 'drpepper', 0),
(387, 'steam:11000010a01bdb9', 'diamond', 0),
(388, 'steam:11000010a01bdb9', 'icetea', 0),
(389, 'steam:11000010a01bdb9', 'icetea', 0),
(390, 'steam:11000010a01bdb9', 'diamond', 0),
(391, 'steam:11000010a01bdb9', 'icetea', 0),
(392, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(393, 'steam:11000010a01bdb9', 'jager', 0),
(394, 'steam:11000010a01bdb9', 'jager', 0),
(395, 'steam:11000010a01bdb9', 'whool', 0),
(396, 'steam:11000010a01bdb9', 'jager', 0),
(397, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(398, 'steam:11000010a01bdb9', 'mojito', 0),
(399, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(400, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(401, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(402, 'steam:11000010a01bdb9', 'jager', 0),
(403, 'steam:11000010a01bdb9', 'mojito', 0),
(404, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(405, 'steam:11000010a01bdb9', 'mojito', 0),
(406, 'steam:11000010a01bdb9', 'whool', 0),
(407, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(408, 'steam:11000010a01bdb9', 'drpepper', 0),
(409, 'steam:11000010a01bdb9', 'mojito', 0),
(410, 'steam:11000010a01bdb9', 'mojito', 0),
(411, 'steam:11000010a01bdb9', 'whool', 0),
(412, 'steam:11000010a01bdb9', 'diamond', 0),
(413, 'steam:11000010a01bdb9', 'drpepper', 0),
(414, 'steam:11000010a01bdb9', 'whool', 0),
(415, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(416, 'steam:11000010a01bdb9', 'mojito', 0),
(417, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(418, 'steam:11000010a01bdb9', 'jager', 0),
(419, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(420, 'steam:11000010a01bdb9', 'diamond', 0),
(421, 'steam:11000010a01bdb9', 'diamond', 0),
(422, 'steam:11000010a01bdb9', 'whool', 0),
(423, 'steam:11000010a01bdb9', 'jager', 0),
(424, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(425, 'steam:11000010a01bdb9', 'diamond', 0),
(426, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(427, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(428, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(429, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(430, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(431, 'steam:11000010a01bdb9', 'whool', 0),
(432, 'steam:11000010a01bdb9', 'bread', 0),
(433, 'steam:11000010a01bdb9', 'coke', 0),
(434, 'steam:11000010a01bdb9', 'golem', 0),
(435, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(436, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(437, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(438, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(439, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(440, 'steam:11000010a01bdb9', 'whool', 0),
(441, 'steam:11000010a01bdb9', 'bread', 0),
(442, 'steam:11000010a01bdb9', 'coke', 0),
(443, 'steam:11000010a01bdb9', 'coke', 0),
(444, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(445, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(446, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(447, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(448, 'steam:11000010a01bdb9', 'bread', 0),
(449, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(450, 'steam:11000010a01bdb9', 'bread', 0),
(451, 'steam:11000010a01bdb9', 'coke', 0),
(452, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(453, 'steam:11000010a01bdb9', 'golem', 0),
(454, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(455, 'steam:11000010a01bdb9', 'coke', 0),
(456, 'steam:11000010a01bdb9', 'bread', 0),
(457, 'steam:11000010a01bdb9', 'whool', 0),
(458, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(459, 'steam:11000010a01bdb9', 'jager', 0),
(460, 'steam:11000010a01bdb9', 'jager', 0),
(461, 'steam:11000010a01bdb9', 'bread', 0),
(462, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(463, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(464, 'steam:11000010a01bdb9', 'bread', 0),
(465, 'steam:11000010a01bdb9', 'coke', 0),
(466, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(467, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(468, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(469, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(470, 'steam:11000010a01bdb9', 'bread', 0),
(471, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(472, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(473, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(474, 'steam:11000010a01bdb9', 'bolchips', 0),
(475, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(476, 'steam:11000010a01bdb9', 'whisky', 0),
(477, 'steam:11000010a01bdb9', 'golem', 0),
(478, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(479, 'steam:11000010a01bdb9', 'golem', 0),
(480, 'steam:11000010a01bdb9', 'bolchips', 0),
(481, 'steam:11000010a01bdb9', 'commercial_license', 0),
(482, 'steam:11000010a01bdb9', 'bolchips', 0),
(483, 'steam:11000010a01bdb9', 'golem', 0),
(484, 'steam:11000010a01bdb9', 'golem', 0),
(485, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(486, 'steam:11000010a01bdb9', 'bolchips', 0),
(487, 'steam:11000010a01bdb9', 'whisky', 0),
(488, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(489, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(490, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(491, 'steam:11000010a01bdb9', 'bolchips', 0),
(492, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(493, 'steam:11000010a01bdb9', 'whisky', 0),
(494, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(495, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(496, 'steam:11000010a01bdb9', 'bolchips', 0),
(497, 'steam:11000010a01bdb9', 'lsd', 0),
(498, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(499, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(500, 'steam:11000010a01bdb9', 'coke', 0),
(501, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(502, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(503, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(504, 'steam:11000010a01bdb9', 'coke', 0),
(505, 'steam:11000010a01bdb9', 'golem', 0),
(506, 'steam:11000010a01bdb9', 'whisky', 0),
(507, 'steam:11000010a01bdb9', 'lsd', 0),
(508, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(509, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(510, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(511, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(512, 'steam:11000010a01bdb9', 'golem', 0),
(513, 'steam:11000010a01bdb9', 'energy', 0),
(514, 'steam:11000010a01bdb9', 'lsd', 0),
(515, 'steam:11000010a01bdb9', 'whisky', 0),
(516, 'steam:11000010a01bdb9', 'commercial_license', 0),
(517, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(518, 'steam:11000010a01bdb9', 'soda', 0),
(519, 'steam:11000010a01bdb9', 'energy', 0),
(520, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(521, 'steam:11000010a01bdb9', 'lsd', 0),
(522, 'steam:11000010a01bdb9', 'lsd', 0),
(523, 'steam:11000010a01bdb9', 'lsd', 0),
(524, 'steam:11000010a01bdb9', 'compansator', 0),
(525, 'steam:11000010a01bdb9', 'bolchips', 0),
(526, 'steam:11000010a01bdb9', 'commercial_license', 0),
(527, 'steam:11000010a01bdb9', 'commercial_license', 0),
(528, 'steam:11000010a01bdb9', 'soda', 0),
(529, 'steam:11000010a01bdb9', 'energy', 0),
(530, 'steam:11000010a01bdb9', 'compansator', 0),
(531, 'steam:11000010a01bdb9', 'whisky', 0),
(532, 'steam:11000010a01bdb9', 'commercial_license', 0),
(533, 'steam:11000010a01bdb9', 'whisky', 0),
(534, 'steam:11000010a01bdb9', 'compansator', 0),
(535, 'steam:11000010a01bdb9', 'compansator', 0),
(536, 'steam:11000010a01bdb9', 'soda', 0),
(537, 'steam:11000010a01bdb9', 'soda', 0),
(538, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(539, 'steam:11000010a01bdb9', 'bolchips', 0),
(540, 'steam:11000010a01bdb9', 'energy', 0),
(541, 'steam:11000010a01bdb9', 'commercial_license', 0),
(542, 'steam:11000010a01bdb9', 'ice', 0),
(543, 'steam:11000010a01bdb9', 'commercial_license', 0),
(544, 'steam:11000010a01bdb9', 'energy', 0),
(545, 'steam:11000010a01bdb9', 'whisky', 0),
(546, 'steam:11000010a01bdb9', 'compansator', 0),
(547, 'steam:11000010a01bdb9', 'lsd', 0),
(548, 'steam:11000010a01bdb9', 'energy', 0),
(549, 'steam:11000010a01bdb9', 'commercial_license', 0),
(550, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(551, 'steam:11000010a01bdb9', 'compansator', 0),
(552, 'steam:11000010a01bdb9', 'lsd', 0),
(553, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(554, 'steam:11000010a01bdb9', 'ice', 0),
(555, 'steam:11000010a01bdb9', 'soda', 0),
(556, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(557, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(558, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(559, 'steam:11000010a01bdb9', 'ice', 0),
(560, 'steam:11000010a01bdb9', 'ice', 0),
(561, 'steam:11000010a01bdb9', 'teqpaf', 0),
(562, 'steam:11000010a01bdb9', 'teqpaf', 0),
(563, 'steam:11000010a01bdb9', 'teqpaf', 0),
(564, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(565, 'steam:11000010a01bdb9', 'saucisson', 0),
(566, 'steam:11000010a01bdb9', 'wood', 0),
(567, 'steam:11000010a01bdb9', 'teqpaf', 0),
(568, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(569, 'steam:11000010a01bdb9', 'soda', 0),
(570, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(571, 'steam:11000010a01bdb9', 'wood', 0),
(572, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(573, 'steam:11000010a01bdb9', 'energy', 0),
(574, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(575, 'steam:11000010a01bdb9', 'wood', 0),
(576, 'steam:11000010a01bdb9', 'ice', 0),
(577, 'steam:11000010a01bdb9', 'ice', 0),
(578, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(579, 'steam:11000010a01bdb9', 'compansator', 0),
(580, 'steam:11000010a01bdb9', 'wood', 0),
(581, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(582, 'steam:11000010a01bdb9', 'energy', 0),
(583, 'steam:11000010a01bdb9', 'teqpaf', 0),
(584, 'steam:11000010a01bdb9', 'teqpaf', 0),
(585, 'steam:11000010a01bdb9', 'compansator', 0),
(586, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(587, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(588, 'steam:11000010a01bdb9', 'ice', 0),
(589, 'steam:11000010a01bdb9', 'soda', 0),
(590, 'steam:11000010a01bdb9', 'soda', 0),
(591, 'steam:11000010a01bdb9', 'ice', 0),
(592, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(593, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(594, 'steam:11000010a01bdb9', 'wood', 0),
(595, 'steam:11000010a01bdb9', 'saucisson', 0),
(596, 'steam:11000010a01bdb9', 'teqpaf', 0),
(597, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(598, 'steam:11000010a01bdb9', 'saucisson', 0),
(599, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(600, 'steam:11000010a01bdb9', 'vegetables', 0),
(601, 'steam:11000010a01bdb9', 'wood', 0),
(602, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(603, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(604, 'steam:11000010a01bdb9', 'wood', 0),
(605, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(606, 'steam:11000010a01bdb9', 'saucisson', 0),
(607, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(608, 'steam:11000010a01bdb9', 'pilot_license', 0),
(609, 'steam:11000010a01bdb9', 'meth', 0),
(610, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(611, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(612, 'steam:11000010a01bdb9', 'vegetables', 0),
(613, 'steam:11000010a01bdb9', 'vegetables', 0),
(614, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(615, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(616, 'steam:11000010a01bdb9', 'saucisson', 0),
(617, 'steam:11000010a01bdb9', 'meth', 0),
(618, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(619, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(620, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(621, 'steam:11000010a01bdb9', 'vegetables', 0),
(622, 'steam:11000010a01bdb9', 'teqpaf', 0),
(623, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(624, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(625, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(626, 'steam:11000010a01bdb9', 'saucisson', 0),
(627, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(628, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(629, 'steam:11000010a01bdb9', 'wood', 0),
(630, 'steam:11000010a01bdb9', 'saucisson', 0),
(631, 'steam:11000010a01bdb9', 'saucisson', 0),
(632, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(633, 'steam:11000010a01bdb9', 'pilot_license', 0),
(634, 'steam:11000010a01bdb9', 'marrage_license', 0),
(635, 'steam:11000010a01bdb9', 'marrage_license', 0),
(636, 'steam:11000010a01bdb9', 'gazbottle', 0),
(637, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(638, 'steam:11000010a01bdb9', 'meth', 0),
(639, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(640, 'steam:11000010a01bdb9', 'hollow', 0),
(641, 'steam:11000010a01bdb9', 'pilot_license', 0),
(642, 'steam:11000010a01bdb9', 'gazbottle', 0),
(643, 'steam:11000010a01bdb9', 'meth', 0),
(644, 'steam:11000010a01bdb9', 'hollow', 0),
(645, 'steam:11000010a01bdb9', 'vegetables', 0),
(646, 'steam:11000010a01bdb9', 'marrage_license', 0),
(647, 'steam:11000010a01bdb9', 'gazbottle', 0),
(648, 'steam:11000010a01bdb9', 'vegetables', 0),
(649, 'steam:11000010a01bdb9', 'pilot_license', 0),
(650, 'steam:11000010a01bdb9', 'meth', 0),
(651, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(652, 'steam:11000010a01bdb9', 'diving_license', 0),
(653, 'steam:11000010a01bdb9', 'hollow', 0),
(654, 'steam:11000010a01bdb9', 'vegetables', 0),
(655, 'steam:11000010a01bdb9', 'vegetables', 0),
(656, 'steam:11000010a01bdb9', 'pilot_license', 0),
(657, 'steam:11000010a01bdb9', 'meth', 0),
(658, 'steam:11000010a01bdb9', 'meth', 0),
(659, 'steam:11000010a01bdb9', 'marrage_license', 0),
(660, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(661, 'steam:11000010a01bdb9', 'hollow', 0),
(662, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(663, 'steam:11000010a01bdb9', 'marrage_license', 0),
(664, 'steam:11000010a01bdb9', 'pilot_license', 0),
(665, 'steam:11000010a01bdb9', 'gazbottle', 0),
(666, 'steam:11000010a01bdb9', 'pilot_license', 0),
(667, 'steam:11000010a01bdb9', 'hollow', 0),
(668, 'steam:11000010a01bdb9', 'pilot_license', 0),
(669, 'steam:11000010a01bdb9', 'meth', 0),
(670, 'steam:11000010a01bdb9', 'hollow', 0),
(671, 'steam:11000010a01bdb9', 'marrage_license', 0),
(672, 'steam:11000010a01bdb9', 'diving_license', 0),
(673, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(674, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(675, 'steam:11000010a01bdb9', 'diving_license', 0),
(676, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(677, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(678, 'steam:11000010a01bdb9', 'gazbottle', 0),
(679, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(680, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(681, 'steam:11000010a01bdb9', 'scope', 0),
(682, 'steam:11000010a01bdb9', 'gazbottle', 0),
(683, 'steam:11000010a01bdb9', 'marrage_license', 0),
(684, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(685, 'steam:11000010a01bdb9', 'fishing_license', 0),
(686, 'steam:11000010a01bdb9', 'scope', 0),
(687, 'steam:11000010a01bdb9', 'gazbottle', 0),
(688, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(689, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(690, 'steam:11000010a01bdb9', 'diving_license', 0),
(691, 'steam:11000010a01bdb9', 'fishing_license', 0),
(692, 'steam:11000010a01bdb9', 'scope', 0),
(693, 'steam:11000010a01bdb9', 'hollow', 0),
(694, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(695, 'steam:11000010a01bdb9', 'fishing_license', 0),
(696, 'steam:11000010a01bdb9', 'diving_license', 0),
(697, 'steam:11000010a01bdb9', 'fishing_license', 0),
(698, 'steam:11000010a01bdb9', 'diving_license', 0),
(699, 'steam:11000010a01bdb9', 'scope', 0),
(700, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(701, 'steam:11000010a01bdb9', 'gazbottle', 0),
(702, 'steam:11000010a01bdb9', 'scope', 0),
(703, 'steam:11000010a01bdb9', 'diving_license', 0),
(704, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(705, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(706, 'steam:11000010a01bdb9', 'scope', 0),
(707, 'steam:11000010a01bdb9', 'hollow', 0),
(708, 'steam:11000010a01bdb9', 'marrage_license', 0),
(709, 'steam:11000010a01bdb9', 'diving_license', 0),
(710, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(711, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(712, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(713, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(714, 'steam:11000010a01bdb9', 'fixkit', 0),
(715, 'steam:11000010a01bdb9', 'washed_stone', 0),
(716, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(717, 'steam:11000010a01bdb9', 'petrol', 0),
(718, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(719, 'steam:11000010a01bdb9', 'taxi_license', 0),
(720, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(721, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(722, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(723, 'steam:11000010a01bdb9', 'fishing_license', 0),
(724, 'steam:11000010a01bdb9', 'fishing_license', 0),
(725, 'steam:11000010a01bdb9', 'fishing_license', 0),
(726, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(727, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(728, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(729, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(730, 'steam:11000010a01bdb9', 'fixkit', 0),
(731, 'steam:11000010a01bdb9', 'fishing_license', 0),
(732, 'steam:11000010a01bdb9', 'taxi_license', 0),
(733, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(734, 'steam:11000010a01bdb9', 'scope', 0),
(735, 'steam:11000010a01bdb9', 'taxi_license', 0),
(736, 'steam:11000010a01bdb9', 'taxi_license', 0),
(737, 'steam:11000010a01bdb9', 'barrel', 0),
(738, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(739, 'steam:11000010a01bdb9', 'barrel', 0),
(740, 'steam:11000010a01bdb9', 'washed_stone', 0),
(741, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(742, 'steam:11000010a01bdb9', 'washed_stone', 0),
(743, 'steam:11000010a01bdb9', 'fixkit', 0),
(744, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(745, 'steam:11000010a01bdb9', 'taxi_license', 0),
(746, 'steam:11000010a01bdb9', 'scope', 0),
(747, 'steam:11000010a01bdb9', 'washed_stone', 0),
(748, 'steam:11000010a01bdb9', 'washed_stone', 0),
(749, 'steam:11000010a01bdb9', 'fixkit', 0),
(750, 'steam:11000010a01bdb9', 'washed_stone', 0),
(751, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(752, 'steam:11000010a01bdb9', 'taxi_license', 0),
(753, 'steam:11000010a01bdb9', 'petrol', 0),
(754, 'steam:11000010a01bdb9', 'limonade', 0),
(755, 'steam:11000010a01bdb9', 'metreshooter', 0),
(756, 'steam:11000010a01bdb9', 'petrol', 0),
(757, 'steam:11000010a01bdb9', 'barrel', 0),
(758, 'steam:11000010a01bdb9', 'petrol', 0),
(759, 'steam:11000010a01bdb9', 'metreshooter', 0),
(760, 'steam:11000010a01bdb9', 'taxi_license', 0),
(761, 'steam:11000010a01bdb9', 'barrel', 0),
(762, 'steam:11000010a01bdb9', 'chocolate', 0),
(763, 'steam:11000010a01bdb9', 'fixkit', 0),
(764, 'steam:11000010a01bdb9', 'barrel', 0),
(765, 'steam:11000010a01bdb9', 'metreshooter', 0),
(766, 'steam:11000010a01bdb9', 'fixkit', 0),
(767, 'steam:11000010a01bdb9', 'petrol', 0),
(768, 'steam:11000010a01bdb9', 'limonade', 0),
(769, 'steam:11000010a01bdb9', 'washed_stone', 0),
(770, 'steam:11000010a01bdb9', 'fixkit', 0),
(771, 'steam:11000010a01bdb9', 'gold', 0),
(772, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(773, 'steam:11000010a01bdb9', 'washed_stone', 0),
(774, 'steam:11000010a01bdb9', 'gold', 0),
(775, 'steam:11000010a01bdb9', 'limonade', 0),
(776, 'steam:11000010a01bdb9', 'petrol', 0),
(777, 'steam:11000010a01bdb9', 'metreshooter', 0),
(778, 'steam:11000010a01bdb9', 'gold', 0),
(779, 'steam:11000010a01bdb9', 'metreshooter', 0),
(780, 'steam:11000010a01bdb9', 'barrel', 0),
(781, 'steam:11000010a01bdb9', 'barrel', 0),
(782, 'steam:11000010a01bdb9', 'chocolate', 0),
(783, 'steam:11000010a01bdb9', 'gold', 0),
(784, 'steam:11000010a01bdb9', 'limonade', 0),
(785, 'steam:11000010a01bdb9', 'limonade', 0),
(786, 'steam:11000010a01bdb9', 'limonade', 0),
(787, 'steam:11000010a01bdb9', 'metreshooter', 0),
(788, 'steam:11000010a01bdb9', 'barrel', 0),
(789, 'steam:11000010a01bdb9', 'taxi_license', 0),
(790, 'steam:11000010a01bdb9', 'petrol', 0),
(791, 'steam:11000010a01bdb9', 'fixkit', 0),
(792, 'steam:11000010a01bdb9', 'petrol', 0),
(793, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(794, 'steam:11000010a01bdb9', 'blowpipe', 0),
(795, 'steam:11000010a01bdb9', 'drivers_license', 0),
(796, 'steam:11000010a01bdb9', 'stone', 0),
(797, 'steam:11000010a01bdb9', 'opium', 0),
(798, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(799, 'steam:11000010a01bdb9', 'copper', 0),
(800, 'steam:11000010a01bdb9', 'chocolate', 0),
(801, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(802, 'steam:11000010a01bdb9', 'limonade', 0),
(803, 'steam:11000010a01bdb9', 'blowpipe', 0),
(804, 'steam:11000010a01bdb9', 'drivers_license', 0),
(805, 'steam:11000010a01bdb9', 'chocolate', 0),
(806, 'steam:11000010a01bdb9', 'blowpipe', 0),
(807, 'steam:11000010a01bdb9', 'gold', 0),
(808, 'steam:11000010a01bdb9', 'drivers_license', 0),
(809, 'steam:11000010a01bdb9', 'gold', 0),
(810, 'steam:11000010a01bdb9', 'stone', 0),
(811, 'steam:11000010a01bdb9', 'chocolate', 0),
(812, 'steam:11000010a01bdb9', 'blowpipe', 0),
(813, 'steam:11000010a01bdb9', 'metreshooter', 0),
(814, 'steam:11000010a01bdb9', 'limonade', 0),
(815, 'steam:11000010a01bdb9', 'blowpipe', 0),
(816, 'steam:11000010a01bdb9', 'drivers_license', 0),
(817, 'steam:11000010a01bdb9', 'metreshooter', 0),
(818, 'steam:11000010a01bdb9', 'chocolate', 0),
(819, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(820, 'steam:11000010a01bdb9', 'opium', 0),
(821, 'steam:11000010a01bdb9', 'stone', 0),
(822, 'steam:11000010a01bdb9', 'stone', 0),
(823, 'steam:11000010a01bdb9', 'gold', 0),
(824, 'steam:11000010a01bdb9', 'gold', 0),
(825, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(826, 'steam:11000010a01bdb9', 'stone', 0),
(827, 'steam:11000010a01bdb9', 'chocolate', 0),
(828, 'steam:11000010a01bdb9', 'chocolate', 0),
(829, 'steam:11000010a01bdb9', 'stone', 0),
(830, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(831, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(832, 'steam:11000010a01bdb9', 'blowpipe', 0),
(833, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(834, 'steam:11000010a01bdb9', 'copper', 0),
(835, 'steam:11000010a01bdb9', 'armor', 0),
(836, 'steam:11000010a01bdb9', 'copper', 0),
(837, 'steam:11000010a01bdb9', 'opium', 0),
(838, 'steam:11000010a01bdb9', 'grip', 0),
(839, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(840, 'steam:11000010a01bdb9', 'copper', 0),
(841, 'steam:11000010a01bdb9', 'armor', 0),
(842, 'steam:11000010a01bdb9', 'copper', 0),
(843, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(844, 'steam:11000010a01bdb9', 'opium', 0),
(845, 'steam:11000010a01bdb9', 'grip', 0),
(846, 'steam:11000010a01bdb9', 'grip', 0),
(847, 'steam:11000010a01bdb9', 'copper', 0),
(848, 'steam:11000010a01bdb9', 'stone', 0),
(849, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(850, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(851, 'steam:11000010a01bdb9', 'bandage', 0),
(852, 'steam:11000010a01bdb9', 'grip', 0),
(853, 'steam:11000010a01bdb9', 'blowpipe', 0),
(854, 'steam:11000010a01bdb9', 'copper', 0),
(855, 'steam:11000010a01bdb9', 'opium', 0),
(856, 'steam:11000010a01bdb9', 'grip', 0),
(857, 'steam:11000010a01bdb9', 'stone', 0),
(858, 'steam:11000010a01bdb9', 'opium', 0),
(859, 'steam:11000010a01bdb9', 'copper', 0),
(860, 'steam:11000010a01bdb9', 'drivers_license', 0),
(861, 'steam:11000010a01bdb9', 'drivers_license', 0),
(862, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(863, 'steam:11000010a01bdb9', 'drivers_license', 0),
(864, 'steam:11000010a01bdb9', 'rhum', 0),
(865, 'steam:11000010a01bdb9', 'rhum', 0),
(866, 'steam:11000010a01bdb9', 'drivers_license', 0),
(867, 'steam:11000010a01bdb9', 'armor', 0),
(868, 'steam:11000010a01bdb9', 'opium', 0),
(869, 'steam:11000010a01bdb9', 'armor', 0),
(870, 'steam:11000010a01bdb9', 'grip', 0),
(871, 'steam:11000010a01bdb9', 'blowpipe', 0),
(872, 'steam:11000010a01bdb9', 'opium', 0),
(873, 'steam:11000010a01bdb9', 'medikit', 0),
(874, 'steam:11000010a01bdb9', 'bandage', 0),
(875, 'steam:11000010a01bdb9', 'medikit', 0),
(876, 'steam:11000010a01bdb9', 'nitro', 0),
(877, 'steam:11000010a01bdb9', 'armor', 0),
(878, 'steam:11000010a01bdb9', 'bandage', 0),
(879, 'steam:11000010a01bdb9', 'rhum', 0),
(880, 'steam:11000010a01bdb9', 'grip', 0),
(881, 'steam:11000010a01bdb9', 'hunting_license', 0),
(882, 'steam:11000010a01bdb9', 'bandage', 0),
(883, 'steam:11000010a01bdb9', 'meat', 0),
(884, 'steam:11000010a01bdb9', 'rhum', 0),
(885, 'steam:11000010a01bdb9', 'medikit', 0),
(886, 'steam:11000010a01bdb9', 'rhum', 0),
(887, 'steam:11000010a01bdb9', 'armor', 0),
(888, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(889, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(890, 'steam:11000010a01bdb9', 'armor', 0),
(891, 'steam:11000010a01bdb9', 'meat', 0),
(892, 'steam:11000010a01bdb9', 'rhum', 0),
(893, 'steam:11000010a01bdb9', 'carokit', 0),
(894, 'steam:11000010a01bdb9', 'rhum', 0),
(895, 'steam:11000010a01bdb9', 'grip', 0),
(896, 'steam:11000010a01bdb9', 'nitro', 0),
(897, 'steam:11000010a01bdb9', 'nitro', 0),
(898, 'steam:11000010a01bdb9', 'bandage', 0),
(899, 'steam:11000010a01bdb9', 'armor', 0),
(900, 'steam:11000010a01bdb9', 'nitro', 0),
(901, 'steam:11000010a01bdb9', 'carokit', 0),
(902, 'steam:11000010a01bdb9', 'bandage', 0),
(903, 'steam:11000010a01bdb9', 'fixtool', 0),
(904, 'steam:11000010a01bdb9', 'bandage', 0),
(905, 'steam:11000010a01bdb9', 'meat', 0),
(906, 'steam:11000010a01bdb9', 'rhum', 0),
(907, 'steam:11000010a01bdb9', 'hunting_license', 0),
(908, 'steam:11000010a01bdb9', 'medikit', 0),
(909, 'steam:11000010a01bdb9', 'nitro', 0),
(910, 'steam:11000010a01bdb9', 'medikit', 0),
(911, 'steam:11000010a01bdb9', 'bandage', 0),
(912, 'steam:11000010a01bdb9', 'meat', 0),
(913, 'steam:11000010a01bdb9', 'nitro', 0),
(914, 'steam:11000010a01bdb9', 'medikit', 0),
(915, 'steam:11000010a01bdb9', 'fixtool', 0),
(916, 'steam:11000010a01bdb9', 'meat', 0),
(917, 'steam:11000010a01bdb9', 'carokit', 0),
(918, 'steam:11000010a01bdb9', 'nitro', 0),
(919, 'steam:11000010a01bdb9', 'nitro', 0),
(920, 'steam:11000010a01bdb9', 'carokit', 0),
(921, 'steam:11000010a01bdb9', 'hunting_license', 0),
(922, 'steam:11000010a01bdb9', 'meat', 0),
(923, 'steam:11000010a01bdb9', 'fixtool', 0),
(924, 'steam:11000010a01bdb9', 'black_chip', 0),
(925, 'steam:11000010a01bdb9', 'meat', 0),
(926, 'steam:11000010a01bdb9', 'meat', 0),
(927, 'steam:11000010a01bdb9', 'medikit', 0),
(928, 'steam:11000010a01bdb9', 'black_chip', 0),
(929, 'steam:11000010a01bdb9', 'black_chip', 0),
(930, 'steam:11000010a01bdb9', 'hunting_license', 0),
(931, 'steam:11000010a01bdb9', 'fixtool', 0),
(932, 'steam:11000010a01bdb9', 'carokit', 0),
(933, 'steam:11000010a01bdb9', 'carokit', 0),
(934, 'steam:11000010a01bdb9', 'hunting_license', 0),
(935, 'steam:11000010a01bdb9', 'fixtool', 0),
(936, 'steam:11000010a01bdb9', 'hunting_license', 0),
(937, 'steam:11000010a01bdb9', 'black_chip', 0),
(938, 'steam:11000010a01bdb9', 'fixtool', 0),
(939, 'steam:11000010a01bdb9', 'hunting_license', 0),
(940, 'steam:11000010a01bdb9', 'hunting_license', 0),
(941, 'steam:11000010a01bdb9', 'carokit', 0),
(942, 'steam:11000010a01bdb9', 'carokit', 0),
(943, 'steam:11000010a01bdb9', 'black_chip', 0),
(944, 'steam:11000010a01bdb9', 'medikit', 0),
(945, 'steam:11000010a01bdb9', 'black_chip', 0),
(946, 'steam:11000010a01bdb9', 'black_chip', 0),
(947, 'steam:11000010a01bdb9', 'black_chip', 0),
(948, 'steam:11000010a01bdb9', 'fixtool', 0),
(949, 'steam:11000010a01bdb9', 'fixtool', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Adder', 'adder', 900000, 'super'),
('Akuma', 'AKUMA', 7500, 'motorcycles'),
('Alpha', 'alpha', 60000, 'sports'),
('Ardent', 'ardent', 1150000, 'sportsclassics'),
('Asea', 'asea', 5500, 'sedans'),
('Autarch', 'autarch', 1955000, 'super'),
('Avarus', 'avarus', 18000, 'motorcycles'),
('Bagger', 'bagger', 13500, 'motorcycles'),
('Baller', 'baller2', 40000, 'suvs'),
('Baller Sport', 'baller3', 60000, 'suvs'),
('Banshee', 'banshee', 70000, 'sports'),
('Banshee 900R', 'banshee2', 255000, 'super'),
('Bati 801', 'bati', 12000, 'motorcycles'),
('Bati 801RR', 'bati2', 19000, 'motorcycles'),
('Bestia GTS', 'bestiagts', 55000, 'sports'),
('BF400', 'bf400', 6500, 'motorcycles'),
('Bf Injection', 'bfinjection', 16000, 'offroad'),
('Bifta', 'bifta', 12000, 'offroad'),
('Bison', 'bison', 45000, 'vans'),
('Blade', 'blade', 15000, 'muscle'),
('Blazer', 'blazer', 6500, 'offroad'),
('Blazer Sport', 'blazer4', 8500, 'offroad'),
('blazer5', 'blazer5', 1755600, 'offroad'),
('Blista', 'blista', 8000, 'compacts'),
('BMX (velo)', 'bmx', 160, 'motorcycles'),
('Bobcat XL', 'bobcatxl', 32000, 'vans'),
('Brawler', 'brawler', 45000, 'offroad'),
('Brioso R/A', 'brioso', 18000, 'compacts'),
('Btype', 'btype', 62000, 'sportsclassics'),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
('Buccaneer', 'buccaneer', 18000, 'muscle'),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
('Buffalo', 'buffalo', 12000, 'sports'),
('Buffalo S', 'buffalo2', 20000, 'sports'),
('Bullet', 'bullet', 90000, 'super'),
('Burrito', 'burrito3', 19000, 'vans'),
('Camper', 'camper', 42000, 'vans'),
('Carbonizzare', 'carbonizzare', 75000, 'sports'),
('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
('Casco', 'casco', 30000, 'sportsclassics'),
('Cavalcade', 'cavalcade2', 55000, 'suvs'),
('Cheetah', 'cheetah', 375000, 'super'),
('Chimera', 'chimera', 38000, 'motorcycles'),
('Chino', 'chino', 15000, 'muscle'),
('Chino Luxe', 'chino2', 19000, 'muscle'),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
('Comet', 'comet2', 65000, 'sports'),
('Comet 5', 'comet5', 1145000, 'sports'),
('Contender', 'contender', 70000, 'suvs'),
('Coquette', 'coquette', 65000, 'sports'),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
('Cyclone', 'cyclone', 1890000, 'super'),
('Daemon', 'daemon', 11500, 'motorcycles'),
('Daemon High', 'daemon2', 13500, 'motorcycles'),
('Defiler', 'defiler', 9800, 'motorcycles'),
('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
('Dominator', 'dominator', 35000, 'muscle'),
('Double T', 'double', 28000, 'motorcycles'),
('Dubsta', 'dubsta', 45000, 'suvs'),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
('Dukes', 'dukes', 28000, 'muscle'),
('Dune Buggy', 'dune', 8000, 'offroad'),
('Elegy', 'elegy2', 38500, 'sports'),
('Emperor', 'emperor', 8500, 'sedans'),
('Enduro', 'enduro', 5500, 'motorcycles'),
('Entity XF', 'entityxf', 425000, 'super'),
('Esskey', 'esskey', 4200, 'motorcycles'),
('Exemplar', 'exemplar', 32000, 'coupes'),
('F620', 'f620', 40000, 'coupes'),
('Faction', 'faction', 20000, 'muscle'),
('Faction Rider', 'faction2', 30000, 'muscle'),
('Faction XL', 'faction3', 40000, 'muscle'),
('Faggio', 'faggio', 1900, 'motorcycles'),
('Vespa', 'faggio2', 2800, 'motorcycles'),
('Felon', 'felon', 42000, 'coupes'),
('Felon GT', 'felon2', 55000, 'coupes'),
('Feltzer', 'feltzer2', 55000, 'sports'),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
('Fixter (velo)', 'fixter', 225, 'motorcycles'),
('FMJ', 'fmj', 185000, 'super'),
('Fhantom', 'fq2', 17000, 'suvs'),
('Fugitive', 'fugitive', 12000, 'sedans'),
('Furore GT', 'furoregt', 45000, 'sports'),
('Fusilade', 'fusilade', 40000, 'sports'),
('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
('Gauntlet', 'gauntlet', 30000, 'muscle'),
('Gang Burrito', 'gburrito', 45000, 'vans'),
('Burrito', 'gburrito2', 29000, 'vans'),
('Glendale', 'glendale', 6500, 'sedans'),
('Grabger', 'granger', 50000, 'suvs'),
('Gresley', 'gresley', 47500, 'suvs'),
('GT 500', 'gt500', 785000, 'sportsclassics'),
('Guardian', 'guardian', 45000, 'offroad'),
('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
('Hermes', 'hermes', 535000, 'muscle'),
('Hexer', 'hexer', 12000, 'motorcycles'),
('Hotknife', 'hotknife', 125000, 'muscle'),
('Huntley S', 'huntley', 40000, 'suvs'),
('Hustler', 'hustler', 625000, 'muscle'),
('Infernus', 'infernus', 180000, 'super'),
('Innovation', 'innovation', 23500, 'motorcycles'),
('Intruder', 'intruder', 7500, 'sedans'),
('Issi', 'issi2', 10000, 'compacts'),
('Jackal', 'jackal', 38000, 'coupes'),
('Jester', 'jester', 65000, 'sports'),
('Jester(Racecar)', 'jester2', 135000, 'sports'),
('Journey', 'journey', 6500, 'vans'),
('Kamacho', 'kamacho', 345000, 'offroad'),
('Khamelion', 'khamelion', 38000, 'sports'),
('Kuruma', 'kuruma', 30000, 'sports'),
('Landstalker', 'landstalker', 35000, 'suvs'),
('RE-7B', 'le7b', 325000, 'super'),
('Lynx', 'lynx', 40000, 'sports'),
('Mamba', 'mamba', 70000, 'sports'),
('Manana', 'manana', 12800, 'sportsclassics'),
('Manchez', 'manchez', 5300, 'motorcycles'),
('Massacro', 'massacro', 65000, 'sports'),
('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
('Mesa', 'mesa', 16000, 'suvs'),
('Mesa Trail', 'mesa3', 40000, 'suvs'),
('Minivan', 'minivan', 13000, 'vans'),
('Monroe', 'monroe', 55000, 'sportsclassics'),
('The Liberator', 'monster', 210000, 'offroad'),
('Moonbeam', 'moonbeam', 18000, 'vans'),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
('Nemesis', 'nemesis', 5800, 'motorcycles'),
('Neon', 'neon', 1500000, 'sports'),
('Nightblade', 'nightblade', 35000, 'motorcycles'),
('Nightshade', 'nightshade', 65000, 'muscle'),
('9F', 'ninef', 65000, 'sports'),
('9F Cabrio', 'ninef2', 80000, 'sports'),
('Omnis', 'omnis', 35000, 'sports'),
('Oppressor', 'oppressor', 3524500, 'super'),
('Oracle XS', 'oracle2', 35000, 'coupes'),
('Osiris', 'osiris', 160000, 'super'),
('Panto', 'panto', 10000, 'compacts'),
('Paradise', 'paradise', 19000, 'vans'),
('Pariah', 'pariah', 1420000, 'sports'),
('Patriot', 'patriot', 55000, 'suvs'),
('PCJ-600', 'pcj', 6200, 'motorcycles'),
('Penumbra', 'penumbra', 28000, 'sports'),
('Pfister', 'pfister811', 85000, 'super'),
('Phoenix', 'phoenix', 12500, 'muscle'),
('Picador', 'picador', 18000, 'muscle'),
('Pigalle', 'pigalle', 20000, 'sportsclassics'),
('06 Tahoe', 'police8', 0, 'emergency'),
('Prairie', 'prairie', 12000, 'compacts'),
('Premier', 'premier', 8000, 'sedans'),
('Primo Custom', 'primo2', 14000, 'sedans'),
('X80 Proto', 'prototipo', 2500000, 'super'),
('Radius', 'radi', 29000, 'suvs'),
('raiden', 'raiden', 1375000, 'sports'),
('Rapid GT', 'rapidgt', 35000, 'sports'),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
('Reaper', 'reaper', 150000, 'super'),
('Rebel', 'rebel2', 35000, 'offroad'),
('Regina', 'regina', 5000, 'sedans'),
('Retinue', 'retinue', 615000, 'sportsclassics'),
('Revolter', 'revolter', 1610000, 'sports'),
('riata', 'riata', 380000, 'offroad'),
('Rocoto', 'rocoto', 45000, 'suvs'),
('Ruffian', 'ruffian', 6800, 'motorcycles'),
('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
('Rumpo', 'rumpo', 15000, 'vans'),
('Rumpo Trail', 'rumpo3', 19500, 'vans'),
('Sabre Turbo', 'sabregt', 20000, 'muscle'),
('Sabre GT', 'sabregt2', 25000, 'muscle'),
('Sanchez', 'sanchez', 5300, 'motorcycles'),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
('Sanctus', 'sanctus', 25000, 'motorcycles'),
('Sandking', 'sandking', 55000, 'offroad'),
('Savestra', 'savestra', 990000, 'sportsclassics'),
('SC 1', 'sc1', 1603000, 'super'),
('Schafter', 'schafter2', 25000, 'sedans'),
('Schafter V12', 'schafter3', 50000, 'sports'),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
('Seminole', 'seminole', 25000, 'suvs'),
('Sentinel', 'sentinel', 32000, 'coupes'),
('Sentinel XS', 'sentinel2', 40000, 'coupes'),
('Sentinel3', 'sentinel3', 650000, 'sports'),
('Seven 70', 'seven70', 39500, 'sports'),
('ETR1', 'sheava', 220000, 'super'),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
('Slam Van', 'slamvan3', 11500, 'muscle'),
('Sovereign', 'sovereign', 22000, 'motorcycles'),
('Stinger', 'stinger', 80000, 'sportsclassics'),
('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
('Streiter', 'streiter', 500000, 'sports'),
('Stretch', 'stretch', 90000, 'sedans'),
('Stromberg', 'stromberg', 3185350, 'sports'),
('Sultan', 'sultan', 15000, 'sports'),
('Sultan RS', 'sultanrs', 65000, 'super'),
('Super Diamond', 'superd', 130000, 'sedans'),
('Surano', 'surano', 50000, 'sports'),
('Surfer', 'surfer', 12000, 'vans'),
('T20', 't20', 300000, 'super'),
('Tailgater', 'tailgater', 30000, 'sedans'),
('Tampa', 'tampa', 16000, 'muscle'),
('Drift Tampa', 'tampa2', 80000, 'sports'),
('Thrust', 'thrust', 24000, 'motorcycles'),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
('Trophy Truck', 'trophytruck', 60000, 'offroad'),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
('Tropos', 'tropos', 40000, 'sports'),
('Turismo R', 'turismor', 350000, 'super'),
('Tyrus', 'tyrus', 600000, 'super'),
('Vacca', 'vacca', 120000, 'super'),
('Vader', 'vader', 7200, 'motorcycles'),
('Verlierer', 'verlierer2', 70000, 'sports'),
('Vigero', 'vigero', 12500, 'muscle'),
('Virgo', 'virgo', 14000, 'muscle'),
('Viseris', 'viseris', 875000, 'sportsclassics'),
('Visione', 'visione', 2250000, 'super'),
('Voltic', 'voltic', 90000, 'super'),
('Voltic 2', 'voltic2', 3830400, 'super'),
('Voodoo', 'voodoo', 7200, 'muscle'),
('Vortex', 'vortex', 9800, 'motorcycles'),
('Warrener', 'warrener', 4000, 'sedans'),
('Washington', 'washington', 9000, 'sedans'),
('Windsor', 'windsor', 95000, 'coupes'),
('Windsor Drop', 'windsor2', 125000, 'coupes'),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
('XLS', 'xls', 32000, 'suvs'),
('Yosemite', 'yosemite', 485000, 'muscle'),
('Youga', 'youga', 10800, 'vans'),
('Youga Luxuary', 'youga2', 14500, 'vans'),
('Z190', 'z190', 900000, 'sportsclassics'),
('Zentorno', 'zentorno', 1500000, 'super'),
('Zion', 'zion', 36000, 'coupes'),
('Zion Cabrio', 'zion2', 45000, 'coupes'),
('Zombie', 'zombiea', 9500, 'motorcycles'),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
('Z-Type', 'ztype', 220000, 'sportsclassics');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'Police'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 300),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(11, 'GunShop', 'WEAPON_STUNGUN', 50),
(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
(25, 'GunShop', 'WEAPON_GRENADE', 500),
(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
(27, 'GunShop', 'WEAPON_BZGAS', 200),
(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(31, 'GunShop', 'WEAPON_BALL', 50),
(32, 'BlackWeashop', 'WEAPON_BALL', 50),
(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist`
--

CREATE TABLE `whitelist` (
  `identifier` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interiors`
--
ALTER TABLE `interiors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_heli`
--
ALTER TABLE `user_heli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `heli_plate` (`heli_plate`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist`
--
ALTER TABLE `whitelist`
  ADD PRIMARY KEY (`identifier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `interiors`
--
ALTER TABLE `interiors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'key id', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2074;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_heli`
--
ALTER TABLE `user_heli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=950;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"devserver\",\"table\":\"items\"},{\"db\":\"essentialmode\",\"table\":\"owned_vehicles\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float UNSIGNED NOT NULL DEFAULT '0',
  `y` float UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2019-08-15 01:58:33', '{\"IgnoreMultiSubmitErrors\":true,\"Console\\/Mode\":\"collapse\",\"Server\\/hide_db\":\"\",\"Server\\/only_db\":\"\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
