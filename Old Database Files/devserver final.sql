-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2020 at 02:41 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
('caution', 'Caution', 0),
('property_black_money', 'Silver Sale Property', 0),
('society_admin', 'admin', 1),
('society_airlines', 'Airlines', 1),
('society_ambulance', 'Ambulance', 1),
('society_avocat', 'Avocat', 1),
('society_ballas', 'Ballas', 1),
('society_biker', 'Biker', 1),
('society_bishops', 'Bishops', 1),
('society_bountyhunter', 'Bountyhunter', 1),
('society_cardealer', 'Car Dealer', 1),
('society_carthief', 'Car Thief', 1),
('society_dismay', 'Dismay', 1),
('society_dock', 'Marina', 1),
('society_fire', 'fire', 1),
('society_foodtruck', 'Foodtruck', 1),
('society_gitrdone', 'GrD Construction', 1),
('society_grove', 'Grove', 1),
('society_irish', 'Irish', 1),
('society_mafia', 'Mafia', 1),
('society_mecano', 'Mechanic', 1),
('society_parking', 'Parking Enforcement', 1),
('society_police', 'Police', 1),
('society_police_black_money', 'Police Black Money', 1),
('society_realestateagent', 'Real Estae Agent', 1),
('society_rebel', 'Rebel', 1),
('society_rodriguez', 'Rodriguez', 1),
('society_taxi', 'Taxi', 1),
('society_unicorn', 'Unicorn', 1),
('society_vagos', 'Vagos', 1),
('vault_black_money', 'Money Vault', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_admin', 0, NULL),
(2, 'society_airlines', 0, NULL),
(3, 'society_ambulance', 0, NULL),
(4, 'society_avocat', 0, NULL),
(5, 'society_ballas', 0, NULL),
(6, 'society_biker', 0, NULL),
(7, 'society_bishops', 0, NULL),
(8, 'society_bountyhunter', 0, NULL),
(9, 'society_cardealer', 0, NULL),
(10, 'society_carthief', 0, NULL),
(11, 'society_dismay', 0, NULL),
(12, 'society_dock', 0, NULL),
(13, 'society_fire', 0, NULL),
(14, 'society_foodtruck', 0, NULL),
(15, 'society_gitrdone', 0, NULL),
(16, 'society_grove', 0, NULL),
(17, 'society_irish', 0, NULL),
(18, 'society_mafia', 0, NULL),
(19, 'society_mecano', 0, NULL),
(20, 'society_parking', 0, NULL),
(21, 'society_police', 0, NULL),
(22, 'society_police_black_money', 0, NULL),
(23, 'society_realestateagent', 0, NULL),
(24, 'society_rebel', 0, NULL),
(25, 'society_rodriguez', 0, NULL),
(26, 'society_taxi', 0, NULL),
(27, 'society_unicorn', 0, NULL),
(28, 'society_vagos', 0, NULL),
(29, 'vault_black_money', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_admin', 'admin', 1),
('society_airlines', 'Airlines', 1),
('society_avocat', 'Avocat', 1),
('society_ballas', 'Ballas', 1),
('society_biker', 'Biker', 1),
('society_bishops', 'Bishops', 1),
('society_bountyhunter', 'Bountyhunter', 1),
('society_cardealer', 'Car Dealer', 1),
('society_carthief', 'Car Thief', 1),
('society_citizen', 'Mafia', 1),
('society_dismay', 'Dismay', 1),
('society_dock', 'Marina', 1),
('society_fire', 'fire', 1),
('society_gitrdone', 'GrD Construction', 1),
('society_grove', 'Grove', 1),
('society_irish', 'Irish', 1),
('society_mafia', 'Mafia', 1),
('society_mecano', 'Mechanic', 1),
('society_police', 'Police', 1),
('society_rebel', 'Rebel', 1),
('society_rodriguez', 'Rodriguez', 1),
('society_taxi', 'Taxi', 1),
('society_unicorn', 'Unicorn', 1),
('society_unicorn_fridge', 'Unicorn (frigo)', 1),
('society_vagos', 'Vagos', 1),
('vault', 'Vault', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `baninfo`
--

CREATE TABLE `baninfo` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin DEFAULT 'no info',
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT 'no info',
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT 'no info',
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT '0.0.0.0',
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT 'no info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `baninfo`
--

INSERT INTO `baninfo` (`id`, `identifier`, `license`, `liveid`, `xblid`, `discord`, `playerip`, `playername`) VALUES
(1, 'steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 'live:844427762648155', 'xbl:2533274970162138', 'discord:316956589259096065', 'ip:108.39.247.113', 'stickybombz'),
(2, 'steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 'no info', 'no info', 'discord:416744619162730496', 'ip:174.23.155.211', 'K9Marine'),
(3, 'steam:1100001068ef13c', 'license:a8183dd6814d40e6469dba7693a5816a6a3e8209', 'live:914801382008973', 'xbl:2535456297922904', 'no info', 'ip:173.72.175.244', 'Soft-Hearted Devil'),
(4, 'steam:110000135837b81', 'license:a8183dd6814d40e6469dba7693a5816a6a3e8209', 'live:914801382008973', 'xbl:2535456297922904', 'discord:372129901991559169', 'ip:173.72.175.244', 'Pacotaco627'),
(5, 'steam:110000112969e8f', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 'live:985157476597128', 'xbl:2535467589236859', 'discord:197754376645902338', 'ip:136.35.33.33', 'SuperSteve902');

-- --------------------------------------------------------

--
-- Table structure for table `banlist`
--

CREATE TABLE `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `banlisthistory`
--

CREATE TABLE `banlisthistory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `target_type` varchar(50) NOT NULL,
  `target` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `businesses`
--

CREATE TABLE `businesses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(75) NOT NULL,
  `blipname` varchar(75) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `price` int(11) NOT NULL,
  `earnings` int(11) NOT NULL,
  `position` text NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `stock_price` int(11) NOT NULL DEFAULT 100,
  `employees` text NOT NULL,
  `taxrate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `irpid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(36) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ems_rank` int(11) DEFAULT -1,
  `leo_rank` int(11) DEFAULT -1,
  `tow_rank` int(11) DEFAULT -1,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `admin_rank` int(11) DEFAULT -1,
  `biker_rank` int(11) DEFAULT -1,
  `offdutyleo_rank` int(11) DEFAULT -1,
  `offdutyems_rank` int(11) DEFAULT -1,
  `fire_rank` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `irpid`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `is_dead`, `last_property`, `ems_rank`, `leo_rank`, `tow_rank`, `phone_number`, `admin_rank`, `biker_rank`, `offdutyleo_rank`, `offdutyems_rank`, `fire_rank`) VALUES
(4, 'steam:11000010a01bdb9', 'jhWk4lXACb', NULL, 0, '', NULL, 'unemployed', 0, NULL, NULL, 0, NULL, NULL, 'Tommie', 'Pickles', '1988-12-28', 'm', '175', NULL, 0, NULL, -1, -1, -1, '0', -1, -1, -1, -1, -1),
(5, 'steam:110000132580eb0', 'rLDPg3ERJi', NULL, 0, '', NULL, 'unemployed', 0, NULL, NULL, 0, NULL, NULL, 'Jak', 'Fulton', '1988-10-10', 'm', '174', NULL, 0, NULL, -1, -1, -1, '0', -1, -1, -1, -1, -1),
(6, 'steam:1100001068ef13c', 'iGY2ACfgQN', NULL, 0, '', NULL, 'unemployed', 0, NULL, NULL, 0, NULL, NULL, 'William', 'Woodard', '1989-07-27', 'm', '176', NULL, 0, NULL, -1, -1, -1, '0', -1, -1, -1, -1, -1),
(7, 'steam:110000135837b81', 'iyLruRgxtf', NULL, 0, '', NULL, 'unemployed', 0, NULL, NULL, 0, NULL, NULL, 'William', 'Woodard', '2000-11-1', 'm', '176', NULL, 0, NULL, -1, -1, -1, '0', -1, -1, -1, -1, -1),
(8, 'steam:110000112969e8f', 'RndAQADGjs', NULL, 0, '', NULL, 'unemployed', 0, NULL, NULL, 0, NULL, NULL, 'Steven', 'Super', '2003-05-08', 'm', '180', NULL, 0, NULL, -1, -1, -1, '0', -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `character_inventory`
--

CREATE TABLE `character_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `irpid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `character_inventory`
--

INSERT INTO `character_inventory` (`id`, `identifier`, `irpid`, `item`, `count`) VALUES
(190, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'lsd', 0),
(191, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'marijuana', 0),
(192, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'gazbottle', 0),
(193, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'meth_pooch', 0),
(194, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'lotteryticket', 0),
(195, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cocacola', 0),
(196, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'weed', 0),
(197, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'marriage_license', 0),
(198, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cdl', 0),
(199, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'vodkafruit', 0),
(200, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'carotool', 0),
(201, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'painkiller', 0),
(202, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cocaine', 0),
(203, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fish', 0),
(204, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'dabs', 0),
(205, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'chips', 0),
(206, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'alive_chicken', 0),
(207, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pcp', 0),
(208, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cola', 0),
(209, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'weapons_license2', 0),
(210, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'contrat', 0),
(211, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'diamond', 0),
(212, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'sportlunch', 0),
(213, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'drpepper', 0),
(214, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'binoculars', 0),
(215, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'gold', 0),
(216, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'opium', 0),
(217, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'rhumcoca', 0),
(218, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'crack', 0),
(219, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'firstaidpass', 0),
(220, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'vodka', 0),
(221, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bolcacahuetes', 0),
(222, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'ice', 0),
(223, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cigarett', 0),
(224, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'boating_license', 0),
(225, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'wrench', 0),
(226, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cheesebows', 0),
(227, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'leather', 0),
(228, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'martini', 0),
(229, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'plongee2', 0),
(230, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'golem', 0),
(231, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bolchips', 0),
(232, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'defibrillateur', 0),
(233, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'mixapero', 0),
(234, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'firstaidkit', 0),
(235, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cannabis', 0),
(236, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'armor', 0),
(237, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bolnoixcajou', 0),
(238, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'jagerbomb', 0),
(239, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'mojito', 0),
(240, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'coke_pooch', 0),
(241, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'macka', 0),
(242, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'medikit', 0),
(243, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'weapons_license1', 0),
(244, 'steam:11000010a01bdb9', 'jhWk4lXACb', '9mm_rounds', 0),
(245, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'icetea', 0),
(246, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bolpistache', 0),
(247, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'shotgun_shells', 0),
(248, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'copper', 0),
(249, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'receipt', 0),
(250, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bread', 0),
(251, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fishing_license', 0),
(252, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'saucisson', 0),
(253, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'donut', 0),
(254, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'clip', 0),
(255, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'yusuf', 0),
(256, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'hunting_license', 0),
(257, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'beer', 0),
(258, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'metreshooter', 0),
(259, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'opium_pooch', 0),
(260, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'packaged_plank', 0),
(261, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'ephedra', 0),
(262, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'teqpaf', 0),
(263, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fanta', 0),
(264, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'flashlight', 0),
(265, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'powerade', 0),
(266, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'weapons_license3', 0),
(267, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'mlic', 0),
(268, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'narcan', 0),
(269, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'clothe', 0),
(270, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'wood', 0),
(271, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'whool', 0),
(272, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'menthe', 0),
(273, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'dlic', 0),
(274, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'whiskycoca', 0),
(275, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'whisky', 0),
(276, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'weed_pooch', 0),
(277, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'whiskey', 0),
(278, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_STUNGUN', 0),
(279, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_PUMPSHOTGUN', 0),
(280, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fakepee', 0),
(281, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'licenseplate', 0),
(282, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'meth', 0),
(283, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'radio', 0),
(284, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'turtle_pooch', 0),
(285, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'lsd_pooch', 0),
(286, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_BAT', 0),
(287, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'bandage', 0),
(288, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'coke', 0),
(289, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'water', 0),
(290, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fixkit', 0),
(291, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'washed_stone', 0),
(292, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'grip', 0),
(293, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'vodkaenergy', 0),
(294, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'coca', 0),
(295, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'coffee', 0),
(296, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'vegetables', 0),
(297, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_FLASHLIGHT', 0),
(298, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'lockpick', 0),
(299, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'turtle', 0),
(300, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'scratchoff_used', 0),
(301, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'iron', 0),
(302, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'drugtest', 0),
(303, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'limonade', 0),
(304, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'taxi_license', 0),
(305, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'tacos', 0),
(306, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pastacarbonara', 0),
(307, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'sprite', 0),
(308, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'soda', 0),
(309, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'slaughtered_chicken', 0),
(310, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'lighter', 0),
(311, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'cutted_wood', 0),
(312, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'litter_pooch', 0),
(313, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'gasoline', 0),
(314, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'scratchoff', 0),
(315, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'litter', 0),
(316, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'loka', 0),
(317, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'boitier', 0),
(318, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'rhum', 0),
(319, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'jusfruit', 0),
(320, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'burger', 0),
(321, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'protein_shake', 0),
(322, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_KNIFE', 0),
(323, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'breathalyzer', 0),
(324, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'heroine', 0),
(325, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'marabou', 0),
(326, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'meat', 0),
(327, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'silencieux', 0),
(328, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'plongee1', 0),
(329, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'energy', 0),
(330, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'carokit', 0),
(331, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'ephedrine', 0),
(332, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pizza', 0),
(333, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pilot_license', 0),
(334, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pills', 0),
(335, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'petrol_raffin', 0),
(336, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'croquettes', 0),
(337, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'baconburger', 0),
(338, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'petrol', 0),
(339, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pearl_pooch', 0),
(340, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'pearl', 0),
(341, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'stone', 0),
(342, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fabric', 0),
(343, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'grapperaisin', 0),
(344, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'diving_license', 0),
(345, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'packaged_chicken', 0),
(346, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'tequila', 0),
(347, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'nitrocannister', 0),
(348, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'WEAPON_PISTOL', 0),
(349, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'rhumfruit', 0),
(350, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'fixtool', 0),
(351, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'jager', 0),
(352, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'poppy', 0),
(353, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'blowpipe', 0),
(354, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'blackberry', 0),
(355, 'steam:11000010a01bdb9', 'jhWk4lXACb', 'gym_membership', 0),
(445, 'steam:110000132580eb0', 'rLDPg3ERJi', 'petrol_raffin', 0),
(446, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cocacola', 0),
(447, 'steam:110000132580eb0', 'rLDPg3ERJi', 'mixapero', 0),
(448, 'steam:110000132580eb0', 'rLDPg3ERJi', 'whiskey', 0),
(449, 'steam:110000132580eb0', 'rLDPg3ERJi', 'silencieux', 0),
(450, 'steam:110000132580eb0', 'rLDPg3ERJi', 'weed_pooch', 0),
(451, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fish', 0),
(452, 'steam:110000132580eb0', 'rLDPg3ERJi', 'turtle', 0),
(453, 'steam:110000132580eb0', 'rLDPg3ERJi', 'firstaidpass', 0),
(454, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cutted_wood', 0),
(455, 'steam:110000132580eb0', 'rLDPg3ERJi', 'blowpipe', 0),
(456, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pearl', 0),
(457, 'steam:110000132580eb0', 'rLDPg3ERJi', 'ice', 0),
(458, 'steam:110000132580eb0', 'rLDPg3ERJi', 'meth_pooch', 0),
(459, 'steam:110000132580eb0', 'rLDPg3ERJi', 'meat', 0),
(460, 'steam:110000132580eb0', 'rLDPg3ERJi', 'vodkaenergy', 0),
(461, 'steam:110000132580eb0', 'rLDPg3ERJi', 'boitier', 0),
(462, 'steam:110000132580eb0', 'rLDPg3ERJi', 'litter', 0),
(463, 'steam:110000132580eb0', 'rLDPg3ERJi', 'whisky', 0),
(464, 'steam:110000132580eb0', 'rLDPg3ERJi', 'iron', 0),
(465, 'steam:110000132580eb0', 'rLDPg3ERJi', 'licenseplate', 0),
(466, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cigarett', 0),
(467, 'steam:110000132580eb0', 'rLDPg3ERJi', 'ephedra', 0),
(468, 'steam:110000132580eb0', 'rLDPg3ERJi', 'hunting_license', 0),
(469, 'steam:110000132580eb0', 'rLDPg3ERJi', 'medikit', 0),
(470, 'steam:110000132580eb0', 'rLDPg3ERJi', 'weapons_license2', 0),
(471, 'steam:110000132580eb0', 'rLDPg3ERJi', 'mlic', 0),
(472, 'steam:110000132580eb0', 'rLDPg3ERJi', 'boating_license', 0),
(473, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pilot_license', 0),
(474, 'steam:110000132580eb0', 'rLDPg3ERJi', 'dlic', 0),
(475, 'steam:110000132580eb0', 'rLDPg3ERJi', 'nitrocannister', 0),
(476, 'steam:110000132580eb0', 'rLDPg3ERJi', 'tacos', 0),
(477, 'steam:110000132580eb0', 'rLDPg3ERJi', 'opium_pooch', 0),
(478, 'steam:110000132580eb0', 'rLDPg3ERJi', 'firstaidkit', 0),
(479, 'steam:110000132580eb0', 'rLDPg3ERJi', 'armor', 0),
(480, 'steam:110000132580eb0', 'rLDPg3ERJi', 'gasoline', 0),
(481, 'steam:110000132580eb0', 'rLDPg3ERJi', 'vodkafruit', 0),
(482, 'steam:110000132580eb0', 'rLDPg3ERJi', 'coffee', 0),
(483, 'steam:110000132580eb0', 'rLDPg3ERJi', 'rhum', 0),
(484, 'steam:110000132580eb0', 'rLDPg3ERJi', 'chips', 0),
(485, 'steam:110000132580eb0', 'rLDPg3ERJi', 'jager', 0),
(486, 'steam:110000132580eb0', 'rLDPg3ERJi', 'rhumcoca', 0),
(487, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bolcacahuetes', 0),
(488, 'steam:110000132580eb0', 'rLDPg3ERJi', 'lockpick', 0),
(489, 'steam:110000132580eb0', 'rLDPg3ERJi', 'plongee1', 0),
(490, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cocaine', 0),
(491, 'steam:110000132580eb0', 'rLDPg3ERJi', 'burger', 0),
(492, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pearl_pooch', 0),
(493, 'steam:110000132580eb0', 'rLDPg3ERJi', 'washed_stone', 0),
(494, 'steam:110000132580eb0', 'rLDPg3ERJi', 'coke_pooch', 0),
(495, 'steam:110000132580eb0', 'rLDPg3ERJi', 'blackberry', 0),
(496, 'steam:110000132580eb0', 'rLDPg3ERJi', 'tequila', 0),
(497, 'steam:110000132580eb0', 'rLDPg3ERJi', 'energy', 0),
(498, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bread', 0),
(499, 'steam:110000132580eb0', 'rLDPg3ERJi', 'soda', 0),
(500, 'steam:110000132580eb0', 'rLDPg3ERJi', 'grip', 0),
(501, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bolnoixcajou', 0),
(502, 'steam:110000132580eb0', 'rLDPg3ERJi', 'jusfruit', 0),
(503, 'steam:110000132580eb0', 'rLDPg3ERJi', 'lsd_pooch', 0),
(504, 'steam:110000132580eb0', 'rLDPg3ERJi', 'vodka', 0),
(505, 'steam:110000132580eb0', 'rLDPg3ERJi', 'lotteryticket', 0),
(506, 'steam:110000132580eb0', 'rLDPg3ERJi', 'powerade', 0),
(507, 'steam:110000132580eb0', 'rLDPg3ERJi', 'taxi_license', 0),
(508, 'steam:110000132580eb0', 'rLDPg3ERJi', 'limonade', 0),
(509, 'steam:110000132580eb0', 'rLDPg3ERJi', 'weapons_license1', 0),
(510, 'steam:110000132580eb0', 'rLDPg3ERJi', 'metreshooter', 0),
(511, 'steam:110000132580eb0', 'rLDPg3ERJi', 'scratchoff_used', 0),
(512, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_PISTOL', 0),
(513, 'steam:110000132580eb0', 'rLDPg3ERJi', 'flashlight', 0),
(514, 'steam:110000132580eb0', 'rLDPg3ERJi', 'carokit', 0),
(515, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fishing_license', 0),
(516, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pizza', 0),
(517, 'steam:110000132580eb0', 'rLDPg3ERJi', 'packaged_chicken', 0),
(518, 'steam:110000132580eb0', 'rLDPg3ERJi', 'stone', 0),
(519, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pcp', 0),
(520, 'steam:110000132580eb0', 'rLDPg3ERJi', 'painkiller', 0),
(521, 'steam:110000132580eb0', 'rLDPg3ERJi', 'diving_license', 0),
(522, 'steam:110000132580eb0', 'rLDPg3ERJi', 'packaged_plank', 0),
(523, 'steam:110000132580eb0', 'rLDPg3ERJi', 'diamond', 0),
(524, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_FLASHLIGHT', 0),
(525, 'steam:110000132580eb0', 'rLDPg3ERJi', 'defibrillateur', 0),
(526, 'steam:110000132580eb0', 'rLDPg3ERJi', 'dabs', 0),
(527, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fixtool', 0),
(528, 'steam:110000132580eb0', 'rLDPg3ERJi', 'wrench', 0),
(529, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cola', 0),
(530, 'steam:110000132580eb0', 'rLDPg3ERJi', 'wood', 0),
(531, 'steam:110000132580eb0', 'rLDPg3ERJi', 'teqpaf', 0),
(532, 'steam:110000132580eb0', 'rLDPg3ERJi', 'rhumfruit', 0),
(533, 'steam:110000132580eb0', 'rLDPg3ERJi', 'whiskycoca', 0),
(534, 'steam:110000132580eb0', 'rLDPg3ERJi', 'carotool', 0),
(535, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cannabis', 0),
(536, 'steam:110000132580eb0', 'rLDPg3ERJi', 'martini', 0),
(537, 'steam:110000132580eb0', 'rLDPg3ERJi', 'lsd', 0),
(538, 'steam:110000132580eb0', 'rLDPg3ERJi', 'weed', 0),
(539, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_STUNGUN', 0),
(540, 'steam:110000132580eb0', 'rLDPg3ERJi', 'loka', 0),
(541, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_PUMPSHOTGUN', 0),
(542, 'steam:110000132580eb0', 'rLDPg3ERJi', 'gazbottle', 0),
(543, 'steam:110000132580eb0', 'rLDPg3ERJi', 'marijuana', 0),
(544, 'steam:110000132580eb0', 'rLDPg3ERJi', 'water', 0),
(545, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cheesebows', 0),
(546, 'steam:110000132580eb0', 'rLDPg3ERJi', 'drpepper', 0),
(547, 'steam:110000132580eb0', 'rLDPg3ERJi', 'contrat', 0),
(548, 'steam:110000132580eb0', 'rLDPg3ERJi', '9mm_rounds', 0),
(549, 'steam:110000132580eb0', 'rLDPg3ERJi', 'plongee2', 0),
(550, 'steam:110000132580eb0', 'rLDPg3ERJi', 'donut', 0),
(551, 'steam:110000132580eb0', 'rLDPg3ERJi', 'macka', 0),
(552, 'steam:110000132580eb0', 'rLDPg3ERJi', 'turtle_pooch', 0),
(553, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_BAT', 0),
(554, 'steam:110000132580eb0', 'rLDPg3ERJi', 'meth', 0),
(555, 'steam:110000132580eb0', 'rLDPg3ERJi', 'marabou', 0),
(556, 'steam:110000132580eb0', 'rLDPg3ERJi', 'icetea', 0),
(557, 'steam:110000132580eb0', 'rLDPg3ERJi', 'protein_shake', 0),
(558, 'steam:110000132580eb0', 'rLDPg3ERJi', 'vegetables', 0),
(559, 'steam:110000132580eb0', 'rLDPg3ERJi', 'whool', 0),
(560, 'steam:110000132580eb0', 'rLDPg3ERJi', 'breathalyzer', 0),
(561, 'steam:110000132580eb0', 'rLDPg3ERJi', 'receipt', 0),
(562, 'steam:110000132580eb0', 'rLDPg3ERJi', 'sportlunch', 0),
(563, 'steam:110000132580eb0', 'rLDPg3ERJi', 'scratchoff', 0),
(564, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fanta', 0),
(565, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bandage', 0),
(566, 'steam:110000132580eb0', 'rLDPg3ERJi', 'shotgun_shells', 0),
(567, 'steam:110000132580eb0', 'rLDPg3ERJi', 'grapperaisin', 0),
(568, 'steam:110000132580eb0', 'rLDPg3ERJi', 'coca', 0),
(569, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fakepee', 0),
(570, 'steam:110000132580eb0', 'rLDPg3ERJi', 'binoculars', 0),
(571, 'steam:110000132580eb0', 'rLDPg3ERJi', 'golem', 0),
(572, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bolchips', 0),
(573, 'steam:110000132580eb0', 'rLDPg3ERJi', 'saucisson', 0),
(574, 'steam:110000132580eb0', 'rLDPg3ERJi', 'baconburger', 0),
(575, 'steam:110000132580eb0', 'rLDPg3ERJi', 'litter_pooch', 0),
(576, 'steam:110000132580eb0', 'rLDPg3ERJi', 'gold', 0),
(577, 'steam:110000132580eb0', 'rLDPg3ERJi', 'sprite', 0),
(578, 'steam:110000132580eb0', 'rLDPg3ERJi', 'croquettes', 0),
(579, 'steam:110000132580eb0', 'rLDPg3ERJi', 'cdl', 0),
(580, 'steam:110000132580eb0', 'rLDPg3ERJi', 'weapons_license3', 0),
(581, 'steam:110000132580eb0', 'rLDPg3ERJi', 'heroine', 0),
(582, 'steam:110000132580eb0', 'rLDPg3ERJi', 'coke', 0),
(583, 'steam:110000132580eb0', 'rLDPg3ERJi', 'lighter', 0),
(584, 'steam:110000132580eb0', 'rLDPg3ERJi', 'copper', 0),
(585, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fixkit', 0),
(586, 'steam:110000132580eb0', 'rLDPg3ERJi', 'petrol', 0),
(587, 'steam:110000132580eb0', 'rLDPg3ERJi', 'crack', 0),
(588, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pastacarbonara', 0),
(589, 'steam:110000132580eb0', 'rLDPg3ERJi', 'drugtest', 0),
(590, 'steam:110000132580eb0', 'rLDPg3ERJi', 'clip', 0),
(591, 'steam:110000132580eb0', 'rLDPg3ERJi', 'bolpistache', 0),
(592, 'steam:110000132580eb0', 'rLDPg3ERJi', 'marriage_license', 0),
(593, 'steam:110000132580eb0', 'rLDPg3ERJi', 'poppy', 0),
(594, 'steam:110000132580eb0', 'rLDPg3ERJi', 'narcan', 0),
(595, 'steam:110000132580eb0', 'rLDPg3ERJi', 'WEAPON_KNIFE', 0),
(596, 'steam:110000132580eb0', 'rLDPg3ERJi', 'radio', 0),
(597, 'steam:110000132580eb0', 'rLDPg3ERJi', 'menthe', 0),
(598, 'steam:110000132580eb0', 'rLDPg3ERJi', 'beer', 0),
(599, 'steam:110000132580eb0', 'rLDPg3ERJi', 'fabric', 0),
(600, 'steam:110000132580eb0', 'rLDPg3ERJi', 'clothe', 0),
(601, 'steam:110000132580eb0', 'rLDPg3ERJi', 'gym_membership', 0),
(602, 'steam:110000132580eb0', 'rLDPg3ERJi', 'pills', 0),
(603, 'steam:110000132580eb0', 'rLDPg3ERJi', 'ephedrine', 0),
(604, 'steam:110000132580eb0', 'rLDPg3ERJi', 'slaughtered_chicken', 0),
(605, 'steam:110000132580eb0', 'rLDPg3ERJi', 'mojito', 0),
(606, 'steam:110000132580eb0', 'rLDPg3ERJi', 'leather', 0),
(607, 'steam:110000132580eb0', 'rLDPg3ERJi', 'yusuf', 0),
(608, 'steam:110000132580eb0', 'rLDPg3ERJi', 'opium', 0),
(609, 'steam:110000132580eb0', 'rLDPg3ERJi', 'jagerbomb', 0),
(610, 'steam:110000132580eb0', 'rLDPg3ERJi', 'alive_chicken', 0),
(700, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'diving_license', 0),
(701, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'carokit', 0),
(702, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cola', 0),
(703, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'jager', 0),
(704, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'gazbottle', 0),
(705, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'baconburger', 0),
(706, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'receipt', 0),
(707, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'painkiller', 0),
(708, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'soda', 0),
(709, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'lsd', 0),
(710, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'jagerbomb', 0),
(711, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'chips', 0),
(712, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pearl', 0),
(713, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'nitrocannister', 0),
(714, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'turtle', 0),
(715, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pearl_pooch', 0),
(716, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'loka', 0),
(717, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'leather', 0),
(718, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'meat', 0),
(719, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'iron', 0),
(720, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pilot_license', 0),
(721, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'sprite', 0),
(722, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cigarett', 0),
(723, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'armor', 0),
(724, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'turtle_pooch', 0),
(725, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'carotool', 0),
(726, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'drpepper', 0),
(727, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cdl', 0),
(728, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cocacola', 0),
(729, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'petrol', 0),
(730, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'powerade', 0),
(731, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fixkit', 0),
(732, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'breathalyzer', 0),
(733, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'martini', 0),
(734, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'beer', 0),
(735, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'litter', 0),
(736, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'packaged_plank', 0),
(737, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'clip', 0),
(738, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'water', 0),
(739, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'grapperaisin', 0),
(740, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'boating_license', 0),
(741, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'croquettes', 0),
(742, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'menthe', 0),
(743, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'opium', 0),
(744, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pastacarbonara', 0),
(745, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fishing_license', 0),
(746, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'licenseplate', 0),
(747, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_STUNGUN', 0),
(748, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fanta', 0),
(749, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'medikit', 0),
(750, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'coca', 0),
(751, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'coffee', 0),
(752, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'teqpaf', 0),
(753, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'ephedrine', 0),
(754, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'saucisson', 0),
(755, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_KNIFE', 0),
(756, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'boitier', 0),
(757, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bread', 0),
(758, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'ephedra', 0),
(759, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'blackberry', 0),
(760, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cheesebows', 0),
(761, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'icetea', 0),
(762, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'drugtest', 0),
(763, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'energy', 0),
(764, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'grip', 0),
(765, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'meth_pooch', 0),
(766, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bolnoixcajou', 0),
(767, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'vodka', 0),
(768, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'wrench', 0),
(769, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'wood', 0),
(770, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'lighter', 0),
(771, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'packaged_chicken', 0),
(772, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'metreshooter', 0),
(773, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'whool', 0),
(774, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'poppy', 0),
(775, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'whiskycoca', 0),
(776, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'defibrillateur', 0),
(777, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bolpistache', 0),
(778, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'plongee1', 0),
(779, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'whisky', 0),
(780, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'whiskey', 0),
(781, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'opium_pooch', 0),
(782, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'marijuana', 0),
(783, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'donut', 0),
(784, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pcp', 0),
(785, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pizza', 0),
(786, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_FLASHLIGHT', 0),
(787, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'marabou', 0),
(788, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'weed', 0),
(789, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_PUMPSHOTGUN', 0),
(790, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_PISTOL', 0),
(791, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'weed_pooch', 0),
(792, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'WEAPON_BAT', 0),
(793, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'scratchoff_used', 0),
(794, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'weapons_license2', 0),
(795, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'copper', 0),
(796, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'weapons_license1', 0),
(797, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'washed_stone', 0),
(798, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'vodkafruit', 0),
(799, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'vodkaenergy', 0),
(800, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'yusuf', 0),
(801, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'stone', 0),
(802, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'vegetables', 0),
(803, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'dabs', 0),
(804, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'blowpipe', 0),
(805, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'lsd_pooch', 0),
(806, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'gold', 0),
(807, 'steam:1100001068ef13c', 'iGY2ACfgQN', '9mm_rounds', 0),
(808, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'coke', 0),
(809, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'taxi_license', 0),
(810, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'mlic', 0),
(811, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'diamond', 0),
(812, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bolchips', 0),
(813, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'coke_pooch', 0),
(814, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'slaughtered_chicken', 0),
(815, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'silencieux', 0),
(816, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'ice', 0),
(817, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'clothe', 0),
(818, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'meth', 0),
(819, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'crack', 0),
(820, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'radio', 0),
(821, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'shotgun_shells', 0),
(822, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'weapons_license3', 0),
(823, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bandage', 0),
(824, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fixtool', 0),
(825, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'scratchoff', 0),
(826, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'burger', 0),
(827, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'firstaidpass', 0),
(828, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'jusfruit', 0),
(829, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'rhumcoca', 0),
(830, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'rhum', 0),
(831, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'golem', 0),
(832, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'contrat', 0),
(833, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fakepee', 0),
(834, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'macka', 0),
(835, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'rhumfruit', 0),
(836, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'protein_shake', 0),
(837, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'binoculars', 0),
(838, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'gym_membership', 0),
(839, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'pills', 0),
(840, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cutted_wood', 0),
(841, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'tacos', 0),
(842, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'sportlunch', 0),
(843, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'hunting_license', 0),
(844, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'lockpick', 0),
(845, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'petrol_raffin', 0),
(846, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'gasoline', 0),
(847, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'flashlight', 0),
(848, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fabric', 0),
(849, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'firstaidkit', 0),
(850, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'mojito', 0),
(851, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'litter_pooch', 0),
(852, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'narcan', 0),
(853, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'dlic', 0),
(854, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'mixapero', 0),
(855, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'lotteryticket', 0),
(856, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'bolcacahuetes', 0),
(857, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'marriage_license', 0),
(858, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'plongee2', 0),
(859, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cannabis', 0),
(860, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'limonade', 0),
(861, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'alive_chicken', 0),
(862, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'heroine', 0),
(863, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'fish', 0),
(864, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'cocaine', 0),
(865, 'steam:1100001068ef13c', 'iGY2ACfgQN', 'tequila', 0),
(866, 'steam:110000135837b81', 'iyLruRgxtf', 'defuser', 0),
(867, 'steam:110000135837b81', 'iyLruRgxtf', 'bread', 0),
(868, 'steam:110000135837b81', 'iyLruRgxtf', 'limonade', 0),
(869, 'steam:110000135837b81', 'iyLruRgxtf', 'scratchoff', 0),
(870, 'steam:110000135837b81', 'iyLruRgxtf', 'leather', 0),
(871, 'steam:110000135837b81', 'iyLruRgxtf', 'meat', 0),
(872, 'steam:110000135837b81', 'iyLruRgxtf', 'stone', 0),
(873, 'steam:110000135837b81', 'iyLruRgxtf', 'lockpick', 0),
(874, 'steam:110000135837b81', 'iyLruRgxtf', 'bombpart3', 0),
(875, 'steam:110000135837b81', 'iyLruRgxtf', 'ice', 0),
(876, 'steam:110000135837b81', 'iyLruRgxtf', 'pcp', 0),
(877, 'steam:110000135837b81', 'iyLruRgxtf', 'fishing_license', 0),
(878, 'steam:110000135837b81', 'iyLruRgxtf', 'flashlight', 0),
(879, 'steam:110000135837b81', 'iyLruRgxtf', 'radio', 0),
(880, 'steam:110000135837b81', 'iyLruRgxtf', 'dabs', 0),
(881, 'steam:110000135837b81', 'iyLruRgxtf', 'hunting_license', 0),
(882, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_KNIFE', 0),
(883, 'steam:110000135837b81', 'iyLruRgxtf', 'cheesebows', 0),
(884, 'steam:110000135837b81', 'iyLruRgxtf', 'blackberry', 0),
(885, 'steam:110000135837b81', 'iyLruRgxtf', 'bolchips', 0),
(886, 'steam:110000135837b81', 'iyLruRgxtf', 'croquettes', 0),
(887, 'steam:110000135837b81', 'iyLruRgxtf', 'litter_pooch', 0),
(888, 'steam:110000135837b81', 'iyLruRgxtf', 'iron', 0),
(889, 'steam:110000135837b81', 'iyLruRgxtf', 'bolcacahuetes', 0),
(890, 'steam:110000135837b81', 'iyLruRgxtf', 'boitier', 0),
(891, 'steam:110000135837b81', 'iyLruRgxtf', 'vodkaenergy', 0),
(892, 'steam:110000135837b81', 'iyLruRgxtf', 'turtle', 0),
(893, 'steam:110000135837b81', 'iyLruRgxtf', 'jagerbomb', 0),
(894, 'steam:110000135837b81', 'iyLruRgxtf', 'saucisson', 0),
(895, 'steam:110000135837b81', 'iyLruRgxtf', 'pearl', 0),
(896, 'steam:110000135837b81', 'iyLruRgxtf', 'ephedrine', 0),
(897, 'steam:110000135837b81', 'iyLruRgxtf', 'pizza', 0),
(898, 'steam:110000135837b81', 'iyLruRgxtf', 'icetea', 0),
(899, 'steam:110000135837b81', 'iyLruRgxtf', 'rhumfruit', 0),
(900, 'steam:110000135837b81', 'iyLruRgxtf', 'grip', 0),
(901, 'steam:110000135837b81', 'iyLruRgxtf', 'bolnoixcajou', 0),
(902, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_FLASHLIGHT', 0),
(903, 'steam:110000135837b81', 'iyLruRgxtf', 'contrat', 0),
(904, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_PUMPSHOTGUN', 0),
(905, 'steam:110000135837b81', 'iyLruRgxtf', 'energy', 0),
(906, 'steam:110000135837b81', 'iyLruRgxtf', 'clip', 0),
(907, 'steam:110000135837b81', 'iyLruRgxtf', 'lsd_pooch', 0),
(908, 'steam:110000135837b81', 'iyLruRgxtf', 'weapons_license1', 0),
(909, 'steam:110000135837b81', 'iyLruRgxtf', 'gold', 0),
(910, 'steam:110000135837b81', 'iyLruRgxtf', 'fanta', 0),
(911, 'steam:110000135837b81', 'iyLruRgxtf', 'grapperaisin', 0),
(912, 'steam:110000135837b81', 'iyLruRgxtf', 'heroine', 0),
(913, 'steam:110000135837b81', 'iyLruRgxtf', 'carjack', 0),
(914, 'steam:110000135837b81', 'iyLruRgxtf', 'wrench', 0),
(915, 'steam:110000135837b81', 'iyLruRgxtf', 'cola', 0),
(916, 'steam:110000135837b81', 'iyLruRgxtf', 'powerade', 0),
(917, 'steam:110000135837b81', 'iyLruRgxtf', 'coca', 0),
(918, 'steam:110000135837b81', 'iyLruRgxtf', 'coke', 0),
(919, 'steam:110000135837b81', 'iyLruRgxtf', 'lighter', 0),
(920, 'steam:110000135837b81', 'iyLruRgxtf', 'donut', 0),
(921, 'steam:110000135837b81', 'iyLruRgxtf', 'cdl', 0),
(922, 'steam:110000135837b81', 'iyLruRgxtf', 'gym_membership', 0),
(923, 'steam:110000135837b81', 'iyLruRgxtf', 'diamond', 0),
(924, 'steam:110000135837b81', 'iyLruRgxtf', 'marabou', 0),
(925, 'steam:110000135837b81', 'iyLruRgxtf', 'yusuf', 0),
(926, 'steam:110000135837b81', 'iyLruRgxtf', 'baconburger', 0),
(927, 'steam:110000135837b81', 'iyLruRgxtf', 'blowpipe', 0),
(928, 'steam:110000135837b81', 'iyLruRgxtf', 'plongee1', 0),
(929, 'steam:110000135837b81', 'iyLruRgxtf', 'jusfruit', 0),
(930, 'steam:110000135837b81', 'iyLruRgxtf', 'burger', 0),
(931, 'steam:110000135837b81', 'iyLruRgxtf', 'cigarett', 0),
(932, 'steam:110000135837b81', 'iyLruRgxtf', 'cocaine', 0),
(933, 'steam:110000135837b81', 'iyLruRgxtf', 'opium', 0),
(934, 'steam:110000135837b81', 'iyLruRgxtf', 'opium_pooch', 0),
(935, 'steam:110000135837b81', 'iyLruRgxtf', 'defibrillateur', 0),
(936, 'steam:110000135837b81', 'iyLruRgxtf', 'shotgun_shells', 0),
(937, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_BAT', 0),
(938, 'steam:110000135837b81', 'iyLruRgxtf', 'marriage_license', 0),
(939, 'steam:110000135837b81', 'iyLruRgxtf', 'boating_license', 0),
(940, 'steam:110000135837b81', 'iyLruRgxtf', 'wood', 0),
(941, 'steam:110000135837b81', 'iyLruRgxtf', 'whool', 0),
(942, 'steam:110000135837b81', 'iyLruRgxtf', 'licenseplate', 0),
(943, 'steam:110000135837b81', 'iyLruRgxtf', 'whisky', 0),
(944, 'steam:110000135837b81', 'iyLruRgxtf', 'water', 0),
(945, 'steam:110000135837b81', 'iyLruRgxtf', 'packaged_chicken', 0),
(946, 'steam:110000135837b81', 'iyLruRgxtf', 'whiskey', 0),
(947, 'steam:110000135837b81', 'iyLruRgxtf', 'fish', 0),
(948, 'steam:110000135837b81', 'iyLruRgxtf', 'petrol_raffin', 0),
(949, 'steam:110000135837b81', 'iyLruRgxtf', 'washed_stone', 0),
(950, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_STUNGUN', 0),
(951, 'steam:110000135837b81', 'iyLruRgxtf', 'meth', 0),
(952, 'steam:110000135837b81', 'iyLruRgxtf', 'weed', 0),
(953, 'steam:110000135837b81', 'iyLruRgxtf', 'fixkit', 0),
(954, 'steam:110000135837b81', 'iyLruRgxtf', '9mm_rounds', 0),
(955, 'steam:110000135837b81', 'iyLruRgxtf', 'weapons_license3', 0),
(956, 'steam:110000135837b81', 'iyLruRgxtf', 'soda', 0),
(957, 'steam:110000135837b81', 'iyLruRgxtf', 'weed_pooch', 0),
(958, 'steam:110000135837b81', 'iyLruRgxtf', 'fixtool', 0),
(959, 'steam:110000135837b81', 'iyLruRgxtf', 'ephedra', 0),
(960, 'steam:110000135837b81', 'iyLruRgxtf', 'receipt', 0),
(961, 'steam:110000135837b81', 'iyLruRgxtf', 'speccheck', 0),
(962, 'steam:110000135837b81', 'iyLruRgxtf', 'macka', 0),
(963, 'steam:110000135837b81', 'iyLruRgxtf', 'trash', 0),
(964, 'steam:110000135837b81', 'iyLruRgxtf', 'poppy', 0),
(965, 'steam:110000135837b81', 'iyLruRgxtf', 'binoculars', 0),
(966, 'steam:110000135837b81', 'iyLruRgxtf', 'vegetables', 0),
(967, 'steam:110000135837b81', 'iyLruRgxtf', 'firstaidkit', 0),
(968, 'steam:110000135837b81', 'iyLruRgxtf', 'crack', 0),
(969, 'steam:110000135837b81', 'iyLruRgxtf', 'firstaidpass', 0),
(970, 'steam:110000135837b81', 'iyLruRgxtf', 'marijuana', 0),
(971, 'steam:110000135837b81', 'iyLruRgxtf', 'turtle_pooch', 0),
(972, 'steam:110000135837b81', 'iyLruRgxtf', 'lsd', 0),
(973, 'steam:110000135837b81', 'iyLruRgxtf', 'mojito', 0),
(974, 'steam:110000135837b81', 'iyLruRgxtf', 'copper', 0),
(975, 'steam:110000135837b81', 'iyLruRgxtf', 'pills', 0),
(976, 'steam:110000135837b81', 'iyLruRgxtf', 'sprite', 0),
(977, 'steam:110000135837b81', 'iyLruRgxtf', 'sportlunch', 0),
(978, 'steam:110000135837b81', 'iyLruRgxtf', 'vodkafruit', 0),
(979, 'steam:110000135837b81', 'iyLruRgxtf', 'fabric', 0),
(980, 'steam:110000135837b81', 'iyLruRgxtf', 'weapons_license2', 0),
(981, 'steam:110000135837b81', 'iyLruRgxtf', 'packaged_plank', 0),
(982, 'steam:110000135837b81', 'iyLruRgxtf', 'gazbottle', 0),
(983, 'steam:110000135837b81', 'iyLruRgxtf', 'coffee', 0),
(984, 'steam:110000135837b81', 'iyLruRgxtf', 'slaughtered_chicken', 0),
(985, 'steam:110000135837b81', 'iyLruRgxtf', 'silencieux', 0),
(986, 'steam:110000135837b81', 'iyLruRgxtf', 'clothe', 0),
(987, 'steam:110000135837b81', 'iyLruRgxtf', 'scratchoff_used', 0),
(988, 'steam:110000135837b81', 'iyLruRgxtf', 'drpepper', 0),
(989, 'steam:110000135837b81', 'iyLruRgxtf', 'teqpaf', 0),
(990, 'steam:110000135837b81', 'iyLruRgxtf', 'rhum', 0),
(991, 'steam:110000135837b81', 'iyLruRgxtf', 'protein_shake', 0),
(992, 'steam:110000135837b81', 'iyLruRgxtf', 'bolpistache', 0),
(993, 'steam:110000135837b81', 'iyLruRgxtf', 'alive_chicken', 0),
(994, 'steam:110000135837b81', 'iyLruRgxtf', 'plongee2', 0),
(995, 'steam:110000135837b81', 'iyLruRgxtf', 'meth_pooch', 0),
(996, 'steam:110000135837b81', 'iyLruRgxtf', 'litter', 0),
(997, 'steam:110000135837b81', 'iyLruRgxtf', 'beer', 0),
(998, 'steam:110000135837b81', 'iyLruRgxtf', 'cannabis', 0),
(999, 'steam:110000135837b81', 'iyLruRgxtf', 'narcan', 0),
(1000, 'steam:110000135837b81', 'iyLruRgxtf', 'carotool', 0),
(1001, 'steam:110000135837b81', 'iyLruRgxtf', 'fakepee', 0),
(1002, 'steam:110000135837b81', 'iyLruRgxtf', 'mixapero', 0),
(1003, 'steam:110000135837b81', 'iyLruRgxtf', 'WEAPON_PISTOL', 0),
(1004, 'steam:110000135837b81', 'iyLruRgxtf', 'chips', 0),
(1005, 'steam:110000135837b81', 'iyLruRgxtf', 'cocacola', 0),
(1006, 'steam:110000135837b81', 'iyLruRgxtf', 'bombpart1', 0),
(1007, 'steam:110000135837b81', 'iyLruRgxtf', 'vodka', 0),
(1008, 'steam:110000135837b81', 'iyLruRgxtf', 'tequila', 0),
(1009, 'steam:110000135837b81', 'iyLruRgxtf', 'diving_license', 0),
(1010, 'steam:110000135837b81', 'iyLruRgxtf', 'loka', 0),
(1011, 'steam:110000135837b81', 'iyLruRgxtf', 'taxi_license', 0),
(1012, 'steam:110000135837b81', 'iyLruRgxtf', 'pilot_license', 0),
(1013, 'steam:110000135837b81', 'iyLruRgxtf', 'jager', 0),
(1014, 'steam:110000135837b81', 'iyLruRgxtf', 'mlic', 0),
(1015, 'steam:110000135837b81', 'iyLruRgxtf', 'tacos', 0),
(1016, 'steam:110000135837b81', 'iyLruRgxtf', 'petrol', 0),
(1017, 'steam:110000135837b81', 'iyLruRgxtf', 'drugtest', 0),
(1018, 'steam:110000135837b81', 'iyLruRgxtf', 'coke_pooch', 0),
(1019, 'steam:110000135837b81', 'iyLruRgxtf', 'rhumcoca', 0),
(1020, 'steam:110000135837b81', 'iyLruRgxtf', 'pastacarbonara', 0),
(1021, 'steam:110000135837b81', 'iyLruRgxtf', 'armor', 0),
(1022, 'steam:110000135837b81', 'iyLruRgxtf', 'painkiller', 0),
(1023, 'steam:110000135837b81', 'iyLruRgxtf', 'martini', 0),
(1024, 'steam:110000135837b81', 'iyLruRgxtf', 'nitrocannister', 0),
(1025, 'steam:110000135837b81', 'iyLruRgxtf', 'metreshooter', 0),
(1026, 'steam:110000135837b81', 'iyLruRgxtf', 'golem', 0),
(1027, 'steam:110000135837b81', 'iyLruRgxtf', 'whiskycoca', 0),
(1028, 'steam:110000135837b81', 'iyLruRgxtf', 'pearl_pooch', 0),
(1029, 'steam:110000135837b81', 'iyLruRgxtf', 'gasoline', 0),
(1030, 'steam:110000135837b81', 'iyLruRgxtf', 'bombpart2', 0),
(1031, 'steam:110000135837b81', 'iyLruRgxtf', 'lotteryticket', 0),
(1032, 'steam:110000135837b81', 'iyLruRgxtf', 'medikit', 0),
(1033, 'steam:110000135837b81', 'iyLruRgxtf', 'engbomb', 0),
(1034, 'steam:110000135837b81', 'iyLruRgxtf', 'menthe', 0),
(1035, 'steam:110000135837b81', 'iyLruRgxtf', 'bandage', 0),
(1036, 'steam:110000135837b81', 'iyLruRgxtf', 'carokit', 0),
(1037, 'steam:110000135837b81', 'iyLruRgxtf', 'dlic', 0),
(1038, 'steam:110000135837b81', 'iyLruRgxtf', 'breathalyzer', 0),
(1039, 'steam:110000135837b81', 'iyLruRgxtf', 'cutted_wood', 0),
(1121, 'steam:110000112969e8f', 'RndAQADGjs', 'defuser', 0),
(1122, 'steam:110000112969e8f', 'RndAQADGjs', 'bread', 0),
(1123, 'steam:110000112969e8f', 'RndAQADGjs', 'limonade', 0),
(1124, 'steam:110000112969e8f', 'RndAQADGjs', 'leather', 0),
(1125, 'steam:110000112969e8f', 'RndAQADGjs', 'scratchoff', 0),
(1126, 'steam:110000112969e8f', 'RndAQADGjs', 'meat', 0),
(1127, 'steam:110000112969e8f', 'RndAQADGjs', 'stone', 0),
(1128, 'steam:110000112969e8f', 'RndAQADGjs', 'lockpick', 0),
(1129, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart3', 0),
(1130, 'steam:110000112969e8f', 'RndAQADGjs', 'ice', 0),
(1131, 'steam:110000112969e8f', 'RndAQADGjs', 'pcp', 0),
(1132, 'steam:110000112969e8f', 'RndAQADGjs', 'fishing_license', 0),
(1133, 'steam:110000112969e8f', 'RndAQADGjs', 'flashlight', 0),
(1134, 'steam:110000112969e8f', 'RndAQADGjs', 'dabs', 0),
(1135, 'steam:110000112969e8f', 'RndAQADGjs', 'radio', 0),
(1136, 'steam:110000112969e8f', 'RndAQADGjs', 'hunting_license', 0),
(1137, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(1138, 'steam:110000112969e8f', 'RndAQADGjs', 'cheesebows', 0),
(1139, 'steam:110000112969e8f', 'RndAQADGjs', 'blackberry', 0),
(1140, 'steam:110000112969e8f', 'RndAQADGjs', 'bolchips', 0),
(1141, 'steam:110000112969e8f', 'RndAQADGjs', 'croquettes', 0),
(1142, 'steam:110000112969e8f', 'RndAQADGjs', 'litter_pooch', 0),
(1143, 'steam:110000112969e8f', 'RndAQADGjs', 'iron', 0),
(1144, 'steam:110000112969e8f', 'RndAQADGjs', 'bolcacahuetes', 0),
(1145, 'steam:110000112969e8f', 'RndAQADGjs', 'boitier', 0),
(1146, 'steam:110000112969e8f', 'RndAQADGjs', 'vodkaenergy', 0),
(1147, 'steam:110000112969e8f', 'RndAQADGjs', 'turtle', 0),
(1148, 'steam:110000112969e8f', 'RndAQADGjs', 'jagerbomb', 0),
(1149, 'steam:110000112969e8f', 'RndAQADGjs', 'saucisson', 0),
(1150, 'steam:110000112969e8f', 'RndAQADGjs', 'pearl', 0),
(1151, 'steam:110000112969e8f', 'RndAQADGjs', 'ephedrine', 0),
(1152, 'steam:110000112969e8f', 'RndAQADGjs', 'pizza', 0),
(1153, 'steam:110000112969e8f', 'RndAQADGjs', 'icetea', 0),
(1154, 'steam:110000112969e8f', 'RndAQADGjs', 'rhumfruit', 0),
(1155, 'steam:110000112969e8f', 'RndAQADGjs', 'grip', 0),
(1156, 'steam:110000112969e8f', 'RndAQADGjs', 'bolnoixcajou', 0),
(1157, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(1158, 'steam:110000112969e8f', 'RndAQADGjs', 'contrat', 0),
(1159, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(1160, 'steam:110000112969e8f', 'RndAQADGjs', 'energy', 0),
(1161, 'steam:110000112969e8f', 'RndAQADGjs', 'clip', 0),
(1162, 'steam:110000112969e8f', 'RndAQADGjs', 'lsd_pooch', 0),
(1163, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license1', 0),
(1164, 'steam:110000112969e8f', 'RndAQADGjs', 'gold', 0),
(1165, 'steam:110000112969e8f', 'RndAQADGjs', 'fanta', 0),
(1166, 'steam:110000112969e8f', 'RndAQADGjs', 'grapperaisin', 0),
(1167, 'steam:110000112969e8f', 'RndAQADGjs', 'heroine', 0),
(1168, 'steam:110000112969e8f', 'RndAQADGjs', 'carjack', 0),
(1169, 'steam:110000112969e8f', 'RndAQADGjs', 'wrench', 0),
(1170, 'steam:110000112969e8f', 'RndAQADGjs', 'cola', 0),
(1171, 'steam:110000112969e8f', 'RndAQADGjs', 'powerade', 0),
(1172, 'steam:110000112969e8f', 'RndAQADGjs', 'coca', 0),
(1173, 'steam:110000112969e8f', 'RndAQADGjs', 'coke', 0),
(1174, 'steam:110000112969e8f', 'RndAQADGjs', 'lighter', 0),
(1175, 'steam:110000112969e8f', 'RndAQADGjs', 'donut', 0),
(1176, 'steam:110000112969e8f', 'RndAQADGjs', 'cdl', 0),
(1177, 'steam:110000112969e8f', 'RndAQADGjs', 'gym_membership', 0),
(1178, 'steam:110000112969e8f', 'RndAQADGjs', 'diamond', 0),
(1179, 'steam:110000112969e8f', 'RndAQADGjs', 'marabou', 0),
(1180, 'steam:110000112969e8f', 'RndAQADGjs', 'yusuf', 0),
(1181, 'steam:110000112969e8f', 'RndAQADGjs', 'baconburger', 0),
(1182, 'steam:110000112969e8f', 'RndAQADGjs', 'blowpipe', 0),
(1183, 'steam:110000112969e8f', 'RndAQADGjs', 'plongee1', 0),
(1184, 'steam:110000112969e8f', 'RndAQADGjs', 'jusfruit', 0),
(1185, 'steam:110000112969e8f', 'RndAQADGjs', 'burger', 0),
(1186, 'steam:110000112969e8f', 'RndAQADGjs', 'cigarett', 0),
(1187, 'steam:110000112969e8f', 'RndAQADGjs', 'cocaine', 0),
(1188, 'steam:110000112969e8f', 'RndAQADGjs', 'opium', 0),
(1189, 'steam:110000112969e8f', 'RndAQADGjs', 'opium_pooch', 0),
(1190, 'steam:110000112969e8f', 'RndAQADGjs', 'defibrillateur', 0),
(1191, 'steam:110000112969e8f', 'RndAQADGjs', 'shotgun_shells', 0),
(1192, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_BAT', 0),
(1193, 'steam:110000112969e8f', 'RndAQADGjs', 'marriage_license', 0),
(1194, 'steam:110000112969e8f', 'RndAQADGjs', 'boating_license', 0),
(1195, 'steam:110000112969e8f', 'RndAQADGjs', 'wood', 0),
(1196, 'steam:110000112969e8f', 'RndAQADGjs', 'whool', 0),
(1197, 'steam:110000112969e8f', 'RndAQADGjs', 'licenseplate', 0),
(1198, 'steam:110000112969e8f', 'RndAQADGjs', 'whisky', 0),
(1199, 'steam:110000112969e8f', 'RndAQADGjs', 'water', 0),
(1200, 'steam:110000112969e8f', 'RndAQADGjs', 'packaged_chicken', 0),
(1201, 'steam:110000112969e8f', 'RndAQADGjs', 'whiskey', 0),
(1202, 'steam:110000112969e8f', 'RndAQADGjs', 'fish', 0),
(1203, 'steam:110000112969e8f', 'RndAQADGjs', 'petrol_raffin', 0),
(1204, 'steam:110000112969e8f', 'RndAQADGjs', 'washed_stone', 0),
(1205, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(1206, 'steam:110000112969e8f', 'RndAQADGjs', 'meth', 0),
(1207, 'steam:110000112969e8f', 'RndAQADGjs', 'weed', 0),
(1208, 'steam:110000112969e8f', 'RndAQADGjs', 'fixkit', 0),
(1209, 'steam:110000112969e8f', 'RndAQADGjs', '9mm_rounds', 0),
(1210, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license3', 0),
(1211, 'steam:110000112969e8f', 'RndAQADGjs', 'soda', 0),
(1212, 'steam:110000112969e8f', 'RndAQADGjs', 'weed_pooch', 0),
(1213, 'steam:110000112969e8f', 'RndAQADGjs', 'fixtool', 0),
(1214, 'steam:110000112969e8f', 'RndAQADGjs', 'ephedra', 0),
(1215, 'steam:110000112969e8f', 'RndAQADGjs', 'receipt', 0),
(1216, 'steam:110000112969e8f', 'RndAQADGjs', 'speccheck', 0),
(1217, 'steam:110000112969e8f', 'RndAQADGjs', 'macka', 0),
(1218, 'steam:110000112969e8f', 'RndAQADGjs', 'trash', 0),
(1219, 'steam:110000112969e8f', 'RndAQADGjs', 'poppy', 0),
(1220, 'steam:110000112969e8f', 'RndAQADGjs', 'binoculars', 0),
(1221, 'steam:110000112969e8f', 'RndAQADGjs', 'vegetables', 0),
(1222, 'steam:110000112969e8f', 'RndAQADGjs', 'firstaidkit', 0),
(1223, 'steam:110000112969e8f', 'RndAQADGjs', 'crack', 0),
(1224, 'steam:110000112969e8f', 'RndAQADGjs', 'firstaidpass', 0),
(1225, 'steam:110000112969e8f', 'RndAQADGjs', 'marijuana', 0),
(1226, 'steam:110000112969e8f', 'RndAQADGjs', 'turtle_pooch', 0),
(1227, 'steam:110000112969e8f', 'RndAQADGjs', 'lsd', 0),
(1228, 'steam:110000112969e8f', 'RndAQADGjs', 'mojito', 0),
(1229, 'steam:110000112969e8f', 'RndAQADGjs', 'copper', 0),
(1230, 'steam:110000112969e8f', 'RndAQADGjs', 'pills', 0),
(1231, 'steam:110000112969e8f', 'RndAQADGjs', 'sprite', 0),
(1232, 'steam:110000112969e8f', 'RndAQADGjs', 'sportlunch', 0),
(1233, 'steam:110000112969e8f', 'RndAQADGjs', 'vodkafruit', 0),
(1234, 'steam:110000112969e8f', 'RndAQADGjs', 'fabric', 0),
(1235, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license2', 0),
(1236, 'steam:110000112969e8f', 'RndAQADGjs', 'packaged_plank', 0),
(1237, 'steam:110000112969e8f', 'RndAQADGjs', 'gazbottle', 0),
(1238, 'steam:110000112969e8f', 'RndAQADGjs', 'coffee', 0),
(1239, 'steam:110000112969e8f', 'RndAQADGjs', 'slaughtered_chicken', 0),
(1240, 'steam:110000112969e8f', 'RndAQADGjs', 'silencieux', 0),
(1241, 'steam:110000112969e8f', 'RndAQADGjs', 'clothe', 0),
(1242, 'steam:110000112969e8f', 'RndAQADGjs', 'scratchoff_used', 0),
(1243, 'steam:110000112969e8f', 'RndAQADGjs', 'drpepper', 0),
(1244, 'steam:110000112969e8f', 'RndAQADGjs', 'teqpaf', 0),
(1245, 'steam:110000112969e8f', 'RndAQADGjs', 'rhum', 0),
(1246, 'steam:110000112969e8f', 'RndAQADGjs', 'protein_shake', 0),
(1247, 'steam:110000112969e8f', 'RndAQADGjs', 'bolpistache', 0),
(1248, 'steam:110000112969e8f', 'RndAQADGjs', 'alive_chicken', 0),
(1249, 'steam:110000112969e8f', 'RndAQADGjs', 'plongee2', 0),
(1250, 'steam:110000112969e8f', 'RndAQADGjs', 'meth_pooch', 0),
(1251, 'steam:110000112969e8f', 'RndAQADGjs', 'litter', 0),
(1252, 'steam:110000112969e8f', 'RndAQADGjs', 'beer', 0),
(1253, 'steam:110000112969e8f', 'RndAQADGjs', 'cannabis', 0),
(1254, 'steam:110000112969e8f', 'RndAQADGjs', 'narcan', 0),
(1255, 'steam:110000112969e8f', 'RndAQADGjs', 'carotool', 0),
(1256, 'steam:110000112969e8f', 'RndAQADGjs', 'fakepee', 0),
(1257, 'steam:110000112969e8f', 'RndAQADGjs', 'mixapero', 0),
(1258, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(1259, 'steam:110000112969e8f', 'RndAQADGjs', 'chips', 0),
(1260, 'steam:110000112969e8f', 'RndAQADGjs', 'cocacola', 0),
(1261, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart1', 0),
(1262, 'steam:110000112969e8f', 'RndAQADGjs', 'vodka', 0),
(1263, 'steam:110000112969e8f', 'RndAQADGjs', 'tequila', 0),
(1264, 'steam:110000112969e8f', 'RndAQADGjs', 'diving_license', 0),
(1265, 'steam:110000112969e8f', 'RndAQADGjs', 'loka', 0),
(1266, 'steam:110000112969e8f', 'RndAQADGjs', 'taxi_license', 0),
(1267, 'steam:110000112969e8f', 'RndAQADGjs', 'pilot_license', 0),
(1268, 'steam:110000112969e8f', 'RndAQADGjs', 'jager', 0),
(1269, 'steam:110000112969e8f', 'RndAQADGjs', 'mlic', 0),
(1270, 'steam:110000112969e8f', 'RndAQADGjs', 'tacos', 0),
(1271, 'steam:110000112969e8f', 'RndAQADGjs', 'petrol', 0),
(1272, 'steam:110000112969e8f', 'RndAQADGjs', 'drugtest', 0),
(1273, 'steam:110000112969e8f', 'RndAQADGjs', 'coke_pooch', 0),
(1274, 'steam:110000112969e8f', 'RndAQADGjs', 'rhumcoca', 0),
(1275, 'steam:110000112969e8f', 'RndAQADGjs', 'pastacarbonara', 0),
(1276, 'steam:110000112969e8f', 'RndAQADGjs', 'armor', 0),
(1277, 'steam:110000112969e8f', 'RndAQADGjs', 'painkiller', 0),
(1278, 'steam:110000112969e8f', 'RndAQADGjs', 'martini', 0),
(1279, 'steam:110000112969e8f', 'RndAQADGjs', 'nitrocannister', 0),
(1280, 'steam:110000112969e8f', 'RndAQADGjs', 'metreshooter', 0),
(1281, 'steam:110000112969e8f', 'RndAQADGjs', 'golem', 0),
(1282, 'steam:110000112969e8f', 'RndAQADGjs', 'whiskycoca', 0),
(1283, 'steam:110000112969e8f', 'RndAQADGjs', 'pearl_pooch', 0),
(1284, 'steam:110000112969e8f', 'RndAQADGjs', 'gasoline', 0);
INSERT INTO `character_inventory` (`id`, `identifier`, `irpid`, `item`, `count`) VALUES
(1285, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart2', 0),
(1286, 'steam:110000112969e8f', 'RndAQADGjs', 'lotteryticket', 0),
(1287, 'steam:110000112969e8f', 'RndAQADGjs', 'medikit', 0),
(1288, 'steam:110000112969e8f', 'RndAQADGjs', 'engbomb', 0),
(1289, 'steam:110000112969e8f', 'RndAQADGjs', 'menthe', 0),
(1290, 'steam:110000112969e8f', 'RndAQADGjs', 'bandage', 0),
(1291, 'steam:110000112969e8f', 'RndAQADGjs', 'carokit', 0),
(1292, 'steam:110000112969e8f', 'RndAQADGjs', 'dlic', 0),
(1293, 'steam:110000112969e8f', 'RndAQADGjs', 'breathalyzer', 0),
(1294, 'steam:110000112969e8f', 'RndAQADGjs', 'cutted_wood', 0);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coffees`
--

INSERT INTO `coffees` (`id`, `name`, `item`, `price`) VALUES
(1, 'Coffee', 'coffee', 30);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `name` varchar(60) NOT NULL,
  `label` varchar(100) NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_admin', 'admin', 1),
('society_avocat', 'Avocat', 1),
('society_ballas', 'Ballas', 1),
('society_biker', 'Biker', 1),
('society_bishops', 'Bishops', 1),
('society_bountyhunter', 'Bountyhunter', 1),
('society_carthief', 'Car Thief', 1),
('society_dismay', 'Dismay', 1),
('society_fire', 'fire', 1),
('society_gitrdone', 'GrD Construction', 1),
('society_grove', 'Grove', 1),
('society_irish', 'Irish', 1),
('society_mafia', 'Mafia', 1),
('society_police', 'Police', 1),
('society_rebel', 'Rebel', 1),
('society_rodriguez', 'Rodriguez', 1),
('society_unicorn', 'Unicorn', 1),
('society_vagos', 'Vagos', 1),
('user_mask', 'Masque', 0),
('vault', 'Vault', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `owner` varchar(60) DEFAULT NULL,
  `data` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_admin', NULL, '{}'),
(2, 'society_avocat', NULL, '{}'),
(3, 'society_ballas', NULL, '{}'),
(4, 'society_biker', NULL, '{}'),
(5, 'society_bishops', NULL, '{}'),
(6, 'society_bountyhunter', NULL, '{}'),
(7, 'society_carthief', NULL, '{}'),
(8, 'society_dismay', NULL, '{}'),
(9, 'society_fire', NULL, '{}'),
(10, 'society_gitrdone', NULL, '{}'),
(11, 'society_grove', NULL, '{}'),
(12, 'society_irish', NULL, '{}'),
(13, 'society_mafia', NULL, '{}'),
(14, 'society_police', NULL, '{}'),
(15, 'society_rebel', NULL, '{}'),
(16, 'society_rodriguez', NULL, '{}'),
(17, 'society_unicorn', NULL, '{}'),
(18, 'society_vagos', NULL, '{}'),
(19, 'vault', NULL, '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `dock_vehicles`
--

CREATE TABLE `dock_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3),
(8, 'Raket', 3000, 0),
(9, 'Raket', 5000, 0),
(10, 'Raket', 10000, 1),
(11, 'Raket', 20000, 1),
(12, 'Raket', 50000, 2),
(13, 'Raket', 150000, 3),
(14, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`name`, `label`, `limit`, `rare`, `can_remove`, `price`) VALUES
('9mm_rounds', '9mm Rounds', 20, 0, 1, 0),
('alive_chicken', 'Alive Chicken', -1, 0, 1, 0),
('armor', 'Armor', -1, 0, 1, 0),
('baconburger', 'Bacon Burger', -1, 0, 1, 0),
('bandage', 'Bandage', 20, 0, 1, 0),
('beer', 'Beer', 30, 0, 1, 0),
('binoculars', 'Binoculars', 1, 0, 1, 0),
('blackberry', 'blackberry', -1, 0, 1, 0),
('blowpipe', 'Blowtorch', -1, 0, 1, 0),
('boating_license', 'Boating License', -1, 0, 1, 0),
('boitier', 'Darknet', -1, 0, 1, 0),
('bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1, 0),
('bolchips', 'Bowl of Chips', 5, 0, 1, 0),
('bolnoixcajou', 'Bowl of Cashews', 5, 0, 1, 0),
('bolpistache', 'Bowl of Pistachios', 5, 0, 1, 0),
('bombpart1', 'Casing', -1, 0, 1, 0),
('bombpart2', 'Wires', -1, 0, 1, 0),
('bombpart3', 'Trigger', -1, 0, 1, 0),
('bread', 'Bread', -1, 0, 1, 0),
('breathalyzer', 'Breathalyzer', 10, 0, 1, 0),
('burger', 'Burger', 20, 0, 1, 0),
('cannabis', 'Cannabis', 50, 0, 1, 0),
('carjack', 'Car Jack', 1, 0, 1, 0),
('carokit', 'Body Kit', -1, 0, 1, 0),
('carotool', 'Tools', -1, 0, 1, 0),
('cdl', 'Commerical Drivers License', -1, 0, 1, 0),
('cheesebows', 'Cheese Doodles', -1, 0, 1, 0),
('chips', 'Chips', -1, 0, 1, 0),
('cigarett', 'Cigarette', -1, 0, 1, 0),
('clip', 'Chargeur', -1, 0, 1, 0),
('clothe', 'Clothe', -1, 0, 1, 0),
('coca', 'CocaPlant', 150, 0, 1, 0),
('cocacola', 'Coca Cola', -1, 0, 1, 0),
('cocaine', 'Coke', 50, 0, 1, 0),
('coffee', 'Coffee', -1, 0, 1, 0),
('coke', 'Coke', 0, 0, 0, 0),
('coke_pooch', 'Pouch of coke', 0, 0, 0, 0),
('cola', 'Coke', -1, 0, 1, 0),
('contrat', 'Salvage', 15, 0, 1, 0),
('copper', 'Copper', -1, 0, 1, 0),
('crack', 'Crack', 25, 0, 1, 0),
('croquettes', 'Croquettes', -1, 0, 1, 0),
('cutted_wood', 'Cut Wood', -1, 0, 1, 0),
('dabs', 'Dabs', 50, 0, 1, 0),
('defibrillateur', 'AED', 1, 0, 1, 0),
('defuser', 'Bomb Defuser', 1, 0, 1, 0),
('diamond', 'Diamond', -1, 0, 1, 0),
('diving_license', 'Diving License', -1, 0, 1, 0),
('dlic', 'Drivers License', -1, 0, 1, 0),
('donut', 'Policeman\'s Best Friend', -1, 0, 1, 0),
('drpepper', 'Dr. Pepper', 5, 0, 1, 0),
('drugtest', 'DrugTest', 10, 0, 1, 0),
('energy', 'Energy Drink', 5, 0, 1, 0),
('engbomb', 'Engine Bomb', 5, 0, 1, 0),
('ephedra', 'Ephedra', 100, 0, 1, 0),
('ephedrine', 'Ephedrine', 100, 0, 1, 0),
('fabric', 'Fabric', -1, 0, 1, 0),
('fakepee', 'Fake Pee', 5, 0, 1, 0),
('fanta', 'Fanta Exotic', -1, 0, 1, 0),
('firstaidkit', 'First Aid Kit', 1, 0, 1, 0),
('firstaidpass', 'First Aid Pass', 1, 0, 0, 0),
('fish', 'Fish', -1, 0, 1, 0),
('fishing_license', 'Fishing License', -1, 0, 1, 0),
('fixkit', 'Repair Kit', -1, 0, 1, 0),
('fixtool', 'Repair Tools', -1, 0, 1, 0),
('flashlight', 'Flashlight', -1, 0, 1, 0),
('gasoline', 'Gasoline', -1, 0, 1, 0),
('gazbottle', 'Gas Bottle', -1, 0, 1, 0),
('gold', 'Gold', -1, 0, 1, 0),
('golem', 'Golem', 5, 0, 1, 0),
('grapperaisin', 'Bunch of Grapes', 5, 0, 1, 0),
('grip', 'Grip', -1, 0, 1, 0),
('gym_membership', 'Gym Membership', -1, 0, 1, 0),
('heroine', 'Heroine', 10, 0, 1, 0),
('hunting_license', 'Hunting License', -1, 0, 1, 0),
('ice', 'Glaçon', 5, 0, 1, 0),
('icetea', 'Ice Tea', 5, 0, 1, 0),
('iron', 'Iron', -1, 0, 1, 0),
('jager', 'Jägermeister', 5, 0, 1, 0),
('jagerbomb', 'Jägerbomb', 5, 0, 1, 0),
('jusfruit', 'Fruit Juice', 5, 0, 1, 0),
('leather', 'Leather', -1, 0, 1, 0),
('licenseplate', 'License plate', -1, 0, 1, 0),
('lighter', 'Bic', -1, 0, 1, 0),
('limonade', 'Limonade', 5, 0, 1, 0),
('litter', 'Litter', -1, 0, 1, 0),
('litter_pooch', 'Pochon de LITTER', -1, 0, 1, 0),
('lockpick', 'Lockpick', -1, 0, 1, 0),
('loka', 'Loka Crush', -1, 0, 1, 0),
('lotteryticket', 'lottery ticket', -1, 0, 1, 0),
('lsd', 'Lsd', -1, 0, 1, 0),
('lsd_pooch', 'Pochon de LSD', -1, 0, 1, 0),
('macka', 'Ham sammy', -1, 0, 1, 0),
('marabou', 'Milk Chocolate ', -1, 0, 1, 0),
('marijuana', 'Marijuana', 250, 0, 1, 0),
('marriage_license', 'Marriage License', -1, 0, 1, 0),
('martini', 'Martini blanc', 5, 0, 1, 0),
('meat', 'Meat', -1, 0, 1, 0),
('medikit', 'Medikit', 100, 0, 1, 0),
('menthe', 'Mint leaf', 10, 0, 1, 0),
('meth', 'Meth', 25, 0, 1, 0),
('meth_pooch', 'Pouch of meth', 0, 0, 0, 0),
('metreshooter', 'Vodka Shooter', 3, 0, 1, 0),
('mixapero', 'Mixed Nuts', 3, 0, 1, 0),
('mlic', 'Motorcycle License', -1, 0, 1, 0),
('mojito', 'Mojito', 5, 0, 1, 0),
('narcan', 'Narcan', 10, 0, 1, 0),
('nitrocannister', 'NOS', 1, 0, 1, 0),
('opium', 'Opium', 50, 0, 1, 0),
('opium_pooch', 'Pouch of opium', 0, 0, 0, 0),
('packaged_chicken', 'Packaged Chicken', -1, 0, 1, 0),
('packaged_plank', 'Packaged Plank', -1, 0, 1, 0),
('painkiller', 'Painkiller', 10, 0, 1, 0),
('pastacarbonara', 'Pasta Carbonara', -1, 0, 1, 0),
('pcp', 'PCP', 25, 0, 1, 0),
('pearl', 'Pearl', -1, 0, 1, 0),
('pearl_pooch', 'Pochon de Pearl', -1, 0, 1, 0),
('petrol', 'Gas', -1, 0, 1, 0),
('petrol_raffin', 'Refined Oil', -1, 0, 1, 0),
('pills', 'Pills', 10, 0, 1, 0),
('pilot_license', 'Pilot License', -1, 0, 1, 0),
('pizza', 'Kebab Pizza', -1, 0, 1, 0),
('plongee1', 'Short Dive', -1, 0, 1, 0),
('plongee2', 'Long Dive', -1, 0, 1, 0),
('poppy', 'Poppy', 100, 0, 1, 0),
('powerade', 'Powerade', -1, 0, 1, 0),
('protein_shake', 'Protein Shake', -1, 0, 1, 0),
('radio', 'Radio', 1, 0, 0, 0),
('receipt', 'W.C receipt', 100, 0, 1, 0),
('rhum', 'Rhum', 5, 0, 1, 0),
('rhumcoca', 'Rhum-coca', 5, 0, 1, 0),
('rhumfruit', 'Rum with Fruit', 5, 0, 1, 0),
('saucisson', 'Sausage', 5, 0, 1, 0),
('scratchoff', 'Scratchoff Ticket', -1, 0, 1, 0),
('scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1, 0),
('shotgun_shells', 'Shotgun Shells', 20, 0, 1, 0),
('silencieux', 'Siliencer', -1, 0, 1, 0),
('slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1, 0),
('soda', 'Soda', 5, 0, 1, 0),
('speccheck', 'Spectrometer', 1, 0, 1, 0),
('sportlunch', 'Sportlunch', -1, 0, 1, 0),
('sprite', 'Sprite', -1, 0, 1, 0),
('stone', 'Stone', -1, 0, 1, 0),
('tacos', 'Tacos', 20, 0, 1, 0),
('taxi_license', 'Taxi License', -1, 0, 1, 0),
('teqpaf', 'Tequila Sunrise', 5, 0, 1, 0),
('tequila', 'Tequila', 10, 0, 1, 0),
('trash', 'Trash', -1, 0, 1, 0),
('turtle', 'Turtle', -1, 0, 1, 0),
('turtle_pooch', 'Pouch of turtle', -1, 0, 1, 0),
('vegetables', 'Vegetables', 20, 0, 1, 0),
('vodka', 'Vodka', 10, 0, 1, 0),
('vodkaenergy', 'Vodka-energy', 5, 0, 1, 0),
('vodkafruit', 'Vodka with Fruit', 5, 0, 1, 0),
('washed_stone', 'Washed Stone', -1, 0, 1, 0),
('water', 'Water', -1, 0, 1, 0),
('weapons_license1', 'Class 1 Weapons License', -1, 0, 1, 0),
('weapons_license2', 'Class 2 Weapons License', -1, 0, 1, 0),
('weapons_license3', 'Class 3 Weapons License', -1, 0, 1, 0),
('WEAPON_BAT', 'Baseball Bat', 1, 0, 1, 0),
('WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1, 0),
('WEAPON_KNIFE', 'Knife', 100, 1, 1, 0),
('WEAPON_PISTOL', 'Pistol', 100, 1, 1, 0),
('WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1, 0),
('WEAPON_STUNGUN', 'Taser', 100, 1, 1, 0),
('weed', 'Weed', 0, 0, 0, 0),
('weed_pooch', 'Pouch of weed', -1, 0, 0, 0),
('whiskey', 'Whiskey', 10, 0, 1, 0),
('whisky', 'Whisky', 5, 0, 1, 0),
('whiskycoca', 'Whisky-coca', 5, 0, 1, 0),
('whool', 'Whool', -1, 0, 1, 0),
('wood', 'Wood', -1, 0, 1, 0),
('wrench', 'Wrench', 1, 0, 1, 0),
('yusuf', 'Skin', -1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `name` varchar(50) NOT NULL,
  `label` varchar(50) DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('admin', 'Admin', 1),
('airlines', 'Airlines', 1),
('ambulance', 'AMR', 1),
('ballas', 'Ballas', 1),
('biker', 'Biker', 1),
('bishops', 'Bishops', 1),
('bountyhunter', 'Bountyhunter', 1),
('brinks', 'Brinks', 0),
('bus', 'RTD', 0),
('cardealer', 'Car Dealer', 1),
('carthief', 'Car Thief', 1),
('coastguard', 'CoastGuard', 1),
('deliverer', 'Amazon', 0),
('dismay', 'Dismay', 1),
('dispatch', 'Dispatch', 1),
('dock', 'Marina', 0),
('fire', 'LFD', 1),
('fisherman', 'Fisherman', 0),
('fuel', 'Refiner', 0),
('garbage', 'Garbage Driver', 0),
('gitrdone', 'GrD Construction', 1),
('gopostal', 'GoPostal', 0),
('grove', 'Grove Street Family', 1),
('irish', 'Irish', 1),
('lawyer', 'lawyer', 1),
('lumberjack', 'Woodcutter', 0),
('mafia', 'Mafia', 1),
('mechanic', 'Mechanic', 0),
('miner', 'Miner', 0),
('offambulance', 'Off-Duty EMS', 1),
('offpolice', 'Off-Duty LEO', 1),
('parking', 'Parking Enforcement', 1),
('pizza', 'Pizzadelivery', 0),
('police', 'LEO', 1),
('poolcleaner', 'PoolCleaner', 0),
('ranger', 'parkranger', 1),
('rebel', 'Rebel', 1),
('reporter', 'Journalist', 0),
('rodriguez', 'Rodriguez', 1),
('Salvage', 'Salvage', 1),
('security', 'Palm City Security', 0),
('slaughterer', 'Slaughterer', 0),
('taxi', 'Taxi', 0),
('textil', 'Couturier', 0),
('traffic', 'trafficofficer', 1),
('trucker', 'Eddies Trucking', 0),
('unemployed', 'Unemployed', 0),
('unicorn', 'Unicorn', 1),
('vagos', 'Vagos', 1),
('windowcleaner', 'Spick N Span', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(50) DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `label` varchar(50) NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext NOT NULL,
  `skin_female` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(0, 'police', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(1, 'lumberjack', 0, 'interim', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'interim', 'Employee', 250, '{}', '{}'),
(3, 'fuel', 0, 'interim', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'textil', 0, 'interim', 'Employee', 250, '{}', '{}'),
(6, 'miner', 0, 'interim', 'Employee', 250, '{}', '{}'),
(7, 'slaughterer', 0, 'interim', 'Employee', 250, '{}', '{}'),
(8, 'mechanic', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mechanic', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mechanic', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mechanic', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mechanic', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'pilot', 'Pilot', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'employee', 'Employee', 100, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'dispatch', 0, 'dispatcher', 'Dispatcher', 400, '{}', '{}'),
(78, 'dispatch', 1, 'boss', 'Dispatch Command', 800, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'soldato', 'Prospect', 1500, '{}', '{}'),
(164, 'biker', 1, 'capo', 'Chapter member', 1800, '{}', '{}'),
(165, 'biker', 2, 'consigliere', 'Nomad', 2100, '{}', '{}'),
(166, 'biker', 3, 'boss', 'President', 10000, '{}', '{}'),
(167, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(168, 'cardealer', 0, 'recruit', 'Recruit', 100, '{}', '{}'),
(169, 'cardealer', 1, 'novice', 'Novice', 250, '{}', '{}'),
(170, 'cardealer', 2, 'experienced', 'Experienced', 350, '{}', '{}'),
(171, 'cardealer', 3, 'boss', 'Boss', 500, '{}', '{}'),
(338, 'offpolice', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(339, 'offambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(340, 'offpolice', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(341, 'offpolice', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(342, 'offpolice', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(343, 'offpolice', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(344, 'offpolice', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(345, 'offpolice', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(346, 'offpolice', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(347, 'offpolice', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(348, 'offpolice', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(349, 'offpolice', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(350, 'offpolice', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(351, 'offpolice', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(352, 'offpolice', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(353, 'offpolice', 14, 'reserve', '(ACSO) Reserve Officer', 150, '{}', '{}'),
(354, 'offpolice', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(355, 'offpolice', 16, 'corporal', '(ACSO) Corporal', 250, '{}', '{}'),
(356, 'offpolice', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(357, 'offpolice', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(358, 'offpolice', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(359, 'offpolice', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(360, 'offpolice', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(361, 'offpolice', 22, 'chief', '(ACSO) Chief', 400, '{}', '{}'),
(362, 'offpolice', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(363, 'offpolice', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(364, 'offpolice', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(365, 'offpolice', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(366, 'offpolice', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(367, 'offpolice', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(368, 'offpolice', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(369, 'offpolice', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(370, 'offpolice', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(371, 'offpolice', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(372, 'offpolice', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(373, 'offpolice', 34, 'chief', '(CSP) Chief', 400, '{}', '{}'),
(374, 'offpolice', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(375, 'offpolice', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(376, 'offpolice', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(377, 'offambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(378, 'offambulance', 2, 'emr', 'Emergency Medical Response', 150, '{}', '{}'),
(379, 'offambulance', 3, 'emt', 'Emergency Medical Technician', 200, '{}', '{}'),
(380, 'offambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(381, 'offambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(382, 'offambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(383, 'offambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(384, 'offambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(385, 'offambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(386, 'offambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(387, 'offambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(388, 'offambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(389, 'police', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(390, 'police', 40, 'dispatch', 'Dispatch Command', 800, '{}', '{}'),
(391, 'windowcleaner', 0, 'cleaner', 'Cleaner', 100, '{}', '{}'),
(392, 'gitrdone', 0, 'laborer', 'Laborer', 60, '{}', '{}'),
(393, 'gitrdone', 1, 'operator', 'Equipment Operator', 60, '{}', '{}'),
(394, 'gitrdone', 2, 'foreman', 'Job Foreman', 85, '{}', '{}'),
(395, 'gitrdone', 3, 'boss', 'Contractor / Owner', 100, '{}', '{}'),
(396, 'parking', 0, 'meter_maid', 'Meter Maid', 1500, '{}', '{}'),
(397, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 2000, '{}', '{}'),
(398, 'parking', 2, 'boss', 'CEO', 2500, '{}', '{}'),
(399, 'dock', 0, 'recruit', 'Recrue', 10, '{}', '{}'),
(400, 'dock', 1, 'novice', 'Novice', 25, '{}', '{}'),
(401, 'dock', 2, 'experienced', 'Experimente', 40, '{}', '{}'),
(402, 'dock', 3, 'boss', 'Patron', 0, '{}', '{}'),
(403, 'police', 43, 'event', 'Event ', 0, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `nitro_vehicles`
--

CREATE TABLE `nitro_vehicles` (
  `plate` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext NOT NULL,
  `owner` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `vehicle` longtext NOT NULL,
  `owner` varchar(60) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Etat de la voiture',
  `garage_name` varchar(50) NOT NULL DEFAULT 'Garage_Centre',
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) NOT NULL DEFAULT 'voiture',
  `plate` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'car',
  `job` varchar(50) DEFAULT NULL,
  `finance` int(32) NOT NULL DEFAULT 0,
  `financetimer` int(32) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `jamstate` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(10) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(100) NOT NULL,
  `item` varchar(100) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'PDShop', 'coffee', 1),
(2, 'PDShop', 'donut', 1),
(3, 'PDShop', 'clip', 1),
(4, 'PDShop', 'armor', 1),
(5, 'PDShop', 'medikit', 0),
(6, 'CityHall', 'weapons_license1', 100),
(7, 'CityHall', 'weapons_license2', 200),
(8, 'CityHall', 'weapons_license3', 300),
(62, 'CityHall', 'hunting_license', 100),
(63, 'CityHall', 'fishing_license', 100),
(64, 'CityHall', 'diving_license', 50),
(65, 'CityHall', 'marriage_license', 5000),
(69, 'DMV', 'boating_license', 40),
(70, 'DMV', 'taxi_license', 175),
(71, 'DMV', 'pilot_license', 500),
(72, 'PDShop', 'radio', 0),
(73, 'DMV', 'licenseplate', 50);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `irpid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(36) COLLATE utf8mb4_bin NOT NULL DEFAULT '{}',
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `lastpos` varchar(255) COLLATE utf8mb4_bin DEFAULT '{706.98700000000, -964.94200000000,  31.39550000000000, 142.503463745117}',
  `DmvTest` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Required',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `irpid`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `is_dead`, `last_property`, `lastpos`, `DmvTest`, `phone_number`) VALUES
('steam:1100001068ef13c', 'iGY2ACfgQN', 'license:a8183dd6814d40e6469dba7693a5816a6a3e8209', 0, 'Soft-Hearted Devil', NULL, 'unemployed', 0, '[]', '{\"x\":1848.8,\"z\":33.7,\"y\":3671.6}', 500, 0, 'user', 'William', 'Woodard', '1989-07-27', 'm', '176', '[{\"percent\":94.325,\"val\":943250,\"name\":\"hunger\"},{\"percent\":94.325,\"val\":943250,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', 0, NULL, '{1847.6202392578, 3671.8911132813,  33.70240020752, 294.93377685547}', 'Required', '0'),
('steam:11000010a01bdb9', 'jhWk4lXACb', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 2147483647, 'stickybombz', NULL, 'mechanic', 0, '[{\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"ammo\":0},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_extended\",\"flashlight\"],\"ammo\":1018},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"ammo\":94},{\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"ammo\":84},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"ammo\":1084},{\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"ammo\":0}]', '{\"x\":253.1,\"y\":-1507.1,\"z\":29.1}', 3750, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '175', '[{\"val\":476400,\"percent\":47.64,\"name\":\"hunger\"},{\"val\":476400,\"percent\":47.64,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 0, NULL, '{253.05795288086, -1508.0120849609,  29.141916275024, 177.38539123535}', 'Required', '599-4640'),
('steam:110000112969e8f', 'RndAQADGjs', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 0, 'SuperSteve902', NULL, 'unemployed', 0, '[]', '{\"x\":252.7,\"y\":-1508.6,\"z\":29.1}', 100, 0, 'user', 'Steven', 'Super', '2003-05-08', 'm', '180', '[{\"val\":497850,\"percent\":49.785,\"name\":\"hunger\"},{\"val\":497850,\"percent\":49.785,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 0, NULL, '{252.7265625, -1508.5845947266,  29.142040252686, 348.54959106445}', 'Required', '0'),
('steam:110000132580eb0', 'rLDPg3ERJi', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 0, 'K9Marine', NULL, 'police', 36, '[{\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"label\":\"Nightstick\"},{\"ammo\":84,\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"],\"label\":\"Combat pistol\"},{\"ammo\":84,\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[\"flashlight\"],\"label\":\"Pump shotgun\"},{\"ammo\":197,\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_extended\",\"flashlight\",\"scope\"],\"label\":\"Carbine rifle\"},{\"ammo\":197,\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"label\":\"Special carbine\"},{\"ammo\":84,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"label\":\"Fire extinguisher\"},{\"ammo\":84,\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"label\":\"Taser\"},{\"ammo\":10,\"name\":\"WEAPON_SNOWBALL\",\"components\":[],\"label\":\"Snow ball\"},{\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"label\":\"Flashlight\"},{\"ammo\":0,\"name\":\"GADGET_PARACHUTE\",\"components\":[],\"label\":\"Parachute\"},{\"ammo\":25,\"name\":\"WEAPON_FLARE\",\"components\":[],\"label\":\"Flare gun\"}]', '{\"y\":3670.3,\"z\":32.9,\"x\":1960.1}', 120200, 0, 'user', 'Jak', 'Fulton', '1988-10-10', 'm', '174', '[{\"val\":811525,\"name\":\"hunger\",\"percent\":81.1525},{\"val\":811525,\"name\":\"thirst\",\"percent\":81.1525},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 0, NULL, '{2002.0523681641, 3769.5952148438,  31.840539932251, 301.5007019043}', 'Required', '101-5635'),
('steam:110000135837b81', 'iyLruRgxtf', 'license:a8183dd6814d40e6469dba7693a5816a6a3e8209', 9900, 'Pacotaco627', NULL, 'unemployed', 0, '[]', '{\"x\":298.8,\"y\":-1485.9,\"z\":29.2}', 800, 0, 'user', 'William', 'Woodard', '2000-11-1', 'm', '176', '[{\"val\":482475,\"percent\":48.2475,\"name\":\"hunger\"},{\"val\":482475,\"percent\":48.2475,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 0, NULL, '{298.82998657227, -1485.8967285156,  29.156211853027, 268.72329711914}', 'Required', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) NOT NULL,
  `name` varchar(50) NOT NULL,
  `money` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(1, 'steam:11000010a01bdb9', 'black_money', 0),
(2, 'steam:110000132580eb0', 'black_money', 0),
(3, 'steam:1100001068ef13c', 'black_money', 0),
(4, 'steam:110000135837b81', 'black_money', 0),
(5, 'steam:110000112969e8f', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `irpid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `irpid`, `item`, `count`) VALUES
(37, 'steam:11000010a01bdb9', 'RndAQADGjs', 'lsd', 0),
(38, 'steam:11000010a01bdb9', 'RndAQADGjs', 'marijuana', 0),
(39, 'steam:11000010a01bdb9', 'RndAQADGjs', 'gazbottle', 0),
(40, 'steam:11000010a01bdb9', 'RndAQADGjs', 'meth_pooch', 0),
(41, 'steam:11000010a01bdb9', 'RndAQADGjs', 'lotteryticket', 0),
(42, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cocacola', 0),
(43, 'steam:11000010a01bdb9', 'RndAQADGjs', 'weed', 0),
(44, 'steam:11000010a01bdb9', 'RndAQADGjs', 'marriage_license', 0),
(45, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cdl', 0),
(46, 'steam:11000010a01bdb9', 'RndAQADGjs', 'vodkafruit', 0),
(47, 'steam:11000010a01bdb9', 'RndAQADGjs', 'carotool', 0),
(48, 'steam:11000010a01bdb9', 'RndAQADGjs', 'painkiller', 0),
(49, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cocaine', 0),
(50, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fish', 0),
(51, 'steam:11000010a01bdb9', 'RndAQADGjs', 'dabs', 0),
(52, 'steam:11000010a01bdb9', 'RndAQADGjs', 'chips', 0),
(53, 'steam:11000010a01bdb9', 'RndAQADGjs', 'alive_chicken', 0),
(54, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pcp', 0),
(55, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cola', 0),
(56, 'steam:11000010a01bdb9', 'RndAQADGjs', 'weapons_license2', 0),
(57, 'steam:11000010a01bdb9', 'RndAQADGjs', 'contrat', 0),
(58, 'steam:11000010a01bdb9', 'RndAQADGjs', 'diamond', 0),
(59, 'steam:11000010a01bdb9', 'RndAQADGjs', 'sportlunch', 0),
(60, 'steam:11000010a01bdb9', 'RndAQADGjs', 'drpepper', 0),
(61, 'steam:11000010a01bdb9', 'RndAQADGjs', 'binoculars', 0),
(62, 'steam:11000010a01bdb9', 'RndAQADGjs', 'gold', 0),
(63, 'steam:11000010a01bdb9', 'RndAQADGjs', 'opium', 0),
(64, 'steam:11000010a01bdb9', 'RndAQADGjs', 'rhumcoca', 0),
(65, 'steam:11000010a01bdb9', 'RndAQADGjs', 'crack', 0),
(66, 'steam:11000010a01bdb9', 'RndAQADGjs', 'firstaidpass', 0),
(67, 'steam:11000010a01bdb9', 'RndAQADGjs', 'vodka', 0),
(68, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bolcacahuetes', 0),
(69, 'steam:11000010a01bdb9', 'RndAQADGjs', 'ice', 0),
(70, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cigarett', 0),
(71, 'steam:11000010a01bdb9', 'RndAQADGjs', 'boating_license', 0),
(72, 'steam:11000010a01bdb9', 'RndAQADGjs', 'wrench', 0),
(73, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cheesebows', 0),
(74, 'steam:11000010a01bdb9', 'RndAQADGjs', 'leather', 0),
(75, 'steam:11000010a01bdb9', 'RndAQADGjs', 'martini', 0),
(76, 'steam:11000010a01bdb9', 'RndAQADGjs', 'plongee2', 0),
(77, 'steam:11000010a01bdb9', 'RndAQADGjs', 'golem', 0),
(78, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bolchips', 0),
(79, 'steam:11000010a01bdb9', 'RndAQADGjs', 'defibrillateur', 0),
(80, 'steam:11000010a01bdb9', 'RndAQADGjs', 'mixapero', 0),
(81, 'steam:11000010a01bdb9', 'RndAQADGjs', 'firstaidkit', 0),
(82, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cannabis', 0),
(83, 'steam:11000010a01bdb9', 'RndAQADGjs', 'armor', 0),
(84, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bolnoixcajou', 0),
(85, 'steam:11000010a01bdb9', 'RndAQADGjs', 'jagerbomb', 0),
(86, 'steam:11000010a01bdb9', 'RndAQADGjs', 'mojito', 0),
(87, 'steam:11000010a01bdb9', 'RndAQADGjs', 'coke_pooch', 0),
(88, 'steam:11000010a01bdb9', 'RndAQADGjs', 'macka', 0),
(89, 'steam:11000010a01bdb9', 'RndAQADGjs', 'medikit', 0),
(90, 'steam:11000010a01bdb9', 'RndAQADGjs', 'weapons_license1', 0),
(91, 'steam:11000010a01bdb9', 'RndAQADGjs', '9mm_rounds', 0),
(92, 'steam:11000010a01bdb9', 'RndAQADGjs', 'icetea', 0),
(93, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bolpistache', 0),
(94, 'steam:11000010a01bdb9', 'RndAQADGjs', 'shotgun_shells', 0),
(95, 'steam:11000010a01bdb9', 'RndAQADGjs', 'copper', 0),
(96, 'steam:11000010a01bdb9', 'RndAQADGjs', 'receipt', 0),
(97, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bread', 0),
(98, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fishing_license', 0),
(99, 'steam:11000010a01bdb9', 'RndAQADGjs', 'saucisson', 0),
(100, 'steam:11000010a01bdb9', 'RndAQADGjs', 'donut', 0),
(101, 'steam:11000010a01bdb9', 'RndAQADGjs', 'clip', 0),
(102, 'steam:11000010a01bdb9', 'RndAQADGjs', 'yusuf', 0),
(103, 'steam:11000010a01bdb9', 'RndAQADGjs', 'hunting_license', 0),
(104, 'steam:11000010a01bdb9', 'RndAQADGjs', 'beer', 0),
(105, 'steam:11000010a01bdb9', 'RndAQADGjs', 'metreshooter', 0),
(106, 'steam:11000010a01bdb9', 'RndAQADGjs', 'opium_pooch', 0),
(107, 'steam:11000010a01bdb9', 'RndAQADGjs', 'packaged_plank', 0),
(108, 'steam:11000010a01bdb9', 'RndAQADGjs', 'ephedra', 0),
(109, 'steam:11000010a01bdb9', 'RndAQADGjs', 'teqpaf', 0),
(110, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fanta', 0),
(111, 'steam:11000010a01bdb9', 'RndAQADGjs', 'flashlight', 0),
(112, 'steam:11000010a01bdb9', 'RndAQADGjs', 'powerade', 0),
(113, 'steam:11000010a01bdb9', 'RndAQADGjs', 'weapons_license3', 0),
(114, 'steam:11000010a01bdb9', 'RndAQADGjs', 'mlic', 0),
(115, 'steam:11000010a01bdb9', 'RndAQADGjs', 'narcan', 0),
(116, 'steam:11000010a01bdb9', 'RndAQADGjs', 'clothe', 0),
(117, 'steam:11000010a01bdb9', 'RndAQADGjs', 'wood', 0),
(118, 'steam:11000010a01bdb9', 'RndAQADGjs', 'whool', 0),
(119, 'steam:11000010a01bdb9', 'RndAQADGjs', 'menthe', 0),
(120, 'steam:11000010a01bdb9', 'RndAQADGjs', 'dlic', 0),
(121, 'steam:11000010a01bdb9', 'RndAQADGjs', 'whiskycoca', 0),
(122, 'steam:11000010a01bdb9', 'RndAQADGjs', 'whisky', 0),
(123, 'steam:11000010a01bdb9', 'RndAQADGjs', 'weed_pooch', 0),
(124, 'steam:11000010a01bdb9', 'RndAQADGjs', 'whiskey', 0),
(125, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(126, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(127, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fakepee', 0),
(128, 'steam:11000010a01bdb9', 'RndAQADGjs', 'licenseplate', 0),
(129, 'steam:11000010a01bdb9', 'RndAQADGjs', 'meth', 0),
(130, 'steam:11000010a01bdb9', 'RndAQADGjs', 'radio', 0),
(131, 'steam:11000010a01bdb9', 'RndAQADGjs', 'turtle_pooch', 0),
(132, 'steam:11000010a01bdb9', 'RndAQADGjs', 'lsd_pooch', 0),
(133, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_BAT', 0),
(134, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bandage', 0),
(135, 'steam:11000010a01bdb9', 'RndAQADGjs', 'coke', 0),
(136, 'steam:11000010a01bdb9', 'RndAQADGjs', 'water', 0),
(137, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fixkit', 0),
(138, 'steam:11000010a01bdb9', 'RndAQADGjs', 'washed_stone', 0),
(139, 'steam:11000010a01bdb9', 'RndAQADGjs', 'grip', 0),
(140, 'steam:11000010a01bdb9', 'RndAQADGjs', 'vodkaenergy', 0),
(141, 'steam:11000010a01bdb9', 'RndAQADGjs', 'coca', 0),
(142, 'steam:11000010a01bdb9', 'RndAQADGjs', 'coffee', 0),
(143, 'steam:11000010a01bdb9', 'RndAQADGjs', 'vegetables', 0),
(144, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(145, 'steam:11000010a01bdb9', 'RndAQADGjs', 'lockpick', 0),
(146, 'steam:11000010a01bdb9', 'RndAQADGjs', 'turtle', 0),
(147, 'steam:11000010a01bdb9', 'RndAQADGjs', 'scratchoff_used', 0),
(148, 'steam:11000010a01bdb9', 'RndAQADGjs', 'iron', 0),
(149, 'steam:11000010a01bdb9', 'RndAQADGjs', 'drugtest', 0),
(150, 'steam:11000010a01bdb9', 'RndAQADGjs', 'limonade', 0),
(151, 'steam:11000010a01bdb9', 'RndAQADGjs', 'taxi_license', 0),
(152, 'steam:11000010a01bdb9', 'RndAQADGjs', 'tacos', 0),
(153, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pastacarbonara', 0),
(154, 'steam:11000010a01bdb9', 'RndAQADGjs', 'sprite', 0),
(155, 'steam:11000010a01bdb9', 'RndAQADGjs', 'soda', 0),
(156, 'steam:11000010a01bdb9', 'RndAQADGjs', 'slaughtered_chicken', 0),
(157, 'steam:11000010a01bdb9', 'RndAQADGjs', 'lighter', 0),
(158, 'steam:11000010a01bdb9', 'RndAQADGjs', 'cutted_wood', 0),
(159, 'steam:11000010a01bdb9', 'RndAQADGjs', 'litter_pooch', 0),
(160, 'steam:11000010a01bdb9', 'RndAQADGjs', 'gasoline', 0),
(161, 'steam:11000010a01bdb9', 'RndAQADGjs', 'scratchoff', 0),
(162, 'steam:11000010a01bdb9', 'RndAQADGjs', 'litter', 0),
(163, 'steam:11000010a01bdb9', 'RndAQADGjs', 'loka', 0),
(164, 'steam:11000010a01bdb9', 'RndAQADGjs', 'boitier', 0),
(165, 'steam:11000010a01bdb9', 'RndAQADGjs', 'rhum', 0),
(166, 'steam:11000010a01bdb9', 'RndAQADGjs', 'jusfruit', 0),
(167, 'steam:11000010a01bdb9', 'RndAQADGjs', 'burger', 0),
(168, 'steam:11000010a01bdb9', 'RndAQADGjs', 'protein_shake', 0),
(169, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(170, 'steam:11000010a01bdb9', 'RndAQADGjs', 'breathalyzer', 0),
(171, 'steam:11000010a01bdb9', 'RndAQADGjs', 'heroine', 0),
(172, 'steam:11000010a01bdb9', 'RndAQADGjs', 'marabou', 0),
(173, 'steam:11000010a01bdb9', 'RndAQADGjs', 'meat', 0),
(174, 'steam:11000010a01bdb9', 'RndAQADGjs', 'silencieux', 0),
(175, 'steam:11000010a01bdb9', 'RndAQADGjs', 'plongee1', 0),
(176, 'steam:11000010a01bdb9', 'RndAQADGjs', 'energy', 0),
(177, 'steam:11000010a01bdb9', 'RndAQADGjs', 'carokit', 0),
(178, 'steam:11000010a01bdb9', 'RndAQADGjs', 'ephedrine', 0),
(179, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pizza', 0),
(180, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pilot_license', 0),
(181, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pills', 0),
(182, 'steam:11000010a01bdb9', 'RndAQADGjs', 'petrol_raffin', 0),
(183, 'steam:11000010a01bdb9', 'RndAQADGjs', 'croquettes', 0),
(184, 'steam:11000010a01bdb9', 'RndAQADGjs', 'baconburger', 0),
(185, 'steam:11000010a01bdb9', 'RndAQADGjs', 'petrol', 0),
(186, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pearl_pooch', 0),
(187, 'steam:11000010a01bdb9', 'RndAQADGjs', 'pearl', 0),
(188, 'steam:11000010a01bdb9', 'RndAQADGjs', 'stone', 0),
(189, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fabric', 0),
(190, 'steam:11000010a01bdb9', 'RndAQADGjs', 'grapperaisin', 0),
(191, 'steam:11000010a01bdb9', 'RndAQADGjs', 'diving_license', 0),
(192, 'steam:11000010a01bdb9', 'RndAQADGjs', 'packaged_chicken', 0),
(193, 'steam:11000010a01bdb9', 'RndAQADGjs', 'tequila', 0),
(194, 'steam:11000010a01bdb9', 'RndAQADGjs', 'nitrocannister', 0),
(195, 'steam:11000010a01bdb9', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(196, 'steam:11000010a01bdb9', 'RndAQADGjs', 'rhumfruit', 0),
(197, 'steam:11000010a01bdb9', 'RndAQADGjs', 'fixtool', 0),
(198, 'steam:11000010a01bdb9', 'RndAQADGjs', 'jager', 0),
(199, 'steam:11000010a01bdb9', 'RndAQADGjs', 'poppy', 0),
(200, 'steam:11000010a01bdb9', 'RndAQADGjs', 'blowpipe', 0),
(201, 'steam:11000010a01bdb9', 'RndAQADGjs', 'blackberry', 0),
(202, 'steam:11000010a01bdb9', 'RndAQADGjs', 'gym_membership', 0),
(203, 'steam:110000132580eb0', 'RndAQADGjs', 'petrol_raffin', 0),
(204, 'steam:110000132580eb0', 'RndAQADGjs', 'cocacola', 0),
(205, 'steam:110000132580eb0', 'RndAQADGjs', 'mixapero', 0),
(206, 'steam:110000132580eb0', 'RndAQADGjs', 'whiskey', 0),
(207, 'steam:110000132580eb0', 'RndAQADGjs', 'silencieux', 0),
(208, 'steam:110000132580eb0', 'RndAQADGjs', 'weed_pooch', 0),
(209, 'steam:110000132580eb0', 'RndAQADGjs', 'fish', 0),
(210, 'steam:110000132580eb0', 'RndAQADGjs', 'turtle', 0),
(211, 'steam:110000132580eb0', 'RndAQADGjs', 'firstaidpass', 0),
(212, 'steam:110000132580eb0', 'RndAQADGjs', 'cutted_wood', 0),
(213, 'steam:110000132580eb0', 'RndAQADGjs', 'blowpipe', 0),
(214, 'steam:110000132580eb0', 'RndAQADGjs', 'pearl', 0),
(215, 'steam:110000132580eb0', 'RndAQADGjs', 'ice', 0),
(216, 'steam:110000132580eb0', 'RndAQADGjs', 'meth_pooch', 0),
(217, 'steam:110000132580eb0', 'RndAQADGjs', 'meat', 0),
(218, 'steam:110000132580eb0', 'RndAQADGjs', 'vodkaenergy', 0),
(219, 'steam:110000132580eb0', 'RndAQADGjs', 'boitier', 0),
(220, 'steam:110000132580eb0', 'RndAQADGjs', 'litter', 0),
(221, 'steam:110000132580eb0', 'RndAQADGjs', 'whisky', 0),
(222, 'steam:110000132580eb0', 'RndAQADGjs', 'iron', 0),
(223, 'steam:110000132580eb0', 'RndAQADGjs', 'licenseplate', 0),
(224, 'steam:110000132580eb0', 'RndAQADGjs', 'cigarett', 0),
(225, 'steam:110000132580eb0', 'RndAQADGjs', 'ephedra', 0),
(226, 'steam:110000132580eb0', 'RndAQADGjs', 'hunting_license', 0),
(227, 'steam:110000132580eb0', 'RndAQADGjs', 'medikit', 0),
(228, 'steam:110000132580eb0', 'RndAQADGjs', 'weapons_license2', 0),
(229, 'steam:110000132580eb0', 'RndAQADGjs', 'mlic', 0),
(230, 'steam:110000132580eb0', 'RndAQADGjs', 'boating_license', 0),
(231, 'steam:110000132580eb0', 'RndAQADGjs', 'pilot_license', 0),
(232, 'steam:110000132580eb0', 'RndAQADGjs', 'dlic', 0),
(233, 'steam:110000132580eb0', 'RndAQADGjs', 'nitrocannister', 0),
(234, 'steam:110000132580eb0', 'RndAQADGjs', 'tacos', 0),
(235, 'steam:110000132580eb0', 'RndAQADGjs', 'opium_pooch', 0),
(236, 'steam:110000132580eb0', 'RndAQADGjs', 'firstaidkit', 0),
(237, 'steam:110000132580eb0', 'RndAQADGjs', 'armor', 0),
(238, 'steam:110000132580eb0', 'RndAQADGjs', 'gasoline', 0),
(239, 'steam:110000132580eb0', 'RndAQADGjs', 'vodkafruit', 0),
(240, 'steam:110000132580eb0', 'RndAQADGjs', 'coffee', 0),
(241, 'steam:110000132580eb0', 'RndAQADGjs', 'rhum', 0),
(242, 'steam:110000132580eb0', 'RndAQADGjs', 'chips', 0),
(243, 'steam:110000132580eb0', 'RndAQADGjs', 'jager', 0),
(244, 'steam:110000132580eb0', 'RndAQADGjs', 'rhumcoca', 0),
(245, 'steam:110000132580eb0', 'RndAQADGjs', 'bolcacahuetes', 0),
(246, 'steam:110000132580eb0', 'RndAQADGjs', 'lockpick', 0),
(247, 'steam:110000132580eb0', 'RndAQADGjs', 'plongee1', 0),
(248, 'steam:110000132580eb0', 'RndAQADGjs', 'cocaine', 0),
(249, 'steam:110000132580eb0', 'RndAQADGjs', 'burger', 0),
(250, 'steam:110000132580eb0', 'RndAQADGjs', 'pearl_pooch', 0),
(251, 'steam:110000132580eb0', 'RndAQADGjs', 'washed_stone', 0),
(252, 'steam:110000132580eb0', 'RndAQADGjs', 'coke_pooch', 0),
(253, 'steam:110000132580eb0', 'RndAQADGjs', 'blackberry', 0),
(254, 'steam:110000132580eb0', 'RndAQADGjs', 'tequila', 0),
(255, 'steam:110000132580eb0', 'RndAQADGjs', 'energy', 0),
(256, 'steam:110000132580eb0', 'RndAQADGjs', 'bread', 0),
(257, 'steam:110000132580eb0', 'RndAQADGjs', 'soda', 0),
(258, 'steam:110000132580eb0', 'RndAQADGjs', 'grip', 0),
(259, 'steam:110000132580eb0', 'RndAQADGjs', 'bolnoixcajou', 0),
(260, 'steam:110000132580eb0', 'RndAQADGjs', 'jusfruit', 0),
(261, 'steam:110000132580eb0', 'RndAQADGjs', 'lsd_pooch', 0),
(262, 'steam:110000132580eb0', 'RndAQADGjs', 'vodka', 0),
(263, 'steam:110000132580eb0', 'RndAQADGjs', 'lotteryticket', 0),
(264, 'steam:110000132580eb0', 'RndAQADGjs', 'powerade', 0),
(265, 'steam:110000132580eb0', 'RndAQADGjs', 'taxi_license', 0),
(266, 'steam:110000132580eb0', 'RndAQADGjs', 'limonade', 0),
(267, 'steam:110000132580eb0', 'RndAQADGjs', 'weapons_license1', 0),
(268, 'steam:110000132580eb0', 'RndAQADGjs', 'metreshooter', 0),
(269, 'steam:110000132580eb0', 'RndAQADGjs', 'scratchoff_used', 0),
(270, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(271, 'steam:110000132580eb0', 'RndAQADGjs', 'flashlight', 0),
(272, 'steam:110000132580eb0', 'RndAQADGjs', 'carokit', 0),
(273, 'steam:110000132580eb0', 'RndAQADGjs', 'fishing_license', 0),
(274, 'steam:110000132580eb0', 'RndAQADGjs', 'pizza', 0),
(275, 'steam:110000132580eb0', 'RndAQADGjs', 'packaged_chicken', 0),
(276, 'steam:110000132580eb0', 'RndAQADGjs', 'stone', 0),
(277, 'steam:110000132580eb0', 'RndAQADGjs', 'pcp', 0),
(278, 'steam:110000132580eb0', 'RndAQADGjs', 'painkiller', 0),
(279, 'steam:110000132580eb0', 'RndAQADGjs', 'diving_license', 0),
(280, 'steam:110000132580eb0', 'RndAQADGjs', 'packaged_plank', 0),
(281, 'steam:110000132580eb0', 'RndAQADGjs', 'diamond', 0),
(282, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(283, 'steam:110000132580eb0', 'RndAQADGjs', 'defibrillateur', 0),
(284, 'steam:110000132580eb0', 'RndAQADGjs', 'dabs', 0),
(285, 'steam:110000132580eb0', 'RndAQADGjs', 'fixtool', 0),
(286, 'steam:110000132580eb0', 'RndAQADGjs', 'wrench', 0),
(287, 'steam:110000132580eb0', 'RndAQADGjs', 'cola', 0),
(288, 'steam:110000132580eb0', 'RndAQADGjs', 'wood', 0),
(289, 'steam:110000132580eb0', 'RndAQADGjs', 'teqpaf', 0),
(290, 'steam:110000132580eb0', 'RndAQADGjs', 'rhumfruit', 0),
(291, 'steam:110000132580eb0', 'RndAQADGjs', 'whiskycoca', 0),
(292, 'steam:110000132580eb0', 'RndAQADGjs', 'carotool', 0),
(293, 'steam:110000132580eb0', 'RndAQADGjs', 'cannabis', 0),
(294, 'steam:110000132580eb0', 'RndAQADGjs', 'martini', 0),
(295, 'steam:110000132580eb0', 'RndAQADGjs', 'lsd', 0),
(296, 'steam:110000132580eb0', 'RndAQADGjs', 'weed', 0),
(297, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(298, 'steam:110000132580eb0', 'RndAQADGjs', 'loka', 0),
(299, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(300, 'steam:110000132580eb0', 'RndAQADGjs', 'gazbottle', 0),
(301, 'steam:110000132580eb0', 'RndAQADGjs', 'marijuana', 0),
(302, 'steam:110000132580eb0', 'RndAQADGjs', 'water', 0),
(303, 'steam:110000132580eb0', 'RndAQADGjs', 'cheesebows', 0),
(304, 'steam:110000132580eb0', 'RndAQADGjs', 'drpepper', 0),
(305, 'steam:110000132580eb0', 'RndAQADGjs', 'contrat', 0),
(306, 'steam:110000132580eb0', 'RndAQADGjs', '9mm_rounds', 0),
(307, 'steam:110000132580eb0', 'RndAQADGjs', 'plongee2', 0),
(308, 'steam:110000132580eb0', 'RndAQADGjs', 'donut', 0),
(309, 'steam:110000132580eb0', 'RndAQADGjs', 'macka', 0),
(310, 'steam:110000132580eb0', 'RndAQADGjs', 'turtle_pooch', 0),
(311, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_BAT', 0),
(312, 'steam:110000132580eb0', 'RndAQADGjs', 'meth', 0),
(313, 'steam:110000132580eb0', 'RndAQADGjs', 'marabou', 0),
(314, 'steam:110000132580eb0', 'RndAQADGjs', 'icetea', 0),
(315, 'steam:110000132580eb0', 'RndAQADGjs', 'protein_shake', 0),
(316, 'steam:110000132580eb0', 'RndAQADGjs', 'vegetables', 0),
(317, 'steam:110000132580eb0', 'RndAQADGjs', 'whool', 0),
(318, 'steam:110000132580eb0', 'RndAQADGjs', 'breathalyzer', 0),
(319, 'steam:110000132580eb0', 'RndAQADGjs', 'receipt', 0),
(320, 'steam:110000132580eb0', 'RndAQADGjs', 'sportlunch', 0),
(321, 'steam:110000132580eb0', 'RndAQADGjs', 'scratchoff', 0),
(322, 'steam:110000132580eb0', 'RndAQADGjs', 'fanta', 0),
(323, 'steam:110000132580eb0', 'RndAQADGjs', 'bandage', 0),
(324, 'steam:110000132580eb0', 'RndAQADGjs', 'shotgun_shells', 0),
(325, 'steam:110000132580eb0', 'RndAQADGjs', 'grapperaisin', 0),
(326, 'steam:110000132580eb0', 'RndAQADGjs', 'coca', 0),
(327, 'steam:110000132580eb0', 'RndAQADGjs', 'fakepee', 0),
(328, 'steam:110000132580eb0', 'RndAQADGjs', 'binoculars', 0),
(329, 'steam:110000132580eb0', 'RndAQADGjs', 'golem', 0),
(330, 'steam:110000132580eb0', 'RndAQADGjs', 'bolchips', 0),
(331, 'steam:110000132580eb0', 'RndAQADGjs', 'saucisson', 0),
(332, 'steam:110000132580eb0', 'RndAQADGjs', 'baconburger', 0),
(333, 'steam:110000132580eb0', 'RndAQADGjs', 'litter_pooch', 0),
(334, 'steam:110000132580eb0', 'RndAQADGjs', 'gold', 0),
(335, 'steam:110000132580eb0', 'RndAQADGjs', 'sprite', 0),
(336, 'steam:110000132580eb0', 'RndAQADGjs', 'croquettes', 0),
(337, 'steam:110000132580eb0', 'RndAQADGjs', 'cdl', 0),
(338, 'steam:110000132580eb0', 'RndAQADGjs', 'weapons_license3', 0),
(339, 'steam:110000132580eb0', 'RndAQADGjs', 'heroine', 0),
(340, 'steam:110000132580eb0', 'RndAQADGjs', 'coke', 0),
(341, 'steam:110000132580eb0', 'RndAQADGjs', 'lighter', 0),
(342, 'steam:110000132580eb0', 'RndAQADGjs', 'copper', 0),
(343, 'steam:110000132580eb0', 'RndAQADGjs', 'fixkit', 0),
(344, 'steam:110000132580eb0', 'RndAQADGjs', 'petrol', 0),
(345, 'steam:110000132580eb0', 'RndAQADGjs', 'crack', 0),
(346, 'steam:110000132580eb0', 'RndAQADGjs', 'pastacarbonara', 0),
(347, 'steam:110000132580eb0', 'RndAQADGjs', 'drugtest', 0),
(348, 'steam:110000132580eb0', 'RndAQADGjs', 'clip', 0),
(349, 'steam:110000132580eb0', 'RndAQADGjs', 'bolpistache', 0),
(350, 'steam:110000132580eb0', 'RndAQADGjs', 'marriage_license', 0),
(351, 'steam:110000132580eb0', 'RndAQADGjs', 'poppy', 0),
(352, 'steam:110000132580eb0', 'RndAQADGjs', 'narcan', 0),
(353, 'steam:110000132580eb0', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(354, 'steam:110000132580eb0', 'RndAQADGjs', 'radio', 0),
(355, 'steam:110000132580eb0', 'RndAQADGjs', 'menthe', 0),
(356, 'steam:110000132580eb0', 'RndAQADGjs', 'beer', 0),
(357, 'steam:110000132580eb0', 'RndAQADGjs', 'fabric', 0),
(358, 'steam:110000132580eb0', 'RndAQADGjs', 'clothe', 0),
(359, 'steam:110000132580eb0', 'RndAQADGjs', 'gym_membership', 0),
(360, 'steam:110000132580eb0', 'RndAQADGjs', 'pills', 0),
(361, 'steam:110000132580eb0', 'RndAQADGjs', 'ephedrine', 0),
(362, 'steam:110000132580eb0', 'RndAQADGjs', 'slaughtered_chicken', 0),
(363, 'steam:110000132580eb0', 'RndAQADGjs', 'mojito', 0),
(364, 'steam:110000132580eb0', 'RndAQADGjs', 'leather', 0),
(365, 'steam:110000132580eb0', 'RndAQADGjs', 'yusuf', 0),
(366, 'steam:110000132580eb0', 'RndAQADGjs', 'opium', 0),
(367, 'steam:110000132580eb0', 'RndAQADGjs', 'jagerbomb', 0),
(368, 'steam:110000132580eb0', 'RndAQADGjs', 'alive_chicken', 0),
(369, 'steam:1100001068ef13c', 'RndAQADGjs', 'diving_license', 0),
(370, 'steam:1100001068ef13c', 'RndAQADGjs', 'carokit', 0),
(371, 'steam:1100001068ef13c', 'RndAQADGjs', 'cola', 0),
(372, 'steam:1100001068ef13c', 'RndAQADGjs', 'jager', 0),
(373, 'steam:1100001068ef13c', 'RndAQADGjs', 'gazbottle', 0),
(374, 'steam:1100001068ef13c', 'RndAQADGjs', 'baconburger', 0),
(375, 'steam:1100001068ef13c', 'RndAQADGjs', 'receipt', 0),
(376, 'steam:1100001068ef13c', 'RndAQADGjs', 'painkiller', 0),
(377, 'steam:1100001068ef13c', 'RndAQADGjs', 'soda', 0),
(378, 'steam:1100001068ef13c', 'RndAQADGjs', 'lsd', 0),
(379, 'steam:1100001068ef13c', 'RndAQADGjs', 'jagerbomb', 0),
(380, 'steam:1100001068ef13c', 'RndAQADGjs', 'chips', 0),
(381, 'steam:1100001068ef13c', 'RndAQADGjs', 'pearl', 0),
(382, 'steam:1100001068ef13c', 'RndAQADGjs', 'nitrocannister', 0),
(383, 'steam:1100001068ef13c', 'RndAQADGjs', 'turtle', 0),
(384, 'steam:1100001068ef13c', 'RndAQADGjs', 'pearl_pooch', 0),
(385, 'steam:1100001068ef13c', 'RndAQADGjs', 'loka', 0),
(386, 'steam:1100001068ef13c', 'RndAQADGjs', 'leather', 0),
(387, 'steam:1100001068ef13c', 'RndAQADGjs', 'meat', 0),
(388, 'steam:1100001068ef13c', 'RndAQADGjs', 'iron', 0),
(389, 'steam:1100001068ef13c', 'RndAQADGjs', 'pilot_license', 0),
(390, 'steam:1100001068ef13c', 'RndAQADGjs', 'sprite', 0),
(391, 'steam:1100001068ef13c', 'RndAQADGjs', 'cigarett', 0),
(392, 'steam:1100001068ef13c', 'RndAQADGjs', 'armor', 0),
(393, 'steam:1100001068ef13c', 'RndAQADGjs', 'turtle_pooch', 0),
(394, 'steam:1100001068ef13c', 'RndAQADGjs', 'carotool', 0),
(395, 'steam:1100001068ef13c', 'RndAQADGjs', 'drpepper', 0),
(396, 'steam:1100001068ef13c', 'RndAQADGjs', 'cdl', 0),
(397, 'steam:1100001068ef13c', 'RndAQADGjs', 'cocacola', 0),
(398, 'steam:1100001068ef13c', 'RndAQADGjs', 'petrol', 0),
(399, 'steam:1100001068ef13c', 'RndAQADGjs', 'powerade', 0),
(400, 'steam:1100001068ef13c', 'RndAQADGjs', 'fixkit', 0),
(401, 'steam:1100001068ef13c', 'RndAQADGjs', 'breathalyzer', 0),
(402, 'steam:1100001068ef13c', 'RndAQADGjs', 'martini', 0),
(403, 'steam:1100001068ef13c', 'RndAQADGjs', 'beer', 0),
(404, 'steam:1100001068ef13c', 'RndAQADGjs', 'litter', 0),
(405, 'steam:1100001068ef13c', 'RndAQADGjs', 'packaged_plank', 0),
(406, 'steam:1100001068ef13c', 'RndAQADGjs', 'clip', 0),
(407, 'steam:1100001068ef13c', 'RndAQADGjs', 'water', 0),
(408, 'steam:1100001068ef13c', 'RndAQADGjs', 'grapperaisin', 0),
(409, 'steam:1100001068ef13c', 'RndAQADGjs', 'boating_license', 0),
(410, 'steam:1100001068ef13c', 'RndAQADGjs', 'croquettes', 0),
(411, 'steam:1100001068ef13c', 'RndAQADGjs', 'menthe', 0),
(412, 'steam:1100001068ef13c', 'RndAQADGjs', 'opium', 0),
(413, 'steam:1100001068ef13c', 'RndAQADGjs', 'pastacarbonara', 0),
(414, 'steam:1100001068ef13c', 'RndAQADGjs', 'fishing_license', 0),
(415, 'steam:1100001068ef13c', 'RndAQADGjs', 'licenseplate', 0),
(416, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(417, 'steam:1100001068ef13c', 'RndAQADGjs', 'fanta', 0),
(418, 'steam:1100001068ef13c', 'RndAQADGjs', 'medikit', 0),
(419, 'steam:1100001068ef13c', 'RndAQADGjs', 'coca', 0),
(420, 'steam:1100001068ef13c', 'RndAQADGjs', 'coffee', 0),
(421, 'steam:1100001068ef13c', 'RndAQADGjs', 'teqpaf', 0),
(422, 'steam:1100001068ef13c', 'RndAQADGjs', 'ephedrine', 0),
(423, 'steam:1100001068ef13c', 'RndAQADGjs', 'saucisson', 0),
(424, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(425, 'steam:1100001068ef13c', 'RndAQADGjs', 'boitier', 0),
(426, 'steam:1100001068ef13c', 'RndAQADGjs', 'bread', 0),
(427, 'steam:1100001068ef13c', 'RndAQADGjs', 'ephedra', 0),
(428, 'steam:1100001068ef13c', 'RndAQADGjs', 'blackberry', 0),
(429, 'steam:1100001068ef13c', 'RndAQADGjs', 'cheesebows', 0),
(430, 'steam:1100001068ef13c', 'RndAQADGjs', 'icetea', 0),
(431, 'steam:1100001068ef13c', 'RndAQADGjs', 'drugtest', 0),
(432, 'steam:1100001068ef13c', 'RndAQADGjs', 'energy', 0),
(433, 'steam:1100001068ef13c', 'RndAQADGjs', 'grip', 0),
(434, 'steam:1100001068ef13c', 'RndAQADGjs', 'meth_pooch', 0),
(435, 'steam:1100001068ef13c', 'RndAQADGjs', 'bolnoixcajou', 0),
(436, 'steam:1100001068ef13c', 'RndAQADGjs', 'vodka', 0),
(437, 'steam:1100001068ef13c', 'RndAQADGjs', 'wrench', 0),
(438, 'steam:1100001068ef13c', 'RndAQADGjs', 'wood', 0),
(439, 'steam:1100001068ef13c', 'RndAQADGjs', 'lighter', 0),
(440, 'steam:1100001068ef13c', 'RndAQADGjs', 'packaged_chicken', 0),
(441, 'steam:1100001068ef13c', 'RndAQADGjs', 'metreshooter', 0),
(442, 'steam:1100001068ef13c', 'RndAQADGjs', 'whool', 0),
(443, 'steam:1100001068ef13c', 'RndAQADGjs', 'poppy', 0),
(444, 'steam:1100001068ef13c', 'RndAQADGjs', 'whiskycoca', 0),
(445, 'steam:1100001068ef13c', 'RndAQADGjs', 'defibrillateur', 0),
(446, 'steam:1100001068ef13c', 'RndAQADGjs', 'bolpistache', 0),
(447, 'steam:1100001068ef13c', 'RndAQADGjs', 'plongee1', 0),
(448, 'steam:1100001068ef13c', 'RndAQADGjs', 'whisky', 0),
(449, 'steam:1100001068ef13c', 'RndAQADGjs', 'whiskey', 0),
(450, 'steam:1100001068ef13c', 'RndAQADGjs', 'opium_pooch', 0),
(451, 'steam:1100001068ef13c', 'RndAQADGjs', 'marijuana', 0),
(452, 'steam:1100001068ef13c', 'RndAQADGjs', 'donut', 0),
(453, 'steam:1100001068ef13c', 'RndAQADGjs', 'pcp', 0),
(454, 'steam:1100001068ef13c', 'RndAQADGjs', 'pizza', 0),
(455, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(456, 'steam:1100001068ef13c', 'RndAQADGjs', 'marabou', 0),
(457, 'steam:1100001068ef13c', 'RndAQADGjs', 'weed', 0),
(458, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(459, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(460, 'steam:1100001068ef13c', 'RndAQADGjs', 'weed_pooch', 0),
(461, 'steam:1100001068ef13c', 'RndAQADGjs', 'WEAPON_BAT', 0),
(462, 'steam:1100001068ef13c', 'RndAQADGjs', 'scratchoff_used', 0),
(463, 'steam:1100001068ef13c', 'RndAQADGjs', 'weapons_license2', 0),
(464, 'steam:1100001068ef13c', 'RndAQADGjs', 'copper', 0),
(465, 'steam:1100001068ef13c', 'RndAQADGjs', 'weapons_license1', 0),
(466, 'steam:1100001068ef13c', 'RndAQADGjs', 'washed_stone', 0),
(467, 'steam:1100001068ef13c', 'RndAQADGjs', 'vodkafruit', 0),
(468, 'steam:1100001068ef13c', 'RndAQADGjs', 'vodkaenergy', 0),
(469, 'steam:1100001068ef13c', 'RndAQADGjs', 'yusuf', 0),
(470, 'steam:1100001068ef13c', 'RndAQADGjs', 'stone', 0),
(471, 'steam:1100001068ef13c', 'RndAQADGjs', 'vegetables', 0),
(472, 'steam:1100001068ef13c', 'RndAQADGjs', 'dabs', 0),
(473, 'steam:1100001068ef13c', 'RndAQADGjs', 'blowpipe', 0),
(474, 'steam:1100001068ef13c', 'RndAQADGjs', 'lsd_pooch', 0),
(475, 'steam:1100001068ef13c', 'RndAQADGjs', 'gold', 0),
(476, 'steam:1100001068ef13c', 'RndAQADGjs', '9mm_rounds', 0),
(477, 'steam:1100001068ef13c', 'RndAQADGjs', 'coke', 0),
(478, 'steam:1100001068ef13c', 'RndAQADGjs', 'taxi_license', 0),
(479, 'steam:1100001068ef13c', 'RndAQADGjs', 'mlic', 0),
(480, 'steam:1100001068ef13c', 'RndAQADGjs', 'diamond', 0),
(481, 'steam:1100001068ef13c', 'RndAQADGjs', 'bolchips', 0),
(482, 'steam:1100001068ef13c', 'RndAQADGjs', 'coke_pooch', 0),
(483, 'steam:1100001068ef13c', 'RndAQADGjs', 'slaughtered_chicken', 0),
(484, 'steam:1100001068ef13c', 'RndAQADGjs', 'silencieux', 0),
(485, 'steam:1100001068ef13c', 'RndAQADGjs', 'ice', 0),
(486, 'steam:1100001068ef13c', 'RndAQADGjs', 'clothe', 0),
(487, 'steam:1100001068ef13c', 'RndAQADGjs', 'meth', 0),
(488, 'steam:1100001068ef13c', 'RndAQADGjs', 'crack', 0),
(489, 'steam:1100001068ef13c', 'RndAQADGjs', 'radio', 0),
(490, 'steam:1100001068ef13c', 'RndAQADGjs', 'shotgun_shells', 0),
(491, 'steam:1100001068ef13c', 'RndAQADGjs', 'weapons_license3', 0),
(492, 'steam:1100001068ef13c', 'RndAQADGjs', 'bandage', 0),
(493, 'steam:1100001068ef13c', 'RndAQADGjs', 'fixtool', 0),
(494, 'steam:1100001068ef13c', 'RndAQADGjs', 'scratchoff', 0),
(495, 'steam:1100001068ef13c', 'RndAQADGjs', 'burger', 0),
(496, 'steam:1100001068ef13c', 'RndAQADGjs', 'firstaidpass', 0),
(497, 'steam:1100001068ef13c', 'RndAQADGjs', 'jusfruit', 0),
(498, 'steam:1100001068ef13c', 'RndAQADGjs', 'rhumcoca', 0),
(499, 'steam:1100001068ef13c', 'RndAQADGjs', 'rhum', 0),
(500, 'steam:1100001068ef13c', 'RndAQADGjs', 'golem', 0),
(501, 'steam:1100001068ef13c', 'RndAQADGjs', 'contrat', 0),
(502, 'steam:1100001068ef13c', 'RndAQADGjs', 'fakepee', 0),
(503, 'steam:1100001068ef13c', 'RndAQADGjs', 'macka', 0),
(504, 'steam:1100001068ef13c', 'RndAQADGjs', 'rhumfruit', 0),
(505, 'steam:1100001068ef13c', 'RndAQADGjs', 'protein_shake', 0),
(506, 'steam:1100001068ef13c', 'RndAQADGjs', 'binoculars', 0),
(507, 'steam:1100001068ef13c', 'RndAQADGjs', 'gym_membership', 0),
(508, 'steam:1100001068ef13c', 'RndAQADGjs', 'pills', 0),
(509, 'steam:1100001068ef13c', 'RndAQADGjs', 'cutted_wood', 0),
(510, 'steam:1100001068ef13c', 'RndAQADGjs', 'tacos', 0),
(511, 'steam:1100001068ef13c', 'RndAQADGjs', 'sportlunch', 0),
(512, 'steam:1100001068ef13c', 'RndAQADGjs', 'hunting_license', 0),
(513, 'steam:1100001068ef13c', 'RndAQADGjs', 'lockpick', 0),
(514, 'steam:1100001068ef13c', 'RndAQADGjs', 'petrol_raffin', 0),
(515, 'steam:1100001068ef13c', 'RndAQADGjs', 'gasoline', 0),
(516, 'steam:1100001068ef13c', 'RndAQADGjs', 'flashlight', 0),
(517, 'steam:1100001068ef13c', 'RndAQADGjs', 'fabric', 0),
(518, 'steam:1100001068ef13c', 'RndAQADGjs', 'firstaidkit', 0),
(519, 'steam:1100001068ef13c', 'RndAQADGjs', 'mojito', 0),
(520, 'steam:1100001068ef13c', 'RndAQADGjs', 'litter_pooch', 0),
(521, 'steam:1100001068ef13c', 'RndAQADGjs', 'narcan', 0),
(522, 'steam:1100001068ef13c', 'RndAQADGjs', 'dlic', 0),
(523, 'steam:1100001068ef13c', 'RndAQADGjs', 'mixapero', 0),
(524, 'steam:1100001068ef13c', 'RndAQADGjs', 'lotteryticket', 0),
(525, 'steam:1100001068ef13c', 'RndAQADGjs', 'bolcacahuetes', 0),
(526, 'steam:1100001068ef13c', 'RndAQADGjs', 'marriage_license', 0),
(527, 'steam:1100001068ef13c', 'RndAQADGjs', 'plongee2', 0),
(528, 'steam:1100001068ef13c', 'RndAQADGjs', 'cannabis', 0),
(529, 'steam:1100001068ef13c', 'RndAQADGjs', 'limonade', 0),
(530, 'steam:1100001068ef13c', 'RndAQADGjs', 'alive_chicken', 0),
(531, 'steam:1100001068ef13c', 'RndAQADGjs', 'heroine', 0),
(532, 'steam:1100001068ef13c', 'RndAQADGjs', 'fish', 0),
(533, 'steam:1100001068ef13c', 'RndAQADGjs', 'cocaine', 0),
(534, 'steam:1100001068ef13c', 'RndAQADGjs', 'tequila', 0),
(535, 'steam:110000135837b81', 'RndAQADGjs', 'defuser', 0),
(536, 'steam:110000135837b81', 'RndAQADGjs', 'bread', 0),
(537, 'steam:110000135837b81', 'RndAQADGjs', 'limonade', 0),
(538, 'steam:110000135837b81', 'RndAQADGjs', 'scratchoff', 0),
(539, 'steam:110000135837b81', 'RndAQADGjs', 'leather', 0),
(540, 'steam:110000135837b81', 'RndAQADGjs', 'meat', 0),
(541, 'steam:110000135837b81', 'RndAQADGjs', 'stone', 0),
(542, 'steam:110000135837b81', 'RndAQADGjs', 'lockpick', 0),
(543, 'steam:110000135837b81', 'RndAQADGjs', 'bombpart3', 0),
(544, 'steam:110000135837b81', 'RndAQADGjs', 'ice', 0),
(545, 'steam:110000135837b81', 'RndAQADGjs', 'pcp', 0),
(546, 'steam:110000135837b81', 'RndAQADGjs', 'fishing_license', 0),
(547, 'steam:110000135837b81', 'RndAQADGjs', 'flashlight', 0),
(548, 'steam:110000135837b81', 'RndAQADGjs', 'radio', 0),
(549, 'steam:110000135837b81', 'RndAQADGjs', 'dabs', 0),
(550, 'steam:110000135837b81', 'RndAQADGjs', 'hunting_license', 0),
(551, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(552, 'steam:110000135837b81', 'RndAQADGjs', 'cheesebows', 0),
(553, 'steam:110000135837b81', 'RndAQADGjs', 'blackberry', 0),
(554, 'steam:110000135837b81', 'RndAQADGjs', 'bolchips', 0),
(555, 'steam:110000135837b81', 'RndAQADGjs', 'croquettes', 0),
(556, 'steam:110000135837b81', 'RndAQADGjs', 'litter_pooch', 0),
(557, 'steam:110000135837b81', 'RndAQADGjs', 'iron', 0),
(558, 'steam:110000135837b81', 'RndAQADGjs', 'bolcacahuetes', 0),
(559, 'steam:110000135837b81', 'RndAQADGjs', 'boitier', 0),
(560, 'steam:110000135837b81', 'RndAQADGjs', 'vodkaenergy', 0),
(561, 'steam:110000135837b81', 'RndAQADGjs', 'turtle', 0),
(562, 'steam:110000135837b81', 'RndAQADGjs', 'jagerbomb', 0),
(563, 'steam:110000135837b81', 'RndAQADGjs', 'saucisson', 0),
(564, 'steam:110000135837b81', 'RndAQADGjs', 'pearl', 0),
(565, 'steam:110000135837b81', 'RndAQADGjs', 'ephedrine', 0),
(566, 'steam:110000135837b81', 'RndAQADGjs', 'pizza', 0),
(567, 'steam:110000135837b81', 'RndAQADGjs', 'icetea', 0),
(568, 'steam:110000135837b81', 'RndAQADGjs', 'rhumfruit', 0),
(569, 'steam:110000135837b81', 'RndAQADGjs', 'grip', 0),
(570, 'steam:110000135837b81', 'RndAQADGjs', 'bolnoixcajou', 0),
(571, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(572, 'steam:110000135837b81', 'RndAQADGjs', 'contrat', 0),
(573, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(574, 'steam:110000135837b81', 'RndAQADGjs', 'energy', 0),
(575, 'steam:110000135837b81', 'RndAQADGjs', 'clip', 0),
(576, 'steam:110000135837b81', 'RndAQADGjs', 'lsd_pooch', 0),
(577, 'steam:110000135837b81', 'RndAQADGjs', 'weapons_license1', 0),
(578, 'steam:110000135837b81', 'RndAQADGjs', 'gold', 0),
(579, 'steam:110000135837b81', 'RndAQADGjs', 'fanta', 0),
(580, 'steam:110000135837b81', 'RndAQADGjs', 'grapperaisin', 0),
(581, 'steam:110000135837b81', 'RndAQADGjs', 'heroine', 0),
(582, 'steam:110000135837b81', 'RndAQADGjs', 'carjack', 0),
(583, 'steam:110000135837b81', 'RndAQADGjs', 'wrench', 0),
(584, 'steam:110000135837b81', 'RndAQADGjs', 'cola', 0),
(585, 'steam:110000135837b81', 'RndAQADGjs', 'powerade', 0),
(586, 'steam:110000135837b81', 'RndAQADGjs', 'coca', 0),
(587, 'steam:110000135837b81', 'RndAQADGjs', 'coke', 0),
(588, 'steam:110000135837b81', 'RndAQADGjs', 'lighter', 0),
(589, 'steam:110000135837b81', 'RndAQADGjs', 'donut', 0),
(590, 'steam:110000135837b81', 'RndAQADGjs', 'cdl', 0),
(591, 'steam:110000135837b81', 'RndAQADGjs', 'gym_membership', 0),
(592, 'steam:110000135837b81', 'RndAQADGjs', 'diamond', 0),
(593, 'steam:110000135837b81', 'RndAQADGjs', 'marabou', 0),
(594, 'steam:110000135837b81', 'RndAQADGjs', 'yusuf', 0),
(595, 'steam:110000135837b81', 'RndAQADGjs', 'baconburger', 0),
(596, 'steam:110000135837b81', 'RndAQADGjs', 'blowpipe', 0),
(597, 'steam:110000135837b81', 'RndAQADGjs', 'plongee1', 0),
(598, 'steam:110000135837b81', 'RndAQADGjs', 'jusfruit', 0),
(599, 'steam:110000135837b81', 'RndAQADGjs', 'burger', 0),
(600, 'steam:110000135837b81', 'RndAQADGjs', 'cigarett', 0),
(601, 'steam:110000135837b81', 'RndAQADGjs', 'cocaine', 0),
(602, 'steam:110000135837b81', 'RndAQADGjs', 'opium', 0),
(603, 'steam:110000135837b81', 'RndAQADGjs', 'opium_pooch', 0),
(604, 'steam:110000135837b81', 'RndAQADGjs', 'defibrillateur', 0),
(605, 'steam:110000135837b81', 'RndAQADGjs', 'shotgun_shells', 0),
(606, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_BAT', 0),
(607, 'steam:110000135837b81', 'RndAQADGjs', 'marriage_license', 0),
(608, 'steam:110000135837b81', 'RndAQADGjs', 'boating_license', 0),
(609, 'steam:110000135837b81', 'RndAQADGjs', 'wood', 0),
(610, 'steam:110000135837b81', 'RndAQADGjs', 'whool', 0),
(611, 'steam:110000135837b81', 'RndAQADGjs', 'licenseplate', 0),
(612, 'steam:110000135837b81', 'RndAQADGjs', 'whisky', 0),
(613, 'steam:110000135837b81', 'RndAQADGjs', 'water', 0),
(614, 'steam:110000135837b81', 'RndAQADGjs', 'packaged_chicken', 0),
(615, 'steam:110000135837b81', 'RndAQADGjs', 'whiskey', 0),
(616, 'steam:110000135837b81', 'RndAQADGjs', 'fish', 0),
(617, 'steam:110000135837b81', 'RndAQADGjs', 'petrol_raffin', 0),
(618, 'steam:110000135837b81', 'RndAQADGjs', 'washed_stone', 0),
(619, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(620, 'steam:110000135837b81', 'RndAQADGjs', 'meth', 0),
(621, 'steam:110000135837b81', 'RndAQADGjs', 'weed', 0),
(622, 'steam:110000135837b81', 'RndAQADGjs', 'fixkit', 0),
(623, 'steam:110000135837b81', 'RndAQADGjs', '9mm_rounds', 0),
(624, 'steam:110000135837b81', 'RndAQADGjs', 'weapons_license3', 0),
(625, 'steam:110000135837b81', 'RndAQADGjs', 'soda', 0),
(626, 'steam:110000135837b81', 'RndAQADGjs', 'weed_pooch', 0),
(627, 'steam:110000135837b81', 'RndAQADGjs', 'fixtool', 0),
(628, 'steam:110000135837b81', 'RndAQADGjs', 'ephedra', 0),
(629, 'steam:110000135837b81', 'RndAQADGjs', 'receipt', 0),
(630, 'steam:110000135837b81', 'RndAQADGjs', 'speccheck', 0),
(631, 'steam:110000135837b81', 'RndAQADGjs', 'macka', 0),
(632, 'steam:110000135837b81', 'RndAQADGjs', 'trash', 0),
(633, 'steam:110000135837b81', 'RndAQADGjs', 'poppy', 0),
(634, 'steam:110000135837b81', 'RndAQADGjs', 'binoculars', 0),
(635, 'steam:110000135837b81', 'RndAQADGjs', 'vegetables', 0),
(636, 'steam:110000135837b81', 'RndAQADGjs', 'firstaidkit', 0),
(637, 'steam:110000135837b81', 'RndAQADGjs', 'crack', 0),
(638, 'steam:110000135837b81', 'RndAQADGjs', 'firstaidpass', 0),
(639, 'steam:110000135837b81', 'RndAQADGjs', 'marijuana', 0),
(640, 'steam:110000135837b81', 'RndAQADGjs', 'turtle_pooch', 0),
(641, 'steam:110000135837b81', 'RndAQADGjs', 'lsd', 0),
(642, 'steam:110000135837b81', 'RndAQADGjs', 'mojito', 0),
(643, 'steam:110000135837b81', 'RndAQADGjs', 'copper', 0),
(644, 'steam:110000135837b81', 'RndAQADGjs', 'pills', 0),
(645, 'steam:110000135837b81', 'RndAQADGjs', 'sprite', 0),
(646, 'steam:110000135837b81', 'RndAQADGjs', 'sportlunch', 0),
(647, 'steam:110000135837b81', 'RndAQADGjs', 'vodkafruit', 0),
(648, 'steam:110000135837b81', 'RndAQADGjs', 'fabric', 0),
(649, 'steam:110000135837b81', 'RndAQADGjs', 'weapons_license2', 0),
(650, 'steam:110000135837b81', 'RndAQADGjs', 'packaged_plank', 0),
(651, 'steam:110000135837b81', 'RndAQADGjs', 'gazbottle', 0),
(652, 'steam:110000135837b81', 'RndAQADGjs', 'coffee', 0),
(653, 'steam:110000135837b81', 'RndAQADGjs', 'slaughtered_chicken', 0),
(654, 'steam:110000135837b81', 'RndAQADGjs', 'silencieux', 0),
(655, 'steam:110000135837b81', 'RndAQADGjs', 'clothe', 0),
(656, 'steam:110000135837b81', 'RndAQADGjs', 'scratchoff_used', 0),
(657, 'steam:110000135837b81', 'RndAQADGjs', 'drpepper', 0),
(658, 'steam:110000135837b81', 'RndAQADGjs', 'teqpaf', 0),
(659, 'steam:110000135837b81', 'RndAQADGjs', 'rhum', 0),
(660, 'steam:110000135837b81', 'RndAQADGjs', 'protein_shake', 0),
(661, 'steam:110000135837b81', 'RndAQADGjs', 'bolpistache', 0),
(662, 'steam:110000135837b81', 'RndAQADGjs', 'alive_chicken', 0),
(663, 'steam:110000135837b81', 'RndAQADGjs', 'plongee2', 0),
(664, 'steam:110000135837b81', 'RndAQADGjs', 'meth_pooch', 0),
(665, 'steam:110000135837b81', 'RndAQADGjs', 'litter', 0),
(666, 'steam:110000135837b81', 'RndAQADGjs', 'beer', 0),
(667, 'steam:110000135837b81', 'RndAQADGjs', 'cannabis', 0),
(668, 'steam:110000135837b81', 'RndAQADGjs', 'narcan', 0),
(669, 'steam:110000135837b81', 'RndAQADGjs', 'carotool', 0),
(670, 'steam:110000135837b81', 'RndAQADGjs', 'fakepee', 0),
(671, 'steam:110000135837b81', 'RndAQADGjs', 'mixapero', 0),
(672, 'steam:110000135837b81', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(673, 'steam:110000135837b81', 'RndAQADGjs', 'chips', 0),
(674, 'steam:110000135837b81', 'RndAQADGjs', 'cocacola', 0),
(675, 'steam:110000135837b81', 'RndAQADGjs', 'bombpart1', 0),
(676, 'steam:110000135837b81', 'RndAQADGjs', 'vodka', 0),
(677, 'steam:110000135837b81', 'RndAQADGjs', 'tequila', 0),
(678, 'steam:110000135837b81', 'RndAQADGjs', 'diving_license', 0),
(679, 'steam:110000135837b81', 'RndAQADGjs', 'loka', 0),
(680, 'steam:110000135837b81', 'RndAQADGjs', 'taxi_license', 0),
(681, 'steam:110000135837b81', 'RndAQADGjs', 'pilot_license', 0),
(682, 'steam:110000135837b81', 'RndAQADGjs', 'jager', 0),
(683, 'steam:110000135837b81', 'RndAQADGjs', 'mlic', 0),
(684, 'steam:110000135837b81', 'RndAQADGjs', 'tacos', 0),
(685, 'steam:110000135837b81', 'RndAQADGjs', 'petrol', 0),
(686, 'steam:110000135837b81', 'RndAQADGjs', 'drugtest', 0),
(687, 'steam:110000135837b81', 'RndAQADGjs', 'coke_pooch', 0),
(688, 'steam:110000135837b81', 'RndAQADGjs', 'rhumcoca', 0),
(689, 'steam:110000135837b81', 'RndAQADGjs', 'pastacarbonara', 0),
(690, 'steam:110000135837b81', 'RndAQADGjs', 'armor', 0),
(691, 'steam:110000135837b81', 'RndAQADGjs', 'painkiller', 0),
(692, 'steam:110000135837b81', 'RndAQADGjs', 'martini', 0),
(693, 'steam:110000135837b81', 'RndAQADGjs', 'nitrocannister', 0),
(694, 'steam:110000135837b81', 'RndAQADGjs', 'metreshooter', 0),
(695, 'steam:110000135837b81', 'RndAQADGjs', 'golem', 0),
(696, 'steam:110000135837b81', 'RndAQADGjs', 'whiskycoca', 0),
(697, 'steam:110000135837b81', 'RndAQADGjs', 'pearl_pooch', 0),
(698, 'steam:110000135837b81', 'RndAQADGjs', 'gasoline', 0),
(699, 'steam:110000135837b81', 'RndAQADGjs', 'bombpart2', 0),
(700, 'steam:110000135837b81', 'RndAQADGjs', 'lotteryticket', 0),
(701, 'steam:110000135837b81', 'RndAQADGjs', 'medikit', 0),
(702, 'steam:110000135837b81', 'RndAQADGjs', 'engbomb', 0),
(703, 'steam:110000135837b81', 'RndAQADGjs', 'menthe', 0),
(704, 'steam:110000135837b81', 'RndAQADGjs', 'bandage', 0),
(705, 'steam:110000135837b81', 'RndAQADGjs', 'carokit', 0),
(706, 'steam:110000135837b81', 'RndAQADGjs', 'dlic', 0),
(707, 'steam:110000135837b81', 'RndAQADGjs', 'breathalyzer', 0),
(708, 'steam:110000135837b81', 'RndAQADGjs', 'cutted_wood', 0),
(709, 'steam:11000010a01bdb9', 'RndAQADGjs', 'defuser', 0),
(710, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bombpart3', 0),
(711, 'steam:11000010a01bdb9', 'RndAQADGjs', 'carjack', 0),
(712, 'steam:11000010a01bdb9', 'RndAQADGjs', 'trash', 0),
(713, 'steam:11000010a01bdb9', 'RndAQADGjs', 'speccheck', 0),
(714, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bombpart1', 0),
(715, 'steam:11000010a01bdb9', 'RndAQADGjs', 'bombpart2', 0),
(716, 'steam:11000010a01bdb9', 'RndAQADGjs', 'engbomb', 0),
(717, 'steam:110000112969e8f', 'RndAQADGjs', 'defuser', 0),
(718, 'steam:110000112969e8f', 'RndAQADGjs', 'bread', 0),
(719, 'steam:110000112969e8f', 'RndAQADGjs', 'limonade', 0),
(720, 'steam:110000112969e8f', 'RndAQADGjs', 'leather', 0),
(721, 'steam:110000112969e8f', 'RndAQADGjs', 'scratchoff', 0),
(722, 'steam:110000112969e8f', 'RndAQADGjs', 'meat', 0),
(723, 'steam:110000112969e8f', 'RndAQADGjs', 'stone', 0),
(724, 'steam:110000112969e8f', 'RndAQADGjs', 'lockpick', 0),
(725, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart3', 0),
(726, 'steam:110000112969e8f', 'RndAQADGjs', 'ice', 0),
(727, 'steam:110000112969e8f', 'RndAQADGjs', 'pcp', 0),
(728, 'steam:110000112969e8f', 'RndAQADGjs', 'fishing_license', 0),
(729, 'steam:110000112969e8f', 'RndAQADGjs', 'flashlight', 0),
(730, 'steam:110000112969e8f', 'RndAQADGjs', 'dabs', 0),
(731, 'steam:110000112969e8f', 'RndAQADGjs', 'radio', 0),
(732, 'steam:110000112969e8f', 'RndAQADGjs', 'hunting_license', 0),
(733, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_KNIFE', 0),
(734, 'steam:110000112969e8f', 'RndAQADGjs', 'cheesebows', 0),
(735, 'steam:110000112969e8f', 'RndAQADGjs', 'blackberry', 0),
(736, 'steam:110000112969e8f', 'RndAQADGjs', 'bolchips', 0),
(737, 'steam:110000112969e8f', 'RndAQADGjs', 'croquettes', 0),
(738, 'steam:110000112969e8f', 'RndAQADGjs', 'litter_pooch', 0),
(739, 'steam:110000112969e8f', 'RndAQADGjs', 'iron', 0),
(740, 'steam:110000112969e8f', 'RndAQADGjs', 'bolcacahuetes', 0),
(741, 'steam:110000112969e8f', 'RndAQADGjs', 'boitier', 0),
(742, 'steam:110000112969e8f', 'RndAQADGjs', 'vodkaenergy', 0),
(743, 'steam:110000112969e8f', 'RndAQADGjs', 'turtle', 0),
(744, 'steam:110000112969e8f', 'RndAQADGjs', 'jagerbomb', 0),
(745, 'steam:110000112969e8f', 'RndAQADGjs', 'saucisson', 0),
(746, 'steam:110000112969e8f', 'RndAQADGjs', 'pearl', 0),
(747, 'steam:110000112969e8f', 'RndAQADGjs', 'ephedrine', 0),
(748, 'steam:110000112969e8f', 'RndAQADGjs', 'pizza', 0),
(749, 'steam:110000112969e8f', 'RndAQADGjs', 'icetea', 0),
(750, 'steam:110000112969e8f', 'RndAQADGjs', 'rhumfruit', 0),
(751, 'steam:110000112969e8f', 'RndAQADGjs', 'grip', 0),
(752, 'steam:110000112969e8f', 'RndAQADGjs', 'bolnoixcajou', 0),
(753, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_FLASHLIGHT', 0),
(754, 'steam:110000112969e8f', 'RndAQADGjs', 'contrat', 0),
(755, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_PUMPSHOTGUN', 0),
(756, 'steam:110000112969e8f', 'RndAQADGjs', 'energy', 0),
(757, 'steam:110000112969e8f', 'RndAQADGjs', 'clip', 0),
(758, 'steam:110000112969e8f', 'RndAQADGjs', 'lsd_pooch', 0),
(759, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license1', 0),
(760, 'steam:110000112969e8f', 'RndAQADGjs', 'gold', 0),
(761, 'steam:110000112969e8f', 'RndAQADGjs', 'fanta', 0),
(762, 'steam:110000112969e8f', 'RndAQADGjs', 'grapperaisin', 0),
(763, 'steam:110000112969e8f', 'RndAQADGjs', 'heroine', 0),
(764, 'steam:110000112969e8f', 'RndAQADGjs', 'carjack', 1),
(765, 'steam:110000112969e8f', 'RndAQADGjs', 'wrench', 0),
(766, 'steam:110000112969e8f', 'RndAQADGjs', 'cola', 0),
(767, 'steam:110000112969e8f', 'RndAQADGjs', 'powerade', 0),
(768, 'steam:110000112969e8f', 'RndAQADGjs', 'coca', 0),
(769, 'steam:110000112969e8f', 'RndAQADGjs', 'coke', 0),
(770, 'steam:110000112969e8f', 'RndAQADGjs', 'lighter', 0),
(771, 'steam:110000112969e8f', 'RndAQADGjs', 'donut', 0),
(772, 'steam:110000112969e8f', 'RndAQADGjs', 'cdl', 0),
(773, 'steam:110000112969e8f', 'RndAQADGjs', 'gym_membership', 0),
(774, 'steam:110000112969e8f', 'RndAQADGjs', 'diamond', 0),
(775, 'steam:110000112969e8f', 'RndAQADGjs', 'marabou', 0),
(776, 'steam:110000112969e8f', 'RndAQADGjs', 'yusuf', 0),
(777, 'steam:110000112969e8f', 'RndAQADGjs', 'baconburger', 0),
(778, 'steam:110000112969e8f', 'RndAQADGjs', 'blowpipe', 0),
(779, 'steam:110000112969e8f', 'RndAQADGjs', 'plongee1', 0),
(780, 'steam:110000112969e8f', 'RndAQADGjs', 'jusfruit', 0),
(781, 'steam:110000112969e8f', 'RndAQADGjs', 'burger', 0),
(782, 'steam:110000112969e8f', 'RndAQADGjs', 'cigarett', 0),
(783, 'steam:110000112969e8f', 'RndAQADGjs', 'cocaine', 0),
(784, 'steam:110000112969e8f', 'RndAQADGjs', 'opium', 0),
(785, 'steam:110000112969e8f', 'RndAQADGjs', 'opium_pooch', 0),
(786, 'steam:110000112969e8f', 'RndAQADGjs', 'defibrillateur', 0),
(787, 'steam:110000112969e8f', 'RndAQADGjs', 'shotgun_shells', 0),
(788, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_BAT', 0),
(789, 'steam:110000112969e8f', 'RndAQADGjs', 'marriage_license', 0),
(790, 'steam:110000112969e8f', 'RndAQADGjs', 'boating_license', 0),
(791, 'steam:110000112969e8f', 'RndAQADGjs', 'wood', 0),
(792, 'steam:110000112969e8f', 'RndAQADGjs', 'whool', 0),
(793, 'steam:110000112969e8f', 'RndAQADGjs', 'licenseplate', 0),
(794, 'steam:110000112969e8f', 'RndAQADGjs', 'whisky', 0),
(795, 'steam:110000112969e8f', 'RndAQADGjs', 'water', 0),
(796, 'steam:110000112969e8f', 'RndAQADGjs', 'packaged_chicken', 0),
(797, 'steam:110000112969e8f', 'RndAQADGjs', 'whiskey', 0),
(798, 'steam:110000112969e8f', 'RndAQADGjs', 'fish', 0),
(799, 'steam:110000112969e8f', 'RndAQADGjs', 'petrol_raffin', 0),
(800, 'steam:110000112969e8f', 'RndAQADGjs', 'washed_stone', 0),
(801, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_STUNGUN', 0),
(802, 'steam:110000112969e8f', 'RndAQADGjs', 'meth', 0),
(803, 'steam:110000112969e8f', 'RndAQADGjs', 'weed', 0),
(804, 'steam:110000112969e8f', 'RndAQADGjs', 'fixkit', 0),
(805, 'steam:110000112969e8f', 'RndAQADGjs', '9mm_rounds', 0),
(806, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license3', 0),
(807, 'steam:110000112969e8f', 'RndAQADGjs', 'soda', 0),
(808, 'steam:110000112969e8f', 'RndAQADGjs', 'weed_pooch', 0),
(809, 'steam:110000112969e8f', 'RndAQADGjs', 'fixtool', 0),
(810, 'steam:110000112969e8f', 'RndAQADGjs', 'ephedra', 0),
(811, 'steam:110000112969e8f', 'RndAQADGjs', 'receipt', 0),
(812, 'steam:110000112969e8f', 'RndAQADGjs', 'speccheck', 0),
(813, 'steam:110000112969e8f', 'RndAQADGjs', 'macka', 0),
(814, 'steam:110000112969e8f', 'RndAQADGjs', 'trash', 0),
(815, 'steam:110000112969e8f', 'RndAQADGjs', 'poppy', 0),
(816, 'steam:110000112969e8f', 'RndAQADGjs', 'binoculars', 0),
(817, 'steam:110000112969e8f', 'RndAQADGjs', 'vegetables', 0),
(818, 'steam:110000112969e8f', 'RndAQADGjs', 'firstaidkit', 0),
(819, 'steam:110000112969e8f', 'RndAQADGjs', 'crack', 0),
(820, 'steam:110000112969e8f', 'RndAQADGjs', 'firstaidpass', 0),
(821, 'steam:110000112969e8f', 'RndAQADGjs', 'marijuana', 0),
(822, 'steam:110000112969e8f', 'RndAQADGjs', 'turtle_pooch', 0),
(823, 'steam:110000112969e8f', 'RndAQADGjs', 'lsd', 0),
(824, 'steam:110000112969e8f', 'RndAQADGjs', 'mojito', 0),
(825, 'steam:110000112969e8f', 'RndAQADGjs', 'copper', 0),
(826, 'steam:110000112969e8f', 'RndAQADGjs', 'pills', 0),
(827, 'steam:110000112969e8f', 'RndAQADGjs', 'sprite', 0),
(828, 'steam:110000112969e8f', 'RndAQADGjs', 'sportlunch', 0),
(829, 'steam:110000112969e8f', 'RndAQADGjs', 'vodkafruit', 0),
(830, 'steam:110000112969e8f', 'RndAQADGjs', 'fabric', 0),
(831, 'steam:110000112969e8f', 'RndAQADGjs', 'weapons_license2', 0),
(832, 'steam:110000112969e8f', 'RndAQADGjs', 'packaged_plank', 0),
(833, 'steam:110000112969e8f', 'RndAQADGjs', 'gazbottle', 0),
(834, 'steam:110000112969e8f', 'RndAQADGjs', 'coffee', 0),
(835, 'steam:110000112969e8f', 'RndAQADGjs', 'slaughtered_chicken', 0),
(836, 'steam:110000112969e8f', 'RndAQADGjs', 'silencieux', 0),
(837, 'steam:110000112969e8f', 'RndAQADGjs', 'clothe', 0),
(838, 'steam:110000112969e8f', 'RndAQADGjs', 'scratchoff_used', 0),
(839, 'steam:110000112969e8f', 'RndAQADGjs', 'drpepper', 0),
(840, 'steam:110000112969e8f', 'RndAQADGjs', 'teqpaf', 0),
(841, 'steam:110000112969e8f', 'RndAQADGjs', 'rhum', 0),
(842, 'steam:110000112969e8f', 'RndAQADGjs', 'protein_shake', 0),
(843, 'steam:110000112969e8f', 'RndAQADGjs', 'bolpistache', 0),
(844, 'steam:110000112969e8f', 'RndAQADGjs', 'alive_chicken', 0),
(845, 'steam:110000112969e8f', 'RndAQADGjs', 'plongee2', 0),
(846, 'steam:110000112969e8f', 'RndAQADGjs', 'meth_pooch', 0),
(847, 'steam:110000112969e8f', 'RndAQADGjs', 'litter', 0),
(848, 'steam:110000112969e8f', 'RndAQADGjs', 'beer', 0),
(849, 'steam:110000112969e8f', 'RndAQADGjs', 'cannabis', 0),
(850, 'steam:110000112969e8f', 'RndAQADGjs', 'narcan', 0),
(851, 'steam:110000112969e8f', 'RndAQADGjs', 'carotool', 0),
(852, 'steam:110000112969e8f', 'RndAQADGjs', 'fakepee', 0),
(853, 'steam:110000112969e8f', 'RndAQADGjs', 'mixapero', 0),
(854, 'steam:110000112969e8f', 'RndAQADGjs', 'WEAPON_PISTOL', 0),
(855, 'steam:110000112969e8f', 'RndAQADGjs', 'chips', 0),
(856, 'steam:110000112969e8f', 'RndAQADGjs', 'cocacola', 0),
(857, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart1', 0),
(858, 'steam:110000112969e8f', 'RndAQADGjs', 'vodka', 0),
(859, 'steam:110000112969e8f', 'RndAQADGjs', 'tequila', 0),
(860, 'steam:110000112969e8f', 'RndAQADGjs', 'diving_license', 0),
(861, 'steam:110000112969e8f', 'RndAQADGjs', 'loka', 0),
(862, 'steam:110000112969e8f', 'RndAQADGjs', 'taxi_license', 0),
(863, 'steam:110000112969e8f', 'RndAQADGjs', 'pilot_license', 0),
(864, 'steam:110000112969e8f', 'RndAQADGjs', 'jager', 0),
(865, 'steam:110000112969e8f', 'RndAQADGjs', 'mlic', 0),
(866, 'steam:110000112969e8f', 'RndAQADGjs', 'tacos', 0),
(867, 'steam:110000112969e8f', 'RndAQADGjs', 'petrol', 0),
(868, 'steam:110000112969e8f', 'RndAQADGjs', 'drugtest', 0),
(869, 'steam:110000112969e8f', 'RndAQADGjs', 'coke_pooch', 0),
(870, 'steam:110000112969e8f', 'RndAQADGjs', 'rhumcoca', 0),
(871, 'steam:110000112969e8f', 'RndAQADGjs', 'pastacarbonara', 0),
(872, 'steam:110000112969e8f', 'RndAQADGjs', 'armor', 0),
(873, 'steam:110000112969e8f', 'RndAQADGjs', 'painkiller', 0),
(874, 'steam:110000112969e8f', 'RndAQADGjs', 'martini', 0),
(875, 'steam:110000112969e8f', 'RndAQADGjs', 'nitrocannister', 0),
(876, 'steam:110000112969e8f', 'RndAQADGjs', 'metreshooter', 0),
(877, 'steam:110000112969e8f', 'RndAQADGjs', 'golem', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `irpid`, `item`, `count`) VALUES
(878, 'steam:110000112969e8f', 'RndAQADGjs', 'whiskycoca', 0),
(879, 'steam:110000112969e8f', 'RndAQADGjs', 'pearl_pooch', 0),
(880, 'steam:110000112969e8f', 'RndAQADGjs', 'gasoline', 0),
(881, 'steam:110000112969e8f', 'RndAQADGjs', 'bombpart2', 0),
(882, 'steam:110000112969e8f', 'RndAQADGjs', 'lotteryticket', 0),
(883, 'steam:110000112969e8f', 'RndAQADGjs', 'medikit', 0),
(884, 'steam:110000112969e8f', 'RndAQADGjs', 'engbomb', 0),
(885, 'steam:110000112969e8f', 'RndAQADGjs', 'menthe', 0),
(886, 'steam:110000112969e8f', 'RndAQADGjs', 'bandage', 0),
(887, 'steam:110000112969e8f', 'RndAQADGjs', 'carokit', 0),
(888, 'steam:110000112969e8f', 'RndAQADGjs', 'dlic', 0),
(889, 'steam:110000112969e8f', 'RndAQADGjs', 'breathalyzer', 0),
(890, 'steam:110000112969e8f', 'RndAQADGjs', 'cutted_wood', 0),
(891, 'steam:110000132580eb0', '', 'defuser', 0),
(892, 'steam:110000132580eb0', '', 'speccheck', 0),
(893, 'steam:110000132580eb0', '', 'bombpart3', 0),
(894, 'steam:110000132580eb0', '', 'bombpart1', 0),
(895, 'steam:110000132580eb0', '', 'bombpart2', 0),
(896, 'steam:110000132580eb0', '', 'carjack', 0),
(897, 'steam:110000132580eb0', '', 'engbomb', 0),
(898, 'steam:110000132580eb0', '', 'trash', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `owner` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `inshop` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `inshop`) VALUES
('Peterbuilt', '337flatbed', 120000, 'utility', 1),
('3500flatbed', '3500 Flatbed', 78000, 'imports', 1),
('Adder', 'adder', 900000, 'super', 1),
('Akuma', 'AKUMA', 7500, 'motorcycles', 1),
('Alpha', 'alpha', 60000, 'sports', 1),
('Ardent', 'ardent', 1150000, 'sportsclassics', 1),
('Asea', 'asea', 5500, 'sedans', 1),
('Autarch', 'autarch', 1955000, 'super', 1),
('Avarus', 'avarus', 18000, 'motorcycles', 1),
('Bagger', 'bagger', 13500, 'motorcycles', 1),
('Baller', 'baller2', 40000, 'suvs', 1),
('Baller Sport', 'baller3', 60000, 'suvs', 1),
('Banshee', 'banshee', 70000, 'sports', 1),
('Banshee 900R', 'banshee2', 255000, 'sports', 1),
('Bati 801', 'bati', 12000, 'motorcycles', 1),
('Bati 801RR', 'bati2', 19000, 'motorcycles', 1),
('Bestia GTS', 'bestiagts', 55000, 'sports', 1),
('BF400', 'bf400', 6500, 'motorcycles', 1),
('Bf Injection', 'bfinjection', 16000, 'offroad', 1),
('Bifta', 'bifta', 12000, 'offroad', 1),
('Bison', 'bison', 45000, 'vans', 1),
('Blade', 'blade', 15000, 'muscle', 1),
('Blazer', 'blazer', 6500, 'offroad', 1),
('Blazer Sport', 'blazer4', 8500, 'offroad', 1),
('blazer5', 'blazer5', 1755600, 'offroad', 1),
('Blista', 'blista', 8000, 'compacts', 1),
('BMX (velo)', 'bmx', 160, 'motorcycles', 1),
('Bobcat XL', 'bobcatxl', 32000, 'vans', 1),
('Brawler', 'brawler', 45000, 'offroad', 1),
('Brioso R/A', 'brioso', 18000, 'compacts', 1),
('Btype', 'btype', 62000, 'sportsclassics', 1),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 1),
('Btype Luxe', 'btype3', 85000, 'sportsclassics', 1),
('Buccaneer', 'buccaneer', 18000, 'muscle', 1),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 1),
('Buffalo', 'buffalo', 12000, 'sports', 1),
('Buffalo S', 'buffalo2', 20000, 'sports', 1),
('Bullet', 'bullet', 90000, 'super', 1),
('Burrito', 'burrito3', 19000, 'vans', 1),
('Camper', 'camper', 42000, 'vans', 1),
('Carbonizzare', 'carbonizzare', 75000, 'sports', 1),
('Carbon RS', 'carbonrs', 18000, 'motorcycles', 1),
('Casco', 'casco', 30000, 'sportsclassics', 1),
('Cavalcade', 'cavalcade2', 55000, 'suvs', 1),
('Cheetah', 'cheetah', 375000, 'super', 1),
('Chimera', 'chimera', 38000, 'motorcycles', 1),
('Chino', 'chino', 15000, 'muscle', 1),
('Chino Luxe', 'chino2', 19000, 'muscle', 1),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 1),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 1),
('Cognoscenti', 'cognoscenti', 55000, 'sedans', 1),
('Comet', 'comet2', 65000, 'sports', 1),
('Comet 5', 'comet5', 1145000, 'sports', 1),
('Contender', 'contender', 70000, 'suvs', 1),
('Coquette', 'coquette', 65000, 'sports', 1),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 1),
('Coquette BlackFin', 'coquette3', 55000, 'muscle', 1),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles', 1),
('Cyclone', 'cyclone', 1890000, 'super', 1),
('Daemon', 'daemon', 11500, 'motorcycles', 1),
('Daemon High', 'daemon2', 13500, 'motorcycles', 1),
('Defiler', 'defiler', 9800, 'motorcycles', 1),
('Deluxo', 'deluxo', 4721500, 'sportsclassics', 1),
('Dominator', 'dominator', 35000, 'muscle', 1),
('Double T', 'double', 28000, 'motorcycles', 1),
('Dubsta', 'dubsta', 45000, 'suvs', 1),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 1),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 1),
('Dukes', 'dukes', 28000, 'muscle', 1),
('Dune Buggy', 'dune', 8000, 'offroad', 1),
('Elegy', 'elegy2', 38500, 'sports', 1),
('Emperor', 'emperor', 8500, 'sedans', 1),
('Enduro', 'enduro', 5500, 'motorcycles', 1),
('Entity XF', 'entityxf', 425000, 'importcars', 0),
('Esskey', 'esskey', 4200, 'motorcycles', 1),
('Exemplar', 'exemplar', 32000, 'coupes', 1),
('F620', 'f620', 40000, 'coupes', 1),
('Faction', 'faction', 20000, 'muscle', 1),
('Faction Rider', 'faction2', 30000, 'muscle', 1),
('Faction XL', 'faction3', 40000, 'muscle', 1),
('Faggio', 'faggio', 1900, 'motorcycles', 1),
('Vespa', 'faggio2', 2800, 'motorcycles', 1),
('Felon', 'felon', 42000, 'coupes', 1),
('Felon GT', 'felon2', 55000, 'coupes', 1),
('Feltzer', 'feltzer2', 55000, 'sports', 1),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 1),
('Freightliner', 'fhauler', 70000, 'imports', 1),
('Fixter (velo)', 'fixter', 225, 'motorcycles', 1),
('Flat Bed', 'flatbed', 90000, 'utility', 1),
('FMJ', 'fmj', 185000, 'super', 1),
('Road King', 'foxharley1', 23000, 'importbikes', 0),
('Road Glide', 'foxharley2', 30000, 'importbikes', 0),
('Fhantom', 'fq2', 17000, 'suvs', 1),
('Fugitive', 'fugitive', 12000, 'sedans', 1),
('Furore GT', 'furoregt', 45000, 'sports', 1),
('Fusilade', 'fusilade', 40000, 'sports', 1),
('Gargoyle', 'gargoyle', 16500, 'motorcycles', 1),
('Gauntlet', 'gauntlet', 30000, 'muscle', 1),
('Gang Burrito', 'gburrito', 45000, 'vans', 1),
('Burrito', 'gburrito2', 29000, 'vans', 1),
('Glendale', 'glendale', 6500, 'sedans', 1),
('Grabger', 'granger', 50000, 'suvs', 1),
('Gresley', 'gresley', 47500, 'suvs', 1),
('GT 500', 'gt500', 785000, 'sportsclassics', 1),
('Guardian', 'guardian', 45000, 'offroad', 1),
('Hakuchou', 'hakuchou', 31000, 'motorcycles', 1),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 1),
('Heavy Wrecker', 'hdwrecker', 500000, 'utility', 1),
('Hellcat', 'hellcatlb', 65000, 'importcars', 0),
('Hermes', 'hermes', 535000, 'muscle', 1),
('Hexer', 'hexer', 12000, 'motorcycles', 1),
('Hotknife', 'hotknife', 125000, 'muscle', 1),
('Huntley S', 'huntley', 40000, 'suvs', 1),
('Hustler', 'hustler', 625000, 'muscle', 1),
('Street Glide', 'hvrod', 25000, 'imports', 1),
('Infernus', 'infernus', 180000, 'super', 1),
('Innovation', 'innovation', 23500, 'motorcycles', 1),
('Intruder', 'intruder', 7500, 'sedans', 1),
('Issi', 'issi2', 10000, 'compacts', 1),
('Jackal', 'jackal', 38000, 'coupes', 1),
('Jester', 'jester', 65000, 'sports', 1),
('Jester(Racecar)', 'jester2', 135000, 'sports', 1),
('Journey', 'journey', 6500, 'vans', 1),
('k9', 'k9', 1, 'emergency', 1),
('Kamacho', 'kamacho', 345000, 'offroad', 1),
('Khamelion', 'khamelion', 38000, 'sports', 1),
('Kuruma', 'kuruma', 30000, 'sports', 1),
('Landstalker', 'landstalker', 35000, 'suvs', 1),
('RE-7B', 'le7b', 325000, 'super', 1),
('Lynx', 'lynx', 40000, 'sports', 1),
('Mamba', 'mamba', 70000, 'sports', 1),
('Manana', 'manana', 12800, 'sportsclassics', 1),
('Manchez', 'manchez', 5300, 'motorcycles', 1),
('Massacro', 'massacro', 65000, 'sports', 1),
('Massacro(Racecar)', 'massacro2', 130000, 'sports', 1),
('Mesa', 'mesa', 16000, 'suvs', 1),
('Mesa Trail', 'mesa3', 40000, 'suvs', 1),
('Minivan', 'minivan', 13000, 'vans', 1),
('Monroe', 'monroe', 55000, 'sportsclassics', 1),
('The Liberator', 'monster', 210000, 'offroad', 1),
('Moonbeam', 'moonbeam', 18000, 'vans', 1),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 1),
('Nemesis', 'nemesis', 5800, 'motorcycles', 1),
('Neon', 'neon', 1500000, 'sports', 1),
('Nightblade', 'nightblade', 35000, 'motorcycles', 1),
('Nightshade', 'nightshade', 65000, 'muscle', 1),
('9F', 'ninef', 65000, 'sports', 1),
('9F Cabrio', 'ninef2', 80000, 'sports', 1),
('Omnis', 'omnis', 35000, 'sports', 1),
('Oppressor', 'oppressor', 3524500, 'super', 1),
('Oracle XS', 'oracle2', 35000, 'coupes', 1),
('Osiris', 'osiris', 160000, 'super', 1),
('Panto', 'panto', 10000, 'compacts', 1),
('Paradise', 'paradise', 19000, 'vans', 1),
('Pariah', 'pariah', 1420000, 'sports', 1),
('Patriot', 'patriot', 55000, 'suvs', 1),
('PCJ-600', 'pcj', 6200, 'motorcycles', 1),
('Penumbra', 'penumbra', 28000, 'sports', 1),
('Pfister', 'pfister811', 85000, 'super', 1),
('Phoenix', 'phoenix', 12500, 'muscle', 1),
('Picador', 'picador', 18000, 'muscle', 1),
('Pigalle', 'pigalle', 20000, 'sportsclassics', 1),
('Prairie', 'prairie', 12000, 'compacts', 1),
('Premier', 'premier', 8000, 'sedans', 1),
('Primo Custom', 'primo2', 14000, 'sedans', 1),
('X80 Proto', 'prototipo', 2500000, 'super', 1),
('qrv', 'qrv', 1, 'emergency', 0),
('Radius', 'radi', 29000, 'suvs', 1),
('raiden', 'raiden', 1375000, 'sports', 1),
('Rapid GT', 'rapidgt', 35000, 'sports', 1),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 1),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 1),
('Reaper', 'reaper', 150000, 'importcars', 0),
('Rebel', 'rebel2', 35000, 'offroad', 1),
('Regina', 'regina', 5000, 'sedans', 1),
('Retinue', 'retinue', 615000, 'sportsclassics', 1),
('Revolter', 'revolter', 1610000, 'sports', 1),
('riata', 'riata', 380000, 'offroad', 1),
('Rocoto', 'rocoto', 45000, 'suvs', 1),
('Ruffian', 'ruffian', 6800, 'motorcycles', 1),
('Ruiner 2', 'ruiner2', 5745600, 'muscle', 1),
('Rumpo', 'rumpo', 15000, 'vans', 1),
('Rumpo Trail', 'rumpo3', 19500, 'vans', 1),
('Sabre Turbo', 'sabregt', 20000, 'muscle', 1),
('Sabre GT', 'sabregt2', 25000, 'muscle', 1),
('Sanchez', 'sanchez', 5300, 'motorcycles', 1),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 1),
('Sanctus', 'sanctus', 25000, 'motorcycles', 1),
('Sandking', 'sandking', 55000, 'offroad', 1),
('Savestra', 'savestra', 990000, 'sportsclassics', 1),
('SC 1', 'sc1', 1603000, 'super', 1),
('Schafter', 'schafter2', 25000, 'sedans', 1),
('Schafter V12', 'schafter3', 50000, 'sports', 1),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles', 1),
('Seminole', 'seminole', 25000, 'suvs', 1),
('Sentinel', 'sentinel', 32000, 'coupes', 1),
('Sentinel XS', 'sentinel2', 40000, 'coupes', 1),
('Sentinel3', 'sentinel3', 650000, 'sports', 1),
('Seven 70', 'seven70', 39500, 'sports', 1),
('ETR1', 'sheava', 220000, 'importcars', 0),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 1),
('Slam Van', 'slamvan3', 11500, 'muscle', 1),
('Indian Chief', 'slave', 46000, 'importbikes', 0),
('Sovereign', 'sovereign', 22000, 'motorcycles', 1),
('Spirit', 'spirit', 15000, 'imports', 1),
('Stinger', 'stinger', 80000, 'sportsclassics', 1),
('Stinger GT', 'stingergt', 75000, 'sportsclassics', 1),
('Streiter', 'streiter', 500000, 'sports', 1),
('Stretch', 'stretch', 90000, 'sedans', 1),
('Stromberg', 'stromberg', 3185350, 'sports', 1),
('Sultan', 'sultan', 15000, 'sports', 1),
('Sultan RS', 'sultanrs', 65000, 'importcars', 0),
('Super Diamond', 'superd', 130000, 'sedans', 1),
('Surano', 'surano', 50000, 'sports', 1),
('Surfer', 'surfer', 12000, 'vans', 1),
('T20', 't20', 300000, 'super', 1),
('Tailgater', 'tailgater', 30000, 'sedans', 1),
('Tampa', 'tampa', 16000, 'muscle', 1),
('Drift Tampa', 'tampa2', 80000, 'importcars', 0),
('Thrust', 'thrust', 24000, 'motorcycles', 1),
('Tow Truck', 'towtruck', 30000, 'utility', 1),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles', 1),
('Trophy Truck', 'trophytruck', 60000, 'offroad', 1),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 1),
('Tropos', 'tropos', 40000, 'sports', 1),
('Turismo R', 'turismor', 350000, 'super', 1),
('Tyrus', 'tyrus', 600000, 'super', 1),
('Vacca', 'vacca', 120000, 'super', 1),
('Vader', 'vader', 7200, 'motorcycles', 1),
('Verlierer', 'verlierer2', 70000, 'sports', 1),
('Vigero', 'vigero', 12500, 'muscle', 1),
('Virgo', 'virgo', 14000, 'muscle', 1),
('Viseris', 'viseris', 875000, 'sportsclassics', 1),
('Visione', 'visione', 2250000, 'super', 1),
('Voltic', 'voltic', 90000, 'super', 1),
('Voltic 2', 'voltic2', 3830400, 'super', 1),
('Voodoo', 'voodoo', 7200, 'muscle', 1),
('Vortex', 'vortex', 9800, 'motorcycles', 1),
('Warrener', 'warrener', 4000, 'sedans', 1),
('Washington', 'washington', 9000, 'sedans', 1),
('Windsor', 'windsor', 95000, 'coupes', 1),
('Windsor Drop', 'windsor2', 125000, 'importcars', 0),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 1),
('XLS', 'xls', 32000, 'suvs', 1),
('Yosemite', 'yosemite', 485000, 'muscle', 1),
('Youga', 'youga', 10800, 'vans', 1),
('Youga Luxuary', 'youga2', 14500, 'vans', 1),
('Z190', 'z190', 900000, 'sportsclassics', 1),
('Zentorno', 'zentorno', 1500000, 'super', 1),
('Zion', 'zion', 36000, 'coupes', 1),
('Zion Cabrio', 'zion2', 45000, 'coupes', 1),
('Zombie', 'zombiea', 9500, 'motorcycles', 1),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 1),
('Z-Type', 'ztype', 220000, 'sportsclassics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_display`
--

CREATE TABLE `vehicles_display` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT 10,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicles_display`
--

INSERT INTO `vehicles_display` (`ID`, `name`, `model`, `profit`, `price`) VALUES
(1, 'Windsor Drop', 'windsor2', 10, 125000),
(2, 'Reaper', 'reaper', 10, 150000),
(3, 'Hell Cat', 'hellcatlb', 10, 65000),
(4, 'Road Glide', 'foxharley2', 10, 30000),
(5, 'Road King', 'foxharley1', 10, 23000),
(6, 'Indian Chief', 'slave', 10, 46000);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'emergency'),
('importbikes', 'Import Bikes'),
('importcars', 'Import Cars'),
('imports', 'imports'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('utility', 'Utility'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 1000),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 200),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 150),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 60),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 35),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 75),
(9, 'GunShop', 'WEAPON_BAT', 30),
(10, 'BlackWeashop', 'WEAPON_BAT', 5),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 10000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 18000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 24000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 26000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 50),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 5),
(25, 'GunShop', 'WEAPON_BALL', 15),
(26, 'BlackWeashop', 'WEAPON_BALL', 2),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 46),
(28, 'GunShop', 'WEAPON_PISTOL50', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`),
  ADD KEY `index_addon_account_data_account_name` (`account_name`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`),
  ADD KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`),
  ADD KEY `index_addon_inventory_inventory_name` (`inventory_name`);

--
-- Indexes for table `baninfo`
--
ALTER TABLE `baninfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banlist`
--
ALTER TABLE `banlist`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `businesses`
--
ALTER TABLE `businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `character_inventory`
--
ALTER TABLE `character_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_datastore_data_name_owner` (`name`,`owner`),
  ADD KEY `index_datastore_data_name` (`name`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_vehicles`
--
ALTER TABLE `dock_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nitro_vehicles`
--
ALTER TABLE `nitro_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `vehsowned` (`owner`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baninfo`
--
ALTER TABLE `baninfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `businesses`
--
ALTER TABLE `businesses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `character_inventory`
--
ALTER TABLE `character_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1295;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dock_vehicles`
--
ALTER TABLE `dock_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=899;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
