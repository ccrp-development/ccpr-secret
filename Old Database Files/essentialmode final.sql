-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2020 at 02:43 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `essentialmode`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1),
(33, 'society_admin', 'admin', 1),
(34, 'society_biker', 'Biker', 1),
(35, 'society_cardealer', 'Car Dealer', 1),
(36, 'society_biker', 'Biker', 1),
(37, 'society_gitrdone', 'GrD Construction', 1),
(38, 'society_parking', 'Parking Enforcement', 1),
(39, 'vault_black_money', 'Money Vault', 1),
(40, 'society_police_black_money', 'Police Black Money', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_ambulance', 50, NULL),
(2, 'society_police', 211000, NULL),
(3, 'society_mecano', 0, NULL),
(4, 'society_taxi', 0, NULL),
(5, 'society_fire', 0, NULL),
(6, 'society_airlines', 0, NULL),
(7, 'society_mafia', 0, NULL),
(8, 'society_rebel', 0, NULL),
(9, 'society_unicorn', 0, NULL),
(10, 'society_dock', 0, NULL),
(11, 'society_avocat', 0, NULL),
(12, 'society_irish', 0, NULL),
(13, 'society_rodriguez', 0, NULL),
(14, 'society_bishops', 0, NULL),
(15, 'society_bountyhunter', 0, NULL),
(16, 'society_dismay', 0, NULL),
(17, 'society_grove', 0, NULL),
(18, 'society_foodtruck', 0, NULL),
(19, 'society_vagos', 0, NULL),
(20, 'society_ballas', 0, NULL),
(21, 'society_carthief', 0, NULL),
(22, 'society_realestateagent', 0, NULL),
(23, 'society_admin', 0, NULL),
(24, 'society_biker', 5000000, NULL),
(25, 'society_cardealer', 99495, NULL),
(26, 'caution', 0, 'steam:110000132580eb0'),
(27, 'property_black_money', 0, 'steam:110000132580eb0'),
(28, 'caution', 0, 'steam:11000010a01bdb9'),
(29, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(30, 'caution', 0, 'steam:11000010ddba29f'),
(31, 'property_black_money', 0, 'steam:11000010ddba29f'),
(32, 'property_black_money', 0, 'steam:110000112969e8f'),
(33, 'caution', 0, 'steam:110000112969e8f'),
(34, 'property_black_money', 0, 'steam:1100001068ef13c'),
(35, 'caution', 0, 'steam:1100001068ef13c'),
(36, 'caution', 0, 'steam:11000010c2ebf86'),
(37, 'property_black_money', 0, 'steam:11000010c2ebf86'),
(38, 'property_black_money', 0, 'steam:11000010a078bc7'),
(39, 'caution', 0, 'steam:11000010a078bc7'),
(40, 'society_gitrdone', 0, NULL),
(41, 'society_parking', 0, NULL),
(42, 'caution', 0, 'steam:11000013d51c2ff'),
(43, 'property_black_money', 0, 'steam:11000013d51c2ff'),
(44, 'caution', 0, 'steam:11000011a2fc3d0'),
(45, 'property_black_money', 0, 'steam:11000011a2fc3d0'),
(46, 'caution', 0, 'steam:1100001048af48f'),
(47, 'property_black_money', 0, 'steam:1100001048af48f'),
(48, 'caution', 0, 'steam:11000013bc17e0e'),
(49, 'property_black_money', 0, 'steam:11000013bc17e0e'),
(50, 'caution', 0, 'steam:1100001079cf4ab'),
(51, 'property_black_money', 0, 'steam:1100001079cf4ab'),
(52, 'caution', 0, 'steam:11000013b85ca5f'),
(53, 'property_black_money', 0, 'steam:11000013b85ca5f'),
(54, 'caution', 0, 'steam:110000116c3388b'),
(55, 'property_black_money', 0, 'steam:110000116c3388b'),
(56, 'caution', 0, 'steam:11000010e305422'),
(57, 'property_black_money', 0, 'steam:11000010e305422'),
(58, 'caution', 0, 'steam:110000106e27137'),
(59, 'property_black_money', 0, 'steam:110000106e27137'),
(60, 'caution', 0, 'steam:11000010b1872b5'),
(61, 'property_black_money', 0, 'steam:11000010b1872b5'),
(62, 'caution', 0, 'steam:110000107d0cc24'),
(63, 'property_black_money', 0, 'steam:110000107d0cc24'),
(64, 'caution', 0, 'steam:110000136d5a1bd'),
(65, 'property_black_money', 0, 'steam:110000136d5a1bd'),
(66, 'caution', 0, 'steam:11000010a92984b'),
(67, 'property_black_money', 0, 'steam:11000010a92984b'),
(68, 'caution', 0, 'steam:110000118dd2d76'),
(69, 'property_black_money', 0, 'steam:110000118dd2d76'),
(70, 'caution', 0, 'steam:110000111e8e1e7'),
(71, 'property_black_money', 0, 'steam:110000111e8e1e7'),
(72, 'caution', 0, 'steam:1100001339af9da'),
(73, 'property_black_money', 0, 'steam:1100001339af9da'),
(74, 'caution', 0, 'steam:110000135837b81'),
(75, 'property_black_money', 0, 'steam:110000135837b81'),
(76, 'caution', 0, 'steam:1100001157932f6'),
(77, 'property_black_money', 0, 'steam:1100001157932f6'),
(78, 'caution', 0, 'steam:11000010ced5d9e'),
(79, 'property_black_money', 0, 'steam:11000010ced5d9e'),
(80, 'caution', 0, 'steam:11000013b83354d'),
(81, 'property_black_money', 0, 'steam:11000013b83354d'),
(82, 'caution', 0, 'steam:110000136177a4e'),
(83, 'property_black_money', 0, 'steam:110000136177a4e'),
(84, 'caution', 0, 'steam:110000100a858c4'),
(85, 'property_black_money', 0, 'steam:110000100a858c4'),
(86, 'vault_black_money', 0, NULL),
(87, 'society_police_black_money', 10701, NULL),
(88, 'caution', 0, 'steam:11000010d0d2e73'),
(89, 'property_black_money', 0, 'steam:11000010d0d2e73'),
(90, 'caution', 0, 'steam:110000114917791'),
(91, 'property_black_money', 0, 'steam:110000114917791'),
(92, 'caution', 0, 'steam:110000132e9c0db'),
(93, 'property_black_money', 0, 'steam:110000132e9c0db'),
(94, 'caution', 0, 'steam:110000105bca616'),
(95, 'property_black_money', 0, 'steam:110000105bca616'),
(96, 'caution', 0, 'steam:11000013e5f9bf9'),
(97, 'property_black_money', 0, 'steam:11000013e5f9bf9'),
(98, 'caution', 0, 'steam:1100001045201b3'),
(99, 'property_black_money', 0, 'steam:1100001045201b3');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_mecano', 'Mechanic', 1),
(3, 'society_taxi', 'Taxi', 1),
(5, 'property', 'Property', 0),
(6, 'society_fire', 'fire', 1),
(7, 'society_airlines', 'Airlines', 1),
(8, 'society_mafia', 'Mafia', 1),
(9, 'society_citizen', 'Mafia', 1),
(10, 'society_rebel', 'Rebel', 1),
(11, 'society_unicorn', 'Unicorn', 1),
(12, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(13, 'society_unicorn', 'Unicorn', 1),
(14, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(15, 'society_dock', 'Marina', 1),
(16, 'society_avocat', 'Avocat', 1),
(17, 'society_irish', 'Irish', 1),
(18, 'society_rodriguez', 'Rodriguez', 1),
(19, 'society_avocat', 'Avocat', 1),
(20, 'society_bishops', 'Bishops', 1),
(21, 'society_irish', 'Irish', 1),
(22, 'society_bountyhunter', 'Bountyhunter', 1),
(23, 'society_dismay', 'Dismay', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_grove', 'Grove', 1),
(26, 'society_vagos', 'Vagos', 1),
(27, 'society_ballas', 'Ballas', 1),
(28, 'society_carthief', 'Car Thief', 1),
(29, 'society_admin', 'admin', 1),
(30, 'society_biker', 'Biker', 1),
(31, 'society_cardealer', 'Car Dealer', 1),
(32, 'society_biker', 'Biker', 1),
(33, 'society_gitrdone', 'GrD Construction', 1),
(34, 'vault', 'Vault', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `baninfo`
--

CREATE TABLE `baninfo` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin DEFAULT 'no info',
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT 'no info',
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT 'no info',
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT '0.0.0.0',
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT 'no info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `baninfo`
--

INSERT INTO `baninfo` (`id`, `identifier`, `license`, `liveid`, `xblid`, `discord`, `playerip`, `playername`) VALUES
(1, 'steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 'live:844427762648155', 'xbl:2533274970162138', 'discord:316956589259096065', 'ip:108.39.247.113', 'stickybombz'),
(2, 'steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 'no info', 'no info', 'discord:416744619162730496', 'ip:174.23.155.211', 'K9Marine'),
(3, 'steam:110000112969e8f', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 'live:985157476597128', 'xbl:2535467589236859', 'discord:197754376645902338', 'ip:136.35.33.33', 'Super'),
(4, 'steam:1100001048af48f', 'license:153aa3e5d9507effcabab9417a1d004ac511b461', 'no info', 'no info', 'no info', 'ip:50.107.157.70', 'VaderSanchez'),
(5, 'steam:11000013bc17e0e', 'license:ca6024211ff0ae80db75aae57447e9ae5424aa8b', 'live:844429205478254', 'xbl:2535412874003034', 'discord:556129511037730818', 'ip:2.61.119.240', 'Police Los Angeles'),
(6, 'steam:1100001068ef13c', 'license:a4979e4221783962685bb8a6105e2b93fc364e77', 'live:1055518641986243', 'xbl:2535405567256855', 'discord:372129901991559169', 'ip:173.72.175.244', 'Soft-Hearted Devil'),
(7, 'steam:1100001079cf4ab', 'license:a179630c24d11ca0c145594e3a475486473db24e', 'live:914798072622782', 'xbl:2535455193687852', 'no info', 'ip:66.24.225.175', 'yankee930'),
(8, 'steam:11000010a078bc7', 'license:00eb03a770b9661e961de7dd80810b9a2656c8cd', 'live:1688853852909340', 'no info', 'discord:313235445549105152', 'ip:76.102.73.195', '414 - Michael G.'),
(9, 'steam:11000010c2ebf86', 'license:58dec6dbc91fa2da51ac590892604b46a231610a', 'live:1899947113129119', 'xbl:2535417591160549', 'discord:187593248951238656', 'ip:67.61.230.198', 'Noss'),
(10, 'steam:11000010ddba29f', 'license:50280a9855e0a77982745eecb274fb074ce3bd7e', 'live:844428293110174', 'xbl:2535466272047308', 'discord:196189496922865664', 'ip:73.130.28.22', 'EONeillYT'),
(11, 'steam:11000013b85ca5f', 'license:e4e17ebd082256997566b01a196bcd73c5d640d3', 'live:1055520843344299', 'xbl:2533274931796243', 'discord:338438567506214912', 'ip:68.97.1.114', 'Abortion Completus'),
(12, 'steam:110000116c3388b', 'license:b29ae5a8518d336b2206291ad278e896a0f62eff', 'live:312711582677323', 'xbl:2535438774618425', 'no info', 'ip:24.191.63.194', 'aiden'),
(13, 'steam:11000010e305422', 'license:ddfb551aea86f87da6ee683c8ea988b25addb242', 'no info', 'no info', 'discord:135178698038050816', 'ip:71.90.122.24', 'Chad'),
(14, 'steam:110000106e27137', 'license:e9f42c40f4e5bf45aed9dacdd43d9ea36a3c64c8', 'no info', 'no info', 'no info', 'ip:94.244.66.21', 'Game_Over'),
(15, 'steam:11000010b1872b5', 'license:0ec05580af4487b67ad4b00a98490acf1c789d86', 'live:844428048977762', 'xbl:2535442411247336', 'discord:494638676077182986', 'ip:191.176.185.33', 'Naramy'),
(16, 'steam:110000107d0cc24', 'license:b2aab97ba1d8664223231aafce8e6aa1d7c58adb', 'live:1055521324154791', 'xbl:2533274967890191', 'no info', 'ip:76.226.177.133', 'Salty Vet'),
(17, 'steam:110000136d5a1bd', 'license:9502f3b2f1bf1222b103a7182c8016644ff9035a', 'live:1055518392661167', 'no info', 'discord:432390725951291402', 'ip:74.132.233.222', 'Deadline Plays'),
(18, 'steam:11000010a92984b', 'license:f621c182818ed82de3ae135051a5720394cc87ff', 'no info', 'no info', 'discord:251031115102420992', 'ip:68.10.180.80', 'Erik M. 2L-354'),
(19, 'steam:110000118dd2d76', 'license:fde9971e001f43afb8c2737b9deddb41decff6d0', 'live:1055518483739792', 'xbl:2535473309774148', 'discord:302173905479794689', 'ip:67.55.240.209', 'the_gta_killerv'),
(20, 'steam:110000111e8e1e7', 'license:f621c182818ed82de3ae135051a5720394cc87ff', 'no info', 'no info', 'discord:251031115102420992', 'ip:68.10.180.80', 'Erik M. P-21'),
(21, 'steam:1100001339af9da', 'license:2d546c5899826ec35a7da8005ebf1189475faf96', 'live:914798509111527', 'xbl:2535440859136570', 'no info', 'ip:76.188.214.205', 'devcole94'),
(22, 'steam:110000135837b81', 'license:a8183dd6814d40e6469dba7693a5816a6a3e8209', 'live:914801382008973', 'xbl:2535456297922904', 'no info', 'ip:173.72.175.244', 'Pacotaco627'),
(23, 'steam:1100001157932f6', 'license:8f3768cd8b3733c1d1dd4808b1fe03eb3fbb84cc', 'live:914802190228034', 'xbl:2535431010137930', 'no info', 'ip:92.111.242.14', 'gielbazelmans1'),
(24, 'steam:11000010ced5d9e', 'license:aba5f93eddc14939daf2611f210fa9dd354db459', 'live:362084445588953', 'xbl:2535440229869954', 'no info', 'ip:83.233.37.144', 'Ice teeth'),
(25, 'steam:11000013b83354d', 'license:a557154e37603171c47c0b756a1ef92f7c72cf70', 'no info', 'no info', 'no info', 'ip:188.167.200.229', 'LUJS'),
(26, 'steam:110000136177a4e', 'license:cc1fb4f4f7ddadf9ac11a23477ceca023b131e15', 'live:985156617976807', 'xbl:2533275011123954', 'discord:374327779325640705', 'ip:99.20.61.19', 'nickbaseball1'),
(27, 'steam:110000100a858c4', 'license:426a58c7bb92e8a849e0a91128f5d37f95a09787', 'live:1055521115070526', 'xbl:2535463590305403', 'no info', 'ip:71.215.1.180', 'Trashkiller'),
(28, 'steam:11000011a2fc3d0', 'license:cbd32fcfb9b1fb3581285f3a55ea11765aafcf21', 'live:985154486298848', 'xbl:2535455373702369', 'discord:567084551508197378', 'ip:107.131.18.5', 'Reaper'),
(29, 'steam:11000010d0d2e73', 'license:32e308bf39c99059c54ad34fbd659d1a6f8d6fe1', 'no info', 'no info', 'discord:176581495769530369', 'ip:208.54.229.89', 'TurtleScrub'),
(30, 'steam:110000114917791', 'license:cd22a3f743bf556f15163036cc9caecb9faf9abb', 'live:844428425649208', 'xbl:2535418497088725', 'discord:257989045345320962', 'ip:65.113.197.21', 'Kyler'),
(31, 'steam:110000132e9c0db', 'license:29f455978c1fa94e90792eab3f8663e1a8550103', 'live:844429164621006', 'xbl:2535424474559144', 'discord:478789108509048843', 'ip:184.97.145.228', 'lbenne365'),
(32, 'steam:110000105bca616', 'license:485ef5249c0bb0440309b9db23b3017b1c8a6158', 'no info', 'no info', 'discord:149347927792287744', 'ip:173.163.17.105', 'Mattison | 333'),
(33, 'steam:11000013e5f9bf9', 'license:e395e4e431532475752c51e031992b36194f46f2', 'live:914801195197016', 'xbl:2535460588540849', 'discord:468554162096046101', 'ip:70.15.141.2', 'mrblitzer72'),
(34, 'steam:1100001045201b3', 'license:8fc00f967a893f9c1e6bc75e6f3f9807cb4a612a', 'live:1899946044892869', 'xbl:2533274890204239', 'discord:229297496105418753', 'ip:160.2.63.199', 'S. Vance | S17 (Brodsort)');

-- --------------------------------------------------------

--
-- Table structure for table `banlist`
--

CREATE TABLE `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `banlisthistory`
--

CREATE TABLE `banlisthistory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `banlisthistory`
--

INSERT INTO `banlisthistory` (`id`, `identifier`, `license`, `liveid`, `xblid`, `discord`, `playerip`, `targetplayername`, `sourceplayername`, `reason`, `timeat`, `added`, `expiration`, `permanent`) VALUES
(1, 'steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 'no info', 'no info', 'discord:416744619162730496', 'ip:174.23.169.31', 'K9Marine', 'stickybombz', 'testing', 1572762811, 'Sun Nov  3 01:33:31 2019', 1572849211, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `ban_issued` varchar(50) NOT NULL,
  `banned_until` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `Column 9` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bans`
--

INSERT INTO `bans` (`ID`, `name`, `identifier`, `reason`, `ban_issued`, `banned_until`, `staff_name`, `staff_steamid`, `Column 9`) VALUES
(1, 'reaper', 'steam:11000011a2fc3d0', 'potential hacker', '11/2/2019', '11/2/2029', 'stickybombz', 'steam:11000010a01bdb9', ''),
(2, 'stickybombz', 'steam:11000010a01bdb9', 'testing', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT -1,
  `leo_rank` int(11) DEFAULT -1,
  `tow_rank` int(11) DEFAULT -1,
  `admin_rank` int(11) DEFAULT -1,
  `biker_rank` int(11) DEFAULT -1,
  `offdutyleo_rank` int(11) DEFAULT -1,
  `offdutyems_rank` int(11) DEFAULT -1,
  `fire_rank` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `admin_rank`, `biker_rank`, `offdutyleo_rank`, `offdutyems_rank`, `fire_rank`) VALUES
('steam:110000118dd2d76', 'Dillon', 'Wells', '12271992', 'M', '72', -1, -1, -1, -1, -1, -1, -1, -1),
('steam:110000112969e8f', 'Steven', 'Super', '05/08/2000', 'M', '90', -1, -1, -1, -1, -1, -1, -1, -1),
('steam:110000132580eb0', 'Jak', 'Fulton', '10-10-1988', 'M', '74', -1, -1, -1, -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commend`
--

CREATE TABLE `commend` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `community_name` varchar(50) NOT NULL,
  `discord_webhook` varchar(50) NOT NULL,
  `joinmessage` enum('T','F') NOT NULL,
  `chatcommands` enum('T','F') NOT NULL,
  `checktimeout` int(11) NOT NULL,
  `trustscore` int(11) NOT NULL,
  `tswarn` int(11) NOT NULL,
  `tskick` int(11) NOT NULL,
  `tsban` int(11) NOT NULL,
  `tscommend` int(11) NOT NULL,
  `tstime` int(11) NOT NULL,
  `recent_time` int(11) NOT NULL,
  `permissions` varchar(50) NOT NULL,
  `serveractions` varchar(50) NOT NULL,
  `debug` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'user_mask', 'Masque', 0),
(3, 'property', 'Property', 0),
(4, 'society_fire', 'fire', 1),
(5, 'society_mafia', 'Mafia', 1),
(7, 'society_rebel', 'Rebel', 1),
(8, 'society_unicorn', 'Unicorn', 1),
(9, 'society_unicorn', 'Unicorn', 1),
(10, 'society_avocat', 'Avocat', 1),
(11, 'society_irish', 'Irish', 1),
(12, 'society_rodriguez', 'Rodriguez', 1),
(13, 'society_avocat', 'Avocat', 1),
(14, 'society_bishops', 'Bishops', 1),
(15, 'society_irish', 'Irish', 1),
(16, 'society_bountyhunter', 'Bountyhunter', 1),
(17, 'society_dismay', 'Dismay', 1),
(18, 'society_bountyhunter', 'Bountyhunter', 1),
(19, 'society_grove', 'Grove', 1),
(20, 'society_vagos', 'Vagos', 1),
(21, 'society_ballas', 'Ballas', 1),
(22, 'society_carthief', 'Car Thief', 1),
(23, 'society_admin', 'admin', 1),
(24, 'society_biker', 'Biker', 1),
(25, 'society_biker', 'Biker', 1),
(26, 'society_gitrdone', 'GrD Construction', 1),
(27, 'vault', 'Vault', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_police', NULL, '{}'),
(2, 'society_fire', NULL, '{}'),
(3, 'society_mafia', NULL, '{}'),
(4, 'society_rebel', NULL, '{}'),
(5, 'society_unicorn', NULL, '{}'),
(6, 'society_avocat', NULL, '{}'),
(7, 'society_irish', NULL, '{}'),
(8, 'society_rodriguez', NULL, '{}'),
(9, 'society_bishops', NULL, '{}'),
(10, 'society_bountyhunter', NULL, '{}'),
(11, 'society_dismay', NULL, '{}'),
(12, 'society_grove', NULL, '{}'),
(13, 'society_vagos', NULL, '{}'),
(14, 'society_ballas', NULL, '{}'),
(15, 'society_carthief', NULL, '{}'),
(16, 'society_admin', NULL, '{}'),
(17, 'society_biker', NULL, '{}'),
(32, 'user_mask', 'steam:110000132580eb0', '{}'),
(33, 'property', 'steam:110000132580eb0', '{}'),
(34, 'user_mask', 'steam:110000107e03e6e', '{}'),
(35, 'property', 'steam:110000107e03e6e', '{}'),
(44, 'user_mask', 'steam:11000011a9b2c0c', '{}'),
(45, 'property', 'steam:11000011a9b2c0c', '{}'),
(46, 'user_mask', 'Char1:11000010a01bdb9', '{}'),
(47, 'property', 'Char1:11000010a01bdb9', '{}'),
(48, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(49, 'property', 'steam:11000010a01bdb9', '{}'),
(50, 'property', 'steam:1100001068ef13c', '{}'),
(51, 'user_mask', 'steam:1100001068ef13c', '{}'),
(52, 'user_mask', 'steam:11000010bf93966', '{}'),
(53, 'property', 'steam:11000010bf93966', '{}'),
(54, 'property', 'steam:110000112969e8f', '{}'),
(55, 'user_mask', 'steam:110000112969e8f', '{}'),
(56, 'property', 'steam:110000117b74e38', '{}'),
(57, 'user_mask', 'steam:110000117b74e38', '{}'),
(58, 'property', 'steam:11000010ddba29f', '{}'),
(59, 'user_mask', 'steam:11000010ddba29f', '{}'),
(60, 'property', 'steam:11000010c2ebf86', '{}'),
(61, 'user_mask', 'steam:11000010c2ebf86', '{}'),
(62, 'property', 'steam:11000010a078bc7', '{}'),
(63, 'user_mask', 'steam:11000010a078bc7', '{}'),
(64, 'society_gitrdone', NULL, '{}'),
(65, 'user_mask', 'steam:11000013d51c2ff', '{}'),
(66, 'property', 'steam:11000013d51c2ff', '{}'),
(67, 'user_mask', 'steam:11000011a2fc3d0', '{}'),
(68, 'property', 'steam:11000011a2fc3d0', '{}'),
(69, 'user_mask', 'steam:1100001048af48f', '{}'),
(70, 'property', 'steam:1100001048af48f', '{}'),
(71, 'user_mask', 'steam:11000013bc17e0e', '{}'),
(72, 'property', 'steam:11000013bc17e0e', '{}'),
(73, 'user_mask', 'steam:1100001079cf4ab', '{}'),
(74, 'property', 'steam:1100001079cf4ab', '{}'),
(75, 'user_mask', 'steam:11000013b85ca5f', '{}'),
(76, 'property', 'steam:11000013b85ca5f', '{}'),
(77, 'user_mask', 'steam:110000116c3388b', '{}'),
(78, 'property', 'steam:110000116c3388b', '{}'),
(79, 'user_mask', 'steam:11000010e305422', '{}'),
(80, 'property', 'steam:11000010e305422', '{}'),
(81, 'user_mask', 'steam:110000106e27137', '{}'),
(82, 'property', 'steam:110000106e27137', '{}'),
(83, 'user_mask', 'steam:11000010b1872b5', '{}'),
(84, 'property', 'steam:11000010b1872b5', '{}'),
(85, 'user_mask', 'steam:110000107d0cc24', '{}'),
(86, 'property', 'steam:110000107d0cc24', '{}'),
(87, 'user_mask', 'steam:110000136d5a1bd', '{}'),
(88, 'property', 'steam:110000136d5a1bd', '{}'),
(89, 'user_mask', 'steam:11000010a92984b', '{}'),
(90, 'property', 'steam:11000010a92984b', '{}'),
(91, 'user_mask', 'steam:110000118dd2d76', '{}'),
(92, 'property', 'steam:110000118dd2d76', '{}'),
(93, 'user_mask', 'steam:110000111e8e1e7', '{}'),
(94, 'property', 'steam:110000111e8e1e7', '{}'),
(95, 'user_mask', 'steam:1100001339af9da', '{}'),
(96, 'property', 'steam:1100001339af9da', '{}'),
(97, 'user_mask', 'steam:110000135837b81', '{}'),
(98, 'property', 'steam:110000135837b81', '{}'),
(99, 'user_mask', 'steam:1100001157932f6', '{}'),
(100, 'property', 'steam:1100001157932f6', '{}'),
(101, 'user_mask', 'steam:11000010ced5d9e', '{}'),
(102, 'property', 'steam:11000010ced5d9e', '{}'),
(103, 'user_mask', 'steam:11000013b83354d', '{}'),
(104, 'property', 'steam:11000013b83354d', '{}'),
(105, 'user_mask', 'steam:110000136177a4e', '{}'),
(106, 'property', 'steam:110000136177a4e', '{}'),
(107, 'user_mask', 'steam:110000100a858c4', '{}'),
(108, 'property', 'steam:110000100a858c4', '{}'),
(109, 'vault', NULL, '{}'),
(110, 'user_mask', 'steam:11000010d0d2e73', '{}'),
(111, 'property', 'steam:11000010d0d2e73', '{}'),
(112, 'user_mask', 'steam:110000114917791', '{}'),
(113, 'property', 'steam:110000114917791', '{}'),
(114, 'user_mask', 'steam:110000132e9c0db', '{}'),
(115, 'property', 'steam:110000132e9c0db', '{}'),
(116, 'user_mask', 'steam:110000105bca616', '{}'),
(117, 'property', 'steam:110000105bca616', '{}'),
(118, 'user_mask', 'steam:11000013e5f9bf9', '{}'),
(119, 'property', 'steam:11000013e5f9bf9', '{}'),
(120, 'user_mask', 'steam:1100001045201b3', '{}'),
(121, 'property', 'steam:1100001045201b3', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Toro', 'toro', 2000, 'dock'),
(3, 'Dinghy3', 'dinghy3', 2000, 'dock'),
(4, 'Seashark', 'seashark', 1000, 'dock'),
(5, 'Submarine', 'submersible2', 4000, 'dock');

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `dpkeybinds`
--

CREATE TABLE `dpkeybinds` (
  `id` varchar(50) DEFAULT NULL,
  `keybind1` varchar(50) DEFAULT 'num4',
  `emote1` varchar(255) DEFAULT '',
  `keybind2` varchar(50) DEFAULT 'num5',
  `emote2` varchar(255) DEFAULT '',
  `keybind3` varchar(50) DEFAULT 'num6',
  `emote3` varchar(255) DEFAULT '',
  `keybind4` varchar(50) DEFAULT 'num7',
  `emote4` varchar(255) DEFAULT '',
  `keybind5` varchar(50) DEFAULT 'num8',
  `emote5` varchar(255) DEFAULT '',
  `keybind6` varchar(50) DEFAULT 'num9',
  `emote6` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dpkeybinds`
--

INSERT INTO `dpkeybinds` (`id`, `keybind1`, `emote1`, `keybind2`, `emote2`, `keybind3`, `emote3`, `keybind4`, `emote4`, `keybind5`, `emote5`, `keybind6`, `emote6`) VALUES
('steam:11000010a01bdb9', 'num4', 'cop', 'num5', 'medic', 'num6', 'smoke', 'num7', 'cig', 'num8', '', 'num9', 'salute'),
('steam:110000132580eb0', 'num4', '', 'num5', '', 'num6', '', 'num7', 'cigar2', 'num8', '', 'num9', 'cop2'),
('steam:1100001068ef13c', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000112969e8f', 'num4', 'cop2', 'num5', 'medic', 'num6', 'smoke', 'num7', 'cig', 'num8', '', 'num9', 'salute'),
('steam:110000118dd2d76', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000011a2fc3d0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010ddba29f', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010c2ebf86', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010d0d2e73', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000114917791', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000132e9c0db', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000135837b81', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000105bca616', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013e5f9bf9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001045201b3', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ballas`
--

CREATE TABLE `fine_types_ballas` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_ballas`
--

INSERT INTO `fine_types_ballas` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3),
(8, 'Raket', 3000, 0),
(9, 'Raket', 5000, 0),
(10, 'Raket', 10000, 1),
(11, 'Raket', 20000, 1),
(12, 'Raket', 50000, 2),
(13, 'Raket', 150000, 3),
(14, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bishops`
--

CREATE TABLE `fine_types_bishops` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bishops`
--

INSERT INTO `fine_types_bishops` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_dismay`
--

CREATE TABLE `fine_types_dismay` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_dismay`
--

INSERT INTO `fine_types_dismay` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_grove`
--

CREATE TABLE `fine_types_grove` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_grove`
--

INSERT INTO `fine_types_grove` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_irish`
--

CREATE TABLE `fine_types_irish` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_irish`
--

INSERT INTO `fine_types_irish` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_mafia`
--

CREATE TABLE `fine_types_mafia` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_mafia`
--

INSERT INTO `fine_types_mafia` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rebel`
--

CREATE TABLE `fine_types_rebel` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rebel`
--

INSERT INTO `fine_types_rebel` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rodriguez`
--

CREATE TABLE `fine_types_rodriguez` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rodriguez`
--

INSERT INTO `fine_types_rodriguez` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_vagos`
--

CREATE TABLE `fine_types_vagos` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_vagos`
--

INSERT INTO `fine_types_vagos` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `glovebox_inventory`
--

INSERT INTO `glovebox_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(5, '03HSO812', '{}', 1),
(14, '86TKD185', '{}', 1),
(35, 'TYH 041 ', '{\"weapons\":[{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"ammo\":4500}]}', 1),
(37, '06VHJ277', '{\"coffres\":[{\"count\":1,\"name\":\"licenseplate\"}]}', 1),
(59, '08JTP908', '{}', 1),
(61, '22OAJ106', '{}', 1),
(66, '83DBS050', '{\"coffres\":[]}', 1),
(67, '67BPN934', '{\"coffres\":[]}', 1),
(69, '46DYC179', '{}', 1),
(70, '63RFP608', '{}', 1),
(79, '09VYF811', '{}', 1),
(80, 'L84 ANL ', '{\"weapons\":[{\"name\":\"WEAPON_PISTOL\",\"label\":\"Pistol\",\"ammo\":0},{\"name\":\"WEAPON_PISTOL\",\"label\":\"Pistol\",\"ammo\":36}],\"coffres\":[{\"count\":98,\"name\":\"washed_stone\"}]}', 1),
(115, 'LVP 454 ', '{\"weapons\":[{\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"ammo\":30}]}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`, `price`) VALUES
(1, 'bread', 'Bread', -1, 0, 1, 0),
(2, 'water', 'Water', -1, 0, 1, 0),
(3, 'weed', 'Weed', 0, 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1, 0),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1, 0),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1, 0),
(14, 'fish', 'Fish', -1, 0, 1, 0),
(15, 'stone', 'Stone', -1, 0, 1, 0),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1, 0),
(17, 'copper', 'Copper', -1, 0, 1, 0),
(18, 'iron', 'Iron', -1, 0, 1, 0),
(19, 'gold', 'Gold', -1, 0, 1, 0),
(20, 'diamond', 'Diamond', -1, 0, 1, 0),
(21, 'wood', 'Wood', -1, 0, 1, 0),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1, 0),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1, 0),
(27, 'whool', 'Whool', -1, 0, 1, 0),
(28, 'fabric', 'Fabric', -1, 0, 1, 0),
(29, 'clothe', 'Clothe', -1, 0, 1, 0),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1, 0),
(31, 'fixtool', 'Repair Tools', -1, 0, 1, 0),
(32, 'carotool', 'Tools', -1, 0, 1, 0),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1, 0),
(34, 'fixkit', 'Repair Kit', -1, 0, 1, 0),
(35, 'carokit', 'Body Kit', -1, 0, 1, 0),
(36, 'beer', 'Beer', -1, 0, 1, 0),
(37, 'bandage', 'Bandage', 20, 0, 1, 0),
(38, 'medikit', 'Medikit', 100, 0, 1, 0),
(39, 'pills', 'Pills', 10, 0, 1, 0),
(40, 'lockpick', 'Lockpick', -1, 0, 1, 0),
(41, 'vodka', 'Vodka', -1, 0, 1, 0),
(42, 'coffee', 'Coffee', -1, 0, 1, 0),
(49, 'clip', 'Ammo', -1, 0, 1, 0),
(50, 'jager', 'Jägermeister', 5, 0, 1, 0),
(51, 'vodka', 'Vodka', 5, 0, 1, 0),
(52, 'rhum', 'Rhum', 5, 0, 1, 0),
(53, 'whisky', 'Whisky', 5, 0, 1, 0),
(54, 'tequila', 'Tequila', 5, 0, 1, 0),
(55, 'martini', 'Martini blanc', 5, 0, 1, 0),
(56, 'soda', 'Soda', 5, 0, 1, 0),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1, 0),
(58, 'icetea', 'Ice Tea', 5, 0, 1, 0),
(59, 'energy', 'Energy Drink', 5, 0, 1, 0),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1, 0),
(61, 'limonade', 'Limonade', 5, 0, 1, 0),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1, 0),
(63, 'bolnoixcajou', 'Bowl of Cashews', 5, 0, 1, 0),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1, 0),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1, 0),
(66, 'saucisson', 'Sausage', 5, 0, 1, 0),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1, 0),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1, 0),
(69, 'golem', 'Golem', 5, 0, 1, 0),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1, 0),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1, 0),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1, 0),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1, 0),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1, 0),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1, 0),
(76, 'mojito', 'Mojito', 5, 0, 1, 0),
(77, 'ice', 'Glaçon', 5, 0, 1, 0),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1, 0),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1, 0),
(81, 'menthe', 'Mint leaf', 10, 0, 1, 0),
(82, 'cola', 'Coke', -1, 0, 1, 0),
(105, 'vegetables', 'Vegetables', 20, 0, 1, 0),
(117, 'turtle', 'Turtle', -1, 0, 1, 0),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1, 0),
(119, 'lsd', 'Lsd', -1, 0, 1, 0),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1, 0),
(121, 'pearl', 'Pearl', -1, 0, 1, 0),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1, 0),
(123, 'litter', 'Litter', -1, 0, 1, 0),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1, 0),
(128, 'meat', 'Meat', 20, 0, 1, 0),
(129, 'tacos', 'Tacos', 20, 0, 1, 0),
(130, 'burger', 'Burger', 20, 0, 1, 0),
(131, 'silencieux', 'Siliencer', -1, 0, 1, 0),
(132, 'flashlight', 'Flashlight', -1, 0, 1, 0),
(133, 'grip', 'Grip', -1, 0, 1, 0),
(134, 'yusuf', 'Skin', -1, 0, 1, 0),
(135, 'binoculars', 'Binoculars', 1, 0, 1, 0),
(136, 'croquettes', 'Croquettes', -1, 0, 1, 0),
(137, 'blackberry', 'blackberry', -1, 0, 1, 0),
(138, 'lighter', 'Bic', -1, 0, 1, 0),
(139, 'cigarett', 'Cigarette', -1, 0, 1, 0),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1, 0),
(2010, 'armour', 'Armor', -1, 0, 1, 0),
(2011, 'receipt', 'Receipt', 100, 0, 1, 0),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1, 0),
(2013, 'powerade', 'Powerade', -1, 0, 1, 0),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1, 0),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1, 0),
(2016, 'plongee1', 'Short Dive', -1, 0, 1, 0),
(2017, 'plongee2', 'Long Dive', -1, 0, 1, 0),
(2018, 'contrat', 'Salvage', 15, 0, 1, 0),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1, 0),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1, 0),
(2021, 'meat', 'Meat', -1, 0, 1, 0),
(2022, 'leather', 'Leather', -1, 0, 1, 0),
(2023, 'cannabis', 'Cannabis', 50, 0, 1, 0),
(2024, 'marijuana', 'Marijuana', 250, 0, 1, 0),
(2025, 'coca', 'CocaPlant', 150, 0, 1, 0),
(2026, 'cocaine', 'Coke', 50, 0, 1, 0),
(2027, 'ephedra', 'Ephedra', 100, 0, 1, 0),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1, 0),
(2029, 'poppy', 'Poppy', 100, 0, 1, 0),
(2030, 'opium', 'Opium', 50, 0, 1, 0),
(2031, 'meth', 'Meth', 25, 0, 1, 0),
(2032, 'heroine', 'Heroine', 10, 0, 1, 0),
(2033, 'beer', 'Beer', 30, 0, 1, 0),
(2034, 'tequila', 'Tequila', 10, 0, 1, 0),
(2035, 'vodka', 'Vodka', 10, 0, 1, 0),
(2036, 'whiskey', 'Whiskey', 10, 0, 1, 0),
(2037, 'crack', 'Crack', 25, 0, 1, 0),
(2038, 'drugtest', 'DrugTest', 10, 0, 1, 0),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1, 0),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1, 0),
(2041, 'pcp', 'PCP', 25, 0, 1, 0),
(2042, 'dabs', 'Dabs', 50, 0, 1, 0),
(2043, 'painkiller', 'Painkiller', 10, 0, 1, 0),
(2044, 'narcan', 'Narcan', 10, 0, 1, 0),
(2045, 'boitier', 'Darknet', -1, 0, 1, 0),
(2046, 'cocacola', 'Coca Cola', -1, 0, 1, 0),
(2047, 'fanta', 'Fanta Exotic', -1, 0, 1, 0),
(2048, 'sprite', 'Sprite', -1, 0, 1, 0),
(2049, 'loka', 'Loka Crush', -1, 0, 1, 0),
(2050, 'cheesebows', 'Cheese Doodles', -1, 0, 1, 0),
(2051, 'chips', 'Chips', -1, 0, 1, 0),
(2052, 'marabou', 'Milk Chocolate ', -1, 0, 1, 0),
(2053, 'pizza', 'Kebab Pizza', -1, 0, 1, 0),
(2054, 'baconburger', 'Bacon Burger', -1, 0, 1, 0),
(2055, 'pastacarbonara', 'Pasta Carbonara', -1, 0, 1, 0),
(2056, 'macka', 'Ham sammy', -1, 0, 1, 0),
(2059, 'lotteryticket', 'lottery ticket', -1, 0, 1, 0),
(2060, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1, 0),
(2061, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1, 0),
(2062, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1, 0),
(2063, 'hunting_license', 'Hunting License', -1, 0, 1, 0),
(2064, 'fishing_license', 'Fishing License', -1, 0, 1, 0),
(2065, 'diving_license', 'Diving License', -1, 0, 1, 0),
(2066, 'marriage_license', 'Marriage License', -1, 0, 1, 0),
(2067, 'pilot_license', 'Pilot License', -1, 0, 1, 0),
(2068, 'taxi_license', 'Taxi License', -1, 0, 1, 0),
(2069, 'cdl', 'Commerical Drivers License', -1, 0, 1, 0),
(2070, 'mlic', 'Motorcycle License', -1, 0, 1, 0),
(2071, 'dlic', 'Drivers License', -1, 0, 1, 0),
(2072, 'boating_license', 'Boating License', -1, 0, 1, 0),
(2073, 'firstaidpass', 'First Aid Pass', 1, 0, 0, 0),
(2074, 'nitrocannister', 'NOS', 1, 0, 1, 0),
(2075, 'wrench', 'Wrench', 1, 0, 1, 0),
(2076, 'firstaidkit', 'First Aid Kit', 1, 0, 1, 0),
(2077, 'defibrillateur', 'AED', 1, 0, 1, 0),
(2078, 'radio', 'Radio', 1, 0, 0, 0),
(2079, 'WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1, 0),
(2080, 'WEAPON_STUNGUN', 'Taser', 100, 1, 1, 0),
(2081, 'WEAPON_KNIFE', 'Knife', 100, 1, 1, 0),
(2082, 'WEAPON_BAT', 'Baseball Bat', 1, 0, 1, 0),
(2083, 'WEAPON_PISTOL', 'Pistol', 100, 1, 1, 0),
(2084, 'WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1, 0),
(2085, '9mm_rounds', '9mm Rounds', 20, 0, 1, 0),
(2086, 'shotgun_shells', 'Shotgun Shells', 20, 0, 1, 0),
(2087, 'receipt', 'W.C receipt', 100, 0, 1, 0),
(2088, 'licenseplate', 'License plate', -1, 0, 1, 0),
(2089, 'gps', 'GPS', -1, 0, 1, 0),
(2090, 'petrol', 'Oil', 24, 0, 1, 0),
(2091, 'petrol_raffin', 'Refined Oil', 24, 0, 1, 0),
(2092, 'essence', 'Gas', 24, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'Unemployed', 0),
(3, 'police', 'LEO', 1),
(4, 'slaughterer', 'Slaughterer', 0),
(5, 'fisherman', 'Fisherman', 0),
(6, 'miner', 'Miner', 0),
(7, 'lumberjack', 'Woodcutter', 0),
(8, 'fuel', 'Refiner', 1),
(9, 'reporter', 'Journalist', 0),
(10, 'textil', 'Couturier', 0),
(11, 'mechanic', 'Mechanic', 0),
(12, 'taxi', 'Taxi', 0),
(14, 'trucker', 'Eddies Trucking', 0),
(16, 'fire', 'LFD', 1),
(17, 'deliverer', 'Amazon', 0),
(18, 'brinks', 'Brinks', 0),
(19, 'gopostal', 'GoPostal', 0),
(20, 'airlines', 'Airlines', 1),
(21, 'ambulance', 'AMR', 1),
(22, 'mafia', 'Mafia', 1),
(24, 'rebel', 'Rebel', 1),
(25, 'security', 'Palm City Security', 0),
(28, 'garbage', 'Garbage Driver', 0),
(29, 'pizza', 'Pizzadelivery', 0),
(30, 'ranger', 'parkranger', 1),
(33, 'unicorn', 'Unicorn', 1),
(34, 'bus', 'RTD', 0),
(36, 'coastguard', 'CoastGuard', 1),
(38, 'irish', 'Irish', 1),
(39, 'rodriguez', 'Rodriguez', 1),
(41, 'bishops', 'Bishops', 1),
(42, 'irish', 'Irish', 1),
(43, 'lawyer', 'lawyer', 1),
(45, 'dismay', 'Dismay', 1),
(46, 'bountyhunter', 'Bountyhunter', 1),
(47, 'grove', 'Grove Street Family', 1),
(49, 'vagos', 'Vagos', 1),
(50, 'ballas', 'Ballas', 1),
(51, 'traffic', 'trafficofficer', 1),
(52, 'poolcleaner', 'PoolCleaner', 0),
(53, 'carthief', 'Car Thief', 1),
(54, 'Salvage', 'Salvage', 1),
(55, 'dispatch', 'Dispatch', 1),
(56, 'admin', 'Admin', 1),
(57, 'biker', 'HHMC', 1),
(58, 'cardealer', 'Car Dealer', 1),
(59, 'offpolice', 'Off-Duty LEO', 1),
(60, 'offambulance', 'Off-Duty EMS', 1),
(61, 'biker', 'Biker', 1),
(62, 'windowcleaner', 'Spick N Span', 0),
(63, 'gitrdone', 'GrD Construction', 1),
(64, 'parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(0, 'police', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(1, 'lumberjack', 0, 'employee', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'employee', 'Employee', 250, '{}', '{}'),
(3, 'fueler', 0, 'employee', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'tailor', 0, 'employee', 'Employee', 250, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(6, 'miner', 0, 'employee', 'Employee', 250, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(7, 'slaughterer', 0, 'employee', 'Employee', 250, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(8, 'mechanic', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mechanic', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mechanic', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mechanic', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mechanic', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'pilot', 'Pilot', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'employee', 'Employee', 100, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'dispatch', 0, 'dispatcher', 'Dispatcher', 400, '{}', '{}'),
(78, 'dispatch', 1, 'boss', 'Dispatch Command', 800, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'Sergeant', '(CCPD) Sergeant', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'Sergeant', '(CSP) Sergeant', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'soldato', 'Prospect', 1500, '{}', '{}'),
(164, 'biker', 1, 'capo', 'Chapter member', 1800, '{}', '{}'),
(165, 'biker', 2, 'consigliere', 'Nomad', 2100, '{}', '{}'),
(166, 'biker', 3, 'boss', 'President', 10000, '{}', '{}'),
(167, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(168, 'cardealer', 0, 'recruit', 'Recruit', 100, '{}', '{}'),
(169, 'cardealer', 1, 'novice', 'Novice', 250, '{}', '{}'),
(170, 'cardealer', 2, 'experienced', 'Experienced', 350, '{}', '{}'),
(171, 'cardealer', 3, 'boss', 'Boss', 500, '{}', '{}'),
(338, 'offpolice', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(339, 'offambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(340, 'offpolice', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(341, 'offpolice', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(342, 'offpolice', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(343, 'offpolice', 4, 'Sergeant', '(CCPD) Sergeant', 250, '{}', '{}'),
(344, 'offpolice', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(345, 'offpolice', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(346, 'offpolice', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(347, 'offpolice', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(348, 'offpolice', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(349, 'offpolice', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(350, 'offpolice', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(351, 'offpolice', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(352, 'offpolice', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(353, 'offpolice', 14, 'reserve', '(ACSO) Reserve Officer', 150, '{}', '{}'),
(354, 'offpolice', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(355, 'offpolice', 16, 'corporal', '(ACSO) Corporal', 250, '{}', '{}'),
(356, 'offpolice', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(357, 'offpolice', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(358, 'offpolice', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(359, 'offpolice', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(360, 'offpolice', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(361, 'offpolice', 22, 'chief', '(ACSO) Chief', 400, '{}', '{}'),
(362, 'offpolice', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(363, 'offpolice', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(364, 'offpolice', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(365, 'offpolice', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(366, 'offpolice', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(367, 'offpolice', 28, 'Sergeant', '(CSP) Sergeant', 250, '{}', '{}'),
(368, 'offpolice', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(369, 'offpolice', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(370, 'offpolice', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(371, 'offpolice', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(372, 'offpolice', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(373, 'offpolice', 34, 'chief', '(CSP) Chief', 400, '{}', '{}'),
(374, 'offpolice', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(375, 'offpolice', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(376, 'offpolice', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(377, 'offambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(378, 'offambulance', 2, 'emr', 'Emergency Medical Response', 150, '{}', '{}'),
(379, 'offambulance', 3, 'emt', 'Emergency Medical Technician', 200, '{}', '{}'),
(380, 'offambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(381, 'offambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(382, 'offambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(383, 'offambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(384, 'offambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(385, 'offambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(386, 'offambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(387, 'offambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(388, 'offambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(389, 'police', 39, 'dispatcher', 'Dispatcher', 400, '{}', '{}'),
(390, 'police', 40, 'dispatch', 'Dispatch Command', 800, '{}', '{}'),
(391, 'windowcleaner', 0, 'cleaner', 'Cleaner', 100, '{}', '{}'),
(392, 'gitrdone', 0, 'laborer', 'Laborer', 60, '{}', '{}'),
(393, 'gitrdone', 1, 'operator', 'Equipment Operator', 60, '{}', '{}'),
(394, 'gitrdone', 2, 'foreman', 'Job Foreman', 85, '{}', '{}'),
(395, 'gitrdone', 3, 'boss', 'Contractor / Owner', 100, '{}', '{}'),
(396, 'parking', 0, 'meter_maid', 'Meter Maid', 1500, '{}', '{}'),
(397, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 2000, '{}', '{}'),
(398, 'parking', 2, 'boss', 'CEO', 2500, '{}', '{}'),
(399, 'offpolice', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(400, 'offpolice', 40, 'dispatch', 'Dispatch Command', 800, '{}', '{}'),
(401, 'police', 41, 'event', 'Event', 0, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kicks`
--

CREATE TABLE `kicks` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `nitro_vehicles`
--

CREATE TABLE `nitro_vehicles` (
  `plate` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicles`
--

CREATE TABLE `old_vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicles`
--

INSERT INTO `old_vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 15000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
(4, 'Chino', 'chino', 15000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 250000, 'muscle'),
(7, 'Dominator', 'dominator', 60000, 'muscle'),
(8, 'Dukes', 'dukes', 28000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
(11, 'Faction', 'faction', 20000, 'muscle'),
(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
(13, 'Faction XL', 'faction3', 40000, 'muscle'),
(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
(16, 'Picador', 'picador', 18000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
(20, 'Tampa', 'tampa', 16000, 'muscle'),
(21, 'Virgo', 'virgo', 14000, 'muscle'),
(22, 'Vigero', 'vigero', 12500, 'muscle'),
(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
(24, 'Blista', 'blista', 42958, 'compacts'),
(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
(26, 'Issi', 'issi2', 10000, 'compacts'),
(27, 'Panto', 'panto', 10000, 'compacts'),
(28, 'Prairie', 'prairie', 12000, 'compacts'),
(29, 'Bison', 'bison', 45000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
(31, 'Burrito', 'burrito3', 19000, 'work'),
(32, 'Burrito', 'gburrito2', 29000, 'vans'),
(33, 'Camper', 'camper', 42000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
(35, 'Journey', 'journey', 6500, 'vans'),
(36, 'Minivan', 'minivan', 13000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
(39, 'Paradise', 'paradise', 19000, 'vans'),
(40, 'Rumpo', 'rumpo', 30000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
(42, 'Surfer', 'surfer', 12000, 'vans'),
(43, 'Youga', 'youga', 10800, 'vans'),
(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
(45, 'Asea', 'asea', 5500, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
(47, 'Emperor', 'emperor', 8500, 'sedans'),
(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
(49, 'Glendale', 'glendale', 6500, 'sedans'),
(50, 'Intruder', 'intruder', 7500, 'sedans'),
(51, 'Premier', 'premier', 8000, 'sedans'),
(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
(53, 'Regina', 'regina', 5000, 'sedans'),
(54, 'Schafter', 'schafter2', 25000, 'sedans'),
(55, 'Stretch', 'stretch', 90000, 'sedans'),
(56, 'Phantom', 'superd', 250000, 'sedans'),
(57, 'MercedezAMG', 'tailgater', 130000, 'modded'),
(58, 'Warrener', 'warrener', 120000, 'sedans'),
(59, 'Washington', 'washington', 90000, 'sedans'),
(60, 'Baller', 'baller2', 40000, 'suvs'),
(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
(63, 'Contender', 'contender', 70000, 'suvs'),
(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
(66, 'Fhantom', 'fq2', 17000, 'suvs'),
(67, 'Grabger', 'granger', 50000, 'suvs'),
(68, 'Gresley', 'gresley', 47500, 'suvs'),
(69, 'Huntley S', 'huntley', 40000, 'suvs'),
(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
(71, 'Mesa', 'mesa', 16000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
(73, 'Patriot', 'patriot', 55000, 'suvs'),
(74, 'Radius', 'radi', 29000, 'suvs'),
(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
(76, 'Seminole', 'seminole', 25000, 'suvs'),
(77, 'XLS', 'xls', 70000, 'suvs'),
(78, 'Btype', 'btype', 62000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
(81, 'Casco', 'casco', 350000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
(83, 'Manana', 'manana', 12800, 'sportsclassics'),
(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 220000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
(89, 'Porsche Classic', 'ztype', 160000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 12000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
(92, 'Blazer', 'blazer', 6500, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
(94, 'Brawler', 'brawler', 75000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
(97, 'Guardian', 'guardian', 45000, 'offroad'),
(98, 'Rebel', 'rebel2', 35000, 'offroad'),
(99, 'Sandking', 'sandking', 55000, 'offroad'),
(100, 'The Liberator', 'monster', 210000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
(104, 'Exemplar', 'exemplar', 132000, 'modded'),
(105, 'Silvia', 'f620', 80000, 'modded'),
(106, 'Felon', 'felon', 42000, 'coupes'),
(107, 'Felon GT', 'felon2', 55000, 'coupes'),
(108, 'Jackal', 'jackal', 38000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
(112, 'Windsor', 'windsor', 350000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 180000, 'coupes'),
(114, 'Zion', 'zion', 40000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 70000, 'coupes'),
(116, '9F', 'ninef', 65000, 'sports'),
(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 125000, 'modded'),
(120, 'Bestia GTS', 'bestiagts', 400000, 'sports'),
(121, 'Buffalo', 'buffalo', 120000, 'modded'),
(122, 'wide body charger', 'buffalo2', 175000, 'modded'),
(123, 'Carbonizzare', 'carbonizzare', 110000, 'modded'),
(124, 'Comet', 'comet2', 200000, 'modded'),
(125, 'Coquette', 'coquette', 65000, 'sports'),
(126, 'Vantage', 'tampa2', 230000, 'sports'),
(127, 'Elegy', 'elegy2', 175000, 'sports'),
(128, 'Feltzer', 'feltzer2', 125000, 'sports'),
(129, 'Furore GT', 'furoregt', 45000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
(133, 'Khamelion', 'khamelion', 38000, 'sports'),
(134, 'Kuruma', 'kuruma', 75000, 'sports'),
(135, 'Lynx', 'lynx', 40000, 'sports'),
(136, 'Mamba', 'mamba', 70000, 'sports'),
(137, 'Massacro', 'massacro', 65000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
(139, 'Omnis', 'omnis', 35000, 'sports'),
(140, 'Penumbra', 'penumbra', 28000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
(143, 'Schafter V12', 'schafter3', 130000, 'sports'),
(144, 'Seven 70', 'seven70', 290000, 'sports'),
(145, 'Sultan', 'sultan', 55000, 'sports'),
(146, 'Surano', 'surano', 50000, 'sports'),
(147, 'Tropos', 'tropos', 95000, 'sports'),
(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
(149, 'Adder', 'adder', 1000000, 'modded'),
(150, 'Banshee 900R', 'banshee2', 350000, 'super'),
(151, 'Bullet', 'bullet', 250000, 'super'),
(152, 'Cheetah', 'cheetah', 500000, 'modded'),
(153, 'Entity XF', 'entityxf', 425000, 'super'),
(154, 'Maserati ET1', 'sheava', 500000, 'super'),
(155, 'FMJ', 'fmj', 185000, 'super'),
(156, 'Infernus', 'infernus', 240000, 'modded'),
(157, 'Osiris', 'osiris', 160000, 'super'),
(158, 'Pfister', 'pfister811', 85000, 'super'),
(159, 'RE-7B', 'le7b', 325000, 'super'),
(160, 'Reaper', 'reaper', 150000, 'super'),
(161, 'Sultan RS', 'sultanrs', 130000, 'super'),
(162, 'T20', 't20', 300000, 'super'),
(163, 'Turismo R', 'turismor', 350000, 'super'),
(164, 'Tyrus', 'tyrus', 600000, 'super'),
(165, 'Vacca', 'vacca', 120000, 'super'),
(166, 'Voltic', 'voltic', 900000, 'super'),
(167, 'X80 Proto', 'prototipo', 950000, 'super'),
(168, 'Zentorno', 'zentorno', 700000, 'super'),
(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
(174, 'BF400', 'bf400', 6500, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
(183, 'Double T', 'double', 28000, 'motorcycles'),
(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 7500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 7500, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 80000, 'motorcycles'),
(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
(207, 'Vader', 'vader', 7200, 'motorcycles'),
(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
(213, 'Ruiner', 'ruiner', 90000, 'muscle'),
(214, 'TunedComet', 'comet3', 300000, 'super'),
(215, 'TunedslamVan', 'slamvan3', 270000, 'muscle'),
(216, 'TunedVirgo', 'virgo3', 220000, 'muscle'),
(217, 'Nero', 'nero', 400000, 'super'),
(218, 'TunedBucaneer', 'buccaneer2', 200000, 'muscle'),
(219, 'TunedChino', 'chino2', 190000, 'muscle'),
(220, 'TunedElegy', 'elegy', 220000, 'sports'),
(221, 'TunedFaction', 'faction2', 190000, 'muscle'),
(222, 'PegassiFCR', 'fcr', 8000, 'motorcycles'),
(223, 'TunedPegassi', 'fcr2', 15000, 'motorcycles'),
(224, 'ProgenItaliGTB', 'italigtb', 260000, 'super'),
(225, 'TunedItaliGTB', 'italigtb2', 320000, 'super'),
(226, 'Tunedminivan', 'minivan2', 160000, 'suvs'),
(227, 'TunedNero', 'nero2', 500000, 'super'),
(228, 'Primo', 'primo', 35000, 'coupes'),
(229, 'DewbaucheeSpecter', 'specter', 220000, 'sports'),
(230, 'Tunedspecter', 'specter2', 290000, 'sports'),
(231, 'TunedVan', 'slamvan2', 25000, 'muscle'),
(232, 'EMSCar', 'emscar', 1000, 'work'),
(233, 'EMSCar2', 'emscar2', 1000, 'work'),
(234, 'EMSVan', 'emsvan', 1000, 'work'),
(235, 'EMSSuv', 'emssuv', 1000, 'work'),
(236, 'Rat Loader', 'ratloader', 10000, 'work'),
(237, 'Sand King2', 'sandking2', 60000, 'offroad'),
(238, 'Sadler', 'sadler', 75000, 'offroad'),
(239, 'Taxi', 'taxi', 15000, 'work'),
(240, 'Rubble', 'rubble', 100000, 'work'),
(241, 'Tour Bus', 'tourbus', 30000, 'work'),
(242, 'Tow Truck', 'towtruck', 25000, 'work'),
(243, 'Flat Bed', 'flatbed', 27000, 'work'),
(244, 'Clown', 'speedo2', 16000, 'work'),
(246, 'Stratum', 'stratum', 80000, 'sports'),
(249, 'Mazda', 'blista3', 40000, 'compacts'),
(250, 'Honda Civic', 'blista2', 30000, 'compacts'),
(251, 'Caddilac', 'buffalo3', 50000, 'sedans'),
(252, 'Golf Green', 'surge', 70000, 'compacts'),
(253, 'Stalion', 'stalion', 60000, 'muscle'),
(254, 'SRT', 'stalion2', 160000, 'muscle'),
(255, 'Mercedez RI', 'serrano', 90000, 'suvs'),
(256, 'Mercedes AMG', 'schafter4', 140000, 'sedans'),
(257, 'BMW M3', 'schafter5', 135000, 'sedans'),
(258, 'BMW M3', 'schwarzer', 150000, 'sedans'),
(259, 'gt500', 'gt500', 45000, 'doomsday'),
(260, 'comet4', 'comet4', 180000, 'doomsday'),
(261, 'comet5', 'comet5', 200000, 'doomsday'),
(263, 'hermes', 'hermes', 60000, 'doomsday'),
(264, 'hustler', 'hustler', 60000, 'doomsday'),
(265, 'kamacho', 'kamacho', 50000, 'doomsday'),
(266, 'neon', 'neon', 100000, 'doomsday'),
(267, 'pariah', 'pariah', 100000, 'doomsday'),
(268, 'raiden', 'raiden', 75000, 'doomsday'),
(269, 'revolter', 'revolter', 75000, 'doomsday'),
(270, 'riata', 'riata', 75000, 'doomsday'),
(271, 'savestra', 'savestra', 75000, 'doomsday'),
(272, 'sc1', 'sc1', 75000, 'doomsday'),
(273, 'streiter', 'streiter', 75000, 'doomsday'),
(274, 'stromberg', 'stromberg', 75000, 'doomsday'),
(275, 'sentinel3', 'sentinel3', 75000, 'doomsday'),
(276, 'viseris', 'viseris', 75000, 'doomsday'),
(277, 'yosemite', 'yosemite', 75000, 'doomsday'),
(278, 'z190', 'z190', 75000, 'doomsday'),
(279, 'Mustang', 'musty5', 90000, 'modded'),
(280, 'Infiniti G37', 'g37cs', 50000, 'modded'),
(281, 'Peugeot 107', 'p107', 30000, 'modded'),
(282, 'Renault Megane', 'renmeg', 70000, 'modded'),
(283, 'Lamborghini Hurricane', 'lh610', 230000, 'modded'),
(284, 'Aston Cygnet', 'cygnet11', 40000, 'modded'),
(285, 'Cadillac CTS', 'cadicts', 55000, 'modded'),
(286, 'Mini John Cooper', 'miniub', 45000, 'modded'),
(287, 'Lotus Espirit V8', 'lev8', 130000, 'modded'),
(288, 'Lambo Veneno', 'lamven', 500000, 'modded'),
(289, 'Nissan GTR SpecV', 'gtrublu', 230000, 'modded'),
(290, 'Genesis', 'genublu', 60000, 'modded'),
(291, 'Porsche Cayman R', 'caymanub', 190000, 'modded'),
(292, 'Porsche 911 GT', '911ublu', 700000, 'modded'),
(293, 'Ferrari Laferrari', 'laferublu', 450000, 'modded'),
(294, 'Mclaren 12c', 'mcublu', 400000, 'modded'),
(295, 'Merc SLR', 'slrublu', 200000, 'modded'),
(297, 'Merc SLS AMG Electric', 'slsublue', 150000, 'modded'),
(298, 'Dodge Charger', 'charublu', 90000, 'modded'),
(299, 'subaru 22b', '22bbublu', 80000, 'modded'),
(300, 'Focus RS', 'focusublu', 70000, 'modded'),
(301, 'Mazda furai', 'furaiub', 350000, 'modded'),
(302, 'Ferrari F50', 'f50ub', 350000, 'modded'),
(303, 'Porsche 550a', 'p550a', 250000, 'modded'),
(304, 'Porsche 959', 'p959', 250000, 'modded'),
(305, 'Porsche 944', 'p944', 180000, 'modded'),
(306, 'dodge Viper', 'vip99', 450000, 'modded'),
(307, 'Mazda Rx8', 'rx8', 50000, 'modded'),
(308, 'Ferrari 599', 'gtbf', 290000, 'modded'),
(309, 'Tesla Roadster', 'tesla11', 55000, 'modded'),
(310, 'Mazda Mx5a', 'mx5a', 45000, 'modded'),
(311, 'toyota Celica', 'celicassi', 48000, 'modded'),
(312, 'Toyota celica T', 'celicassi2', 55000, 'modded'),
(313, 'Aston Martin Vanquish', 'amv12', 280000, 'modded'),
(314, 'Subari WRX STI', 'sti05', 80000, 'modded'),
(315, 'Porsche Panamera', 'panamera', 200000, 'modded'),
(316, 'Ferrari 360', 'f360', 250000, 'modded'),
(317, 'Lambo Mura', 'miura', 230000, 'modded'),
(318, 'chevrolet Corvette', 'zr1c3', 180000, 'modded'),
(319, 'Lambo Gallardo', 'gallardo', 300000, 'modded'),
(320, 'Corvette Stingray', 'vc7', 230000, 'modded'),
(321, 'Ferrari Cali', '2fiftygt', 260000, 'modded'),
(322, 'Mercedz Gullwing', '300gsl', 200000, 'modded'),
(323, 'Aston Martin vantage', 'db700', 140000, 'modded'),
(324, 'shelby cobra', 'cobra', 130000, 'modded'),
(325, 'BMW Z4i', 'z4i', 100000, 'modded'),
(326, 'Lambo Huracan', 'huracan', 240000, 'modded'),
(327, 'Ferrari 812', 'ferrari812', 250000, 'modded'),
(328, 'Lambo Veneno', 'veneno', 350000, 'modded'),
(329, 'Ferrari XXK', 'fxxk16', 300000, 'modded'),
(330, 'LaFerrari 15', 'laferrari15', 400000, 'modded'),
(331, 'Italia 458 LW', 'lw458s', 320000, 'modded'),
(332, 'Lykan', 'lykan', 350000, 'modded'),
(333, 'iTalia 458', 'italia458', 290000, 'modded'),
(334, 'Diablous', 'Diablous', 15000, 'motorcycles'),
(335, 'Diablous 2', 'Diablous2', 17000, 'motorcycles'),
(336, 'Raptor', 'Raptor', 20000, 'motorcycles'),
(337, 'Ratbike', 'Ratbike', 16000, 'motorcycles'),
(338, 'Xa21', 'XA21', 340000, 'super'),
(339, 'Penetrator', 'Penetrator', 300000, 'super'),
(340, 'Gp1', 'GP1', 270000, 'super'),
(341, 'Tempestra', 'Tempesta', 260000, 'super'),
(342, 'Toreo', 'Torero', 250000, 'sportsclassics'),
(343, 'Infernus2', 'Infernus2', 240000, 'sportsclassics'),
(344, 'Savestra', 'Savestra', 240000, 'sportsclassics'),
(345, 'Cheetah2', 'Cheetah2', 200000, 'sportsclassics'),
(346, 'Turismo2', 'Turismo2', 180000, 'sportsclassics'),
(347, 'Viceris', 'Viceris', 190000, 'sportsclassics'),
(348, 'JB700', 'JB700', 190000, 'sportsclassics'),
(349, 'Peyote', 'Peyote', 175000, 'sportsclassics'),
(350, 'Ruston', 'Ruston', 150000, 'sports'),
(351, 'Surge', 'Surge', 45000, 'sedans'),
(353, 'Voltic', 'Voltic2', 4000000, 'super'),
(354, 'Dilettante', 'Dilettante', 50000, 'compacts'),
(355, 'Tornado6', 'Tornado6', 150000, 'muscle'),
(356, 'Gauntlet2', 'Gauntlet2', 100000, 'muscle'),
(357, 'Dominator2', 'Dominator2', 100000, 'muscle'),
(358, 'Hurse', 'Lurcher', 60000, 'muscle'),
(359, 'Vagner', 'Vagner', 250000, 'super'),
(360, 'Austarch', 'Autarch', 230000, 'super'),
(361, 'Tornado5', 'tornado5', 75000, 'muscle'),
(362, 'audi a4', 'asterope', 110000, 'sports'),
(363, 'Merc AMG', 'rmodamgc63', 165000, 'sports'),
(364, 'dodgeCharger', '69charger', 140000, 'muscle'),
(365, 'R35', 'r35', 200000, 'modded'),
(367, 'Mustang', 'mgt', 180000, 'modded'),
(368, 'Cheburek', 'cheburek', 20000, 'assault'),
(369, 'Ellie', 'ellie', 100000, 'assault'),
(370, 'Dommy3', 'dominator3', 110000, 'assault'),
(371, 'Enity2', 'entity2', 200000, 'assault'),
(372, 'Fagi', 'fagaloa', 22000, 'assault'),
(373, 'Flash', 'flashgt', 105000, 'assault'),
(374, 'RS200', 'gb200', 135000, 'assault'),
(375, 'Hotring', 'hotring', 140000, 'assault'),
(376, 'Mini', 'issi3', 18000, 'assault'),
(377, 'Jester3', 'jester3', 85000, 'assault'),
(378, 'Michelle', 'michelli', 41000, 'assault'),
(379, 'Tai', 'taipan', 220000, 'assault'),
(380, 'Tezeract', 'tezeract', 180000, 'assault'),
(381, 'Tyrant', 'tyrant', 220000, 'assault'),
(382, 'Impreza', 'ySbrImpS11', 100000, 'modded'),
(383, 'R35 Skyline', 'r35', 200000, 'sports'),
(384, 'Insurgent', 'insurgent3', 200000, 'modded'),
(385, 'Halftrack', 'halftrack', 200000, 'modded'),
(386, 'Tempa', 'tampa3', 200000, 'modded'),
(387, 'Technical', 'technical3', 200000, 'modded'),
(388, 'Tech2', 'technical2', 200000, 'modded'),
(389, 'Barrage', 'barrage', 200000, 'modded'),
(390, 'Boxville', 'boxville5', 200000, 'modded'),
(391, 'Road Glide', 'foxharley2', 3000, 'motorcycles'),
(392, 'Harley Bobber', 'foxharley1', 3000, 'motorcycles');

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicle_categories`
--

CREATE TABLE `old_vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicle_categories`
--

INSERT INTO `old_vehicle_categories` (`id`, `name`, `label`) VALUES
(1, 'compacts', 'Compacts'),
(2, 'coupes', 'Coupés'),
(3, 'sedans', 'Sedans'),
(4, 'sports', 'Sports'),
(5, 'sportsclassics', 'Sports Classics'),
(6, 'super', 'Super'),
(7, 'muscle', 'Muscle'),
(8, 'offroad', 'Off Road'),
(9, 'suvs', 'SUVs'),
(10, 'vans', 'Vans'),
(11, 'motorcycles', 'Motos'),
(12, 'work', 'Work'),
(13, 'doomsday', 'doomsday'),
(14, 'modded', 'Modded'),
(15, 'assault', 'assault');

-- --------------------------------------------------------

--
-- Table structure for table `outfits`
--

CREATE TABLE `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) NOT NULL COMMENT 'mp_m_freemode_01',
  `face` int(11) NOT NULL COMMENT '0',
  `face_text` int(11) NOT NULL COMMENT '0',
  `hair` int(11) NOT NULL COMMENT '0',
  `pants` int(11) NOT NULL COMMENT '0',
  `pants_text` int(11) NOT NULL COMMENT '0',
  `shoes` int(11) NOT NULL COMMENT '0',
  `shoes_text` int(11) NOT NULL COMMENT '0',
  `torso` int(11) NOT NULL COMMENT '0',
  `torso_text` int(11) NOT NULL COMMENT '0',
  `shirt` int(11) NOT NULL COMMENT '0',
  `shirt_text` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `vehicle` longtext NOT NULL,
  `owner` varchar(60) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the car',
  `garage_name` varchar(50) NOT NULL DEFAULT 'Garage_Centre',
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) NOT NULL DEFAULT 'car',
  `plate` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'car',
  `job` varchar(50) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `jamstate` int(11) NOT NULL DEFAULT 0,
  `finance` int(32) NOT NULL DEFAULT 0,
  `financetimer` int(32) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`vehicle`, `owner`, `stored`, `garage_name`, `fourrieremecano`, `vehiclename`, `plate`, `type`, `job`, `state`, `jamstate`, `finance`, `financetimer`) VALUES
('{\"modRoof\":-1,\"modAirFilter\":-1,\"neonEnabled\":[false,false,false,false],\"modSteeringWheel\":-1,\"modTank\":-1,\"modPlateHolder\":-1,\"modSeats\":-1,\"model\":-1030275036,\"modTrunk\":-1,\"color2\":0,\"wheelColor\":156,\"modAerials\":-1,\"modAPlate\":-1,\"plateIndex\":4,\"modEngine\":-1,\"modHydrolic\":-1,\"modArmor\":-1,\"modTrimB\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"extras\":[],\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"plate\":\"88YRQ205\",\"modShifterLeavers\":-1,\"modSuspension\":-1,\"modTrimA\":-1,\"modLivery\":-1,\"modSpeakers\":-1,\"wheels\":0,\"modDial\":-1,\"dirtLevel\":0.0,\"modStruts\":-1,\"modFrame\":-1,\"modTurbo\":false,\"modTransmission\":-1,\"modWindows\":-1,\"color1\":29,\"modSpoilers\":-1,\"modGrille\":-1,\"modFrontWheels\":-1,\"modBackWheels\":-1,\"neonColor\":[255,0,255],\"pearlescentColor\":28,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"modVanityPlate\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"health\":1000,\"modExhaust\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"modHood\":-1,\"windowTint\":-1,\"modFender\":-1,\"modDoorSpeaker\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'car', '', 'car', NULL, 0, 0, 0, 0),
('{\"modSideSkirt\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modBackWheels\":-1,\"modStruts\":-1,\"health\":1000,\"modAPlate\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"modGrille\":-1,\"modXenon\":false,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modTank\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"pearlescentColor\":36,\"dirtLevel\":9.3065328598022,\"modLivery\":-1,\"model\":-133970931,\"color1\":27,\"wheelColor\":111,\"modSpoilers\":-1,\"modAirFilter\":-1,\"modRightFender\":-1,\"plate\":\"04YDL575\",\"modHorns\":-1,\"modVanityPlate\":-1,\"color2\":0,\"modEngine\":-1,\"extras\":[],\"modAerials\":-1,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"plateIndex\":0,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":6,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modFender\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDial\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'Tommies Bike', '04YDL575', 'car', NULL, 0, 0, 45540, -3088),
('{\"modStruts\":-1,\"modLivery\":-1,\"modPlateHolder\":-1,\"modTank\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"modSpoilers\":-1,\"modExhaust\":-1,\"modSteeringWheel\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modShifterLeavers\":-1,\"wheelColor\":0,\"modOrnaments\":-1,\"modSeats\":-1,\"modBackWheels\":-1,\"modGrille\":-1,\"plateIndex\":0,\"modDashboard\":-1,\"model\":234062309,\"extras\":[],\"modWindows\":-1,\"modSuspension\":-1,\"dirtLevel\":0.46202746033669,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modVanityPlate\":-1,\"modRearBumper\":-1,\"health\":1000,\"color1\":0,\"modRoof\":-1,\"modFrame\":-1,\"modSideSkirt\":-1,\"modEngineBlock\":-1,\"modFender\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modHorns\":-1,\"plate\":\"22OAJ106\",\"modXenon\":false,\"modFrontBumper\":-1,\"modSmokeEnabled\":1,\"modDoorSpeaker\":-1,\"pearlescentColor\":0,\"modTurbo\":false,\"wheels\":7,\"modSpeakers\":-1,\"modArmor\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modHood\":-1,\"color2\":0,\"modBrakes\":-1,\"modFrontWheels\":0,\"modAerials\":-1,\"modAPlate\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"windowTint\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'Reaper Sport', '22OAJ106', 'car', NULL, 0, 0, 0, 0),
('{\"modTrimA\":-1,\"modDial\":-1,\"modOrnaments\":-1,\"modSpeakers\":-1,\"color1\":2,\"modFrontWheels\":-1,\"modEngine\":-1,\"modHorns\":-1,\"modFrame\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modRearBumper\":-1,\"dirtLevel\":4.0,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modVanityPlate\":-1,\"color2\":2,\"health\":1000,\"modWindows\":-1,\"tyreSmokeColor\":[255,255,255],\"plate\":\"26ARK388\",\"model\":1373123368,\"modLivery\":-1,\"plateIndex\":1,\"modTransmission\":-1,\"modBackWheels\":-1,\"modTrimB\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"neonEnabled\":[false,false,false,false],\"modHood\":-1,\"modRoof\":-1,\"modArmor\":-1,\"modTank\":-1,\"windowTint\":-1,\"modXenon\":false,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"modSeats\":-1,\"wheels\":5,\"pearlescentColor\":7,\"modRightFender\":-1,\"modSmokeEnabled\":false,\"modAerials\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"extras\":[],\"modDashboard\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"modGrille\":-1,\"modAPlate\":-1,\"modStruts\":-1,\"wheelColor\":156}', 'steam:110000132e9c0db', 1, 'Garage_Centre', 0, 'car', '26ARK388', 'car', NULL, 0, 0, 0, 0),
('{\"color1\":0,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":[],\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"modTransmission\":2,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":4,\"plateIndex\":0,\"pearlescentColor\":0,\"modTrimA\":-1,\"modBrakes\":2,\"modTank\":-1,\"modSuspension\":3,\"wheelColor\":0,\"color2\":0,\"modArmor\":4,\"health\":1000,\"modLivery\":0,\"modFrontBumper\":-1,\"dirtLevel\":0.66330468654633,\"modAerials\":-1,\"modDashboard\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modArchCover\":-1,\"wheels\":7,\"modTurbo\":1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"model\":1581459400,\"modEngineBlock\":-1,\"modRoof\":-1,\"modHorns\":-1,\"modEngine\":3,\"modFender\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modXenon\":false,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"29YEO656\",\"modShifterLeavers\":-1,\"windowTint\":3}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'car', '29YEO656', 'car', NULL, 0, 0, 0, 0),
('{\"neonColor\":[255,0,255],\"color2\":63,\"modHood\":-1,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modSpeakers\":-1,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modDoorSpeaker\":-1,\"plate\":\"45WEE435\",\"plateIndex\":0,\"modAerials\":-1,\"modBrakes\":-1,\"extras\":{\"12\":false,\"10\":true},\"modSpoilers\":-1,\"wheelColor\":156,\"health\":1000,\"modExhaust\":-1,\"modRearBumper\":-1,\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"color1\":63,\"dirtLevel\":7.0,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modWindows\":-1,\"modAirFilter\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modArmor\":-1,\"modShifterLeavers\":-1,\"modGrille\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modFender\":-1,\"modTurbo\":false,\"modFrontBumper\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"wheels\":7,\"modSmokeEnabled\":false,\"modStruts\":-1,\"modLivery\":-1,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"model\":1737773231,\"modTransmission\":-1,\"modXenon\":false,\"modHorns\":-1,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":5,\"modBackWheels\":-1,\"modSideSkirt\":-1,\"modSeats\":-1,\"modTrimA\":-1,\"modRoof\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', '45WEE435', 'car', NULL, 0, 0, 0, 0),
('{\"modOrnaments\":-1,\"wheelColor\":0,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modDial\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"color1\":112,\"modRightFender\":-1,\"modStruts\":-1,\"model\":-1930048799,\"modAirFilter\":-1,\"extras\":[],\"dirtLevel\":3.2446548938751,\"modSideSkirt\":-1,\"modTrimB\":-1,\"tyreSmokeColor\":[255,255,255],\"modLivery\":-1,\"modGrille\":-1,\"wheels\":7,\"modXenon\":false,\"pearlescentColor\":134,\"modSpoilers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"health\":998,\"modRoof\":-1,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modFender\":-1,\"modEngine\":-1,\"modDoorSpeaker\":-1,\"modHood\":-1,\"plateIndex\":0,\"modExhaust\":-1,\"modTransmission\":-1,\"plate\":\"46DYC179\",\"modAerials\":-1,\"modShifterLeavers\":-1,\"neonEnabled\":[false,false,false,false],\"modTank\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":1,\"modFrontBumper\":-1,\"color2\":112,\"modBrakes\":-1,\"modFrame\":-1,\"modVanityPlate\":-1,\"modSpeakers\":-1,\"modWindows\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Windsor Drop', '46DYC179', 'car', NULL, 0, 0, 123750, 2342),
('{\"bodyHealth\":1000.0,\"fuelLevel\":89.836616516113,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modLivery\":-1,\"modRoof\":-1,\"modDial\":-1,\"modFrontBumper\":-1,\"wheelColor\":12,\"engineHealth\":1000.0,\"model\":-2107990196,\"modFrontWheels\":3,\"modRightFender\":-1,\"modPlateHolder\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modEngineBlock\":-1,\"wheels\":4,\"modGrille\":-1,\"plateIndex\":1,\"modArchCover\":-1,\"modExhaust\":-1,\"pearlescentColor\":13,\"modSpoilers\":-1,\"modAerials\":-1,\"modFender\":-1,\"color1\":61,\"modDashboard\":-1,\"extras\":[],\"modSmokeEnabled\":1,\"modSuspension\":-1,\"windowTint\":1,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modEngine\":3,\"modTrimA\":-1,\"plate\":\"47HIN374\",\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modXenon\":1,\"modHorns\":0,\"modFrame\":-1,\"dirtLevel\":4.2695202827454,\"modSideSkirt\":-1,\"modTrunk\":-1,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modOrnaments\":-1,\"health\":1000,\"color2\":12,\"modAirFilter\":-1,\"modTransmission\":2,\"modSteeringWheel\":-1,\"modRearBumper\":-1}', 'steam:1100001045201b3', 1, 'Garage_Centre', 0, 'car', '47HIN374', 'car', NULL, 0, 0, 40500, 2706),
('{\"color1\":62,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":{\"2\":true,\"1\":false},\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"plateIndex\":4,\"pearlescentColor\":0,\"modTrimA\":-1,\"modBrakes\":-1,\"modTank\":-1,\"modSuspension\":-1,\"wheelColor\":0,\"color2\":118,\"modArmor\":-1,\"health\":1000,\"modLivery\":-1,\"modFrontBumper\":-1,\"dirtLevel\":0.53735888004303,\"modAerials\":-1,\"modDashboard\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modArchCover\":-1,\"wheels\":6,\"modTurbo\":false,\"modGrille\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"model\":305501667,\"modEngineBlock\":-1,\"modRoof\":-1,\"modHorns\":-1,\"modEngine\":-1,\"modFender\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modXenon\":false,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"49VGY104\",\"modShifterLeavers\":-1,\"windowTint\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'Harley 2', '49VGY104', 'car', NULL, 0, 0, 24080, 1370),
('{\"modOrnaments\":-1,\"wheelColor\":0,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modDial\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"color1\":62,\"modRightFender\":-1,\"modStruts\":-1,\"model\":305501667,\"modAirFilter\":-1,\"extras\":{\"1\":true,\"2\":false},\"dirtLevel\":3.3734350204468,\"modSideSkirt\":-1,\"modTrimB\":-1,\"tyreSmokeColor\":[255,255,255],\"modLivery\":-1,\"modGrille\":-1,\"wheels\":6,\"modXenon\":false,\"pearlescentColor\":0,\"modSpoilers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"health\":1000,\"modRoof\":-1,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modFender\":-1,\"modEngine\":-1,\"modDoorSpeaker\":-1,\"modHood\":-1,\"plateIndex\":4,\"modExhaust\":-1,\"modTransmission\":-1,\"plate\":\"60BCJ894\",\"modAerials\":-1,\"modShifterLeavers\":-1,\"neonEnabled\":[false,false,false,false],\"modTank\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":1,\"modFrontBumper\":-1,\"color2\":118,\"modBrakes\":-1,\"modFrame\":-1,\"modVanityPlate\":-1,\"modSpeakers\":-1,\"modWindows\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Road Glide', '60BCJ894', 'car', NULL, 0, 0, 29700, 2342),
('{\"modOrnaments\":-1,\"wheelColor\":0,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modDial\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"color1\":92,\"modRightFender\":-1,\"modStruts\":-1,\"model\":545993358,\"modAirFilter\":-1,\"extras\":{\"3\":false,\"2\":false,\"1\":true},\"dirtLevel\":8.4386205673218,\"modSideSkirt\":-1,\"modTrimB\":-1,\"tyreSmokeColor\":[255,255,255],\"modLivery\":-1,\"modGrille\":-1,\"wheels\":6,\"modXenon\":false,\"pearlescentColor\":0,\"modSpoilers\":-1,\"modArmor\":-1,\"modTrunk\":-1,\"health\":1000,\"modRoof\":-1,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modFender\":-1,\"modEngine\":-1,\"modDoorSpeaker\":-1,\"modHood\":-1,\"plateIndex\":4,\"modExhaust\":-1,\"modTransmission\":-1,\"plate\":\"63RFP608\",\"modAerials\":-1,\"modShifterLeavers\":-1,\"neonEnabled\":[false,false,false,false],\"modTank\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":1,\"modFrontBumper\":-1,\"color2\":104,\"modBrakes\":-1,\"modFrame\":-1,\"modVanityPlate\":-1,\"modSpeakers\":-1,\"modWindows\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Road King', '63RFP608', 'car', NULL, 0, 0, 22770, 2342),
('{\"modTrimB\":-1,\"modGrille\":-1,\"modDial\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":147,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modEngineBlock\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"color2\":27,\"modTrunk\":-1,\"modTank\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modRoof\":-1,\"pearlescentColor\":0,\"extras\":{\"1\":true},\"color1\":6,\"modDashboard\":-1,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modVanityPlate\":-1,\"modLivery\":-1,\"modAPlate\":-1,\"modFrame\":-1,\"modSeats\":-1,\"modWindows\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"plateIndex\":0,\"modPlateHolder\":-1,\"modTransmission\":-1,\"modSpoilers\":-1,\"modSideSkirt\":-1,\"modAirFilter\":-1,\"model\":-1624886479,\"modHood\":-1,\"modStruts\":-1,\"modArmor\":-1,\"wheels\":1,\"modBrakes\":-1,\"health\":1000,\"modAerials\":-1,\"modEngine\":-1,\"modFrontWheels\":-1,\"dirtLevel\":15.0,\"modFender\":-1,\"modShifterLeavers\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"plate\":\"67BPN934\",\"modRightFender\":-1,\"modTurbo\":false,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"modDoorSpeaker\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Hellcat', '67BPN934', 'car', NULL, 0, 0, 0, 0),
('{\"modHorns\":-1,\"plateIndex\":4,\"model\":545993358,\"dirtLevel\":9.0,\"modTank\":-1,\"modHood\":-1,\"modRightFender\":-1,\"modSmokeEnabled\":false,\"modSpeakers\":-1,\"modWindows\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":0,\"modDashboard\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"plate\":\"67VBA069\",\"modBrakes\":-1,\"modFrame\":-1,\"wheelColor\":0,\"modSteeringWheel\":-1,\"wheels\":6,\"modArchCover\":-1,\"modAPlate\":-1,\"modEngine\":-1,\"modSuspension\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"modOrnaments\":-1,\"color1\":92,\"modTrimA\":-1,\"modFender\":-1,\"modAerials\":-1,\"color2\":104,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modArmor\":-1,\"windowTint\":-1,\"health\":1000,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modTrimB\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modXenon\":false,\"modLivery\":-1,\"modTransmission\":-1,\"modFrontBumper\":-1,\"modGrille\":-1,\"modStruts\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"neonEnabled\":[false,false,false,false],\"extras\":{\"2\":true,\"3\":false,\"1\":true},\"modHydrolic\":-1}', 'steam:11000010d0d2e73', 1, 'Garage_Centre', 0, 'car', '67VBA069', 'car', NULL, 0, 0, 0, 0),
('{\"modHood\":-1,\"modAirFilter\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modWindows\":-1,\"modBackWheels\":-1,\"wheels\":3,\"modRightFender\":-1,\"modTurbo\":false,\"health\":992,\"tyreSmokeColor\":[255,255,255],\"modArmor\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modFender\":-1,\"color2\":18,\"pearlescentColor\":160,\"modTank\":-1,\"modSpoilers\":-1,\"modRearBumper\":-1,\"color1\":0,\"plate\":\"80FAS030\",\"windowTint\":4,\"model\":1482913898,\"modSpeakers\":-1,\"dirtLevel\":13.306154251099,\"modRoof\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modSeats\":-1,\"modGrille\":-1,\"modStruts\":-1,\"extras\":[],\"modAPlate\":-1,\"modOrnaments\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":1,\"wheelColor\":156,\"modEngine\":-1,\"modFrontBumper\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modShifterLeavers\":-1,\"modLivery\":-1,\"modFrame\":-1,\"modDashboard\":-1,\"modTrunk\":-1,\"modAerials\":-1,\"modTransmission\":-1,\"modEngineBlock\":-1,\"plateIndex\":2}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', '80FAS030', 'car', NULL, 0, 0, 0, 0),
('{\"pearlescentColor\":156,\"fuelLevel\":79.232009887695,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modDial\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modTransmission\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modTank\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.17351016402245,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modEngine\":3,\"modFender\":-1,\"modPlateHolder\":-1,\"modXenon\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":true,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"modSmokeEnabled\":1,\"modSuspension\":3,\"plateIndex\":4,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modExhaust\":-1,\"modTrimA\":-1,\"plate\":\"Z\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"model\":-2063562682,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modLivery\":1,\"health\":1000,\"color2\":134,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'Hellcat', '81OHZ637', 'car', NULL, 0, 0, 0, 0),
('{\"modRoof\":-1,\"wheelColor\":156,\"health\":1000,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modTank\":-1,\"neonColor\":[255,0,255],\"plate\":\"81TPF399\",\"extras\":[],\"wheels\":5,\"modBrakes\":-1,\"modRightFender\":-1,\"modLivery\":-1,\"pearlescentColor\":7,\"modEngineBlock\":-1,\"modXenon\":false,\"modBackWheels\":-1,\"modFender\":-1,\"modHood\":-1,\"modSmokeEnabled\":false,\"modSpeakers\":-1,\"modAPlate\":-1,\"modSuspension\":-1,\"modSpoilers\":-1,\"modExhaust\":-1,\"modGrille\":-1,\"modAerials\":-1,\"modVanityPlate\":-1,\"modStruts\":-1,\"modSeats\":-1,\"modTransmission\":-1,\"modArmor\":-1,\"modRearBumper\":-1,\"modTrunk\":-1,\"model\":1373123368,\"modOrnaments\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"plateIndex\":1,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modDial\":-1,\"tyreSmokeColor\":[255,255,255],\"color1\":2,\"modDoorSpeaker\":-1,\"modDashboard\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modFrontWheels\":-1,\"color2\":2,\"modTrimA\":-1,\"dirtLevel\":12.0,\"modHydrolic\":-1,\"modTurbo\":false,\"modArchCover\":-1}', 'steam:110000132e9c0db', 1, 'Garage_Centre', 0, 'car', '81TPF399', 'car', NULL, 0, 0, 0, 0),
('{\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modBackWheels\":-1,\"modLivery\":-1,\"neonEnabled\":[false,false,false,false],\"modRoof\":-1,\"windowTint\":-1,\"dirtLevel\":15.0,\"modOrnaments\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modFrame\":-1,\"modRearBumper\":-1,\"modHorns\":-1,\"modEngine\":1,\"modArmor\":-1,\"modEngineBlock\":-1,\"modDial\":-1,\"modSpeakers\":-1,\"modRightFender\":-1,\"color2\":18,\"model\":-133970931,\"neonColor\":[255,0,255],\"modStruts\":-1,\"tyreSmokeColor\":[255,255,255],\"modTransmission\":-1,\"wheels\":6,\"modArchCover\":-1,\"modSmokeEnabled\":1,\"modHydrolic\":-1,\"pearlescentColor\":111,\"extras\":[],\"modHood\":-1,\"color1\":6,\"wheelColor\":111,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modXenon\":false,\"health\":326,\"modSeats\":-1,\"plateIndex\":0,\"modPlateHolder\":-1,\"modWindows\":-1,\"modBrakes\":-1,\"modAPlate\":-1,\"plate\":\"83DBS050\",\"modFender\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"modExhaust\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"modFrontBumper\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"modGrille\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Indian Chief', '83DBS050', 'car', NULL, 0, 0, 0, 0),
('{\"color1\":0,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modFrontBumper\":0,\"modPlateHolder\":-1,\"modExhaust\":0,\"extras\":[],\"modXenon\":1,\"modEngineBlock\":-1,\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"wheelColor\":38,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"plateIndex\":0,\"pearlescentColor\":4,\"modTrimA\":-1,\"modBrakes\":2,\"modTank\":-1,\"modSuspension\":-1,\"modAPlate\":-1,\"modTransmission\":2,\"modArmor\":-1,\"health\":1000,\"modLivery\":-1,\"modSmokeEnabled\":1,\"dirtLevel\":0.0,\"modAerials\":-1,\"modDashboard\":-1,\"modRearBumper\":-1,\"modSideSkirt\":1,\"modStruts\":-1,\"modDoorSpeaker\":-1,\"wheels\":6,\"modVanityPlate\":-1,\"modTurbo\":1,\"modGrille\":-1,\"modHorns\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"color2\":41,\"modRightFender\":-1,\"model\":-1606187161,\"modSpoilers\":-1,\"modRoof\":3,\"modSpeakers\":-1,\"modEngine\":3,\"modFender\":2,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"88NRU848\",\"modShifterLeavers\":-1,\"windowTint\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', '88NRU848', 'car', NULL, 0, 0, 24650, 1766),
('{\"modTrimA\":-1,\"modTurbo\":false,\"health\":1000,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"modOrnaments\":-1,\"extras\":{\"11\":false,\"12\":false,\"9\":false,\"2\":false,\"6\":false,\"5\":false,\"8\":false,\"7\":false,\"10\":false,\"1\":false,\"4\":false,\"3\":false},\"wheels\":0,\"modTank\":-1,\"model\":-1763515681,\"plate\":\"A 2\",\"modArmor\":-1,\"modRearBumper\":-1,\"modLivery\":0,\"modSmokeEnabled\":false,\"pearlescentColor\":134,\"modRoof\":-1,\"modDashboard\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"wheelColor\":134,\"modSteeringWheel\":-1,\"modXenon\":false,\"modAerials\":-1,\"modTransmission\":-1,\"modDoorSpeaker\":-1,\"color1\":134,\"modFrame\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"dirtLevel\":3.0,\"modRightFender\":-1,\"modArchCover\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"color2\":134,\"windowTint\":-1,\"modBrakes\":-1,\"modSuspension\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"plateIndex\":0,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modSpeakers\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"modEngine\":-1,\"modTrimB\":-1,\"neonEnabled\":[false,false,false,false],\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modWindows\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'A 2', 'car', 'police', 0, 0, 0, 0),
('{\"modRoof\":-1,\"modOrnaments\":-1,\"health\":1000,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"wheels\":0,\"modTank\":-1,\"modAirFilter\":-1,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modLivery\":0,\"modSmokeEnabled\":false,\"pearlescentColor\":0,\"modFrame\":-1,\"color2\":0,\"modHood\":-1,\"modDial\":-1,\"wheelColor\":111,\"modTrimA\":-1,\"modXenon\":false,\"modAerials\":-1,\"modGrille\":-1,\"modDoorSpeaker\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"modFender\":-1,\"modStruts\":-1,\"dirtLevel\":0.0,\"modRightFender\":-1,\"modArchCover\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modSpoilers\":-1,\"modArmor\":-1,\"modBrakes\":-1,\"modSteeringWheel\":-1,\"plate\":\"A 2 GH\",\"modBackWheels\":-1,\"modFrontBumper\":-1,\"extras\":{\"11\":false,\"12\":true,\"9\":false,\"2\":false,\"6\":false,\"5\":false,\"8\":false,\"7\":false,\"10\":false,\"1\":false,\"4\":false,\"3\":false},\"color1\":0,\"neonEnabled\":[false,false,false,false],\"windowTint\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modAPlate\":-1,\"modTrimB\":-1,\"modEngine\":-1,\"model\":112512917,\"modExhaust\":-1,\"modWindows\":-1,\"modSpeakers\":-1,\"plateIndex\":4}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'A 2 GH', 'car', 'police', 0, 0, 0, 0),
('{\"modVanityPlate\":-1,\"model\":-61406477,\"modSmokeEnabled\":false,\"health\":1000,\"modSpeakers\":-1,\"wheels\":0,\"modDial\":-1,\"modDoorSpeaker\":-1,\"modLivery\":0,\"wheelColor\":140,\"modTransmission\":-1,\"dirtLevel\":0.0,\"modFrame\":-1,\"extras\":{\"12\":false,\"11\":true,\"2\":false,\"1\":false,\"8\":false,\"7\":false,\"10\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":false},\"plate\":\"A2 E\",\"modExhaust\":-1,\"modArmor\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modGrille\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"modTank\":-1,\"modDashboard\":-1,\"modTrimB\":-1,\"modHood\":-1,\"modTrimA\":-1,\"modEngine\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"color2\":134,\"modAerials\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modRightFender\":-1,\"modStruts\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"windowTint\":-1,\"modHorns\":-1,\"tyreSmokeColor\":[255,255,255],\"modHydrolic\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"color1\":134,\"modXenon\":false,\"modOrnaments\":-1,\"modRearBumper\":-1,\"plateIndex\":4,\"modAPlate\":-1,\"modArchCover\":-1,\"modBrakes\":-1,\"modSideSkirt\":-1,\"modPlateHolder\":-1,\"modFrontBumper\":-1,\"modShifterLeavers\":-1,\"modBackWheels\":-1,\"modWindows\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'A2 E', 'car', 'police', 0, 0, 0, 0),
('{\"modTrimA\":-1,\"modTurbo\":false,\"health\":1000,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"modOrnaments\":-1,\"extras\":{\"11\":false,\"12\":false,\"9\":false,\"2\":false,\"6\":false,\"5\":false,\"8\":false,\"7\":false,\"10\":false,\"1\":false,\"4\":true,\"3\":false},\"wheels\":0,\"modTank\":-1,\"model\":-1059115956,\"plate\":\"A2 GH2\",\"modArmor\":-1,\"modRearBumper\":-1,\"modLivery\":0,\"modSmokeEnabled\":false,\"pearlescentColor\":0,\"modRoof\":-1,\"modDashboard\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"wheelColor\":0,\"modSteeringWheel\":-1,\"modXenon\":false,\"modAerials\":-1,\"modTransmission\":-1,\"modDoorSpeaker\":-1,\"color1\":0,\"modFrame\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"dirtLevel\":0.0,\"modRightFender\":-1,\"modArchCover\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"color2\":0,\"windowTint\":-1,\"modBrakes\":-1,\"modSuspension\":-1,\"modDial\":-1,\"modBackWheels\":-1,\"plateIndex\":4,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modSpeakers\":-1,\"modAirFilter\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"modEngine\":-1,\"modTrimB\":-1,\"neonEnabled\":[false,false,false,false],\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modWindows\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'A2 GH2', 'car', 'police', 0, 0, 0, 0),
('{\"modSuspension\":3,\"modTank\":-1,\"modAPlate\":-1,\"wheelColor\":156,\"modFrontWheels\":-1,\"modFrame\":-1,\"modAerials\":-1,\"modAirFilter\":-1,\"modFrontBumper\":-1,\"color1\":134,\"modSpoilers\":-1,\"modXenon\":1,\"health\":1000,\"modTrunk\":-1,\"modRoof\":-1,\"modGrille\":-1,\"dirtLevel\":0.0058408631011844,\"modEngineBlock\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"plate\":\"A2 GHT\",\"modSmokeEnabled\":1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modBrakes\":2,\"modVanityPlate\":-1,\"wheels\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":false,\"4\":true,\"1\":true,\"2\":false,\"7\":true,\"12\":true,\"9\":true,\"11\":true,\"10\":true,\"8\":true},\"modDial\":-1,\"modTransmission\":2,\"modFender\":-1,\"modHorns\":-1,\"modHydrolic\":-1,\"modTurbo\":1,\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modArmor\":4,\"modStruts\":-1,\"modHood\":-1,\"modRightFender\":-1,\"model\":-1591990051,\"tyreSmokeColor\":[255,255,255],\"color2\":134,\"modSideSkirt\":-1,\"modEngine\":3,\"modDoorSpeaker\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modSeats\":-1,\"modTrimA\":-1,\"modBackWheels\":-1,\"modLivery\":0,\"modRearBumper\":-1,\"pearlescentColor\":0,\"modWindows\":-1,\"modExhaust\":-1,\"modSpeakers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'A2 GHT', 'car', 'police', 0, 0, 0, 0),
('{\"color1\":0,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":{\"6\":false,\"7\":true,\"4\":false,\"5\":false,\"8\":false,\"9\":false,\"12\":true,\"2\":false,\"3\":false,\"11\":false,\"1\":false},\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"plateIndex\":0,\"pearlescentColor\":0,\"modTrimA\":-1,\"modBrakes\":2,\"modTank\":-1,\"modSuspension\":-1,\"wheelColor\":0,\"color2\":0,\"modArmor\":4,\"health\":1000,\"modLivery\":2,\"modFrontBumper\":-1,\"dirtLevel\":0.024275157600641,\"modAerials\":-1,\"modDashboard\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modArchCover\":-1,\"wheels\":1,\"modTurbo\":false,\"modGrille\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"model\":108289116,\"modEngineBlock\":-1,\"modRoof\":-1,\"modHorns\":-1,\"modEngine\":3,\"modFender\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modXenon\":false,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"APX 828\",\"modShifterLeavers\":-1,\"windowTint\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'APX 828', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"plate\":\"AQP 788\",\"modRoof\":-1,\"modWindows\":-1,\"modExhaust\":-1,\"modSeats\":-1,\"modTurbo\":false,\"modPlateHolder\":-1,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modAerials\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"modDoorSpeaker\":-1,\"modTrimA\":-1,\"modHorns\":-1,\"modArmor\":-1,\"modXenon\":false,\"dirtLevel\":0.0,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modOrnaments\":-1,\"color2\":0,\"modHood\":-1,\"modTank\":-1,\"modLivery\":1,\"modFender\":-1,\"wheelColor\":111,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modEngine\":-1,\"modSideSkirt\":-1,\"modRearBumper\":-1,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modRightFender\":-1,\"modBrakes\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"plateIndex\":4,\"extras\":{\"6\":false,\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"1\":false,\"11\":false,\"12\":false,\"10\":true,\"9\":false,\"8\":false,\"7\":false},\"modFrame\":-1,\"health\":1000,\"modSuspension\":-1,\"modGrille\":-1,\"modArchCover\":-1,\"color1\":0,\"modTrunk\":-1,\"modHydrolic\":-1,\"pearlescentColor\":0,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"wheels\":0,\"model\":112512917,\"modDashboard\":-1,\"modAPlate\":-1,\"modSteeringWheel\":-1}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'AQP 788', 'car', 'police', 0, 0, 0, 0),
('{\"modRoof\":-1,\"modEngine\":3,\"dirtLevel\":0.17933934926987,\"modSeats\":-1,\"modFender\":-1,\"modSmokeEnabled\":1,\"modXenon\":1,\"modSpoilers\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"neonEnabled\":[false,false,false,false],\"modArchCover\":-1,\"modTank\":-1,\"modExhaust\":-1,\"modEngineBlock\":-1,\"modHood\":-1,\"color1\":134,\"pearlescentColor\":0,\"neonColor\":[255,0,255],\"modTrunk\":-1,\"modBackWheels\":-1,\"modTurbo\":1,\"modBrakes\":2,\"extras\":{\"11\":true,\"5\":true,\"8\":true,\"7\":true,\"2\":true,\"1\":true,\"4\":true,\"3\":true,\"12\":true},\"modTrimA\":-1,\"modOrnaments\":-1,\"modLivery\":0,\"modHorns\":-1,\"modVanityPlate\":-1,\"modFrame\":-1,\"modSteeringWheel\":-1,\"wheels\":3,\"modSideSkirt\":-1,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"modArmor\":4,\"health\":1000,\"modGrille\":-1,\"modTrimB\":-1,\"modAerials\":-1,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"modTransmission\":2,\"modRearBumper\":-1,\"modSuspension\":3,\"modHydrolic\":-1,\"plateIndex\":4,\"modWindows\":-1,\"wheelColor\":156,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modStruts\":-1,\"plate\":\"ASUPER\",\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modAPlate\":-1,\"color2\":134,\"modShifterLeavers\":-1,\"model\":1922257928}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'ASUPER', 'car', 'police', 0, 0, 0, 0),
('{\"pearlescentColor\":156,\"fuelLevel\":79.232009887695,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modDial\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modTransmission\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modTank\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.17351016402245,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modEngine\":3,\"modFender\":-1,\"modPlateHolder\":-1,\"modXenon\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":true,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"modSmokeEnabled\":1,\"modSuspension\":3,\"plateIndex\":4,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modExhaust\":-1,\"modTrimA\":-1,\"plate\":\"Z\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"model\":-2063562682,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modLivery\":1,\"health\":1000,\"color2\":134,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'car', 'BFZ 526', 'car', 'police', 0, 0, 0, 0),
('{\"modWindows\":-1,\"neonColor\":[255,0,255],\"modAerials\":-1,\"modTank\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modExhaust\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0,\"pearlescentColor\":0,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"plate\":\"BHV 509\",\"modXenon\":false,\"modSeats\":-1,\"modDashboard\":-1,\"modDial\":-1,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modHydrolic\":-1,\"color1\":0,\"modRoof\":-1,\"tyreSmokeColor\":[255,255,255],\"extras\":{\"9\":false,\"12\":true,\"7\":false,\"8\":false,\"5\":true,\"6\":false,\"3\":false,\"4\":false,\"1\":true,\"2\":false,\"11\":false},\"modTrunk\":-1,\"modSpoilers\":-1,\"modTransmission\":-1,\"health\":1000,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"wheels\":1,\"modBrakes\":-1,\"modLivery\":3,\"modFrontWheels\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"model\":108289116,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"color2\":0,\"modTrimB\":-1,\"neonEnabled\":[false,false,false,false],\"modTurbo\":false,\"modFrontBumper\":-1,\"modAPlate\":-1,\"wheelColor\":0,\"modHorns\":-1,\"modRightFender\":-1,\"plateIndex\":0,\"modGrille\":-1,\"modFender\":-1}', 'steam:110000111e8e1e7', 1, 'Garage_Centre', 0, 'car', 'BHV 509', 'car', 'police', 0, 0, 0, 0),
('{\"modHood\":-1,\"modSpoilers\":-1,\"modAerials\":-1,\"modBackWheels\":-1,\"color2\":0,\"modEngine\":-1,\"modSmokeEnabled\":false,\"wheelColor\":156,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modOrnaments\":-1,\"modBrakes\":-1,\"modTrunk\":-1,\"modLivery\":0,\"modStruts\":-1,\"modExhaust\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"wheels\":0,\"model\":-544011608,\"modSpeakers\":-1,\"modRoof\":-1,\"extras\":{\"3\":true,\"1\":true},\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modTransmission\":-1,\"modWindows\":-1,\"modXenon\":false,\"modPlateHolder\":-1,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modDial\":-1,\"modFender\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"modHorns\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modArmor\":-1,\"modFrame\":-1,\"modTurbo\":false,\"pearlescentColor\":0,\"modAPlate\":-1,\"health\":1000,\"neonEnabled\":[false,false,false,false],\"modGrille\":-1,\"dirtLevel\":0.0,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modSteeringWheel\":-1,\"plate\":\"BIY 350\",\"color1\":0,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modTank\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'BIY 350', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":false,\"12\":false,\"10\":false,\"2\":false,\"7\":true},\"modWindows\":-1,\"modArchCover\":-1,\"plate\":\"C 1\",\"modSuspension\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modTrimB\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"model\":-1627000575,\"wheelColor\":156,\"modDoorSpeaker\":-1,\"modLivery\":0,\"modTurbo\":false,\"modDashboard\":-1,\"dirtLevel\":0.0,\"plateIndex\":4,\"modBackWheels\":-1,\"modRoof\":-1,\"modRearBumper\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"color1\":111,\"modFrontWheels\":-1,\"modHorns\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"health\":1000,\"modDial\":-1,\"neonColor\":[255,0,255],\"modAirFilter\":-1,\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTrunk\":-1,\"modHood\":-1,\"modSpoilers\":-1}', '', 1, 'Garage_Centre', 0, 'car', 'C 1', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":3,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":134,\"modHydrolic\":-1,\"extras\":{\"1\":false,\"3\":true,\"2\":false},\"wheelColor\":134,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-793815985,\"color1\":134,\"modLivery\":2,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":5.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 4X4\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":134,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 4X4', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":6,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":1,\"modHydrolic\":-1,\"extras\":{\"5\":true,\"4\":true,\"3\":true,\"2\":false,\"9\":true,\"8\":false,\"7\":false,\"6\":true,\"12\":false,\"1\":false,\"11\":false,\"10\":false},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":178383100,\"color1\":111,\"modLivery\":0,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":1.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 BIKE\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":134,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 BIKE', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":134,\"modHydrolic\":-1,\"extras\":{\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"12\":false,\"1\":false,\"11\":false,\"10\":false},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":1829278388,\"color1\":134,\"modLivery\":3,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":5.0,\"plateIndex\":0,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 CSUP\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":0,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 CSUP', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":3,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":false,\"12\":true,\"10\":false,\"2\":false,\"3\":true},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-1674384553,\"color1\":111,\"modLivery\":2,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":0.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 EXLSU\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 EXLSU', 'car', 'police', 0, 0, 0, 0),
('{\"modOrnaments\":-1,\"modLivery\":2,\"modGrille\":-1,\"modRightFender\":-1,\"modSuspension\":3,\"modAerials\":-1,\"modTank\":-1,\"modSteeringWheel\":-1,\"color1\":111,\"modHood\":-1,\"modEngineBlock\":-1,\"modArmor\":4,\"modSpoilers\":-1,\"modTrimB\":-1,\"modSeats\":-1,\"modHydrolic\":-1,\"modBrakes\":2,\"wheels\":1,\"model\":332822304,\"modFrontBumper\":-1,\"modTrimA\":-1,\"modXenon\":1,\"modShifterLeavers\":-1,\"modAPlate\":-1,\"plate\":\"C1 EXPD\",\"windowTint\":-1,\"wheelColor\":156,\"modDashboard\":-1,\"modWindows\":-1,\"modSpeakers\":-1,\"modExhaust\":-1,\"modHorns\":-1,\"plateIndex\":4,\"color2\":111,\"extras\":{\"1\":true,\"2\":false,\"3\":true,\"4\":false,\"10\":false,\"9\":false,\"11\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false},\"modFrame\":-1,\"modTurbo\":false,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modRoof\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.60541826486588,\"modSmokeEnabled\":1,\"pearlescentColor\":0,\"modDial\":-1,\"modFender\":-1,\"modTransmission\":2,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"neonColor\":[255,0,255],\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modBackWheels\":-1,\"modEngine\":3,\"health\":1000,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 EXPD', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":true,\"12\":false,\"10\":true,\"2\":true,\"3\":true},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":1912215274,\"color1\":111,\"modLivery\":1,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":3.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 EXPL\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 EXPL', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":false,\"4\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"12\":true,\"1\":false,\"11\":false,\"10\":false},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-686097090,\"color1\":111,\"modLivery\":0,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":3.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 EXSU2\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 EXSU2', 'car', 'police', 0, 0, 0, 0);
INSERT INTO `owned_vehicles` (`vehicle`, `owner`, `stored`, `garage_name`, `fourrieremecano`, `vehiclename`, `plate`, `type`, `job`, `state`, `jamstate`, `finance`, `financetimer`) VALUES
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":false,\"12\":true,\"1\":false,\"11\":false,\"10\":false},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-686097090,\"color1\":111,\"modLivery\":3,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":0.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 EXSU3\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 EXSU3', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"wheelColor\":156,\"extras\":{\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"8\":true,\"7\":true,\"12\":false,\"1\":false,\"11\":true},\"wheels\":3,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":134,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modWindows\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modTrunk\":-1,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modTurbo\":false,\"modHorns\":-1,\"modArchCover\":-1,\"modLivery\":1,\"modGrille\":-1,\"modDashboard\":-1,\"dirtLevel\":7.0240249633789,\"plateIndex\":4,\"modBackWheels\":-1,\"neonColor\":[255,0,255],\"modRearBumper\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"modEngine\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modPlateHolder\":-1,\"health\":1000,\"modStruts\":-1,\"modOrnaments\":-1,\"modDial\":-1,\"modRoof\":-1,\"plate\":\"C1 FORD\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"windowTint\":-1,\"modSideSkirt\":-1,\"model\":1922257928,\"modAirFilter\":-1,\"pearlescentColor\":0,\"color1\":134,\"modXenon\":false,\"modShifterLeavers\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 FORD', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":134,\"pearlescentColor\":0,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":134,\"wheels\":3,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":1299980363,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"3\":true,\"2\":false,\"1\":false},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 MT\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 MT', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":0,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"3\":true,\"2\":true},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-1205689942,\"color1\":132,\"modLivery\":-1,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":10.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 RIOT\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":0,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 RIOT', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":134,\"pearlescentColor\":0,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":134,\"wheels\":1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":1142969281,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"4\":false,\"3\":true,\"2\":false},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 SDPSC\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 SDPSC', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":4,\"pearlescentColor\":4,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":4,\"wheels\":0,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":-1233667982,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"1\":true,\"3\":true},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 SFPIU\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":2,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 SFPIU', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":4,\"pearlescentColor\":4,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":4,\"wheels\":3,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":1391522146,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"4\":false,\"3\":false,\"1\":true},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 ST\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":1,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 ST', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":0,\"pearlescentColor\":1,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":111,\"wheels\":0,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":-686097090,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":1.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"8\":false,\"7\":false,\"12\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"10\":false,\"11\":true,\"5\":false,\"2\":false,\"1\":false},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 SUPER\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":1,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 SUPER', 'car', 'police', 0, 0, 0, 0),
('{\"modGrille\":-1,\"modHydrolic\":-1,\"neonColor\":[255,0,255],\"modXenon\":1,\"modArmor\":4,\"modTank\":-1,\"modHorns\":-1,\"modHood\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modTransmission\":2,\"extras\":{\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":true,\"2\":false,\"1\":true,\"10\":false,\"11\":true,\"9\":false},\"modEngine\":3,\"modStruts\":-1,\"pearlescentColor\":0,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modSideSkirt\":-1,\"color1\":111,\"plate\":\"C1 SUPT\",\"modArchCover\":-1,\"model\":332822304,\"modSuspension\":3,\"color2\":111,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modFrontBumper\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modSpoilers\":-1,\"modFender\":-1,\"modShifterLeavers\":-1,\"plateIndex\":4,\"wheelColor\":156,\"modRightFender\":-1,\"modDial\":-1,\"modWindows\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"wheels\":1,\"modTurbo\":1,\"modOrnaments\":-1,\"health\":1000,\"modTrunk\":-1,\"modPlateHolder\":-1,\"modLivery\":0,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modBackWheels\":-1,\"modRoof\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"dirtLevel\":0.052202239632607,\"modFrontWheels\":-1,\"modBrakes\":2}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 SUPT', 'car', 'police', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":3,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"10\":true},\"wheelColor\":156,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"model\":-1291872016,\"color1\":111,\"modLivery\":1,\"modHorns\":-1,\"modDashboard\":-1,\"dirtLevel\":11.0,\"plateIndex\":4,\"modBackWheels\":-1,\"health\":1000,\"modWindows\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"modDial\":-1,\"modTrunk\":-1,\"plate\":\"C1 TAHOE\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"pearlescentColor\":1,\"modEngine\":-1,\"modSideSkirt\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 TAHOE', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":4,\"pearlescentColor\":4,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":4,\"wheels\":3,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":2065038206,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"4\":false,\"3\":false,\"2\":false,\"1\":false},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 TDPS\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 TDPS', 'car', 'police', 0, 0, 0, 0),
('{\"modDial\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modSpeakers\":-1,\"modXenon\":false,\"wheelColor\":156,\"health\":1000,\"windowTint\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"color2\":99,\"pearlescentColor\":99,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"color1\":99,\"wheels\":1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":-1017960488,\"modArchCover\":-1,\"modGrille\":-1,\"dirtLevel\":0.0,\"modTrimA\":-1,\"modFrame\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"extras\":{\"4\":false,\"7\":false,\"3\":true},\"modStruts\":-1,\"modSeats\":-1,\"modDashboard\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"modTurbo\":false,\"modTrunk\":-1,\"modFender\":-1,\"plate\":\"C1 UC\",\"modAerials\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modTank\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modOrnaments\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 UC', 'car', 'police', 0, 0, 0, 0),
('{\"health\":1000,\"modHorns\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.3664565384388,\"modXenon\":1,\"modVanityPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modWindows\":-1,\"modSpeakers\":-1,\"modEngineBlock\":-1,\"extras\":{\"12\":false,\"9\":false,\"6\":false,\"5\":true,\"8\":true,\"7\":true,\"2\":true,\"1\":true,\"4\":false,\"3\":false,\"11\":true},\"neonEnabled\":[false,false,false,false],\"modGrille\":-1,\"modTrimB\":-1,\"modDial\":-1,\"modArmor\":4,\"modBrakes\":-1,\"pearlescentColor\":0,\"modRightFender\":-1,\"windowTint\":-1,\"modFrame\":-1,\"wheelColor\":0,\"color2\":0,\"modTrimA\":-1,\"modStruts\":-1,\"wheels\":1,\"modFrontWheels\":-1,\"modOrnaments\":-1,\"modHood\":-1,\"modAPlate\":-1,\"plateIndex\":0,\"modSuspension\":-1,\"modBackWheels\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modSpoilers\":-1,\"modHydrolic\":-1,\"modEngine\":-1,\"color1\":0,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"modArchCover\":-1,\"plate\":\"C1 UNMRK\",\"modRoof\":-1,\"modSteeringWheel\":-1,\"modFender\":-1,\"modTank\":-1,\"modSideSkirt\":-1,\"modTransmission\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modAirFilter\":-1,\"modLivery\":2,\"modFrontBumper\":-1,\"modTrunk\":-1,\"modDoorSpeaker\":-1,\"model\":108289116,\"modShifterLeavers\":-1,\"modTurbo\":false}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 UNMRK', 'car', 'police', 0, 0, 0, 0),
('{\"modXenon\":1,\"modSideSkirt\":-1,\"plateIndex\":4,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modFrontBumper\":-1,\"modHorns\":-1,\"modTransmission\":2,\"modArmor\":4,\"modAPlate\":-1,\"modSuspension\":3,\"modEngine\":3,\"wheelColor\":156,\"modFrame\":-1,\"modRightFender\":-1,\"plate\":\"C1 VIC\",\"modHydrolic\":-1,\"modTrunk\":-1,\"extras\":{\"7\":false,\"10\":false,\"2\":false,\"5\":false,\"12\":true},\"pearlescentColor\":1,\"color1\":111,\"modDial\":-1,\"modRearBumper\":-1,\"modSeats\":-1,\"model\":-994755493,\"windowTint\":-1,\"modBackWheels\":-1,\"modStruts\":-1,\"modEngineBlock\":-1,\"modAerials\":-1,\"modSpoilers\":-1,\"modVanityPlate\":-1,\"modOrnaments\":-1,\"neonColor\":[255,0,255],\"modDashboard\":-1,\"dirtLevel\":0.0091020157560706,\"modSpeakers\":-1,\"modSmokeEnabled\":1,\"modTank\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modTurbo\":1,\"modWindows\":-1,\"modTrimB\":-1,\"modShifterLeavers\":-1,\"wheels\":1,\"modRoof\":-1,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modLivery\":0,\"modFender\":-1,\"modTrimA\":-1,\"neonEnabled\":[false,false,false,false],\"modExhaust\":-1,\"modPlateHolder\":-1,\"tyreSmokeColor\":[255,255,255],\"health\":1000,\"color2\":0,\"modGrille\":-1,\"modBrakes\":2}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'C1 VIC', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":134,\"modFrame\":-1,\"modHood\":-1,\"modFrontWheels\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"neonEnabled\":[false,false,false,false],\"modHorns\":-1,\"plate\":\"CHARGER\",\"modSuspension\":-1,\"modSideSkirt\":-1,\"neonColor\":[255,0,255],\"modTurbo\":1,\"health\":1000,\"modLivery\":1,\"modDoorSpeaker\":-1,\"modPlateHolder\":-1,\"modSpoilers\":-1,\"modBrakes\":2,\"modFender\":-1,\"pearlescentColor\":134,\"modGrille\":-1,\"modSeats\":-1,\"modExhaust\":-1,\"modEngine\":3,\"modXenon\":false,\"dirtLevel\":0.011579330079257,\"modBackWheels\":-1,\"modAPlate\":-1,\"modFrontBumper\":-1,\"color2\":134,\"modArmor\":4,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modArchCover\":-1,\"extras\":{\"3\":false,\"12\":true,\"1\":false,\"2\":false,\"10\":false,\"4\":false,\"9\":false,\"11\":false,\"7\":false,\"8\":false,\"5\":false,\"6\":false},\"modOrnaments\":-1,\"wheels\":0,\"modWindows\":-1,\"modRightFender\":-1,\"modRoof\":-1,\"modTrunk\":-1,\"modAirFilter\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modSmokeEnabled\":1,\"modEngineBlock\":-1,\"modTank\":-1,\"modRearBumper\":-1,\"windowTint\":-1,\"modSteeringWheel\":-1,\"modTrimA\":-1,\"color1\":134,\"plateIndex\":0,\"modTransmission\":2,\"model\":447530523,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'CHARGER', 'car', 'police', 0, 0, 0, 0),
('{\"modArmor\":-1,\"modDial\":-1,\"modTrimA\":-1,\"modSeats\":-1,\"fuelLevel\":93.896003723145,\"color1\":111,\"modSpeakers\":-1,\"modDoorSpeaker\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modTrunk\":-1,\"modAPlate\":-1,\"plateIndex\":4,\"modRoof\":-1,\"modSideSkirt\":-1,\"wheels\":0,\"plate\":\"DEK 816\",\"color2\":0,\"model\":-1627000575,\"modTank\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":1,\"modFender\":-1,\"modXenon\":false,\"modVanityPlate\":-1,\"modShifterLeavers\":-1,\"wheelColor\":156,\"neonEnabled\":[false,false,false,false],\"windowTint\":-1,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"dirtLevel\":1.0,\"modStruts\":-1,\"modDashboard\":-1,\"modSpoilers\":-1,\"health\":1000,\"modTurbo\":false,\"modGrille\":-1,\"modTrimB\":-1,\"modAirFilter\":-1,\"modHydrolic\":-1,\"modPlateHolder\":-1,\"modHood\":-1,\"modBrakes\":-1,\"modSuspension\":-1,\"extras\":{\"2\":false,\"10\":true,\"7\":false,\"12\":false,\"5\":false},\"modLivery\":1,\"modWindows\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modHorns\":-1,\"engineHealth\":1000.0,\"modEngine\":-1,\"modArchCover\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"modBackWheels\":-1}', 'steam:11000010d0d2e73', 1, 'Garage_Centre', 0, 'car', 'DEK 816', 'car', 'police', 0, 0, 0, 0),
('{\"modArmor\":-1,\"modDial\":-1,\"modTrimA\":-1,\"modSeats\":-1,\"modHood\":-1,\"color1\":134,\"modVanityPlate\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontBumper\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"modAPlate\":-1,\"plateIndex\":0,\"modRoof\":-1,\"modSideSkirt\":-1,\"wheels\":1,\"plate\":\"DPN 409\",\"color2\":134,\"model\":938090162,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"pearlescentColor\":134,\"modSpoilers\":-1,\"modXenon\":false,\"modShifterLeavers\":-1,\"modOrnaments\":-1,\"modSpeakers\":-1,\"neonEnabled\":[false,false,false,false],\"modGrille\":-1,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"dirtLevel\":1.0,\"modStruts\":-1,\"fuelLevel\":85.188003540039,\"modDashboard\":-1,\"health\":1000,\"wheelColor\":134,\"modEngineBlock\":-1,\"modRightFender\":-1,\"modAirFilter\":-1,\"bodyHealth\":1000.0,\"modTurbo\":false,\"modTank\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modPlateHolder\":-1,\"modLivery\":0,\"windowTint\":-1,\"modWindows\":-1,\"extras\":{\"2\":true,\"3\":false,\"1\":true,\"6\":false,\"7\":false,\"4\":false,\"5\":false,\"8\":false,\"9\":false,\"10\":false,\"11\":false,\"12\":false},\"modHydrolic\":-1,\"modExhaust\":-1,\"modTrimB\":-1,\"engineHealth\":1000.0,\"modEngine\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modFender\":-1,\"modFrame\":-1,\"modBackWheels\":-1}', 'steam:110000105bca616', 1, 'Garage_Centre', 0, 'car', 'DPN 409', 'car', 'police', 0, 0, 0, 0),
('{\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"modTrimB\":-1,\"modDial\":-1,\"color1\":0,\"neonColor\":[255,0,255],\"modHorns\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modTrimA\":-1,\"modAerials\":-1,\"modFender\":-1,\"modArmor\":4,\"modFrontBumper\":-1,\"plateIndex\":4,\"plate\":\"DYH 575\",\"modPlateHolder\":-1,\"modLivery\":1,\"modTank\":-1,\"modSmokeEnabled\":1,\"modOrnaments\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modHood\":-1,\"dirtLevel\":0.22127741575241,\"windowTint\":4,\"color2\":0,\"model\":-686097090,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"modTransmission\":2,\"modEngine\":3,\"modTurbo\":1,\"modGrille\":-1,\"modDashboard\":-1,\"modTrunk\":-1,\"modBrakes\":2,\"modSeats\":-1,\"modVanityPlate\":-1,\"extras\":{\"8\":false,\"9\":false,\"10\":false,\"11\":true,\"12\":false,\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false},\"modDoorSpeaker\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"wheels\":0,\"health\":1000,\"modXenon\":false,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":3,\"modStruts\":-1,\"modSpeakers\":-1,\"pearlescentColor\":0,\"modSteeringWheel\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Paleto', 0, 'FPIU', 'DYH 575', 'car', 'police', 0, 0, 0, 0),
('{\"modSideSkirt\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modVanityPlate\":-1,\"modOrnaments\":-1,\"plateIndex\":0,\"health\":1000,\"modTrimB\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"modTransmission\":2,\"modEngineBlock\":-1,\"modHood\":-1,\"modRearBumper\":-1,\"modAPlate\":-1,\"modDoorSpeaker\":-1,\"modEngine\":-1,\"color2\":3,\"modAerials\":-1,\"modDial\":-1,\"modSpoilers\":2,\"modPlateHolder\":-1,\"neonEnabled\":[1,1,1,1],\"modTank\":-1,\"modBackWheels\":-1,\"pearlescentColor\":5,\"modFrame\":-1,\"modHydrolic\":-1,\"plate\":\"E4OFP\",\"modSuspension\":-1,\"modStruts\":-1,\"extras\":[],\"color1\":3,\"modAirFilter\":-1,\"modSpeakers\":-1,\"modXenon\":1,\"modTrunk\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modFrontWheels\":-1,\"modLivery\":-1,\"tyreSmokeColor\":[255,255,255],\"modWindows\":-1,\"modTrimA\":-1,\"modSmokeEnabled\":1,\"wheelColor\":156,\"modBrakes\":-1,\"wheels\":7,\"neonColor\":[0,255,0],\"windowTint\":1,\"modRoof\":-1,\"modDashboard\":-1,\"modArchCover\":-1,\"modSeats\":-1,\"modFender\":-1,\"modHorns\":-1,\"modRightFender\":-1,\"dirtLevel\":4.4012460708618,\"modTurbo\":1,\"model\":234062309}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'E4OFP', 'car', NULL, 0, 0, 148500, -954),
('{\"pearlescentColor\":156,\"fuelLevel\":79.232009887695,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modDial\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modTransmission\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modTank\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.17351016402245,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modEngine\":3,\"modFender\":-1,\"modPlateHolder\":-1,\"modXenon\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":true,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"modSmokeEnabled\":1,\"modSuspension\":3,\"plateIndex\":4,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modExhaust\":-1,\"modTrimA\":-1,\"plate\":\"Z\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"model\":-2063562682,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modLivery\":1,\"health\":1000,\"color2\":134,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'EQZ 335', 'car', 'police', 0, 0, 0, 0),
('{\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"modTrimB\":-1,\"modDial\":-1,\"color1\":0,\"neonColor\":[255,0,255],\"modHorns\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modTrimA\":-1,\"modAerials\":-1,\"modFender\":-1,\"modArmor\":4,\"modFrontBumper\":-1,\"plateIndex\":0,\"plate\":\"FKT 060\",\"modPlateHolder\":-1,\"modLivery\":0,\"modTank\":-1,\"modSmokeEnabled\":1,\"modOrnaments\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modHood\":-1,\"dirtLevel\":0.55614870786667,\"windowTint\":-1,\"color2\":0,\"model\":1829278388,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":0,\"modTransmission\":2,\"modEngine\":3,\"modTurbo\":1,\"modGrille\":-1,\"modDashboard\":-1,\"modTrunk\":-1,\"modBrakes\":2,\"modSeats\":-1,\"modVanityPlate\":-1,\"extras\":{\"8\":false,\"9\":false,\"10\":true,\"11\":false,\"12\":false,\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false},\"modDoorSpeaker\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"wheels\":1,\"health\":1000,\"modXenon\":false,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":3,\"modStruts\":-1,\"modSpeakers\":-1,\"pearlescentColor\":0,\"modSteeringWheel\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Paleto', 0, 'Dodge Charger', 'FKT 060', 'car', 'police', 0, 0, 0, 0),
('{\"modRoof\":-1,\"modEngine\":3,\"dirtLevel\":0.069621816277504,\"modExhaust\":-1,\"modFender\":-1,\"modBrakes\":2,\"modDial\":-1,\"modDashboard\":-1,\"windowTint\":-1,\"modAPlate\":-1,\"color1\":0,\"modArchCover\":-1,\"modTank\":-1,\"modSeats\":-1,\"modEngineBlock\":-1,\"modHood\":-1,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modAirFilter\":-1,\"modFrontBumper\":-1,\"modTurbo\":false,\"modHydrolic\":-1,\"extras\":{\"1\":true,\"3\":true},\"modTrunk\":-1,\"model\":-1233667982,\"modXenon\":false,\"modHorns\":-1,\"modTrimA\":-1,\"modFrame\":-1,\"modSteeringWheel\":-1,\"health\":1000,\"modSideSkirt\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modArmor\":4,\"pearlescentColor\":0,\"modGrille\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modSpeakers\":-1,\"wheels\":0,\"modVanityPlate\":-1,\"modTransmission\":2,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modLivery\":2,\"modSmokeEnabled\":1,\"modWindows\":-1,\"wheelColor\":156,\"modRightFender\":-1,\"modOrnaments\":-1,\"modStruts\":-1,\"plate\":\"IDD 692\",\"modFrontWheels\":-1,\"plateIndex\":4,\"modTrimB\":-1,\"color2\":4,\"modShifterLeavers\":-1,\"modBackWheels\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'IDD 692', 'car', 'police', 0, 0, 0, 0),
('{\"modTrimB\":-1,\"modGrille\":-1,\"modDial\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modEngineBlock\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modSpeakers\":-1,\"modHydrolic\":-1,\"modHorns\":-1,\"color2\":3,\"modTrunk\":-1,\"modTank\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modRoof\":-1,\"pearlescentColor\":5,\"extras\":[],\"color1\":3,\"modDashboard\":-1,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modVanityPlate\":-1,\"modLivery\":-1,\"modAPlate\":-1,\"modFrame\":-1,\"modSeats\":-1,\"modWindows\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"plateIndex\":0,\"modPlateHolder\":-1,\"modTransmission\":-1,\"modSpoilers\":-1,\"modSideSkirt\":-1,\"modAirFilter\":-1,\"model\":234062309,\"modHood\":-1,\"modStruts\":-1,\"modArmor\":-1,\"wheels\":7,\"modBrakes\":-1,\"health\":977,\"modAerials\":-1,\"modEngine\":3,\"modFrontWheels\":-1,\"dirtLevel\":15.0,\"modFender\":-1,\"modShifterLeavers\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"plate\":\"L84 ANL\",\"modRightFender\":-1,\"modTurbo\":1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"modDoorSpeaker\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'Reaper', 'L84 ANL', 'car', NULL, 0, 0, 148500, 2342),
('{\"modWindows\":-1,\"neonColor\":[255,0,255],\"modAerials\":-1,\"modTank\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modExhaust\":-1,\"modTrimA\":-1,\"dirtLevel\":7.0,\"pearlescentColor\":134,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"plate\":\"LAB 592\",\"modXenon\":false,\"modSeats\":-1,\"modDashboard\":-1,\"modDial\":-1,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modHydrolic\":-1,\"color1\":134,\"modRoof\":-1,\"tyreSmokeColor\":[255,255,255],\"extras\":{\"3\":false,\"1\":true,\"2\":false},\"modTrunk\":-1,\"modSpoilers\":-1,\"modTransmission\":-1,\"health\":1000,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"wheels\":3,\"modBrakes\":-1,\"modLivery\":1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"model\":-793815985,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"color2\":134,\"modTrimB\":-1,\"neonEnabled\":[false,false,false,false],\"modTurbo\":false,\"modFrontBumper\":-1,\"modAPlate\":-1,\"wheelColor\":134,\"modHorns\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modGrille\":-1,\"modFender\":-1}', 'steam:110000111e8e1e7', 1, 'Garage_Centre', 0, 'car', 'LAB 592', 'car', 'police', 0, 0, 0, 0),
('{\"windowTint\":-1,\"modDial\":-1,\"modTrimA\":-1,\"modSeats\":-1,\"modSpoilers\":-1,\"color1\":111,\"modSpeakers\":-1,\"modVanityPlate\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modTrunk\":-1,\"modAPlate\":-1,\"plateIndex\":4,\"modRoof\":-1,\"modSideSkirt\":-1,\"wheels\":1,\"plate\":\"LCL 931\",\"color2\":0,\"model\":-994755493,\"modArmor\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":1,\"modTank\":-1,\"modXenon\":false,\"modTrimB\":-1,\"modShifterLeavers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"extras\":{\"2\":false,\"10\":false,\"7\":false,\"12\":false,\"5\":true},\"modWindows\":-1,\"dirtLevel\":3.0,\"modStruts\":-1,\"modTransmission\":-1,\"modDashboard\":-1,\"health\":1000,\"fuelLevel\":84.184005737305,\"modTurbo\":false,\"modRightFender\":-1,\"modAirFilter\":-1,\"modGrille\":-1,\"modBrakes\":-1,\"modHood\":-1,\"modHorns\":-1,\"modPlateHolder\":-1,\"neonColor\":[255,0,255],\"modLivery\":0,\"modSuspension\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modExhaust\":-1,\"wheelColor\":156,\"engineHealth\":1000.0,\"modEngine\":-1,\"modArchCover\":-1,\"modFender\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"modBackWheels\":-1}', 'steam:1100001045201b3', 1, 'Garage_Centre', 0, 'car', 'LCL 931', 'car', 'police', 0, 0, 0, 0),
('{\"modOrnaments\":-1,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modAirFilter\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"modTrunk\":-1,\"windowTint\":-1,\"plateIndex\":4,\"modExhaust\":-1,\"modSeats\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"color2\":0,\"modRearBumper\":-1,\"modSpeakers\":-1,\"modBackWheels\":-1,\"modTank\":-1,\"pearlescentColor\":0,\"modDial\":-1,\"dirtLevel\":0.0,\"health\":1000,\"modVanityPlate\":-1,\"modRoof\":-1,\"modWindows\":-1,\"modPlateHolder\":-1,\"modSuspension\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modHydrolic\":-1,\"modShifterLeavers\":-1,\"color1\":0,\"modSmokeEnabled\":false,\"modFrontBumper\":-1,\"model\":112512917,\"extras\":{\"10\":false,\"11\":true,\"1\":true,\"12\":false,\"3\":false,\"2\":false,\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false},\"plate\":\"LEJ 917\",\"modDashboard\":-1,\"modLivery\":0,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":111,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modStruts\":-1,\"modXenon\":false,\"modGrille\":-1,\"modTrimA\":-1,\"modFender\":-1,\"modHood\":-1,\"modFrame\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTransmission\":-1,\"wheels\":0}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'LEJ 917', 'car', 'police', 0, 0, 0, 0),
('{\"plate\":\"LVP 454\",\"modArmor\":-1,\"modTrunk\":-1,\"modFrontBumper\":-1,\"modStruts\":-1,\"color2\":111,\"plateIndex\":4,\"modAirFilter\":-1,\"modXenon\":false,\"modGrille\":-1,\"modTank\":-1,\"wheels\":3,\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modDashboard\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modRightFender\":-1,\"modLivery\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"health\":1000,\"dirtLevel\":0.04232931882143,\"modHydrolic\":-1,\"modFender\":-1,\"modSpeakers\":-1,\"model\":1051898383,\"color1\":111,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1,\"modSeats\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modRoof\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":true},\"windowTint\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modWindows\":-1,\"modSmokeEnabled\":1,\"modSuspension\":-1,\"modTrimA\":-1,\"tyreSmokeColor\":[255,255,255],\"modEngine\":3,\"modArchCover\":-1,\"modAerials\":-1,\"modTurbo\":false,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modFrame\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"modBrakes\":2,\"modDial\":-1,\"modTrimB\":-1,\"pearlescentColor\":0,\"modExhaust\":-1,\"modPlateHolder\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Ocean1', 0, 'Deputy Medical Director\'s Vehicle', 'LVP 454', 'car', 'ambulance', 0, 0, 0, 0),
('{\"modEngine\":3,\"fuelLevel\":78.056282043457,\"modRightFender\":-1,\"modHood\":-1,\"modTrimB\":-1,\"modAirFilter\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"extras\":{\"5\":false,\"6\":true,\"3\":false,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"model\":-2063562682,\"modHorns\":0,\"windowTint\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modDial\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.33622017502785,\"modPlateHolder\":-1,\"modOrnaments\":-1,\"pearlescentColor\":0,\"modSpoilers\":-1,\"modAPlate\":-1,\"modFender\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"bodyHealth\":1000.0,\"modTrimA\":-1,\"plate\":\"MJJ 525\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modTransmission\":2,\"modXenon\":1,\"modArmor\":4,\"modFrame\":-1,\"modTank\":-1,\"modSideSkirt\":-1,\"color1\":0,\"modVanityPlate\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modDashboard\":-1,\"modAerials\":-1,\"health\":1000,\"color2\":134,\"modSeats\":-1,\"modLivery\":1,\"modRearBumper\":-1,\"modExhaust\":-1}', 'steam:110000105bca616', 1, 'Garage_Centre', 0, 'car', 'MJJ 525', 'car', 'police', 0, 0, 0, 0),
('{\"pearlescentColor\":156,\"fuelLevel\":79.232009887695,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modDial\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modTransmission\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modTank\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.17351016402245,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modEngine\":3,\"modFender\":-1,\"modPlateHolder\":-1,\"modXenon\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":true,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"modSmokeEnabled\":1,\"modSuspension\":3,\"plateIndex\":4,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modExhaust\":-1,\"modTrimA\":-1,\"plate\":\"Z\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"model\":-2063562682,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modLivery\":1,\"health\":1000,\"color2\":134,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'MZR 824', 'car', 'police', 0, 0, 0, 0),
('{\"plate\":\"NYM 085\",\"modSeats\":-1,\"dirtLevel\":0.014988760463893,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"modLivery\":1,\"modSteeringWheel\":-1,\"modArmor\":4,\"modBrakes\":2,\"modSideSkirt\":-1,\"modDoorSpeaker\":-1,\"modFender\":-1,\"windowTint\":-1,\"modHood\":-1,\"color1\":111,\"modFrame\":-1,\"extras\":{\"7\":true,\"5\":false,\"12\":false,\"2\":false,\"10\":false},\"modDashboard\":-1,\"modRearBumper\":-1,\"modXenon\":1,\"modOrnaments\":-1,\"modStruts\":-1,\"modAerials\":-1,\"model\":-994755493,\"modAPlate\":-1,\"modRightFender\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"modSpeakers\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"modTrimB\":-1,\"modTurbo\":1,\"modTransmission\":2,\"modTank\":-1,\"pearlescentColor\":1,\"neonColor\":[255,0,255],\"modHorns\":-1,\"modWindows\":-1,\"neonEnabled\":[false,false,false,false],\"modEngineBlock\":-1,\"tyreSmokeColor\":[255,255,255],\"modExhaust\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modDial\":-1,\"modFrontWheels\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"modSuspension\":-1,\"health\":1000,\"color2\":0,\"modEngine\":3,\"wheels\":1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSmokeEnabled\":1,\"wheelColor\":156,\"modRoof\":-1}', 'steam:11000010d0d2e73', 1, 'Garage_Centre', 0, 'car', 'NYM 085', 'car', 'police', 0, 0, 0, 0),
('{\"modRoof\":-1,\"modTrunk\":-1,\"model\":1456588330,\"plateIndex\":4,\"modRightFender\":-1,\"modBrakes\":-1,\"modHydrolic\":-1,\"modTransmission\":-1,\"modAerials\":-1,\"modFrontWheels\":-1,\"modTrimA\":-1,\"modTank\":-1,\"color1\":111,\"neonEnabled\":[false,false,false,false],\"modDoorSpeaker\":-1,\"modDial\":-1,\"modSteeringWheel\":-1,\"modEngineBlock\":-1,\"modLivery\":2,\"modSideSkirt\":-1,\"modSeats\":-1,\"modEngine\":-1,\"modPlateHolder\":-1,\"modWindows\":-1,\"modDashboard\":-1,\"health\":1000,\"color2\":111,\"tyreSmokeColor\":[255,255,255],\"modAPlate\":-1,\"modBackWheels\":-1,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"modArchCover\":-1,\"windowTint\":-1,\"wheels\":3,\"pearlescentColor\":0,\"modFrontBumper\":-1,\"plate\":\"O1 SUV\",\"modSpeakers\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modGrille\":-1,\"modFrame\":-1,\"modTurbo\":false,\"wheelColor\":156,\"modSmokeEnabled\":false,\"modTrimB\":-1,\"modArmor\":-1,\"modAirFilter\":-1,\"modHood\":-1,\"modXenon\":false,\"extras\":{\"2\":false,\"1\":false,\"6\":false,\"5\":false,\"4\":false,\"3\":false},\"dirtLevel\":7.0,\"modVanityPlate\":-1,\"modExhaust\":-1,\"modFender\":-1,\"modOrnaments\":-1,\"modHorns\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'car', 'O1 SUV', 'car', 'police', 0, 0, 0, 0),
('{\"modHydrolic\":-1,\"modRightFender\":-1,\"model\":1799416425,\"modArmor\":4,\"modHorns\":-1,\"modTransmission\":-1,\"modAirFilter\":-1,\"modSpeakers\":-1,\"health\":1000,\"modVanityPlate\":-1,\"modFrontBumper\":-1,\"modEngine\":3,\"modTrimA\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modRoof\":-1,\"wheels\":0,\"modSeats\":-1,\"plateIndex\":4,\"modFrame\":-1,\"wheelColor\":156,\"modSideSkirt\":-1,\"modFender\":-1,\"modDashboard\":-1,\"modTrimB\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modWindows\":-1,\"modTank\":-1,\"modFrontWheels\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":1,\"modSuspension\":-1,\"modGrille\":-1,\"plate\":\"O1 TRK\",\"modAerials\":-1,\"modTurbo\":false,\"modHood\":-1,\"modLivery\":1,\"pearlescentColor\":0,\"modAPlate\":-1,\"windowTint\":-1,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modPlateHolder\":-1,\"extras\":{\"6\":false,\"5\":true,\"10\":true,\"7\":true,\"2\":true,\"1\":true,\"4\":true,\"3\":true,\"11\":true,\"9\":true,\"12\":true,\"8\":true},\"modExhaust\":-1,\"modDoorSpeaker\":-1,\"modTrunk\":-1,\"color1\":149,\"modSteeringWheel\":-1,\"modShifterLeavers\":-1,\"color2\":134,\"modXenon\":false,\"modBrakes\":2,\"modOrnaments\":-1,\"dirtLevel\":3.5246226787567}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'car', 'O1 TRK', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modHood\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"windowTint\":1,\"modTransmission\":2,\"plate\":\"O9 CAM\",\"modFrontBumper\":-1,\"modSideSkirt\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"health\":1000,\"modLivery\":0,\"pearlescentColor\":1,\"modPlateHolder\":-1,\"modTurbo\":1,\"modBrakes\":2,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modGrille\":-1,\"modArchCover\":-1,\"modExhaust\":-1,\"modSeats\":-1,\"modXenon\":1,\"dirtLevel\":0.0030850216280669,\"modBackWheels\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"color2\":21,\"modOrnaments\":-1,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"modEngine\":3,\"modHydrolic\":-1,\"extras\":{\"3\":false,\"4\":false,\"1\":false,\"2\":false,\"10\":true,\"9\":false,\"7\":false,\"8\":true,\"5\":false,\"6\":false},\"modRoof\":-1,\"wheels\":0,\"modWindows\":-1,\"modRightFender\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modHorns\":-1,\"modTrunk\":-1,\"modEngineBlock\":-1,\"modTank\":-1,\"modRearBumper\":-1,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modFrontWheels\":-1,\"color1\":21,\"plateIndex\":4,\"modArmor\":4,\"model\":-144458181,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'O9 CAM', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modHood\":-1,\"modSmokeEnabled\":false,\"modVanityPlate\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modFrontBumper\":-1,\"plate\":\"O9 CHARG\",\"windowTint\":-1,\"modSideSkirt\":-1,\"modTransmission\":-1,\"modArmor\":-1,\"health\":1000,\"modLivery\":0,\"wheels\":1,\"modPlateHolder\":-1,\"modWindows\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modTurbo\":false,\"modGrille\":-1,\"modArchCover\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modXenon\":false,\"dirtLevel\":6.0,\"modBackWheels\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"color2\":134,\"pearlescentColor\":0,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"modHorns\":-1,\"neonEnabled\":[false,false,false,false],\"extras\":{\"11\":true,\"12\":false,\"1\":false,\"2\":false,\"10\":false,\"3\":false,\"9\":false,\"4\":false,\"7\":false,\"8\":false,\"5\":false,\"6\":false},\"modDoorSpeaker\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"modHydrolic\":-1,\"modRoof\":-1,\"modTrimA\":-1,\"modAirFilter\":-1,\"modAerials\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1,\"modTank\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"color1\":134,\"plateIndex\":0,\"neonColor\":[255,0,255],\"model\":1829278388,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'O9 CHARG', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modHood\":-1,\"modFrontWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":3,\"color1\":111,\"modAerials\":-1,\"plate\":\"O9 K9\",\"modTank\":-1,\"modSideSkirt\":-1,\"modTransmission\":2,\"neonEnabled\":[false,false,false,false],\"health\":1000,\"modLivery\":2,\"pearlescentColor\":1,\"modPlateHolder\":-1,\"modHorns\":-1,\"modBrakes\":2,\"modFender\":-1,\"modTurbo\":1,\"modGrille\":-1,\"modSeats\":-1,\"modExhaust\":-1,\"modArmor\":4,\"modXenon\":false,\"dirtLevel\":0.099277645349503,\"modBackWheels\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"color2\":0,\"modTrimA\":-1,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":1,\"modRightFender\":-1,\"extras\":{\"11\":false,\"12\":false,\"1\":false,\"2\":false,\"10\":true,\"4\":false,\"9\":true,\"3\":false,\"7\":false,\"8\":true,\"5\":true,\"6\":true},\"modRoof\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"windowTint\":-1,\"modSuspension\":-1,\"neonColor\":[255,0,255],\"modAirFilter\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modSmokeEnabled\":1,\"modEngineBlock\":-1,\"modFrontBumper\":-1,\"modRearBumper\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modOrnaments\":-1,\"model\":-1590802065,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'O9 K9', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modHood\":-1,\"modFrontWheels\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modSpoilers\":-1,\"plate\":\"O9 TAURU\",\"modSmokeEnabled\":1,\"modSideSkirt\":-1,\"neonColor\":[255,0,255],\"neonEnabled\":[false,false,false,false],\"health\":1000,\"modLivery\":2,\"pearlescentColor\":1,\"modPlateHolder\":-1,\"modTank\":-1,\"modBrakes\":2,\"modFender\":-1,\"modTurbo\":1,\"modGrille\":-1,\"modArchCover\":-1,\"modExhaust\":-1,\"modTrimA\":-1,\"modXenon\":false,\"dirtLevel\":2.5392453670502,\"modBackWheels\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"color2\":0,\"modTransmission\":2,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"color1\":111,\"extras\":{\"3\":true,\"4\":false,\"1\":false,\"2\":false,\"10\":false,\"12\":false,\"9\":false,\"11\":true,\"7\":false,\"8\":true,\"5\":false,\"6\":false},\"modArmor\":4,\"wheels\":3,\"modStruts\":-1,\"modHydrolic\":-1,\"modRoof\":-1,\"modEngine\":3,\"modAirFilter\":-1,\"modSuspension\":-1,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modRearBumper\":-1,\"modWindows\":-1,\"modSteeringWheel\":-1,\"modHorns\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modOrnaments\":-1,\"model\":-448292453,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'O9 TAURU', 'car', 'police', 0, 0, 0, 0);
INSERT INTO `owned_vehicles` (`vehicle`, `owner`, `stored`, `garage_name`, `fourrieremecano`, `vehiclename`, `plate`, `type`, `job`, `state`, `jamstate`, `finance`, `financetimer`) VALUES
('{\"neonEnabled\":[false,false,false,false],\"modHorns\":-1,\"modEngine\":-1,\"windowTint\":-1,\"color2\":134,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSuspension\":-1,\"modShifterLeavers\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modFrame\":-1,\"modFender\":-1,\"modArmor\":-1,\"modWindows\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"wheels\":0,\"modAerials\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"dirtLevel\":8.0,\"extras\":{\"10\":false,\"7\":false,\"12\":false,\"8\":false,\"5\":true,\"6\":false,\"3\":false,\"4\":false,\"1\":false,\"2\":false,\"9\":false,\"11\":false},\"wheelColor\":156,\"modSpoilers\":-1,\"modRearBumper\":-1,\"modLivery\":0,\"modBrakes\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"modDial\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modRightFender\":-1,\"modArchCover\":-1,\"modHood\":-1,\"modExhaust\":-1,\"modSpeakers\":-1,\"modTrimA\":-1,\"model\":-1649544918,\"modDashboard\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"modFrontWheels\":-1,\"modVanityPlate\":-1,\"modAPlate\":-1,\"health\":1000,\"modSeats\":-1,\"plate\":\"OEP 586\",\"modTank\":-1,\"color1\":134,\"tyreSmokeColor\":[255,255,255],\"pearlescentColor\":156,\"modXenon\":false,\"modTrimB\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'car', 'OEP 586', 'car', 'ambulance', 0, 0, 0, 0),
('{\"modFrontBumper\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"color2\":0,\"modHydrolic\":-1,\"extras\":{\"5\":true,\"4\":false,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true,\"12\":false,\"1\":false,\"11\":false,\"10\":false},\"modWindows\":-1,\"modArchCover\":-1,\"modSuspension\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modSeats\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"health\":1000,\"modTank\":-1,\"modAerials\":-1,\"modArmor\":-1,\"modFender\":-1,\"modRoof\":-1,\"wheelColor\":156,\"color1\":111,\"modLivery\":1,\"modFrontWheels\":-1,\"modDashboard\":-1,\"dirtLevel\":0.0,\"plateIndex\":4,\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modRearBumper\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"neonColor\":[255,0,255],\"modTurbo\":false,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modDoorSpeaker\":-1,\"modDial\":-1,\"modHorns\":-1,\"plate\":\"P37\",\"modExhaust\":-1,\"modVanityPlate\":-1,\"pearlescentColor\":1,\"modRightFender\":-1,\"modXenon\":false,\"modAPlate\":-1,\"modGrille\":-1,\"modSideSkirt\":-1,\"modEngine\":-1,\"modTrunk\":-1,\"modShifterLeavers\":-1,\"model\":842072999,\"modHood\":-1,\"modSpoilers\":-1}', 'steam:11000010ddba29f', 1, 'Garage_Centre', 0, 'car', 'P37', 'car', 'police', 0, 0, 0, 0),
('{\"modArmor\":-1,\"modDial\":-1,\"fuelLevel\":94.264022827148,\"modSeats\":-1,\"modTransmission\":-1,\"color1\":134,\"modTank\":-1,\"modVanityPlate\":-1,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modTrunk\":-1,\"modAPlate\":-1,\"plateIndex\":4,\"modRoof\":-1,\"modSideSkirt\":-1,\"wheels\":0,\"plate\":\"PAC 891\",\"color2\":134,\"model\":-61406477,\"modAirFilter\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"modXenon\":false,\"extras\":{\"2\":false,\"3\":false,\"1\":true,\"6\":false,\"7\":false,\"4\":false,\"5\":false,\"8\":false,\"9\":false,\"11\":false,\"10\":false,\"12\":false},\"modSpeakers\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modSmokeEnabled\":false,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"dirtLevel\":0.0,\"wheelColor\":140,\"modSuspension\":-1,\"modDoorSpeaker\":-1,\"modSpoilers\":-1,\"health\":1000,\"modTurbo\":false,\"modGrille\":-1,\"modRightFender\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modPlateHolder\":-1,\"modHood\":-1,\"modBrakes\":-1,\"neonColor\":[255,0,255],\"modHorns\":-1,\"modLivery\":0,\"modTrimA\":-1,\"modAerials\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"modExhaust\":-1,\"bodyHealth\":1000.0,\"engineHealth\":1000.0,\"modEngine\":-1,\"modArchCover\":-1,\"modWindows\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"modBackWheels\":-1}', 'steam:110000105bca616', 1, 'Garage_Centre', 0, 'car', 'PAC 891', 'car', 'police', 0, 0, 0, 0),
('{\"modSpoilers\":-1,\"extras\":{\"10\":true,\"5\":false,\"7\":false,\"2\":false,\"12\":false},\"modBrakes\":-1,\"modAerials\":-1,\"modAPlate\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"modLivery\":0,\"modTrimB\":-1,\"wheels\":0,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modArchCover\":-1,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modFender\":-1,\"plate\":\"PCT 201\",\"dirtLevel\":2.0,\"modFrame\":-1,\"modFrontBumper\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modSeats\":-1,\"modHydrolic\":-1,\"modSmokeEnabled\":false,\"modHorns\":-1,\"modHood\":-1,\"model\":-1627000575,\"modSuspension\":-1,\"modEngine\":-1,\"modXenon\":false,\"modRoof\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modSpeakers\":-1,\"color1\":111,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modVanityPlate\":-1,\"modShifterLeavers\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"health\":1000,\"modDial\":-1,\"modEngineBlock\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":0,\"modTank\":-1,\"pearlescentColor\":1}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'PCT 201', 'car', 'police', 0, 0, 0, 0),
('{\"color1\":0,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":{\"6\":false,\"10\":false,\"4\":false,\"5\":false,\"8\":false,\"9\":false,\"12\":false,\"11\":true,\"2\":false,\"3\":false,\"7\":false,\"1\":false},\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"modTransmission\":2,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"plateIndex\":4,\"pearlescentColor\":0,\"modTrimA\":-1,\"modBrakes\":2,\"modTank\":-1,\"modSuspension\":3,\"wheelColor\":0,\"color2\":0,\"modArmor\":4,\"health\":1000,\"modLivery\":1,\"modFrontBumper\":-1,\"dirtLevel\":0.42855310440063,\"modAerials\":-1,\"modDashboard\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modArchCover\":-1,\"wheels\":3,\"modTurbo\":1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"model\":-448292453,\"modEngineBlock\":-1,\"modRoof\":-1,\"modHorns\":-1,\"modEngine\":3,\"modFender\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modXenon\":false,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"QDN 457\",\"modShifterLeavers\":-1,\"windowTint\":4}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'QDN 457', 'car', 'police', 0, 0, 0, 0),
('{\"modWindows\":-1,\"neonColor\":[255,0,255],\"modAerials\":-1,\"modTank\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modExhaust\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0,\"pearlescentColor\":0,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modStruts\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"plate\":\"REQ 617\",\"modXenon\":false,\"modSeats\":-1,\"modDashboard\":-1,\"modDial\":-1,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modHydrolic\":-1,\"color1\":134,\"modRoof\":-1,\"tyreSmokeColor\":[255,255,255],\"extras\":{\"3\":false,\"1\":false,\"2\":true},\"modTrunk\":-1,\"modSpoilers\":-1,\"modTransmission\":-1,\"health\":1000,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modEngineBlock\":-1,\"modEngine\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"wheels\":3,\"modBrakes\":-1,\"modLivery\":1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modBackWheels\":-1,\"model\":-288708329,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"color2\":134,\"modTrimB\":-1,\"neonEnabled\":[false,false,false,false],\"modTurbo\":false,\"modFrontBumper\":-1,\"modAPlate\":-1,\"wheelColor\":156,\"modHorns\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modGrille\":-1,\"modFender\":-1}', 'steam:110000111e8e1e7', 1, 'Garage_Centre', 0, 'car', 'REQ 617', 'car', 'police', 0, 0, 0, 0),
('{\"modSpoilers\":-1,\"modAPlate\":-1,\"modBrakes\":-1,\"health\":1000,\"modDoorSpeaker\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"modExhaust\":-1,\"dirtLevel\":1.0,\"neonColor\":[255,0,255],\"bodyHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modFrame\":-1,\"model\":1912215274,\"plateIndex\":4,\"modAirFilter\":-1,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modFender\":-1,\"modTrimA\":-1,\"modRearBumper\":-1,\"modAerials\":-1,\"modOrnaments\":-1,\"modTrunk\":-1,\"modHorns\":-1,\"modEngine\":-1,\"modSpeakers\":-1,\"modTurbo\":false,\"modHydrolic\":-1,\"modGrille\":-1,\"wheelColor\":156,\"modTrimB\":-1,\"color1\":111,\"plate\":\"S-17 01\",\"extras\":{\"3\":true,\"2\":true,\"5\":true,\"10\":false,\"12\":true},\"modWindows\":-1,\"modXenon\":false,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modArchCover\":-1,\"modFrontBumper\":-1,\"color2\":0,\"pearlescentColor\":1,\"modDial\":-1,\"modPlateHolder\":-1,\"windowTint\":-1,\"wheels\":0,\"modTank\":-1,\"modLivery\":2,\"modSeats\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modRoof\":-1,\"fuelLevel\":84.080009460449,\"modShifterLeavers\":-1,\"modStruts\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modSteeringWheel\":-1,\"modSmokeEnabled\":false,\"modArmor\":-1,\"modHood\":-1}', 'steam:1100001045201b3', 1, 'Garage_Centre', 0, 'car', 'S-17 01', 'car', 'police', 0, 0, 0, 0),
('{\"wheels\":0,\"modAPlate\":-1,\"modGrille\":-1,\"health\":1000,\"modBackWheels\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"modFrame\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modExhaust\":-1,\"neonEnabled\":[false,false,false,false],\"modDashboard\":-1,\"model\":447530523,\"plateIndex\":0,\"modSpoilers\":-1,\"engineHealth\":1000.0,\"modSmokeEnabled\":false,\"dirtLevel\":0.0,\"modFender\":-1,\"modWindows\":-1,\"modRearBumper\":-1,\"modSuspension\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"modLivery\":0,\"modEngine\":-1,\"modStruts\":-1,\"modSpeakers\":-1,\"modBrakes\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"color1\":134,\"plate\":\"S-17 02\",\"extras\":{\"9\":false,\"8\":false,\"11\":false,\"10\":false,\"12\":false,\"1\":true,\"3\":false,\"2\":true,\"5\":false,\"4\":false,\"7\":false,\"6\":false},\"modRightFender\":-1,\"modXenon\":false,\"modTrimB\":-1,\"windowTint\":-1,\"modArchCover\":-1,\"color2\":134,\"modShifterLeavers\":-1,\"pearlescentColor\":134,\"modDial\":-1,\"modPlateHolder\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modHorns\":-1,\"modDoorSpeaker\":-1,\"modTurbo\":false,\"wheelColor\":134,\"modVanityPlate\":-1,\"modRoof\":-1,\"fuelLevel\":82.280014038086,\"modSteeringWheel\":-1,\"modTrimA\":-1,\"modTrunk\":-1,\"modEngineBlock\":-1,\"modTank\":-1,\"modSeats\":-1,\"modArmor\":-1,\"modSideSkirt\":-1}', 'steam:1100001045201b3', 1, 'Garage_Centre', 0, 'car', 'S-17 02', 'car', 'police', 0, 0, 0, 0),
('{\"dirtLevel\":4.0,\"modExhaust\":-1,\"tyreSmokeColor\":[255,255,255],\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modVanityPlate\":-1,\"color2\":0,\"plateIndex\":4,\"modGrille\":-1,\"modRearBumper\":-1,\"modRoof\":-1,\"modSideSkirt\":-1,\"modTrimB\":-1,\"modRightFender\":-1,\"windowTint\":-1,\"modTank\":-1,\"modXenon\":false,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"modPlateHolder\":-1,\"modSteeringWheel\":-1,\"modWindows\":-1,\"modHorns\":-1,\"neonColor\":[255,0,255],\"modSpeakers\":-1,\"modSeats\":-1,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrimA\":-1,\"wheelColor\":156,\"neonEnabled\":[false,false,false,false],\"modDial\":-1,\"modTurbo\":false,\"model\":-994755493,\"color1\":111,\"modAPlate\":-1,\"modArmor\":-1,\"modHydrolic\":-1,\"plate\":\"SFH 969\",\"modSpoilers\":-1,\"modEngine\":-1,\"modHood\":-1,\"modLivery\":0,\"modTrunk\":-1,\"modStruts\":-1,\"wheels\":1,\"modFrame\":-1,\"modArchCover\":-1,\"modFender\":-1,\"modAerials\":-1,\"pearlescentColor\":1,\"modBrakes\":-1,\"modTransmission\":-1,\"modFrontWheels\":-1,\"modFrontBumper\":-1,\"modShifterLeavers\":-1,\"health\":1000,\"extras\":{\"2\":false,\"10\":false,\"5\":false,\"12\":false,\"7\":true}}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'SFH 969', 'car', 'police', 0, 0, 0, 0),
('{\"modTrimB\":-1,\"modSpoilers\":-1,\"modRoof\":-1,\"modShifterLeavers\":-1,\"modBackWheels\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"color2\":134,\"modBrakes\":-1,\"modTransmission\":-1,\"modSmokeEnabled\":false,\"modSteeringWheel\":-1,\"modTurbo\":false,\"fuelLevel\":79.972015380859,\"modFrontWheels\":-1,\"wheels\":0,\"modAPlate\":-1,\"pearlescentColor\":156,\"color1\":134,\"plateIndex\":4,\"model\":-2063562682,\"modHood\":-1,\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modSuspension\":-1,\"modXenon\":false,\"modFrontBumper\":-1,\"modLivery\":0,\"plate\":\"SLJ 176\",\"modSeats\":-1,\"modVanityPlate\":-1,\"windowTint\":-1,\"modStruts\":-1,\"modRearBumper\":-1,\"health\":1000,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modEngine\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modGrille\":-1,\"modHydrolic\":-1,\"modSpeakers\":-1,\"modHorns\":-1,\"modTrunk\":-1,\"modAirFilter\":-1,\"modAerials\":-1,\"modTrimA\":-1,\"modOrnaments\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modFender\":-1,\"modRightFender\":-1,\"modArchCover\":-1,\"dirtLevel\":2.0,\"modExhaust\":-1,\"extras\":{\"9\":true,\"8\":true,\"7\":true,\"6\":true,\"11\":true,\"4\":true,\"3\":true,\"2\":true,\"1\":true,\"12\":true,\"10\":true,\"5\":true}}', 'steam:1100001045201b3', 1, 'Garage_Centre', 0, 'car', 'SLJ 176', 'car', 'police', 0, 0, 0, 0),
('{\"modFrame\":-1,\"modAerials\":-1,\"modXenon\":false,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modTurbo\":1,\"modSeats\":-1,\"modEngineBlock\":-1,\"modFender\":-1,\"modExhaust\":-1,\"modTransmission\":2,\"dirtLevel\":0.012593860737979,\"pearlescentColor\":0,\"modTrimA\":-1,\"modFrontBumper\":-1,\"modSpeakers\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modShifterLeavers\":-1,\"modAirFilter\":-1,\"modSteeringWheel\":-1,\"modLivery\":1,\"model\":-1059115956,\"modRightFender\":-1,\"modTank\":-1,\"extras\":{\"8\":false,\"9\":false,\"2\":true,\"1\":true,\"10\":true,\"11\":false,\"12\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false},\"modArmor\":4,\"modVanityPlate\":-1,\"color2\":0,\"windowTint\":-1,\"modSmokeEnabled\":1,\"wheels\":0,\"modSideSkirt\":-1,\"health\":1000,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modAPlate\":-1,\"plate\":\"SP CHARG\",\"modHydrolic\":-1,\"modTrimB\":-1,\"plateIndex\":4,\"modWindows\":-1,\"modStruts\":-1,\"wheelColor\":0,\"modOrnaments\":-1,\"modBrakes\":2,\"modArchCover\":-1,\"modRoof\":-1,\"modHood\":-1,\"modDashboard\":-1,\"color1\":0,\"modDial\":-1,\"modEngine\":3,\"modGrille\":-1,\"modSpoilers\":-1,\"modDoorSpeaker\":-1,\"modBackWheels\":-1,\"modSuspension\":-1,\"modHorns\":-1,\"neonEnabled\":[false,false,false,false]}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'SP CHARG', 'car', 'police', 0, 0, 0, 0),
('{\"modShifterLeavers\":-1,\"wheelColor\":111,\"modFrame\":-1,\"modHood\":-1,\"modFrontWheels\":-1,\"modVanityPlate\":-1,\"modEngine\":3,\"modTurbo\":1,\"neonEnabled\":[false,false,false,false],\"plate\":\"SP GHOST\",\"modTransmission\":2,\"modSideSkirt\":-1,\"neonColor\":[255,0,255],\"modArmor\":4,\"health\":1000,\"modLivery\":0,\"color1\":113,\"modPlateHolder\":-1,\"modTrimA\":-1,\"modBrakes\":2,\"modFender\":-1,\"pearlescentColor\":0,\"modGrille\":-1,\"modSeats\":-1,\"modExhaust\":-1,\"wheels\":0,\"modXenon\":1,\"dirtLevel\":0.012858490459621,\"modBackWheels\":-1,\"modAPlate\":-1,\"modFrontBumper\":-1,\"color2\":0,\"windowTint\":-1,\"modDial\":-1,\"modSpeakers\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modTank\":-1,\"extras\":{\"11\":false,\"4\":false,\"1\":false,\"2\":false,\"10\":false,\"12\":true,\"9\":false,\"3\":false,\"7\":false,\"8\":false,\"5\":true,\"6\":false},\"modTrunk\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modHorns\":-1,\"modHydrolic\":-1,\"modTrimB\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modRearBumper\":-1,\"modWindows\":-1,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modAerials\":-1,\"model\":112512917,\"modDashboard\":-1}', 'steam:110000132580eb0', 1, 'Garage_Centre', 0, 'car', 'SP GHOST', 'car', 'police', 0, 0, 0, 0),
('{\"modEngine\":-1,\"modOrnaments\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modTank\":-1,\"modTransmission\":-1,\"modWindows\":-1,\"modSmokeEnabled\":false,\"modXenon\":false,\"modSpeakers\":-1,\"health\":1000,\"dirtLevel\":0.0,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modSpoilers\":-1,\"modBackWheels\":-1,\"modPlateHolder\":-1,\"modHorns\":-1,\"modBrakes\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modSuspension\":-1,\"neonColor\":[255,0,255],\"color1\":0,\"plate\":\"TQO 709\",\"modTurbo\":false,\"modSeats\":-1,\"modAerials\":-1,\"modFrontBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"modRightFender\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"color2\":0,\"model\":-1059115956,\"modTrunk\":-1,\"pearlescentColor\":0,\"wheelColor\":0,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modArchCover\":-1,\"wheels\":0,\"modAirFilter\":-1,\"modStruts\":-1,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modRoof\":-1,\"modDashboard\":-1,\"modHood\":-1,\"extras\":{\"10\":false,\"11\":true,\"1\":false,\"12\":false,\"5\":false,\"4\":false,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true},\"modGrille\":-1,\"plateIndex\":4,\"modFrame\":-1,\"modLivery\":1,\"modExhaust\":-1,\"modArmor\":-1}', 'steam:110000118dd2d76', 1, 'Garage_Centre', 0, 'car', 'TQO 709', 'car', 'police', 0, 0, 0, 0),
('{\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modTrimB\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modExhaust\":-1,\"modSmokeEnabled\":1,\"modSteeringWheel\":-1,\"extras\":{\"6\":false,\"10\":true,\"4\":true,\"5\":true,\"8\":true,\"9\":true,\"12\":true,\"11\":true,\"2\":true,\"3\":true,\"7\":true,\"1\":true},\"modFrame\":-1,\"modDial\":-1,\"modWindows\":-1,\"modTransmission\":2,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrontWheels\":-1,\"plateIndex\":4,\"pearlescentColor\":134,\"modTrimA\":-1,\"modBrakes\":2,\"modTank\":-1,\"modSuspension\":3,\"wheelColor\":0,\"color2\":134,\"modArmor\":4,\"health\":1000,\"modLivery\":1,\"modFrontBumper\":-1,\"dirtLevel\":2.1721239089966,\"modAerials\":-1,\"modDashboard\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modSpoilers\":-1,\"modArchCover\":-1,\"wheels\":1,\"modTurbo\":1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"model\":-311667528,\"modEngineBlock\":-1,\"modRoof\":-1,\"modHorns\":-1,\"modEngine\":3,\"modFender\":-1,\"neonColor\":[255,0,255],\"modSeats\":-1,\"modXenon\":1,\"modAirFilter\":-1,\"modOrnaments\":-1,\"plate\":\"VYK 558\",\"modShifterLeavers\":-1,\"windowTint\":-1}', 'steam:1100001068ef13c', 1, 'Garage_venise', 0, 'car', 'VYK 558', 'car', 'police', 0, 0, 0, 0),
('{\"plate\":\"XRM 785\",\"modArmor\":-1,\"modTrunk\":-1,\"modFrontBumper\":-1,\"modStruts\":-1,\"color2\":134,\"plateIndex\":4,\"modAirFilter\":-1,\"modXenon\":false,\"modGrille\":-1,\"modTank\":-1,\"wheels\":0,\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modDashboard\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modRightFender\":-1,\"modLivery\":0,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"health\":1000,\"dirtLevel\":0.011246376670897,\"modHydrolic\":-1,\"modFender\":-1,\"modSpeakers\":-1,\"model\":-1649544918,\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1,\"modSeats\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modRoof\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"extras\":{\"11\":false,\"12\":false,\"1\":true,\"2\":false,\"7\":false,\"8\":false,\"9\":false,\"10\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false},\"windowTint\":-1,\"modRearBumper\":-1,\"modDoorSpeaker\":-1,\"modWindows\":-1,\"modSmokeEnabled\":1,\"modSuspension\":-1,\"modTrimA\":-1,\"tyreSmokeColor\":[255,255,255],\"modEngine\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modTurbo\":false,\"modSteeringWheel\":-1,\"neonColor\":[255,0,255],\"modFrame\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"modBrakes\":-1,\"modDial\":-1,\"modTrimB\":-1,\"pearlescentColor\":156,\"modExhaust\":-1,\"modPlateHolder\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Paleto', 0, 'Ford F450 Ambulance AMR', 'XRM 785', 'car', 'ambulance', 0, 0, 0, 0),
('{\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"modTrimB\":-1,\"modDial\":-1,\"color1\":0,\"neonColor\":[255,0,255],\"modHorns\":-1,\"modShifterLeavers\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modAirFilter\":-1,\"modTrimA\":-1,\"modAerials\":-1,\"modFender\":-1,\"modArmor\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"plate\":\"XUL 174\",\"modPlateHolder\":-1,\"modLivery\":0,\"modTank\":-1,\"modSmokeEnabled\":1,\"modOrnaments\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modHood\":-1,\"dirtLevel\":0.13243879377842,\"windowTint\":-1,\"color2\":0,\"model\":-793815985,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":0,\"modTransmission\":-1,\"modEngine\":-1,\"modTurbo\":false,\"modGrille\":-1,\"modDashboard\":-1,\"modTrunk\":-1,\"modBrakes\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"extras\":{\"1\":true,\"2\":false,\"3\":false},\"modDoorSpeaker\":-1,\"modExhaust\":-1,\"modAPlate\":-1,\"wheels\":3,\"health\":1000,\"modXenon\":false,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modSuspension\":-1,\"modStruts\":-1,\"modSpeakers\":-1,\"pearlescentColor\":0,\"modSteeringWheel\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modHydrolic\":-1,\"modBackWheels\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Paleto', 0, 'Chevy Tahoe Wintergreen', 'XUL 174', 'car', 'police', 0, 0, 0, 0),
('{\"color2\":134,\"modSpeakers\":-1,\"modWindows\":-1,\"neonEnabled\":[false,false,false,false],\"wheelColor\":140,\"modSmokeEnabled\":false,\"modBrakes\":-1,\"modTank\":-1,\"modSpoilers\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modAirFilter\":-1,\"bodyHealth\":1000.0,\"fuelLevel\":91.280014038086,\"modSeats\":-1,\"neonColor\":[255,0,255],\"wheels\":0,\"modBackWheels\":-1,\"modFender\":-1,\"modHood\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"dirtLevel\":1.0,\"modXenon\":false,\"modEngineBlock\":-1,\"plate\":\"YDG 183\",\"modTrimA\":-1,\"modTransmission\":-1,\"extras\":{\"12\":false,\"11\":false,\"9\":false,\"2\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"1\":false,\"10\":false,\"3\":false,\"4\":true},\"modAerials\":-1,\"modAPlate\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modShifterLeavers\":-1,\"engineHealth\":1000.0,\"modStruts\":-1,\"model\":-2063562682,\"modDial\":-1,\"modArmor\":-1,\"modArchCover\":-1,\"modLivery\":1,\"modSteeringWheel\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modOrnaments\":-1,\"modDashboard\":-1,\"color1\":134,\"plateIndex\":4,\"pearlescentColor\":156,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"health\":1000,\"modFrame\":-1,\"modSuspension\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modEngine\":-1,\"modSideSkirt\":-1,\"windowTint\":-1,\"modTurbo\":false,\"modVanityPlate\":-1,\"modRoof\":-1}', 'steam:110000105bca616', 1, 'Garage_Centre', 0, 'car', 'YDG 183', 'car', 'police', 0, 0, 0, 0),
('{\"modRoof\":-1,\"modTurbo\":1,\"color2\":0,\"modWindows\":-1,\"dirtLevel\":0.85592067241669,\"color1\":0,\"modOrnaments\":-1,\"health\":1000,\"modPlateHolder\":-1,\"modSeats\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"extras\":{\"1\":true,\"3\":true,\"2\":true,\"5\":true,\"4\":true,\"7\":true,\"6\":true,\"9\":true,\"8\":true,\"11\":true,\"10\":true,\"12\":false},\"modVanityPlate\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modDial\":-1,\"modFrontWheels\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"wheelColor\":156,\"wheels\":6,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modSpoilers\":-1,\"plateIndex\":4,\"modBackWheels\":-1,\"modEngine\":-1,\"windowTint\":-1,\"modHorns\":-1,\"neonColor\":[255,0,255],\"modSmokeEnabled\":1,\"plate\":\"YTE 982\",\"modDoorSpeaker\":-1,\"modXenon\":false,\"modArchCover\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modDashboard\":-1,\"modSuspension\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modHood\":-1,\"modAerials\":-1,\"modEngineBlock\":-1,\"modTank\":-1,\"modTrimA\":-1,\"modTrimB\":-1,\"model\":178383100,\"modSideSkirt\":-1,\"modLivery\":0,\"modAPlate\":-1,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTransmission\":-1,\"pearlescentColor\":0}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'Sheriff PD Bike', 'YTE 982', 'car', 'police', 0, 0, 0, 0),
('{\"pearlescentColor\":156,\"fuelLevel\":79.232009887695,\"modArmor\":4,\"modHood\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"modDial\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"wheelColor\":140,\"engineHealth\":1000.0,\"modRoof\":-1,\"modFrontWheels\":-1,\"modAerials\":-1,\"windowTint\":-1,\"modTransmission\":2,\"tyreSmokeColor\":[255,255,255],\"modStruts\":-1,\"modTurbo\":1,\"modTank\":-1,\"wheels\":0,\"modGrille\":-1,\"dirtLevel\":0.17351016402245,\"modDashboard\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modSpoilers\":-1,\"modEngine\":3,\"modFender\":-1,\"modPlateHolder\":-1,\"modXenon\":1,\"extras\":{\"5\":true,\"6\":true,\"3\":true,\"4\":true,\"1\":true,\"2\":true,\"12\":true,\"11\":true,\"10\":true,\"9\":true,\"7\":true,\"8\":true},\"modSmokeEnabled\":1,\"modSuspension\":3,\"plateIndex\":4,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modBrakes\":2,\"modExhaust\":-1,\"modTrimA\":-1,\"plate\":\"Z\",\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modSeats\":-1,\"modAirFilter\":-1,\"model\":-2063562682,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modHydrolic\":-1,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modAPlate\":-1,\"modLivery\":1,\"health\":1000,\"color2\":134,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1}', 'steam:110000112969e8f', 1, 'Garage_Centre', 0, 'car', 'Z', 'car', 'police', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles_old`
--

CREATE TABLE `owned_vehicles_old` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `state of the car` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the car',
  `finance` int(32) NOT NULL DEFAULT 0,
  `financetimer` int(32) NOT NULL DEFAULT 0,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `jamstate` tinyint(11) NOT NULL DEFAULT 0,
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `owned_vehicles_old`
--

INSERT INTO `owned_vehicles_old` (`id`, `vehicle`, `owner`, `plate`, `state of the car`, `state`, `finance`, `financetimer`, `type`, `job`, `stored`, `jamstate`, `fourrieremecano`, `vehiclename`) VALUES
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '02JWM791', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010c2ebf86', '06VHJ277', 0, 1, 64350, 2143, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000112969e8f', '08JTP908', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000011a2fc3d0', '08RQZ688', 0, 1, 64350, 2850, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '24AAY727', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '25OHR247', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010a01bdb9', '29FJK237', 0, 1, 148500, 2860, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:1100001068ef13c', '87QJY824', 0, 1, 22770, 2241, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'DCD 740', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'E4 OFP', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'JYW 987', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'K9', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'NKJ 338', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010a01bdb9', 'O1', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'SEK 992', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'TUH 503', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:1100001068ef13c', 'TYH 041', 0, 1, 0, 0, 'car', 'ambulance', 1, 0, 0, 'voiture');

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_app_chat`
--

INSERT INTO `phone_app_chat` (`id`, `channel`, `message`, `time`) VALUES
(1, 'packopills', 'Selling a pack of pills', '2019-11-17 06:26:31'),
(2, 'packopills', 'Hey', '2019-11-17 06:29:47'),
(3, 'packopills', 'I\'m buying', '2019-11-17 06:29:49'),
(4, 'packopills', 'Okay how much', '2019-11-17 06:30:32'),
(5, 'packopills', 'Its Mostly Fentnayl', '2019-11-17 06:30:56'),
(6, 'packopills', 'We will take nothing below 500G', '2019-11-17 06:31:45'),
(7, 'packopills', '*it can be fake*', '2019-11-17 06:32:09'),
(8, 'packopills', 'How much in total do you guys have?', '2019-11-17 06:32:48'),
(9, 'packopills', 'about 500 Pills', '2019-11-17 06:33:23'),
(10, 'packopills', 'How much you want for them?', '2019-11-17 06:33:38'),
(11, 'packopills', 'We will take nothhing below 500 KK', '2019-11-17 06:33:57'),
(12, 'packopills', '500K', '2019-11-17 06:34:03'),
(13, 'packopills', 'I can arrange 500K for 500 Pills', '2019-11-17 06:34:35'),
(14, 'packopills', 'Where do you want to meet', '2019-11-17 06:34:53'),
(15, 'packopills', '3037 ASAP', '2019-11-17 06:36:52'),
(16, 'packopills', 'I\'ll be there in an hour. Wait.', '2019-11-17 06:37:27'),
(17, 'packopills', 'omw', '2019-11-17 06:38:59'),
(18, 'packopills', 'Make sure your not being followed', '2019-11-17 06:40:18'),
(19, 'packopills', 'K', '2019-11-17 06:40:35'),
(20, 'packopills', '...', '2019-11-17 06:40:50'),
(21, 'packopills', 'Here', '2019-11-17 06:44:19'),
(22, 'packopills', 'Be there soon', '2019-11-17 06:44:35'),
(23, 'packopills', 'Here', '2019-11-17 07:03:34'),
(24, 'packopills', 'In the hangar', '2019-11-17 07:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(1, '273-8087', '256-8589', 1, '2019-11-17 19:50:17', 0),
(3, '840-7465', '2738087', 1, '2019-11-17 21:39:45', 0),
(4, '273-8087', '840-7465', 1, '2019-11-17 21:39:51', 1),
(5, '840-7465', '273-8087', 0, '2019-11-17 21:39:51', 1),
(6, '273-8087', '840-7465', 1, '2019-11-17 22:52:06', 0),
(7, '840-7465', '273-8087', 0, '2019-11-17 22:52:06', 0),
(8, '101-5635', '415-8959', 1, '2020-01-01 04:20:01', 0),
(9, '415-8959', '101-5635', 0, '2020-01-01 04:20:01', 0),
(10, '101-5635', '373-0149', 1, '2020-01-01 04:20:30', 1),
(11, '373-0149', '101-5635', 0, '2020-01-01 04:20:30', 1),
(12, '101-5635', '373-0149', 1, '2020-01-01 04:20:31', 1),
(13, '373-0149', '101-5635', 0, '2020-01-01 04:20:31', 1),
(14, '101-5635', '373-0149', 1, '2020-01-01 04:20:32', 1),
(15, '373-0149', '101-5635', 0, '2020-01-01 04:20:32', 1),
(16, '101-5635', '373-0149', 1, '2020-01-01 04:21:18', 1),
(17, '373-0149', '101-5635', 0, '2020-01-01 04:21:18', 1),
(18, '101-5635', '273-8087', 1, '2020-01-04 03:04:36', 0),
(19, '273-8087', '101-5635', 0, '2020-01-04 03:04:36', 0),
(20, '101-5635', '273-8087', 1, '2020-01-04 03:05:02', 0),
(21, '273-8087', '101-5635', 0, '2020-01-04 03:05:02', 0),
(22, '101-5635', '273-8087', 1, '2020-01-04 03:05:10', 0),
(23, '273-8087', '101-5635', 0, '2020-01-04 03:05:10', 0),
(24, '101-5635', '273-8087', 1, '2020-01-04 03:05:18', 1),
(25, '273-8087', '101-5635', 0, '2020-01-04 03:05:18', 1),
(26, '273-8087', '840-7465', 1, '2020-01-04 03:19:12', 1),
(27, '840-7465', '273-8087', 0, '2020-01-04 03:19:12', 1),
(28, '840-7465', '273-8087', 1, '2020-01-04 03:20:06', 1),
(29, '273-8087', '840-7465', 0, '2020-01-04 03:20:06', 1),
(30, '840-7465', '273-8087', 1, '2020-01-04 04:41:23', 1),
(31, '273-8087', '840-7465', 0, '2020-01-04 04:41:23', 1),
(32, '840-7465', '273-8087', 1, '2020-01-04 04:41:24', 1),
(33, '273-8087', '840-7465', 0, '2020-01-04 04:41:24', 1),
(34, '373-0149', '505-3521', 1, '2020-01-04 09:24:53', 0),
(35, '505-3521', '373-0149', 0, '2020-01-04 09:24:53', 0),
(36, '505-3521', '373-0149', 1, '2020-01-04 09:25:19', 1),
(37, '373-0149', '505-3521', 0, '2020-01-04 09:25:19', 1),
(38, '273-8087', '101-5635', 1, '2020-01-11 06:56:49', 0),
(39, '101-5635', '273-8087', 0, '2020-01-11 06:56:49', 0),
(40, '101-5635', '273-8087', 1, '2020-01-18 06:47:06', 1),
(41, '273-8087', '101-5635', 0, '2020-01-18 06:47:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_users_contacts`
--

INSERT INTO `phone_users_contacts` (`id`, `identifier`, `number`, `display`) VALUES
(9, 'steam:110000132580eb0', '415-8959', 'Eric CPD'),
(10, 'steam:11000010ddba29f', '101-5635', 'k9'),
(11, 'steam:11000010a01bdb9', '415-8959', 'Donut Man'),
(12, 'steam:11000010ddba29f', '626-4798', 'Sticky'),
(13, 'steam:1100001068ef13c', '739-5135', 'Micheal'),
(14, 'steam:1100001068ef13c', '256-8589', 'Robert (noss)'),
(15, 'steam:11000010a01bdb9', '256-8589 ', 'Nos'),
(16, 'steam:1100001068ef13c', '451-8959', 'Eric'),
(17, 'steam:1100001068ef13c', '840-7465', 'Grant'),
(18, 'steam:1100001068ef13c', '373-0149', 'Joshua'),
(19, 'steam:110000132580eb0', '273-8087', 'Devil'),
(20, 'steam:110000132580eb0', '840-7465', 'Super'),
(22, 'steam:110000112969e8f', '273-8087', 'Devil'),
(23, 'steam:110000112969e8f', '256-8589', 'Ray'),
(24, 'steam:11000010ddba29f', '273-8087', 'Devil'),
(25, 'steam:11000010a01bdb9', '373-0149', 'K9'),
(26, 'steam:110000132580eb0', '101-5635', 'Sticky'),
(27, 'steam:11000010a01bdb9', '273-8087', 'Devil'),
(28, 'steam:1100001068ef13c', '101-5635', 'Robert B.'),
(29, 'steam:11000010a01bdb9', '840-7465', 'Grant'),
(30, 'steam:110000112969e8f', '101-5635', 'Robbie'),
(31, 'steam:110000132580eb0', '505-3521', 'P31Wells'),
(32, 'steam:110000118dd2d76', '3730149', 'k9 boss'),
(33, 'steam:110000112969e8f', '339-2972', 'Corey'),
(34, 'steam:110000112969e8f', '5053521', 'Dillon'),
(35, 'steam:110000112969e8f', '8018436', 'Turtle'),
(36, 'steam:1100001068ef13c', '339-2979', 'Corey Mattison');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `steam` varchar(50) NOT NULL,
  `playtime` int(11) NOT NULL,
  `firstjoined` varchar(50) NOT NULL,
  `lastplayed` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Apartment de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modyrn1Apartment', 'Apartment Modern 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modyrn2Apartment', 'Apartment Modern 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modyrn3Apartment', 'Apartment Modern 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Apartment Mody 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Apartment Mody 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Apartment Mody 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Apartment Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Apartment Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Apartment Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Apartment Persian 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Apartment Persian 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Apartment Persian 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Apartment Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Apartment Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Apartment Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Apartment Seductive 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Apartment Seductive 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Apartment Seductive 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Apartment Regal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Apartment Regal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Apartment Regal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Apartment Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Apartment Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Apartment Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `received_bans`
--

CREATE TABLE `received_bans` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `banned_by` varchar(255) DEFAULT NULL,
  `banned_on` varchar(255) DEFAULT NULL,
  `ban_expires` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `ID` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `connection` int(11) NOT NULL,
  `rcon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `server_actions`
--

CREATE TABLE `server_actions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_do` varchar(255) DEFAULT NULL,
  `action_ammount` varchar(255) DEFAULT NULL,
  `byadmin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'PDShop', 'coffee', 1),
(2, 'PDShop', 'donut', 1),
(3, 'PDShop', 'clip', 1),
(4, 'PDShop', 'armour', 1),
(5, 'PDShop', 'medikit', 0),
(6, 'CityHall', 'weapons_license1', 100),
(7, 'CityHall', 'weapons_license2', 200),
(8, 'CityHall', 'weapons_license3', 300),
(62, 'CityHall', 'hunting_license', 100),
(63, 'CityHall', 'fishing_license', 100),
(64, 'CityHall', 'diving_license', 50),
(65, 'CityHall', 'marriage_license', 5000),
(69, 'DMV', 'boating_license', 40),
(70, 'DMV', 'taxi_license', 175),
(71, 'DMV', 'pilot_license', 500),
(72, 'PDShop', 'radio', 0),
(73, 'DMV', 'licenseplate', 50),
(74, 'PDShop', 'gps', 1);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trunk_inventory`
--

INSERT INTO `trunk_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(7, 'DCD 740 ', '{\"coffre\":[{\"count\":6,\"name\":\"medikit\"},{\"count\":4,\"name\":\"clip\"}]}', 1),
(9, 'SEK 992 ', '{\"coffre\":[{\"count\":4,\"name\":\"medikit\"},{\"count\":6,\"name\":\"clip\"}]}', 1),
(10, '06VHJ277', '{\"weapons\":[]}', 1),
(16, '81OHZ637', '{}', 1),
(18, '83DBS050', '{\"coffre\":[]}', 1),
(21, 'C1 UNMRK', '{}', 1),
(25, ' E4OFP  ', '{\"weapons\":[{\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"ammo\":21},{\"name\":\"WEAPON_SPECIALCARBINE\",\"label\":\"Special carbine\",\"ammo\":0},{\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\",\"ammo\":252},{\"name\":\"WEAPON_FLAREGUN\",\"label\":\"Flaregun\",\"ammo\":20},{\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\",\"ammo\":256},{\"name\":\"WEAPON_PISTOL50\",\"label\":\"Pistol .50\",\"ammo\":0},{\"name\":\"WEAPON_ASSAULTSMG\",\"label\":\"Assault SMG\",\"ammo\":84},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\",\"ammo\":214}]}', 1),
(39, 'SP GHOST', '{\"coffre\":[{\"count\":20,\"name\":\"armour\"}],\"weapons\":[{\"label\":\"Pistol\",\"ammo\":9999,\"name\":\"WEAPON_PISTOL\"}]}', 1),
(41, '22OAJ106', '{}', 1),
(47, ' O9 K9  ', '{\"weapons\":[{\"ammo\":9999,\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\"},{\"ammo\":9999,\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\"},{\"ammo\":9999,\"label\":\"AP pistol\",\"name\":\"WEAPON_APPISTOL\"},{\"ammo\":9999,\"label\":\"Double-Action Revolver\",\"name\":\"WEAPON_DOUBLEACTION\"}],\"black_money\":[{\"amount\":43455}],\"coffre\":[{\"name\":\"armour\",\"count\":10},{\"name\":\"licenseplate\",\"count\":2},{\"name\":\"lighter\",\"count\":1},{\"name\":\"weapons_license1\",\"count\":1},{\"name\":\"weapons_license2\",\"count\":1},{\"name\":\"weapons_license3\",\"count\":1},{\"name\":\"cigarett\",\"count\":7},{\"name\":\"clip\",\"count\":205},{\"name\":\"donut\",\"count\":90},{\"name\":\"medikit\",\"count\":90},{\"name\":\"coffee\",\"count\":100},{\"name\":\"lockpick\",\"count\":9}]}', 1),
(48, 'O9 CHARG', '{\"coffre\":[{\"name\":\"donut\",\"count\":100},{\"name\":\"medikit\",\"count\":90},{\"name\":\"clip\",\"count\":100},{\"name\":\"armour\",\"count\":99},{\"name\":\"coffee\",\"count\":100},{\"name\":\"lockpick\",\"count\":1},{\"name\":\"lotteryticket\",\"count\":10}]}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(39, 'tpickles', 'stoner420', 'kermit.png'),
(40, 'CCPDChief', 'CCPD37', NULL),
(41, 'HHMC K9', '123456', NULL),
(42, 'Super', 'Sadie0508', 'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiM5Y2gtbPlAhUFFzQIHfjzCj0QjRx6BAgBEAQ&url=https%3A%2F%2Fimgur.com%2Fgallery%2Fn4BAiU4&psig=AOvVaw37Ry-1QIi6eye82EcpQl2S&ust=1571955644077203'),
(43, 'Robert_Kerr', 'Dennis1993', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_likes`
--

INSERT INTO `twitter_likes` (`id`, `authorId`, `tweetId`) VALUES
(6, 42, 170),
(7, 42, 173),
(8, 42, 172),
(9, 42, 171),
(10, 42, 174),
(11, 42, 175),
(12, 42, 176),
(13, 42, 177);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 39, 'steam:11000010a01bdb9', 'New city who dis?', '2019-08-09 08:24:48', 0),
(171, 42, 'steam:110000112969e8f', 'Heyyyyyy', '2019-10-23 22:19:15', 1),
(172, 43, 'steam:11000010c2ebf86', '@super I\'m coming for you, watch out!', '2019-10-23 22:19:32', 1),
(173, 43, 'steam:11000010c2ebf86', 'hahah @super can\'t even access his own office #loser', '2019-10-23 22:38:42', 1),
(174, 43, 'steam:11000010c2ebf86', '#devilbrokeit', '2019-10-23 23:19:27', 1),
(175, 42, 'steam:110000112969e8f', 'We\'re live streaming!', '2019-10-25 01:32:56', 1),
(176, 39, 'steam:11000010a01bdb9', 'Erics Driving is #horrible', '2019-10-25 01:33:04', 1),
(177, 40, 'steam:11000010ddba29f', '@tpickles wash my truck', '2019-10-25 01:33:48', 1),
(178, 42, 'steam:110000112969e8f', 'Y\'all here about that Shootout in Sandy?! Crazy!!!!!', '2019-11-17 07:13:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `rank` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `steamid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeplayed` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `online` int(10) NOT NULL DEFAULT 0,
  `server` int(10) NOT NULL DEFAULT 1,
  `is_dead` tinyint(1) DEFAULT 0,
  `DmvTest` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Required',
  `lastpos` varchar(255) COLLATE utf8mb4_bin DEFAULT '{706.98700000000, -964.94200000000,  31.39550000000000, 142.503463745117}',
  `skin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `ID`, `rank`, `steamid`, `license`, `money`, `name`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `last_property`, `animal`, `timeplayed`, `online`, `server`, `is_dead`, `DmvTest`, `lastpos`, `skin`) VALUES
('steam:110000118dd2d76', 1, '', '', 'license:fde9971e001f43afb8c2737b9deddb41decff6d0', 500, 'the_gta_killerv', 'unemployed', 0, NULL, NULL, 2500, 0, 'user', '[{\"name\":\"hunger\",\"percent\":49.38,\"val\":493800},{\"name\":\"thirst\",\"percent\":49.38,\"val\":493800},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', 'Dillon', 'Wells', '12271992', 'M', '72', '101-5635', NULL, NULL, '0', 0, 1, 0, 'Required', '{422.93078613281, -970.70458984375,  30.739030838013, 79.178413391113}', NULL),
('steam:110000112969e8f', 2, '', '', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 500, 'Super', 'unemployed', 0, NULL, NULL, 2500, 0, 'user', '[{\"name\":\"hunger\",\"percent\":49.51,\"val\":495100},{\"name\":\"thirst\",\"percent\":49.51,\"val\":495100},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', 'Steven', 'Super', '05/08/2000', 'M', '90', '273-8087', NULL, NULL, '0', 0, 1, 0, 'Required', '{439.95935058594, -978.85827636719,  30.68932723999, 111.15930175781}', 0),
('steam:110000132580eb0', 3, '', '', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 500, 'K9Marine', 'unemployed', 0, NULL, NULL, 2500, 0, 'user', '[{\"val\":976400,\"name\":\"hunger\",\"percent\":97.64},{\"val\":976400,\"name\":\"thirst\",\"percent\":97.64},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', 'Jak', 'Fulton', '10-10-1988', 'M', '74', '626-4798', NULL, NULL, '0', 0, 1, 0, 'Required', '{706.98700000000, -964.94200000000,  31.39550000000000, 142.503463745117}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(1, 'steam:110000132580eb0', 'black_money', 0),
(2, 'steam:11000010a01bdb9', 'black_money', 0),
(3, 'steam:11000010ddba29f', 'black_money', 4754),
(4, 'steam:110000112969e8f', 'black_money', 8199),
(5, 'steam:1100001068ef13c', 'black_money', 0),
(6, 'steam:11000010c2ebf86', 'black_money', 173),
(7, 'steam:11000010a078bc7', 'black_money', 0),
(8, 'steam:11000013d51c2ff', 'black_money', 0),
(9, 'steam:11000011a2fc3d0', 'black_money', 0),
(10, 'steam:1100001048af48f', 'black_money', 0),
(11, 'steam:11000013bc17e0e', 'black_money', 0),
(12, 'steam:1100001079cf4ab', 'black_money', 0),
(13, 'steam:11000013b85ca5f', 'black_money', 0),
(14, 'steam:110000116c3388b', 'black_money', 0),
(15, 'steam:11000010e305422', 'black_money', 0),
(16, 'steam:110000106e27137', 'black_money', 0),
(17, 'steam:11000010b1872b5', 'black_money', 0),
(18, 'steam:110000107d0cc24', 'black_money', 0),
(19, 'steam:110000136d5a1bd', 'black_money', 0),
(20, 'steam:11000010a92984b', 'black_money', 0),
(21, 'steam:110000118dd2d76', 'black_money', 0),
(22, 'steam:110000111e8e1e7', 'black_money', 0),
(23, 'steam:1100001339af9da', 'black_money', 0),
(24, 'steam:110000135837b81', 'black_money', 0),
(25, 'steam:1100001157932f6', 'black_money', 0),
(26, 'steam:11000010ced5d9e', 'black_money', 0),
(27, 'steam:11000013b83354d', 'black_money', 0),
(28, 'steam:110000136177a4e', 'black_money', 0),
(29, 'steam:110000100a858c4', 'black_money', 0),
(30, 'steam:11000010d0d2e73', 'black_money', 0),
(31, 'steam:110000114917791', 'black_money', 0),
(32, 'steam:110000132e9c0db', 'black_money', 0),
(33, 'steam:110000105bca616', 'black_money', 0),
(34, 'steam:11000013e5f9bf9', 'black_money', 0),
(35, 'steam:1100001045201b3', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin_notes`
--

CREATE TABLE `user_admin_notes` (
  `id` int(11) NOT NULL,
  `note` longblob DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `note_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_documents`
--

INSERT INTO `user_documents` (`id`, `owner`, `data`) VALUES
(4, 'steam:110000132580eb0', '{\"headerJobLabel\":\"AMR\",\"headerSubtitle\":\"Official document when person refuses treatment by Medical Personal. Must be given to citizen by Medical Personnel\",\"headerDateCreated\":\"04/11/2019 22:16:5\",\"headerJobGrade\":\"Medical Director\",\"headerLastName\":\"Pickles\",\"submittable\":true,\"headerFirstName\":\"Tommie\",\"headerDateOfBirth\":\"12/28/1988\",\"headerTitle\":\"REFUSAL OF MEDICAL TREATMENT\",\"elements\":[{\"label\":\"INJURED FIRSTNAME\",\"value\":\"Jak \",\"type\":\"input\",\"elementid\":\"_m0\"},{\"label\":\"INJURED LASTNAME\",\"value\":\"Fulton\",\"type\":\"input\",\"elementid\":\"_m1\"},{\"label\":\"DATE\",\"value\":\"11/4/2019\",\"type\":\"input\",\"elementid\":\"_m2\",\"can_be_empty\":false},{\"label\":\"MEDICAL NOTES\",\"value\":\"THE SIGNED PATIENT REFUSED MEDICAL TREATMENT BY MEDICAL PERSONNEL. I HAVE ADVISED PATIENT TO SEEK MEDICAL ATTENTION AT EARLIEST CONVIENCE. TREATING PERSONNEL(INSERT NAME HERE)HAS FILED THIS DOCUMENT WITH THE DEPARTMENT AND GIVEN A COPY TO THE PATIENT\",\"can_be_edited\":true,\"elementid\":\"_m3\",\"type\":\"textarea\"},{\"label\":\"PATIENT INFORMATION\",\"value\":\"This left blank for a reason\",\"can_be_edited\":true,\"elementid\":\"_m4\",\"type\":\"textarea\"}]}'),
(5, 'steam:1100001068ef13c', '{\"headerDateCreated\":\"23/11/2019 17:37:0\",\"headerSubtitle\":\"Official document when person refuses treatment by Medical Personal. Must be given to citizen by Medical Personnel\",\"signed\":true,\"headerFirstName\":\"William\",\"submittable\":true,\"headerTitle\":\"REFUSAL OF MEDICAL TREATMENT\",\"headerLastName\":\"Woodard\",\"elements\":[{\"label\":\"INJURED FIRSTNAME\",\"value\":\"Super\",\"type\":\"input\",\"elementid\":\"_m4\"},{\"label\":\"INJURED LASTNAME\",\"value\":\"Steve\",\"type\":\"input\",\"elementid\":\"_m5\"},{\"value\":\"11/29/2019\",\"label\":\"DATE\",\"can_be_empty\":false,\"elementid\":\"_m6\",\"type\":\"input\"},{\"value\":\"N/A\",\"can_be_edited\":true,\"label\":\"PATIENT INFORMATION\",\"elementid\":\"_m7\",\"type\":\"textarea\"},{\"value\":\"THE SIGNED PATIENT REFUSED MEDICAL TREATMENT BY MEDICAL PERSONNEL. I HAVE ADVISED PATIENT TO SEEK MEDICAL ATTENTION AT EARLIEST CONVIENCE. TREATING PERSONNEL William Woodard HAS FILED THIS DOCUMENT WITH THE DEPARTMENT AND GIVEN A COPY TO THE PATIENT\",\"can_be_edited\":true,\"label\":\"AKNOWLEDGEMENT\",\"elementid\":\"_m8\",\"type\":\"textarea\"}],\"headerJobLabel\":\"AMR\",\"headerJobGrade\":\"Deputy Medical Director\",\"headerDateOfBirth\":\"7/27/1989\"}'),
(7, 'steam:1100001068ef13c', '{\"headerDateCreated\":\"14/12/2019 0:32:35\",\"headerJobLabel\":\"AMR\",\"headerSubtitle\":\"Official medical report provided by a psychologist.\",\"signed\":true,\"submittable\":true,\"headerDateOfBirth\":\"7/27/1989\",\"headerTitle\":\"MEDICAL REPORT - PSYCHOLOGY\",\"headerLastName\":\"Woodard\",\"headerJobGrade\":\"Deputy Medical Director\",\"headerFirstName\":\"William\",\"elements\":[{\"label\":\"INSURED FIRSTNAME\",\"elementid\":\"_m0\",\"type\":\"input\",\"value\":\"Tommie\"},{\"label\":\"INSURED LASTNAME\",\"elementid\":\"_m1\",\"type\":\"input\",\"value\":\"Pickles\"},{\"elementid\":\"_m2\",\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"value\":\"12/20\",\"type\":\"input\"},{\"label\":\"MEDICAL NOTES\",\"elementid\":\"_m3\",\"type\":\"textarea\",\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED MENTALLY HEALTHY BY THE LOWEST APPROVED PSYCHOLOGY STANDARDS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\"}]}'),
(8, 'steam:11000010a01bdb9', '{\"headerDateCreated\":\"14/12/2019 0:32:35\",\"headerJobLabel\":\"AMR\",\"submittable\":true,\"signed\":true,\"headerSubtitle\":\"Official medical report provided by a psychologist.\",\"headerDateOfBirth\":\"7/27/1989\",\"headerTitle\":\"MEDICAL REPORT - PSYCHOLOGY\",\"headerLastName\":\"Woodard\",\"headerJobGrade\":\"Deputy Medical Director\",\"headerFirstName\":\"William\",\"elements\":[{\"value\":\"Tommie\",\"elementid\":\"_m0\",\"label\":\"INSURED FIRSTNAME\",\"type\":\"input\"},{\"value\":\"Pickles\",\"elementid\":\"_m1\",\"label\":\"INSURED LASTNAME\",\"type\":\"input\"},{\"elementid\":\"_m2\",\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"value\":\"12/20\",\"type\":\"input\"},{\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED MENTALLY HEALTHY BY THE LOWEST APPROVED PSYCHOLOGY STANDARDS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\",\"elementid\":\"_m3\",\"label\":\"MEDICAL NOTES\",\"type\":\"textarea\"}]}'),
(9, 'steam:1100001068ef13c', '{\"headerJobGrade\":\"Deputy Medical Director\",\"signed\":true,\"submittable\":true,\"headerLastName\":\"Woodard\",\"headerSubtitle\":\"Official medical report provided by a psychologist.\",\"headerJobLabel\":\"AMR\",\"elements\":[{\"elementid\":\"_m16\",\"label\":\"INSURED FIRSTNAME\",\"type\":\"input\",\"value\":\"Mami\"},{\"elementid\":\"_m17\",\"label\":\"INSURED LASTNAME\",\"type\":\"input\",\"value\":\"Strippa\"},{\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"elementid\":\"_m18\",\"value\":\"12/20\",\"type\":\"input\"},{\"elementid\":\"_m19\",\"label\":\"MEDICAL NOTES\",\"type\":\"textarea\",\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED MENTALLY HEALTHY BY THE LOWEST APPROVED PSYCHOLOGY STANDARDS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\"}],\"headerTitle\":\"MEDICAL REPORT - PSYCHOLOGY\",\"headerDateOfBirth\":\"7/27/1989\",\"headerFirstName\":\"William\",\"headerDateCreated\":\"20/12/2019 23:49:28\"}'),
(10, 'steam:110000132580eb0', '{\"headerJobGrade\":\"Deputy Medical Director\",\"signed\":true,\"submittable\":true,\"headerLastName\":\"Woodard\",\"headerSubtitle\":\"Official medical report provided by a psychologist.\",\"headerJobLabel\":\"AMR\",\"elements\":[{\"value\":\"Mami\",\"label\":\"INSURED FIRSTNAME\",\"elementid\":\"_m16\",\"type\":\"input\"},{\"value\":\"Strippa\",\"label\":\"INSURED LASTNAME\",\"elementid\":\"_m17\",\"type\":\"input\"},{\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"elementid\":\"_m18\",\"value\":\"12/20\",\"type\":\"input\"},{\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED MENTALLY HEALTHY BY THE LOWEST APPROVED PSYCHOLOGY STANDARDS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\",\"label\":\"MEDICAL NOTES\",\"elementid\":\"_m19\",\"type\":\"textarea\"}],\"headerDateOfBirth\":\"7/27/1989\",\"headerTitle\":\"MEDICAL REPORT - PSYCHOLOGY\",\"headerFirstName\":\"William\",\"headerDateCreated\":\"20/12/2019 23:49:28\"}'),
(11, 'steam:1100001068ef13c', '{\"headerJobGrade\":\"Deputy Medical Director\",\"signed\":true,\"submittable\":true,\"headerLastName\":\"Woodard\",\"headerSubtitle\":\"Official medical report provided by a pathologist.\",\"headerJobLabel\":\"AMR\",\"elements\":[{\"elementid\":\"_m32\",\"label\":\"INSURED FIRSTNAME\",\"type\":\"input\",\"value\":\"Mami\"},{\"elementid\":\"_m33\",\"label\":\"INSURED LASTNAME\",\"type\":\"input\",\"value\":\"Strippa\"},{\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"elementid\":\"_m34\",\"value\":\"12/20\",\"type\":\"input\"},{\"elementid\":\"_m35\",\"label\":\"MEDICAL NOTES\",\"type\":\"textarea\",\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED HEALTHY WITH NO DETECTED LONGTERM CONDITIONS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\"}],\"headerTitle\":\"MEDICAL REPORT - PATHOLOGY\",\"headerDateOfBirth\":\"7/27/1989\",\"headerFirstName\":\"William\",\"headerDateCreated\":\"21/12/2019 0:21:23\"}'),
(12, 'steam:110000112969e8f', '{\"headerJobGrade\":\"Deputy Medical Director\",\"signed\":true,\"submittable\":true,\"headerLastName\":\"Woodard\",\"headerSubtitle\":\"Official medical report provided by a pathologist.\",\"headerJobLabel\":\"AMR\",\"elements\":[{\"value\":\"Mami\",\"label\":\"INSURED FIRSTNAME\",\"elementid\":\"_m32\",\"type\":\"input\"},{\"value\":\"Strippa\",\"label\":\"INSURED LASTNAME\",\"elementid\":\"_m33\",\"type\":\"input\"},{\"label\":\"VALID UNTIL\",\"can_be_empty\":false,\"elementid\":\"_m34\",\"value\":\"12/20\",\"type\":\"input\"},{\"value\":\"THE AFOREMENTIONED INSURED CITIZEN WAS TESTED BY A HEALTHCARE OFFICIAL AND DETERMINED HEALTHY WITH NO DETECTED LONGTERM CONDITIONS. THIS REPORT IS VALID UNTIL THE AFOREMENTIONED EXPIRATION DATE.\",\"label\":\"MEDICAL NOTES\",\"elementid\":\"_m35\",\"type\":\"textarea\"}],\"headerDateOfBirth\":\"7/27/1989\",\"headerTitle\":\"MEDICAL REPORT - PATHOLOGY\",\"headerFirstName\":\"William\",\"headerDateCreated\":\"21/12/2019 0:21:23\"}');

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:110000118dd2d76', 'metreshooter', 0),
(2, 'steam:110000118dd2d76', 'bread', 0),
(3, 'steam:110000118dd2d76', 'sprite', 0),
(4, 'steam:110000118dd2d76', 'diamond', 0),
(5, 'steam:110000118dd2d76', 'cannabis', 0),
(6, 'steam:110000118dd2d76', 'breathalyzer', 0),
(7, 'steam:110000118dd2d76', 'flashlight', 0),
(8, 'steam:110000118dd2d76', 'cutted_wood', 0),
(9, 'steam:110000118dd2d76', 'licenseplate', 0),
(10, 'steam:110000118dd2d76', 'packaged_plank', 0),
(11, 'steam:110000118dd2d76', 'mojito', 0),
(12, 'steam:110000118dd2d76', 'bandage', 0),
(13, 'steam:110000118dd2d76', 'energy', 0),
(14, 'steam:110000118dd2d76', 'defibrillateur', 0),
(15, 'steam:110000118dd2d76', 'weapons_license2', 0),
(16, 'steam:110000118dd2d76', 'WEAPON_BAT', 0),
(17, 'steam:110000118dd2d76', 'armour', 0),
(18, 'steam:110000118dd2d76', 'slaughtered_chicken', 0),
(19, 'steam:110000118dd2d76', 'coca', 0),
(20, 'steam:110000118dd2d76', 'rhum', 0),
(21, 'steam:110000118dd2d76', 'lockpick', 0),
(22, 'steam:110000118dd2d76', 'blackberry', 0),
(23, 'steam:110000118dd2d76', 'wrench', 0),
(24, 'steam:110000118dd2d76', 'menthe', 0),
(25, 'steam:110000118dd2d76', 'gold', 0),
(26, 'steam:110000118dd2d76', 'vodkafruit', 0),
(27, 'steam:110000118dd2d76', 'yusuf', 0),
(28, 'steam:110000118dd2d76', 'coke_pooch', 0),
(29, 'steam:110000118dd2d76', '9mm_rounds', 0),
(30, 'steam:110000118dd2d76', 'washed_stone', 0),
(31, 'steam:110000118dd2d76', 'scratchoff', 0),
(32, 'steam:110000118dd2d76', 'radio', 0),
(33, 'steam:110000118dd2d76', 'plongee1', 0),
(34, 'steam:110000118dd2d76', 'opium', 0),
(35, 'steam:110000118dd2d76', 'gym_membership', 0),
(36, 'steam:110000118dd2d76', 'chips', 0),
(37, 'steam:110000118dd2d76', 'cola', 0),
(38, 'steam:110000118dd2d76', 'dabs', 0),
(39, 'steam:110000118dd2d76', 'rhumfruit', 0),
(40, 'steam:110000118dd2d76', 'whiskey', 0),
(41, 'steam:110000118dd2d76', 'crack', 0),
(42, 'steam:110000118dd2d76', 'weed_pooch', 0),
(43, 'steam:110000118dd2d76', 'bolpistache', 0),
(44, 'steam:110000118dd2d76', 'whiskycoca', 0),
(45, 'steam:110000118dd2d76', 'binoculars', 0),
(46, 'steam:110000118dd2d76', 'coke', 0),
(47, 'steam:110000118dd2d76', 'marriage_license', 0),
(48, 'steam:110000118dd2d76', 'fabric', 0),
(49, 'steam:110000118dd2d76', 'lotteryticket', 0),
(50, 'steam:110000118dd2d76', 'essence', 0),
(51, 'steam:110000118dd2d76', 'tacos', 0),
(52, 'steam:110000118dd2d76', 'martini', 0),
(53, 'steam:110000118dd2d76', 'cheesebows', 0),
(54, 'steam:110000118dd2d76', 'limonade', 0),
(55, 'steam:110000118dd2d76', 'pizza', 0),
(56, 'steam:110000118dd2d76', 'WEAPON_FLASHLIGHT', 0),
(57, 'steam:110000118dd2d76', 'carokit', 0),
(58, 'steam:110000118dd2d76', 'croquettes', 0),
(59, 'steam:110000118dd2d76', 'firstaidpass', 0),
(60, 'steam:110000118dd2d76', 'saucisson', 0),
(61, 'steam:110000118dd2d76', 'donut', 0),
(62, 'steam:110000118dd2d76', 'packaged_chicken', 0),
(63, 'steam:110000118dd2d76', 'lighter', 0),
(64, 'steam:110000118dd2d76', 'painkiller', 0),
(65, 'steam:110000118dd2d76', 'mlic', 0),
(66, 'steam:110000118dd2d76', 'litter_pooch', 0),
(67, 'steam:110000118dd2d76', 'pearl_pooch', 0),
(68, 'steam:110000118dd2d76', 'bolnoixcajou', 0),
(69, 'steam:110000118dd2d76', 'golem', 0),
(70, 'steam:110000118dd2d76', 'sportlunch', 0),
(71, 'steam:110000118dd2d76', 'fanta', 0),
(72, 'steam:110000118dd2d76', 'lsd', 0),
(73, 'steam:110000118dd2d76', 'ice', 0),
(74, 'steam:110000118dd2d76', 'coffee', 0),
(75, 'steam:110000118dd2d76', 'turtle_pooch', 0),
(76, 'steam:110000118dd2d76', 'turtle', 0),
(77, 'steam:110000118dd2d76', 'firstaidkit', 0),
(78, 'steam:110000118dd2d76', 'petrol_raffin', 0),
(79, 'steam:110000118dd2d76', 'taxi_license', 0),
(80, 'steam:110000118dd2d76', 'clip', 0),
(81, 'steam:110000118dd2d76', 'opium_pooch', 0),
(82, 'steam:110000118dd2d76', 'WEAPON_PUMPSHOTGUN', 0),
(83, 'steam:110000118dd2d76', 'fixkit', 0),
(84, 'steam:110000118dd2d76', 'powerade', 0),
(85, 'steam:110000118dd2d76', 'cigarett', 0),
(86, 'steam:110000118dd2d76', 'pilot_license', 0),
(87, 'steam:110000118dd2d76', 'medikit', 0),
(88, 'steam:110000118dd2d76', 'drpepper', 0),
(89, 'steam:110000118dd2d76', 'jagerbomb', 0),
(90, 'steam:110000118dd2d76', 'baconburger', 0),
(91, 'steam:110000118dd2d76', 'iron', 0),
(92, 'steam:110000118dd2d76', 'gps', 0),
(93, 'steam:110000118dd2d76', 'shotgun_shells', 0),
(94, 'steam:110000118dd2d76', 'plongee2', 0),
(95, 'steam:110000118dd2d76', 'WEAPON_PISTOL', 0),
(96, 'steam:110000118dd2d76', 'WEAPON_KNIFE', 0),
(97, 'steam:110000118dd2d76', 'meth_pooch', 0),
(98, 'steam:110000118dd2d76', 'WEAPON_STUNGUN', 0),
(99, 'steam:110000118dd2d76', 'marijuana', 0),
(100, 'steam:110000118dd2d76', 'litter', 0),
(101, 'steam:110000118dd2d76', 'nitrocannister', 0),
(102, 'steam:110000118dd2d76', 'soda', 0),
(103, 'steam:110000118dd2d76', 'jager', 0),
(104, 'steam:110000118dd2d76', 'stone', 0),
(105, 'steam:110000118dd2d76', 'vodkaenergy', 0),
(106, 'steam:110000118dd2d76', 'pcp', 0),
(107, 'steam:110000118dd2d76', 'dlic', 0),
(108, 'steam:110000118dd2d76', 'cdl', 0),
(109, 'steam:110000118dd2d76', 'whool', 0),
(110, 'steam:110000118dd2d76', 'rhumcoca', 0),
(111, 'steam:110000118dd2d76', 'diving_license', 0),
(112, 'steam:110000118dd2d76', 'clothe', 0),
(113, 'steam:110000118dd2d76', 'fishing_license', 0),
(114, 'steam:110000118dd2d76', 'mixapero', 0),
(115, 'steam:110000118dd2d76', 'poppy', 0),
(116, 'steam:110000118dd2d76', 'fixtool', 0),
(117, 'steam:110000118dd2d76', 'weapons_license3', 0),
(118, 'steam:110000118dd2d76', 'wood', 0),
(119, 'steam:110000118dd2d76', 'blowpipe', 0),
(120, 'steam:110000118dd2d76', 'weapons_license1', 0),
(121, 'steam:110000118dd2d76', 'macka', 0),
(122, 'steam:110000118dd2d76', 'vodka', 0),
(123, 'steam:110000118dd2d76', 'receipt', 0),
(124, 'steam:110000118dd2d76', 'petrol', 0),
(125, 'steam:110000118dd2d76', 'marabou', 0),
(126, 'steam:110000118dd2d76', 'loka', 0),
(127, 'steam:110000118dd2d76', 'boating_license', 0),
(128, 'steam:110000118dd2d76', 'weed', 0),
(129, 'steam:110000118dd2d76', 'vegetables', 0),
(130, 'steam:110000118dd2d76', 'teqpaf', 0),
(131, 'steam:110000118dd2d76', 'boitier', 0),
(132, 'steam:110000118dd2d76', 'narcan', 0),
(133, 'steam:110000118dd2d76', 'cocaine', 0),
(134, 'steam:110000118dd2d76', 'bolchips', 0),
(135, 'steam:110000118dd2d76', 'copper', 0),
(136, 'steam:110000118dd2d76', 'fish', 0),
(137, 'steam:110000118dd2d76', 'jusfruit', 0),
(138, 'steam:110000118dd2d76', 'grapperaisin', 0),
(139, 'steam:110000118dd2d76', 'leather', 0),
(140, 'steam:110000118dd2d76', 'fakepee', 0),
(141, 'steam:110000118dd2d76', 'cocacola', 0),
(142, 'steam:110000118dd2d76', 'contrat', 0),
(143, 'steam:110000118dd2d76', 'grip', 0),
(144, 'steam:110000118dd2d76', 'beer', 0),
(145, 'steam:110000118dd2d76', 'whisky', 0),
(146, 'steam:110000118dd2d76', 'tequila', 0),
(147, 'steam:110000118dd2d76', 'scratchoff_used', 0),
(148, 'steam:110000118dd2d76', 'heroine', 0),
(149, 'steam:110000118dd2d76', 'hunting_license', 0),
(150, 'steam:110000118dd2d76', 'gazbottle', 0),
(151, 'steam:110000118dd2d76', 'drugtest', 0),
(152, 'steam:110000118dd2d76', 'ephedra', 0),
(153, 'steam:110000118dd2d76', 'carotool', 0),
(154, 'steam:110000118dd2d76', 'protein_shake', 0),
(155, 'steam:110000118dd2d76', 'pastacarbonara', 0),
(156, 'steam:110000118dd2d76', 'alive_chicken', 0),
(157, 'steam:110000118dd2d76', 'bolcacahuetes', 0),
(158, 'steam:110000118dd2d76', 'silencieux', 0),
(159, 'steam:110000118dd2d76', 'water', 0),
(160, 'steam:110000118dd2d76', 'icetea', 0),
(161, 'steam:110000118dd2d76', 'meat', 0),
(162, 'steam:110000118dd2d76', 'burger', 0),
(163, 'steam:110000118dd2d76', 'ephedrine', 0),
(164, 'steam:110000118dd2d76', 'lsd_pooch', 0),
(165, 'steam:110000118dd2d76', 'pills', 0),
(166, 'steam:110000118dd2d76', 'meth', 0),
(167, 'steam:110000118dd2d76', 'pearl', 0),
(168, 'steam:110000112969e8f', 'metreshooter', 0),
(169, 'steam:110000112969e8f', 'sprite', 0),
(170, 'steam:110000112969e8f', 'bread', 0),
(171, 'steam:110000112969e8f', 'cannabis', 0),
(172, 'steam:110000112969e8f', 'diamond', 0),
(173, 'steam:110000112969e8f', 'breathalyzer', 0),
(174, 'steam:110000112969e8f', 'flashlight', 0),
(175, 'steam:110000112969e8f', 'cutted_wood', 0),
(176, 'steam:110000112969e8f', 'licenseplate', 0),
(177, 'steam:110000112969e8f', 'packaged_plank', 0),
(178, 'steam:110000112969e8f', 'mojito', 0),
(179, 'steam:110000112969e8f', 'bandage', 0),
(180, 'steam:110000112969e8f', 'energy', 0),
(181, 'steam:110000112969e8f', 'defibrillateur', 0),
(182, 'steam:110000112969e8f', 'weapons_license2', 0),
(183, 'steam:110000112969e8f', 'WEAPON_BAT', 0),
(184, 'steam:110000112969e8f', 'armour', 0),
(185, 'steam:110000112969e8f', 'slaughtered_chicken', 0),
(186, 'steam:110000112969e8f', 'coca', 0),
(187, 'steam:110000112969e8f', 'rhum', 0),
(188, 'steam:110000112969e8f', 'lockpick', 0),
(189, 'steam:110000112969e8f', 'blackberry', 0),
(190, 'steam:110000112969e8f', 'wrench', 0),
(191, 'steam:110000112969e8f', 'menthe', 0),
(192, 'steam:110000112969e8f', 'gold', 0),
(193, 'steam:110000112969e8f', 'vodkafruit', 0),
(194, 'steam:110000112969e8f', 'yusuf', 0),
(195, 'steam:110000112969e8f', 'coke_pooch', 0),
(196, 'steam:110000112969e8f', '9mm_rounds', 0),
(197, 'steam:110000112969e8f', 'washed_stone', 0),
(198, 'steam:110000112969e8f', 'scratchoff', 0),
(199, 'steam:110000112969e8f', 'radio', 0),
(200, 'steam:110000112969e8f', 'plongee1', 0),
(201, 'steam:110000112969e8f', 'opium', 0),
(202, 'steam:110000112969e8f', 'gym_membership', 0),
(203, 'steam:110000112969e8f', 'chips', 0),
(204, 'steam:110000112969e8f', 'cola', 0),
(205, 'steam:110000112969e8f', 'dabs', 0),
(206, 'steam:110000112969e8f', 'rhumfruit', 0),
(207, 'steam:110000112969e8f', 'whiskey', 0),
(208, 'steam:110000112969e8f', 'crack', 0),
(209, 'steam:110000112969e8f', 'weed_pooch', 0),
(210, 'steam:110000112969e8f', 'bolpistache', 0),
(211, 'steam:110000112969e8f', 'whiskycoca', 0),
(212, 'steam:110000112969e8f', 'binoculars', 0),
(213, 'steam:110000112969e8f', 'coke', 0),
(214, 'steam:110000112969e8f', 'marriage_license', 0),
(215, 'steam:110000112969e8f', 'fabric', 0),
(216, 'steam:110000112969e8f', 'lotteryticket', 0),
(217, 'steam:110000112969e8f', 'essence', 0),
(218, 'steam:110000112969e8f', 'tacos', 0),
(219, 'steam:110000112969e8f', 'martini', 0),
(220, 'steam:110000112969e8f', 'cheesebows', 0),
(221, 'steam:110000112969e8f', 'limonade', 0),
(222, 'steam:110000112969e8f', 'pizza', 0),
(223, 'steam:110000112969e8f', 'WEAPON_FLASHLIGHT', 0),
(224, 'steam:110000112969e8f', 'carokit', 0),
(225, 'steam:110000112969e8f', 'croquettes', 0),
(226, 'steam:110000112969e8f', 'firstaidpass', 0),
(227, 'steam:110000112969e8f', 'saucisson', 0),
(228, 'steam:110000112969e8f', 'donut', 0),
(229, 'steam:110000112969e8f', 'packaged_chicken', 0),
(230, 'steam:110000112969e8f', 'lighter', 0),
(231, 'steam:110000112969e8f', 'painkiller', 0),
(232, 'steam:110000112969e8f', 'mlic', 0),
(233, 'steam:110000112969e8f', 'litter_pooch', 0),
(234, 'steam:110000112969e8f', 'pearl_pooch', 0),
(235, 'steam:110000112969e8f', 'bolnoixcajou', 0),
(236, 'steam:110000112969e8f', 'golem', 0),
(237, 'steam:110000112969e8f', 'sportlunch', 0),
(238, 'steam:110000112969e8f', 'fanta', 0),
(239, 'steam:110000112969e8f', 'lsd', 0),
(240, 'steam:110000112969e8f', 'ice', 0),
(241, 'steam:110000112969e8f', 'coffee', 0),
(242, 'steam:110000112969e8f', 'turtle_pooch', 0),
(243, 'steam:110000112969e8f', 'turtle', 0),
(244, 'steam:110000112969e8f', 'firstaidkit', 0),
(245, 'steam:110000112969e8f', 'petrol_raffin', 0),
(246, 'steam:110000112969e8f', 'taxi_license', 0),
(247, 'steam:110000112969e8f', 'clip', 0),
(248, 'steam:110000112969e8f', 'opium_pooch', 0),
(249, 'steam:110000112969e8f', 'WEAPON_PUMPSHOTGUN', 0),
(250, 'steam:110000112969e8f', 'fixkit', 0),
(251, 'steam:110000112969e8f', 'powerade', 0),
(252, 'steam:110000112969e8f', 'cigarett', 0),
(253, 'steam:110000112969e8f', 'pilot_license', 0),
(254, 'steam:110000112969e8f', 'medikit', 0),
(255, 'steam:110000112969e8f', 'drpepper', 0),
(256, 'steam:110000112969e8f', 'jagerbomb', 0),
(257, 'steam:110000112969e8f', 'baconburger', 0),
(258, 'steam:110000112969e8f', 'iron', 0),
(259, 'steam:110000112969e8f', 'gps', 0),
(260, 'steam:110000112969e8f', 'shotgun_shells', 0),
(261, 'steam:110000112969e8f', 'plongee2', 0),
(262, 'steam:110000112969e8f', 'WEAPON_PISTOL', 0),
(263, 'steam:110000112969e8f', 'WEAPON_KNIFE', 0),
(264, 'steam:110000112969e8f', 'meth_pooch', 0),
(265, 'steam:110000112969e8f', 'WEAPON_STUNGUN', 0),
(266, 'steam:110000112969e8f', 'marijuana', 0),
(267, 'steam:110000112969e8f', 'litter', 0),
(268, 'steam:110000112969e8f', 'nitrocannister', 0),
(269, 'steam:110000112969e8f', 'soda', 0),
(270, 'steam:110000112969e8f', 'jager', 0),
(271, 'steam:110000112969e8f', 'stone', 0),
(272, 'steam:110000112969e8f', 'vodkaenergy', 0),
(273, 'steam:110000112969e8f', 'pcp', 0),
(274, 'steam:110000112969e8f', 'dlic', 0),
(275, 'steam:110000112969e8f', 'cdl', 0),
(276, 'steam:110000112969e8f', 'whool', 0),
(277, 'steam:110000112969e8f', 'rhumcoca', 0),
(278, 'steam:110000112969e8f', 'diving_license', 0),
(279, 'steam:110000112969e8f', 'clothe', 0),
(280, 'steam:110000112969e8f', 'fishing_license', 0),
(281, 'steam:110000112969e8f', 'mixapero', 0),
(282, 'steam:110000112969e8f', 'poppy', 0),
(283, 'steam:110000112969e8f', 'fixtool', 0),
(284, 'steam:110000112969e8f', 'weapons_license3', 0),
(285, 'steam:110000112969e8f', 'wood', 0),
(286, 'steam:110000112969e8f', 'blowpipe', 0),
(287, 'steam:110000112969e8f', 'weapons_license1', 0),
(288, 'steam:110000112969e8f', 'macka', 0),
(289, 'steam:110000112969e8f', 'vodka', 0),
(290, 'steam:110000112969e8f', 'receipt', 0),
(291, 'steam:110000112969e8f', 'petrol', 0),
(292, 'steam:110000112969e8f', 'marabou', 0),
(293, 'steam:110000112969e8f', 'loka', 0),
(294, 'steam:110000112969e8f', 'boating_license', 0),
(295, 'steam:110000112969e8f', 'weed', 0),
(296, 'steam:110000112969e8f', 'vegetables', 0),
(297, 'steam:110000112969e8f', 'teqpaf', 0),
(298, 'steam:110000112969e8f', 'boitier', 0),
(299, 'steam:110000112969e8f', 'narcan', 0),
(300, 'steam:110000112969e8f', 'cocaine', 0),
(301, 'steam:110000112969e8f', 'bolchips', 0),
(302, 'steam:110000112969e8f', 'copper', 0),
(303, 'steam:110000112969e8f', 'fish', 0),
(304, 'steam:110000112969e8f', 'jusfruit', 0),
(305, 'steam:110000112969e8f', 'grapperaisin', 0),
(306, 'steam:110000112969e8f', 'leather', 0),
(307, 'steam:110000112969e8f', 'fakepee', 0),
(308, 'steam:110000112969e8f', 'cocacola', 0),
(309, 'steam:110000112969e8f', 'contrat', 0),
(310, 'steam:110000112969e8f', 'grip', 0),
(311, 'steam:110000112969e8f', 'beer', 0),
(312, 'steam:110000112969e8f', 'whisky', 0),
(313, 'steam:110000112969e8f', 'tequila', 0),
(314, 'steam:110000112969e8f', 'scratchoff_used', 0),
(315, 'steam:110000112969e8f', 'heroine', 0),
(316, 'steam:110000112969e8f', 'hunting_license', 0),
(317, 'steam:110000112969e8f', 'gazbottle', 0),
(318, 'steam:110000112969e8f', 'drugtest', 0),
(319, 'steam:110000112969e8f', 'ephedra', 0),
(320, 'steam:110000112969e8f', 'carotool', 0),
(321, 'steam:110000112969e8f', 'protein_shake', 0),
(322, 'steam:110000112969e8f', 'pastacarbonara', 0),
(323, 'steam:110000112969e8f', 'alive_chicken', 0),
(324, 'steam:110000112969e8f', 'bolcacahuetes', 0),
(325, 'steam:110000112969e8f', 'silencieux', 0),
(326, 'steam:110000112969e8f', 'water', 0),
(327, 'steam:110000112969e8f', 'icetea', 0),
(328, 'steam:110000112969e8f', 'meat', 0),
(329, 'steam:110000112969e8f', 'burger', 0),
(330, 'steam:110000112969e8f', 'ephedrine', 0),
(331, 'steam:110000112969e8f', 'lsd_pooch', 0),
(332, 'steam:110000112969e8f', 'pills', 0),
(333, 'steam:110000112969e8f', 'meth', 0),
(334, 'steam:110000112969e8f', 'pearl', 0),
(335, 'steam:110000132580eb0', 'meat', 0),
(336, 'steam:110000132580eb0', 'heroine', 0),
(337, 'steam:110000132580eb0', 'grapperaisin', 0),
(338, 'steam:110000132580eb0', 'firstaidkit', 0),
(339, 'steam:110000132580eb0', 'lockpick', 0),
(340, 'steam:110000132580eb0', 'turtle', 0),
(341, 'steam:110000132580eb0', 'coke', 0),
(342, 'steam:110000132580eb0', 'ice', 0),
(343, 'steam:110000132580eb0', 'breathalyzer', 0),
(344, 'steam:110000132580eb0', 'cocacola', 0),
(345, 'steam:110000132580eb0', 'lsd_pooch', 0),
(346, 'steam:110000132580eb0', 'essence', 0),
(347, 'steam:110000132580eb0', 'opium', 0),
(348, 'steam:110000132580eb0', 'armour', 0),
(349, 'steam:110000132580eb0', 'icetea', 0),
(350, 'steam:110000132580eb0', 'burger', 0),
(351, 'steam:110000132580eb0', 'teqpaf', 0),
(352, 'steam:110000132580eb0', 'crack', 0),
(353, 'steam:110000132580eb0', 'slaughtered_chicken', 0),
(354, 'steam:110000132580eb0', 'whiskycoca', 0),
(355, 'steam:110000132580eb0', 'rhumcoca', 0),
(356, 'steam:110000132580eb0', 'WEAPON_BAT', 0),
(357, 'steam:110000132580eb0', 'packaged_plank', 0),
(358, 'steam:110000132580eb0', 'bread', 0),
(359, 'steam:110000132580eb0', 'petrol_raffin', 0),
(360, 'steam:110000132580eb0', 'blowpipe', 0),
(361, 'steam:110000132580eb0', 'bolcacahuetes', 0),
(362, 'steam:110000132580eb0', 'lsd', 0),
(363, 'steam:110000132580eb0', 'ephedra', 0),
(364, 'steam:110000132580eb0', 'petrol', 0),
(365, 'steam:110000132580eb0', 'hunting_license', 0),
(366, 'steam:110000132580eb0', 'grip', 0),
(367, 'steam:110000132580eb0', 'cheesebows', 0),
(368, 'steam:110000132580eb0', 'marijuana', 0),
(369, 'steam:110000132580eb0', 'weed', 0),
(370, 'steam:110000132580eb0', 'cola', 0),
(371, 'steam:110000132580eb0', 'litter', 0),
(372, 'steam:110000132580eb0', 'mojito', 0),
(373, 'steam:110000132580eb0', 'energy', 0),
(374, 'steam:110000132580eb0', 'baconburger', 0),
(375, 'steam:110000132580eb0', 'marriage_license', 0),
(376, 'steam:110000132580eb0', 'donut', 0),
(377, 'steam:110000132580eb0', 'jusfruit', 0),
(378, 'steam:110000132580eb0', 'WEAPON_KNIFE', 0),
(379, 'steam:110000132580eb0', 'weed_pooch', 0),
(380, 'steam:110000132580eb0', 'martini', 0),
(381, 'steam:110000132580eb0', 'pills', 0),
(382, 'steam:110000132580eb0', 'WEAPON_PUMPSHOTGUN', 0),
(383, 'steam:110000132580eb0', 'coca', 0),
(384, 'steam:110000132580eb0', 'meth_pooch', 0),
(385, 'steam:110000132580eb0', 'litter_pooch', 0),
(386, 'steam:110000132580eb0', 'tequila', 0),
(387, 'steam:110000132580eb0', 'croquettes', 0),
(388, 'steam:110000132580eb0', 'lotteryticket', 0),
(389, 'steam:110000132580eb0', 'shotgun_shells', 0),
(390, 'steam:110000132580eb0', 'cocaine', 0),
(391, 'steam:110000132580eb0', 'vodkafruit', 0),
(392, 'steam:110000132580eb0', 'pcp', 0),
(393, 'steam:110000132580eb0', 'fish', 0),
(394, 'steam:110000132580eb0', 'defibrillateur', 0),
(395, 'steam:110000132580eb0', 'clothe', 0),
(396, 'steam:110000132580eb0', 'turtle_pooch', 0),
(397, 'steam:110000132580eb0', 'washed_stone', 0),
(398, 'steam:110000132580eb0', 'boitier', 0),
(399, 'steam:110000132580eb0', 'nitrocannister', 0),
(400, 'steam:110000132580eb0', 'clip', 0),
(401, 'steam:110000132580eb0', 'drpepper', 0),
(402, 'steam:110000132580eb0', 'stone', 0),
(403, 'steam:110000132580eb0', 'firstaidpass', 0),
(404, 'steam:110000132580eb0', 'cdl', 0),
(405, 'steam:110000132580eb0', 'soda', 0),
(406, 'steam:110000132580eb0', 'weapons_license3', 0),
(407, 'steam:110000132580eb0', 'bolchips', 0),
(408, 'steam:110000132580eb0', 'whool', 0),
(409, 'steam:110000132580eb0', 'medikit', 0),
(410, 'steam:110000132580eb0', 'mlic', 0),
(411, 'steam:110000132580eb0', 'WEAPON_STUNGUN', 0),
(412, 'steam:110000132580eb0', 'sportlunch', 0),
(413, 'steam:110000132580eb0', 'weapons_license2', 0),
(414, 'steam:110000132580eb0', 'limonade', 0),
(415, 'steam:110000132580eb0', 'radio', 0),
(416, 'steam:110000132580eb0', 'alive_chicken', 0),
(417, 'steam:110000132580eb0', 'poppy', 0),
(418, 'steam:110000132580eb0', 'protein_shake', 0),
(419, 'steam:110000132580eb0', 'drugtest', 0),
(420, 'steam:110000132580eb0', 'binoculars', 0),
(421, 'steam:110000132580eb0', 'rhumfruit', 0),
(422, 'steam:110000132580eb0', 'vodkaenergy', 0),
(423, 'steam:110000132580eb0', 'licenseplate', 0),
(424, 'steam:110000132580eb0', 'bandage', 0),
(425, 'steam:110000132580eb0', 'meth', 0),
(426, 'steam:110000132580eb0', 'tacos', 0),
(427, 'steam:110000132580eb0', 'gazbottle', 0),
(428, 'steam:110000132580eb0', 'boating_license', 0),
(429, 'steam:110000132580eb0', 'pearl_pooch', 0),
(430, 'steam:110000132580eb0', 'coke_pooch', 0),
(431, 'steam:110000132580eb0', 'gps', 0),
(432, 'steam:110000132580eb0', 'fakepee', 0),
(433, 'steam:110000132580eb0', 'cutted_wood', 0),
(434, 'steam:110000132580eb0', 'vodka', 0),
(435, 'steam:110000132580eb0', 'whisky', 0),
(436, 'steam:110000132580eb0', 'lighter', 0),
(437, 'steam:110000132580eb0', 'dlic', 0),
(438, 'steam:110000132580eb0', '9mm_rounds', 0),
(439, 'steam:110000132580eb0', 'WEAPON_PISTOL', 0),
(440, 'steam:110000132580eb0', 'WEAPON_FLASHLIGHT', 0),
(441, 'steam:110000132580eb0', 'wood', 0),
(442, 'steam:110000132580eb0', 'receipt', 0),
(443, 'steam:110000132580eb0', 'dabs', 0),
(444, 'steam:110000132580eb0', 'taxi_license', 0),
(445, 'steam:110000132580eb0', 'iron', 0),
(446, 'steam:110000132580eb0', 'weapons_license1', 0),
(447, 'steam:110000132580eb0', 'narcan', 0),
(448, 'steam:110000132580eb0', 'pilot_license', 0),
(449, 'steam:110000132580eb0', 'jagerbomb', 0),
(450, 'steam:110000132580eb0', 'diving_license', 0),
(451, 'steam:110000132580eb0', 'fabric', 0),
(452, 'steam:110000132580eb0', 'fishing_license', 0),
(453, 'steam:110000132580eb0', 'macka', 0),
(454, 'steam:110000132580eb0', 'pastacarbonara', 0),
(455, 'steam:110000132580eb0', 'pizza', 0),
(456, 'steam:110000132580eb0', 'vegetables', 0),
(457, 'steam:110000132580eb0', 'wrench', 0),
(458, 'steam:110000132580eb0', 'fanta', 0),
(459, 'steam:110000132580eb0', 'opium_pooch', 0),
(460, 'steam:110000132580eb0', 'contrat', 0),
(461, 'steam:110000132580eb0', 'loka', 0),
(462, 'steam:110000132580eb0', 'sprite', 0),
(463, 'steam:110000132580eb0', 'gym_membership', 0),
(464, 'steam:110000132580eb0', 'saucisson', 0),
(465, 'steam:110000132580eb0', 'golem', 0),
(466, 'steam:110000132580eb0', 'painkiller', 0),
(467, 'steam:110000132580eb0', 'bolpistache', 0),
(468, 'steam:110000132580eb0', 'gold', 0),
(469, 'steam:110000132580eb0', 'carokit', 0),
(470, 'steam:110000132580eb0', 'rhum', 0),
(471, 'steam:110000132580eb0', 'blackberry', 0),
(472, 'steam:110000132580eb0', 'plongee2', 0),
(473, 'steam:110000132580eb0', 'marabou', 0),
(474, 'steam:110000132580eb0', 'water', 0),
(475, 'steam:110000132580eb0', 'packaged_chicken', 0),
(476, 'steam:110000132580eb0', 'jager', 0),
(477, 'steam:110000132580eb0', 'scratchoff', 0),
(478, 'steam:110000132580eb0', 'ephedrine', 0),
(479, 'steam:110000132580eb0', 'copper', 0),
(480, 'steam:110000132580eb0', 'whiskey', 0),
(481, 'steam:110000132580eb0', 'menthe', 0),
(482, 'steam:110000132580eb0', 'plongee1', 0),
(483, 'steam:110000132580eb0', 'cannabis', 0),
(484, 'steam:110000132580eb0', 'leather', 0),
(485, 'steam:110000132580eb0', 'scratchoff_used', 0),
(486, 'steam:110000132580eb0', 'pearl', 0),
(487, 'steam:110000132580eb0', 'mixapero', 0),
(488, 'steam:110000132580eb0', 'chips', 0),
(489, 'steam:110000132580eb0', 'coffee', 0),
(490, 'steam:110000132580eb0', 'powerade', 0),
(491, 'steam:110000132580eb0', 'cigarett', 0),
(492, 'steam:110000132580eb0', 'diamond', 0),
(493, 'steam:110000132580eb0', 'yusuf', 0),
(494, 'steam:110000132580eb0', 'beer', 0),
(495, 'steam:110000132580eb0', 'flashlight', 0),
(496, 'steam:110000132580eb0', 'silencieux', 0),
(497, 'steam:110000132580eb0', 'fixtool', 0),
(498, 'steam:110000132580eb0', 'carotool', 0),
(499, 'steam:110000132580eb0', 'bolnoixcajou', 0),
(500, 'steam:110000132580eb0', 'metreshooter', 0),
(501, 'steam:110000132580eb0', 'fixkit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(11) NOT NULL,
  `reported_by` varchar(80) DEFAULT NULL,
  `report_type` varchar(255) DEFAULT NULL,
  `report_comment` varchar(255) DEFAULT NULL,
  `report_admin` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_warnings`
--

CREATE TABLE `user_warnings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `warning` longtext DEFAULT NULL,
  `time_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `inshop` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `inshop`) VALUES
('3500flatbed', '3500 Flatbed', 78000, 'imports', 1),
('Adder', 'adder', 900000, 'super', 1),
('Akuma', 'AKUMA', 7500, 'motorcycles', 1),
('Alpha', 'alpha', 60000, 'sports', 1),
('Ardent', 'ardent', 1150000, 'sportsclassics', 1),
('Asea', 'asea', 5500, 'sedans', 1),
('Autarch', 'autarch', 1955000, 'super', 1),
('Avarus', 'avarus', 18000, 'motorcycles', 1),
('Bagger', 'bagger', 13500, 'motorcycles', 1),
('Baller', 'baller2', 40000, 'suvs', 1),
('Baller Sport', 'baller3', 60000, 'suvs', 1),
('Banshee', 'banshee', 70000, 'sports', 1),
('Banshee 900R', 'banshee2', 255000, 'sports', 1),
('Bati 801', 'bati', 12000, 'motorcycles', 1),
('Bati 801RR', 'bati2', 19000, 'motorcycles', 1),
('Bestia GTS', 'bestiagts', 55000, 'sports', 1),
('BF400', 'bf400', 6500, 'motorcycles', 1),
('Bf Injection', 'bfinjection', 16000, 'offroad', 1),
('Bifta', 'bifta', 12000, 'offroad', 1),
('Bison', 'bison', 45000, 'vans', 1),
('Blade', 'blade', 15000, 'muscle', 1),
('Blazer', 'blazer', 6500, 'offroad', 1),
('Blazer Sport', 'blazer4', 8500, 'offroad', 1),
('blazer5', 'blazer5', 1755600, 'offroad', 1),
('Blista', 'blista', 8000, 'compacts', 1),
('BMX (velo)', 'bmx', 160, 'motorcycles', 1),
('Bobcat XL', 'bobcatxl', 32000, 'vans', 1),
('Brawler', 'brawler', 45000, 'offroad', 1),
('Brioso R/A', 'brioso', 18000, 'compacts', 1),
('Btype', 'btype', 62000, 'sportsclassics', 1),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 1),
('Btype Luxe', 'btype3', 85000, 'sportsclassics', 1),
('Buccaneer', 'buccaneer', 18000, 'muscle', 1),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 1),
('Buffalo', 'buffalo', 12000, 'sports', 1),
('Buffalo S', 'buffalo2', 20000, 'sports', 1),
('Bullet', 'bullet', 90000, 'super', 1),
('Burrito', 'burrito3', 19000, 'vans', 1),
('Camper', 'camper', 42000, 'vans', 1),
('Carbonizzare', 'carbonizzare', 75000, 'sports', 1),
('Carbon RS', 'carbonrs', 18000, 'motorcycles', 1),
('Casco', 'casco', 30000, 'sportsclassics', 1),
('Cavalcade', 'cavalcade2', 55000, 'suvs', 1),
('Cheetah', 'cheetah', 375000, 'super', 1),
('Chimera', 'chimera', 38000, 'motorcycles', 1),
('Chino', 'chino', 15000, 'muscle', 1),
('Chino Luxe', 'chino2', 19000, 'muscle', 1),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 1),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 1),
('Cognoscenti', 'cognoscenti', 55000, 'sedans', 1),
('Comet', 'comet2', 65000, 'sports', 1),
('Comet 5', 'comet5', 1145000, 'sports', 1),
('Contender', 'contender', 70000, 'suvs', 1),
('Coquette', 'coquette', 65000, 'sports', 1),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 1),
('Coquette BlackFin', 'coquette3', 55000, 'muscle', 1),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles', 1),
('Cyclone', 'cyclone', 1890000, 'super', 1),
('Daemon', 'daemon', 11500, 'motorcycles', 1),
('Daemon High', 'daemon2', 13500, 'motorcycles', 1),
('Defiler', 'defiler', 9800, 'motorcycles', 1),
('Deluxo', 'deluxo', 4721500, 'sportsclassics', 1),
('GMC Sierra', 'denali16', 60000, 'importcars', 0),
('Dominator', 'dominator', 35000, 'muscle', 1),
('Double T', 'double', 28000, 'motorcycles', 1),
('Dubsta', 'dubsta', 45000, 'suvs', 1),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 1),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 1),
('Dukes', 'dukes', 28000, 'muscle', 1),
('Dune Buggy', 'dune', 8000, 'offroad', 1),
('Elegy', 'elegy2', 38500, 'sports', 1),
('Emperor', 'emperor', 8500, 'sedans', 1),
('Enduro', 'enduro', 5500, 'motorcycles', 1),
('Entity XF', 'entityxf', 425000, 'importcars', 0),
('Esskey', 'esskey', 4200, 'motorcycles', 1),
('Exemplar', 'exemplar', 32000, 'coupes', 1),
('F620', 'f620', 40000, 'coupes', 1),
('Faction', 'faction', 20000, 'muscle', 1),
('Faction Rider', 'faction2', 30000, 'muscle', 1),
('Faction XL', 'faction3', 40000, 'muscle', 1),
('Faggio', 'faggio', 1900, 'motorcycles', 1),
('Vespa', 'faggio2', 2800, 'motorcycles', 1),
('Felon', 'felon', 42000, 'coupes', 1),
('Felon GT', 'felon2', 55000, 'coupes', 1),
('Feltzer', 'feltzer2', 55000, 'sports', 1),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 1),
('Fixter (velo)', 'fixter', 225, 'motorcycles', 1),
('Flat Bed', 'flatbed', 90000, 'utility', 1),
('FMJ', 'fmj', 185000, 'super', 1),
('Road King', 'foxharley1', 23000, 'importbikes', 0),
('Road Glide', 'foxharley2', 30000, 'importbikes', 0),
('Fhantom', 'fq2', 17000, 'suvs', 1),
('Fugitive', 'fugitive', 12000, 'sedans', 1),
('Furore GT', 'furoregt', 45000, 'sports', 1),
('Fusilade', 'fusilade', 40000, 'sports', 1),
('Gargoyle', 'gargoyle', 16500, 'motorcycles', 1),
('Gauntlet', 'gauntlet', 30000, 'muscle', 1),
('Gang Burrito', 'gburrito', 45000, 'vans', 1),
('Burrito', 'gburrito2', 29000, 'vans', 1),
('Glendale', 'glendale', 6500, 'sedans', 1),
('Grabger', 'granger', 50000, 'suvs', 1),
('Gresley', 'gresley', 47500, 'suvs', 1),
('GT 500', 'gt500', 785000, 'sportsclassics', 1),
('Guardian', 'guardian', 45000, 'offroad', 1),
('Hakuchou', 'hakuchou', 31000, 'motorcycles', 1),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 1),
('Heavy Wrecker', 'hdwrecker', 500000, 'utility', 1),
('Hermes', 'hermes', 535000, 'muscle', 1),
('Hexer', 'hexer', 12000, 'motorcycles', 1),
('Hotknife', 'hotknife', 125000, 'muscle', 1),
('Huntley S', 'huntley', 40000, 'suvs', 1),
('Hustler', 'hustler', 625000, 'muscle', 1),
('Street Glide', 'hvrod', 25000, 'imports', 1),
('Infernus', 'infernus', 180000, 'super', 1),
('Innovation', 'innovation', 23500, 'motorcycles', 1),
('Intruder', 'intruder', 7500, 'sedans', 1),
('Issi', 'issi2', 10000, 'compacts', 1),
('Jackal', 'jackal', 38000, 'coupes', 1),
('Jester', 'jester', 65000, 'sports', 1),
('Jester(Racecar)', 'jester2', 135000, 'sports', 1),
('Journey', 'journey', 6500, 'vans', 1),
('k9', 'k9', 2147483647, 'emergency', 0),
('Kamacho', 'kamacho', 345000, 'offroad', 1),
('Khamelion', 'khamelion', 38000, 'sports', 1),
('Kuruma', 'kuruma', 30000, 'sports', 1),
('Landstalker', 'landstalker', 35000, 'suvs', 1),
('RE-7B', 'le7b', 325000, 'super', 1),
('Lynx', 'lynx', 40000, 'sports', 1),
('Mamba', 'mamba', 70000, 'sports', 1),
('Manana', 'manana', 12800, 'sportsclassics', 1),
('Manchez', 'manchez', 5300, 'motorcycles', 1),
('Massacro', 'massacro', 65000, 'sports', 1),
('Massacro(Racecar)', 'massacro2', 130000, 'sports', 1),
('Mesa', 'mesa', 16000, 'suvs', 1),
('Mesa Trail', 'mesa3', 40000, 'suvs', 1),
('Minivan', 'minivan', 13000, 'vans', 1),
('Monroe', 'monroe', 55000, 'sportsclassics', 1),
('The Liberator', 'monster', 210000, 'offroad', 1),
('Moonbeam', 'moonbeam', 18000, 'vans', 1),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 1),
('Nemesis', 'nemesis', 5800, 'motorcycles', 1),
('Neon', 'neon', 1500000, 'sports', 1),
('Nightblade', 'nightblade', 35000, 'importbikes', 0),
('Nightshade', 'nightshade', 65000, 'muscle', 1),
('9F', 'ninef', 65000, 'sports', 1),
('9F Cabrio', 'ninef2', 80000, 'sports', 1),
('Omnis', 'omnis', 35000, 'sports', 1),
('Oppressor', 'oppressor', 3524500, 'super', 1),
('Oracle XS', 'oracle2', 35000, 'coupes', 1),
('Osiris', 'osiris', 160000, 'super', 1),
('Panto', 'panto', 10000, 'compacts', 1),
('Paradise', 'paradise', 19000, 'vans', 1),
('Pariah', 'pariah', 1420000, 'sports', 1),
('Patriot', 'patriot', 55000, 'suvs', 1),
('PCJ-600', 'pcj', 6200, 'motorcycles', 1),
('Penumbra', 'penumbra', 28000, 'sports', 1),
('Pfister', 'pfister811', 85000, 'super', 1),
('Phoenix', 'phoenix', 12500, 'muscle', 1),
('Picador', 'picador', 18000, 'muscle', 1),
('Pigalle', 'pigalle', 20000, 'sportsclassics', 1),
('Prairie', 'prairie', 12000, 'compacts', 1),
('Premier', 'premier', 8000, 'sedans', 1),
('Primo Custom', 'primo2', 14000, 'sedans', 1),
('X80 Proto', 'prototipo', 2500000, 'super', 1),
('Radius', 'radi', 29000, 'suvs', 1),
('raiden', 'raiden', 1375000, 'sports', 1),
('Rapid GT', 'rapidgt', 35000, 'sports', 1),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 1),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 1),
('Reaper', 'reaper', 150000, 'importcars', 0),
('Rebel', 'rebel2', 35000, 'offroad', 1),
('Regina', 'regina', 5000, 'sedans', 1),
('Retinue', 'retinue', 615000, 'sportsclassics', 1),
('Revolter', 'revolter', 1610000, 'sports', 1),
('riata', 'riata', 380000, 'offroad', 1),
('Rocoto', 'rocoto', 45000, 'suvs', 1),
('Ruffian', 'ruffian', 6800, 'motorcycles', 1),
('Ruiner 2', 'ruiner2', 5745600, 'muscle', 1),
('Rumpo', 'rumpo', 15000, 'vans', 1),
('Rumpo Trail', 'rumpo3', 19500, 'vans', 1),
('Sabre Turbo', 'sabregt', 20000, 'muscle', 1),
('Sabre GT', 'sabregt2', 25000, 'muscle', 1),
('Sanchez', 'sanchez', 5300, 'motorcycles', 1),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 1),
('Sanctus', 'sanctus', 25000, 'motorcycles', 1),
('Sandking', 'sandking', 55000, 'offroad', 1),
('Savestra', 'savestra', 990000, 'sportsclassics', 1),
('SC 1', 'sc1', 1603000, 'super', 1),
('Schafter', 'schafter2', 25000, 'sedans', 1),
('Schafter V12', 'schafter3', 50000, 'sports', 1),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles', 1),
('Seminole', 'seminole', 25000, 'suvs', 1),
('Sentinel', 'sentinel', 32000, 'coupes', 1),
('Sentinel XS', 'sentinel2', 40000, 'coupes', 1),
('Sentinel3', 'sentinel3', 650000, 'sports', 1),
('Seven 70', 'seven70', 39500, 'sports', 1),
('ETR1', 'sheava', 220000, 'importcars', 0),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 1),
('Slam Van', 'slamvan3', 11500, 'muscle', 1),
('Sovereign', 'sovereign', 22000, 'motorcycles', 1),
('Stinger', 'stinger', 80000, 'sportsclassics', 1),
('Stinger GT', 'stingergt', 75000, 'sportsclassics', 1),
('Streiter', 'streiter', 500000, 'sports', 1),
('Stretch', 'stretch', 90000, 'sedans', 1),
('Stromberg', 'stromberg', 3185350, 'sports', 1),
('Sultan', 'sultan', 15000, 'sports', 1),
('Sultan RS', 'sultanrs', 65000, 'importcars', 0),
('Super Diamond', 'superd', 130000, 'sedans', 1),
('Surano', 'surano', 50000, 'sports', 1),
('Surfer', 'surfer', 12000, 'vans', 1),
('T20', 't20', 300000, 'super', 1),
('Tailgater', 'tailgater', 30000, 'sedans', 1),
('Tampa', 'tampa', 16000, 'muscle', 1),
('Drift Tampa', 'tampa2', 80000, 'importcars', 0),
('Thrust', 'thrust', 24000, 'motorcycles', 1),
('Tow Truck', 'towtruck', 30000, 'utility', 1),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles', 1),
('Trophy Truck', 'trophytruck', 60000, 'offroad', 1),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 1),
('Tropos', 'tropos', 40000, 'sports', 1),
('Turismo R', 'turismor', 350000, 'super', 1),
('Tyrus', 'tyrus', 600000, 'super', 1),
('Vacca', 'vacca', 120000, 'super', 1),
('Vader', 'vader', 7200, 'motorcycles', 1),
('Verlierer', 'verlierer2', 70000, 'sports', 1),
('Vigero', 'vigero', 12500, 'muscle', 1),
('Virgo', 'virgo', 14000, 'muscle', 1),
('Viseris', 'viseris', 875000, 'sportsclassics', 1),
('Visione', 'visione', 2250000, 'super', 1),
('Voltic', 'voltic', 90000, 'super', 1),
('Voltic 2', 'voltic2', 3830400, 'super', 1),
('Voodoo', 'voodoo', 7200, 'muscle', 1),
('Vortex', 'vortex', 9800, 'motorcycles', 1),
('Warrener', 'warrener', 4000, 'sedans', 1),
('Washington', 'washington', 9000, 'sedans', 1),
('Windsor', 'windsor', 95000, 'coupes', 1),
('Windsor Drop', 'windsor2', 125000, 'importcars', 0),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 1),
('XLS', 'xls', 32000, 'suvs', 1),
('Yosemite', 'yosemite', 485000, 'muscle', 1),
('Youga', 'youga', 10800, 'vans', 1),
('Youga Luxuary', 'youga2', 14500, 'vans', 1),
('Z190', 'z190', 900000, 'sportsclassics', 1),
('Zentorno', 'zentorno', 1500000, 'super', 1),
('Zion', 'zion', 36000, 'coupes', 1),
('Zion Cabrio', 'zion2', 45000, 'coupes', 1),
('Zombie', 'zombiea', 9500, 'motorcycles', 1),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 1),
('Z-Type', 'ztype', 220000, 'sportsclassics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_display`
--

CREATE TABLE `vehicles_display` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT 10,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles_display`
--

INSERT INTO `vehicles_display` (`ID`, `name`, `model`, `profit`, `price`) VALUES
(1, 'Windsor Drop', 'windsor2', 10, 125000),
(2, 'Reaper', 'reaper', 10, 150000),
(3, 'GMC Sierra', 'denali16', 10, 60000),
(4, 'Road Glide', 'foxharley2', 10, 30000),
(5, 'Road King', 'foxharley1', 10, 23000),
(6, 'Nightblade', 'nightblade', 10, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'emergency'),
('importbikes', 'Import Bikes'),
('importcars', 'Import Cars'),
('imports', 'imports'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('utility', 'Utility'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `fullname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `crimes` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 1000),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 200),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 150),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 60),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 35),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 75),
(9, 'GunShop', 'WEAPON_BAT', 30),
(10, 'BlackWeashop', 'WEAPON_BAT', 5),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 10000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 18000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 24000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 26000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 50),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 5),
(25, 'GunShop', 'WEAPON_BALL', 15),
(26, 'BlackWeashop', 'WEAPON_BALL', 2),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 46),
(28, 'GunShop', 'WEAPON_PISTOL50', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baninfo`
--
ALTER TABLE `baninfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banlist`
--
ALTER TABLE `banlist`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commend`
--
ALTER TABLE `commend`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `gsr`
--
ALTER TABLE `gsr`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kicks`
--
ALTER TABLE `kicks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nitro_vehicles`
--
ALTER TABLE `nitro_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `vehsowned` (`owner`);

--
-- Indexes for table `owned_vehicles_old`
--
ALTER TABLE `owned_vehicles_old`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `index_owned_vehicles_owner` (`owner`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `license` (`license`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_bans`
--
ALTER TABLE `received_bans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `server_actions`
--
ALTER TABLE `server_actions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin_notes`
--
ALTER TABLE `user_admin_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_warnings`
--
ALTER TABLE `user_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baninfo`
--
ALTER TABLE `baninfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commend`
--
ALTER TABLE `commend`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2093;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- AUTO_INCREMENT for table `kicks`
--
ALTER TABLE `kicks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;

--
-- AUTO_INCREMENT for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=388;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `received_bans`
--
ALTER TABLE `received_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_actions`
--
ALTER TABLE `server_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=502;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_warnings`
--
ALTER TABLE `user_warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
