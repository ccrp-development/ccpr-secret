-- Script pack by godgutten
-- 
-- Set the resource manifest
resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'


-- Script 1 
client_script 'script1/client.lua'
server_script 'script1/server.lua'

-- Script 2
client_script 'script2/client.lua'
server_script 'script2/server.lua'

-- Script 3
client_script 'script3/client.lua'
server_script 'script3/server.lua'

