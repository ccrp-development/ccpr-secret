-- Cancel all commands being sent to class
AddEventHandler("chatMessage", function(source, name, message)
	if (startswith(message, "/")) then
		local cmd = stringsplit(message, " ")
		if cmd[1] == "/crouch" then
			CancelEvent()
			TriggerClientEvent("TogC", source)
		end
	end
end)

function startswith(String, Start)
	return string.sub(String,1,string.len(Start))==Start
end

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end