-- Cancel all cmds being sent to class
AddEventHandler("chatMessage", function(source, name, message)
	if (startswith(message, "/")) then
		local cmd = stringsplit(message, " ")
		if cmd[1] == "/ticket" then
			CancelEvent()
			if tablelength(cmd) > 3 then
				local tPID = tonumber(cmd[2])
				if tPID == -1 then
					return
				end
				local am = tonumber(cmd[3])
				local mg = concatTableFromIndex(cmd, 4)
				print(GetPlayerName(source) .." is ticketing ".. GetPlayerName(tPID).. " for $".. am .." for ".. mg)
				
				TriggerClientEvent("WT", source)
				TriggerClientEvent('chatMessage', tPID, 'SYSTEM', {0,0,0}, "You have been ticketed $" .. am.. " by officer ".. GetPlayerName(source).." for ".. mg)
			else 
				print(GetPlayerName(source) .." is ticketing without params")
				TriggerClientEvent("WT", source)
			end
		end
	end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function concatTableFromIndex(T, index)
	local count = 0
	local mg = ""
	for _ in pairs(T) do
		count = count + 1
		if count >= index then
			mg = mg.. " ".. tostring(T[count])
		end
	end
	return mg
end

function startswith(String, Start)
	return string.sub(String,1,string.len(Start))==Start
end