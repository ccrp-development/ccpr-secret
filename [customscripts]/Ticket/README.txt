Ticket by Albo1125 (LUA, FiveReborn).
Please review the licence before using.

Commands to type in chat (T):
/ticket - Perform ticket animation. No hassle.
/ticket PLAYERID AMOUNT REASON - Tickets player with specified PLAYERID for the specified AMOUNT of dollars, and gives the given REASON.
	Example: /ticket 2 250 Drinking in public - Will give the player with PLAYERID 2 a ticket of $250 for Drinking in public.