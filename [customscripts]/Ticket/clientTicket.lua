--Coded by Albo1125. Strictly no unauthorized redistribution. Please review the licence - breaking this has consequences.

RegisterNetEvent("WT")
AddEventHandler("WT", function()
	Citizen.CreateThread(function()
		TaskStartScenarioInPlace(GetPlayerPed(-1), "CODE_HUMAN_MEDIC_TIME_OF_DEATH", 0, true)
		while not IsPedActiveInScenario(GetPlayerPed(-1)) do
			Citizen.Wait(10)
		end
		local wC = 0
		while IsPedActiveInScenario(GetPlayerPed(-1)) do
			Citizen.Wait(10)
			wC = wC + 1
			if wC >= 300 then	
				break
			end
		end
		ClearPedTasks(GetPlayerPed(-1))
	end)
end)


RegisterNetEvent("notify")
AddEventHandler("notify", function(msg)
	ShowNotification(msg)
end)

function ShowNotification(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end