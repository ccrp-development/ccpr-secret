RegisterServerEvent('INV:displayPlayerInventory')
RegisterServerEvent('INV:displayVehicleInventory')
RegisterServerEvent('INV:addPlayer')

messageSender = "DOJInventory"
messageSenderColor = {200,0,0}



players = {}


AddEventHandler('INV:addPlayer', function()
	players[source] = true
end)


AddEventHandler('playerDropped',function(reason)
	players[source] = nil
end)


AddEventHandler('INV:displayPlayerInventory', function(from, playerInventory, name)
	TriggerClientEvent('INV:showOtherInventory', from, playerInventory, name)
end)


AddEventHandler('INV:displayVehicleInventory', function(from, playerInventory, name)
	TriggerClientEvent('INV:showOtherVehicleInventory', from, playerInventory, name)
end)


AddEventHandler('chatMessage', function(from,name,message)
	if(message:sub(1,1) == "/") then

		local args = stringsplit(message, " ")
		local cmd = args[1]


		if (cmd == "/search") then
			CancelEvent()

			if (args[2] ~= nil) then
				local playerID = tonumber(args[2])

				if(playerID == nil or players[playerID] == nil) then
					TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, "Invalid PlayerID.")
					return
				end

				TriggerClientEvent('INV:returnPlayerInventory', playerID, from, playerID)		

			else
				local customMessage = "Usage: /search <PlayerID>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end



		if (cmd == "/addItem") then
			CancelEvent()

			if (args[2] ~= nil) then
				local newItem = args[2]
				local itemCount = 1
				if(args[3] ~= nil) then
					itemCount = tonumber(args[3])
				end

				if(type(itemCount) == 'number') then

					TriggerClientEvent('INV:addPlayerItem', from, newItem, itemCount)
				else
					TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, "Please enter a valid number as the second argument.")
				end

			else 
				local customMessage = "Usage: /addItem <item_name> <amount(1 default)>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end


		if (cmd == "/inventory") then
			CancelEvent()

			TriggerClientEvent('INV:showPlayerInventory', from)

		end


		if (cmd == "/removeItem") then
			CancelEvent()

			if (args[2] ~= nil) then
				local removeItem = args[2]

				TriggerClientEvent('INV:removePlayerItem', from, removeItem)
			else 
				local customMessage = "Usage: /removeItem <item_name>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end


		if(cmd == "/clearInventory") then
			CancelEvent()

			TriggerClientEvent('INV:clearPlayerInventory', from)
		end







		if (cmd == "/vsearch") then
			CancelEvent()

			if (args[2] ~= nil) then
				local playerID = tonumber(args[2])

				if(playerID == nil or players[playerID] == nil) then
					TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, "Invalid PlayerID.")
					return
				end
				TriggerClientEvent('INV:returnVehicleInventory', playerID, from, playerID)		

			else
				local customMessage = "Usage: /vsearch <PlayerID>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end



		if (cmd == "/vaddItem") then
			CancelEvent()

			if (args[2] ~= nil) then
				local newItem = args[2]
				local itemCount = 1
				if(args[3] ~= nil) then
					itemCount = tonumber(args[3])
				end

				if(type(itemCount) == 'number') then

					TriggerClientEvent('INV:addVehicleItem', from, newItem, itemCount)
				else
					TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, "Please enter a valid number as the second argument.")
				end

			else 
				local customMessage = "Usage: /vaddItem <item_name> <amount(1 default)>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end


		if (cmd == "/vinventory") then
			CancelEvent()

			TriggerClientEvent('INV:showVehicleInventory', from)

		end


		if (cmd == "/vremoveItem") then
			CancelEvent()

			if (args[2] ~= nil) then
				local removeItem = args[2]

				TriggerClientEvent('INV:removeVehicleItem', from, removeItem)
			else 
				local customMessage = "Usage: /vremoveItem <item_name>"

				TriggerClientEvent('chatMessage', from, messageSender, messageSenderColor, customMessage)
			end
		end


		if(cmd == "/vclearInventory") then
			CancelEvent()

			TriggerClientEvent('INV:clearVehicleInventory', from)
		end


	end
end)



function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end