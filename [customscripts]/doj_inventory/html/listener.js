$(function()
{
    window.addEventListener('message', function(event)
    {
        var item = event.data;
        var buf = $('#wrap');
        var customHTML = "<caption>" + item.playerName + "</caption><tr class=\"heading\"><th>Item Name</th><th>Item Count</th></tr>"
        buf.find('table').append(customHTML);
        if (item.meta && item.meta == 'close') {
            document.getElementById("ptbl").innerHTML = "";
            $('#wrap').hide();
            return;
        }

        buf.find('table').append(item.text);
        $('#wrap').show();
    }, false);
});