description 'DOJ Inventory System'

-- Temporary?
ui_page 'html/inventory.html'

client_script "client.lua"
server_script "server.lua"

files {
    'html/inventory.html',
    'html/inventory.css',
    'html/reset.css',
    'html/listener.js',
    'html/res/futurastd-medium.css',
    'html/res/futurastd-medium.eot',
    'html/res/futurastd-medium.woff',
    'html/res/futurastd-medium.ttf',
    'html/res/futurastd-medium.svg'
}