RegisterNetEvent('INV:returnPlayerInventory')
RegisterNetEvent('INV:addPlayerItem')
RegisterNetEvent('INV:removePlayerItem')
RegisterNetEvent('INV:clearPlayerInventory')
RegisterNetEvent('INV:showPlayerInventory')
RegisterNetEvent('INV:showOtherInventory')


RegisterNetEvent('INV:returnVehicleInventory') 
RegisterNetEvent('INV:addVehicleItem')
RegisterNetEvent('INV:removeVehicleItem')
RegisterNetEvent('INV:clearVehicleInventory')
RegisterNetEvent('INV:showVehicleInventory')
RegisterNetEvent('INV:showOtherVehicleInventory')


local playerInventory = {}
local vehicleInventory = {}
local hudOn = false

messageSender = "DOJInventory"
messageSenderColors = {200,0,0}

TriggerServerEvent('INV:addPlayer')


AddEventHandler('INV:returnPlayerInventory', function(requester, me)
	local player = GetCurrentPlayer(me)
	local playerName = GetPlayerName(player)

	TriggerServerEvent('INV:displayPlayerInventory', requester, playerInventory, playerName)
end)


AddEventHandler('INV:addPlayerItem', function(itemName, amount)
	if(playerInventory[itemName] ~= nil) then
		playerInventory[itemName] = playerInventory[itemName] + amount
	else
		playerInventory[itemName] = amount
	end
	local customMessage = "Added " .. amount .. "x " .. itemName .. " to your inventory."
	
	TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
end)


AddEventHandler('INV:removePlayerItem', function(itemName)
	if(playerInventory[itemName] == nil) then
		local customMessage = itemName .. " does not exist in your inventory."
		TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
	else
		playerInventory[itemName] = nil
		local customMessage = "Removed Item: " .. itemName .. " from player inventory."
		TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
	end
end)


AddEventHandler('INV:clearPlayerInventory', function(itemName, amount)
	playerInventory = {}
	local customMessage = "Player inventory cleared."
	
	TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
end)


AddEventHandler('INV:showPlayerInventory', function()
	if not hudOn then
		local counter = 0
		local itemHTML = {}
		local playerName = "Personal Inventory"

		for item,amount in pairs(playerInventory) do
			local customHTML = '<tr><td>' .. item .. '</td><td>' .. amount .. '</td></tr>'
			table.insert(itemHTML, customHTML)
			counter = counter + 1
		end


		if(counter > 0) then 
			SendNUIMessage({ text = table.concat(itemHTML), playerName = "Personal Inventory"})
			hudOn = true

			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Personal Inventory Opened.")
		else
			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Personal Inventory Empty.")
		end
	else
		TriggerEvent('chatMessage', messageSender, messageSenderColors, "Please close the current interface.")
	end
end)


AddEventHandler('INV:showOtherInventory', function(otherPlayerInventory, targetName)
	if not hudOn then
		local itemHTML = {}
		local counter = 0

		for item,amount in pairs(otherPlayerInventory) do
			local customHTML = '<tr><td>' .. item .. '</td><td>' .. amount .. '</td></tr>'
			table.insert(itemHTML, customHTML)
			counter = counter + 1
		end

		local data = {
			text = table.concat(itemHTML),
			playerName = targetName .. " Personal Inventory"
		}

		if(counter > 0) then
			SendNUIMessage(data)
			hudOn = true

			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Target Inventory Opened.")
		else
			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Targets personal inventory is empty.")
		end
	else
		TriggerEvent('chatMessage', messageSender, messageSenderColors, "Please close the current interface.")
	end
end)










AddEventHandler('INV:returnVehicleInventory', function(requester, me)
	local player = GetCurrentPlayer(me)
	local playerName = GetPlayerName(player)


	TriggerServerEvent('INV:displayVehicleInventory', requester, vehicleInventory, playerName)
end)


AddEventHandler('INV:addVehicleItem', function(itemName, amount)
	if(vehicleInventory[itemName] ~= nil) then
		vehicleInventory[itemName] = vehicleInventory[itemName] + amount
	else
		vehicleInventory[itemName] = amount
	end
	local customMessage = "Added " .. amount .. "x " .. itemName .. " to your vehicle inventory."
	
	TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
end)


AddEventHandler('INV:removeVehicleItem', function(itemName)
	if(vehicleInventory[itemName] == nil) then
		local customMessage = itemName .. " does not exist in your vehicle inventory."
		TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
	else
		vehicleInventory[itemName] = nil
		local customMessage = "Removed Item: " .. itemName .. " from vehicle inventory."
		TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
	end
end)


AddEventHandler('INV:clearVehicleInventory', function(itemName, amount)
	vehicleInventory = {}
	local customMessage = "Vehicle inventory cleared."
	
	TriggerEvent('chatMessage', messageSender, messageSenderColors, customMessage)
end)



AddEventHandler('INV:showVehicleInventory', function()
	if not hudOn then
		local counter = 0
		local itemHTML = {}

		for item,amount in pairs(vehicleInventory) do
			local customHTML = '<tr><td>' .. item .. '</td><td>' .. amount .. '</td></tr>'
			table.insert(itemHTML, customHTML)
			counter = counter + 1
		end


		if(counter > 0) then 
			SendNUIMessage({ text = table.concat(itemHTML), playerName = "Personal Vehicle Inventory"})
			hudOn = true

			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Vehicle Inventory Opened.")
		else
			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Vehicle Inventory Empty.")
		end
	else
		TriggerEvent('chatMessage', messageSender, messageSenderColors, "Please close the current interface.")
	end
end)



AddEventHandler('INV:showOtherVehicleInventory', function(otherVehicleInventory, targetName)
	if not hudOn then
		local itemHTML = {}
		local counter = 0

		for item,amount in pairs(otherVehicleInventory) do
			local customHTML = '<tr><td>' .. item .. '</td><td>' .. amount .. '</td></tr>'
			table.insert(itemHTML, customHTML)
			counter = counter + 1
		end

		local data = {
			text = table.concat(itemHTML),
			playerName = targetName .. " Vehicle Inventory"
		}

		if(counter > 0) then
			SendNUIMessage(data)
			hudOn = true

			TriggerEvent('chatMessage', messageSender, messageSenderColors, "Target Inventory Opened.")
		else
			TriggerEvent('chatMessage', messageSender, messageSenderColor, "Targets vehicle inventory is empty.")
		end
	else
		TriggerEvent('chatMessage', messageSender, messageSenderColors, "Please close the current interface.")
	end
end)









function GetCurrentPlayer(myID)
    local player = 0

    for i = 0, 31 do
        if NetworkIsPlayerActive(i) then
            if (GetPlayerServerId(i) == myID) then
                player = i
                break
            end
        end
    end

    return player
end



Citizen.CreateThread(function()
	hudOn = false
	while true do
		Wait(10)  
		if hudOn then
			if IsControlJustPressed(0, 176) then
				hudOn = false
				SendNUIMessage({
					meta = 'close'
				})
				TriggerEvent('chatMessage', messageSender, messageSenderColors, "Inventory Closed.")
			end
		end
	end
end)