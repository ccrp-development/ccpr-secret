--Coded by Albo1125. Strictly no unauthorized redistribution. Please review the licence - breaking this has consequences.

sV = {}
local pO = false
pOP = nil
local pOV = {}
myPlayer = GetPlayerId()
local mPM = false
local wFS = false

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if(myPlayer == -1)then
			myPlayer = GetPlayerId()
		end 

		if IsControlJustReleased(0, 51) then	

			if(IsPedInAnyPoliceVehicle(GetPlayerPed(-1)))then
				if(sV[myPlayer] ~= nil)then
					if(pO == true)then
						if mPM == true then
							ShowNotification('Manual positioning mode exited')
							mPM = false
						else
							ShowNotification("Use arrow keys to move vehicle. Press E to exit manual positioning mode.")
							mPM = true
						end	
					end
				end
			end
		end
		
		if IsControlJustReleased(0, 21)then
			if(IsPedInAnyPoliceVehicle(GetPlayerPed(-1)))then
				if(sV[myPlayer] ~= nil)then	
					TriggerServerEvent("pOC")
					ShowNotification("Pullover cancelled.")
					pOP = nil
					pO = false
					wFS = false
				else
					local pc = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 10.0, 0.0)
					local closest = GetClosestVehicle(pc['x'], pc['y'], pc['z'], 10.0, 0, 70)
					if(IsVehicleSeatFree(closest, -1) == false)then
						pO = false
						pOP = nil
						
						if(closest ~= nil and closest ~= false and not IsPedPlayerPed(GetPedInVehicleSeat(closest, -1))) then
							
							TriggerServerEvent("selV_S")
							ShowNotification("Activate your siren to perform a traffic stop, or Shift to cancel.")
							wFS = true
						end
					end
				end	
			end
		end
		
		if(IsPedInAnyPoliceVehicle(GetPlayerPed(-1)) and wFS == true) then
			if IsVehicleSirenOn(GetVehiclePedIsIn(GetPlayerPed(-1), false)) then
				local pc = GetOffsetFromEntityInWorldCoords(sV[myPlayer], 5.0, 35.0, 0.0)
				TriggerServerEvent("pOV_S", pc['x'], pc['y'], pc['z'])
				wFS = false
			end
		end
		
		if mPM == true and pO == true then
			if IsControlJustReleased(0, 172) then
				manualMove('forward')
			elseif IsControlJustReleased(0, 173) then
				manualMove('backward')
			elseif IsControlJustReleased(0, 174) then
				manualMove('left')
			elseif IsControlJustReleased(0, 175) then
				manualMove('right')
			end
		end				
	end
end)

RegisterNetEvent("selV_C")
AddEventHandler("selV_C", function(owner)
	local pc = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 10.0, 0.0)
	local entity = GetClosestVehicle(pc['x'], pc['y'], pc['z'], 10.0, 0, 70)

	sV[owner] = entity
end)

RegisterNetEvent("pINGO_C")
AddEventHandler("pINGO_C", function(owner, x, y, z)		
	local entity = sV[owner]
	
	TaskVehicleDriveToCoord(GetPedInVehicleSeat(entity, -1), entity, x, y, z, 40, 1, GetEntityModel(entity), 2883621, 0, -1)
end)

RegisterNetEvent("pO_C")
AddEventHandler("pO_C", function(owner, x, y, z)
	if(owner == myPlayer)then
		pO = true
		pOP = GetPedInVehicleSeat(sV[myPlayer], -1)
	end

	FreezeEntityPosition(sV[owner], true)
	pOV[sV[owner]] = true
	
	FreezeEntityPosition(sV[owner], true)
end)		

RegisterNetEvent("pOC")
AddEventHandler("pOC", function(owner)
	pOV[sV[owner]] = nil
	SetVehicleEngineOn(sV[owner], true, true, true)
	TaskSetBlockingOfNonTemporaryEvents(GetPedInVehicleSeat(sV[owner], -1), false)
	SetVehicleSteerBias(sV[owner], 0.0)
	FreezeEntityPosition(sV[owner], false)
	sV[owner] = nil
end)	

RegisterNetEvent("drO")
AddEventHandler("drO", function(owner)
	FreezeEntityPosition(sV[owner], false)
	sV[owner] = nil
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		
		if(sV[myPlayer] ~= nil)then
			local playerLoc = GetEntityCoords(GetPlayerPed(-1))
			local vehLoc = GetEntityCoords(sV[myPlayer])
			if(Vdist(vehLoc['x'], vehLoc['y'], vehLoc['z'], playerLoc['x'], playerLoc['y'], playerLoc['z']) > 100.0)then
				TriggerServerEvent("drO", sV[myPlayer])
			end
			
			DrawMarker(0, vehLoc['x'], vehLoc['y'], vehLoc['z'] + 1.8, 0, 0, 0, 0, 0, 0, 0.5001, 0.5001, 0.5001, 255,255,0,255, 0,0, 0,0)
		end
		for k,v in pairs(pOV) do
			FreezeEntityPosition(k, true)
			SetVehicleEngineOn(k, false, true, true)
			TaskSetBlockingOfNonTemporaryEvents(GetPedInVehicleSeat(k, -1), true)
		end
	end
end)

function manualMove(direction)
	if(pO == false or sV == false or IsPedInAnyVehicle(GetPlayerPed(-1)) == false)then
	else
		
		local park = GetOffsetFromEntityInWorldCoords(sV[myPlayer], 0.0, 0.0, 0.0)
		local yaw = GetEntityHeading(sV[myPlayer])
		if(direction == "forward")then
			park = GetOffsetFromEntityInWorldCoords(sV[myPlayer], 0.0, 0.5, 0.0)
		elseif(direction == "backward")then
			park = GetOffsetFromEntityInWorldCoords(sV[myPlayer], 0.0, -0.5, 0.0)
		elseif(direction == "left")then
			park = GetOffsetFromEntityInWorldCoords(sV[myPlayer], -0.5, 0.0, 0.0)
		elseif(direction == "right")then
			park = GetOffsetFromEntityInWorldCoords(sV[myPlayer], 0.5, 0.0, 0.0)
		end
		
		TriggerServerEvent("mOV_S", park.x, park.y, park.z, yaw)
	end
end

RegisterNetEvent("pOMV_C")
AddEventHandler("pOMV_C", function(owner, x, y, z, yaw)
	pOV[sV[owner]] = nil
	FreezeEntityPosition(sV[owner], false)
	SetEntityCoords(sV[owner], x, y, z)
	SetEntityHeading(sV[owner], yaw)
	FreezeEntityPosition(sV[owner], true)
	pOV[sV[owner]] = true
end)