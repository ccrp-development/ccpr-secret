TrafficStop by Albo1125 (LUA, FiveReborn).
Please review the licence before using.

Press Shift (keyboard) or A (controller) to initiate a Traffic Stop on an AI vehicle. 
Activate your siren, wait for it to pull over, and optionally press E to manually position it.
Press Shift to cancel the Traffic Stop.