--Coded by Albo1125. Strictly no unauthorized redistribution. Please review the licence - breaking this has consequences.

RegisterServerEvent("selV_S")
AddEventHandler("selV_S", function()
	TriggerClientEvent("selV_C", -1, source)
end)

RegisterServerEvent("pOC")
AddEventHandler("pOC", function()
	TriggerClientEvent("pOC", -1, source)
end)

RegisterServerEvent("pOV_S")
AddEventHandler("pOV_S", function(x, y, z)
	TriggerClientEvent("notify", source, "Vehicle pulling over.")	
	TriggerClientEvent("pINGO_C", -1, source, x, y, z)
	
	SetTimeout(3500, function()
		TriggerClientEvent("notify", source, "Press E to enable manual positioning mode. Press Shift to cancel pullover.")
		TriggerClientEvent("pO_C", -1, source, x, y, z)
	end)
end)

RegisterServerEvent("print")
AddEventHandler("print", function(t)
	print(""..t)
end)

RegisterServerEvent("drO")
AddEventHandler("drO", function(ent)
	TriggerClientEvent("drO", -1, source)
end)

RegisterServerEvent("mOV_S")
AddEventHandler("mOV_S", function(x, y, z, yaw)
	TriggerClientEvent("pOMV_C", -1, source, x, y, z, yaw)
end)