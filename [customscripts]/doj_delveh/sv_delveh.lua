--[[
	
	Name: Server Delete Vehicle
	Type: server
	Created: 06/28/2017 12:48PM CST by Kenneth M. CIV-97
	Last Edited: 06/28/2017 4:30PM CST by Nic C. 
	Last Changes: Changed from core dependency to standalone.
	Description: This script handles deleting vehicles.
	
]]

--[[ Chat Message to Activate Vehicle Delete ]]--
RegisterServerEvent("chatMessage")
AddEventHandler('chatMessage', function(source, n, message)

	if message == "/delveh" or message == "/dv" then
	
		CancelEvent()
		TriggerClientEvent("delveh", source)
		
	end
end)
