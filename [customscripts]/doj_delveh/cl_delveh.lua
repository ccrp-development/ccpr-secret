--[[
	
	Name: Client Delete Vehicle
	Type: client
	Created: 06/28/2017 2:04PM CST by Kenneth M. CIV-97
	Last Edited: 06/28/2017 4:30PM CST by Nic C.
	Last Changes: Changed from core dependency to standalone.
	Description: This script handles deleting vehicles.
	
]]

RegisterNetEvent("delveh")
AddEventHandler("delveh", function()
	Citizen.Trace("HI")
	local ped = GetPlayerPed(-1)
	if (IsPedSittingInAnyVehicle(ped)) then
		local vehicle = GetVehiclePedIsIn(ped, false)
		if (GetPedInVehicleSeat(vehicle, -1) == ped) then
			SetEntityAsMissionEntity(vehicle, true, true)
			deleteVehicle(vehicle)
		else
			ShowNotification("~r~Error: ~w~You must be the driver of the vehicle.")
		end
	else
		local position = GetEntityCoords(ped)
		local inFront = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 2.0, 0.0)
		local vehicle = GetVehicleInDirection(position, inFront)
		if (DoesEntityExist(vehicle)) then
			SetEntityAsMissionEntity(vehicle, true, true)
			deleteVehicle(vehicle)
		else
			ShowNotification("~r~Error: ~w~You must be close to or in a vehicle.")
		end
	end
end)

function deleteVehicle(vehicle)
	Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(vehicle))
	if not (DoesEntityExist(vehicle)) then
		ShowNotification("~g~Success: ~w~Vehicle Deleted.")
	end
end

function ShowNotification( text )
    SetNotificationTextEntry( "STRING" )
    AddTextComponentString( text )
    DrawNotification( false, false )
end

function GetVehicleInDirection( coordFrom, coordTo )
    local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0)
    local _, _, _, _, vehicle = GetRaycastResult(rayHandle)
    return vehicle
end