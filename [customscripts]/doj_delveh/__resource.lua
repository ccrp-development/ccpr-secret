--[[
	
	Name: Delete Vehicle
	Type: both
	Created: 06/28/2017 12:48PM CST by Kenneth M. CIV-97
	Last Edited: 06/28/2017 4:30PM CST by Nic C. 
	Last Changes: Changed from core dependency to standalone.
	Description: Deletes vehicles
	
]]

server_script "sv_delveh.lua"
client_script "cl_delveh.lua"