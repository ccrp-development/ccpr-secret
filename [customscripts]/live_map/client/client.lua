local defaultDataSet = {
    ["pos"] = { x=0, y=0, z=0 }, -- Position of the player (vector(x, y, z))
    ["Vehicle"] = "none", -- Vehicle player is in (if any)
    ["Street"] = "Unknown", -- street
    ["icon"] = 6, -- Player blip id (will change with vehicles)
    ["Licence Plate"] = nil
}


local temp = {}

local beenUpdated =  {}

function updateData(name, value)
    table.insert(beenUpdated, name)
    defaultDataSet[name] = value
end

function doVehicleUpdate()
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), 0)

    if temp["vehicle"] ~= vehicle and vehicle ~= 0 then
        local vehicleClass = GetVehicleClass(vehicle)
        updateData("Vehicle", GetDisplayNameFromVehicleModel(GetEntityModel(vehicle)))
        temp["vehicle"] = vehicle
    end

    local plate = GetVehicleNumberPlateText(vehicle)

    if defaultDataSet["Licence Plate"] ~= plate then
        updateData("Licence Plate", plate)
    end
end

function doIconUpdate()
    local ped = GetPlayerPed(-1)
    local newSprite = 6

    if IsEntityDead(ped) then
        newSprite = 163
    else
        if IsPedSittingInAnyVehicle(ped) then

            local vehicle = temp["vehicle"]
            local vehicleModel = GetEntityModel(vehicle)
            local h = GetHashKey

            if vehicleModel == h("rhino") then
                newSprite = 421
            elseif (vehicleModel == h("lazer") or vehicleModel == h("besra") or vehicleModel == h("hydra")) then
                newSprite = 16 -- Jet
            elseif IsThisModelAPlane(vehicleModel) then
                newSprite = 90 -- Airport (plane icon)
            elseif IsThisModelAHeli(vehicleModel) then
                newSprite = 64 -- Helicopter
            elseif (vehicleModel == h("technical") or vehicleModel == h("insurgent") or vehicleModel == h("insurgent2") or vehicleModel == h("limo2")) then
                newSprite = 426 -- GunCar
            elseif (vehicleModel == h("dinghy") or vehicleModel == h("dinghy2") or vehicleModel == h("dinghy3")) then
                newSprite = 404 -- Dinghy
            elseif (vehicleModel == h("submersible") or vehicleModel == h("submersible2")) then
                newSprite = 308 -- Sub
            elseif IsThisModelABoat(vehicleModel) then
                newSprite = 410
            elseif (IsThisModelABike(vehicleModel) or IsThisModelABicycle(vehicleModel)) then
                newSprite = 226
            elseif (vehicleModel == h("policeold2") or vehicleModel == h("policeold1") or vehicleModel == h("policet") or vehicleModel == h("police") or vehicleModel == h("police2") or vehicleModel == h("police3") or vehicleModel == h("policeb") or vehicleModel == h("riot") or vehicleModel == h("sheriff") or vehicleModel == h("sheriff2") or vehicleModel == h("pranger")) then
                newSprite = 56 -- PoliceCar
            elseif vehicleModel == h("taxi") then
                newSprite = 198
            elseif (vehicleModel == h("brickade") or vehicleModel == h("stoackage") or vehicleModel == h("stoackage2")) then
                newSprite = 66 -- ArmoredTruck
            elseif (vehicleModel == h("towtruck") or vehicleModel == h("towtruck")) then
                newSprite = 68
            elseif (vehicleModel == h("trash") or vehicleModel == h("trash2")) then
                newSprite = 318
            else
                newSprite = 225 -- PersonalVehicleCar
            end
        end
    end

    if defaultDataSet["icon"] ~= newSprite then
        updateData("icon", newSprite)
    end
end

local firstSpawn = true


AddEventHandler("playerSpawned", function(spawn)
    if firstSpawn then
        TriggerServerEvent("livemap:playerSpawned")
        for key,val in pairs(defaultDataSet) do
            TriggerServerEvent("livemap:AddPlayerData", key, val)
        end

        firstSpawn = false
    end
end)

Citizen.CreateThread(function()
    while true do
        Wait(3000)

        if NetworkIsPlayerActive(PlayerId()) then

            local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1)))
            local x1,y1,z1 = defaultDataSet["pos"].x, defaultDataSet["pos"].y, defaultDataSet["pos"].z

            local dist = Vdist(x, y, z, x1, y1, z1)

            if (dist >= 50) then

                updateData("pos", {x = x, y=y, z=z} )
            end



            local playerPos = GetEntityCoords( GetPlayerPed( -1 ), true )
            local streetA, streetB = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, playerPos.x, playerPos.y, playerPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
            updateData("Street", GetStreetNameFromHashKey(streetA).." - " ..GetStreetNameFromHashKey(streetB))


            if temp["street"] ~= street then

                local mainStreet = GetStreetNameFromHashKey( streetA )
                updateData("Street", mainStreet)

                temp["street"] = mainStreet
            end


            if IsPedInAnyVehicle(GetPlayerPed(-1)) then
                doVehicleUpdate()

            elseif defaultDataSet["Licence Plate"] ~= nil or defaultDataSet["Vehicle"] ~= nil then

                defaultDataSet["Licence Plate"] = nil
                defaultDataSet["Vehicle"] = nil
                temp["vehicle"] = nil

                TriggerServerEvent("livemap:RemovePlayerData", "Licence Plate")
                TriggerServerEvent("livemap:RemovePlayerData", "Vehicle")
            end

            doIconUpdate()


            for i,k in pairs(beenUpdated) do

                TriggerServerEvent("livemap:UpdatePlayerData", k, defaultDataSet[k])
                table.remove(beenUpdated, i)
            end

        end

    end
end)
