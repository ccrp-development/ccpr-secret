RegisterServerEvent("chatMessage")
AddEventHandler('chatMessage', function(source, n, message)
	msg = stringsplit(message, " ")
	
	if msg[1] == "/emotes" then
		CancelEvent()
		TriggerClientEvent("show", source)		
	elseif msg[1] == "/emote" then
		CancelEvent()
		TriggerClientEvent("play", source, msg[2])
		
	elseif msg[1] == "/point" then
		CancelEvent()
		TriggerClientEvent("point", source)
	end
end)
		

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end
