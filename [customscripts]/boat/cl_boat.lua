Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
		local ped = GetPlayerPed(-1)
		local veh = GetVehiclePedIsIn(ped)
		if (veh ~= nil) then
			if (GetDisplayNameFromVehicleModel(GetEntityModel(veh)) == "DINGHY") then
				local boatCoords = GetEntityCoords(veh)
				local below = GetOffsetFromEntityInWorldCoords(veh, 1.0, 0.0, -1.0)
				local trailer = GetVehicleInDirection(boatCoords, below)
				if (GetDisplayNameFromVehicleModel(GetEntityModel(trailer)) == "BOATTRAILER") then
					if (IsEntityAttached(veh)) then
						Citizen.InvokeNative(0x8509B634FBE7DA11, "STRING") -- BEGIN_TEXT_COMMAND_DISPLAY_HELP
						Citizen.InvokeNative(0x5F68520888E69014, "Press ~INPUT_CONTEXT~ to detach boat.") -- _ADD_TEXT_COMPONENT_SCALEFORM
						Citizen.InvokeNative(0x238FFE5C7B0498A6, 0, false, true, -1) -- END_TEXT_COMMAND_DISPLAY_HELP
						if (IsControlJustReleased(1, 51)) then
							DetachEntity(veh,false,true)
						end
					else
						Citizen.InvokeNative(0x8509B634FBE7DA11, "STRING") -- BEGIN_TEXT_COMMAND_DISPLAY_HELP
						Citizen.InvokeNative(0x5F68520888E69014, "Press ~INPUT_CONTEXT~ to attach boat.") -- _ADD_TEXT_COMPONENT_SCALEFORM
						Citizen.InvokeNative(0x238FFE5C7B0498A6, 0, false, true, -1) -- END_TEXT_COMMAND_DISPLAY_HELP
						if (IsControlJustReleased(1, 51)) then
							AttachEntityToEntity(veh, trailer, 20, 0.0, -1.0, 0.25, 0.0, 0.0, 0.0, false, false, true, false, 20, true)
							TaskLeaveVehicle(ped, veh, 64)
						end
					end
				end
			elseif (GetDisplayNameFromVehicleModel(GetEntityModel(veh)) == "SEASHARK") then
				local boatCoords = GetEntityCoords(veh)
				local below = GetOffsetFromEntityInWorldCoords(veh, 1.0, 0.0, -1.0)
				local trailer = GetVehicleInDirection(boatCoords, below)
				if (GetDisplayNameFromVehicleModel(GetEntityModel(trailer)) == "BOATTRAILER") then
					if (IsEntityAttached(veh)) then
						Citizen.InvokeNative(0x8509B634FBE7DA11, "STRING") -- BEGIN_TEXT_COMMAND_DISPLAY_HELP
						Citizen.InvokeNative(0x5F68520888E69014, "Press ~INPUT_CONTEXT~ to detach boat.") -- _ADD_TEXT_COMPONENT_SCALEFORM
						Citizen.InvokeNative(0x238FFE5C7B0498A6, 0, false, true, -1) -- END_TEXT_COMMAND_DISPLAY_HELP
						if (IsControlJustReleased(1, 51)) then
							DetachEntity(veh,false,true)
						end
					else
						TriggerEvent("core:ShowHelpText", "Press ~INPUT_CONTEXT~ to attach boat.")
						if (IsControlJustReleased(1, 51)) then
							AttachEntityToEntity(veh, trailer, 20, 0.0, -1.0, -0.10, 0.0, 0.0, 0.0, false, false, true, false, 20, true)
							TaskLeaveVehicle(ped, veh, 64)
						end
					end
				end
			end
		end
	end
end)

function GetVehicleInDirection( coordFrom, coordTo )
    local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0)
    local _, _, _, _, vehicle = GetRaycastResult(rayHandle)
    return vehicle
end