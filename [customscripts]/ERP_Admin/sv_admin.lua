local permission = {
	member = 0,
	moderator = 1,
	admin = 2,
	superadmin = 3
}

TriggerEvent('erp:addAdminCommand', 'kick', permission.moderator, function(source, args, user)
		if(GetPlayerName(tonumber(args[2])))then
			local player = tonumber(args[2])

			-- User permission check
			TriggerEvent("erp:getPlayerFromId", player, function(target)
				if(tonumber(target.permission_level) > tonumber(user.permission_level))then
					TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "You're not allowed to kick this person.")
					return
				end

				local reason = args
				table.remove(reason, 1)
				table.remove(reason, 1)
				if(#reason == 0)then
					reason = "Kicked: You have been kicked from the server"
				else
					reason = "Kicked: " .. table.concat(reason, " ")
				end

				TriggerClientEvent('chatMessage', -1, "SYSTEM", {255, 0, 0}, "Player ^2" .. GetPlayerName(player) .. "^0 has been kicked(^2" .. reason .. "^0)")
				DropPlayer(player, reason)
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect player ID!")
		end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient permissions!")
end)

TriggerEvent('erp:addAdminCommand', 'ban', permission.admin, function(source, args, user)
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
			-- User permission check
			local player = tonumber(args[2])
			TriggerEvent("erp:getPlayerFromId", player, function(target)
					if(tonumber(target.permission_level) > tonumber(user.permission_level))then
						TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "You're not allowed to target this person!")
						return
					end
				local reason = args
				table.remove(reason, 1)
				table.remove(reason, 1)
				reason = "Banned: " .. table.concat(reason, " ")
				if(reason == "Banned: ")then
					reason = reason .. "You have been banned."
				end

				TriggerEvent("erp:setPlayerData", player, "banned", 1, function(response, success)
				end)

				TriggerEvent("erp:setPlayerData", player, "banreason", reason, function(response, success)
				end)

				DropPlayer(player, reason)

				TriggerClientEvent('chatMessage', -1, "SYSTEM", {255, 0, 0}, "Player ^2" .. GetPlayerName(player) .. "^0 has been banned(^2" .. reason .. "^0)")
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Incorrect player ID!")
		end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient permissions!")
end)

TriggerEvent('erp:addAdminCommand', 'announce', permission.moderator, function(source, args, user)
	table.remove(args, 1)
	TriggerClientEvent('chatMessage', -1, "ANNOUNCEMENT", {255, 0, 0}, "" .. table.concat(args, " "))
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Insufficient permissions!")
end)
