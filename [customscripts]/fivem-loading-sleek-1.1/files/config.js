/*
Author: @NicolasStr_
If you have any issue, please consider opening an issue on Github
https://github.com/NicolasStr/fivem-loading-sleek/
*/

/* CONFIG FILE */

/* BACKGROUNDS */
var backgrounds = [
	"img/bg1.jpg",
	"img/bg2.jpg",
	"img/bg3.jpg"
],

/* TEXTS DISPLAYED TOP RIGHT */
texts = [
	"EST. 2013",
	"Not Just a Community, but a Family.",
	"Where everyone has a chance to make a difference."
],

/* TEXT DISPLAYED TOP LEFT */
welcomeText = "Welcome to DoJRP",
serverName = "Server I",

/* ID OF YOUTUBE VIDEO FOR BACKGROUND, IF THIS IS NOT NULL, THIS WILL REPLACE ACTUALS BACKGROUNDS AND DISPLAY YOUTUBE VIDEO AS BACKGROUND */
youtubeID = "",

/* SET VOLUME TO 0 IF YOU DONT WANT SOUNDFILE TO LOAD OR/AND PLAY SOUNDFILE WILL BE DISABLED IF YOUTUBE VIDEO ID IS SET  */
volume = 0,
soundFile = "sound.ogg"
/* DEFAULT FILE INCLUDED: Not For Nothing - Otis McDonald, https://www.youtube.com/watch?v=Uy5ODz63TbM */