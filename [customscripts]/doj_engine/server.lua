admins = {
	-- Admin
	"steam:110000107903c9c", -- Polecat324
	"steam:1100001099e1703", -- Angel R.
	"steam:11000010fee4740", -- CrazyGhost314

        "steam:110000109B4B265", -- J. Brandon

	-- PD
	"steam:1100001071d9e86", -- Rakazul
	"steam:1100001089ad6ce", -- Nick M.
	"steam:110000107CFA66C", -- Andrew P.
	"steam:110000104CC1F43", -- Josh W.

	"steam:11000010EF21868", -- Tony S.
	"steam:1100001168BF46E", -- Chris W.
	"steam:110000107C3AC79", -- Samson B.
        "steam:11000010630FF17", -- Jon C.



	-- SO
	"steam:1100001018BF0FA", -- Ryon M. 3C-3
	"steam:110000108879BA0", -- Reid W. 3C-4

	"steam:1100001177C3282", -- Alex J.

	"steam:1100001059795ED", -- Casey W.
	"steam:1100001191B880B", -- Jacob B.
	"steam:11000010CBFF992", -- Nicholas B.
	"steam:11000010D97B073", -- Justin T.

	-- HP
	"steam:110000102f6a4bc", -- Baker D.
	"steam:110000107155D50", -- Martin D
	"steam:11000010e838fb0", -- Devon C.

	-- FD
	"steam:11000010820a0da", -- Ian P
	"steam:110000104753D3F", -- Ashton
	"steam:11000010EFBAA97", -- Sam

	"steam:11000010A1859C4", -- Daniel G.
	"steam:1100001080E5158", -- Tommy M.
	"steam:110000106C92FCD", -- Lawrence Y.
	
	-- CIV
	"steam:110000106F1F832", -- John S.
	"steam:110000108E06A3D", -- Svanik S. CIV-2

	-- CZG
	"steam:11000010434EA36", -- jfavignano
	"steam:11000010F65C2DD", -- Blackhallow
	"steam:11000010549CE12", -- Buggs

	-- HEAD DEV
	"steam:1100001043628cf", -- Sloosecannon
	"steam:110000106645a94", -- Stealth22
	
	
	-- Communications
	"steam:11000010DA0603D", -- Alex M. C-101
}

RegisterServerEvent("chatMessage")
AddEventHandler('chatMessage', function(source, n, message)
	pm = stringsplit(message, " ")
	
	if pm[1] == "/fix" then
	
		local id = GetPlayerIdentifiers(source)[1]
		good = false
		for i = 1, #admins do
			if (id == string.lower(admins[i])) then
				--TriggerClientEvent("fix", source)
				good = true
				--RconPrint("Good")
			end
			
		end
	
		if good then
			--RconPrint("Good")
			TriggerClientEvent("fix", source)
		elseif (good == false) then
			--RconPrint("Bad")
			TriggerClientEvent("chatMessage", source, "^1DOJ Engine", {255, 0, 0}, "You cannot use this command!")
		end
	end
end)
	
RegisterServerEvent("fixsync")
AddEventHandler('fixsync', function(vehicle)
	
	TriggerClientEvent("fixSync", -1)
	
end)
	
RegisterServerEvent("deadsync")
AddEventHandler('deadsync', function(vehicle)
	
	TriggerClientEvent("cardead", -1, vehicle)
	
end)
	
	

	
function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end
