--[[
	
	Name: Server Cuff Script
	Type: server
	Created: 06/27/2017 9:44AM CST by Kenneth M.
	Last Edit: 06/27/2017 1:01PM PST by Nic C. & Kenneth M.
	
]]

admins = {
	-- Admin
	"steam:110000107903c9c", -- Polecat324
	"steam:1100001099e1703", -- Angel R.
	"steam:11000010fee4740", -- CrazyGhost314

        "steam:110000109B4B265", -- J. Brandon

	-- PD
	"steam:1100001071d9e86", -- Rakazul
	"steam:1100001089ad6ce", -- Nick M.
	"steam:110000107CFA66C", -- Andrew P.
	"steam:110000104CC1F43", -- Josh W.

	"steam:11000010EF21868", -- Tony S.
	"steam:1100001168BF46E", -- Chris W.
	"steam:110000107C3AC79", -- Samson B.
        "steam:11000010630FF17", -- Jon C.



	-- SO
	"steam:1100001018BF0FA", -- Ryon M. 3C-3
	"steam:110000108879BA0", -- Reid W. 3C-4
	"steam:1100001177C3282", -- Alex J.

	"steam:1100001059795ED", -- Casey W.
	"steam:1100001191B880B", -- Jacob B.
	"steam:11000010CBFF992", -- Nicholas B.
	"steam:11000010D97B073", -- Justin T.

	-- HP
	"steam:110000102f6a4bc", -- Baker D.
	"steam:110000107155D50", -- Martin D
	"steam:11000010e838fb0", -- Devon C.

	-- FD
	"steam:11000010820a0da", -- Ian P
	"steam:110000104753D3F", -- Ashton
	"steam:11000010EFBAA97", -- Sam

	"steam:11000010A1859C4", -- Daniel G.
	"steam:1100001080E5158", -- Tommy M.
	"steam:110000106C92FCD", -- Lawrence Y.
	
	-- CIV
	"steam:110000106F1F832", -- John S.
	"steam:110000108E06A3D", -- Svanik S. CIV-2

	-- CZG
	"steam:11000010434EA36", -- jfavignano
	"steam:11000010F65C2DD", -- Blackhallow
	"steam:11000010549CE12", -- Buggs

	-- HEAD DEV
	"steam:1100001043628cf", -- Sloosecannon
	"steam:110000106645a94", -- Stealth22


	-- Communications
	"steam:11000010DA0603D", -- Alex M. C-101
}


RegisterServerEvent("cuffNear")
AddEventHandler("cuffNear", function(id)

	TriggerClientEvent("cuff1", id)

end)

RegisterServerEvent("chatMessage")
AddEventHandler('chatMessage', function(source, n, message)
    cm = stringsplit(message, " ")

    if cm[1] == "/kick" then
      CancelEvent()
    if (cm[2] ~= nil) and (cm[3] ~= nil) then
      local tPID = tonumber(cm[2])
	  
	  table.remove(cm, 1)
	  table.remove(cm, 1)
	  local reason = table.concat(cm, " ")
	  
	  
	  local id = GetPlayerIdentifiers(source)[1]
		good = false
		for i = 1, #admins do
			if (id == string.lower(admins[i])) then
				good = true
			end
			
		end
	
		if good then
			--RconPrint("Good")
			DropPlayer(tPID, reason)
		elseif (good == false) then
			--RconPrint("Bad")
			TriggerClientEvent("chatMessage", source, "^1DoJ", {255, 0, 0}, "You cannot use this command!")
		end
	  
	  
	 
      --TriggerClientEvent("cuff1", tPID)
	elseif (cm[2] == nil) then
		TriggerClientEvent( 'chatMessage', source, "^1DoJ", {255, 0, 0}, "Wrong usage: /kick [id] [reason]")
	elseif (cm[3] == nil) then
		TriggerClientEvent( 'chatMessage', source, "^1DoJ", {255, 0, 0}, "Wrong usage: /kick [id] [reason]")
    end
  end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function tablelength(T)
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end
