resource_type 'gametype' { name = 'EmergencyRP' }

-- Manifest
resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

description 'EmergencyRP by Albo1125.'

-- Server
server_script 'server/queries.lua'
server_script 'server/main.lua'
server_script 'server/util.lua'

-- Client
client_script 'client/main.lua'