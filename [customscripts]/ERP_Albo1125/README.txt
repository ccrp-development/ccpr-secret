EmergencyRP by Albo1125 - LUA, FiveReborn.
Please review the licence.
Thank you to Kanersps's Essentialmode for inspiration.

Make sure to force steam enabled on your server in citmp-server.yml. Change DisableAuth to false.

Installation of the sql file is complex. It requires an sql server to be running. Import the mysqldump using your sql management tool (cmd will do, use >source path.sql).

Installation of the three folders:
Replace the old chat_client and chat_server files in [system]/chat
Add ERP_Admin and ERP_Albo1125 as new resources.

Change the database password in ERP_Albo1125/server/queries.sql to that of your server.

New people joining will automatically be assigned to a civilian role as a member rank.

Default commands:
/me - shows info about yourself from the database.
/roles - shows the current roles with their IDs. Minimum rank: moderator.
/setrank PLAYERID RANKID - Changes the rank of PLAYERID to RANKID (e.g. admin, moderator). Minimum rank: admin.
/assignrole PLAYERID ROLEID - Changes role of PLAYERID to ROLEID (e.g. police, fire/ems). Minimum rank: moderator.
/kick PLAYERID - Kicks the PLAYERID from the server. Minimum rank: moderator
/ban PLAYERID REASON - Bans the PLAYERID from the server and stores the REASON. Minimum rank: admin.
/announce MESSAGE - Broadcasts the MESSAGE to everyone on the server (bypasses localchat). Minimum rank: moderator.

Ranks with IDs: 
member = 0
moderator = 1
admin = 2
superadmin = 3

Roles with IDs:
Type /roles ingame.