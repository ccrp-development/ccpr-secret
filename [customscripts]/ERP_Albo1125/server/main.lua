﻿-- Coded by Albo1125
-- Strictly no unauthorized redistribution. Please review the licence - breaking this has consequences.
Users = {}
commands = {}

local permission = {
	member = 0,
	moderator = 1,
	admin = 2,
	superadmin = 3
}

require "resources/[dojrp]/[customscripts]/ERP_Albo1125/lib/MySQL"

AddEventHandler('playerConnecting', function(name, setCallback)
	local identifiers = GetPlayerIdentifiers(source)
		for i = 1, #identifiers do
			local identifier = identifiers[i]
			local banned = checkBan(identifier)
			print("Checking user ban: (" .. name .. ") " .. identifier)

			if(banned)then
				if(tonumber(banned.banned) == 1)then
			  		setCallback(banned.banreason)
					CancelEvent()
				end
			return
	    end
	end
end)

AddEventHandler('playerDropped', function()
	Users[source] = nil
end)

local justJoined = {}

RegisterServerEvent('erp:firstJoinProper')
AddEventHandler('erp:firstJoinProper', function()
	local identifiers = GetPlayerIdentifiers(source)
	for i = 1, #identifiers do
		if(Users[source] == nil)then
			print("Loading user: " .. GetPlayerName(source))

			local identifier = identifiers[i]
			registerUser(identifier, source)

			TriggerEvent('erp:initialized', source)
			justJoined[source] = true
		end
	end
end)

RegisterServerEvent('playerSpawn')
AddEventHandler('playerSpawn', function()
	if(justJoined[source])then
		TriggerEvent("erp:firstSpawn", source)
		justJoined[source] = nil
	end
end)

AddEventHandler("onResourceStart", function(name)
	if(name ~= "EmergencyRP")then
		return
	end
	
	print("Resource restarted, loading users...")

	for value = 0, 1000 do
		local identifiers = GetPlayerIdentifiers(value)
		for i = 1, #identifiers do
			if(Users[value] == nil)then
				print("Loading user: " .. GetPlayerName(value))

				local identifier = identifiers[i]
				registerUser(identifier, value)
			end
	    end
	end
	print("Done")
end)

AddEventHandler('chatMessage', function(source, n, message)
	local command = stringsplit(message, " ")
	CancelEvent()
	if(string.sub(message, 1, 1) ~= "/") then 
		TriggerEvent("erp:getPlayerFromId", source, function(target)
			TriggerClientEvent('chatMessageRanged', -1, source, "^2"..getRoleName(target.role) .. "|^0 " .. GetPlayerName(source), message) 
			end
		)
	end
	for key,value in pairs(commands) do
		if("/" .. key == command[1])then
			CancelEvent()
			if(commands[key].perm > 0)then
				if(tonumber(Users[source]['permission_level']) >= commands[key].perm)then
					commands[key].cmd(source, command, Users[source])
					TriggerEvent("erp:adminCommandRan", source, command)
				else
					print("Admin command failed: ".. key .. ". Executer: ".. GetPlayerName(source))
					if commands[key].callbackfailed ~= nil then
						
						commands[key].callbackfailed(source, command, Users[source])
					end
				end
			elseif (tablelength(commands[key].roles) > 0) then
				if(tonumber(Users[source]['permission_level']) > 1 or contains(commands[key].roles, tonumber(Users[source]['role'])))then
					commands[key].cmd(source, command, Users[source])
					TriggerEvent("erp:rolesCommandRan", source, command)
				else
					print("Roled command failed: ".. key .. ". Executer: ".. GetPlayerName(source))
					if commands[key].callbackfailed ~= nil then
						commands[key].callbackfailed(source, command, Users[source])
					end
				end
			else
				commands[key].cmd(source, command, Users[source])
			end
		end
	end
end)

AddEventHandler('erp:addCommand', function(command, callback)
    print("Adding command: ".. command)
	commands[command] = {}
	commands[command].perm = 0
	commands[command].roles = {}
	commands[command].cmd = callback
end)

AddEventHandler('erp:addRolesCommand', function(command, roles, callback, callbackfailed)
    print("Adding roles command: ".. command.. " for ".. roles)
	commands[command] = {}
	commands[command].perm = 0
	commands[command].roles = roles
	commands[command].cmd = callback
	commands[command].callbackfailed = callbackfailed
end)

AddEventHandler('erp:addAdminCommand', function(command, perm, callback, callbackfailed)
	print("Adding admin command: ".. command.. ". Permlvl " .. perm)
	commands[command] = {}
	commands[command].perm = perm
	commands[command].cmd = callback
	commands[command].roles = {}
	commands[command].callbackfailed = callbackfailed
end)

RegisterServerEvent('erp:updatePositions')
AddEventHandler('erp:updatePositions', function(x, y, z)
		if(Users[source])then
			Users[source].coords = {['x'] = x, ['y'] = y, ['z'] = z}
		end
end)


commands['me'] = {}
commands['me'].perm = 0
commands['me'].roles = {}
commands['me'].cmd = function(source, args, user)

	TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "Your role is ^2".. getRoleName(user.role).. "^0. You are a ^2"..getkeyforvalue(permission, tonumber(user.permission_level)))
	
end

commands['roles'] = {}
commands['roles'].perm = 1
commands['roles'].roles = {}
commands['roles'].cmd = function(source, args, user)
	local count = 0
	while getRoleName(count) ~= nil do
		TriggerClientEvent('chatMessage', source, 'ROLES', {255, 0, 0}, count.. ":"..getRoleName(count))
		count = count + 1
	end
end

commands['setrank'] = {}
commands['setrank'].perm = 2
commands['setrank'].cmd = function(source, args, user)
	if tablelength(args) ~= 3 then return end
	if(GetPlayerName(tonumber(args[2])))then
		local player = tonumber(args[2])
		if player == source then return end
		local newperm = tonumber(args[3])
		if newperm > 3 or newperm < 0 then TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "Perms range: 0-2") return end
		-- User permission check
		TriggerEvent("erp:getPlayerFromId", player, function(target)
			if(tonumber(target.permission_level) > tonumber(user.permission_level))then
				TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "You're not allowed to re-assign this player's perms.")
				return
			elseif newperm >= tonumber(user.permission_level) and tonumber(user.permission_level) < 3 then
				TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "New perms must be lower than your own.")
				return
			end

			TriggerEvent("erp:setPlayerData", player, "permission_level", newperm, function(response, success)
				if success == true then
					TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "You changed ^2" .. GetPlayerName(player) .. "'s^0 role to ^2 " .. getkeyforvalue(permission, newperm))
					TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Your role was changed to ^2 " .. getkeyforvalue(permission, newperm))
				else
					print(response)
					TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, response)
				end
					
			end)

			
		end)
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Invalid player ID!")
	end
end

commands['assignrole'] = {}
commands['assignrole'].perm = 1
commands['assignrole'].cmd = function(source, args, user)
	if tablelength(args) ~= 3 then return end
	if(GetPlayerName(tonumber(args[2])))then
		local player = tonumber(args[2])
		local newrole = tonumber(args[3])
		if getRoleName(newrole) == nil then return end
		-- User permission check
		TriggerEvent("erp:getPlayerFromId", player, function(target)
			if(tonumber(target.permission_level) > tonumber(user.permission_level))then
				TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "You're not allowed to re-assign this player.")
				return
			end

			TriggerEvent("erp:setPlayerData", player, "role", newrole, function(response, success)
				if success == true then
					TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "You changed ^2" .. GetPlayerName(player) .. "'s^0 role to ^2 " .. getRoleName(newrole))
					TriggerClientEvent('chatMessage', player, "SYSTEM", {255, 0, 0}, "Your role was changed to ^2 " .. getRoleName(newrole))
				else
					print(response)
					TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, response)
				end
					
			end)

			
		end)
	else
		TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Invalid player ID!")
	end
end

print("EmergencyRP by Albo1125 (LUA, FiveReborn). Licencing and copyright apply. Requires SQL database.")
