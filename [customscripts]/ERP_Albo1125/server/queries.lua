-- Coded by Albo1125
-- Strictly no unauthorized redistribution. Please review the licence - breaking this has consequences.
require "resources/[dojrp]/[customscripts]/ERP_Albo1125/lib/MySQL"

-- MySQL:open("IP", "dbname", "user", "password")
MySQL:open("127.0.0.1", "EmergencyRP_Albo1125", "root", "doj2017")

function LoadUser(identifier, source)
	local executed_query = MySQL:executeQuery("SELECT * FROM users WHERE identifier = '@name'", {['@name'] = identifier})
	local result = MySQL:getResults(executed_query, {'role', 'permission_level', 'money', 'banned', 'banreason', 'identifier'}, "identifier")

	Users[source] = result[1]
	TriggerEvent('erp:playerLoaded', source, Users[source])
end

function checkBan(identifier)
	local executed_query = MySQL:executeQuery("SELECT * FROM users WHERE identifier = '@name'", {['@name'] = identifier})
	local result = MySQL:getResults(executed_query, {'banned', 'banreason'}, "identifier")

	return result[1]
end

function getRoleName(id)
	local executed_query = MySQL:executeQuery("SELECT * FROM roles WHERE Id = '@id'", {['@id'] = id})
	local result = MySQL:getResults(executed_query, {'name'}, "Id")
	if result[1] ~= nil then
		return result[1]['name']
	else
		return nil
	end
end

AddEventHandler('erp:getPlayers', function(cb)
	cb(Users)
end)

function hasAccount(identifier)
	local executed_query = MySQL:executeQuery("SELECT * FROM users WHERE identifier = '@name'", {['@name'] = identifier})
	local result = MySQL:getResults(executed_query, {'role', 'permission_level', 'money', 'banned', 'banreason'}, "identifier")

	if(result[1] ~= nil) then
		return true
	end
	return false
end

function hasPluginData(identifier)
	local executed_query = MySQL:executeQuery("SELECT * FROM plugin_data WHERE identifier = '@name'", {['@name'] = identifier})
	local result = MySQL:getResults(executed_query, {'identifier'}, "identifier")

	if(result[1] ~= nil) then
		return true
	end
	return false
end

function isLoggedIn(source)
	if(Users[GetPlayerName(source)] ~= nil)then
	if(Users[GetPlayerName(source)]['isLoggedIn'] == 1) then
		return true
	else
		return false
	end
	else
		return false
	end
end

function registerUser(identifier, source)
	if not hasAccount(identifier) then
		-- Inserting Default User Account Stats
		MySQL:executeQuery("INSERT INTO users (`identifier`, `permission_level`, `money`, `banned`, `banreason`, `role`) VALUES ('@username', '0', '0', '0', '', '0')",
		{['@username'] = identifier})

		-- Plugin data
		MySQL:executeQuery("INSERT INTO plugin_data (`identifier`, `vehicles`, `weapons`) VALUES ('@username', '', '')",
		{['@username'] = identifier})

		LoadUser(identifier, source)
	else
		LoadUser(identifier, source)
	end
end

AddEventHandler("erp:setPlayerData", function(user, k, v, cb)

	if(Users[user])then
			if(Users[user][k])then
			MySQL:executeQuery("UPDATE users SET @key='@value' WHERE identifier = '@identifier'",
		    {['@key'] = k, ['@value'] = v, ['@identifier'] = Users[user]['identifier']})

			Users[user][k] = v

			cb("Player data edited.", true)
		else
			cb("Column does not exist!", false)
		end
	else
		cb("User could not be found!", false)
	end
end)

AddEventHandler("erp:setPlayerDataId", function(user, k, v, cb)
	MySQL:executeQuery("UPDATE users SET @key='@value' WHERE identifier = '@identifier'",
	{['@key'] = k, ['@value'] = v, ['@identifier'] = user})

	cb("Player data edited.", true)
end)

AddEventHandler("erp:getPlayerFromId", function(user, cb)
	if(Users[user])then
		cb(Users[user])
	else
		cb(nil)
	end
end)
