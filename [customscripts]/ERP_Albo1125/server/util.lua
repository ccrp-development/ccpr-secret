function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function getPlayerIDFromName(n)
	for i = 0,200 do
		if(GetPlayerName(i) == n)then
			return i;
		end
	end
end

function returnIndexesInTable(t)
	local i = 0;
	for _,v in pairs(t)do
 		i = i + 1
	end
	return i;
end

function tableMerge(t1, t2)
    for k,v in pairs(t2) do
        if type(v) == "table" then
            if type(t1[k] or false) == "table" then
                tableMerge(t1[k] or {}, t2[k] or {})
            else
                t1[k] = v
            end
        else
            t1[k] = v
        end
    end
    return t1
end

function tablelength(T)
  if T == nil then return 0 end
  local count = 0
  for _ in pairs(T) do count = count + 1 end
  return count
end

function contains(table, val)

   for i=1,#table do
      if table[i] == val then 
         return true
      end
   end
   return false
end

function getkeyforvalue( t, value )
  for k,v in pairs(t) do
    if v==value then return k
  end
  end
  return nil
end
