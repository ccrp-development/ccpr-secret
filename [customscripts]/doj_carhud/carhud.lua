function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

function drawRct(x,y,width,height,r,g,b,a)
	DrawRect(x + width/2, y + height/2, width, height, r, g, b, a)
end

red = true

Citizen.CreateThread(function()
	while true do
	
		Citizen.Wait(1)
		local get_ped = GetPlayerPed(-1) -- current ped
		local get_ped_veh = GetVehiclePedIsIn(GetPlayerPed(-1),false) -- Current Vehicle ped is in
		local plate_veh = GetVehicleNumberPlateText(get_ped_veh) -- Vehicle Plate
		local veh_stop = IsVehicleStopped(get_ped_veh) -- Parked or not
		local veh_engine_health = GetVehicleEngineHealth(get_ped_veh) -- Vehicle Engine Damage 
		local veh_body_health = GetVehicleBodyHealth(get_ped_veh)
		local veh_burnout = IsVehicleInBurnout(get_ped_veh) -- Vehicle Burnout
		local veh_engine_running = IsVehicleEngineOn(get_ped_veh) -- Engine Running
		local veh_els_running = IsVehicleSirenOn(get_ped_veh)
		local veh_lights_headlights = GetVehicleLightsState(get_ped_veh, true, true)
		
		
		if(IsPedInAnyVehicle(GetPlayerPed(-1), false))then
			local mph = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 2.236936
			
			drawRct(0.11, 0.932, 0.046,0.03,0,0,0,100) 	-- UI:panel kmh	
			drawTxt(0.61, 1.42, 1.0,1.0,0.64 , "~w~" .. math.ceil(mph), 255, 255, 255, 255)  -- INT: kmh
			drawTxt(0.633, 1.432, 1.0,1.0,0.4, "~w~ mph", 255, 255, 255, 255)	-- TXT: kmh
			drawTxt(0.573, 1.245, 1.0,1.0,0.55, "~w~" .. plate_veh, 255, 255, 255, 255) -- TXT: Plate	

			if veh_burnout then
			drawTxt(0.545, 1.247, 1.0,1.0,0.44, "~r~DSC", 255, 255, 255, 200) -- TXT: DSC {veh_burnout}
			else
			drawTxt(0.545, 1.247, 1.0,1.0,0.44, "DSC", 255, 255, 255, 150)
			end		
			
			if veh_engine_running then
				drawTxt(0.629, 1.225, 1.0,1.0,0.45, "~g~ENG", 255, 255, 255, 200) -- TXT: Engine
			else
				drawTxt(0.629, 1.225, 1.0,1.0,0.45, "~r~ENG", 255, 255, 255, 200) -- TXT: Engine
			end
			
			--[[
			if veh_els_running then
				if red then
					drawTxt(0.524, 1.225, 1.0,1.0,0.45, "~r~ELS", 255, 255, 255, 200)
					red = false
					blue = true
				elseif blue then
					drawTxt(0.524, 1.225, 1.0,1.0,0.45, "~b~ELS", 255, 255, 255, 200)
					bluee = false
					red = true
				end
			else
				drawTxt(0.524, 1.225, 1.0,1.0,0.45, "~w~ELS", 255, 255, 255, 200) -- TXT: Engine
			end
			--]]
			
		--[[	if (veh_body_health > 975) and (veh_body_health < 1000) then
				drawTxt(0.629, 1.247, 1.0,1.0,0.45, "~y~Fluid", 255, 255, 255, 200) -- TXT: Fluid
				drawTxt(0.524, 1.247, 1.0,1.0,0.45, "~w~~y~Oil", 255, 255, 255, 200) -- TXT: Oil
				drawTxt(0.655, 1.247, 1.0,1.0,0.45, "~y~AC", 255, 255, 255, 200)
				--]]
			if  (veh_body_health < 940 and  (veh_engine_health < 870)) or (veh_body_health < 700) or (veh_engine_health < 870) then 
				--drawRct(0.159, 0.809, 0.005, 0,0,0,0,100)  -- panel damage
				--drawTxt(0.645, 1.247, 1.0,1.0,0.45, "~r~AC", 255, 255, 255, 200)
				drawTxt(0.629, 1.247, 1.0,1.0,0.45, "~r~Fluid", 255, 255, 255, 200) -- TXT: Fluid
				drawTxt(0.524, 1.247, 1.0,1.0,0.45, "~w~~r~Oil", 255, 255, 255, 200) -- TXT: Oil
				drawTxt(0.655, 1.247, 1.0,1.0,0.45, "~r~AC", 255, 255, 255, 200)
			else
				drawTxt(0.629, 1.247, 1.0,1.0,0.45, "Fluid", 255, 255, 255, 150) -- TXT: Fluid
				drawTxt(0.524, 1.247, 1.0,1.0,0.45, "Oil", 255, 255, 255, 150) -- TXT: Oil
			--	drawRct(0.159, 0.809, 0.005, veh_engine_health / 5800,0,0,0,100)  -- panel damage
				drawTxt(0.655, 1.247, 1.0,1.0,0.45, "~w~AC", 255, 255, 255, 150)
			end	
			
			if 2 > mph then
				SetVehicleBrakeLights(get_ped_veh, true)
			end

		end		
	end
end)
