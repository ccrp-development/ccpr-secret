var handlers = {
		startInitFunctionOrder: function(){
			objectsToBeLoaded++;
			objectsLoaded++;
		},
		initFunctionInvoking: function(){},
		startDataFileEntries: function(data){
			objectsToBeLoaded += Math.max(0, data.count - 1);

			$loaderBar.style.opacity = '1';
			handlers.onLogLine('Loading map data');
		},
		performMapLoadFunction: function(){
			objectsLoaded++;

			var p = Math.max(0, Math.min(100, 100 * objectsLoaded / objectsToBeLoaded));

			//$loaderBar.style.opacity = (p == 100 ? 0 : 1);
			$loaderBarProgress.style.width = p + '%';
			$loaderBarProgressText.textContent = Math.round(p);
		},
		onLogLine: function(data){
			$loaderMessage.setAttribute('text', data.message || data || "");
			$loaderMessage.textContent = data.message || data || "";
		}
	},
	
	$splashText = document.getElementById('splashText'),
	$loaderBar = document.getElementById('loaderBar'),
	$loaderBarProgress = document.querySelector('#loaderBar div'),
	$loaderBarProgressText = document.querySelector('#loaderBar span'),
	$loaderMessage = document.getElementById('loaderMessage'),
	$date = document.getElementById('date'),
	$audio = document.getElementById('audio'),
	
	splashTexts = [
	"EST. 2013",
	"Not Just a Community, but a Family.",
	"Where everyone has a chance to make a difference."
	],

	objectsToBeLoaded = -1,
	objectsLoaded = -1;






//set splashtext to random string from array. if localhost api is supported, then itterate through them in order not to get the same thing multiple times in a row
var splashString;
if(localStorage){
	var a = parseInt(localStorage.getItem('splashTextIterator') || Math.floor(Math.random() * 100))+1;
	localStorage.setItem('splashTextIterator', a);

	splashString = splashTexts[a % splashTexts.length];
}else{
	splashString = splashTexts[Math.floor(Math.random() * splashTexts.length)];
}
$splashText.setAttribute('text', splashString);
$splashText.textContent = splashString;



//set copyright date
var copyrightNotice = '\u00A9 '+Math.min(2017, new Date().getFullYear());
$date.setAttribute('text', copyrightNotice);
$date.textContent = copyrightNotice;


//animate the 3 dots in the loaderMessage
(function(){
	var iterator = 0;
	setInterval(function(){
		iterator++;

		let str = $loaderMessage.textContent.replace(/\s*\.*$/, ['','.','..','...'][iterator % 4]);
		$loaderMessage.setAttribute('text', str);
		$loaderMessage.textContent = str;
	}, 300);
})();



window.addEventListener('message', function(e){
	if(handlers[e.data.eventName]){
		handlers[e.data.eventName](e.data);
	}else{
		console.warn("Handler '"+e.data.eventName+"' doesn't exist!");
	}
});