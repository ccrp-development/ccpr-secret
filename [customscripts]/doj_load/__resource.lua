files {
	'index.html',
	'css/normalize.css',
	'css/style.css',
	'img/bg.jpg',
	'img/darken.png',
	'img/logo.png',
	'js/script.js',
	'misc/badboys.ogg',
	'misc/bankgothic.ttf'
}

loadscreen 'index.html'

resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'