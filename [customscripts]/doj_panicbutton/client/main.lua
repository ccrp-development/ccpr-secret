


RegisterNetEvent("Location")
AddEventHandler("Location", function(name)
		x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
		lastStreet, lastCross = GetStreetNameAtCoord(x, y, z)
        lastStreetName = GetStreetNameFromHashKey(lastStreet)
		lastCrossStreet = GetStreetNameFromHashKey(lastCross)
	    TriggerServerEvent("locationUpdate", lastStreetName, lastCrossStreet, name)			
end)