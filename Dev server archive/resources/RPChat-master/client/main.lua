
RegisterNetEvent('sendProximityMessage')
AddEventHandler('sendProximityMessage', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    TriggerEvent('chatMessage', "^4" .. name .. "", {0, 153, 204}, "^7 " .. message)
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    TriggerEvent('chatMessage', "^4" .. name .. "", {0, 153, 204}, "^7 " .. message)
  end
end)

RegisterNetEvent('sendProximityMessageMe')
AddEventHandler('sendProximityMessageMe', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Me | ^7" .. name.." "..message)
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Me | ^7" .. name.." "..message)
  end
end)

RegisterNetEvent('sendProximityMessageDo')
AddEventHandler('sendProximityMessageDo', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Do | ^7" .. name.." "..message)
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Do | ^7" .. name.." "..message)
  end
end)

RegisterNetEvent('sendProximityMessageLooc')
AddEventHandler('sendProximityMessageLooc', function(id, name, message)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Local OOC | ^5" .. name.." "..message)
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    TriggerEvent('chatMessage', "", {255,0,0}, "^3Local OOC | ^5" .. name.." "..message)
  end
end)

RegisterNetEvent('sendProximityMessageRoll')
AddEventHandler('sendProximityMessageRoll', function(id, name, num)
  local myId = PlayerId()
  local pid = GetPlayerFromServerId(id)
  if pid == myId then
    if num > 5 then
    TriggerEvent("chatMessage", "[ROLL]", {255,0,0}, "" .. name .. "'s Action ^2 Succeeded!")
    end
    if num < 5 then
    TriggerEvent("chatMessage", "[ROLL]", {255,0,0}, "" .. name .. "'s Action ^1 Failed!")
    end
  elseif GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(myId)), GetEntityCoords(GetPlayerPed(pid)), true) < 19.999 then
    if num > 5 then
    TriggerEvent("chatMessage", "[ROLL]", {255,0,0}, "" .. name .. "'s Action ^2 Succeeded!")
    end
    if num < 5 then
    TriggerEvent("chatMessage", "[ROLL]", {255,0,0}, "" .. name .. "'s Action ^1 Failed!")
    end
  end
end)

