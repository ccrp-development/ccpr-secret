Requirements
- ESX
- progressBars
- InteractSound
- vSync

Installation
- Extract to resources folder
- Start in server.cfg
- If sql file provided, import it.
- Check the config for options you might want to change.
- Make sure requirements are installed and started in server.cfg.


Usage

/purge
- Admin command
At 6 PM, following this announcement, the purge event will begin.