Config = {}
Config.Locale = 'en'

Config.DoorList = {

	--
	-- Mission Row First Floor
	--

	-- Entrance Doors
	{
		objName = 'v_ilev_ph_door01',
		objCoords  = {x = 434.747, y = -980.618, z = 30.839},
		textCoords = {x = 434.747, y = -981.50, z = 31.50},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 2.5
	},

	{
		objName = 'v_ilev_ph_door002',
		objCoords  = {x = 434.747, y = -983.215, z = 30.839},
		textCoords = {x = 434.747, y = -982.50, z = 31.50},
		authorizedJobs = { 'police' },
		locked = false,
		distance = 2.5
	},

	-- To locker room & roof
	{
		objName = 'v_ilev_ph_gendoor004',
		objCoords  = {x = 449.698, y = -986.469, z = 30.689},
		textCoords = {x = 450.104, y = -986.388, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Rooftop
	{
		objName = 'v_ilev_gtdoor02',
		objCoords  = {x = 464.361, y = -984.678, z = 43.834},
		textCoords = {x = 464.361, y = -984.050, z = 44.834},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Hallway to roof
	{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 461.286, y = -985.320, z = 30.839},
		textCoords = {x = 461.50, y = -986.00, z = 31.50},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Armory
	{
		objName = 'v_ilev_arm_secdoor',
		objCoords  = {x = 452.618, y = -982.702, z = 30.689},
		textCoords = {x = 453.079, y = -982.600, z = 31.739},
		authorizedJobs = { 'admin' },
		locked = true
	},

	-- Captain Office
	{
		objName = 'v_ilev_ph_gendoor002',
		objCoords  = {x = 447.238, y = -980.630, z = 30.689},
		textCoords = {x = 447.200, y = -980.010, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- To downstairs (double doors)
	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 443.97, y = -989.033, z = 30.6896},
		textCoords = {x = 444.020, y = -989.445, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	{
		objName = 'v_ilev_ph_gendoor005',
		objCoords  = {x = 445.37, y = -988.705, z = 30.6896},
		textCoords = {x = 445.350, y = -989.445, z = 31.739},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	-- 
	-- Mission Row Cells
	--

	-- Main Cells
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 463.815, y = -992.686, z = 24.9149},
		textCoords = {x = 463.30, y = -992.686, z = 25.10},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 1
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.381, y = -993.651, z = 24.914},
		textCoords = {x = 461.806, y = -993.308, z = 25.064},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 2
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.331, y = -998.152, z = 24.914},
		textCoords = {x = 461.806, y = -998.800, z = 25.064},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- Cell 3
	{
		objName = 'v_ilev_ph_cellgate',
		objCoords  = {x = 462.704, y = -1001.92, z = 24.9149},
		textCoords = {x = 461.806, y = -1002.450, z = 25.064},
		authorizedJobs = { 'police' },
		locked = true
	},

	-- To Back
	{
		objName = 'v_ilev_gtdoor',
		objCoords  = {x = 463.478, y = -1003.538, z = 25.005},
		textCoords = {x = 464.00, y = -1003.50, z = 25.50},
		authorizedJobs = { 'police' },
		locked = true
	},

	--
	-- Mission Row Back
	--

	-- Back (double doors)
	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 467.371, y = -1014.452, z = 26.536},
		textCoords = {x = 468.09, y = -1014.452, z = 27.1362},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	{
		objName = 'v_ilev_rc_door2',
		objCoords  = {x = 469.967, y = -1014.452, z = 26.536},
		textCoords = {x = 469.35, y = -1014.452, z = 27.136},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 4
	},

	-- Back Gate
	{
		objName = 'hei_prop_station_gate',
		objCoords  = {x = 488.894, y = -1017.210, z = 27.146},
		textCoords = {x = 488.894, y = -1020.210, z = 30.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 14,
		size = 2
	},

	--
	-- Sandy Shores
	--

	-- Entrance
	{
		objName = 'v_ilev_shrfdoor',
		objCoords  = {x = 1855.105, y = 3683.516, z = 34.266},
		textCoords = {x = 1855.105, y = 3683.516, z = 35.00},
		authorizedJobs = { 'police' },
		locked = true
	},

	--
	-- Paleto Bay
	--

	-- Entrance (double doors)
	{
		objName = 'v_ilev_shrf2door',
		objCoords  = {x = -443.14, y = 6015.685, z = 31.716},
		textCoords = {x = -443.14, y = 6015.685, z = 32.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},

	{
		objName = 'v_ilev_shrf2door',
		objCoords  = {x = -443.951, y = 6016.622, z = 31.716},
		textCoords = {x = -443.951, y = 6016.622, z = 32.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 2.5
	},

	--
	-- Bolingbroke Penitentiary
	--

	-- Entrance (Two big gates)
	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1844.998, y = 2604.810, z = 44.638},
		textCoords = {x = 1844.998, y = 2608.50, z = 48.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},

	{
		objName = 'prop_gate_prison_01',
		objCoords  = {x = 1818.542, y = 2604.812, z = 44.611},
		textCoords = {x = 1818.542, y = 2608.40, z = 48.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 12,
		size = 2
	},

	--
	-- Addons
	--

	
	-- Entrance Gate (Mission Row mod) https://www.gta5-mods.com/maps/mission-row-pd-ymap-fivem-v1
	{
		objName = 'Prop_Gate_airport_01',
		objCoords  = {x = 411.94, y = -1025.39, z = 29.33},
		textCoords = {x = 412.64, y = -1021.49, z = 32.00},
		authorizedJobs = { 'police' },
		locked = true,
		distance = 14,
		size = 2
	},

		-- Side Door (Mission Row mod) https://www.gta5-mods.com/maps/mission-row-pd-ymap-fivem-v1
		{
			objName = 'Prop_BS_Map_Door_01',
			objCoords  = {x = 423.85, y = -998.05, z = 30.77},
			textCoords = {x = 423.85, y = -998.05, z = 31.37},
			authorizedJobs = { 'police' },
			locked = true,
			distance = 2.5
		},

		--Sandy Shores Jail Cell 1
		{
			objName = 'prop_ld_jail_door',
			objCoords  = {x = 1849.713, y = 3708.264, z = 1.21},
			textCoords = {x = 1849.713, y = 3708.264, z = 1.21},
			authorizedJobs = { 'police' },
			locked = true
		},

				--Sandy Shores Jail Cell 2
		{
			objName = 'prop_ld_jail_door',
			objCoords  = {x = 1847.71, y = 3710.89, z = 1.21},
			textCoords = {x = 1847.71, y = 3710.89, z = 1.21},
			authorizedJobs = { 'police' },
			locked = true
		},

				--Sandy Shores Jail Cell 3	
		{
			objName = 'prop_ld_jail_door',
			objCoords  = {x = 1845.86, y = 3705.238, z = 1.21},
			textCoords = {x = 1845.86, y = 3705.238, z = 1.21},
			authorizedJobs = { 'police' },
			locked = true
		},

				--Sandy Shores Jail Cell 4
		{
			objName = 'prop_ld_jail_door',
			objCoords  = {x = 1843.79, y = 3707.81, z = 1.21},
			textCoords = {x = 1843.79, y = 3707.81, z = 1.21},
			authorizedJobs = { 'police' },
			locked = true
		},

				--Stickys House
		{
			objName = 'prop_biolab_g_door',
			objCoords  = {x = -1343.5, y = -744.63, z = 23.35},
			textCoords = {x = -1343.5, y = -744.63, z = 23.35},
			authorizedJobs = { 'admin' },
			locked = true,
			distance = 2.5
		},

				--Mission Row Doors NEW
					--Side Door 1 (Garage Enterance)
				{
					objName = 'v_ilev_gtdoor',
					objCoords  = {x = 444.62, y = -999.00, z = 30.78},
					textCoords = {x = 445.62, y = -998.50, z = 30.78},
					authorizedJobs = { 'police' },
					locked = true,
					distance = 1.0
				},

					--Side Door 2 (Garage Enterance)
				{
					objName = 'v_ilev_gtdoor',
					objCoords  = {x = 447.21, y = -999.00, z = 30.78},
					textCoords = {x = 447.21, y = -998.50, z = 30.78},
					authorizedJobs = { 'police' },
					locked = true,
					distance = 1.0
				},

				
					--Drunk Tank 1
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 467.19220, y = -996.45940, z = 25.00599},
						textCoords = {x = 467.19220, y = -996.45940, z = 25.00599},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Drunk Tank 2 
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 471.47550, y = -996.45940, z = 25.00599},
						textCoords = {x = 471.47550, y = -996.45940, z = 25.00599},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Drunk Tank 3 
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 475.75430, y = -996.45940, z = 25.00599},
						textCoords = {x = 475.75430, y = -996.45940, z = 25.00599},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Drunk Tank 4
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 480.03010, y = -996.45940, z = 25.00599},
						textCoords = {x = 480.03010, y = -996.45940, z = 25.00599},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Interview 1
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 471.47470, y = -1003.53800, z = 25.01223},
						textCoords = {x = 471.47470, y = -1003.53800, z = 25.01223},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Interview 1 (interagation)
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 468.48720, y = -1003.54800, z = 25.01314},
						textCoords = {x = 468.48720, y = -1003.54800, z = 25.01314},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},
				
					--Interview 2
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 480.03010, y = -1003.53800, z = 25.00599},
						textCoords = {x = 480.03010, y = -1003.53800, z = 25.00599},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Interview 2 (interagation)
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 477.04970, y = -1003.55300, z = 25.01203},
						textCoords = {x = 477.04970, y = -1003.55300, z = 25.01203},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},
					
					--Server Room
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 467.59360, y = -977.99330, z = 25.05795},
						textCoords = {x = 467.59360, y = -977.99330, z = 25.05795},
						authorizedJobs = { 'admin' },
						locked = true,
						distance = 2.5
					},

					--Lab
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 463.61460, y = -980.58140, z = 25.05795},
						textCoords = {x = 463.61460, y = -980.58140, z = 25.05795},
						authorizedJobs = { 'ambulance','police' },
						locked = true,
						distance = 2.5
					},		
					
					--Storage
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 471.31540, y = -986.10910, z = 25.05795},
						textCoords = {x = 471.31540, y = -986.10910, z = 25.05795},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},	

					--Medical
					{
						objName = 'v_ilev_ph_gendoor003',
						objCoords  = {x = 438.47100, y = -979.55300, z = 26.82234},
						textCoords = {x = 438.47100, y = -979.55300, z = 26.82234},
						authorizedJobs = { 'ambulance' },
						locked = true,
						distance = 2.5
					},	

					--Unknown Office (Basement)
					{
						objName = 'v_ilev_ph_gendoor003',
						objCoords  = {x = 444.19480, y = -979.55280, z = 26.81845},
						textCoords = {x = 444.19480, y = -979.55280, z = 26.81845},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},	
					
					--Briefing Room
					{
						objName = 'v_ilev_ph_gendoor003',
						objCoords  = {x = 452.63710, y = -979.55460, z = 26.81980},
						textCoords = {x = 452.63710, y = -979.55460, z = 26.81980},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},	

					--Booking Room
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 442.66250, y = -988.24130, z = 26.81977},
						textCoords = {x = 442.66250, y = -988.24130, z = 26.81977},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},	

					--Lineup Viewing
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 442.04760, y = -986.61280, z = 26.81977},
						textCoords = {x = 442.04760, y = -986.61280, z = 26.81977},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Dispatch Door 1
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 444.74730, y = -988.83990, z = 36.05274},
						textCoords = {x = 444.74730, y = -988.83990, z = 36.05274},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Dispatch Door 2
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 449.84080, y = -982.46700, z = 36.04901},
						textCoords = {x = 449.84080, y = -982.46700, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Main Door 2nd Floor
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 449.80230, y = -992.06960, z = 36.04901},
						textCoords = {x = 449.80230, y = -992.06960, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Detective Office Door 1 (2nd Floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 451.29800, y = -980.25380, z = 36.04901},
						textCoords = {x = 451.29800, y = -980.25380, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Detective Office Door 2 (2nd Floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 461.95310, y = -980.25750, z = 36.04901},
						textCoords = {x = 461.95310, y = -980.25750, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--2nd Floor Stairwell
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 463.70000, y = -983.37900, z = 35.99438},
						textCoords = {x = 463.70000, y = -983.37900, z = 35.99438},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Janitors Closet (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 463.70000, y = -989.63120, z = 36.04901},
						textCoords = {x = 463.70000, y = -989.63120, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Meeting Room Door 1 (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 474.62360, y = -991.97270, z = 36.05113},
						textCoords = {x = 474.62360, y = -991.97270, z = 36.05113},
						authorizedJobs = { 'police','admin' },
						locked = true,
						distance = 2.5
					},

					--Meeting Room Door 2 (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 484.03650, y = -991.99790, z = 36.05113},
						textCoords = {x = 484.03650, y = -991.99790, z = 36.05113},
						authorizedJobs = { 'police','admin' },
						locked = true,
						distance = 2.5
					},

					--LEO Office 1 (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 477.88280, y = -1000.39600, z = 36.04901},
						textCoords = {x = 477.88280, y = -1000.39600, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},
					
					--LEO Office 2 (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 477.88280, y = -1009.68300, z = 36.04901},
						textCoords = {x = 477.88280, y = -1009.68300, z = 36.04901},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 2.5
					},

					--Bathroom (2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 468.42920, y = -997.06460, z = 36.04901},
						textCoords = {x = 468.42920, y = -997.06460, z = 36.04901},
						authorizedJobs = { 'police', 'admin', 'ambulance' },
						locked = true,
						distance = 2.5
					},

					--Command Office(2nd floor)
					{
						objName = 'v_ilev_ph_gendoor002',
						objCoords  = {x = 463.41680, y = -1001.01500, z = 36.05486},
						textCoords = {x = 463.41680, y = -1001.01500, z = 36.05486},
						authorizedJobs = { 'police', 'admin' },
						locked = true,
						distance = 2.5
					},

					--DHS Office(2nd floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 459.92210, y = -995.38750, z = 36.05130},
						textCoords = {x = 459.92210, y = -995.38750, z = 36.05130},
						authorizedJobs = { 'police', 'admin' },
						locked = true,
						distance = 2.5
					},

					--Shower 1(Basement floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 466.78690, y = -1004.52500, z = 30.16130},
						textCoords = {x = 466.78690, y = -1004.52500, z = 30.16130},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},					
	

					--Shower 1(Basement floor)
					{
						objName = 'v_ilev_ph_gendoor006',
						objCoords  = {x = 464.28430, y = -1004.52700, z = 30.16130},
						textCoords = {x = 464.28430, y = -1004.52700, z = 30.16130},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},			
					
					--Sandy Evidence Locker
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 1851.11800, y = 3696.45700, z = 34.42181},
						textCoords = {x = 1851.11800, y = 3696.45700, z = 34.42181},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},

					--Sandy Locker Room
					{
						objName = 'v_ilev_rc_door2',
						objCoords  = {x = 1858.76100, y = 3691.17700, z = 34.41916},
						textCoords = {x = 1858.76100, y = 3691.17700, z = 34.41916},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},

					--Sandy Meeting Room
					{
						objName = 'v_ilev_gtdoor',
						objCoords  = {x = 1856.12600, y = 3689.65600, z = 34.41916},
						textCoords = {x = 1856.12600, y = 3689.65600, z = 34.41916},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},

					--Paleto Office
					{
						objName = 'v_ilev_rc_door2',
						objCoords  = {x = -450.71600, y = 6016.37000, z = 31.86633},
						textCoords = {x = -450.71600, y = 6016.37000, z = 31.86633},
						authorizedJobs = { 'police' },
						locked = true,
						distance = 1.5
					},
}