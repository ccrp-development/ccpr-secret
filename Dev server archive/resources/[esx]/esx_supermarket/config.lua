Config              = {}
Config.DrawDistance = 0
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 1
Config.Locale       = 'en'

Config.Zones = {


	PDShop = {
		Items = {},
		Pos = {
			{x = 468.05,   y = -990.19, z = 30.69}
		}
	},

	CityHall = {
		Items = {},
		Pos = {
			{x = 273.2,   y = -277.85, z = 53.94}
		}
	},

	DMV = {
		Items = {},
		Pos = {
			{x = -206.42,   y = -1913.19, z = 27.79}
		}
	},

	
}
