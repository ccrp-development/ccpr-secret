Config                            = {}

Config.Teleporters = {

	['City Hall'] = {
		['Job'] = 'none',
		['Enter'] = { 
			['x'] = 237.97, 
			['y'] = -413.14, 
			['z'] = 48.11,
			['Information'] = '[E] Enter City Hall' 
		},

		['Exit'] = {
			['x'] = 277.75, 
			['y'] = -268.36, 
			['z'] = 53.94, 
			['Information'] = '[E] Exit City Hall' 
		}
	},

	['Court House'] = {
		['Job'] = 'none',
		['Enter'] = { 
			['x'] = 233.26, 
			['y'] = -411.4, 
			['z'] = 48.11,
			['Information'] = '[E] Enter Court House' 
		},

		['Exit'] = {
			['x'] = 235.9, 
			['y'] = -413.43, 
			['z'] = -118.16, 
			['Information'] = '[E] Exit Court House' 
		}
	},
	
	--Next here
}


