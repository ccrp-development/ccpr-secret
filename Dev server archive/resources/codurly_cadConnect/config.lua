Config = {}

--[[ 
************ *****
BASIC CONFIGURATION

Required setup and settings 
for scripts to function.
************ *****
]]--

--- Secret ID found in CAD owner panel under 'FiveM Integration' ---
-- !! DO NOT SHARE THIS SECRET ID !! --
Config.SecretID = "GLaXLJE7xnNADtv"

-- Set to true if you have ESX Properties installed on your server. Set to false if not. --
Config.ESXProperties = true

-- Server Number, set this to match the number in your CAD Owner Panel --
Config.ServerNumber = 1




--[[
##################
ADVANCED CONFIGURATION

Advanced settings to personalize
the scripts.
##################
]]--



--  Print all successful requests to server console --
Config.PrintSuccess = true

--  Endpoint API URL :: DONT TOUCH --
Config.APIURL = "https://plugin-testing.bubbleapps.io/version-test"