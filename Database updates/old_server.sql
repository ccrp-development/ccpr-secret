-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2019 at 01:58 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `old server`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(10, 'society_police', 'Police', 1),
(11, 'property_black_money', 'Argent Sale Propriété', 0),
(12, 'society_realestateagent', 'Agent immobilier', 1),
(13, 'caution', 'Caution', 0),
(14, 'society_taxi', 'Taxi', 1),
(15, 'society_cardealer', 'Concessionnaire', 1),
(16, 'society_banker', 'Banque', 1),
(17, 'bank_savings', 'Livret Bleu', 0),
(18, 'society_mecano', 'Mécano', 1),
(19, 'society_ambulance', 'Ambulance', 1),
(20, 'society_cardealer', 'Concessionnaire', 1),
(21, 'society_foodtruck', 'Foodtruck', 1),
(22, 'society_fire', 'fire', 1),
(23, 'society_mecano', 'Mécano', 1),
(24, 'society_fire', 'fire', 1),
(25, 'society_unicorn', 'Unicorn', 1),
(26, 'society_airlines', 'Airlines', 1),
(27, 'society_foodtruck', 'Foodtruck', 1),
(28, 'society_avocat', 'Advokat', 1),
(29, 'society_journaliste', 'journaliste', 1),
(30, 'society_karting', 'Karing', 1),
(31, 'society_parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(420, 'society_police', 145, NULL),
(421, 'society_realestateagent', 0, NULL),
(422, 'society_taxi', 0, NULL),
(423, 'society_cardealer', 0, NULL),
(424, 'society_banker', 0, NULL),
(425, 'society_mecano', 0, NULL),
(426, 'society_ambulance', 0, NULL),
(427, 'society_foodtruck', 0, NULL),
(497, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(498, 'caution', 0, 'steam:11000010a01bdb9'),
(499, 'bank_savings', 0, 'steam:11000010a01bdb9'),
(500, 'bank_savings', 0, 'steam:1100001068ef13c'),
(501, 'caution', 0, 'steam:1100001068ef13c'),
(502, 'property_black_money', 0, 'steam:1100001068ef13c'),
(503, 'property_black_money', 0, 'steam:110000132580eb0'),
(504, 'bank_savings', 0, 'steam:110000132580eb0'),
(505, 'caution', 0, 'steam:110000132580eb0'),
(506, 'society_fire', 0, NULL),
(507, 'property_black_money', 0, 'steam:110000112f07694'),
(508, 'caution', 0, 'steam:110000112f07694'),
(509, 'bank_savings', 0, 'steam:110000112f07694'),
(510, 'property_black_money', 0, 'steam:1100001042b46e1'),
(511, 'caution', 0, 'steam:1100001042b46e1'),
(512, 'bank_savings', 0, 'steam:1100001042b46e1'),
(513, 'property_black_money', 0, 'steam:11000011820eeac'),
(514, 'bank_savings', 0, 'steam:11000011820eeac'),
(515, 'caution', 0, 'steam:11000011820eeac'),
(516, 'property_black_money', 0, 'steam:11000011258948d'),
(517, 'caution', 0, 'steam:11000011258948d'),
(518, 'bank_savings', 0, 'steam:11000011258948d'),
(519, 'society_unicorn', 9070, NULL),
(520, 'society_airlines', 0, NULL),
(521, 'society_avocat', 0, NULL),
(522, 'property_black_money', 0, 'steam:110000114590956'),
(523, 'bank_savings', 0, 'steam:110000114590956'),
(524, 'caution', 0, 'steam:110000114590956'),
(525, 'caution', 0, 'steam:110000115c5ddc9'),
(526, 'property_black_money', 0, 'steam:110000115c5ddc9'),
(527, 'bank_savings', 0, 'steam:110000115c5ddc9'),
(528, 'society_journaliste', 100000, NULL),
(529, 'society_karting', 100, NULL),
(530, 'property_black_money', 0, 'steam:110000109660ef9'),
(531, 'caution', 0, 'steam:110000109660ef9'),
(532, 'bank_savings', 0, 'steam:110000109660ef9'),
(533, 'bank_savings', 0, 'steam:11000010c6acdce'),
(534, 'property_black_money', 0, 'steam:11000010c6acdce'),
(535, 'caution', 0, 'steam:11000010c6acdce'),
(536, 'bank_savings', 0, 'steam:110000106a5790d'),
(537, 'caution', 0, 'steam:110000106a5790d'),
(538, 'property_black_money', 0, 'steam:110000106a5790d'),
(539, 'property_black_money', 0, 'steam:11000010b0a1bc3'),
(540, 'bank_savings', 0, 'steam:11000010b0a1bc3'),
(541, 'caution', 0, 'steam:11000010b0a1bc3'),
(542, 'bank_savings', 0, 'steam:11000011b78df95'),
(543, 'caution', 0, 'steam:11000011b78df95'),
(544, 'property_black_money', 0, 'steam:11000011b78df95'),
(545, 'property_black_money', 0, 'steam:110000135c88cca'),
(546, 'caution', 0, 'steam:110000135c88cca'),
(547, 'bank_savings', 0, 'steam:110000135c88cca'),
(548, 'society_parking', 10000, NULL),
(549, 'property_black_money', 0, 'steam:11000010473daf1'),
(550, 'bank_savings', 0, 'steam:11000010473daf1'),
(551, 'caution', 0, 'steam:11000010473daf1'),
(552, 'bank_savings', 0, 'steam:110000103b294e0'),
(553, 'property_black_money', 0, 'steam:110000103b294e0'),
(554, 'caution', 0, 'steam:110000103b294e0'),
(555, 'property_black_money', 0, 'steam:110000104e0ed21'),
(556, 'caution', 0, 'steam:110000104e0ed21'),
(557, 'bank_savings', 0, 'steam:110000104e0ed21'),
(558, 'property_black_money', 0, 'steam:1100001238ac476'),
(559, 'bank_savings', 0, 'steam:1100001238ac476'),
(560, 'caution', 0, 'steam:1100001238ac476'),
(561, 'property_black_money', 0, 'steam:11000011742efb1'),
(562, 'caution', 0, 'steam:11000011742efb1'),
(563, 'bank_savings', 0, 'steam:11000011742efb1'),
(564, 'caution', 0, 'steam:110000112969e8f'),
(565, 'property_black_money', 0, 'steam:110000112969e8f'),
(566, 'bank_savings', 0, 'steam:110000112969e8f'),
(567, 'bank_savings', 0, 'steam:110000134857568'),
(568, 'property_black_money', 0, 'steam:110000134857568'),
(569, 'caution', 0, 'steam:110000134857568'),
(570, 'caution', 0, 'steam:110000131f0d83c'),
(571, 'property_black_money', 0, 'steam:110000131f0d83c'),
(572, 'bank_savings', 0, 'steam:110000131f0d83c'),
(573, 'property_black_money', 0, 'steam:110000110931da8'),
(574, 'bank_savings', 0, 'steam:110000110931da8'),
(575, 'caution', 0, 'steam:110000110931da8'),
(576, 'property_black_money', 0, 'steam:1100001185894b3'),
(577, 'bank_savings', 0, 'steam:1100001185894b3'),
(578, 'caution', 0, 'steam:1100001185894b3'),
(579, 'bank_savings', 0, 'steam:11000011a15fea8'),
(580, 'property_black_money', 0, 'steam:11000011a15fea8'),
(581, 'caution', 0, 'steam:11000011a15fea8'),
(582, 'caution', 0, 'steam:1100001183ded9e'),
(583, 'property_black_money', 0, 'steam:1100001183ded9e'),
(584, 'bank_savings', 0, 'steam:1100001183ded9e'),
(585, 'bank_savings', 0, 'steam:110000115815ad6'),
(586, 'caution', 0, 'steam:110000115815ad6'),
(587, 'property_black_money', 0, 'steam:110000115815ad6');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_admin', 'Admin', 1),
(4, 'property', 'Propriété', 0),
(5, 'society_taxi', 'Taxi', 1),
(6, 'society_cardealer', 'Dealer', 1),
(7, 'society_mecano', 'Mechanic', 1),
(8, 'society_cardealer', 'Concesionnaire', 1),
(9, 'society_fire', 'fire', 1),
(10, 'society_mecano', 'Mécano', 1),
(11, 'society_fire', 'fire', 1),
(12, 'society_unicorn', 'Unicorn', 1),
(13, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(14, 'society_airlines', 'Airlines', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(1, 'steam:110000134857568', 'steam:110000132580eb0', 'society', 'society_police', 'Fine: Shooting a Civilian', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofbirth` varchar(255) NOT NULL,
  `sex` varchar(1) NOT NULL DEFAULT 'f',
  `height` varchar(128) NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `fire_rank` int(11) DEFAULT '-1',
  `staff_rank` int(11) DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `fire_rank`, `staff_rank`) VALUES
(4, 'steam:11000010a01bdb9', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coffees`
--

INSERT INTO `coffees` (`id`, `name`, `item`, `price`) VALUES
(1, 'Coffee', 'coffee', 30);

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(11, 'property', 'Property', 0),
(12, 'society_fire', 'fire', 1),
(14, 'society_unicorn', 'Unicorn', 1),
(15, 'society_avocat', 'Advokat', 1),
(16, 'society_journaliste', 'journaliste', 1),
(17, 'society_ambulance', 'Ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(139, 'society_police', NULL, '{}'),
(163, 'property', 'steam:11000010a01bdb9', '{\"dressing\":[]}'),
(164, 'property', 'steam:1100001068ef13c', '{\"dressing\":[{\"label\":\"idk\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":0,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"lipstick_1\":0,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":0,\"tshirt_1\":14,\"beard_4\":0,\"bags_2\":0,\"shoes_1\":3,\"lipstick_4\":0,\"makeup_4\":0,\"mask_1\":0,\"chain_1\":2,\"eyebrows_2\":8,\"eyebrows_4\":0,\"lipstick_2\":0,\"helmet_1\":-1,\"arms\":0,\"eyebrows_1\":4,\"tshirt_2\":0,\"beard_2\":3,\"sex\":1,\"makeup_3\":0,\"beard_3\":11,\"makeup_2\":0,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":8,\"shoes_2\":2,\"decals_2\":0,\"face\":31,\"mask_2\":0,\"hair_color_1\":11,\"torso_1\":34,\"age_2\":0,\"bproof_2\":0,\"pants_1\":0,\"glasses_1\":4,\"hair_1\":19,\"ears_1\":1,\"bproof_1\":0}},{\"label\":\"Cop girl\",\"skin\":{\"beard_1\":11,\"hair_color_2\":2,\"chain_2\":1,\"torso_2\":1,\"skin\":0,\"glasses_2\":0,\"ears_2\":0,\"bags_1\":0,\"decals_1\":0,\"hair_color_1\":11,\"hair_2\":0,\"eyebrows_3\":11,\"makeup_1\":13,\"lipstick_1\":0,\"beard_2\":3,\"glasses_1\":5,\"shoes_1\":14,\"lipstick_4\":0,\"makeup_3\":6,\"tshirt_1\":152,\"mask_1\":0,\"eyebrows_2\":8,\"eyebrows_4\":0,\"chain_1\":2,\"helmet_1\":-1,\"arms\":14,\"makeup_4\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"sex\":1,\"eyebrows_1\":4,\"beard_3\":11,\"makeup_2\":9,\"lipstick_3\":0,\"age_1\":0,\"helmet_2\":0,\"pants_2\":0,\"shoes_2\":15,\"bproof_2\":9,\"face\":31,\"mask_2\":0,\"age_2\":0,\"torso_1\":73,\"decals_2\":0,\"beard_4\":0,\"pants_1\":3,\"bags_2\":0,\"hair_1\":17,\"ears_1\":1,\"bproof_1\":27}}]}'),
(165, 'property', 'steam:110000132580eb0', '{\"dressing\":[{\"skin\":{\"makeup_2\":0,\"arms\":0,\"eyebrows_3\":0,\"eyebrows_4\":0,\"torso_2\":0,\"glasses_2\":0,\"face\":0,\"hair_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"lipstick_3\":0,\"sex\":0,\"beard_3\":0,\"glasses_1\":0,\"bags_1\":0,\"ears_2\":0,\"beard_2\":0,\"chain_1\":0,\"skin\":0,\"makeup_4\":0,\"bproof_1\":0,\"lipstick_1\":0,\"pants_2\":7,\"tshirt_1\":0,\"mask_2\":0,\"ears_1\":-1,\"eyebrows_1\":0,\"lipstick_4\":0,\"shoes_2\":0,\"makeup_1\":0,\"shoes_1\":0,\"lipstick_2\":0,\"makeup_3\":0,\"decals_2\":0,\"helmet_2\":0,\"chain_2\":0,\"beard_1\":0,\"mask_1\":0,\"age_2\":0,\"eyebrows_2\":0,\"hair_1\":0,\"hair_color_2\":0,\"bproof_2\":0,\"torso_1\":0,\"decals_1\":0,\"hair_color_1\":0,\"beard_4\":0,\"pants_1\":9,\"tshirt_2\":0,\"age_1\":0},\"label\":\"test\"}]}'),
(166, 'society_fire', NULL, '{}'),
(167, 'property', 'steam:110000112f07694', '{}'),
(168, 'property', 'steam:1100001042b46e1', '{}'),
(169, 'property', 'steam:11000011820eeac', '{}'),
(170, 'property', 'steam:11000011258948d', '{}'),
(171, 'society_unicorn', NULL, '{}'),
(172, 'society_avocat', NULL, '{}'),
(173, 'property', 'steam:110000114590956', '{}'),
(174, 'property', 'steam:110000115c5ddc9', '{}'),
(175, 'society_journaliste', NULL, '{}'),
(176, 'property', 'steam:110000109660ef9', '{}'),
(177, 'property', 'steam:11000010c6acdce', '{}'),
(178, 'property', 'steam:110000106a5790d', '{}'),
(179, 'property', 'steam:11000010b0a1bc3', '{}'),
(180, 'property', 'steam:11000011b78df95', '{}'),
(181, 'property', 'steam:110000135c88cca', '{}'),
(182, 'property', 'steam:11000010473daf1', '{}'),
(183, 'property', 'steam:110000103b294e0', '{}'),
(184, 'property', 'steam:110000104e0ed21', '{}'),
(185, 'property', 'steam:1100001238ac476', '{}'),
(186, 'property', 'steam:11000011742efb1', '{}'),
(187, 'society_ambulance', NULL, '{}'),
(188, 'property', 'steam:110000112969e8f', '{}'),
(189, 'property', 'steam:110000134857568', '{}'),
(190, 'property', 'steam:110000131f0d83c', '{}'),
(191, 'property', 'steam:110000110931da8', '{}'),
(192, 'property', 'steam:1100001185894b3', '{}'),
(193, 'property', 'steam:11000011a15fea8', '{}'),
(194, 'property', 'steam:1100001183ded9e', '{}'),
(195, 'property', 'steam:110000115815ad6', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Excessive Horn Use', 125, 0),
(2, 'Crossing Lines', 150, 0),
(3, 'Cont', 250, 0),
(4, 'Illegal U-Turn', 250, 0),
(5, 'Illegal Offroading', 375, 0),
(6, 'Illegal Safety Distance', 125, 0),
(7, 'Dangerous / forbidden stop', 450, 0),
(8, 'Illegal Parking', 110, 0),
(9, 'Not respecting the priority on the right', 225, 0),
(10, 'Non-compliance with a priority vehicle', 225, 0),
(11, 'Failure to stop', 350, 0),
(12, 'Failure to comply with a red light', 225, 0),
(13, 'Dangerous Driving', 1000, 0),
(14, 'Vehicle out of state', 100, 0),
(15, 'Driving without a license', 1500, 0),
(16, 'Hit and run', 1125, 0),
(17, 'Speeding <5 km / h', 135, 0),
(18, 'Speeding 5-15 kmh', 175, 0),
(19, 'Speeding 15-30 kmh', 375, 0),
(20, 'Speeding > 30 kmh', 1250, 0),
(21, 'Traffic obstruction', 110, 1),
(22, 'Degradation of the public road', 90, 1),
(23, 'Trouble with public order', 90, 1),
(24, 'Obstructing police operation', 225, 1),
(25, 'Insulting civilians', 135, 1),
(26, 'Insulting a police officer', 550, 1),
(27, 'Verbal threat or intimidation towards civil', 750, 1),
(28, 'Verbal threat or intimidation of a police officer', 1000, 1),
(29, 'Illegal protest', 250, 1),
(30, 'Attempted bribery', 1500, 1),
(31, 'Weapon out in town', 375, 2),
(32, 'Flashing a lethal wepon', 300, 2),
(33, 'Have a gun with no permit', 600, 2),
(34, 'Porting illegal wepons', 700, 2),
(35, 'Using a lockpickk', 300, 2),
(36, 'Car Theft', 3500, 2),
(37, 'Sale OF Drugs', 1500, 2),
(38, 'Drug Manifacturing', 1500, 2),
(39, 'Possetion Of Drugs', 650, 2),
(40, 'Civil Despute', 1500, 2),
(41, 'Takeover agent of the state', 2000, 2),
(42, 'Special deflection', 650, 2),
(43, 'Robbing Store', 4500, 2),
(44, 'Attempt to rob', 1500, 2),
(45, 'Shooting a Civilian', 2000, 3),
(46, 'Shooting a State Agent', 7500, 3),
(47, 'Attempt to Kill CIV', 3000, 3),
(48, 'Attempted murder of LEO', 5000, 3),
(49, 'Murder on civilian', 10000, 3),
(50, 'Murder on STate Agent', 30000, 3),
(51, 'Murder involuntarily', 1800, 3),
(52, 'Business scam', 2000, 2),
(53, 'After Market Parts', 1000, 0),
(54, 'DWI', 3500, 0),
(55, 'avoid and allude ', 4500, 3);

-- --------------------------------------------------------

--
-- Table structure for table `interiors`
--

CREATE TABLE `interiors` (
  `id` int(11) NOT NULL COMMENT 'key id',
  `enter` text NOT NULL COMMENT 'enter coords',
  `exit` text NOT NULL COMMENT 'destination coords',
  `iname` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `interiors`
--

INSERT INTO `interiors` (`id`, `enter`, `exit`, `iname`) VALUES
(1, '{-1045.888,-2751.017,21.3634,321.7075}', '{-1055.37,-2698.47,13.82,234.62}', 'first int');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'weed', 'Weed', -1, 0, 1),
(2, 'weed_pooch', 'Pochon de weed', -1, 0, 1),
(3, 'coke', 'Coke', -1, 0, 1),
(4, 'coke_pooch', 'Pochon de coke', -1, 0, 1),
(5, 'meth', 'Meth', -1, 0, 1),
(6, 'meth_pooch', 'Pochon de meth', -1, 0, 1),
(7, 'opium', 'Opium', -1, 0, 1),
(8, 'opium_pooch', 'Pochon de opium', -1, 0, 1),
(9, 'alive_chicken', 'Poulet vivant', -1, 0, 1),
(10, 'slaughtered_chicken', 'Poulet abattu', -1, 0, 1),
(11, 'packaged_chicken', 'Poulet en barquette', -1, 0, 1),
(12, 'fish', 'Poisson', -1, 0, 1),
(13, 'stone', 'Pierre', -1, 0, 1),
(14, 'washed_stone', 'Pierre Lavée', -1, 0, 1),
(15, 'copper', 'Cuivre', -1, 0, 1),
(16, 'iron', 'Fer', -1, 0, 1),
(17, 'gold', 'Or', -1, 0, 1),
(18, 'diamond', 'Diamant', -1, 0, 1),
(19, 'wood', 'Bois', -1, 0, 1),
(20, 'cutted_wood', 'Bois coupé', -1, 0, 1),
(21, 'packaged_plank', 'Paquet de planches', -1, 0, 1),
(22, 'petrol', 'Pétrole', -1, 0, 1),
(23, 'petrol_raffin', 'Pétrole Raffiné', -1, 0, 1),
(24, 'essence', 'Essence', -1, 0, 1),
(25, 'whool', 'Laine', -1, 0, 1),
(26, 'fabric', 'Tissu', -1, 0, 1),
(27, 'clothe', 'Vêtement', -1, 0, 1),
(28, 'bread', 'Bread', -1, 0, 1),
(29, 'water', 'Water', -1, 0, 1),
(30, 'gazbottle', 'Gas', -1, 0, 1),
(31, 'fixtool', 'FixTool', -1, 0, 1),
(32, 'carotool', 'CarTool', -1, 0, 1),
(33, 'blowpipe', 'BlowPipe', -1, 0, 1),
(34, 'fixkit', 'FixKit', -1, 0, 1),
(35, 'carokit', 'Kit carosserie', -1, 0, 1),
(52, 'bandage', 'Bandage', 50, 0, 1),
(53, 'medikit', 'Medikit', 50, 0, 1),
(55, 'cola', 'Coke', 20, 0, 1),
(56, 'vegetables', 'Vegetables', 20, 0, 1),
(57, 'meat', 'Meat', 20, 0, 1),
(58, 'tacos', 'Tacos', 20, 0, 1),
(59, 'burger', 'Burger', 20, 0, 1),
(62, 'coffee', 'Cafe', 10, 1, 1),
(63, 'donut', 'Donut', 10, 1, 1),
(74, 'black_chip', 'Puce cryptée', 1, 0, 1),
(75, 'chocolate', 'Chocolate', -1, 0, 1),
(76, 'silent', 'Silenced', -1, 0, 1),
(77, 'flashlight', 'Flashlight', -1, 0, 1),
(78, 'grip', 'Grip', -1, 0, 1),
(79, 'nightvision_scope', 'Night Vision Scope', -1, 0, 1),
(80, 'thermal_scope', 'Thermal Vision Scope', -1, 0, 1),
(81, 'extended_magazine', 'Extended Magazine', -1, 0, 1),
(82, 'very_extended_magazine', 'Very Extended Magazine', -1, 0, 1),
(83, 'scope', 'Scope', -1, 0, 1),
(84, 'advanced_scope', 'Advanced Scope', -1, 0, 1),
(85, 'yusuf', 'Luxury Skin', -1, 0, 1),
(86, 'lowrider', 'Lowrider Skin', -1, 0, 1),
(87, 'incendiary', 'Incendiary Bullets', -1, 0, 1),
(88, 'tracer_clip', 'Trackers Bullets', -1, 0, 1),
(89, 'hollow', 'Hollow Bullets', -1, 0, 1),
(90, 'fmj', 'Perforating Bullets', -1, 0, 1),
(91, 'lazer_scope', 'Lazer Scope', -1, 0, 1),
(92, 'compansator', 'Compensator', -1, 0, 1),
(93, 'barrel', 'Barrel', -1, 0, 1),
(100, 'drivers_license', 'Drivers License', -1, 0, 1),
(101, 'motorcycle_license', 'Motorcycle License', -1, 0, 1),
(102, 'commercial_license', 'Commerical Drivers License', -1, 0, 1),
(103, 'boating_license', 'Boating License', -1, 0, 1),
(104, 'taxi_license', 'Taxi License', -1, 0, 1),
(105, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1),
(106, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1),
(107, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1),
(108, 'hunting_license', 'Hunting License', -1, 0, 1),
(109, 'fishing_license', 'Fishing License', -1, 0, 1),
(110, 'diving_license', 'Diving License', -1, 0, 1),
(111, 'marrage_license', 'Marriage License', -1, 0, 1),
(112, 'pilot_license', 'Pilot License', -1, 0, 1),
(2010, 'armor', 'Bulletproof Vest', -1, 0, 1),
(2017, 'clip', 'Clip', -1, 0, 1),
(2018, 'binoculars', 'Binoculars', 1, 0, 1),
(2019, 'lsd', 'Lsd', -1, 0, 1),
(2020, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2021, 'lsd', 'Lsd', -1, 0, 1),
(2022, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(2023, 'jager', 'Jägermeister', 5, 0, 1),
(2024, 'vodka', 'Vodka', 5, 0, 1),
(2025, 'rhum', 'Rhum', 5, 0, 1),
(2026, 'whisky', 'Whisky', 5, 0, 1),
(2027, 'tequila', 'Tequila', 5, 0, 1),
(2028, 'martini', 'Martini blanc', 5, 0, 1),
(2029, 'soda', 'Soda', 5, 0, 1),
(2030, 'jusfruit', 'Jus de fruits', 5, 0, 1),
(2031, 'icetea', 'Ice Tea', 5, 0, 1),
(2032, 'energy', 'Energy Drink', 5, 0, 1),
(2033, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(2034, 'limonade', 'Limonade', 5, 0, 1),
(2035, 'bolcacahuetes', 'Bol de cacahuètes', 5, 0, 1),
(2036, 'bolnoixcajou', 'Bol de noix de cajou', 5, 0, 1),
(2037, 'bolpistache', 'Bol de pistaches', 5, 0, 1),
(2038, 'bolchips', 'Bol de chips', 5, 0, 1),
(2039, 'saucisson', 'Saucisson', 5, 0, 1),
(2040, 'grapperaisin', 'Grappe de raisin', 5, 0, 1),
(2041, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(2042, 'golem', 'Golem', 5, 0, 1),
(2043, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(2044, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(2045, 'vodkafruit', 'Vodka-jus de fruits', 5, 0, 1),
(2046, 'rhumfruit', 'Rhum-jus de fruits', 5, 0, 1),
(2047, 'teqpaf', 'Teq\'paf', 5, 0, 1),
(2048, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(2049, 'mojito', 'Mojito', 5, 0, 1),
(2050, 'ice', 'Glaçon', 5, 0, 1),
(2051, 'mixapero', 'Mix Apéritif', 3, 0, 1),
(2052, 'metreshooter', 'Mètre de shooter', 3, 0, 1),
(2053, 'jagercerbere', 'Jäger Cerbère', 3, 0, 1),
(2054, 'menthe', 'Feuille de menthe', 10, 0, 1),
(2055, 'nitro', 'Nitroso', -1, 0, 1),
(2056, 'fish', 'Fish', -1, 0, 1),
(2057, 'fishingrod', 'Fishing rod', 2, 0, 1),
(2058, 'bait', 'Bait', 20, 0, 1),
(2064, 'coffee', 'Café', -1, 0, 1),
(2069, 'plongee1', 'Plongee courte', -1, 0, 1),
(2070, 'plongee2', 'Plongee longue', -1, 0, 1),
(2073, 'contrat', 'Salvage', 15, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `J_Time` int(10) NOT NULL,
  `J_Cell` varchar(5) COLLATE utf8mb4_bin NOT NULL,
  `Jailer` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `Jailer_ID` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'unemployed', 0),
(5, 'realestateagent', 'Real Estate', 1),
(6, 'slaughterer', 'Butcher', 0),
(7, 'fisherman', 'Fisherman', 0),
(8, 'miner', 'Miner', 0),
(9, 'lumberjack', 'Lumberjack', 0),
(10, 'fuel', 'Refiner', 0),
(11, 'reporter', '9 News', 1),
(12, 'textil', 'Cloth Maker', 0),
(13, 'taxi', 'Taxi/Uber', 0),
(14, 'cardealer', 'Car Dealer', 1),
(15, 'banker', 'Banker', 1),
(20, 'security', 'Commercial City Security', 0),
(21, 'balla', 'balla', 1),
(22, 'family', 'family', 1),
(23, 'LOST MC', 'LOST MC', 1),
(24, 'vagos', 'vagos', 1),
(25, 'bus', 'BlueHound', 0),
(29, 'pizza', 'Pizza Sluts', 0),
(30, 'fire', 'Littleton Fire', 1),
(31, 'mecano', 'Mechanic', 1),
(32, 'brinks', 'Brinks', 0),
(33, 'coastguard', 'coastguard', 1),
(34, 'deliverer', 'Amazon', 0),
(36, 'gopostal', 'CSPS', 0),
(37, 'trucker', 'Trucker', 0),
(38, 'unicorn', 'Unicorn', 0),
(39, 'airlines', 'DIA', 0),
(40, 'banksecurity', 'banksecurity', 0),
(41, 'garbage', 'CC Sanitation', 0),
(42, 'foodtruck', 'Foodtruck', 1),
(43, 'avocat', 'Advokat', 0),
(44, 'police', 'LEO', 1),
(45, 'ambulance', 'AMR', 1),
(46, 'staff', 'Staff', 1),
(47, 'fork', 'Forklift', 1),
(50, 'journaliste', '9News', 0),
(51, 'Salvage', 'Salvage', 0),
(52, 'parking', 'Parking Enforcement', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'rsa', 'RSA', 100, '{}', '{}'),
(16, 'realestateagent', 0, 'location', 'Location', 350, '{}', '{}'),
(17, 'realestateagent', 1, 'vendeur', 'Seller', 550, '{}', '{}'),
(18, 'realestateagent', 2, 'gestion', 'Management', 40, '{}', '{}'),
(19, 'realestateagent', 3, 'boss', 'Boss', 0, '{}', '{}'),
(20, 'lumberjack', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(21, 'fisherman', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(22, 'fuel', 0, 'interim', 'Employee', 1000, '{}', '{}'),
(23, 'reporter', 0, 'employee', 'Employee', 1000, '{}', '{}'),
(24, 'textil', 0, 'interim', 'Employee', 1000, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(25, 'miner', 0, 'interim', 'Employee', 1000, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(26, 'slaughterer', 0, 'interim', 'Employee', 1000, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(27, 'taxi', 0, 'driver', 'Driver', 500, '{}', '{}'),
(32, 'cardealer', 0, 'recruit', 'Recruit', 750, '{}', '{}'),
(33, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
(34, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
(35, 'cardealer', 3, 'boss', 'Boss', 100, '{}', '{}'),
(36, 'banker', 0, 'advisor', 'Adviser', 350, '{}', '{}'),
(37, 'banker', 1, 'banker', 'Banker', 550, '{}', '{}'),
(38, 'banker', 2, 'business_banker', 'Business Banker', 750, '{}', '{}'),
(39, 'banker', 3, 'trader', 'Trader', 800, '{}', '{}'),
(40, 'banker', 4, 'boss', 'Boss', 1000, '{}', '{}'),
(54, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(55, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(63, 'security', 0, 'Basic noob', 'Basic Guard', 200, '{}', '{}'),
(64, 'balla', 0, 'balla', 'balla', 600, '{}', '{}'),
(65, 'family', 0, 'family', 'family', 600, '{}', '{}'),
(66, 'LOST MC', 0, 'LOST MC', 'LOST MC', 600, '{}', '{}'),
(67, 'vagos', 0, 'vagos', 'vagos', 600, '{}', '{}'),
(70, 'bus', 0, 'employee', 'Bus Driver', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(74, 'pizza', 0, 'employee', 'Pizza Bitch', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,jobs\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(78, 'fire', 3, 'boss', 'Commandant', 80, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":41,\"torso_2\":0,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"decals_1\":8,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"pants_2\":0,\"decals_2\":3,\"hair_color_2\":0,\"face\":21,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"hair_color_1\":10,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"shoes\":24,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(79, 'mecano', 0, 'recruit', 'Recruit', 12, '{}', '{}'),
(80, 'mecano', 1, 'novice', 'Novice', 24, '{}', '{}'),
(81, 'mecano', 2, 'experimente', 'Experimente', 36, '{}', '{}'),
(82, 'mecano', 3, 'chief', 'Chef d\'équipe', 48, '{}', '{}'),
(83, 'mecano', 4, 'boss', 'Patron', 0, '{}', '{}'),
(84, 'brinks', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(85, 'coastguard', 0, 'employee', 'Valores', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(86, 'deliverer', 0, 'employee', 'Employee', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(91, 'gopostal', 0, 'employee', 'Sedex', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(92, 'trucker', 0, 'employee', 'Employé', 200, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(93, 'unicorn', 0, 'barman', 'Barman', 300, '{}', '{}'),
(94, 'unicorn', 1, 'dancer', 'Danseur', 300, '{}', '{}'),
(95, 'unicorn', 2, 'viceboss', 'Co-gérant', 500, '{}', '{}'),
(96, 'unicorn', 3, 'boss', 'Gérant', 600, '{}', '{}'),
(97, 'airlines', 0, 'recrue', 'Recrue', 30, '{}', '{}'),
(98, 'airlines', 1, 'chauffeur', 'Chauffeur', 40, '{}', '{}'),
(99, 'airlines', 2, 'pilote', 'Pilote', 50, '{}', '{}'),
(100, 'airlines', 3, 'gerant', 'Gerant', 60, '{}', '{}'),
(101, 'airlines', 4, 'boss', 'Patron', 0, '{}', '{}'),
(102, 'banksecurity', 0, 'employee', 'banksecurity', 200, '{\"tshirt_1\":60,\"torso_1\":130,\"arms\":31,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}', '{\"tshirt_1\":60,\"torso_1\":0,\"arms\":68,\"pants_1\":25,\"glasses_1\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":0,\"shoes\":63,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":65}'),
(103, 'garbage', 0, 'employee', 'Employee', 750, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(104, 'foodtruck', 0, 'cook', 'Cook', 200, '{}', '{}'),
(105, 'foodtruck', 1, 'boss', 'Owner', 300, '{}', '{}'),
(106, 'avocat', 0, 'boss', 'Patron', 500, '', ''),
(107, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(108, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(109, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(110, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(111, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(112, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(113, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(114, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(115, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(116, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(117, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(118, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(119, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(120, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(121, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(122, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(123, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(124, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(125, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(126, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(127, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(128, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(129, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(130, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(131, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(132, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(133, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(134, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(135, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(136, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(137, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(138, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(139, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(140, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(141, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(142, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(143, 'police', 36, 'boss', 'Deputy Commissioner', 2000, '{}', '{}'),
(144, 'police', 37, 'boss', 'Commissioner', 2000, '{}', '{}'),
(145, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(146, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(147, 'fire', 0, 'cadet', 'Cadet', 100, '{\"tshirt_1\":57,\"torso_1\":55,\"arms\":0,\"pants_1\":35,\"glasses\":0,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":5,\"face\":19,\"glasses_2\":1,\"torso_2\":0,\"shoes\":24,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":8}', '{\"tshirt_1\":34,\"torso_1\":48,\"shoes\":24,\"pants_1\":34,\"torso_2\":0,\"decals_2\":0,\"hair_color_2\":0,\"glasses\":0,\"helmet_2\":0,\"hair_2\":3,\"face\":21,\"decals_1\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"arms\":14,\"hair_color_1\":10,\"tshirt_2\":0,\"helmet_1\":57}'),
(148, 'fire', 1, 'probationary', 'Probationary', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"arms\":0,\"decals_1\":8,\"torso_2\":0,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"hair_color_1\":5,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":14,\"pants_1\":34,\"pants_2\":0,\"decals_2\":1,\"hair_color_2\":0,\"shoes\":24,\"helmet_2\":0,\"hair_2\":3,\"decals_1\":7,\"torso_2\":0,\"face\":21,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"glasses_2\":1,\"hair_color_1\":10,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(159, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(160, 'fire', 3, 'firefighter', 'Firefighter', 200, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(161, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(162, 'fire', 5, 'captain', 'Captain', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(163, 'fire', 6, 'sharkone', 'Shark One', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(164, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(165, 'fire', 8, 'fto', 'FTO', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(166, 'fire', 9, 'hr', 'HR', 300, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(167, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(168, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(169, 'fire', 12, 'boss', 'Fire Chief', 2000, '{\"tshirt_1\":58,\"torso_1\":55,\"shoes\":24,\"pants_1\":35,\"pants_2\":0,\"decals_2\":2,\"hair_color_2\":0,\"face\":19,\"helmet_2\":0,\"hair_2\":0,\"glasses\":0,\"decals_1\":8,\"hair_color_1\":5,\"hair_1\":2,\"skin\":34,\"sex\":0,\"glasses_1\":0,\"glasses_2\":1,\"torso_2\":0,\"arms\":41,\"tshirt_2\":0,\"helmet_1\":11}', '{\"tshirt_1\":35,\"torso_1\":48,\"arms\":44,\"pants_1\":34,\"hair_2\":3,\"decals_2\":2,\"hair_color_2\":0,\"hair_color_1\":10,\"helmet_2\":0,\"face\":21,\"shoes\":24,\"torso_2\":0,\"glasses_2\":1,\"hair_1\":11,\"skin\":34,\"sex\":1,\"glasses_1\":5,\"pants_2\":0,\"decals_1\":7,\"glasses\":0,\"tshirt_2\":0,\"helmet_1\":57}'),
(170, 'ambulance', 0, 'cadet', 'Cadet', 100, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(171, 'ambulance', 1, 'probationary', 'Probationary', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(172, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(173, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(174, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(175, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(176, 'ambulance', 6, 'medone', 'Med One', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(178, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(179, 'ambulance', 8, 'fto', 'FTO', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(181, 'ambulance', 10, 'medicalchief', 'Medical Chief', 400, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(182, 'ambulance', 11, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(183, 'ambulance', 12, 'boss', 'Medical Director', 2000, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(184, 'ambulance', 13, 'boss', 'Medical Examiner', 0, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(185, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(186, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(187, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(188, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(189, 'fork', 0, 'employee', 'Operator', 20, '{\"tshirt_1\":59,\"torso_1\":89,\"arms\":31,\"pants_1\":36,\"glasses_1\":19,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":2,\"glasses_2\":0,\"torso_2\":1,\"shoes\":35,\"hair_1\":0,\"skin\":0,\"sex\":0,\"glasses_1\":19,\"pants_2\":0,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":5}', '{\"tshirt_1\":36,\"torso_1\":0,\"arms\":68,\"pants_1\":30,\"glasses_1\":15,\"decals_2\":0,\"hair_color_2\":0,\"helmet_2\":0,\"hair_color_1\":0,\"face\":27,\"glasses_2\":0,\"torso_2\":11,\"shoes\":26,\"hair_1\":5,\"skin\":0,\"sex\":1,\"glasses_1\":15,\"pants_2\":2,\"hair_2\":0,\"decals_1\":0,\"tshirt_2\":0,\"helmet_1\":19}'),
(192, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(193, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(194, 'journaliste', 2, 'investigator', 'Enquêteur', 400, '{}', '{}'),
(195, 'journaliste', 3, 'boss', 'Rédac\' chef', 450, '{}', '{}'),
(196, 'Salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(197, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(198, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(199, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(200, 'ambulance', 9, 'phrn', 'PreHospital Registered Nurse', 300, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Weapon license'),
(6, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('Char1:11000010a01bdb9', 'FMJ 423', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"FMJ 423\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":4.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'GSV 723', '{\"modArchCover\":-1,\"modLivery\":0,\"model\":353883353,\"modRearBumper\":-1,\"windowTint\":-1,\"dirtLevel\":1.0,\"modTransmission\":-1,\"color2\":0,\"modSpeakers\":-1,\"pearlescentColor\":112,\"health\":1000,\"modFrontWheels\":-1,\"modFender\":-1,\"modAirFilter\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modAerials\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"modExhaust\":-1,\"modSideSkirt\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"plate\":\"GSV 723\",\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modBrakes\":-1,\"modGrille\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"modHorns\":-1,\"modSeats\":-1,\"modDoorSpeaker\":-1,\"extras\":[],\"modEngineBlock\":-1,\"modAPlate\":-1,\"modXenon\":false,\"color1\":134,\"modPlateHolder\":-1,\"modOrnaments\":-1,\"modTrimA\":-1,\"modTrimB\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"modDial\":-1,\"modFrame\":-1,\"wheels\":0,\"modSmokeEnabled\":false,\"modVanityPlate\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDashboard\":-1,\"wheelColor\":156,\"modHood\":-1,\"modShifterLeavers\":-1,\"modEngine\":-1}', 'helicopter', 'police', 1),
('Char1:11000010a01bdb9', 'OLQ 222', '{\"pearlescentColor\":134,\"modRoof\":-1,\"modAirFilter\":-1,\"modSeats\":-1,\"modFender\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"modAerials\":-1,\"modHood\":-1,\"windowTint\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"modTank\":-1,\"dirtLevel\":1.0,\"color2\":134,\"modArchCover\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modDashboard\":-1,\"extras\":[],\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modArmor\":-1,\"modSideSkirt\":-1,\"modXenon\":false,\"wheelColor\":134,\"modSpeakers\":-1,\"color1\":134,\"modTurbo\":false,\"modFrontBumper\":-1,\"neonColor\":[255,0,255],\"modAPlate\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"modSuspension\":-1,\"model\":-576293386,\"modGrille\":-1,\"modPlateHolder\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"health\":1000,\"modWindows\":-1,\"plateIndex\":4,\"wheels\":3,\"modStruts\":-1,\"modDial\":-1,\"modTransmission\":-1,\"modTrimB\":-1,\"plate\":\"OLQ 222\",\"modShifterLeavers\":-1,\"modLivery\":7,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'QVU 418', '{\"health\":0,\"modXenon\":false,\"modSpoilers\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modExhaust\":-1,\"modBackWheels\":-1,\"modVanityPlate\":-1,\"modSuspension\":-1,\"modTank\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"extras\":[],\"modRightFender\":-1,\"modTrimB\":-1,\"wheels\":0,\"modShifterLeavers\":-1,\"neonColor\":[0,0,0],\"modTurbo\":false,\"color1\":0,\"modRoof\":-1,\"plateIndex\":-1,\"tyreSmokeColor\":[0,0,0],\"modHydrolic\":-1,\"modTransmission\":-1,\"modPlateHolder\":-1,\"modDashboard\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modSmokeEnabled\":false,\"plate\":\"QVU 418\",\"modTrunk\":-1,\"modOrnaments\":-1,\"modStruts\":-1,\"modAerials\":-1,\"model\":0,\"dirtLevel\":0.0,\"modHood\":-1,\"color2\":0,\"modFender\":-1,\"modRearBumper\":-1,\"pearlescentColor\":0,\"modAirFilter\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modHorns\":-1,\"modArchCover\":-1,\"modFrame\":-1,\"modFrontBumper\":-1,\"windowTint\":-1,\"wheelColor\":0,\"modEngine\":-1,\"modLivery\":-1,\"modEngineBlock\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modSeats\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false]}', 'car', 'ambulance', 1),
('Char1:11000010a01bdb9', 'TQP 485', '{\"modFender\":-1,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modGrille\":-1,\"modFrame\":-1,\"modXenon\":false,\"modArmor\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"modBackWheels\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"dirtLevel\":0.0,\"modSteeringWheel\":-1,\"modDial\":-1,\"model\":178383100,\"modEngine\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"health\":1000,\"extras\":[],\"modBrakes\":-1,\"modHydrolic\":-1,\"modLivery\":0,\"tyreSmokeColor\":[255,255,255],\"modSpeakers\":-1,\"modRoof\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":false,\"pearlescentColor\":134,\"modExhaust\":-1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modHood\":-1,\"modTrimA\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modHorns\":-1,\"color1\":111,\"modSideSkirt\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"color2\":1,\"modTurbo\":false,\"modTransmission\":-1,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"plate\":\"TQP 485\",\"modSuspension\":-1,\"modTank\":-1,\"wheels\":6,\"modRightFender\":-1,\"plateIndex\":4,\"modAPlate\":-1}', 'car', 'police', 0),
('Char1:11000010a01bdb9', 'UGA 061', '{\"modFender\":-1,\"windowTint\":-1,\"health\":1000,\"modAerials\":-1,\"modSpeakers\":-1,\"modShifterLeavers\":-1,\"model\":710198397,\"pearlescentColor\":5,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modBackWheels\":-1,\"modTrunk\":-1,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modFrontBumper\":-1,\"plateIndex\":4,\"modSpoilers\":-1,\"modAPlate\":-1,\"neonColor\":[255,0,255],\"plate\":\"UGA 061\",\"modDashboard\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modStruts\":-1,\"modGrille\":-1,\"modSeats\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modBrakes\":-1,\"modPlateHolder\":-1,\"modDial\":-1,\"modRoof\":-1,\"wheels\":0,\"dirtLevel\":3.0,\"modSuspension\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modTank\":-1,\"modFrontWheels\":-1,\"color2\":63,\"color1\":111,\"modLivery\":-1,\"modVanityPlate\":-1,\"modHorns\":-1,\"modXenon\":false,\"modRightFender\":-1,\"modTrimB\":-1,\"modSideSkirt\":-1,\"modWindows\":-1,\"modArchCover\":-1,\"modHydrolic\":-1,\"neonEnabled\":[false,false,false,false],\"modTrimA\":-1,\"extras\":[],\"modHood\":-1}', 'helicopter', 'ambulance', 1),
('Char1:11000010a01bdb9', 'UGU 071', '{\"modBackWheels\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"modAPlate\":-1,\"modTrimB\":-1,\"modFrame\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modRoof\":-1,\"color1\":111,\"health\":1000,\"modGrille\":-1,\"modAirFilter\":-1,\"modSpeakers\":-1,\"modSideSkirt\":-1,\"modArmor\":-1,\"modSpoilers\":-1,\"modFender\":-1,\"modTransmission\":-1,\"modSeats\":-1,\"modSuspension\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"plateIndex\":4,\"modArchCover\":-1,\"modFrontWheels\":-1,\"modEngine\":-1,\"wheelColor\":156,\"modWindows\":-1,\"dirtLevel\":11.0,\"modDashboard\":-1,\"neonEnabled\":[false,false,false,false],\"modVanityPlate\":-1,\"modXenon\":false,\"modShifterLeavers\":-1,\"modDial\":-1,\"modLivery\":0,\"modPlateHolder\":-1,\"modAerials\":-1,\"color2\":1,\"modFrontBumper\":-1,\"wheels\":6,\"extras\":[],\"pearlescentColor\":134,\"modBrakes\":-1,\"modStruts\":-1,\"modTrunk\":-1,\"modTank\":-1,\"modHorns\":-1,\"plate\":\"UGU 071\",\"model\":178383100,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"modExhaust\":-1,\"modHood\":-1}', 'car', 'police', 1),
('Char1:11000010a01bdb9', 'VFQ 240', '{\"modRightFender\":-1,\"modDial\":-1,\"plateIndex\":4,\"modAPlate\":-1,\"dirtLevel\":7.0,\"model\":-576293386,\"modWindows\":-1,\"modTrunk\":-1,\"modBrakes\":-1,\"modFender\":-1,\"neonColor\":[255,0,255],\"modArmor\":-1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"modTransmission\":-1,\"modEngine\":-1,\"modTrimA\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modHood\":-1,\"modExhaust\":-1,\"modRearBumper\":-1,\"modLivery\":4,\"modBackWheels\":-1,\"color1\":134,\"wheelColor\":134,\"modSeats\":-1,\"modRoof\":-1,\"modPlateHolder\":-1,\"modShifterLeavers\":-1,\"wheels\":3,\"modArchCover\":-1,\"modAerials\":-1,\"color2\":134,\"extras\":[],\"modEngineBlock\":-1,\"tyreSmokeColor\":[255,255,255],\"modHydrolic\":-1,\"modFrontBumper\":-1,\"health\":1000,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modSpoilers\":-1,\"modStruts\":-1,\"modSpeakers\":-1,\"modFrame\":-1,\"modTank\":-1,\"plate\":\"VFQ 240\",\"modAirFilter\":-1,\"modOrnaments\":-1,\"modGrille\":-1,\"modTurbo\":false,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":134,\"modSmokeEnabled\":false,\"modSideSkirt\":-1,\"modHorns\":-1,\"modDashboard\":-1,\"modXenon\":false,\"modDoorSpeaker\":-1}', 'car', 'ambulance', 1);

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `player_name` varchar(255) NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15),
(7, 'LTDgasoline', 'fixkit', 0),
(8, 'TwentyFourSeven', 'binoculars', 1000),
(9, 'RobsLiquor', 'binoculars', 1000),
(10, 'LTDgasoline', 'binoculars', 1000),
(11, 'LTDgasoline', 'plongee1', 250),
(12, 'RobsLiquor', 'plongee1', 250),
(13, 'TwentyFourSeven', 'plongee1', 250),
(14, 'LTDgasoline', 'plongee2', 350),
(15, 'RobsLiquor', 'plongee2', 350),
(16, 'TwentyFourSeven', 'plongee2', 350),
(17, 'LTDgasoline', 'chocolate', 0),
(18, 'PDStation', 'coffee', 1),
(19, 'PDStation', 'donut', 1),
(20, 'PDStation', 'clip', 1),
(21, 'PDStation', 'armor', 1),
(22, 'DMV', 'drivers_license', 80),
(23, 'DMV', 'motorcycle_license', 80),
(24, 'DMV', 'commercial_license', 175),
(25, 'DMV', 'boating_license', 40),
(26, 'DMV', 'taxi_license', 175),
(27, 'City_Hall', 'weapons_license1', 100),
(28, 'City_Hall', 'weapons_license2', 200),
(29, 'City_Hall', 'weapons_license3', 300),
(30, 'City_Hall', 'hunting_license', 100),
(31, 'City_Hall', 'fishing_license', 100),
(32, 'City_Hall', 'diving_license', 50),
(33, 'City_Hall', 'marrage_license', 5000),
(34, 'DMV', 'pilot_license', 500),
(35, 'PDStation', 'medikit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `society` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin,
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `ID` int(255) DEFAULT NULL,
  `steamid` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `rank` varchar(255) COLLATE utf8mb4_bin DEFAULT 'user',
  `is_dead` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `last_property`, `phone_number`, `status`, `ID`, `steamid`, `rank`, `is_dead`) VALUES
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', NULL, 'unemployed', 0, '[]', '{\"x\":-1348.8,\"y\":723.9,\"z\":186.4}', 200, 0, 'user', 'Tommie', 'Pickles', '1988-12-28', 'm', '200', NULL, NULL, NULL, NULL, NULL, 'user', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(138, 'steam:11000010a256910', 'black_money', 0),
(139, 'steam:110000117d5e2b7', 'black_money', 0),
(140, 'steam:1100001123cd531', 'black_money', 0),
(141, 'steam:1100001326230ef', 'black_money', 0),
(142, 'steam:11000010a266dc7', 'black_money', 0),
(143, 'steam:11000010e80a075', 'black_money', 0),
(144, 'steam:110000132b7eb1f', 'black_money', 0),
(145, 'steam:11000010b16012a', 'black_money', 0),
(146, 'steam:1100001184e4440', 'black_money', 0),
(147, 'steam:11000010f75c618', 'black_money', 0),
(148, 'steam:110000104ec6862', 'black_money', 0),
(149, 'steam:11000010c991cb2', 'black_money', 0),
(150, 'steam:11000010cd0b522', 'black_money', 0),
(151, 'steam:11000010b7982b5', 'black_money', 0),
(152, 'steam:11000010548afc9', 'black_money', 0),
(153, 'steam:1100001145029fc', 'black_money', 0),
(154, 'steam:110000116e20aa8', 'black_money', 0),
(155, 'steam:110000105318505', 'black_money', 0),
(156, 'steam:110000106def124', 'black_money', 0),
(157, 'steam:110000112206eef', 'black_money', 0),
(158, 'steam:110000106141cfc', 'black_money', 0),
(159, 'steam:110000119f838fa', 'black_money', 0),
(160, 'steam:110000107f1f2d6', 'black_money', 0),
(161, 'Char1:11000010a01bdb9', 'black_money', 49990000),
(162, 'steam:1100001068ef13c', 'black_money', 0),
(163, 'steam:110000132580eb0', 'black_money', 5000000),
(164, 'steam:110000112f07694', 'black_money', 0),
(165, 'steam:1100001042b46e1', 'black_money', 0),
(166, 'steam:11000011820eeac', 'black_money', 0),
(167, 'steam:11000011258948d', 'black_money', 0),
(168, 'steam:110000114590956', 'black_money', 0),
(169, 'steam:110000115c5ddc9', 'black_money', 0),
(170, 'steam:110000109660ef9', 'black_money', 0),
(171, 'steam:11000010c6acdce', 'black_money', 0),
(172, 'steam:110000106a5790d', 'black_money', 0),
(173, 'steam:11000010b0a1bc3', 'black_money', 0),
(174, 'steam:11000011b78df95', 'black_money', 0),
(175, 'steam:110000135c88cca', 'black_money', 0),
(176, 'steam:11000010473daf1', 'black_money', 0),
(177, 'steam:110000103b294e0', 'black_money', 0),
(178, 'steam:110000104e0ed21', 'black_money', 0),
(179, 'steam:1100001238ac476', 'black_money', 0),
(180, 'steam:11000011742efb1', 'black_money', 0),
(181, 'steam:110000112969e8f', 'black_money', 0),
(182, 'steam:110000134857568', 'black_money', 0),
(183, 'steam:110000131f0d83c', 'black_money', 0),
(184, 'steam:110000110931da8', 'black_money', 0),
(185, 'steam:1100001185894b3', 'black_money', 0),
(186, 'steam:11000011a15fea8', 'black_money', 0),
(187, 'steam:1100001183ded9e', 'black_money', 0),
(188, 'steam:1100001183ded9e', 'black_money', 0),
(189, 'steam:1100001183ded9e', 'black_money', 0),
(190, 'steam:1100001183ded9e', 'black_money', 0),
(191, 'steam:1100001183ded9e', 'black_money', 0),
(192, 'steam:1100001183ded9e', 'black_money', 0),
(193, 'steam:1100001183ded9e', 'black_money', 0),
(194, 'steam:1100001183ded9e', 'black_money', 0),
(195, 'steam:110000115815ad6', 'black_money', 0),
(196, 'steam:110000115815ad6', 'black_money', 0),
(197, 'steam:110000115815ad6', 'black_money', 0),
(198, 'steam:110000115815ad6', 'black_money', 0),
(199, 'steam:110000115815ad6', 'black_money', 0),
(200, 'steam:110000115815ad6', 'black_money', 0),
(201, 'steam:110000115815ad6', 'black_money', 0),
(202, 'steam:110000115815ad6', 'black_money', 0),
(203, 'Char2:11000010a01bdb9', 'black_money', 0),
(204, 'Char2:11000010a01bdb9', 'black_money', 0),
(205, 'Char2:11000010a01bdb9', 'black_money', 0),
(206, 'Char2:11000010a01bdb9', 'black_money', 0),
(207, 'Char2:11000010a01bdb9', 'black_money', 0),
(208, 'Char2:11000010a01bdb9', 'black_money', 0),
(209, 'Char2:11000010a01bdb9', 'black_money', 0),
(210, 'Char2:11000010a01bdb9', 'black_money', 0),
(211, 'steam:11000010a01bdb9', 'black_money', 0),
(212, 'steam:11000010a01bdb9', 'black_money', 0),
(213, 'steam:11000010a01bdb9', 'black_money', 0),
(214, 'steam:11000010a01bdb9', 'black_money', 0),
(215, 'steam:11000010a01bdb9', 'black_money', 0),
(216, 'steam:11000010a01bdb9', 'black_money', 0),
(217, 'steam:11000010a01bdb9', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_contacts`
--

INSERT INTO `user_contacts` (`id`, `identifier`, `name`, `number`) VALUES
(1, 'steam:110000112969e8f', 'K9', 84053),
(2, 'steam:110000132580eb0', 'J-bomb', 56216);

-- --------------------------------------------------------

--
-- Table structure for table `user_heli`
--

CREATE TABLE `user_heli` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `heli_name` varchar(255) DEFAULT NULL,
  `heli_model` varchar(255) DEFAULT NULL,
  `heli_price` int(60) DEFAULT NULL,
  `heli_plate` varchar(255) DEFAULT NULL,
  `heli_state` varchar(255) DEFAULT NULL,
  `heli_colorprimary` varchar(255) DEFAULT NULL,
  `heli_colorsecondary` varchar(255) DEFAULT NULL,
  `heli_pearlescentcolor` varchar(255) DEFAULT NULL,
  `heli_wheelcolor` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(2, 'steam:11000010a01bdb9', 'vodka', 0),
(3, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(4, 'steam:11000010a01bdb9', 'clothe', 0),
(5, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(6, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(7, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(8, 'steam:11000010a01bdb9', 'clothe', 0),
(9, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(10, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(11, 'steam:11000010a01bdb9', 'clothe', 0),
(12, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(13, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(14, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(15, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(16, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(17, 'steam:11000010a01bdb9', 'clothe', 0),
(18, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(19, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(20, 'steam:11000010a01bdb9', 'clothe', 0),
(21, 'steam:11000010a01bdb9', 'vodka', 0),
(22, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(23, 'steam:11000010a01bdb9', 'vodka', 0),
(24, 'steam:11000010a01bdb9', 'vodka', 0),
(25, 'steam:11000010a01bdb9', 'clothe', 0),
(26, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(27, 'steam:11000010a01bdb9', 'vodka', 0),
(28, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(29, 'steam:11000010a01bdb9', 'vodka', 0),
(30, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(31, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(32, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(33, 'steam:11000010a01bdb9', 'silent', 0),
(34, 'steam:11000010a01bdb9', 'jusfruit', 0),
(35, 'steam:11000010a01bdb9', 'coffee', 0),
(36, 'steam:11000010a01bdb9', 'silent', 0),
(37, 'steam:11000010a01bdb9', 'water', 0),
(38, 'steam:11000010a01bdb9', 'jusfruit', 0),
(39, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(40, 'steam:11000010a01bdb9', 'water', 0),
(41, 'steam:11000010a01bdb9', 'silent', 0),
(42, 'steam:11000010a01bdb9', 'water', 0),
(43, 'steam:11000010a01bdb9', 'silent', 0),
(44, 'steam:11000010a01bdb9', 'coffee', 0),
(45, 'steam:11000010a01bdb9', 'coffee', 0),
(46, 'steam:11000010a01bdb9', 'silent', 0),
(47, 'steam:11000010a01bdb9', 'coffee', 0),
(48, 'steam:11000010a01bdb9', 'silent', 0),
(49, 'steam:11000010a01bdb9', 'water', 0),
(50, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(51, 'steam:11000010a01bdb9', 'coffee', 0),
(52, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(53, 'steam:11000010a01bdb9', 'jusfruit', 0),
(54, 'steam:11000010a01bdb9', 'jusfruit', 0),
(55, 'steam:11000010a01bdb9', 'jusfruit', 0),
(56, 'steam:11000010a01bdb9', 'water', 0),
(57, 'steam:11000010a01bdb9', 'coffee', 0),
(58, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(59, 'steam:11000010a01bdb9', 'water', 0),
(60, 'steam:11000010a01bdb9', 'jusfruit', 0),
(61, 'steam:11000010a01bdb9', 'yusuf', 0),
(62, 'steam:11000010a01bdb9', 'binoculars', 0),
(63, 'steam:11000010a01bdb9', 'yusuf', 0),
(64, 'steam:11000010a01bdb9', 'yusuf', 0),
(65, 'steam:11000010a01bdb9', 'binoculars', 0),
(66, 'steam:11000010a01bdb9', 'yusuf', 0),
(67, 'steam:11000010a01bdb9', 'yusuf', 0),
(68, 'steam:11000010a01bdb9', 'lowrider', 0),
(69, 'steam:11000010a01bdb9', 'jusfruit', 0),
(70, 'steam:11000010a01bdb9', 'jusfruit', 0),
(71, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(72, 'steam:11000010a01bdb9', 'silent', 0),
(73, 'steam:11000010a01bdb9', 'coffee', 0),
(74, 'steam:11000010a01bdb9', 'water', 0),
(75, 'steam:11000010a01bdb9', 'water', 0),
(76, 'steam:11000010a01bdb9', 'yusuf', 0),
(77, 'steam:11000010a01bdb9', 'coffee', 0),
(78, 'steam:11000010a01bdb9', 'silent', 0),
(79, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(80, 'steam:11000010a01bdb9', 'lowrider', 0),
(81, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(82, 'steam:11000010a01bdb9', 'tequila', 0),
(83, 'steam:11000010a01bdb9', 'lowrider', 0),
(84, 'steam:11000010a01bdb9', 'carotool', 0),
(85, 'steam:11000010a01bdb9', 'binoculars', 0),
(86, 'steam:11000010a01bdb9', 'fish', 0),
(87, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(88, 'steam:11000010a01bdb9', 'binoculars', 0),
(89, 'steam:11000010a01bdb9', 'tequila', 0),
(90, 'steam:11000010a01bdb9', 'lowrider', 0),
(91, 'steam:11000010a01bdb9', 'binoculars', 0),
(92, 'steam:11000010a01bdb9', 'tequila', 0),
(93, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(94, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(95, 'steam:11000010a01bdb9', 'fish', 0),
(96, 'steam:11000010a01bdb9', 'fish', 0),
(97, 'steam:11000010a01bdb9', 'binoculars', 0),
(98, 'steam:11000010a01bdb9', 'binoculars', 0),
(99, 'steam:11000010a01bdb9', 'fish', 0),
(100, 'steam:11000010a01bdb9', 'fish', 0),
(101, 'steam:11000010a01bdb9', 'tequila', 0),
(102, 'steam:11000010a01bdb9', 'yusuf', 0),
(103, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(104, 'steam:11000010a01bdb9', 'lowrider', 0),
(105, 'steam:11000010a01bdb9', 'lowrider', 0),
(106, 'steam:11000010a01bdb9', 'yusuf', 0),
(107, 'steam:11000010a01bdb9', 'fish', 0),
(108, 'steam:11000010a01bdb9', 'tequila', 0),
(109, 'steam:11000010a01bdb9', 'tequila', 0),
(110, 'steam:11000010a01bdb9', 'fish', 0),
(111, 'steam:11000010a01bdb9', 'tequila', 0),
(112, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(113, 'steam:11000010a01bdb9', 'binoculars', 0),
(114, 'steam:11000010a01bdb9', 'lowrider', 0),
(115, 'steam:11000010a01bdb9', 'fish', 0),
(116, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(117, 'steam:11000010a01bdb9', 'lowrider', 0),
(118, 'steam:11000010a01bdb9', 'tequila', 0),
(119, 'steam:11000010a01bdb9', 'carotool', 0),
(120, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(121, 'steam:11000010a01bdb9', 'contrat', 0),
(122, 'steam:11000010a01bdb9', 'carotool', 0),
(123, 'steam:11000010a01bdb9', 'contrat', 0),
(124, 'steam:11000010a01bdb9', 'martini', 0),
(125, 'steam:11000010a01bdb9', 'carotool', 0),
(126, 'steam:11000010a01bdb9', 'contrat', 0),
(127, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(128, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(129, 'steam:11000010a01bdb9', 'contrat', 0),
(130, 'steam:11000010a01bdb9', 'carotool', 0),
(131, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(132, 'steam:11000010a01bdb9', 'menthe', 0),
(133, 'steam:11000010a01bdb9', 'martini', 0),
(134, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(135, 'steam:11000010a01bdb9', 'contrat', 0),
(136, 'steam:11000010a01bdb9', 'contrat', 0),
(137, 'steam:11000010a01bdb9', 'menthe', 0),
(138, 'steam:11000010a01bdb9', 'martini', 0),
(139, 'steam:11000010a01bdb9', 'nightvision_scope', 0),
(140, 'steam:11000010a01bdb9', 'menthe', 0),
(141, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(142, 'steam:11000010a01bdb9', 'martini', 0),
(143, 'steam:11000010a01bdb9', 'martini', 0),
(144, 'steam:11000010a01bdb9', 'tacos', 0),
(145, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(146, 'steam:11000010a01bdb9', 'contrat', 0),
(147, 'steam:11000010a01bdb9', 'menthe', 0),
(148, 'steam:11000010a01bdb9', 'flashlight', 0),
(149, 'steam:11000010a01bdb9', 'carotool', 0),
(150, 'steam:11000010a01bdb9', 'martini', 0),
(151, 'steam:11000010a01bdb9', 'menthe', 0),
(152, 'steam:11000010a01bdb9', 'menthe', 0),
(153, 'steam:11000010a01bdb9', 'carotool', 0),
(154, 'steam:11000010a01bdb9', 'carotool', 0),
(155, 'steam:11000010a01bdb9', 'contrat', 0),
(156, 'steam:11000010a01bdb9', 'martini', 0),
(157, 'steam:11000010a01bdb9', 'martini', 0),
(158, 'steam:11000010a01bdb9', 'tacos', 0),
(159, 'steam:11000010a01bdb9', 'tacos', 0),
(160, 'steam:11000010a01bdb9', 'menthe', 0),
(161, 'steam:11000010a01bdb9', 'bolpistache', 0),
(162, 'steam:11000010a01bdb9', 'tacos', 0),
(163, 'steam:11000010a01bdb9', 'flashlight', 0),
(164, 'steam:11000010a01bdb9', 'boating_license', 0),
(165, 'steam:11000010a01bdb9', 'flashlight', 0),
(166, 'steam:11000010a01bdb9', 'tacos', 0),
(167, 'steam:11000010a01bdb9', 'bolpistache', 0),
(168, 'steam:11000010a01bdb9', 'tacos', 0),
(169, 'steam:11000010a01bdb9', 'boating_license', 0),
(170, 'steam:11000010a01bdb9', 'bolpistache', 0),
(171, 'steam:11000010a01bdb9', 'bolpistache', 0),
(172, 'steam:11000010a01bdb9', 'menthe', 0),
(173, 'steam:11000010a01bdb9', 'flashlight', 0),
(174, 'steam:11000010a01bdb9', 'flashlight', 0),
(175, 'steam:11000010a01bdb9', 'clip', 0),
(176, 'steam:11000010a01bdb9', 'clip', 0),
(177, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(178, 'steam:11000010a01bdb9', 'clip', 0),
(179, 'steam:11000010a01bdb9', 'bolpistache', 0),
(180, 'steam:11000010a01bdb9', 'bolpistache', 0),
(181, 'steam:11000010a01bdb9', 'bolpistache', 0),
(182, 'steam:11000010a01bdb9', 'clip', 0),
(183, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(184, 'steam:11000010a01bdb9', 'clip', 0),
(185, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(186, 'steam:11000010a01bdb9', 'boating_license', 0),
(187, 'steam:11000010a01bdb9', 'boating_license', 0),
(188, 'steam:11000010a01bdb9', 'tacos', 0),
(189, 'steam:11000010a01bdb9', 'boating_license', 0),
(190, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(191, 'steam:11000010a01bdb9', 'boating_license', 0),
(192, 'steam:11000010a01bdb9', 'boating_license', 0),
(193, 'steam:11000010a01bdb9', 'flashlight', 0),
(194, 'steam:11000010a01bdb9', 'clip', 0),
(195, 'steam:11000010a01bdb9', 'bolpistache', 0),
(196, 'steam:11000010a01bdb9', 'tacos', 0),
(197, 'steam:11000010a01bdb9', 'flashlight', 0),
(198, 'steam:11000010a01bdb9', 'flashlight', 0),
(199, 'steam:11000010a01bdb9', 'clip', 0),
(200, 'steam:11000010a01bdb9', 'boating_license', 0),
(201, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(202, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(203, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(204, 'steam:11000010a01bdb9', 'cola', 0),
(205, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(206, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(207, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(208, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(209, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(210, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(211, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(212, 'steam:11000010a01bdb9', 'bait', 0),
(213, 'steam:11000010a01bdb9', 'cola', 0),
(214, 'steam:11000010a01bdb9', 'clip', 0),
(215, 'steam:11000010a01bdb9', 'cola', 0),
(216, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(217, 'steam:11000010a01bdb9', 'bait', 0),
(218, 'steam:11000010a01bdb9', 'cola', 0),
(219, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(220, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(221, 'steam:11000010a01bdb9', 'cola', 0),
(222, 'steam:11000010a01bdb9', 'bait', 0),
(223, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(224, 'steam:11000010a01bdb9', 'bait', 0),
(225, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(226, 'steam:11000010a01bdb9', 'weed', 0),
(227, 'steam:11000010a01bdb9', 'weed', 0),
(228, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(229, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(230, 'steam:11000010a01bdb9', 'cola', 0),
(231, 'steam:11000010a01bdb9', 'bait', 0),
(232, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(233, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(234, 'steam:11000010a01bdb9', 'fishingrod', 0),
(235, 'steam:11000010a01bdb9', 'fmj', 0),
(236, 'steam:11000010a01bdb9', 'weed', 0),
(237, 'steam:11000010a01bdb9', 'weed', 0),
(238, 'steam:11000010a01bdb9', 'bait', 0),
(239, 'steam:11000010a01bdb9', 'fmj', 0),
(240, 'steam:11000010a01bdb9', 'bait', 0),
(241, 'steam:11000010a01bdb9', 'thermal_scope', 0),
(242, 'steam:11000010a01bdb9', 'weed', 0),
(243, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(244, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(245, 'steam:11000010a01bdb9', 'bait', 0),
(246, 'steam:11000010a01bdb9', 'fishingrod', 0),
(247, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(248, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(249, 'steam:11000010a01bdb9', 'fishingrod', 0),
(250, 'steam:11000010a01bdb9', 'fishingrod', 0),
(251, 'steam:11000010a01bdb9', 'fmj', 0),
(252, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(253, 'steam:11000010a01bdb9', 'cola', 0),
(254, 'steam:11000010a01bdb9', 'weed', 0),
(255, 'steam:11000010a01bdb9', 'fmj', 0),
(256, 'steam:11000010a01bdb9', 'cola', 0),
(257, 'steam:11000010a01bdb9', 'fishingrod', 0),
(258, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(259, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(260, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(261, 'steam:11000010a01bdb9', 'fishingrod', 0),
(262, 'steam:11000010a01bdb9', 'fmj', 0),
(263, 'steam:11000010a01bdb9', 'weed', 0),
(264, 'steam:11000010a01bdb9', 'fmj', 0),
(265, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(266, 'steam:11000010a01bdb9', 'weed', 0),
(267, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(268, 'steam:11000010a01bdb9', 'fabric', 0),
(269, 'steam:11000010a01bdb9', 'fishingrod', 0),
(270, 'steam:11000010a01bdb9', 'fabric', 0),
(271, 'steam:11000010a01bdb9', 'fishingrod', 0),
(272, 'steam:11000010a01bdb9', 'plongee2', 0),
(273, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(274, 'steam:11000010a01bdb9', 'fmj', 0),
(275, 'steam:11000010a01bdb9', 'donut', 0),
(276, 'steam:11000010a01bdb9', 'incendiary', 0),
(277, 'steam:11000010a01bdb9', 'incendiary', 0),
(278, 'steam:11000010a01bdb9', 'fabric', 0),
(279, 'steam:11000010a01bdb9', 'incendiary', 0),
(280, 'steam:11000010a01bdb9', 'donut', 0),
(281, 'steam:11000010a01bdb9', 'incendiary', 0),
(282, 'steam:11000010a01bdb9', 'burger', 0),
(283, 'steam:11000010a01bdb9', 'donut', 0),
(284, 'steam:11000010a01bdb9', 'fabric', 0),
(285, 'steam:11000010a01bdb9', 'burger', 0),
(286, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(287, 'steam:11000010a01bdb9', 'fabric', 0),
(288, 'steam:11000010a01bdb9', 'fabric', 0),
(289, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(290, 'steam:11000010a01bdb9', 'fmj', 0),
(291, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(292, 'steam:11000010a01bdb9', 'burger', 0),
(293, 'steam:11000010a01bdb9', 'incendiary', 0),
(294, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(295, 'steam:11000010a01bdb9', 'plongee2', 0),
(296, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(297, 'steam:11000010a01bdb9', 'donut', 0),
(298, 'steam:11000010a01bdb9', 'burger', 0),
(299, 'steam:11000010a01bdb9', 'incendiary', 0),
(300, 'steam:11000010a01bdb9', 'fabric', 0),
(301, 'steam:11000010a01bdb9', 'plongee2', 0),
(302, 'steam:11000010a01bdb9', 'fabric', 0),
(303, 'steam:11000010a01bdb9', 'donut', 0),
(304, 'steam:11000010a01bdb9', 'donut', 0),
(305, 'steam:11000010a01bdb9', 'burger', 0),
(306, 'steam:11000010a01bdb9', 'incendiary', 0),
(307, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(308, 'steam:11000010a01bdb9', 'plongee2', 0),
(309, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(310, 'steam:11000010a01bdb9', 'burger', 0),
(311, 'steam:11000010a01bdb9', 'incendiary', 0),
(312, 'steam:11000010a01bdb9', 'plongee2', 0),
(313, 'steam:11000010a01bdb9', 'essence', 0),
(314, 'steam:11000010a01bdb9', 'iron', 0),
(315, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(316, 'steam:11000010a01bdb9', 'plongee1', 0),
(317, 'steam:11000010a01bdb9', 'plongee1', 0),
(318, 'steam:11000010a01bdb9', 'donut', 0),
(319, 'steam:11000010a01bdb9', 'mixapero', 0),
(320, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(321, 'steam:11000010a01bdb9', 'iron', 0),
(322, 'steam:11000010a01bdb9', 'burger', 0),
(323, 'steam:11000010a01bdb9', 'essence', 0),
(324, 'steam:11000010a01bdb9', 'burger', 0),
(325, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(326, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(327, 'steam:11000010a01bdb9', 'plongee1', 0),
(328, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(329, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(330, 'steam:11000010a01bdb9', 'plongee2', 0),
(331, 'steam:11000010a01bdb9', 'iron', 0),
(332, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(333, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(334, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(335, 'steam:11000010a01bdb9', 'donut', 0),
(336, 'steam:11000010a01bdb9', 'essence', 0),
(337, 'steam:11000010a01bdb9', 'essence', 0),
(338, 'steam:11000010a01bdb9', 'essence', 0),
(339, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(340, 'steam:11000010a01bdb9', 'mixapero', 0),
(341, 'steam:11000010a01bdb9', 'plongee2', 0),
(342, 'steam:11000010a01bdb9', 'iron', 0),
(343, 'steam:11000010a01bdb9', 'plongee1', 0),
(344, 'steam:11000010a01bdb9', 'essence', 0),
(345, 'steam:11000010a01bdb9', 'plongee2', 0),
(346, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(347, 'steam:11000010a01bdb9', 'plongee1', 0),
(348, 'steam:11000010a01bdb9', 'plongee1', 0),
(349, 'steam:11000010a01bdb9', 'jagercerbere', 0),
(350, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(351, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(352, 'steam:11000010a01bdb9', 'mixapero', 0),
(353, 'steam:11000010a01bdb9', 'mojito', 0),
(354, 'steam:11000010a01bdb9', 'diamond', 0),
(355, 'steam:11000010a01bdb9', 'drpepper', 0),
(356, 'steam:11000010a01bdb9', 'icetea', 0),
(357, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(358, 'steam:11000010a01bdb9', 'icetea', 0),
(359, 'steam:11000010a01bdb9', 'essence', 0),
(360, 'steam:11000010a01bdb9', 'mixapero', 0),
(361, 'steam:11000010a01bdb9', 'mojito', 0),
(362, 'steam:11000010a01bdb9', 'mixapero', 0),
(363, 'steam:11000010a01bdb9', 'essence', 0),
(364, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(365, 'steam:11000010a01bdb9', 'drpepper', 0),
(366, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(367, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(368, 'steam:11000010a01bdb9', 'mixapero', 0),
(369, 'steam:11000010a01bdb9', 'diamond', 0),
(370, 'steam:11000010a01bdb9', 'icetea', 0),
(371, 'steam:11000010a01bdb9', 'mixapero', 0),
(372, 'steam:11000010a01bdb9', 'mixapero', 0),
(373, 'steam:11000010a01bdb9', 'drpepper', 0),
(374, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(375, 'steam:11000010a01bdb9', 'iron', 0),
(376, 'steam:11000010a01bdb9', 'drpepper', 0),
(377, 'steam:11000010a01bdb9', 'drpepper', 0),
(378, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(379, 'steam:11000010a01bdb9', 'iron', 0),
(380, 'steam:11000010a01bdb9', 'plongee1', 0),
(381, 'steam:11000010a01bdb9', 'plongee1', 0),
(382, 'steam:11000010a01bdb9', 'iron', 0),
(383, 'steam:11000010a01bdb9', 'icetea', 0),
(384, 'steam:11000010a01bdb9', 'icetea', 0),
(385, 'steam:11000010a01bdb9', 'iron', 0),
(386, 'steam:11000010a01bdb9', 'drpepper', 0),
(387, 'steam:11000010a01bdb9', 'diamond', 0),
(388, 'steam:11000010a01bdb9', 'icetea', 0),
(389, 'steam:11000010a01bdb9', 'icetea', 0),
(390, 'steam:11000010a01bdb9', 'diamond', 0),
(391, 'steam:11000010a01bdb9', 'icetea', 0),
(392, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(393, 'steam:11000010a01bdb9', 'jager', 0),
(394, 'steam:11000010a01bdb9', 'jager', 0),
(395, 'steam:11000010a01bdb9', 'whool', 0),
(396, 'steam:11000010a01bdb9', 'jager', 0),
(397, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(398, 'steam:11000010a01bdb9', 'mojito', 0),
(399, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(400, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(401, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(402, 'steam:11000010a01bdb9', 'jager', 0),
(403, 'steam:11000010a01bdb9', 'mojito', 0),
(404, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(405, 'steam:11000010a01bdb9', 'mojito', 0),
(406, 'steam:11000010a01bdb9', 'whool', 0),
(407, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(408, 'steam:11000010a01bdb9', 'drpepper', 0),
(409, 'steam:11000010a01bdb9', 'mojito', 0),
(410, 'steam:11000010a01bdb9', 'mojito', 0),
(411, 'steam:11000010a01bdb9', 'whool', 0),
(412, 'steam:11000010a01bdb9', 'diamond', 0),
(413, 'steam:11000010a01bdb9', 'drpepper', 0),
(414, 'steam:11000010a01bdb9', 'whool', 0),
(415, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(416, 'steam:11000010a01bdb9', 'mojito', 0),
(417, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(418, 'steam:11000010a01bdb9', 'jager', 0),
(419, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(420, 'steam:11000010a01bdb9', 'diamond', 0),
(421, 'steam:11000010a01bdb9', 'diamond', 0),
(422, 'steam:11000010a01bdb9', 'whool', 0),
(423, 'steam:11000010a01bdb9', 'jager', 0),
(424, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(425, 'steam:11000010a01bdb9', 'diamond', 0),
(426, 'steam:11000010a01bdb9', 'tracer_clip', 0),
(427, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(428, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(429, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(430, 'steam:11000010a01bdb9', 'extended_magazine', 0),
(431, 'steam:11000010a01bdb9', 'whool', 0),
(432, 'steam:11000010a01bdb9', 'bread', 0),
(433, 'steam:11000010a01bdb9', 'coke', 0),
(434, 'steam:11000010a01bdb9', 'golem', 0),
(435, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(436, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(437, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(438, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(439, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(440, 'steam:11000010a01bdb9', 'whool', 0),
(441, 'steam:11000010a01bdb9', 'bread', 0),
(442, 'steam:11000010a01bdb9', 'coke', 0),
(443, 'steam:11000010a01bdb9', 'coke', 0),
(444, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(445, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(446, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(447, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(448, 'steam:11000010a01bdb9', 'bread', 0),
(449, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(450, 'steam:11000010a01bdb9', 'bread', 0),
(451, 'steam:11000010a01bdb9', 'coke', 0),
(452, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(453, 'steam:11000010a01bdb9', 'golem', 0),
(454, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(455, 'steam:11000010a01bdb9', 'coke', 0),
(456, 'steam:11000010a01bdb9', 'bread', 0),
(457, 'steam:11000010a01bdb9', 'whool', 0),
(458, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(459, 'steam:11000010a01bdb9', 'jager', 0),
(460, 'steam:11000010a01bdb9', 'jager', 0),
(461, 'steam:11000010a01bdb9', 'bread', 0),
(462, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(463, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(464, 'steam:11000010a01bdb9', 'bread', 0),
(465, 'steam:11000010a01bdb9', 'coke', 0),
(466, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(467, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(468, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(469, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(470, 'steam:11000010a01bdb9', 'bread', 0),
(471, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(472, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(473, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(474, 'steam:11000010a01bdb9', 'bolchips', 0),
(475, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(476, 'steam:11000010a01bdb9', 'whisky', 0),
(477, 'steam:11000010a01bdb9', 'golem', 0),
(478, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(479, 'steam:11000010a01bdb9', 'golem', 0),
(480, 'steam:11000010a01bdb9', 'bolchips', 0),
(481, 'steam:11000010a01bdb9', 'commercial_license', 0),
(482, 'steam:11000010a01bdb9', 'bolchips', 0),
(483, 'steam:11000010a01bdb9', 'golem', 0),
(484, 'steam:11000010a01bdb9', 'golem', 0),
(485, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(486, 'steam:11000010a01bdb9', 'bolchips', 0),
(487, 'steam:11000010a01bdb9', 'whisky', 0),
(488, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(489, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(490, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(491, 'steam:11000010a01bdb9', 'bolchips', 0),
(492, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(493, 'steam:11000010a01bdb9', 'whisky', 0),
(494, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(495, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(496, 'steam:11000010a01bdb9', 'bolchips', 0),
(497, 'steam:11000010a01bdb9', 'lsd', 0),
(498, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(499, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(500, 'steam:11000010a01bdb9', 'coke', 0),
(501, 'steam:11000010a01bdb9', 'lazer_scope', 0),
(502, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(503, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(504, 'steam:11000010a01bdb9', 'coke', 0),
(505, 'steam:11000010a01bdb9', 'golem', 0),
(506, 'steam:11000010a01bdb9', 'whisky', 0),
(507, 'steam:11000010a01bdb9', 'lsd', 0),
(508, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(509, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(510, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(511, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(512, 'steam:11000010a01bdb9', 'golem', 0),
(513, 'steam:11000010a01bdb9', 'energy', 0),
(514, 'steam:11000010a01bdb9', 'lsd', 0),
(515, 'steam:11000010a01bdb9', 'whisky', 0),
(516, 'steam:11000010a01bdb9', 'commercial_license', 0),
(517, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(518, 'steam:11000010a01bdb9', 'soda', 0),
(519, 'steam:11000010a01bdb9', 'energy', 0),
(520, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(521, 'steam:11000010a01bdb9', 'lsd', 0),
(522, 'steam:11000010a01bdb9', 'lsd', 0),
(523, 'steam:11000010a01bdb9', 'lsd', 0),
(524, 'steam:11000010a01bdb9', 'compansator', 0),
(525, 'steam:11000010a01bdb9', 'bolchips', 0),
(526, 'steam:11000010a01bdb9', 'commercial_license', 0),
(527, 'steam:11000010a01bdb9', 'commercial_license', 0),
(528, 'steam:11000010a01bdb9', 'soda', 0),
(529, 'steam:11000010a01bdb9', 'energy', 0),
(530, 'steam:11000010a01bdb9', 'compansator', 0),
(531, 'steam:11000010a01bdb9', 'whisky', 0),
(532, 'steam:11000010a01bdb9', 'commercial_license', 0),
(533, 'steam:11000010a01bdb9', 'whisky', 0),
(534, 'steam:11000010a01bdb9', 'compansator', 0),
(535, 'steam:11000010a01bdb9', 'compansator', 0),
(536, 'steam:11000010a01bdb9', 'soda', 0),
(537, 'steam:11000010a01bdb9', 'soda', 0),
(538, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(539, 'steam:11000010a01bdb9', 'bolchips', 0),
(540, 'steam:11000010a01bdb9', 'energy', 0),
(541, 'steam:11000010a01bdb9', 'commercial_license', 0),
(542, 'steam:11000010a01bdb9', 'ice', 0),
(543, 'steam:11000010a01bdb9', 'commercial_license', 0),
(544, 'steam:11000010a01bdb9', 'energy', 0),
(545, 'steam:11000010a01bdb9', 'whisky', 0),
(546, 'steam:11000010a01bdb9', 'compansator', 0),
(547, 'steam:11000010a01bdb9', 'lsd', 0),
(548, 'steam:11000010a01bdb9', 'energy', 0),
(549, 'steam:11000010a01bdb9', 'commercial_license', 0),
(550, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(551, 'steam:11000010a01bdb9', 'compansator', 0),
(552, 'steam:11000010a01bdb9', 'lsd', 0),
(553, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(554, 'steam:11000010a01bdb9', 'ice', 0),
(555, 'steam:11000010a01bdb9', 'soda', 0),
(556, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(557, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(558, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(559, 'steam:11000010a01bdb9', 'ice', 0),
(560, 'steam:11000010a01bdb9', 'ice', 0),
(561, 'steam:11000010a01bdb9', 'teqpaf', 0),
(562, 'steam:11000010a01bdb9', 'teqpaf', 0),
(563, 'steam:11000010a01bdb9', 'teqpaf', 0),
(564, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(565, 'steam:11000010a01bdb9', 'saucisson', 0),
(566, 'steam:11000010a01bdb9', 'wood', 0),
(567, 'steam:11000010a01bdb9', 'teqpaf', 0),
(568, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(569, 'steam:11000010a01bdb9', 'soda', 0),
(570, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(571, 'steam:11000010a01bdb9', 'wood', 0),
(572, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(573, 'steam:11000010a01bdb9', 'energy', 0),
(574, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(575, 'steam:11000010a01bdb9', 'wood', 0),
(576, 'steam:11000010a01bdb9', 'ice', 0),
(577, 'steam:11000010a01bdb9', 'ice', 0),
(578, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(579, 'steam:11000010a01bdb9', 'compansator', 0),
(580, 'steam:11000010a01bdb9', 'wood', 0),
(581, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(582, 'steam:11000010a01bdb9', 'energy', 0),
(583, 'steam:11000010a01bdb9', 'teqpaf', 0),
(584, 'steam:11000010a01bdb9', 'teqpaf', 0),
(585, 'steam:11000010a01bdb9', 'compansator', 0),
(586, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(587, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(588, 'steam:11000010a01bdb9', 'ice', 0),
(589, 'steam:11000010a01bdb9', 'soda', 0),
(590, 'steam:11000010a01bdb9', 'soda', 0),
(591, 'steam:11000010a01bdb9', 'ice', 0),
(592, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(593, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(594, 'steam:11000010a01bdb9', 'wood', 0),
(595, 'steam:11000010a01bdb9', 'saucisson', 0),
(596, 'steam:11000010a01bdb9', 'teqpaf', 0),
(597, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(598, 'steam:11000010a01bdb9', 'saucisson', 0),
(599, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(600, 'steam:11000010a01bdb9', 'vegetables', 0),
(601, 'steam:11000010a01bdb9', 'wood', 0),
(602, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(603, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(604, 'steam:11000010a01bdb9', 'wood', 0),
(605, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(606, 'steam:11000010a01bdb9', 'saucisson', 0),
(607, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(608, 'steam:11000010a01bdb9', 'pilot_license', 0),
(609, 'steam:11000010a01bdb9', 'meth', 0),
(610, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(611, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(612, 'steam:11000010a01bdb9', 'vegetables', 0),
(613, 'steam:11000010a01bdb9', 'vegetables', 0),
(614, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(615, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(616, 'steam:11000010a01bdb9', 'saucisson', 0),
(617, 'steam:11000010a01bdb9', 'meth', 0),
(618, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(619, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(620, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(621, 'steam:11000010a01bdb9', 'vegetables', 0),
(622, 'steam:11000010a01bdb9', 'teqpaf', 0),
(623, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(624, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(625, 'steam:11000010a01bdb9', 'advanced_scope', 0),
(626, 'steam:11000010a01bdb9', 'saucisson', 0),
(627, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(628, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(629, 'steam:11000010a01bdb9', 'wood', 0),
(630, 'steam:11000010a01bdb9', 'saucisson', 0),
(631, 'steam:11000010a01bdb9', 'saucisson', 0),
(632, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(633, 'steam:11000010a01bdb9', 'pilot_license', 0),
(634, 'steam:11000010a01bdb9', 'marrage_license', 0),
(635, 'steam:11000010a01bdb9', 'marrage_license', 0),
(636, 'steam:11000010a01bdb9', 'gazbottle', 0),
(637, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(638, 'steam:11000010a01bdb9', 'meth', 0),
(639, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(640, 'steam:11000010a01bdb9', 'hollow', 0),
(641, 'steam:11000010a01bdb9', 'pilot_license', 0),
(642, 'steam:11000010a01bdb9', 'gazbottle', 0),
(643, 'steam:11000010a01bdb9', 'meth', 0),
(644, 'steam:11000010a01bdb9', 'hollow', 0),
(645, 'steam:11000010a01bdb9', 'vegetables', 0),
(646, 'steam:11000010a01bdb9', 'marrage_license', 0),
(647, 'steam:11000010a01bdb9', 'gazbottle', 0),
(648, 'steam:11000010a01bdb9', 'vegetables', 0),
(649, 'steam:11000010a01bdb9', 'pilot_license', 0),
(650, 'steam:11000010a01bdb9', 'meth', 0),
(651, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(652, 'steam:11000010a01bdb9', 'diving_license', 0),
(653, 'steam:11000010a01bdb9', 'hollow', 0),
(654, 'steam:11000010a01bdb9', 'vegetables', 0),
(655, 'steam:11000010a01bdb9', 'vegetables', 0),
(656, 'steam:11000010a01bdb9', 'pilot_license', 0),
(657, 'steam:11000010a01bdb9', 'meth', 0),
(658, 'steam:11000010a01bdb9', 'meth', 0),
(659, 'steam:11000010a01bdb9', 'marrage_license', 0),
(660, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(661, 'steam:11000010a01bdb9', 'hollow', 0),
(662, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(663, 'steam:11000010a01bdb9', 'marrage_license', 0),
(664, 'steam:11000010a01bdb9', 'pilot_license', 0),
(665, 'steam:11000010a01bdb9', 'gazbottle', 0),
(666, 'steam:11000010a01bdb9', 'pilot_license', 0),
(667, 'steam:11000010a01bdb9', 'hollow', 0),
(668, 'steam:11000010a01bdb9', 'pilot_license', 0),
(669, 'steam:11000010a01bdb9', 'meth', 0),
(670, 'steam:11000010a01bdb9', 'hollow', 0),
(671, 'steam:11000010a01bdb9', 'marrage_license', 0),
(672, 'steam:11000010a01bdb9', 'diving_license', 0),
(673, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(674, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(675, 'steam:11000010a01bdb9', 'diving_license', 0),
(676, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(677, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(678, 'steam:11000010a01bdb9', 'gazbottle', 0),
(679, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(680, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(681, 'steam:11000010a01bdb9', 'scope', 0),
(682, 'steam:11000010a01bdb9', 'gazbottle', 0),
(683, 'steam:11000010a01bdb9', 'marrage_license', 0),
(684, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(685, 'steam:11000010a01bdb9', 'fishing_license', 0),
(686, 'steam:11000010a01bdb9', 'scope', 0),
(687, 'steam:11000010a01bdb9', 'gazbottle', 0),
(688, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(689, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(690, 'steam:11000010a01bdb9', 'diving_license', 0),
(691, 'steam:11000010a01bdb9', 'fishing_license', 0),
(692, 'steam:11000010a01bdb9', 'scope', 0),
(693, 'steam:11000010a01bdb9', 'hollow', 0),
(694, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(695, 'steam:11000010a01bdb9', 'fishing_license', 0),
(696, 'steam:11000010a01bdb9', 'diving_license', 0),
(697, 'steam:11000010a01bdb9', 'fishing_license', 0),
(698, 'steam:11000010a01bdb9', 'diving_license', 0),
(699, 'steam:11000010a01bdb9', 'scope', 0),
(700, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(701, 'steam:11000010a01bdb9', 'gazbottle', 0),
(702, 'steam:11000010a01bdb9', 'scope', 0),
(703, 'steam:11000010a01bdb9', 'diving_license', 0),
(704, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(705, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(706, 'steam:11000010a01bdb9', 'scope', 0),
(707, 'steam:11000010a01bdb9', 'hollow', 0),
(708, 'steam:11000010a01bdb9', 'marrage_license', 0),
(709, 'steam:11000010a01bdb9', 'diving_license', 0),
(710, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(711, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(712, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(713, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(714, 'steam:11000010a01bdb9', 'fixkit', 0),
(715, 'steam:11000010a01bdb9', 'washed_stone', 0),
(716, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(717, 'steam:11000010a01bdb9', 'petrol', 0),
(718, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(719, 'steam:11000010a01bdb9', 'taxi_license', 0),
(720, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(721, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(722, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(723, 'steam:11000010a01bdb9', 'fishing_license', 0),
(724, 'steam:11000010a01bdb9', 'fishing_license', 0),
(725, 'steam:11000010a01bdb9', 'fishing_license', 0),
(726, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(727, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(728, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(729, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(730, 'steam:11000010a01bdb9', 'fixkit', 0),
(731, 'steam:11000010a01bdb9', 'fishing_license', 0),
(732, 'steam:11000010a01bdb9', 'taxi_license', 0),
(733, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(734, 'steam:11000010a01bdb9', 'scope', 0),
(735, 'steam:11000010a01bdb9', 'taxi_license', 0),
(736, 'steam:11000010a01bdb9', 'taxi_license', 0),
(737, 'steam:11000010a01bdb9', 'barrel', 0),
(738, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(739, 'steam:11000010a01bdb9', 'barrel', 0),
(740, 'steam:11000010a01bdb9', 'washed_stone', 0),
(741, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(742, 'steam:11000010a01bdb9', 'washed_stone', 0),
(743, 'steam:11000010a01bdb9', 'fixkit', 0),
(744, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(745, 'steam:11000010a01bdb9', 'taxi_license', 0),
(746, 'steam:11000010a01bdb9', 'scope', 0),
(747, 'steam:11000010a01bdb9', 'washed_stone', 0),
(748, 'steam:11000010a01bdb9', 'washed_stone', 0),
(749, 'steam:11000010a01bdb9', 'fixkit', 0),
(750, 'steam:11000010a01bdb9', 'washed_stone', 0),
(751, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(752, 'steam:11000010a01bdb9', 'taxi_license', 0),
(753, 'steam:11000010a01bdb9', 'petrol', 0),
(754, 'steam:11000010a01bdb9', 'limonade', 0),
(755, 'steam:11000010a01bdb9', 'metreshooter', 0),
(756, 'steam:11000010a01bdb9', 'petrol', 0),
(757, 'steam:11000010a01bdb9', 'barrel', 0),
(758, 'steam:11000010a01bdb9', 'petrol', 0),
(759, 'steam:11000010a01bdb9', 'metreshooter', 0),
(760, 'steam:11000010a01bdb9', 'taxi_license', 0),
(761, 'steam:11000010a01bdb9', 'barrel', 0),
(762, 'steam:11000010a01bdb9', 'chocolate', 0),
(763, 'steam:11000010a01bdb9', 'fixkit', 0),
(764, 'steam:11000010a01bdb9', 'barrel', 0),
(765, 'steam:11000010a01bdb9', 'metreshooter', 0),
(766, 'steam:11000010a01bdb9', 'fixkit', 0),
(767, 'steam:11000010a01bdb9', 'petrol', 0),
(768, 'steam:11000010a01bdb9', 'limonade', 0),
(769, 'steam:11000010a01bdb9', 'washed_stone', 0),
(770, 'steam:11000010a01bdb9', 'fixkit', 0),
(771, 'steam:11000010a01bdb9', 'gold', 0),
(772, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(773, 'steam:11000010a01bdb9', 'washed_stone', 0),
(774, 'steam:11000010a01bdb9', 'gold', 0),
(775, 'steam:11000010a01bdb9', 'limonade', 0),
(776, 'steam:11000010a01bdb9', 'petrol', 0),
(777, 'steam:11000010a01bdb9', 'metreshooter', 0),
(778, 'steam:11000010a01bdb9', 'gold', 0),
(779, 'steam:11000010a01bdb9', 'metreshooter', 0),
(780, 'steam:11000010a01bdb9', 'barrel', 0),
(781, 'steam:11000010a01bdb9', 'barrel', 0),
(782, 'steam:11000010a01bdb9', 'chocolate', 0),
(783, 'steam:11000010a01bdb9', 'gold', 0),
(784, 'steam:11000010a01bdb9', 'limonade', 0),
(785, 'steam:11000010a01bdb9', 'limonade', 0),
(786, 'steam:11000010a01bdb9', 'limonade', 0),
(787, 'steam:11000010a01bdb9', 'metreshooter', 0),
(788, 'steam:11000010a01bdb9', 'barrel', 0),
(789, 'steam:11000010a01bdb9', 'taxi_license', 0),
(790, 'steam:11000010a01bdb9', 'petrol', 0),
(791, 'steam:11000010a01bdb9', 'fixkit', 0),
(792, 'steam:11000010a01bdb9', 'petrol', 0),
(793, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(794, 'steam:11000010a01bdb9', 'blowpipe', 0),
(795, 'steam:11000010a01bdb9', 'drivers_license', 0),
(796, 'steam:11000010a01bdb9', 'stone', 0),
(797, 'steam:11000010a01bdb9', 'opium', 0),
(798, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(799, 'steam:11000010a01bdb9', 'copper', 0),
(800, 'steam:11000010a01bdb9', 'chocolate', 0),
(801, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(802, 'steam:11000010a01bdb9', 'limonade', 0),
(803, 'steam:11000010a01bdb9', 'blowpipe', 0),
(804, 'steam:11000010a01bdb9', 'drivers_license', 0),
(805, 'steam:11000010a01bdb9', 'chocolate', 0),
(806, 'steam:11000010a01bdb9', 'blowpipe', 0),
(807, 'steam:11000010a01bdb9', 'gold', 0),
(808, 'steam:11000010a01bdb9', 'drivers_license', 0),
(809, 'steam:11000010a01bdb9', 'gold', 0),
(810, 'steam:11000010a01bdb9', 'stone', 0),
(811, 'steam:11000010a01bdb9', 'chocolate', 0),
(812, 'steam:11000010a01bdb9', 'blowpipe', 0),
(813, 'steam:11000010a01bdb9', 'metreshooter', 0),
(814, 'steam:11000010a01bdb9', 'limonade', 0),
(815, 'steam:11000010a01bdb9', 'blowpipe', 0),
(816, 'steam:11000010a01bdb9', 'drivers_license', 0),
(817, 'steam:11000010a01bdb9', 'metreshooter', 0),
(818, 'steam:11000010a01bdb9', 'chocolate', 0),
(819, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(820, 'steam:11000010a01bdb9', 'opium', 0),
(821, 'steam:11000010a01bdb9', 'stone', 0),
(822, 'steam:11000010a01bdb9', 'stone', 0),
(823, 'steam:11000010a01bdb9', 'gold', 0),
(824, 'steam:11000010a01bdb9', 'gold', 0),
(825, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(826, 'steam:11000010a01bdb9', 'stone', 0),
(827, 'steam:11000010a01bdb9', 'chocolate', 0),
(828, 'steam:11000010a01bdb9', 'chocolate', 0),
(829, 'steam:11000010a01bdb9', 'stone', 0),
(830, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(831, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(832, 'steam:11000010a01bdb9', 'blowpipe', 0),
(833, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(834, 'steam:11000010a01bdb9', 'copper', 0),
(835, 'steam:11000010a01bdb9', 'armor', 0),
(836, 'steam:11000010a01bdb9', 'copper', 0),
(837, 'steam:11000010a01bdb9', 'opium', 0),
(838, 'steam:11000010a01bdb9', 'grip', 0),
(839, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(840, 'steam:11000010a01bdb9', 'copper', 0),
(841, 'steam:11000010a01bdb9', 'armor', 0),
(842, 'steam:11000010a01bdb9', 'copper', 0),
(843, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(844, 'steam:11000010a01bdb9', 'opium', 0),
(845, 'steam:11000010a01bdb9', 'grip', 0),
(846, 'steam:11000010a01bdb9', 'grip', 0),
(847, 'steam:11000010a01bdb9', 'copper', 0),
(848, 'steam:11000010a01bdb9', 'stone', 0),
(849, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(850, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(851, 'steam:11000010a01bdb9', 'bandage', 0),
(852, 'steam:11000010a01bdb9', 'grip', 0),
(853, 'steam:11000010a01bdb9', 'blowpipe', 0),
(854, 'steam:11000010a01bdb9', 'copper', 0),
(855, 'steam:11000010a01bdb9', 'opium', 0),
(856, 'steam:11000010a01bdb9', 'grip', 0),
(857, 'steam:11000010a01bdb9', 'stone', 0),
(858, 'steam:11000010a01bdb9', 'opium', 0),
(859, 'steam:11000010a01bdb9', 'copper', 0),
(860, 'steam:11000010a01bdb9', 'drivers_license', 0),
(861, 'steam:11000010a01bdb9', 'drivers_license', 0),
(862, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(863, 'steam:11000010a01bdb9', 'drivers_license', 0),
(864, 'steam:11000010a01bdb9', 'rhum', 0),
(865, 'steam:11000010a01bdb9', 'rhum', 0),
(866, 'steam:11000010a01bdb9', 'drivers_license', 0),
(867, 'steam:11000010a01bdb9', 'armor', 0),
(868, 'steam:11000010a01bdb9', 'opium', 0),
(869, 'steam:11000010a01bdb9', 'armor', 0),
(870, 'steam:11000010a01bdb9', 'grip', 0),
(871, 'steam:11000010a01bdb9', 'blowpipe', 0),
(872, 'steam:11000010a01bdb9', 'opium', 0),
(873, 'steam:11000010a01bdb9', 'medikit', 0),
(874, 'steam:11000010a01bdb9', 'bandage', 0),
(875, 'steam:11000010a01bdb9', 'medikit', 0),
(876, 'steam:11000010a01bdb9', 'nitro', 0),
(877, 'steam:11000010a01bdb9', 'armor', 0),
(878, 'steam:11000010a01bdb9', 'bandage', 0),
(879, 'steam:11000010a01bdb9', 'rhum', 0),
(880, 'steam:11000010a01bdb9', 'grip', 0),
(881, 'steam:11000010a01bdb9', 'hunting_license', 0),
(882, 'steam:11000010a01bdb9', 'bandage', 0),
(883, 'steam:11000010a01bdb9', 'meat', 0),
(884, 'steam:11000010a01bdb9', 'rhum', 0),
(885, 'steam:11000010a01bdb9', 'medikit', 0),
(886, 'steam:11000010a01bdb9', 'rhum', 0),
(887, 'steam:11000010a01bdb9', 'armor', 0),
(888, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(889, 'steam:11000010a01bdb9', 'very_extended_magazine', 0),
(890, 'steam:11000010a01bdb9', 'armor', 0),
(891, 'steam:11000010a01bdb9', 'meat', 0),
(892, 'steam:11000010a01bdb9', 'rhum', 0),
(893, 'steam:11000010a01bdb9', 'carokit', 0),
(894, 'steam:11000010a01bdb9', 'rhum', 0),
(895, 'steam:11000010a01bdb9', 'grip', 0),
(896, 'steam:11000010a01bdb9', 'nitro', 0),
(897, 'steam:11000010a01bdb9', 'nitro', 0),
(898, 'steam:11000010a01bdb9', 'bandage', 0),
(899, 'steam:11000010a01bdb9', 'armor', 0),
(900, 'steam:11000010a01bdb9', 'nitro', 0),
(901, 'steam:11000010a01bdb9', 'carokit', 0),
(902, 'steam:11000010a01bdb9', 'bandage', 0),
(903, 'steam:11000010a01bdb9', 'fixtool', 0),
(904, 'steam:11000010a01bdb9', 'bandage', 0),
(905, 'steam:11000010a01bdb9', 'meat', 0),
(906, 'steam:11000010a01bdb9', 'rhum', 0),
(907, 'steam:11000010a01bdb9', 'hunting_license', 0),
(908, 'steam:11000010a01bdb9', 'medikit', 0),
(909, 'steam:11000010a01bdb9', 'nitro', 0),
(910, 'steam:11000010a01bdb9', 'medikit', 0),
(911, 'steam:11000010a01bdb9', 'bandage', 0),
(912, 'steam:11000010a01bdb9', 'meat', 0),
(913, 'steam:11000010a01bdb9', 'nitro', 0),
(914, 'steam:11000010a01bdb9', 'medikit', 0),
(915, 'steam:11000010a01bdb9', 'fixtool', 0),
(916, 'steam:11000010a01bdb9', 'meat', 0),
(917, 'steam:11000010a01bdb9', 'carokit', 0),
(918, 'steam:11000010a01bdb9', 'nitro', 0),
(919, 'steam:11000010a01bdb9', 'nitro', 0),
(920, 'steam:11000010a01bdb9', 'carokit', 0),
(921, 'steam:11000010a01bdb9', 'hunting_license', 0),
(922, 'steam:11000010a01bdb9', 'meat', 0),
(923, 'steam:11000010a01bdb9', 'fixtool', 0),
(924, 'steam:11000010a01bdb9', 'black_chip', 0),
(925, 'steam:11000010a01bdb9', 'meat', 0),
(926, 'steam:11000010a01bdb9', 'meat', 0),
(927, 'steam:11000010a01bdb9', 'medikit', 0),
(928, 'steam:11000010a01bdb9', 'black_chip', 0),
(929, 'steam:11000010a01bdb9', 'black_chip', 0),
(930, 'steam:11000010a01bdb9', 'hunting_license', 0),
(931, 'steam:11000010a01bdb9', 'fixtool', 0),
(932, 'steam:11000010a01bdb9', 'carokit', 0),
(933, 'steam:11000010a01bdb9', 'carokit', 0),
(934, 'steam:11000010a01bdb9', 'hunting_license', 0),
(935, 'steam:11000010a01bdb9', 'fixtool', 0),
(936, 'steam:11000010a01bdb9', 'hunting_license', 0),
(937, 'steam:11000010a01bdb9', 'black_chip', 0),
(938, 'steam:11000010a01bdb9', 'fixtool', 0),
(939, 'steam:11000010a01bdb9', 'hunting_license', 0),
(940, 'steam:11000010a01bdb9', 'hunting_license', 0),
(941, 'steam:11000010a01bdb9', 'carokit', 0),
(942, 'steam:11000010a01bdb9', 'carokit', 0),
(943, 'steam:11000010a01bdb9', 'black_chip', 0),
(944, 'steam:11000010a01bdb9', 'medikit', 0),
(945, 'steam:11000010a01bdb9', 'black_chip', 0),
(946, 'steam:11000010a01bdb9', 'black_chip', 0),
(947, 'steam:11000010a01bdb9', 'black_chip', 0),
(948, 'steam:11000010a01bdb9', 'fixtool', 0),
(949, 'steam:11000010a01bdb9', 'fixtool', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Adder', 'adder', 900000, 'super'),
('Akuma', 'AKUMA', 7500, 'motorcycles'),
('Alpha', 'alpha', 60000, 'sports'),
('Ardent', 'ardent', 1150000, 'sportsclassics'),
('Asea', 'asea', 5500, 'sedans'),
('Autarch', 'autarch', 1955000, 'super'),
('Avarus', 'avarus', 18000, 'motorcycles'),
('Bagger', 'bagger', 13500, 'motorcycles'),
('Baller', 'baller2', 40000, 'suvs'),
('Baller Sport', 'baller3', 60000, 'suvs'),
('Banshee', 'banshee', 70000, 'sports'),
('Banshee 900R', 'banshee2', 255000, 'super'),
('Bati 801', 'bati', 12000, 'motorcycles'),
('Bati 801RR', 'bati2', 19000, 'motorcycles'),
('Bestia GTS', 'bestiagts', 55000, 'sports'),
('BF400', 'bf400', 6500, 'motorcycles'),
('Bf Injection', 'bfinjection', 16000, 'offroad'),
('Bifta', 'bifta', 12000, 'offroad'),
('Bison', 'bison', 45000, 'vans'),
('Blade', 'blade', 15000, 'muscle'),
('Blazer', 'blazer', 6500, 'offroad'),
('Blazer Sport', 'blazer4', 8500, 'offroad'),
('blazer5', 'blazer5', 1755600, 'offroad'),
('Blista', 'blista', 8000, 'compacts'),
('BMX (velo)', 'bmx', 160, 'motorcycles'),
('Bobcat XL', 'bobcatxl', 32000, 'vans'),
('Brawler', 'brawler', 45000, 'offroad'),
('Brioso R/A', 'brioso', 18000, 'compacts'),
('Btype', 'btype', 62000, 'sportsclassics'),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
('Buccaneer', 'buccaneer', 18000, 'muscle'),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
('Buffalo', 'buffalo', 12000, 'sports'),
('Buffalo S', 'buffalo2', 20000, 'sports'),
('Bullet', 'bullet', 90000, 'super'),
('Burrito', 'burrito3', 19000, 'vans'),
('Camper', 'camper', 42000, 'vans'),
('Carbonizzare', 'carbonizzare', 75000, 'sports'),
('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
('Casco', 'casco', 30000, 'sportsclassics'),
('Cavalcade', 'cavalcade2', 55000, 'suvs'),
('Cheetah', 'cheetah', 375000, 'super'),
('Chimera', 'chimera', 38000, 'motorcycles'),
('Chino', 'chino', 15000, 'muscle'),
('Chino Luxe', 'chino2', 19000, 'muscle'),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
('Comet', 'comet2', 65000, 'sports'),
('Comet 5', 'comet5', 1145000, 'sports'),
('Contender', 'contender', 70000, 'suvs'),
('Coquette', 'coquette', 65000, 'sports'),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
('Cyclone', 'cyclone', 1890000, 'super'),
('Daemon', 'daemon', 11500, 'motorcycles'),
('Daemon High', 'daemon2', 13500, 'motorcycles'),
('Defiler', 'defiler', 9800, 'motorcycles'),
('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
('Dominator', 'dominator', 35000, 'muscle'),
('Double T', 'double', 28000, 'motorcycles'),
('Dubsta', 'dubsta', 45000, 'suvs'),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
('Dukes', 'dukes', 28000, 'muscle'),
('Dune Buggy', 'dune', 8000, 'offroad'),
('Elegy', 'elegy2', 38500, 'sports'),
('Emperor', 'emperor', 8500, 'sedans'),
('Enduro', 'enduro', 5500, 'motorcycles'),
('Entity XF', 'entityxf', 425000, 'super'),
('Esskey', 'esskey', 4200, 'motorcycles'),
('Exemplar', 'exemplar', 32000, 'coupes'),
('F620', 'f620', 40000, 'coupes'),
('Faction', 'faction', 20000, 'muscle'),
('Faction Rider', 'faction2', 30000, 'muscle'),
('Faction XL', 'faction3', 40000, 'muscle'),
('Faggio', 'faggio', 1900, 'motorcycles'),
('Vespa', 'faggio2', 2800, 'motorcycles'),
('Felon', 'felon', 42000, 'coupes'),
('Felon GT', 'felon2', 55000, 'coupes'),
('Feltzer', 'feltzer2', 55000, 'sports'),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
('Fixter (velo)', 'fixter', 225, 'motorcycles'),
('FMJ', 'fmj', 185000, 'super'),
('Fhantom', 'fq2', 17000, 'suvs'),
('Fugitive', 'fugitive', 12000, 'sedans'),
('Furore GT', 'furoregt', 45000, 'sports'),
('Fusilade', 'fusilade', 40000, 'sports'),
('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
('Gauntlet', 'gauntlet', 30000, 'muscle'),
('Gang Burrito', 'gburrito', 45000, 'vans'),
('Burrito', 'gburrito2', 29000, 'vans'),
('Glendale', 'glendale', 6500, 'sedans'),
('Grabger', 'granger', 50000, 'suvs'),
('Gresley', 'gresley', 47500, 'suvs'),
('GT 500', 'gt500', 785000, 'sportsclassics'),
('Guardian', 'guardian', 45000, 'offroad'),
('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
('Hermes', 'hermes', 535000, 'muscle'),
('Hexer', 'hexer', 12000, 'motorcycles'),
('Hotknife', 'hotknife', 125000, 'muscle'),
('Huntley S', 'huntley', 40000, 'suvs'),
('Hustler', 'hustler', 625000, 'muscle'),
('Infernus', 'infernus', 180000, 'super'),
('Innovation', 'innovation', 23500, 'motorcycles'),
('Intruder', 'intruder', 7500, 'sedans'),
('Issi', 'issi2', 10000, 'compacts'),
('Jackal', 'jackal', 38000, 'coupes'),
('Jester', 'jester', 65000, 'sports'),
('Jester(Racecar)', 'jester2', 135000, 'sports'),
('Journey', 'journey', 6500, 'vans'),
('Kamacho', 'kamacho', 345000, 'offroad'),
('Khamelion', 'khamelion', 38000, 'sports'),
('Kuruma', 'kuruma', 30000, 'sports'),
('Landstalker', 'landstalker', 35000, 'suvs'),
('RE-7B', 'le7b', 325000, 'super'),
('Lynx', 'lynx', 40000, 'sports'),
('Mamba', 'mamba', 70000, 'sports'),
('Manana', 'manana', 12800, 'sportsclassics'),
('Manchez', 'manchez', 5300, 'motorcycles'),
('Massacro', 'massacro', 65000, 'sports'),
('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
('Mesa', 'mesa', 16000, 'suvs'),
('Mesa Trail', 'mesa3', 40000, 'suvs'),
('Minivan', 'minivan', 13000, 'vans'),
('Monroe', 'monroe', 55000, 'sportsclassics'),
('The Liberator', 'monster', 210000, 'offroad'),
('Moonbeam', 'moonbeam', 18000, 'vans'),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
('Nemesis', 'nemesis', 5800, 'motorcycles'),
('Neon', 'neon', 1500000, 'sports'),
('Nightblade', 'nightblade', 35000, 'motorcycles'),
('Nightshade', 'nightshade', 65000, 'muscle'),
('9F', 'ninef', 65000, 'sports'),
('9F Cabrio', 'ninef2', 80000, 'sports'),
('Omnis', 'omnis', 35000, 'sports'),
('Oppressor', 'oppressor', 3524500, 'super'),
('Oracle XS', 'oracle2', 35000, 'coupes'),
('Osiris', 'osiris', 160000, 'super'),
('Panto', 'panto', 10000, 'compacts'),
('Paradise', 'paradise', 19000, 'vans'),
('Pariah', 'pariah', 1420000, 'sports'),
('Patriot', 'patriot', 55000, 'suvs'),
('PCJ-600', 'pcj', 6200, 'motorcycles'),
('Penumbra', 'penumbra', 28000, 'sports'),
('Pfister', 'pfister811', 85000, 'super'),
('Phoenix', 'phoenix', 12500, 'muscle'),
('Picador', 'picador', 18000, 'muscle'),
('Pigalle', 'pigalle', 20000, 'sportsclassics'),
('06 Tahoe', 'police8', 0, 'emergency'),
('Prairie', 'prairie', 12000, 'compacts'),
('Premier', 'premier', 8000, 'sedans'),
('Primo Custom', 'primo2', 14000, 'sedans'),
('X80 Proto', 'prototipo', 2500000, 'super'),
('Radius', 'radi', 29000, 'suvs'),
('raiden', 'raiden', 1375000, 'sports'),
('Rapid GT', 'rapidgt', 35000, 'sports'),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
('Reaper', 'reaper', 150000, 'super'),
('Rebel', 'rebel2', 35000, 'offroad'),
('Regina', 'regina', 5000, 'sedans'),
('Retinue', 'retinue', 615000, 'sportsclassics'),
('Revolter', 'revolter', 1610000, 'sports'),
('riata', 'riata', 380000, 'offroad'),
('Rocoto', 'rocoto', 45000, 'suvs'),
('Ruffian', 'ruffian', 6800, 'motorcycles'),
('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
('Rumpo', 'rumpo', 15000, 'vans'),
('Rumpo Trail', 'rumpo3', 19500, 'vans'),
('Sabre Turbo', 'sabregt', 20000, 'muscle'),
('Sabre GT', 'sabregt2', 25000, 'muscle'),
('Sanchez', 'sanchez', 5300, 'motorcycles'),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
('Sanctus', 'sanctus', 25000, 'motorcycles'),
('Sandking', 'sandking', 55000, 'offroad'),
('Savestra', 'savestra', 990000, 'sportsclassics'),
('SC 1', 'sc1', 1603000, 'super'),
('Schafter', 'schafter2', 25000, 'sedans'),
('Schafter V12', 'schafter3', 50000, 'sports'),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
('Seminole', 'seminole', 25000, 'suvs'),
('Sentinel', 'sentinel', 32000, 'coupes'),
('Sentinel XS', 'sentinel2', 40000, 'coupes'),
('Sentinel3', 'sentinel3', 650000, 'sports'),
('Seven 70', 'seven70', 39500, 'sports'),
('ETR1', 'sheava', 220000, 'super'),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
('Slam Van', 'slamvan3', 11500, 'muscle'),
('Sovereign', 'sovereign', 22000, 'motorcycles'),
('Stinger', 'stinger', 80000, 'sportsclassics'),
('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
('Streiter', 'streiter', 500000, 'sports'),
('Stretch', 'stretch', 90000, 'sedans'),
('Stromberg', 'stromberg', 3185350, 'sports'),
('Sultan', 'sultan', 15000, 'sports'),
('Sultan RS', 'sultanrs', 65000, 'super'),
('Super Diamond', 'superd', 130000, 'sedans'),
('Surano', 'surano', 50000, 'sports'),
('Surfer', 'surfer', 12000, 'vans'),
('T20', 't20', 300000, 'super'),
('Tailgater', 'tailgater', 30000, 'sedans'),
('Tampa', 'tampa', 16000, 'muscle'),
('Drift Tampa', 'tampa2', 80000, 'sports'),
('Thrust', 'thrust', 24000, 'motorcycles'),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
('Trophy Truck', 'trophytruck', 60000, 'offroad'),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
('Tropos', 'tropos', 40000, 'sports'),
('Turismo R', 'turismor', 350000, 'super'),
('Tyrus', 'tyrus', 600000, 'super'),
('Vacca', 'vacca', 120000, 'super'),
('Vader', 'vader', 7200, 'motorcycles'),
('Verlierer', 'verlierer2', 70000, 'sports'),
('Vigero', 'vigero', 12500, 'muscle'),
('Virgo', 'virgo', 14000, 'muscle'),
('Viseris', 'viseris', 875000, 'sportsclassics'),
('Visione', 'visione', 2250000, 'super'),
('Voltic', 'voltic', 90000, 'super'),
('Voltic 2', 'voltic2', 3830400, 'super'),
('Voodoo', 'voodoo', 7200, 'muscle'),
('Vortex', 'vortex', 9800, 'motorcycles'),
('Warrener', 'warrener', 4000, 'sedans'),
('Washington', 'washington', 9000, 'sedans'),
('Windsor', 'windsor', 95000, 'coupes'),
('Windsor Drop', 'windsor2', 125000, 'coupes'),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
('XLS', 'xls', 32000, 'suvs'),
('Yosemite', 'yosemite', 485000, 'muscle'),
('Youga', 'youga', 10800, 'vans'),
('Youga Luxuary', 'youga2', 14500, 'vans'),
('Z190', 'z190', 900000, 'sportsclassics'),
('Zentorno', 'zentorno', 1500000, 'super'),
('Zion', 'zion', 36000, 'coupes'),
('Zion Cabrio', 'zion2', 45000, 'coupes'),
('Zombie', 'zombiea', 9500, 'motorcycles'),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
('Z-Type', 'ztype', 220000, 'sportsclassics');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'Police'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `plate` varchar(50) NOT NULL,
  `soldby` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 300),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(11, 'GunShop', 'WEAPON_STUNGUN', 50),
(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
(25, 'GunShop', 'WEAPON_GRENADE', 500),
(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
(27, 'GunShop', 'WEAPON_BZGAS', 200),
(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(31, 'GunShop', 'WEAPON_BALL', 50),
(32, 'BlackWeashop', 'WEAPON_BALL', 50),
(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist`
--

CREATE TABLE `whitelist` (
  `identifier` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interiors`
--
ALTER TABLE `interiors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_heli`
--
ALTER TABLE `user_heli`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `heli_plate` (`heli_plate`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist`
--
ALTER TABLE `whitelist`
  ADD PRIMARY KEY (`identifier`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `interiors`
--
ALTER TABLE `interiors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'key id', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2074;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_heli`
--
ALTER TABLE `user_heli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=950;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
