-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2019 at 05:45 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `essentialmode`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1),
(33, 'society_admin', 'admin', 1),
(34, 'society_biker', 'Biker', 1),
(35, 'society_cardealer', 'Car Dealer', 1),
(36, 'society_biker', 'Biker', 1),
(37, 'society_gitrdone', 'GrD Construction', 1),
(38, 'society_parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_ambulance', 0, NULL),
(2, 'society_police', 9500, NULL),
(3, 'society_mecano', 0, NULL),
(4, 'society_taxi', 0, NULL),
(5, 'society_fire', 0, NULL),
(6, 'society_airlines', 0, NULL),
(7, 'society_mafia', 0, NULL),
(8, 'society_rebel', 0, NULL),
(9, 'society_unicorn', 0, NULL),
(10, 'society_dock', 0, NULL),
(11, 'society_avocat', 0, NULL),
(12, 'society_irish', 0, NULL),
(13, 'society_rodriguez', 0, NULL),
(14, 'society_bishops', 0, NULL),
(15, 'society_bountyhunter', 0, NULL),
(16, 'society_dismay', 0, NULL),
(17, 'society_grove', 0, NULL),
(18, 'society_foodtruck', 0, NULL),
(19, 'society_vagos', 0, NULL),
(20, 'society_ballas', 0, NULL),
(21, 'society_carthief', 0, NULL),
(22, 'society_realestateagent', 0, NULL),
(23, 'society_admin', 0, NULL),
(24, 'society_biker', 5000000, NULL),
(25, 'society_cardealer', 67595, NULL),
(26, 'caution', 0, 'steam:110000132580eb0'),
(27, 'property_black_money', 0, 'steam:110000132580eb0'),
(28, 'caution', 0, 'steam:11000010a01bdb9'),
(29, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(30, 'caution', 0, 'steam:11000010ddba29f'),
(31, 'property_black_money', 0, 'steam:11000010ddba29f'),
(32, 'property_black_money', 0, 'steam:110000112969e8f'),
(33, 'caution', 0, 'steam:110000112969e8f'),
(34, 'property_black_money', 0, 'steam:1100001068ef13c'),
(35, 'caution', 0, 'steam:1100001068ef13c'),
(36, 'caution', 0, 'steam:11000010c2ebf86'),
(37, 'property_black_money', 0, 'steam:11000010c2ebf86'),
(38, 'property_black_money', 0, 'steam:11000010a078bc7'),
(39, 'caution', 0, 'steam:11000010a078bc7'),
(40, 'society_gitrdone', 0, NULL),
(41, 'society_parking', 0, NULL),
(42, 'caution', 0, 'steam:11000013d51c2ff'),
(43, 'property_black_money', 0, 'steam:11000013d51c2ff'),
(44, 'caution', 0, 'steam:11000011a2fc3d0'),
(45, 'property_black_money', 0, 'steam:11000011a2fc3d0'),
(46, 'caution', 0, 'steam:1100001048af48f'),
(47, 'property_black_money', 0, 'steam:1100001048af48f'),
(48, 'caution', 0, 'steam:11000013bc17e0e'),
(49, 'property_black_money', 0, 'steam:11000013bc17e0e'),
(50, 'caution', 0, 'steam:1100001079cf4ab'),
(51, 'property_black_money', 0, 'steam:1100001079cf4ab'),
(52, 'caution', 0, 'steam:11000013b85ca5f'),
(53, 'property_black_money', 0, 'steam:11000013b85ca5f');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_mecano', 'Mechanic', 1),
(3, 'society_taxi', 'Taxi', 1),
(5, 'property', 'Property', 0),
(6, 'society_fire', 'fire', 1),
(7, 'society_airlines', 'Airlines', 1),
(8, 'society_mafia', 'Mafia', 1),
(9, 'society_citizen', 'Mafia', 1),
(10, 'society_rebel', 'Rebel', 1),
(11, 'society_unicorn', 'Unicorn', 1),
(12, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(13, 'society_unicorn', 'Unicorn', 1),
(14, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(15, 'society_dock', 'Marina', 1),
(16, 'society_avocat', 'Avocat', 1),
(17, 'society_irish', 'Irish', 1),
(18, 'society_rodriguez', 'Rodriguez', 1),
(19, 'society_avocat', 'Avocat', 1),
(20, 'society_bishops', 'Bishops', 1),
(21, 'society_irish', 'Irish', 1),
(22, 'society_bountyhunter', 'Bountyhunter', 1),
(23, 'society_dismay', 'Dismay', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_grove', 'Grove', 1),
(26, 'society_vagos', 'Vagos', 1),
(27, 'society_ballas', 'Ballas', 1),
(28, 'society_carthief', 'Car Thief', 1),
(29, 'society_admin', 'admin', 1),
(30, 'society_biker', 'Biker', 1),
(31, 'society_cardealer', 'Car Dealer', 1),
(32, 'society_biker', 'Biker', 1),
(33, 'society_gitrdone', 'GrD Construction', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `baninfo`
--

CREATE TABLE `baninfo` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin DEFAULT 'no info',
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT 'no info',
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT 'no info',
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT 'no info',
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT '0.0.0.0',
  `playername` varchar(32) COLLATE utf8mb4_bin DEFAULT 'no info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `baninfo`
--

INSERT INTO `baninfo` (`id`, `identifier`, `license`, `liveid`, `xblid`, `discord`, `playerip`, `playername`) VALUES
(1, 'steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 'live:844427762648155', 'xbl:2533274970162138', 'discord:316956589259096065', 'ip:108.39.247.113', 'stickybombz'),
(2, 'steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 'no info', 'no info', 'discord:416744619162730496', 'ip:174.23.169.31', 'K9Marine'),
(3, 'steam:110000112969e8f', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 'live:985157476597128', 'xbl:2535467589236859', 'discord:197754376645902338', 'ip:136.35.33.33', 'SuperSteve902'),
(4, 'steam:1100001048af48f', 'license:153aa3e5d9507effcabab9417a1d004ac511b461', 'no info', 'no info', 'no info', 'ip:50.107.157.70', 'VaderSanchez'),
(5, 'steam:11000013bc17e0e', 'license:ca6024211ff0ae80db75aae57447e9ae5424aa8b', 'live:844429205478254', 'xbl:2535412874003034', 'discord:556129511037730818', 'ip:2.61.119.240', 'Police Los Angeles'),
(6, 'steam:1100001068ef13c', 'license:a4979e4221783962685bb8a6105e2b93fc364e77', 'live:1055518641986243', 'xbl:2535405567256855', 'discord:372129901991559169', 'ip:173.72.175.244', 'Soft-Hearted Devil'),
(7, 'steam:1100001079cf4ab', 'license:a179630c24d11ca0c145594e3a475486473db24e', 'live:914798072622782', 'xbl:2535455193687852', 'no info', 'ip:66.24.225.175', 'yankee930'),
(8, 'steam:11000010a078bc7', 'license:00eb03a770b9661e961de7dd80810b9a2656c8cd', 'live:1688853852909340', 'no info', 'discord:313235445549105152', 'ip:76.102.73.195', '414 - Michael G.'),
(9, 'steam:11000010c2ebf86', 'license:58dec6dbc91fa2da51ac590892604b46a231610a', 'live:1899947113129119', 'xbl:2535417591160549', 'no info', 'ip:67.61.230.198', 'Noss'),
(10, 'steam:11000010ddba29f', 'license:50280a9855e0a77982745eecb274fb074ce3bd7e', 'live:844428293110174', 'xbl:2535466272047308', 'discord:196189496922865664', 'ip:73.130.28.22', 'EONeillYT'),
(11, 'steam:11000013b85ca5f', 'license:e4e17ebd082256997566b01a196bcd73c5d640d3', 'live:1055520843344299', 'xbl:2533274931796243', 'discord:338438567506214912', 'ip:68.97.1.114', 'Abortion Completus');

-- --------------------------------------------------------

--
-- Table structure for table `banlist`
--

CREATE TABLE `banlist` (
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `expiration` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `banlisthistory`
--

CREATE TABLE `banlisthistory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `liveid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `xblid` varchar(21) COLLATE utf8mb4_bin DEFAULT NULL,
  `discord` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL,
  `playerip` varchar(25) COLLATE utf8mb4_bin DEFAULT NULL,
  `targetplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `sourceplayername` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `timeat` int(11) NOT NULL,
  `added` varchar(40) COLLATE utf8mb4_bin NOT NULL,
  `expiration` int(11) NOT NULL,
  `permanent` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `banlisthistory`
--

INSERT INTO `banlisthistory` (`id`, `identifier`, `license`, `liveid`, `xblid`, `discord`, `playerip`, `targetplayername`, `sourceplayername`, `reason`, `timeat`, `added`, `expiration`, `permanent`) VALUES
(1, 'steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 'no info', 'no info', 'discord:416744619162730496', 'ip:174.23.169.31', 'K9Marine', 'stickybombz', 'testing', 1572762811, 'Sun Nov  3 01:33:31 2019', 1572849211, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `ban_issued` varchar(50) NOT NULL,
  `banned_until` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `Column 9` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bans`
--

INSERT INTO `bans` (`ID`, `name`, `identifier`, `reason`, `ban_issued`, `banned_until`, `staff_name`, `staff_steamid`, `Column 9`) VALUES
(1, 'reaper', 'steam:11000011a2fc3d0', 'potential hacker', '11/2/2019', '11/2/2029', 'stickybombz', 'steam:11000010a01bdb9', ''),
(2, 'stickybombz', 'steam:11000010a01bdb9', 'testing', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT -1,
  `leo_rank` int(11) DEFAULT -1,
  `tow_rank` int(11) DEFAULT -1,
  `admin_rank` int(11) DEFAULT -1,
  `biker_rank` int(11) DEFAULT -1,
  `offdutyleo_rank` int(11) DEFAULT -1,
  `offdutyems_rank` int(11) DEFAULT -1,
  `fire_rank` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `admin_rank`, `biker_rank`, `offdutyleo_rank`, `offdutyems_rank`, `fire_rank`) VALUES
('steam:11000010ddba29f', 'Eric', 'O\'neill', '06/28/1995', 'M', '72', -1, 11, -1, 1, -1, 11, -1, -1),
('steam:110000112969e8f', 'Steven', 'Super', '05/08/2003', 'M', '90', 3, -1, -1, -1, -1, -1, 3, -1),
('steam:1100001068ef13c', 'William', 'Woodard', '7/27/1989', 'M', '76', 10, -1, -1, 2, -1, -1, 10, -1),
('steam:11000010c2ebf86', 'Robert', 'Kerr', '10/23/1995', 'M', '6', -1, -1, -1, -1, -1, -1, -1, -1),
('steam:11000010a078bc7', 'Roy', 'Rogers', '07/04/2000', 'M', '64', 3, -1, -1, 1, -1, -1, 3, 12),
('steam:110000132580eb0', 'Jak', 'Fulton', '10/10/1988', 'M', '74', -1, -1, 36, 3, -1, 36, -1, -1),
('steam:11000010a01bdb9', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', 11, 37, -1, 3, -1, 37, 11, -1),
('steam:11000013bc17e0e', 'Zahar', 'Artonov', 'Asdadd', 'M', '50', -1, -1, -1, -1, -1, -1, -1, -1),
('steam:1100001079cf4ab', 'Kane', 'Brown', '9-3-06', 'M', '96', -1, -1, -1, -1, -1, -1, -1, -1),
('steam:11000013b85ca5f', 'Felipe', 'Gomez', '05/151995', 'M', '6', -1, -1, -1, -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commend`
--

CREATE TABLE `commend` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `community_name` varchar(50) NOT NULL,
  `discord_webhook` varchar(50) NOT NULL,
  `joinmessage` enum('T','F') NOT NULL,
  `chatcommands` enum('T','F') NOT NULL,
  `checktimeout` int(11) NOT NULL,
  `trustscore` int(11) NOT NULL,
  `tswarn` int(11) NOT NULL,
  `tskick` int(11) NOT NULL,
  `tsban` int(11) NOT NULL,
  `tscommend` int(11) NOT NULL,
  `tstime` int(11) NOT NULL,
  `recent_time` int(11) NOT NULL,
  `permissions` varchar(50) NOT NULL,
  `serveractions` varchar(50) NOT NULL,
  `debug` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'user_mask', 'Masque', 0),
(3, 'property', 'Property', 0),
(4, 'society_fire', 'fire', 1),
(5, 'society_mafia', 'Mafia', 1),
(7, 'society_rebel', 'Rebel', 1),
(8, 'society_unicorn', 'Unicorn', 1),
(9, 'society_unicorn', 'Unicorn', 1),
(10, 'society_avocat', 'Avocat', 1),
(11, 'society_irish', 'Irish', 1),
(12, 'society_rodriguez', 'Rodriguez', 1),
(13, 'society_avocat', 'Avocat', 1),
(14, 'society_bishops', 'Bishops', 1),
(15, 'society_irish', 'Irish', 1),
(16, 'society_bountyhunter', 'Bountyhunter', 1),
(17, 'society_dismay', 'Dismay', 1),
(18, 'society_bountyhunter', 'Bountyhunter', 1),
(19, 'society_grove', 'Grove', 1),
(20, 'society_vagos', 'Vagos', 1),
(21, 'society_ballas', 'Ballas', 1),
(22, 'society_carthief', 'Car Thief', 1),
(23, 'society_admin', 'admin', 1),
(24, 'society_biker', 'Biker', 1),
(25, 'society_biker', 'Biker', 1),
(26, 'society_gitrdone', 'GrD Construction', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_police', NULL, '{}'),
(2, 'society_fire', NULL, '{}'),
(3, 'society_mafia', NULL, '{}'),
(4, 'society_rebel', NULL, '{}'),
(5, 'society_unicorn', NULL, '{}'),
(6, 'society_avocat', NULL, '{}'),
(7, 'society_irish', NULL, '{}'),
(8, 'society_rodriguez', NULL, '{}'),
(9, 'society_bishops', NULL, '{}'),
(10, 'society_bountyhunter', NULL, '{}'),
(11, 'society_dismay', NULL, '{}'),
(12, 'society_grove', NULL, '{}'),
(13, 'society_vagos', NULL, '{}'),
(14, 'society_ballas', NULL, '{}'),
(15, 'society_carthief', NULL, '{}'),
(16, 'society_admin', NULL, '{}'),
(17, 'society_biker', NULL, '{}'),
(32, 'user_mask', 'steam:110000132580eb0', '{}'),
(33, 'property', 'steam:110000132580eb0', '{}'),
(34, 'user_mask', 'steam:110000107e03e6e', '{}'),
(35, 'property', 'steam:110000107e03e6e', '{}'),
(44, 'user_mask', 'steam:11000011a9b2c0c', '{}'),
(45, 'property', 'steam:11000011a9b2c0c', '{}'),
(46, 'user_mask', 'Char1:11000010a01bdb9', '{}'),
(47, 'property', 'Char1:11000010a01bdb9', '{}'),
(48, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(49, 'property', 'steam:11000010a01bdb9', '{}'),
(50, 'property', 'steam:1100001068ef13c', '{}'),
(51, 'user_mask', 'steam:1100001068ef13c', '{}'),
(52, 'user_mask', 'steam:11000010bf93966', '{}'),
(53, 'property', 'steam:11000010bf93966', '{}'),
(54, 'property', 'steam:110000112969e8f', '{}'),
(55, 'user_mask', 'steam:110000112969e8f', '{}'),
(56, 'property', 'steam:110000117b74e38', '{}'),
(57, 'user_mask', 'steam:110000117b74e38', '{}'),
(58, 'property', 'steam:11000010ddba29f', '{}'),
(59, 'user_mask', 'steam:11000010ddba29f', '{}'),
(60, 'property', 'steam:11000010c2ebf86', '{}'),
(61, 'user_mask', 'steam:11000010c2ebf86', '{}'),
(62, 'property', 'steam:11000010a078bc7', '{}'),
(63, 'user_mask', 'steam:11000010a078bc7', '{}'),
(64, 'society_gitrdone', NULL, '{}'),
(65, 'user_mask', 'steam:11000013d51c2ff', '{}'),
(66, 'property', 'steam:11000013d51c2ff', '{}'),
(67, 'user_mask', 'steam:11000011a2fc3d0', '{}'),
(68, 'property', 'steam:11000011a2fc3d0', '{}'),
(69, 'user_mask', 'steam:1100001048af48f', '{}'),
(70, 'property', 'steam:1100001048af48f', '{}'),
(71, 'user_mask', 'steam:11000013bc17e0e', '{}'),
(72, 'property', 'steam:11000013bc17e0e', '{}'),
(73, 'user_mask', 'steam:1100001079cf4ab', '{}'),
(74, 'property', 'steam:1100001079cf4ab', '{}'),
(75, 'user_mask', 'steam:11000013b85ca5f', '{}'),
(76, 'property', 'steam:11000013b85ca5f', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Toro', 'toro', 2000, 'dock'),
(3, 'Dinghy3', 'dinghy3', 2000, 'dock'),
(4, 'Seashark', 'seashark', 1000, 'dock'),
(5, 'Submarine', 'submersible2', 4000, 'dock');

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ballas`
--

CREATE TABLE `fine_types_ballas` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_ballas`
--

INSERT INTO `fine_types_ballas` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3),
(8, 'Raket', 3000, 0),
(9, 'Raket', 5000, 0),
(10, 'Raket', 10000, 1),
(11, 'Raket', 20000, 1),
(12, 'Raket', 50000, 2),
(13, 'Raket', 150000, 3),
(14, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bishops`
--

CREATE TABLE `fine_types_bishops` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bishops`
--

INSERT INTO `fine_types_bishops` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_dismay`
--

CREATE TABLE `fine_types_dismay` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_dismay`
--

INSERT INTO `fine_types_dismay` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_grove`
--

CREATE TABLE `fine_types_grove` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_grove`
--

INSERT INTO `fine_types_grove` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_irish`
--

CREATE TABLE `fine_types_irish` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_irish`
--

INSERT INTO `fine_types_irish` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_mafia`
--

CREATE TABLE `fine_types_mafia` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_mafia`
--

INSERT INTO `fine_types_mafia` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rebel`
--

CREATE TABLE `fine_types_rebel` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rebel`
--

INSERT INTO `fine_types_rebel` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rodriguez`
--

CREATE TABLE `fine_types_rodriguez` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rodriguez`
--

INSERT INTO `fine_types_rodriguez` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_vagos`
--

CREATE TABLE `fine_types_vagos` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_vagos`
--

INSERT INTO `fine_types_vagos` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `glovebox_inventory`
--

INSERT INTO `glovebox_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(5, '03HSO812', '{}', 1),
(14, '86TKD185', '{}', 1),
(35, 'TYH 041 ', '{\"weapons\":[{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"ammo\":4500}]}', 1),
(37, '06VHJ277', '{\"coffres\":[{\"count\":1,\"name\":\"licenseplate\"}]}', 1),
(59, '08JTP908', '{}', 1),
(61, '22OAJ106', '{}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT -1,
  `rare` int(11) NOT NULL DEFAULT 0,
  `can_remove` int(11) NOT NULL DEFAULT 1,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`, `price`) VALUES
(1, 'bread', 'Bread', -1, 0, 1, 0),
(2, 'water', 'Water', -1, 0, 1, 0),
(3, 'weed', 'Weed', 0, 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1, 0),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1, 0),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1, 0),
(14, 'fish', 'Fish', -1, 0, 1, 0),
(15, 'stone', 'Stone', -1, 0, 1, 0),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1, 0),
(17, 'copper', 'Copper', -1, 0, 1, 0),
(18, 'iron', 'Iron', -1, 0, 1, 0),
(19, 'gold', 'Gold', -1, 0, 1, 0),
(20, 'diamond', 'Diamond', -1, 0, 1, 0),
(21, 'wood', 'Wood', -1, 0, 1, 0),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1, 0),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1, 0),
(24, 'petrol', 'Gas', -1, 0, 1, 0),
(25, 'petrol_raffin', 'Refined Oil', -1, 0, 1, 0),
(26, 'gasoline', 'Gasoline', -1, 0, 1, 0),
(27, 'whool', 'Whool', -1, 0, 1, 0),
(28, 'fabric', 'Fabric', -1, 0, 1, 0),
(29, 'clothe', 'Clothe', -1, 0, 1, 0),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1, 0),
(31, 'fixtool', 'Repair Tools', -1, 0, 1, 0),
(32, 'carotool', 'Tools', -1, 0, 1, 0),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1, 0),
(34, 'fixkit', 'Repair Kit', -1, 0, 1, 0),
(35, 'carokit', 'Body Kit', -1, 0, 1, 0),
(36, 'beer', 'Beer', -1, 0, 1, 0),
(37, 'bandage', 'Bandage', 20, 0, 1, 0),
(38, 'medikit', 'Medikit', 100, 0, 1, 0),
(39, 'pills', 'Pills', 10, 0, 1, 0),
(40, 'lockpick', 'Lockpick', -1, 0, 1, 0),
(41, 'vodka', 'Vodka', -1, 0, 1, 0),
(42, 'coffee', 'Coffee', -1, 0, 1, 0),
(49, 'clip', 'Chargeur', -1, 0, 1, 0),
(50, 'jager', 'Jägermeister', 5, 0, 1, 0),
(51, 'vodka', 'Vodka', 5, 0, 1, 0),
(52, 'rhum', 'Rhum', 5, 0, 1, 0),
(53, 'whisky', 'Whisky', 5, 0, 1, 0),
(54, 'tequila', 'Tequila', 5, 0, 1, 0),
(55, 'martini', 'Martini blanc', 5, 0, 1, 0),
(56, 'soda', 'Soda', 5, 0, 1, 0),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1, 0),
(58, 'icetea', 'Ice Tea', 5, 0, 1, 0),
(59, 'energy', 'Energy Drink', 5, 0, 1, 0),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1, 0),
(61, 'limonade', 'Limonade', 5, 0, 1, 0),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1, 0),
(63, 'bolnoixcajou', 'Bowl of Cashews', 5, 0, 1, 0),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1, 0),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1, 0),
(66, 'saucisson', 'Sausage', 5, 0, 1, 0),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1, 0),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1, 0),
(69, 'golem', 'Golem', 5, 0, 1, 0),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1, 0),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1, 0),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1, 0),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1, 0),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1, 0),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1, 0),
(76, 'mojito', 'Mojito', 5, 0, 1, 0),
(77, 'ice', 'Glaçon', 5, 0, 1, 0),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1, 0),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1, 0),
(81, 'menthe', 'Mint leaf', 10, 0, 1, 0),
(82, 'cola', 'Coke', -1, 0, 1, 0),
(105, 'vegetables', 'Vegetables', 20, 0, 1, 0),
(117, 'turtle', 'Turtle', -1, 0, 1, 0),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1, 0),
(119, 'lsd', 'Lsd', -1, 0, 1, 0),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1, 0),
(121, 'pearl', 'Pearl', -1, 0, 1, 0),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1, 0),
(123, 'litter', 'Litter', -1, 0, 1, 0),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1, 0),
(128, 'meat', 'Meat', 20, 0, 1, 0),
(129, 'tacos', 'Tacos', 20, 0, 1, 0),
(130, 'burger', 'Burger', 20, 0, 1, 0),
(131, 'silencieux', 'Siliencer', -1, 0, 1, 0),
(132, 'flashlight', 'Flashlight', -1, 0, 1, 0),
(133, 'grip', 'Grip', -1, 0, 1, 0),
(134, 'yusuf', 'Skin', -1, 0, 1, 0),
(135, 'binoculars', 'Binoculars', 1, 0, 1, 0),
(136, 'croquettes', 'Croquettes', -1, 0, 1, 0),
(137, 'blackberry', 'blackberry', -1, 0, 1, 0),
(138, 'lighter', 'Bic', -1, 0, 1, 0),
(139, 'cigarett', 'Cigarette', -1, 0, 1, 0),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1, 0),
(2010, 'armor', 'Armor', -1, 0, 1, 0),
(2011, 'receipt', 'Receipt', 100, 0, 1, 0),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1, 0),
(2013, 'powerade', 'Powerade', -1, 0, 1, 0),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1, 0),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1, 0),
(2016, 'plongee1', 'Short Dive', -1, 0, 1, 0),
(2017, 'plongee2', 'Long Dive', -1, 0, 1, 0),
(2018, 'contrat', 'Salvage', 15, 0, 1, 0),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1, 0),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1, 0),
(2021, 'meat', 'Meat', -1, 0, 1, 0),
(2022, 'leather', 'Leather', -1, 0, 1, 0),
(2023, 'cannabis', 'Cannabis', 50, 0, 1, 0),
(2024, 'marijuana', 'Marijuana', 250, 0, 1, 0),
(2025, 'coca', 'CocaPlant', 150, 0, 1, 0),
(2026, 'cocaine', 'Coke', 50, 0, 1, 0),
(2027, 'ephedra', 'Ephedra', 100, 0, 1, 0),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1, 0),
(2029, 'poppy', 'Poppy', 100, 0, 1, 0),
(2030, 'opium', 'Opium', 50, 0, 1, 0),
(2031, 'meth', 'Meth', 25, 0, 1, 0),
(2032, 'heroine', 'Heroine', 10, 0, 1, 0),
(2033, 'beer', 'Beer', 30, 0, 1, 0),
(2034, 'tequila', 'Tequila', 10, 0, 1, 0),
(2035, 'vodka', 'Vodka', 10, 0, 1, 0),
(2036, 'whiskey', 'Whiskey', 10, 0, 1, 0),
(2037, 'crack', 'Crack', 25, 0, 1, 0),
(2038, 'drugtest', 'DrugTest', 10, 0, 1, 0),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1, 0),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1, 0),
(2041, 'pcp', 'PCP', 25, 0, 1, 0),
(2042, 'dabs', 'Dabs', 50, 0, 1, 0),
(2043, 'painkiller', 'Painkiller', 10, 0, 1, 0),
(2044, 'narcan', 'Narcan', 10, 0, 1, 0),
(2045, 'boitier', 'Darknet', -1, 0, 1, 0),
(2046, 'cocacola', 'Coca Cola', -1, 0, 1, 0),
(2047, 'fanta', 'Fanta Exotic', -1, 0, 1, 0),
(2048, 'sprite', 'Sprite', -1, 0, 1, 0),
(2049, 'loka', 'Loka Crush', -1, 0, 1, 0),
(2050, 'cheesebows', 'Cheese Doodles', -1, 0, 1, 0),
(2051, 'chips', 'Chips', -1, 0, 1, 0),
(2052, 'marabou', 'Milk Chocolate ', -1, 0, 1, 0),
(2053, 'pizza', 'Kebab Pizza', -1, 0, 1, 0),
(2054, 'baconburger', 'Bacon Burger', -1, 0, 1, 0),
(2055, 'pastacarbonara', 'Pasta Carbonara', -1, 0, 1, 0),
(2056, 'macka', 'Ham sammy', -1, 0, 1, 0),
(2059, 'lotteryticket', 'lottery ticket', -1, 0, 1, 0),
(2060, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1, 0),
(2061, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1, 0),
(2062, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1, 0),
(2063, 'hunting_license', 'Hunting License', -1, 0, 1, 0),
(2064, 'fishing_license', 'Fishing License', -1, 0, 1, 0),
(2065, 'diving_license', 'Diving License', -1, 0, 1, 0),
(2066, 'marriage_license', 'Marriage License', -1, 0, 1, 0),
(2067, 'pilot_license', 'Pilot License', -1, 0, 1, 0),
(2068, 'taxi_license', 'Taxi License', -1, 0, 1, 0),
(2069, 'cdl', 'Commerical Drivers License', -1, 0, 1, 0),
(2070, 'mlic', 'Motorcycle License', -1, 0, 1, 0),
(2071, 'dlic', 'Drivers License', -1, 0, 1, 0),
(2072, 'boating_license', 'Boating License', -1, 0, 1, 0),
(2073, 'firstaidpass', 'First Aid Pass', 1, 0, 0, 0),
(2074, 'nitrocannister', 'NOS', 1, 0, 1, 0),
(2075, 'wrench', 'Wrench', 1, 0, 1, 0),
(2076, 'firstaidkit', 'First Aid Kit', 1, 0, 1, 0),
(2077, 'defibrillateur', 'AED', 1, 0, 1, 0),
(2078, 'radio', 'Radio', 1, 0, 0, 0),
(2079, 'WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1, 0),
(2080, 'WEAPON_STUNGUN', 'Taser', 100, 1, 1, 0),
(2081, 'WEAPON_KNIFE', 'Knife', 100, 1, 1, 0),
(2082, 'WEAPON_BAT', 'Baseball Bat', 1, 0, 1, 0),
(2083, 'WEAPON_PISTOL', 'Pistol', 100, 1, 1, 0),
(2084, 'WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1, 0),
(2085, '9mm_rounds', '9mm Rounds', 20, 0, 1, 0),
(2086, 'shotgun_shells', 'Shotgun Shells', 20, 0, 1, 0),
(2087, 'receipt', 'W.C receipt', 100, 0, 1, 0),
(2088, 'licenseplate', 'License plate', -1, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'Unemployed', 0),
(3, 'police', 'LEO', 1),
(4, 'slaughterer', 'Slaughterer', 0),
(5, 'fisherman', 'Fisherman', 0),
(6, 'miner', 'Miner', 0),
(7, 'lumberjack', 'Woodcutter', 0),
(8, 'fuel', 'Refiner', 0),
(9, 'reporter', 'Journalist', 0),
(10, 'textil', 'Couturier', 0),
(11, 'mechanic', 'Mechanic', 0),
(12, 'taxi', 'Taxi', 0),
(14, 'trucker', 'Eddies Trucking', 0),
(16, 'fire', 'LFD', 1),
(17, 'deliverer', 'Amazon', 0),
(18, 'brinks', 'Brinks', 0),
(19, 'gopostal', 'GoPostal', 0),
(20, 'airlines', 'Airlines', 1),
(21, 'ambulance', 'AMR', 1),
(22, 'mafia', 'Mafia', 1),
(24, 'rebel', 'Rebel', 1),
(25, 'security', 'Palm City Security', 0),
(28, 'garbage', 'Garbage Driver', 0),
(29, 'pizza', 'Pizzadelivery', 0),
(30, 'ranger', 'parkranger', 1),
(33, 'unicorn', 'Unicorn', 1),
(34, 'bus', 'RTD', 0),
(36, 'coastguard', 'CoastGuard', 1),
(38, 'irish', 'Irish', 1),
(39, 'rodriguez', 'Rodriguez', 1),
(41, 'bishops', 'Bishops', 1),
(42, 'irish', 'Irish', 1),
(43, 'lawyer', 'lawyer', 1),
(45, 'dismay', 'Dismay', 1),
(46, 'bountyhunter', 'Bountyhunter', 1),
(47, 'grove', 'Grove Street Family', 1),
(49, 'vagos', 'Vagos', 1),
(50, 'ballas', 'Ballas', 1),
(51, 'traffic', 'trafficofficer', 1),
(52, 'poolcleaner', 'PoolCleaner', 0),
(53, 'carthief', 'Car Thief', 1),
(54, 'Salvage', 'Salvage', 1),
(55, 'dispatch', 'Dispatch', 1),
(56, 'admin', 'Admin', 1),
(57, 'biker', 'HHMC', 1),
(58, 'cardealer', 'Car Dealer', 1),
(59, 'offpolice', 'Off-Duty LEO', 1),
(60, 'offambulance', 'Off-Duty EMS', 1),
(61, 'biker', 'Biker', 1),
(62, 'windowcleaner', 'Spick N Span', 0),
(63, 'gitrdone', 'GrD Construction', 1),
(64, 'parking', 'Parking Enforcement', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(0, 'police', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(1, 'lumberjack', 0, 'interim', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'interim', 'Employee', 250, '{}', '{}'),
(3, 'fuel', 0, 'interim', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'textil', 0, 'interim', 'Employee', 250, '{}', '{}'),
(6, 'miner', 0, 'interim', 'Employee', 250, '{}', '{}'),
(7, 'slaughterer', 0, 'interim', 'Employee', 250, '{}', '{}'),
(8, 'mechanic', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mechanic', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mechanic', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mechanic', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mechanic', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'pilot', 'Pilot', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'employee', 'Employee', 100, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'dispatch', 0, 'dispatcher', 'Dispatcher', 400, '{}', '{}'),
(78, 'dispatch', 1, 'boss', 'Dispatch Command', 800, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'soldato', 'Prospect', 1500, '{}', '{}'),
(164, 'biker', 1, 'capo', 'Chapter member', 1800, '{}', '{}'),
(165, 'biker', 2, 'consigliere', 'Nomad', 2100, '{}', '{}'),
(166, 'biker', 3, 'boss', 'President', 10000, '{}', '{}'),
(167, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(168, 'cardealer', 0, 'recruit', 'Recruit', 100, '{}', '{}'),
(169, 'cardealer', 1, 'novice', 'Novice', 250, '{}', '{}'),
(170, 'cardealer', 2, 'experienced', 'Experienced', 350, '{}', '{}'),
(171, 'cardealer', 3, 'boss', 'Boss', 500, '{}', '{}'),
(338, 'offpolice', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(339, 'offambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(340, 'offpolice', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(341, 'offpolice', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(342, 'offpolice', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(343, 'offpolice', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(344, 'offpolice', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(345, 'offpolice', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(346, 'offpolice', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(347, 'offpolice', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(348, 'offpolice', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(349, 'offpolice', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(350, 'offpolice', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(351, 'offpolice', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(352, 'offpolice', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(353, 'offpolice', 14, 'reserve', '(ACSO) Reserve Officer', 150, '{}', '{}'),
(354, 'offpolice', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(355, 'offpolice', 16, 'corporal', '(ACSO) Corporal', 250, '{}', '{}'),
(356, 'offpolice', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(357, 'offpolice', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(358, 'offpolice', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(359, 'offpolice', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(360, 'offpolice', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(361, 'offpolice', 22, 'chief', '(ACSO) Chief', 400, '{}', '{}'),
(362, 'offpolice', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(363, 'offpolice', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(364, 'offpolice', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(365, 'offpolice', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(366, 'offpolice', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(367, 'offpolice', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(368, 'offpolice', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(369, 'offpolice', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(370, 'offpolice', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(371, 'offpolice', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(372, 'offpolice', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(373, 'offpolice', 34, 'chief', '(CSP) Chief', 400, '{}', '{}'),
(374, 'offpolice', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(375, 'offpolice', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(376, 'offpolice', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(377, 'offambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(378, 'offambulance', 2, 'emr', 'Emergency Medical Response', 150, '{}', '{}'),
(379, 'offambulance', 3, 'emt', 'Emergency Medical Technician', 200, '{}', '{}'),
(380, 'offambulance', 4, 'aemt', 'AEMT', 250, '{}', '{}'),
(381, 'offambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(382, 'offambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(383, 'offambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(384, 'offambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(385, 'offambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(386, 'offambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(387, 'offambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(388, 'offambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(389, 'police', 39, 'dispatch', 'Dispatcher', 400, '{}', '{}'),
(390, 'police', 40, 'dispatch', 'Dispatch Command', 800, '{}', '{}'),
(391, 'windowcleaner', 0, 'cleaner', 'Cleaner', 100, '{}', '{}'),
(392, 'gitrdone', 0, 'laborer', 'Laborer', 60, '{}', '{}'),
(393, 'gitrdone', 1, 'operator', 'Equipment Operator', 60, '{}', '{}'),
(394, 'gitrdone', 2, 'foreman', 'Job Foreman', 85, '{}', '{}'),
(395, 'gitrdone', 3, 'boss', 'Contractor / Owner', 100, '{}', '{}'),
(396, 'parking', 0, 'meter_maid', 'Meter Maid', 1500, '{}', '{}'),
(397, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 2000, '{}', '{}'),
(398, 'parking', 2, 'boss', 'CEO', 2500, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kicks`
--

CREATE TABLE `kicks` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `nitro_vehicles`
--

CREATE TABLE `nitro_vehicles` (
  `plate` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicles`
--

CREATE TABLE `old_vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicles`
--

INSERT INTO `old_vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 15000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
(4, 'Chino', 'chino', 15000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 250000, 'muscle'),
(7, 'Dominator', 'dominator', 60000, 'muscle'),
(8, 'Dukes', 'dukes', 28000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
(11, 'Faction', 'faction', 20000, 'muscle'),
(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
(13, 'Faction XL', 'faction3', 40000, 'muscle'),
(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
(16, 'Picador', 'picador', 18000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
(20, 'Tampa', 'tampa', 16000, 'muscle'),
(21, 'Virgo', 'virgo', 14000, 'muscle'),
(22, 'Vigero', 'vigero', 12500, 'muscle'),
(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
(24, 'Blista', 'blista', 42958, 'compacts'),
(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
(26, 'Issi', 'issi2', 10000, 'compacts'),
(27, 'Panto', 'panto', 10000, 'compacts'),
(28, 'Prairie', 'prairie', 12000, 'compacts'),
(29, 'Bison', 'bison', 45000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
(31, 'Burrito', 'burrito3', 19000, 'work'),
(32, 'Burrito', 'gburrito2', 29000, 'vans'),
(33, 'Camper', 'camper', 42000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
(35, 'Journey', 'journey', 6500, 'vans'),
(36, 'Minivan', 'minivan', 13000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
(39, 'Paradise', 'paradise', 19000, 'vans'),
(40, 'Rumpo', 'rumpo', 30000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
(42, 'Surfer', 'surfer', 12000, 'vans'),
(43, 'Youga', 'youga', 10800, 'vans'),
(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
(45, 'Asea', 'asea', 5500, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
(47, 'Emperor', 'emperor', 8500, 'sedans'),
(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
(49, 'Glendale', 'glendale', 6500, 'sedans'),
(50, 'Intruder', 'intruder', 7500, 'sedans'),
(51, 'Premier', 'premier', 8000, 'sedans'),
(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
(53, 'Regina', 'regina', 5000, 'sedans'),
(54, 'Schafter', 'schafter2', 25000, 'sedans'),
(55, 'Stretch', 'stretch', 90000, 'sedans'),
(56, 'Phantom', 'superd', 250000, 'sedans'),
(57, 'MercedezAMG', 'tailgater', 130000, 'modded'),
(58, 'Warrener', 'warrener', 120000, 'sedans'),
(59, 'Washington', 'washington', 90000, 'sedans'),
(60, 'Baller', 'baller2', 40000, 'suvs'),
(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
(63, 'Contender', 'contender', 70000, 'suvs'),
(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
(66, 'Fhantom', 'fq2', 17000, 'suvs'),
(67, 'Grabger', 'granger', 50000, 'suvs'),
(68, 'Gresley', 'gresley', 47500, 'suvs'),
(69, 'Huntley S', 'huntley', 40000, 'suvs'),
(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
(71, 'Mesa', 'mesa', 16000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
(73, 'Patriot', 'patriot', 55000, 'suvs'),
(74, 'Radius', 'radi', 29000, 'suvs'),
(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
(76, 'Seminole', 'seminole', 25000, 'suvs'),
(77, 'XLS', 'xls', 70000, 'suvs'),
(78, 'Btype', 'btype', 62000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
(81, 'Casco', 'casco', 350000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
(83, 'Manana', 'manana', 12800, 'sportsclassics'),
(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 220000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
(89, 'Porsche Classic', 'ztype', 160000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 12000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
(92, 'Blazer', 'blazer', 6500, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
(94, 'Brawler', 'brawler', 75000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
(97, 'Guardian', 'guardian', 45000, 'offroad'),
(98, 'Rebel', 'rebel2', 35000, 'offroad'),
(99, 'Sandking', 'sandking', 55000, 'offroad'),
(100, 'The Liberator', 'monster', 210000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
(104, 'Exemplar', 'exemplar', 132000, 'modded'),
(105, 'Silvia', 'f620', 80000, 'modded'),
(106, 'Felon', 'felon', 42000, 'coupes'),
(107, 'Felon GT', 'felon2', 55000, 'coupes'),
(108, 'Jackal', 'jackal', 38000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
(112, 'Windsor', 'windsor', 350000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 180000, 'coupes'),
(114, 'Zion', 'zion', 40000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 70000, 'coupes'),
(116, '9F', 'ninef', 65000, 'sports'),
(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 125000, 'modded'),
(120, 'Bestia GTS', 'bestiagts', 400000, 'sports'),
(121, 'Buffalo', 'buffalo', 120000, 'modded'),
(122, 'wide body charger', 'buffalo2', 175000, 'modded'),
(123, 'Carbonizzare', 'carbonizzare', 110000, 'modded'),
(124, 'Comet', 'comet2', 200000, 'modded'),
(125, 'Coquette', 'coquette', 65000, 'sports'),
(126, 'Vantage', 'tampa2', 230000, 'sports'),
(127, 'Elegy', 'elegy2', 175000, 'sports'),
(128, 'Feltzer', 'feltzer2', 125000, 'sports'),
(129, 'Furore GT', 'furoregt', 45000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
(133, 'Khamelion', 'khamelion', 38000, 'sports'),
(134, 'Kuruma', 'kuruma', 75000, 'sports'),
(135, 'Lynx', 'lynx', 40000, 'sports'),
(136, 'Mamba', 'mamba', 70000, 'sports'),
(137, 'Massacro', 'massacro', 65000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
(139, 'Omnis', 'omnis', 35000, 'sports'),
(140, 'Penumbra', 'penumbra', 28000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
(143, 'Schafter V12', 'schafter3', 130000, 'sports'),
(144, 'Seven 70', 'seven70', 290000, 'sports'),
(145, 'Sultan', 'sultan', 55000, 'sports'),
(146, 'Surano', 'surano', 50000, 'sports'),
(147, 'Tropos', 'tropos', 95000, 'sports'),
(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
(149, 'Adder', 'adder', 1000000, 'modded'),
(150, 'Banshee 900R', 'banshee2', 350000, 'super'),
(151, 'Bullet', 'bullet', 250000, 'super'),
(152, 'Cheetah', 'cheetah', 500000, 'modded'),
(153, 'Entity XF', 'entityxf', 425000, 'super'),
(154, 'Maserati ET1', 'sheava', 500000, 'super'),
(155, 'FMJ', 'fmj', 185000, 'super'),
(156, 'Infernus', 'infernus', 240000, 'modded'),
(157, 'Osiris', 'osiris', 160000, 'super'),
(158, 'Pfister', 'pfister811', 85000, 'super'),
(159, 'RE-7B', 'le7b', 325000, 'super'),
(160, 'Reaper', 'reaper', 150000, 'super'),
(161, 'Sultan RS', 'sultanrs', 130000, 'super'),
(162, 'T20', 't20', 300000, 'super'),
(163, 'Turismo R', 'turismor', 350000, 'super'),
(164, 'Tyrus', 'tyrus', 600000, 'super'),
(165, 'Vacca', 'vacca', 120000, 'super'),
(166, 'Voltic', 'voltic', 900000, 'super'),
(167, 'X80 Proto', 'prototipo', 950000, 'super'),
(168, 'Zentorno', 'zentorno', 700000, 'super'),
(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
(174, 'BF400', 'bf400', 6500, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
(183, 'Double T', 'double', 28000, 'motorcycles'),
(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 7500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 7500, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 80000, 'motorcycles'),
(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
(207, 'Vader', 'vader', 7200, 'motorcycles'),
(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
(213, 'Ruiner', 'ruiner', 90000, 'muscle'),
(214, 'TunedComet', 'comet3', 300000, 'super'),
(215, 'TunedslamVan', 'slamvan3', 270000, 'muscle'),
(216, 'TunedVirgo', 'virgo3', 220000, 'muscle'),
(217, 'Nero', 'nero', 400000, 'super'),
(218, 'TunedBucaneer', 'buccaneer2', 200000, 'muscle'),
(219, 'TunedChino', 'chino2', 190000, 'muscle'),
(220, 'TunedElegy', 'elegy', 220000, 'sports'),
(221, 'TunedFaction', 'faction2', 190000, 'muscle'),
(222, 'PegassiFCR', 'fcr', 8000, 'motorcycles'),
(223, 'TunedPegassi', 'fcr2', 15000, 'motorcycles'),
(224, 'ProgenItaliGTB', 'italigtb', 260000, 'super'),
(225, 'TunedItaliGTB', 'italigtb2', 320000, 'super'),
(226, 'Tunedminivan', 'minivan2', 160000, 'suvs'),
(227, 'TunedNero', 'nero2', 500000, 'super'),
(228, 'Primo', 'primo', 35000, 'coupes'),
(229, 'DewbaucheeSpecter', 'specter', 220000, 'sports'),
(230, 'Tunedspecter', 'specter2', 290000, 'sports'),
(231, 'TunedVan', 'slamvan2', 25000, 'muscle'),
(232, 'EMSCar', 'emscar', 1000, 'work'),
(233, 'EMSCar2', 'emscar2', 1000, 'work'),
(234, 'EMSVan', 'emsvan', 1000, 'work'),
(235, 'EMSSuv', 'emssuv', 1000, 'work'),
(236, 'Rat Loader', 'ratloader', 10000, 'work'),
(237, 'Sand King2', 'sandking2', 60000, 'offroad'),
(238, 'Sadler', 'sadler', 75000, 'offroad'),
(239, 'Taxi', 'taxi', 15000, 'work'),
(240, 'Rubble', 'rubble', 100000, 'work'),
(241, 'Tour Bus', 'tourbus', 30000, 'work'),
(242, 'Tow Truck', 'towtruck', 25000, 'work'),
(243, 'Flat Bed', 'flatbed', 27000, 'work'),
(244, 'Clown', 'speedo2', 16000, 'work'),
(246, 'Stratum', 'stratum', 80000, 'sports'),
(249, 'Mazda', 'blista3', 40000, 'compacts'),
(250, 'Honda Civic', 'blista2', 30000, 'compacts'),
(251, 'Caddilac', 'buffalo3', 50000, 'sedans'),
(252, 'Golf Green', 'surge', 70000, 'compacts'),
(253, 'Stalion', 'stalion', 60000, 'muscle'),
(254, 'SRT', 'stalion2', 160000, 'muscle'),
(255, 'Mercedez RI', 'serrano', 90000, 'suvs'),
(256, 'Mercedes AMG', 'schafter4', 140000, 'sedans'),
(257, 'BMW M3', 'schafter5', 135000, 'sedans'),
(258, 'BMW M3', 'schwarzer', 150000, 'sedans'),
(259, 'gt500', 'gt500', 45000, 'doomsday'),
(260, 'comet4', 'comet4', 180000, 'doomsday'),
(261, 'comet5', 'comet5', 200000, 'doomsday'),
(263, 'hermes', 'hermes', 60000, 'doomsday'),
(264, 'hustler', 'hustler', 60000, 'doomsday'),
(265, 'kamacho', 'kamacho', 50000, 'doomsday'),
(266, 'neon', 'neon', 100000, 'doomsday'),
(267, 'pariah', 'pariah', 100000, 'doomsday'),
(268, 'raiden', 'raiden', 75000, 'doomsday'),
(269, 'revolter', 'revolter', 75000, 'doomsday'),
(270, 'riata', 'riata', 75000, 'doomsday'),
(271, 'savestra', 'savestra', 75000, 'doomsday'),
(272, 'sc1', 'sc1', 75000, 'doomsday'),
(273, 'streiter', 'streiter', 75000, 'doomsday'),
(274, 'stromberg', 'stromberg', 75000, 'doomsday'),
(275, 'sentinel3', 'sentinel3', 75000, 'doomsday'),
(276, 'viseris', 'viseris', 75000, 'doomsday'),
(277, 'yosemite', 'yosemite', 75000, 'doomsday'),
(278, 'z190', 'z190', 75000, 'doomsday'),
(279, 'Mustang', 'musty5', 90000, 'modded'),
(280, 'Infiniti G37', 'g37cs', 50000, 'modded'),
(281, 'Peugeot 107', 'p107', 30000, 'modded'),
(282, 'Renault Megane', 'renmeg', 70000, 'modded'),
(283, 'Lamborghini Hurricane', 'lh610', 230000, 'modded'),
(284, 'Aston Cygnet', 'cygnet11', 40000, 'modded'),
(285, 'Cadillac CTS', 'cadicts', 55000, 'modded'),
(286, 'Mini John Cooper', 'miniub', 45000, 'modded'),
(287, 'Lotus Espirit V8', 'lev8', 130000, 'modded'),
(288, 'Lambo Veneno', 'lamven', 500000, 'modded'),
(289, 'Nissan GTR SpecV', 'gtrublu', 230000, 'modded'),
(290, 'Genesis', 'genublu', 60000, 'modded'),
(291, 'Porsche Cayman R', 'caymanub', 190000, 'modded'),
(292, 'Porsche 911 GT', '911ublu', 700000, 'modded'),
(293, 'Ferrari Laferrari', 'laferublu', 450000, 'modded'),
(294, 'Mclaren 12c', 'mcublu', 400000, 'modded'),
(295, 'Merc SLR', 'slrublu', 200000, 'modded'),
(297, 'Merc SLS AMG Electric', 'slsublue', 150000, 'modded'),
(298, 'Dodge Charger', 'charublu', 90000, 'modded'),
(299, 'subaru 22b', '22bbublu', 80000, 'modded'),
(300, 'Focus RS', 'focusublu', 70000, 'modded'),
(301, 'Mazda furai', 'furaiub', 350000, 'modded'),
(302, 'Ferrari F50', 'f50ub', 350000, 'modded'),
(303, 'Porsche 550a', 'p550a', 250000, 'modded'),
(304, 'Porsche 959', 'p959', 250000, 'modded'),
(305, 'Porsche 944', 'p944', 180000, 'modded'),
(306, 'dodge Viper', 'vip99', 450000, 'modded'),
(307, 'Mazda Rx8', 'rx8', 50000, 'modded'),
(308, 'Ferrari 599', 'gtbf', 290000, 'modded'),
(309, 'Tesla Roadster', 'tesla11', 55000, 'modded'),
(310, 'Mazda Mx5a', 'mx5a', 45000, 'modded'),
(311, 'toyota Celica', 'celicassi', 48000, 'modded'),
(312, 'Toyota celica T', 'celicassi2', 55000, 'modded'),
(313, 'Aston Martin Vanquish', 'amv12', 280000, 'modded'),
(314, 'Subari WRX STI', 'sti05', 80000, 'modded'),
(315, 'Porsche Panamera', 'panamera', 200000, 'modded'),
(316, 'Ferrari 360', 'f360', 250000, 'modded'),
(317, 'Lambo Mura', 'miura', 230000, 'modded'),
(318, 'chevrolet Corvette', 'zr1c3', 180000, 'modded'),
(319, 'Lambo Gallardo', 'gallardo', 300000, 'modded'),
(320, 'Corvette Stingray', 'vc7', 230000, 'modded'),
(321, 'Ferrari Cali', '2fiftygt', 260000, 'modded'),
(322, 'Mercedz Gullwing', '300gsl', 200000, 'modded'),
(323, 'Aston Martin vantage', 'db700', 140000, 'modded'),
(324, 'shelby cobra', 'cobra', 130000, 'modded'),
(325, 'BMW Z4i', 'z4i', 100000, 'modded'),
(326, 'Lambo Huracan', 'huracan', 240000, 'modded'),
(327, 'Ferrari 812', 'ferrari812', 250000, 'modded'),
(328, 'Lambo Veneno', 'veneno', 350000, 'modded'),
(329, 'Ferrari XXK', 'fxxk16', 300000, 'modded'),
(330, 'LaFerrari 15', 'laferrari15', 400000, 'modded'),
(331, 'Italia 458 LW', 'lw458s', 320000, 'modded'),
(332, 'Lykan', 'lykan', 350000, 'modded'),
(333, 'iTalia 458', 'italia458', 290000, 'modded'),
(334, 'Diablous', 'Diablous', 15000, 'motorcycles'),
(335, 'Diablous 2', 'Diablous2', 17000, 'motorcycles'),
(336, 'Raptor', 'Raptor', 20000, 'motorcycles'),
(337, 'Ratbike', 'Ratbike', 16000, 'motorcycles'),
(338, 'Xa21', 'XA21', 340000, 'super'),
(339, 'Penetrator', 'Penetrator', 300000, 'super'),
(340, 'Gp1', 'GP1', 270000, 'super'),
(341, 'Tempestra', 'Tempesta', 260000, 'super'),
(342, 'Toreo', 'Torero', 250000, 'sportsclassics'),
(343, 'Infernus2', 'Infernus2', 240000, 'sportsclassics'),
(344, 'Savestra', 'Savestra', 240000, 'sportsclassics'),
(345, 'Cheetah2', 'Cheetah2', 200000, 'sportsclassics'),
(346, 'Turismo2', 'Turismo2', 180000, 'sportsclassics'),
(347, 'Viceris', 'Viceris', 190000, 'sportsclassics'),
(348, 'JB700', 'JB700', 190000, 'sportsclassics'),
(349, 'Peyote', 'Peyote', 175000, 'sportsclassics'),
(350, 'Ruston', 'Ruston', 150000, 'sports'),
(351, 'Surge', 'Surge', 45000, 'sedans'),
(353, 'Voltic', 'Voltic2', 4000000, 'super'),
(354, 'Dilettante', 'Dilettante', 50000, 'compacts'),
(355, 'Tornado6', 'Tornado6', 150000, 'muscle'),
(356, 'Gauntlet2', 'Gauntlet2', 100000, 'muscle'),
(357, 'Dominator2', 'Dominator2', 100000, 'muscle'),
(358, 'Hurse', 'Lurcher', 60000, 'muscle'),
(359, 'Vagner', 'Vagner', 250000, 'super'),
(360, 'Austarch', 'Autarch', 230000, 'super'),
(361, 'Tornado5', 'tornado5', 75000, 'muscle'),
(362, 'audi a4', 'asterope', 110000, 'sports'),
(363, 'Merc AMG', 'rmodamgc63', 165000, 'sports'),
(364, 'dodgeCharger', '69charger', 140000, 'muscle'),
(365, 'R35', 'r35', 200000, 'modded'),
(367, 'Mustang', 'mgt', 180000, 'modded'),
(368, 'Cheburek', 'cheburek', 20000, 'assault'),
(369, 'Ellie', 'ellie', 100000, 'assault'),
(370, 'Dommy3', 'dominator3', 110000, 'assault'),
(371, 'Enity2', 'entity2', 200000, 'assault'),
(372, 'Fagi', 'fagaloa', 22000, 'assault'),
(373, 'Flash', 'flashgt', 105000, 'assault'),
(374, 'RS200', 'gb200', 135000, 'assault'),
(375, 'Hotring', 'hotring', 140000, 'assault'),
(376, 'Mini', 'issi3', 18000, 'assault'),
(377, 'Jester3', 'jester3', 85000, 'assault'),
(378, 'Michelle', 'michelli', 41000, 'assault'),
(379, 'Tai', 'taipan', 220000, 'assault'),
(380, 'Tezeract', 'tezeract', 180000, 'assault'),
(381, 'Tyrant', 'tyrant', 220000, 'assault'),
(382, 'Impreza', 'ySbrImpS11', 100000, 'modded'),
(383, 'R35 Skyline', 'r35', 200000, 'sports'),
(384, 'Insurgent', 'insurgent3', 200000, 'modded'),
(385, 'Halftrack', 'halftrack', 200000, 'modded'),
(386, 'Tempa', 'tampa3', 200000, 'modded'),
(387, 'Technical', 'technical3', 200000, 'modded'),
(388, 'Tech2', 'technical2', 200000, 'modded'),
(389, 'Barrage', 'barrage', 200000, 'modded'),
(390, 'Boxville', 'boxville5', 200000, 'modded'),
(391, 'Road Glide', 'foxharley2', 3000, 'motorcycles'),
(392, 'Harley Bobber', 'foxharley1', 3000, 'motorcycles');

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicle_categories`
--

CREATE TABLE `old_vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicle_categories`
--

INSERT INTO `old_vehicle_categories` (`id`, `name`, `label`) VALUES
(1, 'compacts', 'Compacts'),
(2, 'coupes', 'Coupés'),
(3, 'sedans', 'Sedans'),
(4, 'sports', 'Sports'),
(5, 'sportsclassics', 'Sports Classics'),
(6, 'super', 'Super'),
(7, 'muscle', 'Muscle'),
(8, 'offroad', 'Off Road'),
(9, 'suvs', 'SUVs'),
(10, 'vans', 'Vans'),
(11, 'motorcycles', 'Motos'),
(12, 'work', 'Work'),
(13, 'doomsday', 'doomsday'),
(14, 'modded', 'Modded'),
(15, 'assault', 'assault');

-- --------------------------------------------------------

--
-- Table structure for table `outfits`
--

CREATE TABLE `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) NOT NULL COMMENT 'mp_m_freemode_01',
  `face` int(11) NOT NULL COMMENT '0',
  `face_text` int(11) NOT NULL COMMENT '0',
  `hair` int(11) NOT NULL COMMENT '0',
  `pants` int(11) NOT NULL COMMENT '0',
  `pants_text` int(11) NOT NULL COMMENT '0',
  `shoes` int(11) NOT NULL COMMENT '0',
  `shoes_text` int(11) NOT NULL COMMENT '0',
  `torso` int(11) NOT NULL COMMENT '0',
  `torso_text` int(11) NOT NULL COMMENT '0',
  `shirt` int(11) NOT NULL COMMENT '0',
  `shirt_text` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `vehicle` longtext NOT NULL,
  `owner` varchar(60) NOT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the car',
  `garage_name` varchar(50) NOT NULL DEFAULT 'Garage_Centre',
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) NOT NULL DEFAULT 'car',
  `plate` varchar(50) NOT NULL,
  `type` varchar(10) NOT NULL DEFAULT 'car',
  `job` varchar(50) DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT 0,
  `jamstate` int(11) NOT NULL DEFAULT 0,
  `finance` int(32) NOT NULL DEFAULT 0,
  `financetimer` int(32) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`vehicle`, `owner`, `stored`, `garage_name`, `fourrieremecano`, `vehiclename`, `plate`, `type`, `job`, `state`, `jamstate`, `finance`, `financetimer`) VALUES
('{\"modSideSkirt\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modBackWheels\":-1,\"modStruts\":-1,\"health\":1000,\"modAPlate\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"modGrille\":-1,\"modXenon\":false,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modTank\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"pearlescentColor\":36,\"dirtLevel\":9.3065328598022,\"modLivery\":-1,\"model\":-133970931,\"color1\":27,\"wheelColor\":111,\"modSpoilers\":-1,\"modAirFilter\":-1,\"modRightFender\":-1,\"plate\":\"04YDL575\",\"modHorns\":-1,\"modVanityPlate\":-1,\"color2\":0,\"modEngine\":-1,\"extras\":[],\"modAerials\":-1,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"plateIndex\":0,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":6,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modFender\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDial\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'Tommies Bike', '04YDL575', 'car', NULL, 0, 0, 45540, 2847),
('{\"modSideSkirt\":-1,\"modHood\":-1,\"modFender\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"color2\":3,\"modAirFilter\":-1,\"modFrame\":-1,\"modSteeringWheel\":-1,\"color1\":3,\"modGrille\":-1,\"modArchCover\":-1,\"wheels\":7,\"modEngine\":-1,\"wheelColor\":156,\"modDoorSpeaker\":-1,\"health\":1000,\"modVanityPlate\":-1,\"modTurbo\":false,\"model\":234062309,\"modHydrolic\":-1,\"modSuspension\":-1,\"modBrakes\":-1,\"modLivery\":-1,\"modRearBumper\":-1,\"pearlescentColor\":5,\"modFrontWheels\":-1,\"modDashboard\":-1,\"modRightFender\":-1,\"modHorns\":-1,\"dirtLevel\":0.21620784699917,\"plateIndex\":0,\"modEngineBlock\":-1,\"modOrnaments\":-1,\"modTransmission\":-1,\"modSpoilers\":-1,\"modTrimA\":-1,\"modWindows\":-1,\"modShifterLeavers\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrimB\":-1,\"modSeats\":-1,\"neonColor\":[255,0,255],\"modRoof\":-1,\"modAerials\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modStruts\":-1,\"modBackWheels\":-1,\"modDial\":-1,\"extras\":[],\"modFrontBumper\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"windowTint\":-1,\"modAPlate\":-1,\"plate\":\"22OAJ106\",\"modSmokeEnabled\":1,\"modArmor\":-1}', 'steam:1100001068ef13c', 1, 'Garage_Centre', 0, 'car', '22OAJ106', 'car', NULL, 0, 0, 0, 0),
('{\"modSideSkirt\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modBackWheels\":-1,\"modStruts\":-1,\"health\":954,\"modAPlate\":-1,\"modSteeringWheel\":-1,\"modFrame\":-1,\"modPlateHolder\":-1,\"modGrille\":-1,\"modXenon\":false,\"neonColor\":[255,0,255],\"modTrimA\":-1,\"modTank\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"pearlescentColor\":0,\"dirtLevel\":3.3263566493988,\"modLivery\":-1,\"model\":-1624886479,\"color1\":6,\"wheelColor\":147,\"modSpoilers\":-1,\"modAirFilter\":-1,\"modRightFender\":-1,\"plate\":\"81OHZ637\",\"modHorns\":-1,\"modVanityPlate\":-1,\"color2\":27,\"modEngine\":-1,\"extras\":{\"1\":false},\"modAerials\":-1,\"modFrontWheels\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"plateIndex\":0,\"modTransmission\":-1,\"tyreSmokeColor\":[255,255,255],\"wheels\":1,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"modSuspension\":-1,\"modSmokeEnabled\":1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modFender\":-1,\"modBrakes\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDial\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'Hellcat', '81OHZ637', 'car', NULL, 0, 0, 0, 0),
('{\"modSideSkirt\":-1,\"windowTint\":-1,\"neonEnabled\":[false,false,false,false],\"modBackWheels\":-1,\"modStruts\":-1,\"health\":1000,\"modAPlate\":-1,\"dirtLevel\":15.0,\"modFrame\":-1,\"modPlateHolder\":-1,\"modHood\":-1,\"modXenon\":false,\"modGrille\":-1,\"modTrimA\":-1,\"modOrnaments\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modWindows\":-1,\"modTank\":-1,\"modSpeakers\":-1,\"modTrimB\":-1,\"pearlescentColor\":111,\"modFrontBumper\":-1,\"modLivery\":-1,\"model\":-133970931,\"color1\":6,\"wheelColor\":111,\"modSpoilers\":-1,\"modAirFilter\":-1,\"modSteeringWheel\":-1,\"plate\":\"83DBS050\",\"modHorns\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modEngine\":1,\"extras\":[],\"modAerials\":-1,\"modFrontWheels\":-1,\"modArchCover\":-1,\"color2\":18,\"plateIndex\":0,\"modTransmission\":-1,\"modShifterLeavers\":-1,\"modTrunk\":-1,\"modSeats\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"wheels\":6,\"modSmokeEnabled\":1,\"neonColor\":[255,0,255],\"modDoorSpeaker\":-1,\"modHydrolic\":-1,\"modExhaust\":-1,\"tyreSmokeColor\":[255,255,255],\"modFender\":-1,\"modBrakes\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modDial\":-1}', 'steam:11000010c2ebf86', 1, 'Garage_Centre', 0, 'car', '83DBS050', 'car', NULL, 0, 0, 0, 0),
('{\"modStruts\":-1,\"color2\":134,\"modSuspension\":-1,\"plate\":\"O-1\",\"modSteeringWheel\":-1,\"modSeats\":-1,\"modHorns\":-1,\"modGrille\":-1,\"modAPlate\":-1,\"modTank\":-1,\"modTurbo\":false,\"modHydrolic\":-1,\"modTrimB\":-1,\"modPlateHolder\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"modExhaust\":-1,\"modFrontBumper\":-1,\"modXenon\":false,\"modAerials\":-1,\"modArmor\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modTransmission\":-1,\"dirtLevel\":7.0,\"modRearBumper\":-1,\"modArchCover\":-1,\"modSpeakers\":-1,\"modFender\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"health\":1000,\"modWindows\":-1,\"modRightFender\":-1,\"neonEnabled\":[false,false,false,false],\"wheelColor\":134,\"modDoorSpeaker\":-1,\"neonColor\":[255,0,255],\"modFrontWheels\":-1,\"wheels\":3,\"modHood\":-1,\"tyreSmokeColor\":[255,255,255],\"plateIndex\":4,\"color1\":134,\"modDashboard\":-1,\"modSpoilers\":-1,\"modBrakes\":-1,\"model\":-576293386,\"modOrnaments\":-1,\"modShifterLeavers\":-1,\"extras\":{\"11\":false},\"modDial\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modLivery\":3,\"windowTint\":-1,\"modEngineBlock\":-1,\"modRoof\":-1,\"pearlescentColor\":134,\"modTrimA\":-1}', 'steam:11000010a01bdb9', 1, 'Garage_Centre', 0, 'car', 'O-1', 'car', 'police', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles_old`
--

CREATE TABLE `owned_vehicles_old` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `state of the car` tinyint(1) NOT NULL DEFAULT 0,
  `state` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'State of the car',
  `finance` int(32) NOT NULL DEFAULT 0,
  `financetimer` int(32) NOT NULL DEFAULT 0,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `stored` tinyint(1) NOT NULL DEFAULT 0,
  `jamstate` tinyint(11) NOT NULL DEFAULT 0,
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT 0,
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `owned_vehicles_old`
--

INSERT INTO `owned_vehicles_old` (`id`, `vehicle`, `owner`, `plate`, `state of the car`, `state`, `finance`, `financetimer`, `type`, `job`, `stored`, `jamstate`, `fourrieremecano`, `vehiclename`) VALUES
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '02JWM791', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010c2ebf86', '06VHJ277', 0, 1, 64350, 2143, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000112969e8f', '08JTP908', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000011a2fc3d0', '08RQZ688', 0, 1, 64350, 2850, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '24AAY727', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', '25OHR247', 0, 1, 0, 0, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010a01bdb9', '29FJK237', 0, 1, 148500, 2860, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:1100001068ef13c', '87QJY824', 0, 1, 22770, 2241, 'car', '0', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'DCD 740', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'E4 OFP', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'JYW 987', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'K9', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'NKJ 338', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010a01bdb9', 'O1', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:110000132580eb0', 'SEK 992', 0, 1, 0, 0, 'car', 'police', 0, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:11000010ddba29f', 'TUH 503', 0, 1, 0, 0, 'car', 'police', 1, 0, 0, 'voiture'),
(0, '{\"extras\":[],\"pearlescentColor\":5,\"modRearBumper\":-1,\"windowTint\":1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color2\":75,\"modBackWheels\":-1,\"modExhaust\":-1,\"modPlateHolder\":-1,\"modRoof\":-1,\"modAPlate\":-1,\"modTurbo\":1,\"modDoorSpeaker\":-1,\"neonColor\":[0,0,255],\"modDashboard\":-1,\"modBrakes\":2,\"modEngineBlock\":-1,\"wheels\":7,\"neonEnabled\":[1,1,1,1],\"tyreSmokeColor\":[0,0,255],\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"color1\":140,\"modRightFender\":-1,\"modHorns\":-1,\"plate\":\"08JTP908\",\"modFrontWheels\":-1,\"modEngine\":3,\"modTransmission\":2,\"modTrunk\":-1,\"modArmor\":4,\"modHood\":-1,\"modGrille\":-1,\"modSmokeEnabled\":1,\"modWindows\":-1,\"modDial\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSeats\":-1,\"modFrame\":-1,\"modAerials\":-1,\"plateIndex\":0,\"wheelColor\":156,\"modSpoilers\":2,\"modFender\":-1,\"modHydrolic\":-1,\"modAirFilter\":-1,\"modSideSkirt\":-1,\"modXenon\":1,\"model\":234062309,\"modSuspension\":2,\"dirtLevel\":8.8817024230957,\"modTrimA\":-1,\"modLivery\":-1,\"health\":998}', 'steam:1100001068ef13c', 'TYH 041', 0, 1, 0, 0, 'car', 'ambulance', 1, 0, 0, 'voiture');

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_app_chat`
--

INSERT INTO `phone_app_chat` (`id`, `channel`, `message`, `time`) VALUES
(1, 'packopills', 'Selling a pack of pills', '2019-11-17 06:26:31'),
(2, 'packopills', 'Hey', '2019-11-17 06:29:47'),
(3, 'packopills', 'I\'m buying', '2019-11-17 06:29:49'),
(4, 'packopills', 'Okay how much', '2019-11-17 06:30:32'),
(5, 'packopills', 'Its Mostly Fentnayl', '2019-11-17 06:30:56'),
(6, 'packopills', 'We will take nothing below 500G', '2019-11-17 06:31:45'),
(7, 'packopills', '*it can be fake*', '2019-11-17 06:32:09'),
(8, 'packopills', 'How much in total do you guys have?', '2019-11-17 06:32:48'),
(9, 'packopills', 'about 500 Pills', '2019-11-17 06:33:23'),
(10, 'packopills', 'How much you want for them?', '2019-11-17 06:33:38'),
(11, 'packopills', 'We will take nothhing below 500 KK', '2019-11-17 06:33:57'),
(12, 'packopills', '500K', '2019-11-17 06:34:03'),
(13, 'packopills', 'I can arrange 500K for 500 Pills', '2019-11-17 06:34:35'),
(14, 'packopills', 'Where do you want to meet', '2019-11-17 06:34:53'),
(15, 'packopills', '3037 ASAP', '2019-11-17 06:36:52'),
(16, 'packopills', 'I\'ll be there in an hour. Wait.', '2019-11-17 06:37:27'),
(17, 'packopills', 'omw', '2019-11-17 06:38:59'),
(18, 'packopills', 'Make sure your not being followed', '2019-11-17 06:40:18'),
(19, 'packopills', 'K', '2019-11-17 06:40:35'),
(20, 'packopills', '...', '2019-11-17 06:40:50'),
(21, 'packopills', 'Here', '2019-11-17 06:44:19'),
(22, 'packopills', 'Be there soon', '2019-11-17 06:44:35'),
(23, 'packopills', 'Here', '2019-11-17 07:03:34'),
(24, 'packopills', 'In the hangar', '2019-11-17 07:03:37');

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(1, '273-8087', '256-8589', 1, '2019-11-17 19:50:17', 0),
(2, '256-8589', '273-8087', 0, '2019-11-17 19:50:17', 0),
(3, '840-7465', '2738087', 1, '2019-11-17 21:39:45', 0),
(4, '273-8087', '840-7465', 1, '2019-11-17 21:39:51', 1),
(5, '840-7465', '273-8087', 0, '2019-11-17 21:39:51', 1),
(6, '273-8087', '840-7465', 1, '2019-11-17 22:52:06', 0),
(7, '840-7465', '273-8087', 0, '2019-11-17 22:52:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_messages`
--

INSERT INTO `phone_messages` (`id`, `transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES
(140, 'police', '373-0149', 'GPS: -1366.0496826172, 56.300445556641', '2019-11-14 03:59:36', 1, 0),
(139, 'police', '373-0149', 'Alert (101-5635): Some shady prick is selling drugs on West Eclipse Blvd and ', '2019-11-14 03:59:36', 1, 0),
(138, 'police', '373-0149', 'GPS: -1172.3690185547, -1572.2580566406', '2019-11-14 03:46:01', 1, 0),
(137, 'police', '373-0149', 'Alert (101-5635): Some shady prick is selling drugs on Goma St and Melanoma St', '2019-11-14 03:46:01', 1, 0),
(136, 'police', '373-0149', 'GPS: -1172.3690185547, -1572.2580566406', '2019-11-14 03:45:55', 1, 0),
(135, 'police', '373-0149', 'Alert (101-5635): Some shady prick is selling drugs on Goma St and Melanoma St', '2019-11-14 03:45:55', 1, 0),
(141, '273-8087', '739-5135', 'Hi', '2019-11-17 04:59:34', 0, 0),
(142, '739-5135', '273-8087', 'Hi', '2019-11-17 04:59:34', 1, 1),
(143, '451-8959', '273-8087', 'Hi', '2019-11-17 08:07:43', 1, 1),
(182, '840-7465', '256-8589', 'Wyd', '2019-11-17 22:54:55', 1, 0),
(146, '840-7465', '273-8087', 'HHi', '2019-11-17 22:52:14', 1, 1),
(148, '840-7465', '273-8087', 'your a cunt', '2019-11-17 22:52:17', 1, 1),
(181, '256-8589', '840-7465', 'Hey', '2019-11-17 22:54:53', 1, 1),
(180, '840-7465', '256-8589', 'Hey', '2019-11-17 22:54:53', 1, 0),
(149, '840-7465', '273-8087', 'I know you are', '2019-11-17 22:52:23', 1, 0),
(179, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:50', 1, 1),
(152, '840-7465', '273-8087', 'Hey', '2019-11-17 22:53:41', 1, 0),
(178, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:49', 1, 0),
(155, '840-7465', '273-8087', 'Spam Rayy', '2019-11-17 22:53:59', 1, 1),
(177, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:49', 1, 1),
(157, '840-7465', '273-8087', '256-8589', '2019-11-17 22:54:22', 1, 1),
(176, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:49', 1, 0),
(158, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:43', 1, 0),
(159, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:44', 1, 1),
(160, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:44', 1, 0),
(161, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:44', 1, 1),
(162, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:44', 1, 0),
(163, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:44', 1, 1),
(164, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:45', 1, 0),
(165, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:45', 1, 1),
(166, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:45', 1, 0),
(167, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:45', 1, 1),
(168, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:46', 1, 0),
(169, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:46', 1, 1),
(170, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:47', 1, 0),
(171, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:47', 1, 1),
(172, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:47', 1, 0),
(173, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:47', 1, 1),
(174, '273-8087', '256-8589', 'Wake upp', '2019-11-17 22:54:48', 1, 0),
(175, '256-8589', '273-8087', 'Wake upp', '2019-11-17 22:54:48', 1, 1),
(183, '256-8589', '840-7465', 'Wyd', '2019-11-17 22:54:55', 1, 1),
(184, '840-7465', '256-8589', 'Where youa t', '2019-11-17 22:54:56', 1, 0),
(185, '256-8589', '840-7465', 'Where youa t', '2019-11-17 22:54:57', 1, 1),
(186, '840-7465', '256-8589', 'Come here', '2019-11-17 22:54:59', 1, 0),
(187, '256-8589', '840-7465', 'Come here', '2019-11-17 22:55:00', 1, 1),
(188, '840-7465', '256-8589', 'Lol', '2019-11-17 22:55:00', 1, 0),
(189, '256-8589', '840-7465', 'Lol', '2019-11-17 22:55:00', 1, 1),
(190, '840-7465', '256-8589', 'Scrub', '2019-11-17 22:55:02', 1, 0),
(191, '256-8589', '840-7465', 'Scrub', '2019-11-17 22:55:02', 1, 1),
(192, '840-7465', '256-8589', 'IDK What you mean', '2019-11-17 22:55:06', 1, 0),
(193, '256-8589', '840-7465', 'IDK What you mean', '2019-11-17 22:55:06', 1, 1),
(194, '840-7465', '256-8589', 'I\'m not doing anythi9ng', '2019-11-17 22:55:09', 1, 0),
(195, '256-8589', '840-7465', 'I\'m not doing anythi9ng', '2019-11-17 22:55:09', 1, 1),
(196, '840-7465', '256-8589', 'I\'m not spamming you', '2019-11-17 22:55:17', 1, 0),
(197, '256-8589', '840-7465', 'I\'m not spamming you', '2019-11-17 22:55:17', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_users_contacts`
--

INSERT INTO `phone_users_contacts` (`id`, `identifier`, `number`, `display`) VALUES
(9, 'steam:110000132580eb0', '415-8959', 'Eric CPD'),
(10, 'steam:11000010ddba29f', '101-5635', 'k9'),
(11, 'steam:11000010a01bdb9', '415-8959', 'Donut Man'),
(12, 'steam:11000010ddba29f', '626-4798', 'Sticky'),
(13, 'steam:1100001068ef13c', '739-5135', 'Micheal'),
(14, 'steam:1100001068ef13c', '256-8589', 'Robert (noss)'),
(15, 'steam:11000010a01bdb9', '256-8589 ', 'Nos'),
(16, 'steam:1100001068ef13c', '451-8959', 'Eric'),
(17, 'steam:1100001068ef13c', '840-7465', 'Grant'),
(18, 'steam:1100001068ef13c', '373-0149', 'Joshua'),
(19, 'steam:110000132580eb0', '273-8087', 'Devil'),
(20, 'steam:110000132580eb0', '840-7465', 'Super'),
(22, 'steam:110000112969e8f', '273-8087', 'Devil'),
(23, 'steam:110000112969e8f', '256-8589', 'Ray');

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `steam` varchar(50) NOT NULL,
  `playtime` int(11) NOT NULL,
  `firstjoined` varchar(50) NOT NULL,
  `lastplayed` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Apartment de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modyrn1Apartment', 'Apartment Modern 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modyrn2Apartment', 'Apartment Modern 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modyrn3Apartment', 'Apartment Modern 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Apartment Mody 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Apartment Mody 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Apartment Mody 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Apartment Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Apartment Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Apartment Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Apartment Persian 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Apartment Persian 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Apartment Persian 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Apartment Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Apartment Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Apartment Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Apartment Seductive 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Apartment Seductive 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Apartment Seductive 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Apartment Regal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Apartment Regal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Apartment Regal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Apartment Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Apartment Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Apartment Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `received_bans`
--

CREATE TABLE `received_bans` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `banned_by` varchar(255) DEFAULT NULL,
  `banned_on` varchar(255) DEFAULT NULL,
  `ban_expires` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `ID` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `connection` int(11) NOT NULL,
  `rcon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `server_actions`
--

CREATE TABLE `server_actions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_do` varchar(255) DEFAULT NULL,
  `action_ammount` varchar(255) DEFAULT NULL,
  `byadmin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'PDShop', 'coffee', 1),
(2, 'PDShop', 'donut', 1),
(3, 'PDShop', 'clip', 1),
(4, 'PDShop', 'armor', 1),
(5, 'PDShop', 'medikit', 0),
(6, 'CityHall', 'weapons_license1', 100),
(7, 'CityHall', 'weapons_license2', 200),
(8, 'CityHall', 'weapons_license3', 300),
(62, 'CityHall', 'hunting_license', 100),
(63, 'CityHall', 'fishing_license', 100),
(64, 'CityHall', 'diving_license', 50),
(65, 'CityHall', 'marriage_license', 5000),
(69, 'DMV', 'boating_license', 40),
(70, 'DMV', 'taxi_license', 175),
(71, 'DMV', 'pilot_license', 500),
(72, 'PDShop', 'radio', 0),
(73, 'DMV', 'licenseplate', 50);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trunk_inventory`
--

INSERT INTO `trunk_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(7, 'DCD 740 ', '{\"coffre\":[{\"count\":6,\"name\":\"medikit\"},{\"count\":4,\"name\":\"clip\"}]}', 1),
(9, 'SEK 992 ', '{\"coffre\":[{\"count\":4,\"name\":\"medikit\"},{\"count\":6,\"name\":\"clip\"}]}', 1),
(10, '06VHJ277', '{\"weapons\":[]}', 1),
(16, '81OHZ637', '{}', 1),
(18, '83DBS050', '{}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(39, 'tpickles', 'stoner420', 'kermit.png'),
(40, 'CCPDChief', 'CCPD37', NULL),
(41, 'HHMC K9', '123456', NULL),
(42, 'Super', 'Sadie0508', 'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwiM5Y2gtbPlAhUFFzQIHfjzCj0QjRx6BAgBEAQ&url=https%3A%2F%2Fimgur.com%2Fgallery%2Fn4BAiU4&psig=AOvVaw37Ry-1QIi6eye82EcpQl2S&ust=1571955644077203'),
(43, 'Robert_Kerr', 'Dennis1993', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_likes`
--

INSERT INTO `twitter_likes` (`id`, `authorId`, `tweetId`) VALUES
(6, 42, 170),
(7, 42, 173),
(8, 42, 172),
(9, 42, 171),
(10, 42, 174),
(11, 42, 175),
(12, 42, 176),
(13, 42, 177);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 39, 'steam:11000010a01bdb9', 'New city who dis?', '2019-08-09 08:24:48', 0),
(171, 42, 'steam:110000112969e8f', 'Heyyyyyy', '2019-10-23 22:19:15', 1),
(172, 43, 'steam:11000010c2ebf86', '@super I\'m coming for you, watch out!', '2019-10-23 22:19:32', 1),
(173, 43, 'steam:11000010c2ebf86', 'hahah @super can\'t even access his own office #loser', '2019-10-23 22:38:42', 1),
(174, 43, 'steam:11000010c2ebf86', '#devilbrokeit', '2019-10-23 23:19:27', 1),
(175, 42, 'steam:110000112969e8f', 'We\'re live streaming!', '2019-10-25 01:32:56', 1),
(176, 39, 'steam:11000010a01bdb9', 'Erics Driving is #horrible', '2019-10-25 01:33:04', 1),
(177, 40, 'steam:11000010ddba29f', '@tpickles wash my truck', '2019-10-25 01:33:48', 1),
(178, 42, 'steam:110000112969e8f', 'Y\'all here about that Shootout in Sandy?! Crazy!!!!!', '2019-11-17 07:13:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `rank` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `steamid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeplayed` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `online` int(10) NOT NULL DEFAULT 0,
  `server` int(10) NOT NULL DEFAULT 1,
  `is_dead` tinyint(1) DEFAULT 0,
  `DmvTest` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'Required',
  `lastpos` varchar(255) COLLATE utf8mb4_bin DEFAULT '{706.98700000000, -964.94200000000,  31.39550000000000, 142.503463745117}'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `ID`, `rank`, `steamid`, `license`, `money`, `name`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `last_property`, `animal`, `timeplayed`, `online`, `server`, `is_dead`, `DmvTest`, `lastpos`) VALUES
('steam:11000010ddba29f', 3, '', '', 'license:50280a9855e0a77982745eecb274fb074ce3bd7e', 38502, 'EONeillYT', 'police', 11, '[{\"ammo\":0,\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[]},{\"ammo\":950,\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\",\"flashlight\"]},{\"ammo\":1055,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[\"flashlight\"]},{\"ammo\":146,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\",\"flashlight\"]},{\"ammo\":1619,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[]},{\"ammo\":1000,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[]},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]},{\"ammo\":25,\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[]}]', '{\"z\":0.0,\"x\":0.0,\"y\":0.0}', 4931500, 5, 'admin', '[{\"percent\":43.485,\"name\":\"hunger\",\"val\":434850},{\"percent\":43.485,\"name\":\"thirst\",\"val\":434850},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Eric', 'O\'neill', '06/28/1995', 'M', '72', '415-8959', NULL, NULL, '0', 0, 1, 0, 'Required', '{446.79895019531, -972.76745605469,  31.225910186768, 77.834075927734}'),
('steam:110000112969e8f', 5, '', '', 'license:fc9506319293ea778454d0078ba9a36bf826a0f6', 313925, 'SuperSteve902', 'police', 40, '[{\"ammo\":0,\"label\":\"Nightstick\",\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\"},{\"ammo\":1158,\"label\":\"Combat pistol\",\"components\":[\"clip_default\"],\"name\":\"WEAPON_COMBATPISTOL\"},{\"ammo\":1161,\"label\":\"Pump shotgun\",\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":1107,\"label\":\"Special carbine\",\"components\":[\"clip_default\"],\"name\":\"WEAPON_SPECIALCARBINE\"},{\"ammo\":2000,\"label\":\"Fire extinguisher\",\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"ammo\":4500,\"label\":\"Jerrycan\",\"components\":[],\"name\":\"WEAPON_PETROLCAN\"},{\"ammo\":84,\"label\":\"Taser\",\"components\":[],\"name\":\"WEAPON_STUNGUN\"},{\"ammo\":0,\"label\":\"Flashlight\",\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\"}]', '{\"z\":0.0,\"x\":0.0,\"y\":0.0}', 77223, 5, 'admin', '[{\"percent\":44.4,\"name\":\"hunger\",\"val\":444000},{\"percent\":44.4,\"name\":\"thirst\",\"val\":444000},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Steven', 'Super', '05/08/2003', 'M', '90', '840-7465', NULL, NULL, '0', 0, 1, 0, 'Required', '{221.61865234375, -804.04992675781,  30.685327529907, 150.50924682617}'),
('steam:1100001068ef13c', 6, '', '', 'license:a4979e4221783962685bb8a6105e2b93fc364e77', 200, 'Soft-Hearted Devil', 'lumberjack', 0, '[]', '{\"x\":0.0,\"y\":0.0,\"z\":0.0}', 83155, 5, 'admin', '[{\"name\":\"hunger\",\"val\":443675,\"percent\":44.3675},{\"name\":\"thirst\",\"val\":443675,\"percent\":44.3675},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', 'William', 'Woodard', '7/27/1989', 'M', '76', '273-8087', NULL, NULL, '0', 0, 1, 0, 'Required', '{214.84449768066, -799.98394775391,  30.827157974243, 243.32389831543}'),
('steam:11000010c2ebf86', 7, '', '', 'license:58dec6dbc91fa2da51ac590892604b46a231610a', 0, 'Noss', 'mechanic', 0, '[]', '{\"x\":0.0,\"y\":0.0,\"z\":0.0}', 63216, 0, 'user', '[{\"val\":455050,\"percent\":45.505,\"name\":\"hunger\"},{\"val\":455050,\"percent\":45.505,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'Robert', 'Kerr', '10/23/1995', 'M', '72', '256-8589', NULL, NULL, '0', 0, 1, 0, 'Required', '{281.3317565918, -877.38104248047,  28.74880027771, 341.70068359375}'),
('steam:11000010a078bc7', 8, '', '', 'license:00eb03a770b9661e961de7dd80810b9a2656c8cd', 496, '414 - Michael G.', 'fire', 12, '[{\"ammo\":2,\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"]},{\"ammo\":10,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[]},{\"ammo\":30,\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"]},{\"ammo\":1000,\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[]},{\"ammo\":20,\"label\":\"Flaregun\",\"name\":\"WEAPON_FLAREGUN\",\"components\":[]},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]},{\"ammo\":25,\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[]}]', '{\"z\":0.0,\"x\":0.0,\"y\":0.0}', 39400, 5, 'admin', '[{\"percent\":37.6,\"name\":\"hunger\",\"val\":376000},{\"percent\":37.6,\"name\":\"thirst\",\"val\":376000},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Roy', 'Rogers', '07/04/2000', 'M', '64', '739-5135', NULL, NULL, '0', 0, 1, 0, 'Required', '{1201.6729736328, -1462.533203125,  34.834926605225, 40.031719207764}'),
('steam:110000132580eb0', 15, '', '', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 763, 'K9Marine', 'ambulance', 2, '[{\"ammo\":88,\"components\":[\"clip_default\"],\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\"},{\"ammo\":0,\"components\":[\"clip_default\"],\"name\":\"WEAPON_ASSAULTSMG\",\"label\":\"Assault SMG\"},{\"ammo\":130,\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\"},{\"ammo\":0,\"components\":[\"clip_default\"],\"name\":\"WEAPON_SPECIALCARBINE\",\"label\":\"Special carbine\"},{\"ammo\":168,\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\",\"label\":\"Fire extinguisher\"},{\"ammo\":4500,\"components\":[],\"name\":\"WEAPON_PETROLCAN\",\"label\":\"Jerrycan\"},{\"ammo\":168,\"components\":[],\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\"},{\"ammo\":0,\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\",\"label\":\"Flashlight\"}]', '{\"z\":0.0,\"x\":0.0,\"y\":0.0}', 5382096, 9, 'superadmin', '[{\"percent\":49.895,\"name\":\"hunger\",\"val\":498950},{\"percent\":49.895,\"name\":\"thirst\",\"val\":498950},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Jak', 'Fulton', '10-10-1988', 'M', '74', '373-0149', NULL, NULL, '0', 0, 1, 0, 'Required', '{462.07943725586, -3052.7653808594,  5.7911028862, 159.64198303223}'),
('steam:11000010a01bdb9', 22, '', '', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 2743389, 'stickybombz', 'police', 37, '[{\"ammo\":0,\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[]},{\"ammo\":1074,\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_extended\",\"flashlight\"]},{\"ammo\":134,\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[]},{\"ammo\":84,\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"]},{\"ammo\":1084,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[]},{\"ammo\":0,\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[]}]', '{\"x\":0.0,\"y\":0.0,\"z\":0.0}', 90000, 9, 'superadmin', '[{\"val\":733475,\"percent\":73.3475,\"name\":\"hunger\"},{\"val\":733300,\"percent\":73.33,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', '101-5635', NULL, NULL, '0', 0, 1, 0, 'Required', '{448.01538085938, -1020.7680053711,  28.464508056641, 239.94046020508}'),
('steam:1100001048af48f', 23, '', '', 'license:153aa3e5d9507effcabab9417a1d004ac511b461', 500, 'VaderSanchez', 'unemployed', 0, '[]', '{\"z\":0.0,\"y\":0.0,\"x\":0.0}', 2500, 0, 'user', '[{\"name\":\"hunger\",\"percent\":99.6275,\"val\":996275},{\"name\":\"thirst\",\"percent\":99.6275,\"val\":996275},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', '', '', '', '', '', '626-4798', NULL, NULL, '0', 0, 1, 0, 'Required', '{706.23461914063, -964.37860107422,  30.408271789551, 318.11389160156}'),
('steam:11000013bc17e0e', 24, '', '', 'license:ca6024211ff0ae80db75aae57447e9ae5424aa8b', 500, 'Police Los Angeles', 'unemployed', 0, '[]', '{\"z\":0.0,\"y\":0.0,\"x\":0.0}', 2500, 0, 'user', '[{\"name\":\"hunger\",\"percent\":99.825,\"val\":998250},{\"name\":\"thirst\",\"percent\":99.825,\"val\":998250},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', 'Zahar', 'Artonov', 'Asdadd', 'M', '50', '182-3644', NULL, NULL, '0', 0, 1, 0, 'Required', '{706.84112548828, -961.77294921875,  30.395341873169, 302.517578125}'),
('steam:1100001079cf4ab', 25, '', '', 'license:a179630c24d11ca0c145594e3a475486473db24e', 500, 'yankee930', 'unemployed', 0, '[]', '{\"z\":0.0,\"y\":0.0,\"x\":0.0}', 2500, 0, 'user', '[{\"name\":\"hunger\",\"val\":998250,\"percent\":99.825},{\"name\":\"thirst\",\"val\":998250,\"percent\":99.825},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', 'Kane', 'Brown', '9-3-06', 'M', '96', '232-1658', NULL, NULL, '0', 0, 1, 0, 'Required', '{707.84930419922, -961.34405517578,  30.39533996582, 250.92517089844}'),
('steam:11000013b85ca5f', 26, '', '', 'license:e4e17ebd082256997566b01a196bcd73c5d640d3', 500, 'Abortion Completus', 'security', 0, '[{\"ammo\":10,\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\",\"components\":[]},{\"ammo\":60,\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"components\":[\"clip_default\"]}]', '{\"z\":0.0,\"x\":0.0,\"y\":0.0}', 5000, 0, 'user', '[{\"percent\":49.975,\"name\":\"hunger\",\"val\":499750},{\"percent\":49.975,\"name\":\"thirst\",\"val\":499750},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', 'Felipe', 'Gomez', '05/151995', 'M', '6', '989-4456', NULL, NULL, '0', 0, 1, 0, 'Required', '{471.30603027344, -3048.9084472656,  5.2584004402161, 272.59246826172}');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(1, 'steam:110000132580eb0', 'black_money', 0),
(2, 'steam:11000010a01bdb9', 'black_money', 0),
(3, 'steam:11000010ddba29f', 'black_money', 5000),
(4, 'steam:110000112969e8f', 'black_money', 0),
(5, 'steam:1100001068ef13c', 'black_money', 0),
(6, 'steam:11000010c2ebf86', 'black_money', 0),
(7, 'steam:11000010a078bc7', 'black_money', 0),
(8, 'steam:11000013d51c2ff', 'black_money', 0),
(9, 'steam:11000011a2fc3d0', 'black_money', 0),
(10, 'steam:1100001048af48f', 'black_money', 0),
(11, 'steam:11000013bc17e0e', 'black_money', 0),
(12, 'steam:1100001079cf4ab', 'black_money', 0),
(13, 'steam:11000013b85ca5f', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin_notes`
--

CREATE TABLE `user_admin_notes` (
  `id` int(11) NOT NULL,
  `note` longblob DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `note_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_documents`
--

INSERT INTO `user_documents` (`id`, `owner`, `data`) VALUES
(4, 'steam:110000132580eb0', '{\"headerJobLabel\":\"AMR\",\"headerSubtitle\":\"Official document when person refuses treatment by Medical Personal. Must be given to citizen by Medical Personnel\",\"headerDateCreated\":\"04/11/2019 22:16:5\",\"headerJobGrade\":\"Medical Director\",\"headerLastName\":\"Pickles\",\"submittable\":true,\"headerFirstName\":\"Tommie\",\"headerDateOfBirth\":\"12/28/1988\",\"headerTitle\":\"REFUSAL OF MEDICAL TREATMENT\",\"elements\":[{\"label\":\"INJURED FIRSTNAME\",\"value\":\"Jak \",\"type\":\"input\",\"elementid\":\"_m0\"},{\"label\":\"INJURED LASTNAME\",\"value\":\"Fulton\",\"type\":\"input\",\"elementid\":\"_m1\"},{\"label\":\"DATE\",\"value\":\"11/4/2019\",\"type\":\"input\",\"elementid\":\"_m2\",\"can_be_empty\":false},{\"label\":\"MEDICAL NOTES\",\"value\":\"THE SIGNED PATIENT REFUSED MEDICAL TREATMENT BY MEDICAL PERSONNEL. I HAVE ADVISED PATIENT TO SEEK MEDICAL ATTENTION AT EARLIEST CONVIENCE. TREATING PERSONNEL(INSERT NAME HERE)HAS FILED THIS DOCUMENT WITH THE DEPARTMENT AND GIVEN A COPY TO THE PATIENT\",\"can_be_edited\":true,\"elementid\":\"_m3\",\"type\":\"textarea\"},{\"label\":\"PATIENT INFORMATION\",\"value\":\"This left blank for a reason\",\"can_be_edited\":true,\"elementid\":\"_m4\",\"type\":\"textarea\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1001, 'steam:11000010a078bc7', 'narcan', 0),
(1002, 'steam:11000010a078bc7', 'WEAPON_FLASHLIGHT', 0),
(1003, 'steam:11000010a078bc7', 'sprite', 0),
(1004, 'steam:11000010a078bc7', 'WEAPON_BAT', 0),
(1005, 'steam:11000010a078bc7', 'dabs', 0),
(1006, 'steam:11000010a078bc7', 'bandage', 5),
(1007, 'steam:11000010a078bc7', 'taxi_license', 0),
(1008, 'steam:11000010a078bc7', 'lotteryticket', 0),
(1009, 'steam:11000010a078bc7', 'yusuf', 0),
(1010, 'steam:11000010a078bc7', 'saucisson', 0),
(1011, 'steam:11000010a078bc7', 'bolcacahuetes', 0),
(1012, 'steam:11000010a078bc7', 'weapons_license2', 0),
(1013, 'steam:11000010a078bc7', 'teqpaf', 0),
(1014, 'steam:11000010a078bc7', 'tequila', 0),
(1015, 'steam:11000010a078bc7', 'vodkaenergy', 0),
(1016, 'steam:11000010a078bc7', 'flashlight', 0),
(1017, 'steam:11000010a078bc7', 'mojito', 0),
(1018, 'steam:11000010a078bc7', 'coke', 0),
(1019, 'steam:11000010a078bc7', 'fixkit', 0),
(1020, 'steam:11000010a078bc7', 'whisky', 0),
(1021, 'steam:11000010a078bc7', 'meth', 0),
(1022, 'steam:11000010a078bc7', 'WEAPON_STUNGUN', 0),
(1023, 'steam:11000010a078bc7', 'breathalyzer', 0),
(1024, 'steam:11000010a078bc7', 'commercial_license', 0),
(1025, 'steam:11000010a078bc7', 'pizza', 0),
(1026, 'steam:11000010a078bc7', 'coke_pooch', 0),
(1027, 'steam:11000010a078bc7', 'chips', 0),
(1028, 'steam:11000010a078bc7', 'fakepee', 0),
(1029, 'steam:11000010a078bc7', 'boating_license', 0),
(1030, 'steam:11000010a078bc7', 'packaged_chicken', 0),
(1031, 'steam:11000010a078bc7', 'donut', 0),
(1032, 'steam:11000010a078bc7', 'loka', 0),
(1033, 'steam:11000010a078bc7', 'leather', 0),
(1034, 'steam:11000010a078bc7', 'slaughtered_chicken', 0),
(1035, 'steam:11000010a078bc7', 'burger', 0),
(1036, 'steam:11000010a078bc7', 'icetea', 0),
(1037, 'steam:11000010a078bc7', 'gazbottle', 0),
(1038, 'steam:11000010a078bc7', 'drpepper', 0),
(1039, 'steam:11000010a078bc7', 'martini', 0),
(1040, 'steam:11000010a078bc7', 'mixapero', 0),
(1041, 'steam:11000010a078bc7', 'whool', 0),
(1042, 'steam:11000010a078bc7', 'meat', 0),
(1043, 'steam:11000010a078bc7', 'diving_license', 0),
(1044, 'steam:11000010a078bc7', 'bolchips', 0),
(1045, 'steam:11000010a078bc7', 'lsd', 0),
(1046, 'steam:11000010a078bc7', 'lighter', 0),
(1047, 'steam:11000010a078bc7', 'cola', 0),
(1048, 'steam:11000010a078bc7', 'menthe', 0),
(1049, 'steam:11000010a078bc7', 'petrol_raffin', 0),
(1050, 'steam:11000010a078bc7', 'ephedrine', 0),
(1051, 'steam:11000010a078bc7', 'wood', 0),
(1052, 'steam:11000010a078bc7', 'blowpipe', 0),
(1053, 'steam:11000010a078bc7', 'bread', 0),
(1054, 'steam:11000010a078bc7', 'weapons_license1', 0),
(1055, 'steam:11000010a078bc7', 'litter', 0),
(1056, 'steam:11000010a078bc7', 'marijuana', 0),
(1057, 'steam:11000010a078bc7', 'shotgun_shells', 0),
(1058, 'steam:11000010a078bc7', 'energy', 0),
(1059, 'steam:11000010a078bc7', 'carokit', 0),
(1060, 'steam:11000010a078bc7', 'stone', 0),
(1061, 'steam:11000010a078bc7', 'jagerbomb', 0),
(1062, 'steam:11000010a078bc7', 'rhum', 0),
(1063, 'steam:11000010a078bc7', 'jager', 0),
(1064, 'steam:11000010a078bc7', 'cutted_wood', 0),
(1065, 'steam:11000010a078bc7', 'drugtest', 0),
(1066, 'steam:11000010a078bc7', 'WEAPON_PUMPSHOTGUN', 0),
(1067, 'steam:11000010a078bc7', 'pastacarbonara', 0),
(1068, 'steam:11000010a078bc7', 'grapperaisin', 0),
(1069, 'steam:11000010a078bc7', 'grip', 0),
(1070, 'steam:11000010a078bc7', 'plongee2', 0),
(1071, 'steam:11000010a078bc7', 'WEAPON_PISTOL', 0),
(1072, 'steam:11000010a078bc7', '9mm_rounds', 0),
(1073, 'steam:11000010a078bc7', 'fishing_license', 0),
(1074, 'steam:11000010a078bc7', 'lockpick', 0),
(1075, 'steam:11000010a078bc7', 'rhumcoca', 0),
(1076, 'steam:11000010a078bc7', 'opium_pooch', 0),
(1077, 'steam:11000010a078bc7', 'defibrillateur', 0),
(1078, 'steam:11000010a078bc7', 'binoculars', 0),
(1079, 'steam:11000010a078bc7', 'fanta', 0),
(1080, 'steam:11000010a078bc7', 'wrench', 0),
(1081, 'steam:11000010a078bc7', 'carotool', 0),
(1082, 'steam:11000010a078bc7', 'drivers_license', 0),
(1083, 'steam:11000010a078bc7', 'firstaidpass', 0),
(1084, 'steam:11000010a078bc7', 'plongee1', 0),
(1085, 'steam:11000010a078bc7', 'litter_pooch', 0),
(1086, 'steam:11000010a078bc7', 'motorcycle_license', 0),
(1087, 'steam:11000010a078bc7', 'cocaine', 0),
(1088, 'steam:11000010a078bc7', 'pilot_license', 0),
(1089, 'steam:11000010a078bc7', 'coca', 0),
(1090, 'steam:11000010a078bc7', 'WEAPON_KNIFE', 0),
(1091, 'steam:11000010a078bc7', 'fish', 0),
(1092, 'steam:11000010a078bc7', 'marriage_license', 0),
(1093, 'steam:11000010a078bc7', 'clothe', 0),
(1094, 'steam:11000010a078bc7', 'rhumfruit', 0),
(1095, 'steam:11000010a078bc7', 'cheesebows', 0),
(1096, 'steam:11000010a078bc7', 'limonade', 0),
(1097, 'steam:11000010a078bc7', 'marabou', 0),
(1098, 'steam:11000010a078bc7', 'baconburger', 0),
(1099, 'steam:11000010a078bc7', 'poppy', 0),
(1100, 'steam:11000010a078bc7', 'radio', 1),
(1101, 'steam:11000010a078bc7', 'silencieux', 0),
(1102, 'steam:11000010a078bc7', 'weed_pooch', 0),
(1103, 'steam:11000010a078bc7', 'cannabis', 0),
(1104, 'steam:11000010a078bc7', 'coffee', 0),
(1105, 'steam:11000010a078bc7', 'painkiller', 0),
(1106, 'steam:11000010a078bc7', 'ephedra', 0),
(1107, 'steam:11000010a078bc7', 'copper', 0),
(1108, 'steam:11000010a078bc7', 'cocacola', 0),
(1109, 'steam:11000010a078bc7', 'boitier', 0),
(1110, 'steam:11000010a078bc7', 'weed', 0),
(1111, 'steam:11000010a078bc7', 'opium', 0),
(1112, 'steam:11000010a078bc7', 'bolnoixcajou', 0),
(1113, 'steam:11000010a078bc7', 'clip', 0),
(1114, 'steam:11000010a078bc7', 'whiskycoca', 0),
(1115, 'steam:11000010a078bc7', 'diamond', 0),
(1116, 'steam:11000010a078bc7', 'pcp', 0),
(1117, 'steam:11000010a078bc7', 'washed_stone', 0),
(1118, 'steam:11000010a078bc7', 'vodka', 0),
(1119, 'steam:11000010a078bc7', 'ice', 0),
(1120, 'steam:11000010a078bc7', 'vodkafruit', 0),
(1121, 'steam:11000010a078bc7', 'heroine', 0),
(1122, 'steam:11000010a078bc7', 'medikit', 55),
(1123, 'steam:11000010a078bc7', 'croquettes', 0),
(1124, 'steam:11000010a078bc7', 'crack', 0),
(1125, 'steam:11000010a078bc7', 'soda', 0),
(1126, 'steam:11000010a078bc7', 'gold', 0),
(1127, 'steam:11000010a078bc7', 'lsd_pooch', 0),
(1128, 'steam:11000010a078bc7', 'meth_pooch', 0),
(1129, 'steam:11000010a078bc7', 'fabric', 0),
(1130, 'steam:11000010a078bc7', 'packaged_plank', 0),
(1131, 'steam:11000010a078bc7', 'bolpistache', 0),
(1132, 'steam:11000010a078bc7', 'firstaidkit', 0),
(1133, 'steam:11000010a078bc7', 'whiskey', 0),
(1134, 'steam:11000010a078bc7', 'powerade', 0),
(1135, 'steam:11000010a078bc7', 'pills', 0),
(1136, 'steam:11000010a078bc7', 'jusfruit', 0),
(1137, 'steam:11000010a078bc7', 'protein_shake', 0),
(1138, 'steam:11000010a078bc7', 'sportlunch', 0),
(1139, 'steam:11000010a078bc7', 'armor', 0),
(1140, 'steam:11000010a078bc7', 'turtle', 0),
(1141, 'steam:11000010a078bc7', 'tacos', 0),
(1142, 'steam:11000010a078bc7', 'scratchoff_used', 0),
(1143, 'steam:11000010a078bc7', 'water', 0),
(1144, 'steam:11000010a078bc7', 'cigarett', 0),
(1145, 'steam:11000010a078bc7', 'gym_membership', 0),
(1146, 'steam:11000010a078bc7', 'petrol', 0),
(1147, 'steam:11000010a078bc7', 'fixtool', 0),
(1148, 'steam:11000010a078bc7', 'macka', 0),
(1149, 'steam:11000010a01bdb9', 'receipt', 0),
(1150, 'steam:110000132580eb0', 'receipt', 0),
(1151, 'steam:11000013d51c2ff', 'breathalyzer', 0),
(1152, 'steam:11000013d51c2ff', 'meat', 0),
(1153, 'steam:11000013d51c2ff', 'coffee', 0),
(1154, 'steam:11000013d51c2ff', 'mixapero', 0),
(1155, 'steam:11000013d51c2ff', 'firstaidpass', 0),
(1156, 'steam:11000013d51c2ff', 'alive_chicken', 0),
(1157, 'steam:11000013d51c2ff', 'sportlunch', 0),
(1158, 'steam:11000013d51c2ff', 'blackberry', 0),
(1159, 'steam:11000013d51c2ff', 'coke', 0),
(1160, 'steam:11000013d51c2ff', 'powerade', 0),
(1161, 'steam:11000013d51c2ff', 'contrat', 0),
(1162, 'steam:11000013d51c2ff', 'petrol', 0),
(1163, 'steam:11000013d51c2ff', 'opium', 0),
(1164, 'steam:11000013d51c2ff', 'mojito', 0),
(1165, 'steam:11000013d51c2ff', 'diving_license', 0),
(1166, 'steam:11000013d51c2ff', 'litter_pooch', 0),
(1167, 'steam:11000013d51c2ff', 'gym_membership', 0),
(1168, 'steam:11000013d51c2ff', 'scratchoff_used', 0),
(1169, 'steam:11000013d51c2ff', 'marijuana', 0),
(1170, 'steam:11000013d51c2ff', 'WEAPON_BAT', 0),
(1171, 'steam:11000013d51c2ff', 'turtle', 0),
(1172, 'steam:11000013d51c2ff', 'copper', 0),
(1173, 'steam:11000013d51c2ff', 'cannabis', 0),
(1174, 'steam:11000013d51c2ff', 'cheesebows', 0),
(1175, 'steam:11000013d51c2ff', 'jusfruit', 0),
(1176, 'steam:11000013d51c2ff', 'marabou', 0),
(1177, 'steam:11000013d51c2ff', 'whiskycoca', 0),
(1178, 'steam:11000013d51c2ff', 'baconburger', 0),
(1179, 'steam:11000013d51c2ff', 'grip', 0),
(1180, 'steam:11000013d51c2ff', 'hunting_license', 0),
(1181, 'steam:11000013d51c2ff', 'lsd_pooch', 0),
(1182, 'steam:11000013d51c2ff', 'coke_pooch', 0),
(1183, 'steam:11000013d51c2ff', 'iron', 0),
(1184, 'steam:11000013d51c2ff', 'sprite', 0),
(1185, 'steam:11000013d51c2ff', 'wood', 0),
(1186, 'steam:11000013d51c2ff', 'drpepper', 0),
(1187, 'steam:11000013d51c2ff', 'whool', 0),
(1188, 'steam:11000013d51c2ff', 'diamond', 0),
(1189, 'steam:11000013d51c2ff', 'coca', 0),
(1190, 'steam:11000013d51c2ff', 'vodkaenergy', 0),
(1191, 'steam:11000013d51c2ff', 'medikit', 0),
(1192, 'steam:11000013d51c2ff', 'packaged_chicken', 0),
(1193, 'steam:11000013d51c2ff', 'opium_pooch', 0),
(1194, 'steam:11000013d51c2ff', 'fish', 0),
(1195, 'steam:11000013d51c2ff', 'meth_pooch', 0),
(1196, 'steam:11000013d51c2ff', 'soda', 0),
(1197, 'steam:11000013d51c2ff', 'scratchoff', 0),
(1198, 'steam:11000013d51c2ff', 'cigarett', 0),
(1199, 'steam:11000013d51c2ff', 'bolchips', 0),
(1200, 'steam:11000013d51c2ff', 'beer', 0),
(1201, 'steam:11000013d51c2ff', 'weed_pooch', 0),
(1202, 'steam:11000013d51c2ff', 'vodka', 0),
(1203, 'steam:11000013d51c2ff', 'carotool', 0),
(1204, 'steam:11000013d51c2ff', 'washed_stone', 0),
(1205, 'steam:11000013d51c2ff', 'marriage_license', 0),
(1206, 'steam:11000013d51c2ff', 'donut', 0),
(1207, 'steam:11000013d51c2ff', 'pearl_pooch', 0),
(1208, 'steam:11000013d51c2ff', 'poppy', 0),
(1209, 'steam:11000013d51c2ff', 'whisky', 0),
(1210, 'steam:11000013d51c2ff', 'flashlight', 0),
(1211, 'steam:11000013d51c2ff', 'essence', 0),
(1212, 'steam:11000013d51c2ff', 'crack', 0),
(1213, 'steam:11000013d51c2ff', 'armor', 0),
(1214, 'steam:11000013d51c2ff', 'lotteryticket', 0),
(1215, 'steam:11000013d51c2ff', 'motorcycle_license', 0),
(1216, 'steam:11000013d51c2ff', 'water', 0),
(1217, 'steam:11000013d51c2ff', 'slaughtered_chicken', 0),
(1218, 'steam:11000013d51c2ff', 'fishing_license', 0),
(1219, 'steam:11000013d51c2ff', 'tequila', 0),
(1220, 'steam:11000013d51c2ff', 'ice', 0),
(1221, 'steam:11000013d51c2ff', 'yusuf', 0),
(1222, 'steam:11000013d51c2ff', 'tacos', 0),
(1223, 'steam:11000013d51c2ff', 'menthe', 0),
(1224, 'steam:11000013d51c2ff', 'carokit', 0),
(1225, 'steam:11000013d51c2ff', 'rhumfruit', 0),
(1226, 'steam:11000013d51c2ff', 'taxi_license', 0),
(1227, 'steam:11000013d51c2ff', 'lsd', 0),
(1228, 'steam:11000013d51c2ff', 'fakepee', 0),
(1229, 'steam:11000013d51c2ff', 'rhumcoca', 0),
(1230, 'steam:11000013d51c2ff', 'pearl', 0),
(1231, 'steam:11000013d51c2ff', 'jager', 0),
(1232, 'steam:11000013d51c2ff', 'energy', 0),
(1233, 'steam:11000013d51c2ff', 'weed', 0),
(1234, 'steam:11000013d51c2ff', 'martini', 0),
(1235, 'steam:11000013d51c2ff', 'lockpick', 0),
(1236, 'steam:11000013d51c2ff', 'shotgun_shells', 0),
(1237, 'steam:11000013d51c2ff', 'receipt', 0),
(1238, 'steam:11000013d51c2ff', '9mm_rounds', 0),
(1239, 'steam:11000013d51c2ff', 'bolnoixcajou', 0),
(1240, 'steam:11000013d51c2ff', 'narcan', 0),
(1241, 'steam:11000013d51c2ff', 'drugtest', 0),
(1242, 'steam:11000013d51c2ff', 'pilot_license', 0),
(1243, 'steam:11000013d51c2ff', 'boating_license', 0),
(1244, 'steam:11000013d51c2ff', 'WEAPON_FLASHLIGHT', 0),
(1245, 'steam:11000013d51c2ff', 'icetea', 0),
(1246, 'steam:11000013d51c2ff', 'radio', 0),
(1247, 'steam:11000013d51c2ff', 'defibrillateur', 0),
(1248, 'steam:11000013d51c2ff', 'firstaidkit', 0),
(1249, 'steam:11000013d51c2ff', 'wrench', 0),
(1250, 'steam:11000013d51c2ff', 'nitrocannister', 0),
(1251, 'steam:11000013d51c2ff', 'drivers_license', 0),
(1252, 'steam:11000013d51c2ff', 'stone', 0),
(1253, 'steam:11000013d51c2ff', 'commercial_license', 0),
(1254, 'steam:11000013d51c2ff', 'WEAPON_STUNGUN', 0),
(1255, 'steam:11000013d51c2ff', 'plongee1', 0),
(1256, 'steam:11000013d51c2ff', 'teqpaf', 0),
(1257, 'steam:11000013d51c2ff', 'saucisson', 0),
(1258, 'steam:11000013d51c2ff', 'weapons_license2', 0),
(1259, 'steam:11000013d51c2ff', 'golem', 0),
(1260, 'steam:11000013d51c2ff', 'weapons_license1', 0),
(1261, 'steam:11000013d51c2ff', 'macka', 0),
(1262, 'steam:11000013d51c2ff', 'lighter', 0),
(1263, 'steam:11000013d51c2ff', 'pastacarbonara', 0),
(1264, 'steam:11000013d51c2ff', 'pizza', 0),
(1265, 'steam:11000013d51c2ff', 'chips', 0),
(1266, 'steam:11000013d51c2ff', 'loka', 0),
(1267, 'steam:11000013d51c2ff', 'dabs', 0),
(1268, 'steam:11000013d51c2ff', 'cocacola', 0),
(1269, 'steam:11000013d51c2ff', 'boitier', 0),
(1270, 'steam:11000013d51c2ff', 'WEAPON_PISTOL', 0),
(1271, 'steam:11000013d51c2ff', 'jagerbomb', 0),
(1272, 'steam:11000013d51c2ff', 'clip', 0),
(1273, 'steam:11000013d51c2ff', 'protein_shake', 0),
(1274, 'steam:11000013d51c2ff', 'turtle_pooch', 0),
(1275, 'steam:11000013d51c2ff', 'bread', 0),
(1276, 'steam:11000013d51c2ff', 'fixkit', 0),
(1277, 'steam:11000013d51c2ff', 'fanta', 0),
(1278, 'steam:11000013d51c2ff', 'limonade', 0),
(1279, 'steam:11000013d51c2ff', 'WEAPON_KNIFE', 0),
(1280, 'steam:11000013d51c2ff', 'meth', 0),
(1281, 'steam:11000013d51c2ff', 'petrol_raffin', 0),
(1282, 'steam:11000013d51c2ff', 'whiskey', 0),
(1283, 'steam:11000013d51c2ff', 'heroine', 0),
(1284, 'steam:11000013d51c2ff', 'litter', 0),
(1285, 'steam:11000013d51c2ff', 'blowpipe', 0),
(1286, 'steam:11000013d51c2ff', 'rhum', 0),
(1287, 'steam:11000013d51c2ff', 'ephedrine', 0),
(1288, 'steam:11000013d51c2ff', 'clothe', 0),
(1289, 'steam:11000013d51c2ff', 'ephedra', 0),
(1290, 'steam:11000013d51c2ff', 'vodkafruit', 0),
(1291, 'steam:11000013d51c2ff', 'bandage', 0),
(1292, 'steam:11000013d51c2ff', 'leather', 0),
(1293, 'steam:11000013d51c2ff', 'fabric', 0),
(1294, 'steam:11000013d51c2ff', 'cola', 0),
(1295, 'steam:11000013d51c2ff', 'vegetables', 0),
(1296, 'steam:11000013d51c2ff', 'fixtool', 0),
(1297, 'steam:11000013d51c2ff', 'bolcacahuetes', 0),
(1298, 'steam:11000013d51c2ff', 'gazbottle', 0),
(1299, 'steam:11000013d51c2ff', 'bolpistache', 0),
(1300, 'steam:11000013d51c2ff', 'plongee2', 0),
(1301, 'steam:11000013d51c2ff', 'gold', 0),
(1302, 'steam:11000013d51c2ff', 'metreshooter', 0),
(1303, 'steam:11000013d51c2ff', 'grapperaisin', 0),
(1304, 'steam:11000013d51c2ff', 'cocaine', 0),
(1305, 'steam:11000013d51c2ff', 'weapons_license3', 0),
(1306, 'steam:11000013d51c2ff', 'WEAPON_PUMPSHOTGUN', 0),
(1307, 'steam:11000013d51c2ff', 'croquettes', 0),
(1308, 'steam:11000013d51c2ff', 'cutted_wood', 0),
(1309, 'steam:11000013d51c2ff', 'packaged_plank', 0),
(1310, 'steam:11000013d51c2ff', 'binoculars', 0),
(1311, 'steam:11000013d51c2ff', 'pills', 0),
(1312, 'steam:11000013d51c2ff', 'pcp', 0),
(1313, 'steam:11000013d51c2ff', 'silencieux', 0),
(1314, 'steam:11000013d51c2ff', 'painkiller', 0),
(1315, 'steam:11000013d51c2ff', 'burger', 0),
(1316, 'steam:11000011a2fc3d0', 'breathalyzer', 0),
(1317, 'steam:11000011a2fc3d0', 'meat', 0),
(1318, 'steam:11000011a2fc3d0', 'coffee', 0),
(1319, 'steam:11000011a2fc3d0', 'mixapero', 0),
(1320, 'steam:11000011a2fc3d0', 'firstaidpass', 0),
(1321, 'steam:11000011a2fc3d0', 'alive_chicken', 0),
(1322, 'steam:11000011a2fc3d0', 'sportlunch', 0),
(1323, 'steam:11000011a2fc3d0', 'blackberry', 0),
(1324, 'steam:11000011a2fc3d0', 'coke', 0),
(1325, 'steam:11000011a2fc3d0', 'powerade', 0),
(1326, 'steam:11000011a2fc3d0', 'contrat', 0),
(1327, 'steam:11000011a2fc3d0', 'petrol', 0),
(1328, 'steam:11000011a2fc3d0', 'opium', 0),
(1329, 'steam:11000011a2fc3d0', 'mojito', 0),
(1330, 'steam:11000011a2fc3d0', 'diving_license', 0),
(1331, 'steam:11000011a2fc3d0', 'litter_pooch', 0),
(1332, 'steam:11000011a2fc3d0', 'gym_membership', 0),
(1333, 'steam:11000011a2fc3d0', 'scratchoff_used', 0),
(1334, 'steam:11000011a2fc3d0', 'marijuana', 0),
(1335, 'steam:11000011a2fc3d0', 'WEAPON_BAT', 0),
(1336, 'steam:11000011a2fc3d0', 'turtle', 0),
(1337, 'steam:11000011a2fc3d0', 'copper', 0),
(1338, 'steam:11000011a2fc3d0', 'cannabis', 0),
(1339, 'steam:11000011a2fc3d0', 'cheesebows', 0),
(1340, 'steam:11000011a2fc3d0', 'jusfruit', 0),
(1341, 'steam:11000011a2fc3d0', 'marabou', 0),
(1342, 'steam:11000011a2fc3d0', 'whiskycoca', 0),
(1343, 'steam:11000011a2fc3d0', 'baconburger', 0),
(1344, 'steam:11000011a2fc3d0', 'grip', 0),
(1345, 'steam:11000011a2fc3d0', 'hunting_license', 0),
(1346, 'steam:11000011a2fc3d0', 'lsd_pooch', 0),
(1347, 'steam:11000011a2fc3d0', 'coke_pooch', 0),
(1348, 'steam:11000011a2fc3d0', 'iron', 0),
(1349, 'steam:11000011a2fc3d0', 'sprite', 0),
(1350, 'steam:11000011a2fc3d0', 'wood', 0),
(1351, 'steam:11000011a2fc3d0', 'drpepper', 0),
(1352, 'steam:11000011a2fc3d0', 'whool', 0),
(1353, 'steam:11000011a2fc3d0', 'diamond', 0),
(1354, 'steam:11000011a2fc3d0', 'coca', 0),
(1355, 'steam:11000011a2fc3d0', 'vodkaenergy', 0),
(1356, 'steam:11000011a2fc3d0', 'medikit', 0),
(1357, 'steam:11000011a2fc3d0', 'packaged_chicken', 0),
(1358, 'steam:11000011a2fc3d0', 'opium_pooch', 0),
(1359, 'steam:11000011a2fc3d0', 'fish', 0),
(1360, 'steam:11000011a2fc3d0', 'meth_pooch', 0),
(1361, 'steam:11000011a2fc3d0', 'soda', 0),
(1362, 'steam:11000011a2fc3d0', 'scratchoff', 0),
(1363, 'steam:11000011a2fc3d0', 'cigarett', 0),
(1364, 'steam:11000011a2fc3d0', 'bolchips', 0),
(1365, 'steam:11000011a2fc3d0', 'beer', 0),
(1366, 'steam:11000011a2fc3d0', 'weed_pooch', 0),
(1367, 'steam:11000011a2fc3d0', 'vodka', 0),
(1368, 'steam:11000011a2fc3d0', 'carotool', 0),
(1369, 'steam:11000011a2fc3d0', 'washed_stone', 0),
(1370, 'steam:11000011a2fc3d0', 'marriage_license', 0),
(1371, 'steam:11000011a2fc3d0', 'donut', 0),
(1372, 'steam:11000011a2fc3d0', 'pearl_pooch', 0),
(1373, 'steam:11000011a2fc3d0', 'poppy', 0),
(1374, 'steam:11000011a2fc3d0', 'whisky', 0),
(1375, 'steam:11000011a2fc3d0', 'flashlight', 0),
(1376, 'steam:11000011a2fc3d0', 'essence', 0),
(1377, 'steam:11000011a2fc3d0', 'crack', 0),
(1378, 'steam:11000011a2fc3d0', 'armor', 0),
(1379, 'steam:11000011a2fc3d0', 'lotteryticket', 0),
(1380, 'steam:11000011a2fc3d0', 'motorcycle_license', 0),
(1381, 'steam:11000011a2fc3d0', 'water', 0),
(1382, 'steam:11000011a2fc3d0', 'slaughtered_chicken', 0),
(1383, 'steam:11000011a2fc3d0', 'fishing_license', 0),
(1384, 'steam:11000011a2fc3d0', 'tequila', 0),
(1385, 'steam:11000011a2fc3d0', 'ice', 0),
(1386, 'steam:11000011a2fc3d0', 'yusuf', 0),
(1387, 'steam:11000011a2fc3d0', 'tacos', 0),
(1388, 'steam:11000011a2fc3d0', 'menthe', 0),
(1389, 'steam:11000011a2fc3d0', 'carokit', 0),
(1390, 'steam:11000011a2fc3d0', 'rhumfruit', 0),
(1391, 'steam:11000011a2fc3d0', 'taxi_license', 0),
(1392, 'steam:11000011a2fc3d0', 'lsd', 0),
(1393, 'steam:11000011a2fc3d0', 'fakepee', 0),
(1394, 'steam:11000011a2fc3d0', 'rhumcoca', 0),
(1395, 'steam:11000011a2fc3d0', 'pearl', 0),
(1396, 'steam:11000011a2fc3d0', 'jager', 0),
(1397, 'steam:11000011a2fc3d0', 'energy', 0),
(1398, 'steam:11000011a2fc3d0', 'weed', 0),
(1399, 'steam:11000011a2fc3d0', 'martini', 0),
(1400, 'steam:11000011a2fc3d0', 'lockpick', 0),
(1401, 'steam:11000011a2fc3d0', 'shotgun_shells', 0),
(1402, 'steam:11000011a2fc3d0', 'receipt', 0),
(1403, 'steam:11000011a2fc3d0', '9mm_rounds', 0),
(1404, 'steam:11000011a2fc3d0', 'bolnoixcajou', 0),
(1405, 'steam:11000011a2fc3d0', 'narcan', 0),
(1406, 'steam:11000011a2fc3d0', 'drugtest', 0),
(1407, 'steam:11000011a2fc3d0', 'pilot_license', 0),
(1408, 'steam:11000011a2fc3d0', 'boating_license', 0),
(1409, 'steam:11000011a2fc3d0', 'WEAPON_FLASHLIGHT', 0),
(1410, 'steam:11000011a2fc3d0', 'icetea', 0),
(1411, 'steam:11000011a2fc3d0', 'radio', 0),
(1412, 'steam:11000011a2fc3d0', 'defibrillateur', 0),
(1413, 'steam:11000011a2fc3d0', 'firstaidkit', 0),
(1414, 'steam:11000011a2fc3d0', 'wrench', 0),
(1415, 'steam:11000011a2fc3d0', 'nitrocannister', 0),
(1416, 'steam:11000011a2fc3d0', 'drivers_license', 0),
(1417, 'steam:11000011a2fc3d0', 'stone', 0),
(1418, 'steam:11000011a2fc3d0', 'commercial_license', 0),
(1419, 'steam:11000011a2fc3d0', 'WEAPON_STUNGUN', 0),
(1420, 'steam:11000011a2fc3d0', 'plongee1', 0),
(1421, 'steam:11000011a2fc3d0', 'teqpaf', 0),
(1422, 'steam:11000011a2fc3d0', 'saucisson', 0),
(1423, 'steam:11000011a2fc3d0', 'weapons_license2', 0),
(1424, 'steam:11000011a2fc3d0', 'golem', 0),
(1425, 'steam:11000011a2fc3d0', 'weapons_license1', 0),
(1426, 'steam:11000011a2fc3d0', 'macka', 0),
(1427, 'steam:11000011a2fc3d0', 'lighter', 0),
(1428, 'steam:11000011a2fc3d0', 'pastacarbonara', 0),
(1429, 'steam:11000011a2fc3d0', 'pizza', 0),
(1430, 'steam:11000011a2fc3d0', 'chips', 0),
(1431, 'steam:11000011a2fc3d0', 'loka', 0),
(1432, 'steam:11000011a2fc3d0', 'dabs', 0),
(1433, 'steam:11000011a2fc3d0', 'cocacola', 0),
(1434, 'steam:11000011a2fc3d0', 'boitier', 0),
(1435, 'steam:11000011a2fc3d0', 'WEAPON_PISTOL', 0),
(1436, 'steam:11000011a2fc3d0', 'jagerbomb', 0),
(1437, 'steam:11000011a2fc3d0', 'clip', 0),
(1438, 'steam:11000011a2fc3d0', 'protein_shake', 0),
(1439, 'steam:11000011a2fc3d0', 'turtle_pooch', 0),
(1440, 'steam:11000011a2fc3d0', 'bread', 0),
(1441, 'steam:11000011a2fc3d0', 'fixkit', 0),
(1442, 'steam:11000011a2fc3d0', 'fanta', 0),
(1443, 'steam:11000011a2fc3d0', 'limonade', 0),
(1444, 'steam:11000011a2fc3d0', 'WEAPON_KNIFE', 0),
(1445, 'steam:11000011a2fc3d0', 'meth', 0),
(1446, 'steam:11000011a2fc3d0', 'petrol_raffin', 0),
(1447, 'steam:11000011a2fc3d0', 'whiskey', 0),
(1448, 'steam:11000011a2fc3d0', 'heroine', 0),
(1449, 'steam:11000011a2fc3d0', 'litter', 0),
(1450, 'steam:11000011a2fc3d0', 'blowpipe', 0),
(1451, 'steam:11000011a2fc3d0', 'rhum', 0),
(1452, 'steam:11000011a2fc3d0', 'ephedrine', 0),
(1453, 'steam:11000011a2fc3d0', 'clothe', 0),
(1454, 'steam:11000011a2fc3d0', 'ephedra', 0),
(1455, 'steam:11000011a2fc3d0', 'vodkafruit', 0),
(1456, 'steam:11000011a2fc3d0', 'bandage', 0),
(1457, 'steam:11000011a2fc3d0', 'leather', 0),
(1458, 'steam:11000011a2fc3d0', 'fabric', 0),
(1459, 'steam:11000011a2fc3d0', 'cola', 0),
(1460, 'steam:11000011a2fc3d0', 'vegetables', 0),
(1461, 'steam:11000011a2fc3d0', 'fixtool', 0),
(1462, 'steam:11000011a2fc3d0', 'bolcacahuetes', 0),
(1463, 'steam:11000011a2fc3d0', 'gazbottle', 0),
(1464, 'steam:11000011a2fc3d0', 'bolpistache', 0),
(1465, 'steam:11000011a2fc3d0', 'plongee2', 0),
(1466, 'steam:11000011a2fc3d0', 'gold', 0),
(1467, 'steam:11000011a2fc3d0', 'metreshooter', 0),
(1468, 'steam:11000011a2fc3d0', 'grapperaisin', 0),
(1469, 'steam:11000011a2fc3d0', 'cocaine', 0),
(1470, 'steam:11000011a2fc3d0', 'weapons_license3', 0),
(1471, 'steam:11000011a2fc3d0', 'WEAPON_PUMPSHOTGUN', 0),
(1472, 'steam:11000011a2fc3d0', 'croquettes', 0),
(1473, 'steam:11000011a2fc3d0', 'cutted_wood', 0),
(1474, 'steam:11000011a2fc3d0', 'packaged_plank', 0),
(1475, 'steam:11000011a2fc3d0', 'binoculars', 0),
(1476, 'steam:11000011a2fc3d0', 'pills', 0),
(1477, 'steam:11000011a2fc3d0', 'pcp', 0),
(1478, 'steam:11000011a2fc3d0', 'silencieux', 0),
(1479, 'steam:11000011a2fc3d0', 'painkiller', 0),
(1480, 'steam:11000011a2fc3d0', 'burger', 0),
(1481, 'steam:110000112969e8f', 'receipt', 0),
(1482, 'steam:11000010a01bdb9', 'dlic', 1),
(1483, 'steam:11000010a01bdb9', 'mlic', 0),
(1484, 'steam:11000010a01bdb9', 'cdl', 0),
(1485, 'steam:110000112969e8f', 'cdl', 0),
(1486, 'steam:110000112969e8f', 'dlic', 1),
(1487, 'steam:110000112969e8f', 'mlic', 0),
(1488, 'steam:1100001048af48f', 'marabou', 0),
(1489, 'steam:1100001048af48f', 'stone', 0),
(1490, 'steam:1100001048af48f', 'binoculars', 0),
(1491, 'steam:1100001048af48f', 'protein_shake', 0),
(1492, 'steam:1100001048af48f', 'meat', 0),
(1493, 'steam:1100001048af48f', 'mixapero', 0),
(1494, 'steam:1100001048af48f', 'cdl', 0),
(1495, 'steam:1100001048af48f', 'wood', 0),
(1496, 'steam:1100001048af48f', 'cannabis', 0),
(1497, 'steam:1100001048af48f', 'coca', 0),
(1498, 'steam:1100001048af48f', 'flashlight', 0),
(1499, 'steam:1100001048af48f', 'donut', 0),
(1500, 'steam:1100001048af48f', 'drugtest', 0),
(1501, 'steam:1100001048af48f', 'gym_membership', 0),
(1502, 'steam:1100001048af48f', '9mm_rounds', 0),
(1503, 'steam:1100001048af48f', 'marriage_license', 0),
(1504, 'steam:1100001048af48f', 'whool', 0),
(1505, 'steam:1100001048af48f', 'scratchoff', 0),
(1506, 'steam:1100001048af48f', 'opium_pooch', 0),
(1507, 'steam:1100001048af48f', 'cigarett', 0),
(1508, 'steam:1100001048af48f', 'pizza', 0),
(1509, 'steam:1100001048af48f', 'yusuf', 0),
(1510, 'steam:1100001048af48f', 'bolpistache', 0),
(1511, 'steam:1100001048af48f', 'lsd_pooch', 0),
(1512, 'steam:1100001048af48f', 'opium', 0),
(1513, 'steam:1100001048af48f', 'pills', 0),
(1514, 'steam:1100001048af48f', 'baconburger', 0),
(1515, 'steam:1100001048af48f', 'clothe', 0),
(1516, 'steam:1100001048af48f', 'whiskycoca', 0),
(1517, 'steam:1100001048af48f', 'clip', 0),
(1518, 'steam:1100001048af48f', 'fish', 0),
(1519, 'steam:1100001048af48f', 'packaged_plank', 0),
(1520, 'steam:1100001048af48f', 'medikit', 0),
(1521, 'steam:1100001048af48f', 'fixkit', 0),
(1522, 'steam:1100001048af48f', 'soda', 0),
(1523, 'steam:1100001048af48f', 'iron', 0),
(1524, 'steam:1100001048af48f', 'litter_pooch', 0),
(1525, 'steam:1100001048af48f', 'lighter', 0),
(1526, 'steam:1100001048af48f', 'meth_pooch', 0),
(1527, 'steam:1100001048af48f', 'scratchoff_used', 0),
(1528, 'steam:1100001048af48f', 'cola', 0),
(1529, 'steam:1100001048af48f', 'fabric', 0),
(1530, 'steam:1100001048af48f', 'beer', 0),
(1531, 'steam:1100001048af48f', 'croquettes', 0),
(1532, 'steam:1100001048af48f', 'crack', 0),
(1533, 'steam:1100001048af48f', 'fanta', 0),
(1534, 'steam:1100001048af48f', 'sportlunch', 0),
(1535, 'steam:1100001048af48f', 'plongee1', 0),
(1536, 'steam:1100001048af48f', 'golem', 0),
(1537, 'steam:1100001048af48f', 'cocacola', 0),
(1538, 'steam:1100001048af48f', 'carokit', 0),
(1539, 'steam:1100001048af48f', 'mojito', 0),
(1540, 'steam:1100001048af48f', 'bolnoixcajou', 0),
(1541, 'steam:1100001048af48f', 'taxi_license', 0),
(1542, 'steam:1100001048af48f', 'hunting_license', 0),
(1543, 'steam:1100001048af48f', 'weapons_license1', 0),
(1544, 'steam:1100001048af48f', 'turtle_pooch', 0),
(1545, 'steam:1100001048af48f', 'powerade', 0),
(1546, 'steam:1100001048af48f', 'bolcacahuetes', 0),
(1547, 'steam:1100001048af48f', 'litter', 0),
(1548, 'steam:1100001048af48f', 'burger', 0),
(1549, 'steam:1100001048af48f', 'essence', 0),
(1550, 'steam:1100001048af48f', 'bread', 0),
(1551, 'steam:1100001048af48f', 'silencieux', 0),
(1552, 'steam:1100001048af48f', 'alive_chicken', 0),
(1553, 'steam:1100001048af48f', 'copper', 0),
(1554, 'steam:1100001048af48f', 'pearl', 0),
(1555, 'steam:1100001048af48f', 'whisky', 0),
(1556, 'steam:1100001048af48f', 'contrat', 0),
(1557, 'steam:1100001048af48f', 'coke_pooch', 0),
(1558, 'steam:1100001048af48f', 'marijuana', 0),
(1559, 'steam:1100001048af48f', 'plongee2', 0),
(1560, 'steam:1100001048af48f', 'fakepee', 0),
(1561, 'steam:1100001048af48f', 'leather', 0),
(1562, 'steam:1100001048af48f', 'ephedra', 0),
(1563, 'steam:1100001048af48f', 'tacos', 0),
(1564, 'steam:1100001048af48f', 'jagerbomb', 0),
(1565, 'steam:1100001048af48f', 'weed', 0),
(1566, 'steam:1100001048af48f', 'menthe', 0),
(1567, 'steam:1100001048af48f', 'lockpick', 0),
(1568, 'steam:1100001048af48f', 'WEAPON_PISTOL', 0),
(1569, 'steam:1100001048af48f', 'grapperaisin', 0),
(1570, 'steam:1100001048af48f', 'boating_license', 0),
(1571, 'steam:1100001048af48f', 'receipt', 0),
(1572, 'steam:1100001048af48f', 'dabs', 0),
(1573, 'steam:1100001048af48f', 'icetea', 0),
(1574, 'steam:1100001048af48f', 'grip', 0),
(1575, 'steam:1100001048af48f', 'firstaidkit', 0),
(1576, 'steam:1100001048af48f', 'ice', 0),
(1577, 'steam:1100001048af48f', 'carotool', 0),
(1578, 'steam:1100001048af48f', 'diamond', 0),
(1579, 'steam:1100001048af48f', 'WEAPON_PUMPSHOTGUN', 0),
(1580, 'steam:1100001048af48f', 'gold', 0),
(1581, 'steam:1100001048af48f', 'WEAPON_FLASHLIGHT', 0),
(1582, 'steam:1100001048af48f', 'metreshooter', 0),
(1583, 'steam:1100001048af48f', 'WEAPON_BAT', 0),
(1584, 'steam:1100001048af48f', 'WEAPON_KNIFE', 0),
(1585, 'steam:1100001048af48f', 'WEAPON_STUNGUN', 0),
(1586, 'steam:1100001048af48f', 'gazbottle', 0),
(1587, 'steam:1100001048af48f', 'firstaidpass', 0),
(1588, 'steam:1100001048af48f', 'martini', 0),
(1589, 'steam:1100001048af48f', 'weapons_license3', 0),
(1590, 'steam:1100001048af48f', 'wrench', 0),
(1591, 'steam:1100001048af48f', 'tequila', 0),
(1592, 'steam:1100001048af48f', 'water', 0),
(1593, 'steam:1100001048af48f', 'jusfruit', 0),
(1594, 'steam:1100001048af48f', 'coffee', 0),
(1595, 'steam:1100001048af48f', 'radio', 0),
(1596, 'steam:1100001048af48f', 'dlic', 0),
(1597, 'steam:1100001048af48f', 'drpepper', 0),
(1598, 'steam:1100001048af48f', 'blowpipe', 0),
(1599, 'steam:1100001048af48f', 'mlic', 0),
(1600, 'steam:1100001048af48f', 'boitier', 0),
(1601, 'steam:1100001048af48f', 'pilot_license', 0),
(1602, 'steam:1100001048af48f', 'washed_stone', 0),
(1603, 'steam:1100001048af48f', 'fishing_license', 0),
(1604, 'steam:1100001048af48f', 'shotgun_shells', 0),
(1605, 'steam:1100001048af48f', 'whiskey', 0),
(1606, 'steam:1100001048af48f', 'lotteryticket', 0),
(1607, 'steam:1100001048af48f', 'macka', 0),
(1608, 'steam:1100001048af48f', 'pastacarbonara', 0),
(1609, 'steam:1100001048af48f', 'fixtool', 0),
(1610, 'steam:1100001048af48f', 'chips', 0),
(1611, 'steam:1100001048af48f', 'meth', 0),
(1612, 'steam:1100001048af48f', 'ephedrine', 0),
(1613, 'steam:1100001048af48f', 'loka', 0),
(1614, 'steam:1100001048af48f', 'weed_pooch', 0),
(1615, 'steam:1100001048af48f', 'sprite', 0),
(1616, 'steam:1100001048af48f', 'energy', 0),
(1617, 'steam:1100001048af48f', 'cheesebows', 0),
(1618, 'steam:1100001048af48f', 'rhumcoca', 0),
(1619, 'steam:1100001048af48f', 'jager', 0),
(1620, 'steam:1100001048af48f', 'vodkaenergy', 0),
(1621, 'steam:1100001048af48f', 'teqpaf', 0),
(1622, 'steam:1100001048af48f', 'armor', 0),
(1623, 'steam:1100001048af48f', 'slaughtered_chicken', 0),
(1624, 'steam:1100001048af48f', 'pearl_pooch', 0),
(1625, 'steam:1100001048af48f', 'cocaine', 0),
(1626, 'steam:1100001048af48f', 'packaged_chicken', 0),
(1627, 'steam:1100001048af48f', 'saucisson', 0),
(1628, 'steam:1100001048af48f', 'painkiller', 0),
(1629, 'steam:1100001048af48f', 'diving_license', 0),
(1630, 'steam:1100001048af48f', 'narcan', 0),
(1631, 'steam:1100001048af48f', 'vegetables', 0),
(1632, 'steam:1100001048af48f', 'heroine', 0),
(1633, 'steam:1100001048af48f', 'vodkafruit', 0),
(1634, 'steam:1100001048af48f', 'defibrillateur', 0),
(1635, 'steam:1100001048af48f', 'breathalyzer', 0),
(1636, 'steam:1100001048af48f', 'coke', 0),
(1637, 'steam:1100001048af48f', 'vodka', 0),
(1638, 'steam:1100001048af48f', 'poppy', 0),
(1639, 'steam:1100001048af48f', 'petrol', 0),
(1640, 'steam:1100001048af48f', 'limonade', 0),
(1641, 'steam:1100001048af48f', 'rhum', 0),
(1642, 'steam:1100001048af48f', 'nitrocannister', 0),
(1643, 'steam:1100001048af48f', 'turtle', 0),
(1644, 'steam:1100001048af48f', 'blackberry', 0),
(1645, 'steam:1100001048af48f', 'weapons_license2', 0),
(1646, 'steam:1100001048af48f', 'pcp', 0),
(1647, 'steam:1100001048af48f', 'cutted_wood', 0),
(1648, 'steam:1100001048af48f', 'lsd', 0),
(1649, 'steam:1100001048af48f', 'bolchips', 0),
(1650, 'steam:1100001048af48f', 'rhumfruit', 0),
(1651, 'steam:1100001048af48f', 'bandage', 0),
(1652, 'steam:1100001048af48f', 'petrol_raffin', 0),
(1653, 'steam:11000013bc17e0e', 'marabou', 0),
(1654, 'steam:11000013bc17e0e', 'stone', 0),
(1655, 'steam:11000013bc17e0e', 'binoculars', 0),
(1656, 'steam:11000013bc17e0e', 'protein_shake', 0),
(1657, 'steam:11000013bc17e0e', 'meat', 0),
(1658, 'steam:11000013bc17e0e', 'mixapero', 0),
(1659, 'steam:11000013bc17e0e', 'cdl', 0),
(1660, 'steam:11000013bc17e0e', 'wood', 0),
(1661, 'steam:11000013bc17e0e', 'cannabis', 0),
(1662, 'steam:11000013bc17e0e', 'coca', 0),
(1663, 'steam:11000013bc17e0e', 'flashlight', 0),
(1664, 'steam:11000013bc17e0e', 'donut', 0),
(1665, 'steam:11000013bc17e0e', 'drugtest', 0),
(1666, 'steam:11000013bc17e0e', 'gym_membership', 0),
(1667, 'steam:11000013bc17e0e', '9mm_rounds', 0),
(1668, 'steam:11000013bc17e0e', 'marriage_license', 0),
(1669, 'steam:11000013bc17e0e', 'whool', 0),
(1670, 'steam:11000013bc17e0e', 'scratchoff', 0),
(1671, 'steam:11000013bc17e0e', 'opium_pooch', 0),
(1672, 'steam:11000013bc17e0e', 'cigarett', 0),
(1673, 'steam:11000013bc17e0e', 'pizza', 0),
(1674, 'steam:11000013bc17e0e', 'yusuf', 0),
(1675, 'steam:11000013bc17e0e', 'bolpistache', 0),
(1676, 'steam:11000013bc17e0e', 'lsd_pooch', 0),
(1677, 'steam:11000013bc17e0e', 'opium', 0),
(1678, 'steam:11000013bc17e0e', 'pills', 0),
(1679, 'steam:11000013bc17e0e', 'baconburger', 0),
(1680, 'steam:11000013bc17e0e', 'clothe', 0),
(1681, 'steam:11000013bc17e0e', 'whiskycoca', 0),
(1682, 'steam:11000013bc17e0e', 'clip', 0),
(1683, 'steam:11000013bc17e0e', 'fish', 0),
(1684, 'steam:11000013bc17e0e', 'packaged_plank', 0),
(1685, 'steam:11000013bc17e0e', 'medikit', 0),
(1686, 'steam:11000013bc17e0e', 'fixkit', 0),
(1687, 'steam:11000013bc17e0e', 'soda', 0),
(1688, 'steam:11000013bc17e0e', 'iron', 0),
(1689, 'steam:11000013bc17e0e', 'litter_pooch', 0),
(1690, 'steam:11000013bc17e0e', 'lighter', 0),
(1691, 'steam:11000013bc17e0e', 'meth_pooch', 0),
(1692, 'steam:11000013bc17e0e', 'scratchoff_used', 0),
(1693, 'steam:11000013bc17e0e', 'cola', 0),
(1694, 'steam:11000013bc17e0e', 'fabric', 0),
(1695, 'steam:11000013bc17e0e', 'beer', 0),
(1696, 'steam:11000013bc17e0e', 'croquettes', 0),
(1697, 'steam:11000013bc17e0e', 'crack', 0),
(1698, 'steam:11000013bc17e0e', 'fanta', 0),
(1699, 'steam:11000013bc17e0e', 'sportlunch', 0),
(1700, 'steam:11000013bc17e0e', 'plongee1', 0),
(1701, 'steam:11000013bc17e0e', 'golem', 0),
(1702, 'steam:11000013bc17e0e', 'cocacola', 0),
(1703, 'steam:11000013bc17e0e', 'carokit', 0),
(1704, 'steam:11000013bc17e0e', 'mojito', 0),
(1705, 'steam:11000013bc17e0e', 'bolnoixcajou', 0),
(1706, 'steam:11000013bc17e0e', 'taxi_license', 0),
(1707, 'steam:11000013bc17e0e', 'hunting_license', 0),
(1708, 'steam:11000013bc17e0e', 'weapons_license1', 0),
(1709, 'steam:11000013bc17e0e', 'turtle_pooch', 0),
(1710, 'steam:11000013bc17e0e', 'powerade', 0),
(1711, 'steam:11000013bc17e0e', 'bolcacahuetes', 0),
(1712, 'steam:11000013bc17e0e', 'litter', 0),
(1713, 'steam:11000013bc17e0e', 'burger', 0),
(1714, 'steam:11000013bc17e0e', 'essence', 0),
(1715, 'steam:11000013bc17e0e', 'bread', 0),
(1716, 'steam:11000013bc17e0e', 'silencieux', 0),
(1717, 'steam:11000013bc17e0e', 'alive_chicken', 0),
(1718, 'steam:11000013bc17e0e', 'copper', 0),
(1719, 'steam:11000013bc17e0e', 'pearl', 0),
(1720, 'steam:11000013bc17e0e', 'whisky', 0),
(1721, 'steam:11000013bc17e0e', 'contrat', 0),
(1722, 'steam:11000013bc17e0e', 'coke_pooch', 0),
(1723, 'steam:11000013bc17e0e', 'marijuana', 0),
(1724, 'steam:11000013bc17e0e', 'plongee2', 0),
(1725, 'steam:11000013bc17e0e', 'fakepee', 0),
(1726, 'steam:11000013bc17e0e', 'leather', 0),
(1727, 'steam:11000013bc17e0e', 'ephedra', 0),
(1728, 'steam:11000013bc17e0e', 'tacos', 0),
(1729, 'steam:11000013bc17e0e', 'jagerbomb', 0),
(1730, 'steam:11000013bc17e0e', 'weed', 0),
(1731, 'steam:11000013bc17e0e', 'menthe', 0),
(1732, 'steam:11000013bc17e0e', 'lockpick', 0),
(1733, 'steam:11000013bc17e0e', 'WEAPON_PISTOL', 0),
(1734, 'steam:11000013bc17e0e', 'grapperaisin', 0),
(1735, 'steam:11000013bc17e0e', 'boating_license', 0),
(1736, 'steam:11000013bc17e0e', 'receipt', 0),
(1737, 'steam:11000013bc17e0e', 'dabs', 0),
(1738, 'steam:11000013bc17e0e', 'icetea', 0),
(1739, 'steam:11000013bc17e0e', 'grip', 0),
(1740, 'steam:11000013bc17e0e', 'firstaidkit', 0),
(1741, 'steam:11000013bc17e0e', 'ice', 0),
(1742, 'steam:11000013bc17e0e', 'carotool', 0),
(1743, 'steam:11000013bc17e0e', 'diamond', 0),
(1744, 'steam:11000013bc17e0e', 'WEAPON_PUMPSHOTGUN', 0),
(1745, 'steam:11000013bc17e0e', 'gold', 0),
(1746, 'steam:11000013bc17e0e', 'WEAPON_FLASHLIGHT', 0),
(1747, 'steam:11000013bc17e0e', 'metreshooter', 0),
(1748, 'steam:11000013bc17e0e', 'WEAPON_BAT', 0),
(1749, 'steam:11000013bc17e0e', 'WEAPON_KNIFE', 0),
(1750, 'steam:11000013bc17e0e', 'WEAPON_STUNGUN', 0),
(1751, 'steam:11000013bc17e0e', 'gazbottle', 0),
(1752, 'steam:11000013bc17e0e', 'firstaidpass', 0),
(1753, 'steam:11000013bc17e0e', 'martini', 0),
(1754, 'steam:11000013bc17e0e', 'weapons_license3', 0),
(1755, 'steam:11000013bc17e0e', 'wrench', 0),
(1756, 'steam:11000013bc17e0e', 'tequila', 0),
(1757, 'steam:11000013bc17e0e', 'water', 0),
(1758, 'steam:11000013bc17e0e', 'jusfruit', 0),
(1759, 'steam:11000013bc17e0e', 'coffee', 0),
(1760, 'steam:11000013bc17e0e', 'radio', 0),
(1761, 'steam:11000013bc17e0e', 'dlic', 0),
(1762, 'steam:11000013bc17e0e', 'drpepper', 0),
(1763, 'steam:11000013bc17e0e', 'blowpipe', 0),
(1764, 'steam:11000013bc17e0e', 'mlic', 0),
(1765, 'steam:11000013bc17e0e', 'boitier', 0),
(1766, 'steam:11000013bc17e0e', 'pilot_license', 0),
(1767, 'steam:11000013bc17e0e', 'washed_stone', 0),
(1768, 'steam:11000013bc17e0e', 'fishing_license', 0),
(1769, 'steam:11000013bc17e0e', 'shotgun_shells', 0),
(1770, 'steam:11000013bc17e0e', 'whiskey', 0),
(1771, 'steam:11000013bc17e0e', 'lotteryticket', 0),
(1772, 'steam:11000013bc17e0e', 'macka', 0),
(1773, 'steam:11000013bc17e0e', 'pastacarbonara', 0),
(1774, 'steam:11000013bc17e0e', 'fixtool', 0),
(1775, 'steam:11000013bc17e0e', 'chips', 0),
(1776, 'steam:11000013bc17e0e', 'meth', 0),
(1777, 'steam:11000013bc17e0e', 'ephedrine', 0),
(1778, 'steam:11000013bc17e0e', 'loka', 0),
(1779, 'steam:11000013bc17e0e', 'weed_pooch', 0),
(1780, 'steam:11000013bc17e0e', 'sprite', 0),
(1781, 'steam:11000013bc17e0e', 'energy', 0),
(1782, 'steam:11000013bc17e0e', 'cheesebows', 0),
(1783, 'steam:11000013bc17e0e', 'rhumcoca', 0),
(1784, 'steam:11000013bc17e0e', 'jager', 0),
(1785, 'steam:11000013bc17e0e', 'vodkaenergy', 0),
(1786, 'steam:11000013bc17e0e', 'teqpaf', 0),
(1787, 'steam:11000013bc17e0e', 'armor', 0),
(1788, 'steam:11000013bc17e0e', 'slaughtered_chicken', 0),
(1789, 'steam:11000013bc17e0e', 'pearl_pooch', 0),
(1790, 'steam:11000013bc17e0e', 'cocaine', 0),
(1791, 'steam:11000013bc17e0e', 'packaged_chicken', 0),
(1792, 'steam:11000013bc17e0e', 'saucisson', 0),
(1793, 'steam:11000013bc17e0e', 'painkiller', 0),
(1794, 'steam:11000013bc17e0e', 'diving_license', 0),
(1795, 'steam:11000013bc17e0e', 'narcan', 0),
(1796, 'steam:11000013bc17e0e', 'vegetables', 0),
(1797, 'steam:11000013bc17e0e', 'heroine', 0),
(1798, 'steam:11000013bc17e0e', 'vodkafruit', 0),
(1799, 'steam:11000013bc17e0e', 'defibrillateur', 0),
(1800, 'steam:11000013bc17e0e', 'breathalyzer', 0),
(1801, 'steam:11000013bc17e0e', 'coke', 0),
(1802, 'steam:11000013bc17e0e', 'vodka', 0),
(1803, 'steam:11000013bc17e0e', 'poppy', 0),
(1804, 'steam:11000013bc17e0e', 'petrol', 0),
(1805, 'steam:11000013bc17e0e', 'limonade', 0),
(1806, 'steam:11000013bc17e0e', 'rhum', 0),
(1807, 'steam:11000013bc17e0e', 'nitrocannister', 0),
(1808, 'steam:11000013bc17e0e', 'turtle', 0),
(1809, 'steam:11000013bc17e0e', 'blackberry', 0),
(1810, 'steam:11000013bc17e0e', 'weapons_license2', 0),
(1811, 'steam:11000013bc17e0e', 'pcp', 0),
(1812, 'steam:11000013bc17e0e', 'cutted_wood', 0),
(1813, 'steam:11000013bc17e0e', 'lsd', 0),
(1814, 'steam:11000013bc17e0e', 'bolchips', 0),
(1815, 'steam:11000013bc17e0e', 'rhumfruit', 0),
(1816, 'steam:11000013bc17e0e', 'bandage', 0),
(1817, 'steam:11000013bc17e0e', 'petrol_raffin', 0),
(1818, 'steam:110000132580eb0', 'cdl', 0),
(1819, 'steam:110000132580eb0', 'dlic', 1),
(1820, 'steam:110000132580eb0', 'mlic', 0),
(1821, 'steam:110000132580eb0', 'licenseplate', 0),
(1822, 'steam:11000010a01bdb9', 'licenseplate', 0),
(1823, 'steam:110000132580eb0', 'gasoline', 0),
(1824, 'steam:1100001068ef13c', 'gasoline', 0),
(1825, 'steam:1100001068ef13c', 'receipt', 0),
(1826, 'steam:1100001068ef13c', 'dlic', 0),
(1827, 'steam:1100001068ef13c', 'licenseplate', 0),
(1828, 'steam:1100001068ef13c', 'mlic', 0),
(1829, 'steam:1100001068ef13c', 'cdl', 0),
(1830, 'steam:1100001079cf4ab', 'dabs', 0),
(1831, 'steam:1100001079cf4ab', 'scratchoff', 0),
(1832, 'steam:1100001079cf4ab', 'lsd', 0),
(1833, 'steam:1100001079cf4ab', 'WEAPON_STUNGUN', 0),
(1834, 'steam:1100001079cf4ab', 'grapperaisin', 0),
(1835, 'steam:1100001079cf4ab', 'gasoline', 0),
(1836, 'steam:1100001079cf4ab', 'protein_shake', 0),
(1837, 'steam:1100001079cf4ab', 'sportlunch', 0),
(1838, 'steam:1100001079cf4ab', 'packaged_chicken', 0),
(1839, 'steam:1100001079cf4ab', 'flashlight', 0),
(1840, 'steam:1100001079cf4ab', 'rhumfruit', 0),
(1841, 'steam:1100001079cf4ab', 'binoculars', 0),
(1842, 'steam:1100001079cf4ab', 'fish', 0),
(1843, 'steam:1100001079cf4ab', 'WEAPON_FLASHLIGHT', 0),
(1844, 'steam:1100001079cf4ab', 'poppy', 0),
(1845, 'steam:1100001079cf4ab', 'leather', 0),
(1846, 'steam:1100001079cf4ab', 'blowpipe', 0),
(1847, 'steam:1100001079cf4ab', 'marriage_license', 0),
(1848, 'steam:1100001079cf4ab', 'weed_pooch', 0),
(1849, 'steam:1100001079cf4ab', 'fixtool', 0),
(1850, 'steam:1100001079cf4ab', 'opium_pooch', 0),
(1851, 'steam:1100001079cf4ab', 'energy', 0),
(1852, 'steam:1100001079cf4ab', 'menthe', 0),
(1853, 'steam:1100001079cf4ab', 'jusfruit', 0),
(1854, 'steam:1100001079cf4ab', 'coffee', 0),
(1855, 'steam:1100001079cf4ab', 'bolpistache', 0),
(1856, 'steam:1100001079cf4ab', 'saucisson', 0),
(1857, 'steam:1100001079cf4ab', 'whisky', 0),
(1858, 'steam:1100001079cf4ab', 'bolnoixcajou', 0),
(1859, 'steam:1100001079cf4ab', 'petrol_raffin', 0),
(1860, 'steam:1100001079cf4ab', 'cocacola', 0),
(1861, 'steam:1100001079cf4ab', 'gazbottle', 0),
(1862, 'steam:1100001079cf4ab', 'nitrocannister', 0),
(1863, 'steam:1100001079cf4ab', 'weapons_license2', 0),
(1864, 'steam:1100001079cf4ab', 'jager', 0),
(1865, 'steam:1100001079cf4ab', 'cutted_wood', 0),
(1866, 'steam:1100001079cf4ab', 'petrol', 0),
(1867, 'steam:1100001079cf4ab', 'whiskey', 0),
(1868, 'steam:1100001079cf4ab', 'pearl', 0),
(1869, 'steam:1100001079cf4ab', 'marijuana', 0),
(1870, 'steam:1100001079cf4ab', 'meat', 0),
(1871, 'steam:1100001079cf4ab', 'fixkit', 0),
(1872, 'steam:1100001079cf4ab', 'cannabis', 0),
(1873, 'steam:1100001079cf4ab', 'medikit', 0),
(1874, 'steam:1100001079cf4ab', 'bolcacahuetes', 0),
(1875, 'steam:1100001079cf4ab', 'vodka', 0),
(1876, 'steam:1100001079cf4ab', 'lockpick', 0),
(1877, 'steam:1100001079cf4ab', 'ice', 0),
(1878, 'steam:1100001079cf4ab', 'firstaidpass', 0),
(1879, 'steam:1100001079cf4ab', 'tequila', 0),
(1880, 'steam:1100001079cf4ab', 'blackberry', 0),
(1881, 'steam:1100001079cf4ab', 'fakepee', 0),
(1882, 'steam:1100001079cf4ab', 'shotgun_shells', 0),
(1883, 'steam:1100001079cf4ab', 'heroine', 0),
(1884, 'steam:1100001079cf4ab', 'croquettes', 0),
(1885, 'steam:1100001079cf4ab', 'diamond', 0),
(1886, 'steam:1100001079cf4ab', 'drugtest', 0),
(1887, 'steam:1100001079cf4ab', 'receipt', 0),
(1888, 'steam:1100001079cf4ab', 'loka', 0),
(1889, 'steam:1100001079cf4ab', 'vodkafruit', 0),
(1890, 'steam:1100001079cf4ab', 'meth', 0),
(1891, 'steam:1100001079cf4ab', 'carotool', 0),
(1892, 'steam:1100001079cf4ab', 'weed', 0),
(1893, 'steam:1100001079cf4ab', 'lsd_pooch', 0),
(1894, 'steam:1100001079cf4ab', 'packaged_plank', 0),
(1895, 'steam:1100001079cf4ab', 'silencieux', 0),
(1896, 'steam:1100001079cf4ab', 'tacos', 0),
(1897, 'steam:1100001079cf4ab', 'turtle_pooch', 0),
(1898, 'steam:1100001079cf4ab', 'plongee2', 0),
(1899, 'steam:1100001079cf4ab', 'mixapero', 0),
(1900, 'steam:1100001079cf4ab', 'armor', 0),
(1901, 'steam:1100001079cf4ab', 'turtle', 0),
(1902, 'steam:1100001079cf4ab', 'marabou', 0),
(1903, 'steam:1100001079cf4ab', 'litter', 0),
(1904, 'steam:1100001079cf4ab', 'baconburger', 0),
(1905, 'steam:1100001079cf4ab', 'pills', 0),
(1906, 'steam:1100001079cf4ab', 'boitier', 0),
(1907, 'steam:1100001079cf4ab', 'bolchips', 0),
(1908, 'steam:1100001079cf4ab', 'dlic', 0),
(1909, 'steam:1100001079cf4ab', 'alive_chicken', 0),
(1910, 'steam:1100001079cf4ab', 'rhumcoca', 0),
(1911, 'steam:1100001079cf4ab', 'licenseplate', 0),
(1912, 'steam:1100001079cf4ab', 'cola', 0),
(1913, 'steam:1100001079cf4ab', 'ephedra', 0),
(1914, 'steam:1100001079cf4ab', 'stone', 0),
(1915, 'steam:1100001079cf4ab', 'fanta', 0),
(1916, 'steam:1100001079cf4ab', 'pastacarbonara', 0),
(1917, 'steam:1100001079cf4ab', 'whiskycoca', 0),
(1918, 'steam:1100001079cf4ab', 'WEAPON_PUMPSHOTGUN', 0),
(1919, 'steam:1100001079cf4ab', 'WEAPON_PISTOL', 0),
(1920, 'steam:1100001079cf4ab', 'narcan', 0),
(1921, 'steam:1100001079cf4ab', 'WEAPON_KNIFE', 0),
(1922, 'steam:1100001079cf4ab', 'ephedrine', 0),
(1923, 'steam:1100001079cf4ab', 'grip', 0),
(1924, 'steam:1100001079cf4ab', 'scratchoff_used', 0),
(1925, 'steam:1100001079cf4ab', 'defibrillateur', 0),
(1926, 'steam:1100001079cf4ab', 'firstaidkit', 0),
(1927, 'steam:1100001079cf4ab', 'wrench', 0),
(1928, 'steam:1100001079cf4ab', 'chips', 0),
(1929, 'steam:1100001079cf4ab', 'burger', 0),
(1930, 'steam:1100001079cf4ab', 'boating_license', 0),
(1931, 'steam:1100001079cf4ab', 'mlic', 0),
(1932, 'steam:1100001079cf4ab', 'cdl', 0),
(1933, 'steam:1100001079cf4ab', 'pearl_pooch', 0),
(1934, 'steam:1100001079cf4ab', 'martini', 0),
(1935, 'steam:1100001079cf4ab', 'litter_pooch', 0),
(1936, 'steam:1100001079cf4ab', 'fishing_license', 0),
(1937, 'steam:1100001079cf4ab', 'lighter', 0),
(1938, 'steam:1100001079cf4ab', 'weapons_license3', 0),
(1939, 'steam:1100001079cf4ab', 'carokit', 0),
(1940, 'steam:1100001079cf4ab', 'weapons_license1', 0),
(1941, 'steam:1100001079cf4ab', 'vodkaenergy', 0),
(1942, 'steam:1100001079cf4ab', 'macka', 0),
(1943, 'steam:1100001079cf4ab', 'gold', 0),
(1944, 'steam:1100001079cf4ab', 'pizza', 0),
(1945, 'steam:1100001079cf4ab', 'rhum', 0),
(1946, 'steam:1100001079cf4ab', 'metreshooter', 0),
(1947, 'steam:1100001079cf4ab', 'taxi_license', 0),
(1948, 'steam:1100001079cf4ab', 'cigarett', 0),
(1949, 'steam:1100001079cf4ab', 'vegetables', 0),
(1950, 'steam:1100001079cf4ab', 'slaughtered_chicken', 0),
(1951, 'steam:1100001079cf4ab', 'bandage', 0),
(1952, 'steam:1100001079cf4ab', 'icetea', 0),
(1953, 'steam:1100001079cf4ab', 'yusuf', 0),
(1954, 'steam:1100001079cf4ab', '9mm_rounds', 0),
(1955, 'steam:1100001079cf4ab', 'sprite', 0),
(1956, 'steam:1100001079cf4ab', 'pcp', 0),
(1957, 'steam:1100001079cf4ab', 'radio', 0),
(1958, 'steam:1100001079cf4ab', 'WEAPON_BAT', 0),
(1959, 'steam:1100001079cf4ab', 'mojito', 0),
(1960, 'steam:1100001079cf4ab', 'painkiller', 0),
(1961, 'steam:1100001079cf4ab', 'coke', 0),
(1962, 'steam:1100001079cf4ab', 'pilot_license', 0),
(1963, 'steam:1100001079cf4ab', 'jagerbomb', 0),
(1964, 'steam:1100001079cf4ab', 'coke_pooch', 0),
(1965, 'steam:1100001079cf4ab', 'breathalyzer', 0),
(1966, 'steam:1100001079cf4ab', 'limonade', 0),
(1967, 'steam:1100001079cf4ab', 'crack', 0),
(1968, 'steam:1100001079cf4ab', 'donut', 0),
(1969, 'steam:1100001079cf4ab', 'contrat', 0),
(1970, 'steam:1100001079cf4ab', 'drpepper', 0),
(1971, 'steam:1100001079cf4ab', 'fabric', 0),
(1972, 'steam:1100001079cf4ab', 'beer', 0),
(1973, 'steam:1100001079cf4ab', 'whool', 0),
(1974, 'steam:1100001079cf4ab', 'iron', 0),
(1975, 'steam:1100001079cf4ab', 'teqpaf', 0),
(1976, 'steam:1100001079cf4ab', 'copper', 0),
(1977, 'steam:1100001079cf4ab', 'cocaine', 0),
(1978, 'steam:1100001079cf4ab', 'coca', 0),
(1979, 'steam:1100001079cf4ab', 'wood', 0),
(1980, 'steam:1100001079cf4ab', 'cheesebows', 0),
(1981, 'steam:1100001079cf4ab', 'clothe', 0),
(1982, 'steam:1100001079cf4ab', 'meth_pooch', 0),
(1983, 'steam:1100001079cf4ab', 'plongee1', 0),
(1984, 'steam:1100001079cf4ab', 'powerade', 0),
(1985, 'steam:1100001079cf4ab', 'gym_membership', 0),
(1986, 'steam:1100001079cf4ab', 'soda', 0),
(1987, 'steam:1100001079cf4ab', 'bread', 0),
(1988, 'steam:1100001079cf4ab', 'hunting_license', 0),
(1989, 'steam:1100001079cf4ab', 'golem', 0),
(1990, 'steam:1100001079cf4ab', 'diving_license', 0),
(1991, 'steam:1100001079cf4ab', 'lotteryticket', 0),
(1992, 'steam:1100001079cf4ab', 'water', 0),
(1993, 'steam:1100001079cf4ab', 'opium', 0),
(1994, 'steam:1100001079cf4ab', 'clip', 0),
(1995, 'steam:1100001079cf4ab', 'washed_stone', 0),
(1996, 'steam:11000010a078bc7', 'receipt', 0),
(1997, 'steam:11000010a078bc7', 'dlic', 0),
(1998, 'steam:11000010a078bc7', 'mlic', 0),
(1999, 'steam:11000010a078bc7', 'cdl', 0),
(2000, 'steam:11000010a078bc7', 'licenseplate', 0),
(2001, 'steam:11000010a078bc7', 'gasoline', 0),
(2002, 'steam:11000010a01bdb9', 'gasoline', 0),
(2003, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(2004, 'steam:11000010a01bdb9', 'golem', 0),
(2005, 'steam:11000010a01bdb9', 'rhum', 0),
(2006, 'steam:11000010a01bdb9', 'iron', 0),
(2007, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(2008, 'steam:11000010a01bdb9', 'yusuf', 0),
(2009, 'steam:11000010a01bdb9', 'pills', 0),
(2010, 'steam:11000010a01bdb9', 'drpepper', 0),
(2011, 'steam:11000010a01bdb9', 'silencieux', 0),
(2012, 'steam:11000010a01bdb9', 'hunting_license', 0),
(2013, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(2014, 'steam:11000010a01bdb9', 'turtle_pooch', 0),
(2015, 'steam:11000010a01bdb9', 'cannabis', 0),
(2016, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(2017, 'steam:11000010a01bdb9', 'marijuana', 0),
(2018, 'steam:11000010a01bdb9', 'WEAPON_FLASHLIGHT', 0),
(2019, 'steam:11000010a01bdb9', 'jusfruit', 0),
(2020, 'steam:11000010a01bdb9', 'firstaidkit', 0),
(2021, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(2022, 'steam:11000010a01bdb9', 'fishing_license', 0),
(2023, 'steam:11000010a01bdb9', 'metreshooter', 0),
(2024, 'steam:11000010a01bdb9', 'nitrocannister', 0),
(2025, 'steam:11000010a01bdb9', 'lotteryticket', 0),
(2026, 'steam:11000010a01bdb9', 'blowpipe', 0),
(2027, 'steam:11000010a01bdb9', 'croquettes', 0),
(2028, 'steam:11000010a01bdb9', 'litter_pooch', 0),
(2029, 'steam:11000010a01bdb9', 'scratchoff_used', 0),
(2030, 'steam:11000010a01bdb9', 'tequila', 0),
(2031, 'steam:11000010a01bdb9', 'cigarett', 10),
(2032, 'steam:11000010a01bdb9', 'WEAPON_BAT', 0),
(2033, 'steam:11000010a01bdb9', 'vegetables', 0),
(2034, 'steam:11000010a01bdb9', 'painkiller', 0),
(2035, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(2036, 'steam:11000010a01bdb9', 'petrol', 0),
(2037, 'steam:11000010a01bdb9', 'beer', 0),
(2038, 'steam:11000010a01bdb9', 'loka', 0),
(2039, 'steam:11000010a01bdb9', 'donut', 9),
(2040, 'steam:11000010a01bdb9', 'powerade', 0),
(2041, 'steam:11000010a01bdb9', 'martini', 0),
(2042, 'steam:11000010a01bdb9', 'pilot_license', 0),
(2043, 'steam:11000010a01bdb9', 'saucisson', 0),
(2044, 'steam:11000010a01bdb9', 'coca', 0),
(2045, 'steam:11000010a01bdb9', 'teqpaf', 0),
(2046, 'steam:11000010a01bdb9', 'armor', 0),
(2047, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(2048, 'steam:11000010a01bdb9', 'ephedrine', 0),
(2049, 'steam:11000010a01bdb9', 'menthe', 0),
(2050, 'steam:11000010a01bdb9', 'copper', 0),
(2051, 'steam:11000010a01bdb9', 'gazbottle', 0),
(2052, 'steam:11000010a01bdb9', 'ephedra', 0),
(2053, 'steam:11000010a01bdb9', 'stone', 0),
(2054, 'steam:11000010a01bdb9', 'washed_stone', 0),
(2055, 'steam:11000010a01bdb9', 'clothe', 0),
(2056, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(2057, 'steam:11000010a01bdb9', 'water', 0),
(2058, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(2059, 'steam:11000010a01bdb9', 'bandage', 0),
(2060, 'steam:11000010a01bdb9', 'gym_membership', 0),
(2061, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(2062, 'steam:11000010a01bdb9', 'pearl_pooch', 0),
(2063, 'steam:11000010a01bdb9', 'leather', 0),
(2064, 'steam:11000010a01bdb9', 'opium', 0),
(2065, 'steam:11000010a01bdb9', 'firstaidpass', 0),
(2066, 'steam:11000010a01bdb9', 'pcp', 0),
(2067, 'steam:11000010a01bdb9', 'ice', 0),
(2068, 'steam:11000010a01bdb9', 'diamond', 0),
(2069, 'steam:11000010a01bdb9', 'soda', 0),
(2070, 'steam:11000010a01bdb9', 'weed', 0),
(2071, 'steam:11000010a01bdb9', 'lockpick', 0),
(2072, 'steam:11000010a01bdb9', 'crack', 0),
(2073, 'steam:11000010a01bdb9', 'burger', 0),
(2074, 'steam:11000010a01bdb9', 'jager', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(2075, 'steam:11000010a01bdb9', 'coke', 0),
(2076, 'steam:11000010a01bdb9', 'radio', 1),
(2077, 'steam:11000010a01bdb9', 'vodka', 0),
(2078, 'steam:11000010a01bdb9', 'pearl', 0),
(2079, 'steam:11000010a01bdb9', 'pizza', 0),
(2080, 'steam:11000010a01bdb9', 'contrat', 0),
(2081, 'steam:11000010a01bdb9', 'bolpistache', 0),
(2082, 'steam:11000010a01bdb9', 'dabs', 0),
(2083, 'steam:11000010a01bdb9', 'WEAPON_STUNGUN', 0),
(2084, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(2085, 'steam:11000010a01bdb9', 'WEAPON_PUMPSHOTGUN', 0),
(2086, 'steam:11000010a01bdb9', 'protein_shake', 0),
(2087, 'steam:11000010a01bdb9', 'whiskey', 0),
(2088, 'steam:11000010a01bdb9', 'binoculars', 0),
(2089, 'steam:11000010a01bdb9', 'carokit', 0),
(2090, 'steam:11000010a01bdb9', 'sportlunch', 0),
(2091, 'steam:11000010a01bdb9', 'WEAPON_KNIFE', 0),
(2092, 'steam:11000010a01bdb9', 'shotgun_shells', 0),
(2093, 'steam:11000010a01bdb9', '9mm_rounds', 0),
(2094, 'steam:11000010a01bdb9', 'WEAPON_PISTOL', 0),
(2095, 'steam:11000010a01bdb9', 'defibrillateur', 0),
(2096, 'steam:11000010a01bdb9', 'wrench', 0),
(2097, 'steam:11000010a01bdb9', 'fanta', 0),
(2098, 'steam:11000010a01bdb9', 'clip', 6),
(2099, 'steam:11000010a01bdb9', 'meth', 0),
(2100, 'steam:11000010a01bdb9', 'flashlight', 0),
(2101, 'steam:11000010a01bdb9', 'whool', 0),
(2102, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(2103, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(2104, 'steam:11000010a01bdb9', 'fish', 0),
(2105, 'steam:11000010a01bdb9', 'turtle', 0),
(2106, 'steam:11000010a01bdb9', 'taxi_license', 0),
(2107, 'steam:11000010a01bdb9', 'marriage_license', 0),
(2108, 'steam:11000010a01bdb9', 'mojito', 0),
(2109, 'steam:11000010a01bdb9', 'carotool', 0),
(2110, 'steam:11000010a01bdb9', 'diving_license', 0),
(2111, 'steam:11000010a01bdb9', 'macka', 0),
(2112, 'steam:11000010a01bdb9', 'pastacarbonara', 0),
(2113, 'steam:11000010a01bdb9', 'baconburger', 0),
(2114, 'steam:11000010a01bdb9', 'meat', 0),
(2115, 'steam:11000010a01bdb9', 'marabou', 0),
(2116, 'steam:11000010a01bdb9', 'lsd', 0),
(2117, 'steam:11000010a01bdb9', 'chips', 0),
(2118, 'steam:11000010a01bdb9', 'cocacola', 0),
(2119, 'steam:11000010a01bdb9', 'boating_license', 0),
(2120, 'steam:11000010a01bdb9', 'limonade', 0),
(2121, 'steam:11000010a01bdb9', 'heroine', 0),
(2122, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(2123, 'steam:11000010a01bdb9', 'icetea', 0),
(2124, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(2125, 'steam:11000010a01bdb9', 'drugtest', 0),
(2126, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(2127, 'steam:11000010a01bdb9', 'litter', 0),
(2128, 'steam:11000010a01bdb9', 'cocaine', 0),
(2129, 'steam:11000010a01bdb9', 'cheesebows', 0),
(2130, 'steam:11000010a01bdb9', 'cola', 0),
(2131, 'steam:11000010a01bdb9', 'boitier', 0),
(2132, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(2133, 'steam:11000010a01bdb9', 'fixkit', 0),
(2134, 'steam:11000010a01bdb9', 'narcan', 0),
(2135, 'steam:11000010a01bdb9', 'blackberry', 0),
(2136, 'steam:11000010a01bdb9', 'fakepee', 0),
(2137, 'steam:11000010a01bdb9', 'poppy', 0),
(2138, 'steam:11000010a01bdb9', 'gold', 0),
(2139, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(2140, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(2141, 'steam:11000010a01bdb9', 'scratchoff', 0),
(2142, 'steam:11000010a01bdb9', 'plongee1', 0),
(2143, 'steam:11000010a01bdb9', 'coffee', 10),
(2144, 'steam:11000010a01bdb9', 'lighter', 1),
(2145, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(2146, 'steam:11000010a01bdb9', 'medikit', 1),
(2147, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(2148, 'steam:11000010a01bdb9', 'energy', 0),
(2149, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(2150, 'steam:11000010a01bdb9', 'tacos', 0),
(2151, 'steam:11000010a01bdb9', 'plongee2', 0),
(2152, 'steam:11000010a01bdb9', 'sprite', 0),
(2153, 'steam:11000010a01bdb9', 'wood', 0),
(2154, 'steam:11000010a01bdb9', 'fabric', 0),
(2155, 'steam:11000010a01bdb9', 'grip', 0),
(2156, 'steam:11000010a01bdb9', 'bolchips', 0),
(2157, 'steam:11000010a01bdb9', 'bread', 0),
(2158, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(2159, 'steam:11000010a01bdb9', 'mixapero', 0),
(2160, 'steam:11000010a01bdb9', 'fixtool', 0),
(2161, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(2162, 'steam:11000010a01bdb9', 'whisky', 0),
(2163, 'steam:1100001068ef13c', 'weed_pooch', 0),
(2164, 'steam:1100001068ef13c', 'donut', 0),
(2165, 'steam:1100001068ef13c', 'lsd_pooch', 0),
(2166, 'steam:1100001068ef13c', 'cocacola', 6),
(2167, 'steam:1100001068ef13c', 'rhum', 0),
(2168, 'steam:1100001068ef13c', 'heroine', 0),
(2169, 'steam:1100001068ef13c', 'bread', 0),
(2170, 'steam:1100001068ef13c', 'lotteryticket', 4),
(2171, 'steam:1100001068ef13c', 'washed_stone', 0),
(2172, 'steam:1100001068ef13c', 'fixtool', 0),
(2173, 'steam:1100001068ef13c', 'tequila', 0),
(2174, 'steam:1100001068ef13c', 'pcp', 0),
(2175, 'steam:1100001068ef13c', 'nitrocannister', 0),
(2176, 'steam:1100001068ef13c', 'ephedrine', 0),
(2177, 'steam:1100001068ef13c', 'bolnoixcajou', 0),
(2178, 'steam:1100001068ef13c', 'fanta', 0),
(2179, 'steam:1100001068ef13c', 'rhumfruit', 0),
(2180, 'steam:1100001068ef13c', 'baconburger', 0),
(2181, 'steam:1100001068ef13c', 'turtle_pooch', 0),
(2182, 'steam:1100001068ef13c', 'fakepee', 0),
(2183, 'steam:1100001068ef13c', 'menthe', 0),
(2184, 'steam:1100001068ef13c', 'yusuf', 0),
(2185, 'steam:1100001068ef13c', 'alive_chicken', 0),
(2186, 'steam:1100001068ef13c', 'diamond', 0),
(2187, 'steam:1100001068ef13c', 'cannabis', 0),
(2188, 'steam:1100001068ef13c', 'marriage_license', 0),
(2189, 'steam:1100001068ef13c', 'WEAPON_BAT', 0),
(2190, 'steam:1100001068ef13c', '9mm_rounds', 0),
(2191, 'steam:1100001068ef13c', 'crack', 0),
(2192, 'steam:1100001068ef13c', 'blackberry', 0),
(2193, 'steam:1100001068ef13c', 'saucisson', 0),
(2194, 'steam:1100001068ef13c', 'pearl', 0),
(2195, 'steam:1100001068ef13c', 'lsd', 0),
(2196, 'steam:1100001068ef13c', 'copper', 0),
(2197, 'steam:1100001068ef13c', 'lockpick', 0),
(2198, 'steam:1100001068ef13c', 'golem', 0),
(2199, 'steam:1100001068ef13c', 'diving_license', 0),
(2200, 'steam:1100001068ef13c', 'fish', 0),
(2201, 'steam:1100001068ef13c', 'clip', 0),
(2202, 'steam:1100001068ef13c', 'gazbottle', 0),
(2203, 'steam:1100001068ef13c', 'icetea', 0),
(2204, 'steam:1100001068ef13c', 'jagerbomb', 0),
(2205, 'steam:1100001068ef13c', 'fishing_license', 0),
(2206, 'steam:1100001068ef13c', 'burger', 0),
(2207, 'steam:1100001068ef13c', 'clothe', 0),
(2208, 'steam:1100001068ef13c', 'gold', 0),
(2209, 'steam:1100001068ef13c', 'carokit', 0),
(2210, 'steam:1100001068ef13c', 'vodkaenergy', 0),
(2211, 'steam:1100001068ef13c', 'opium_pooch', 0),
(2212, 'steam:1100001068ef13c', 'jusfruit', 0),
(2213, 'steam:1100001068ef13c', 'whiskycoca', 0),
(2214, 'steam:1100001068ef13c', 'tacos', 0),
(2215, 'steam:1100001068ef13c', 'gym_membership', 0),
(2216, 'steam:1100001068ef13c', 'boating_license', 0),
(2217, 'steam:1100001068ef13c', 'energy', 0),
(2218, 'steam:1100001068ef13c', 'pilot_license', 0),
(2219, 'steam:1100001068ef13c', 'bolchips', 0),
(2220, 'steam:1100001068ef13c', 'lighter', 1),
(2221, 'steam:1100001068ef13c', 'coke', 0),
(2222, 'steam:1100001068ef13c', 'carotool', 0),
(2223, 'steam:1100001068ef13c', 'slaughtered_chicken', 0),
(2224, 'steam:1100001068ef13c', 'plongee1', 0),
(2225, 'steam:1100001068ef13c', 'croquettes', 0),
(2226, 'steam:1100001068ef13c', 'sprite', 0),
(2227, 'steam:1100001068ef13c', 'painkiller', 0),
(2228, 'steam:1100001068ef13c', 'drpepper', 0),
(2229, 'steam:1100001068ef13c', 'ice', 0),
(2230, 'steam:1100001068ef13c', 'bandage', 11),
(2231, 'steam:1100001068ef13c', 'chips', 0),
(2232, 'steam:1100001068ef13c', 'boitier', 0),
(2233, 'steam:1100001068ef13c', 'cola', 0),
(2234, 'steam:1100001068ef13c', 'plongee2', 0),
(2235, 'steam:1100001068ef13c', 'vegetables', 0),
(2236, 'steam:1100001068ef13c', 'powerade', 0),
(2237, 'steam:1100001068ef13c', 'cutted_wood', 0),
(2238, 'steam:1100001068ef13c', 'weed', 0),
(2239, 'steam:1100001068ef13c', 'coke_pooch', 0),
(2240, 'steam:1100001068ef13c', 'litter_pooch', 0),
(2241, 'steam:1100001068ef13c', 'bolpistache', 0),
(2242, 'steam:1100001068ef13c', 'protein_shake', 0),
(2243, 'steam:1100001068ef13c', 'grapperaisin', 0),
(2244, 'steam:1100001068ef13c', 'blowpipe', 0),
(2245, 'steam:1100001068ef13c', 'limonade', 0),
(2246, 'steam:1100001068ef13c', 'coca', 0),
(2247, 'steam:1100001068ef13c', 'mixapero', 0),
(2248, 'steam:1100001068ef13c', 'pizza', 0),
(2249, 'steam:1100001068ef13c', 'meat', 0),
(2250, 'steam:1100001068ef13c', 'vodka', 0),
(2251, 'steam:1100001068ef13c', 'defibrillateur', 0),
(2252, 'steam:1100001068ef13c', 'ephedra', 0),
(2253, 'steam:1100001068ef13c', 'marabou', 0),
(2254, 'steam:1100001068ef13c', 'silencieux', 0),
(2255, 'steam:1100001068ef13c', 'whool', 0),
(2256, 'steam:1100001068ef13c', 'armor', 0),
(2257, 'steam:1100001068ef13c', 'shotgun_shells', 0),
(2258, 'steam:1100001068ef13c', 'WEAPON_PUMPSHOTGUN', 0),
(2259, 'steam:1100001068ef13c', 'WEAPON_PISTOL', 0),
(2260, 'steam:1100001068ef13c', 'WEAPON_KNIFE', 0),
(2261, 'steam:1100001068ef13c', 'litter', 0),
(2262, 'steam:1100001068ef13c', 'WEAPON_STUNGUN', 0),
(2263, 'steam:1100001068ef13c', 'WEAPON_FLASHLIGHT', 0),
(2264, 'steam:1100001068ef13c', 'radio', 1),
(2265, 'steam:1100001068ef13c', 'fabric', 0),
(2266, 'steam:1100001068ef13c', 'weapons_license3', 0),
(2267, 'steam:1100001068ef13c', 'wrench', 0),
(2268, 'steam:1100001068ef13c', 'pastacarbonara', 0),
(2269, 'steam:1100001068ef13c', 'turtle', 0),
(2270, 'steam:1100001068ef13c', 'firstaidpass', 0),
(2271, 'steam:1100001068ef13c', 'packaged_plank', 0),
(2272, 'steam:1100001068ef13c', 'sportlunch', 0),
(2273, 'steam:1100001068ef13c', 'whisky', 0),
(2274, 'steam:1100001068ef13c', 'petrol', 0),
(2275, 'steam:1100001068ef13c', 'jager', 0),
(2276, 'steam:1100001068ef13c', 'martini', 0),
(2277, 'steam:1100001068ef13c', 'medikit', 95),
(2278, 'steam:1100001068ef13c', 'taxi_license', 0),
(2279, 'steam:1100001068ef13c', 'hunting_license', 0),
(2280, 'steam:1100001068ef13c', 'contrat', 0),
(2281, 'steam:1100001068ef13c', 'firstaidkit', 0),
(2282, 'steam:1100001068ef13c', 'breathalyzer', 0),
(2283, 'steam:1100001068ef13c', 'weapons_license2', 0),
(2284, 'steam:1100001068ef13c', 'mojito', 0),
(2285, 'steam:1100001068ef13c', 'beer', 0),
(2286, 'steam:1100001068ef13c', 'macka', 0),
(2287, 'steam:1100001068ef13c', 'cheesebows', 7),
(2288, 'steam:1100001068ef13c', 'water', 0),
(2289, 'steam:1100001068ef13c', 'loka', 0),
(2290, 'steam:1100001068ef13c', 'bolcacahuetes', 0),
(2291, 'steam:1100001068ef13c', 'wood', 1),
(2292, 'steam:1100001068ef13c', 'meth_pooch', 0),
(2293, 'steam:1100001068ef13c', 'narcan', 0),
(2294, 'steam:1100001068ef13c', 'dabs', 0),
(2295, 'steam:1100001068ef13c', 'cocaine', 0),
(2296, 'steam:1100001068ef13c', 'weapons_license1', 0),
(2297, 'steam:1100001068ef13c', 'fixkit', 0),
(2298, 'steam:1100001068ef13c', 'petrol_raffin', 0),
(2299, 'steam:1100001068ef13c', 'flashlight', 0),
(2300, 'steam:1100001068ef13c', 'whiskey', 0),
(2301, 'steam:1100001068ef13c', 'drugtest', 0),
(2302, 'steam:1100001068ef13c', 'rhumcoca', 0),
(2303, 'steam:1100001068ef13c', 'iron', 0),
(2304, 'steam:1100001068ef13c', 'poppy', 0),
(2305, 'steam:1100001068ef13c', 'marijuana', 0),
(2306, 'steam:1100001068ef13c', 'pills', 0),
(2307, 'steam:1100001068ef13c', 'packaged_chicken', 0),
(2308, 'steam:1100001068ef13c', 'scratchoff_used', 0),
(2309, 'steam:1100001068ef13c', 'scratchoff', 0),
(2310, 'steam:1100001068ef13c', 'vodkafruit', 0),
(2311, 'steam:1100001068ef13c', 'binoculars', 0),
(2312, 'steam:1100001068ef13c', 'soda', 0),
(2313, 'steam:1100001068ef13c', 'cigarett', 3),
(2314, 'steam:1100001068ef13c', 'meth', 0),
(2315, 'steam:1100001068ef13c', 'coffee', 0),
(2316, 'steam:1100001068ef13c', 'grip', 0),
(2317, 'steam:1100001068ef13c', 'teqpaf', 0),
(2318, 'steam:1100001068ef13c', 'opium', 0),
(2319, 'steam:1100001068ef13c', 'stone', 0),
(2320, 'steam:1100001068ef13c', 'pearl_pooch', 0),
(2321, 'steam:1100001068ef13c', 'metreshooter', 0),
(2322, 'steam:1100001068ef13c', 'leather', 0),
(2323, 'steam:110000132580eb0', 'WEAPON_PISTOL', 0),
(2324, 'steam:110000132580eb0', 'gold', 0),
(2325, 'steam:110000132580eb0', 'mojito', 0),
(2326, 'steam:110000132580eb0', 'weed', 0),
(2327, 'steam:110000132580eb0', 'heroine', 0),
(2328, 'steam:110000132580eb0', 'turtle_pooch', 0),
(2329, 'steam:110000132580eb0', 'tacos', 0),
(2330, 'steam:110000132580eb0', 'clip', 0),
(2331, 'steam:110000132580eb0', 'protein_shake', 0),
(2332, 'steam:110000132580eb0', 'vodkaenergy', 0),
(2333, 'steam:110000132580eb0', 'plongee2', 0),
(2334, 'steam:110000132580eb0', 'flashlight', 0),
(2335, 'steam:110000132580eb0', 'plongee1', 0),
(2336, 'steam:110000132580eb0', 'pills', 0),
(2337, 'steam:110000132580eb0', 'meth_pooch', 0),
(2338, 'steam:110000132580eb0', 'breathalyzer', 0),
(2339, 'steam:110000132580eb0', 'mixapero', 0),
(2340, 'steam:110000132580eb0', 'teqpaf', 0),
(2341, 'steam:110000132580eb0', 'baconburger', 3),
(2342, 'steam:110000132580eb0', 'coke', 0),
(2343, 'steam:110000132580eb0', 'fixtool', 0),
(2344, 'steam:110000132580eb0', 'beer', 0),
(2345, 'steam:110000132580eb0', 'scratchoff', 0),
(2346, 'steam:110000132580eb0', 'yusuf', 0),
(2347, 'steam:110000132580eb0', 'litter', 0),
(2348, 'steam:110000132580eb0', 'rhum', 0),
(2349, 'steam:110000132580eb0', 'whiskycoca', 0),
(2350, 'steam:110000132580eb0', 'binoculars', 0),
(2351, 'steam:110000132580eb0', 'fishing_license', 0),
(2352, 'steam:110000132580eb0', 'bolchips', 0),
(2353, 'steam:110000132580eb0', 'croquettes', 0),
(2354, 'steam:110000132580eb0', 'weed_pooch', 0),
(2355, 'steam:110000132580eb0', 'WEAPON_PUMPSHOTGUN', 0),
(2356, 'steam:110000132580eb0', 'WEAPON_STUNGUN', 0),
(2357, 'steam:110000132580eb0', 'firstaidpass', 0),
(2358, 'steam:110000132580eb0', 'coke_pooch', 0),
(2359, 'steam:110000132580eb0', 'fish', 0),
(2360, 'steam:110000132580eb0', 'vodka', 0),
(2361, 'steam:110000132580eb0', 'bolpistache', 0),
(2362, 'steam:110000132580eb0', 'alive_chicken', 0),
(2363, 'steam:110000132580eb0', 'loka', 4),
(2364, 'steam:110000132580eb0', 'pizza', 0),
(2365, 'steam:110000132580eb0', 'whisky', 0),
(2366, 'steam:110000132580eb0', 'gym_membership', 0),
(2367, 'steam:110000132580eb0', 'boating_license', 0),
(2368, 'steam:110000132580eb0', 'meat', 0),
(2369, 'steam:110000132580eb0', 'WEAPON_BAT', 0),
(2370, 'steam:110000132580eb0', 'pilot_license', 0),
(2371, 'steam:110000132580eb0', 'limonade', 0),
(2372, 'steam:110000132580eb0', 'petrol_raffin', 0),
(2373, 'steam:110000132580eb0', 'menthe', 0),
(2374, 'steam:110000132580eb0', 'saucisson', 0),
(2375, 'steam:110000132580eb0', 'WEAPON_FLASHLIGHT', 0),
(2376, 'steam:110000132580eb0', 'burger', 0),
(2377, 'steam:110000132580eb0', 'rhumfruit', 0),
(2378, 'steam:110000132580eb0', 'iron', 0),
(2379, 'steam:110000132580eb0', 'drugtest', 0),
(2380, 'steam:110000132580eb0', 'wrench', 0),
(2381, 'steam:110000132580eb0', 'donut', 3),
(2382, 'steam:110000132580eb0', 'whool', 0),
(2383, 'steam:110000132580eb0', 'fixkit', 0),
(2384, 'steam:110000132580eb0', 'boitier', 0),
(2385, 'steam:110000132580eb0', 'clothe', 0),
(2386, 'steam:110000132580eb0', 'pcp', 0),
(2387, 'steam:110000132580eb0', 'lsd', 0),
(2388, 'steam:110000132580eb0', 'cutted_wood', 0),
(2389, 'steam:110000132580eb0', 'medikit', 4),
(2390, 'steam:110000132580eb0', 'contrat', 0),
(2391, 'steam:110000132580eb0', 'taxi_license', 0),
(2392, 'steam:110000132580eb0', 'diamond', 0),
(2393, 'steam:110000132580eb0', 'bread', 0),
(2394, 'steam:110000132580eb0', 'carotool', 0),
(2395, 'steam:110000132580eb0', 'washed_stone', 0),
(2396, 'steam:110000132580eb0', 'lighter', 1),
(2397, 'steam:110000132580eb0', 'rhumcoca', 0),
(2398, 'steam:110000132580eb0', 'bolcacahuetes', 0),
(2399, 'steam:110000132580eb0', 'grapperaisin', 0),
(2400, 'steam:110000132580eb0', 'dabs', 0),
(2401, 'steam:110000132580eb0', 'cocacola', 0),
(2402, 'steam:110000132580eb0', 'meth', 0),
(2403, 'steam:110000132580eb0', 'sprite', 0),
(2404, 'steam:110000132580eb0', 'sportlunch', 0),
(2405, 'steam:110000132580eb0', 'martini', 0),
(2406, 'steam:110000132580eb0', 'litter_pooch', 0),
(2407, 'steam:110000132580eb0', 'drpepper', 0),
(2408, 'steam:110000132580eb0', 'pastacarbonara', 0),
(2409, 'steam:110000132580eb0', 'vegetables', 0),
(2410, 'steam:110000132580eb0', 'bandage', 0),
(2411, 'steam:110000132580eb0', 'weapons_license1', 0),
(2412, 'steam:110000132580eb0', 'ice', 0),
(2413, 'steam:110000132580eb0', 'wood', 0),
(2414, 'steam:110000132580eb0', 'jagerbomb', 0),
(2415, 'steam:110000132580eb0', 'icetea', 0),
(2416, 'steam:110000132580eb0', 'diving_license', 0),
(2417, 'steam:110000132580eb0', 'WEAPON_KNIFE', 0),
(2418, 'steam:110000132580eb0', 'water', 0),
(2419, 'steam:110000132580eb0', 'radio', 1),
(2420, 'steam:110000132580eb0', 'defibrillateur', 0),
(2421, 'steam:110000132580eb0', 'hunting_license', 0),
(2422, 'steam:110000132580eb0', 'nitrocannister', 0),
(2423, 'steam:110000132580eb0', 'marabou', 0),
(2424, 'steam:110000132580eb0', 'lotteryticket', 0),
(2425, 'steam:110000132580eb0', 'coca', 0),
(2426, 'steam:110000132580eb0', 'powerade', 0),
(2427, 'steam:110000132580eb0', 'metreshooter', 0),
(2428, 'steam:110000132580eb0', 'jager', 0),
(2429, 'steam:110000132580eb0', 'marriage_license', 0),
(2430, 'steam:110000132580eb0', 'firstaidkit', 0),
(2431, 'steam:110000132580eb0', 'stone', 0),
(2432, 'steam:110000132580eb0', 'weapons_license2', 0),
(2433, 'steam:110000132580eb0', 'shotgun_shells', 0),
(2434, 'steam:110000132580eb0', 'fakepee', 0),
(2435, 'steam:110000132580eb0', 'macka', 0),
(2436, 'steam:110000132580eb0', 'chips', 0),
(2437, 'steam:110000132580eb0', 'cheesebows', 0),
(2438, 'steam:110000132580eb0', 'vodkafruit', 0),
(2439, 'steam:110000132580eb0', 'carokit', 0),
(2440, 'steam:110000132580eb0', 'fanta', 0),
(2441, 'steam:110000132580eb0', 'fabric', 0),
(2442, 'steam:110000132580eb0', 'lockpick', 0),
(2443, 'steam:110000132580eb0', 'copper', 0),
(2444, 'steam:110000132580eb0', 'soda', 0),
(2445, 'steam:110000132580eb0', 'painkiller', 0),
(2446, 'steam:110000132580eb0', 'cocaine', 0),
(2447, 'steam:110000132580eb0', 'coffee', 0),
(2448, 'steam:110000132580eb0', 'tequila', 0),
(2449, 'steam:110000132580eb0', 'bolnoixcajou', 0),
(2450, 'steam:110000132580eb0', 'jusfruit', 0),
(2451, 'steam:110000132580eb0', 'golem', 0),
(2452, 'steam:110000132580eb0', 'crack', 0),
(2453, 'steam:110000132580eb0', 'ephedrine', 0),
(2454, 'steam:110000132580eb0', 'petrol', 0),
(2455, 'steam:110000132580eb0', 'whiskey', 0),
(2456, 'steam:110000132580eb0', 'poppy', 0),
(2457, 'steam:110000132580eb0', 'narcan', 0),
(2458, 'steam:110000132580eb0', 'gazbottle', 0),
(2459, 'steam:110000132580eb0', 'blackberry', 0),
(2460, 'steam:110000132580eb0', 'turtle', 0),
(2461, 'steam:110000132580eb0', 'ephedra', 0),
(2462, 'steam:110000132580eb0', 'weapons_license3', 0),
(2463, 'steam:110000132580eb0', 'blowpipe', 0),
(2464, 'steam:110000132580eb0', '9mm_rounds', 0),
(2465, 'steam:110000132580eb0', 'opium', 0),
(2466, 'steam:110000132580eb0', 'grip', 0),
(2467, 'steam:110000132580eb0', 'slaughtered_chicken', 0),
(2468, 'steam:110000132580eb0', 'cannabis', 0),
(2469, 'steam:110000132580eb0', 'packaged_plank', 0),
(2470, 'steam:110000132580eb0', 'cola', 0),
(2471, 'steam:110000132580eb0', 'leather', 0),
(2472, 'steam:110000132580eb0', 'armor', 0),
(2473, 'steam:110000132580eb0', 'scratchoff_used', 0),
(2474, 'steam:110000132580eb0', 'silencieux', 0),
(2475, 'steam:110000132580eb0', 'packaged_chicken', 0),
(2476, 'steam:110000132580eb0', 'cigarett', 1),
(2477, 'steam:110000132580eb0', 'lsd_pooch', 0),
(2478, 'steam:110000132580eb0', 'pearl_pooch', 0),
(2479, 'steam:110000132580eb0', 'energy', 0),
(2480, 'steam:110000132580eb0', 'opium_pooch', 0),
(2481, 'steam:110000132580eb0', 'pearl', 0),
(2482, 'steam:110000132580eb0', 'marijuana', 0),
(2483, 'steam:110000112969e8f', 'leather', 0),
(2484, 'steam:110000112969e8f', 'martini', 0),
(2485, 'steam:110000112969e8f', 'firstaidkit', 0),
(2486, 'steam:110000112969e8f', 'wrench', 0),
(2487, 'steam:110000112969e8f', 'heroine', 0),
(2488, 'steam:110000112969e8f', 'grapperaisin', 0),
(2489, 'steam:110000112969e8f', 'flashlight', 0),
(2490, 'steam:110000112969e8f', 'marijuana', 0),
(2491, 'steam:110000112969e8f', 'yusuf', 0),
(2492, 'steam:110000112969e8f', 'hunting_license', 0),
(2493, 'steam:110000112969e8f', 'whisky', 0),
(2494, 'steam:110000112969e8f', 'fabric', 0),
(2495, 'steam:110000112969e8f', 'stone', 0),
(2496, 'steam:110000112969e8f', 'protein_shake', 0),
(2497, 'steam:110000112969e8f', 'fanta', 0),
(2498, 'steam:110000112969e8f', 'scratchoff', 0),
(2499, 'steam:110000112969e8f', 'bolcacahuetes', 0),
(2500, 'steam:110000112969e8f', 'carotool', 0),
(2501, 'steam:110000112969e8f', 'whiskycoca', 0),
(2502, 'steam:110000112969e8f', 'drugtest', 0),
(2503, 'steam:110000112969e8f', 'pastacarbonara', 0),
(2504, 'steam:110000112969e8f', 'vegetables', 0),
(2505, 'steam:110000112969e8f', 'scratchoff_used', 0),
(2506, 'steam:110000112969e8f', 'cutted_wood', 0),
(2507, 'steam:110000112969e8f', 'nitrocannister', 0),
(2508, 'steam:110000112969e8f', 'teqpaf', 0),
(2509, 'steam:110000112969e8f', 'coke_pooch', 0),
(2510, 'steam:110000112969e8f', 'limonade', 0),
(2511, 'steam:110000112969e8f', 'bandage', 0),
(2512, 'steam:110000112969e8f', 'pizza', 0),
(2513, 'steam:110000112969e8f', 'meat', 0),
(2514, 'steam:110000112969e8f', 'meth_pooch', 0),
(2515, 'steam:110000112969e8f', 'bolchips', 0),
(2516, 'steam:110000112969e8f', 'cigarett', 0),
(2517, 'steam:110000112969e8f', 'defibrillateur', 0),
(2518, 'steam:110000112969e8f', 'saucisson', 0),
(2519, 'steam:110000112969e8f', 'grip', 0),
(2520, 'steam:110000112969e8f', 'soda', 0),
(2521, 'steam:110000112969e8f', 'weed_pooch', 0),
(2522, 'steam:110000112969e8f', 'lockpick', 0),
(2523, 'steam:110000112969e8f', 'tacos', 0),
(2524, 'steam:110000112969e8f', 'rhumfruit', 0),
(2525, 'steam:110000112969e8f', 'cocacola', 0),
(2526, 'steam:110000112969e8f', 'contrat', 0),
(2527, 'steam:110000112969e8f', 'pearl', 0),
(2528, 'steam:110000112969e8f', 'dabs', 0),
(2529, 'steam:110000112969e8f', 'meth', 0),
(2530, 'steam:110000112969e8f', 'lotteryticket', 1),
(2531, 'steam:110000112969e8f', 'jagerbomb', 0),
(2532, 'steam:110000112969e8f', 'breathalyzer', 0),
(2533, 'steam:110000112969e8f', 'vodkafruit', 0),
(2534, 'steam:110000112969e8f', 'loka', 0),
(2535, 'steam:110000112969e8f', 'whool', 0),
(2536, 'steam:110000112969e8f', 'beer', 0),
(2537, 'steam:110000112969e8f', 'pcp', 0),
(2538, 'steam:110000112969e8f', 'pearl_pooch', 0),
(2539, 'steam:110000112969e8f', 'gazbottle', 0),
(2540, 'steam:110000112969e8f', 'powerade', 0),
(2541, 'steam:110000112969e8f', 'weapons_license3', 0),
(2542, 'steam:110000112969e8f', 'diving_license', 0),
(2543, 'steam:110000112969e8f', 'vodka', 0),
(2544, 'steam:110000112969e8f', 'packaged_plank', 0),
(2545, 'steam:110000112969e8f', 'turtle_pooch', 0),
(2546, 'steam:110000112969e8f', 'marabou', 0),
(2547, 'steam:110000112969e8f', 'macka', 0),
(2548, 'steam:110000112969e8f', 'boitier', 0),
(2549, 'steam:110000112969e8f', 'clip', 0),
(2550, 'steam:110000112969e8f', 'sportlunch', 0),
(2551, 'steam:110000112969e8f', 'plongee1', 0),
(2552, 'steam:110000112969e8f', '9mm_rounds', 0),
(2553, 'steam:110000112969e8f', 'marriage_license', 0),
(2554, 'steam:110000112969e8f', 'tequila', 0),
(2555, 'steam:110000112969e8f', 'menthe', 0),
(2556, 'steam:110000112969e8f', 'cocaine', 0),
(2557, 'steam:110000112969e8f', 'gold', 0),
(2558, 'steam:110000112969e8f', 'wood', 0),
(2559, 'steam:110000112969e8f', 'weapons_license1', 0),
(2560, 'steam:110000112969e8f', 'fixkit', 0),
(2561, 'steam:110000112969e8f', 'iron', 0),
(2562, 'steam:110000112969e8f', 'turtle', 0),
(2563, 'steam:110000112969e8f', 'clothe', 0),
(2564, 'steam:110000112969e8f', 'cola', 0),
(2565, 'steam:110000112969e8f', 'lsd', 0),
(2566, 'steam:110000112969e8f', 'fixtool', 0),
(2567, 'steam:110000112969e8f', 'vodkaenergy', 0),
(2568, 'steam:110000112969e8f', 'slaughtered_chicken', 0),
(2569, 'steam:110000112969e8f', 'lsd_pooch', 0),
(2570, 'steam:110000112969e8f', 'copper', 0),
(2571, 'steam:110000112969e8f', 'jusfruit', 0),
(2572, 'steam:110000112969e8f', 'golem', 0),
(2573, 'steam:110000112969e8f', 'litter_pooch', 0),
(2574, 'steam:110000112969e8f', 'sprite', 0),
(2575, 'steam:110000112969e8f', 'licenseplate', 1),
(2576, 'steam:110000112969e8f', 'shotgun_shells', 0),
(2577, 'steam:110000112969e8f', 'ephedrine', 0),
(2578, 'steam:110000112969e8f', 'silencieux', 0),
(2579, 'steam:110000112969e8f', 'donut', 0),
(2580, 'steam:110000112969e8f', 'WEAPON_PUMPSHOTGUN', 0),
(2581, 'steam:110000112969e8f', 'WEAPON_PISTOL', 0),
(2582, 'steam:110000112969e8f', 'crack', 0),
(2583, 'steam:110000112969e8f', 'rhum', 0),
(2584, 'steam:110000112969e8f', 'bolpistache', 0),
(2585, 'steam:110000112969e8f', 'WEAPON_BAT', 0),
(2586, 'steam:110000112969e8f', 'WEAPON_KNIFE', 0),
(2587, 'steam:110000112969e8f', 'WEAPON_STUNGUN', 0),
(2588, 'steam:110000112969e8f', 'diamond', 0),
(2589, 'steam:110000112969e8f', 'ephedra', 0),
(2590, 'steam:110000112969e8f', 'WEAPON_FLASHLIGHT', 0),
(2591, 'steam:110000112969e8f', 'cheesebows', 0),
(2592, 'steam:110000112969e8f', 'chips', 0),
(2593, 'steam:110000112969e8f', 'weed', 0),
(2594, 'steam:110000112969e8f', 'washed_stone', 0),
(2595, 'steam:110000112969e8f', 'radio', 1),
(2596, 'steam:110000112969e8f', 'firstaidpass', 0),
(2597, 'steam:110000112969e8f', 'boating_license', 0),
(2598, 'steam:110000112969e8f', 'drpepper', 0),
(2599, 'steam:110000112969e8f', 'taxi_license', 0),
(2600, 'steam:110000112969e8f', 'burger', 0),
(2601, 'steam:110000112969e8f', 'plongee2', 0),
(2602, 'steam:110000112969e8f', 'pills', 0),
(2603, 'steam:110000112969e8f', 'pilot_license', 0),
(2604, 'steam:110000112969e8f', 'petrol', 0),
(2605, 'steam:110000112969e8f', 'fishing_license', 0),
(2606, 'steam:110000112969e8f', 'weapons_license2', 0),
(2607, 'steam:110000112969e8f', 'baconburger', 0),
(2608, 'steam:110000112969e8f', 'cannabis', 0),
(2609, 'steam:110000112969e8f', 'opium', 0),
(2610, 'steam:110000112969e8f', 'croquettes', 0),
(2611, 'steam:110000112969e8f', 'icetea', 0),
(2612, 'steam:110000112969e8f', 'jager', 0),
(2613, 'steam:110000112969e8f', 'petrol_raffin', 0),
(2614, 'steam:110000112969e8f', 'narcan', 0),
(2615, 'steam:110000112969e8f', 'gym_membership', 0),
(2616, 'steam:110000112969e8f', 'painkiller', 0),
(2617, 'steam:110000112969e8f', 'coffee', 0),
(2618, 'steam:110000112969e8f', 'armor', 0),
(2619, 'steam:110000112969e8f', 'rhumcoca', 0),
(2620, 'steam:110000112969e8f', 'fakepee', 0),
(2621, 'steam:110000112969e8f', 'whiskey', 0),
(2622, 'steam:110000112969e8f', 'alive_chicken', 0),
(2623, 'steam:110000112969e8f', 'poppy', 0),
(2624, 'steam:110000112969e8f', 'fish', 0),
(2625, 'steam:110000112969e8f', 'mixapero', 0),
(2626, 'steam:110000112969e8f', 'gasoline', 0),
(2627, 'steam:110000112969e8f', 'coke', 0),
(2628, 'steam:110000112969e8f', 'packaged_chicken', 0),
(2629, 'steam:110000112969e8f', 'mojito', 0),
(2630, 'steam:110000112969e8f', 'medikit', 0),
(2631, 'steam:110000112969e8f', 'carokit', 0),
(2632, 'steam:110000112969e8f', 'bread', 0),
(2633, 'steam:110000112969e8f', 'energy', 0),
(2634, 'steam:110000112969e8f', 'binoculars', 0),
(2635, 'steam:110000112969e8f', 'coca', 0),
(2636, 'steam:110000112969e8f', 'metreshooter', 0),
(2637, 'steam:110000112969e8f', 'ice', 0),
(2638, 'steam:110000112969e8f', 'lighter', 0),
(2639, 'steam:110000112969e8f', 'opium_pooch', 0),
(2640, 'steam:110000112969e8f', 'bolnoixcajou', 0),
(2641, 'steam:110000112969e8f', 'water', 0),
(2642, 'steam:110000112969e8f', 'blackberry', 0),
(2643, 'steam:110000112969e8f', 'litter', 0),
(2644, 'steam:110000112969e8f', 'blowpipe', 0),
(2645, 'steam:11000010c2ebf86', 'leather', 0),
(2646, 'steam:11000010c2ebf86', 'martini', 0),
(2647, 'steam:11000010c2ebf86', 'firstaidkit', 0),
(2648, 'steam:11000010c2ebf86', 'wrench', 0),
(2649, 'steam:11000010c2ebf86', 'heroine', 0),
(2650, 'steam:11000010c2ebf86', 'grapperaisin', 0),
(2651, 'steam:11000010c2ebf86', 'flashlight', 0),
(2652, 'steam:11000010c2ebf86', 'marijuana', 0),
(2653, 'steam:11000010c2ebf86', 'yusuf', 0),
(2654, 'steam:11000010c2ebf86', 'hunting_license', 0),
(2655, 'steam:11000010c2ebf86', 'whisky', 0),
(2656, 'steam:11000010c2ebf86', 'fabric', 0),
(2657, 'steam:11000010c2ebf86', 'receipt', 0),
(2658, 'steam:11000010c2ebf86', 'stone', 0),
(2659, 'steam:11000010c2ebf86', 'protein_shake', 0),
(2660, 'steam:11000010c2ebf86', 'fanta', 0),
(2661, 'steam:11000010c2ebf86', 'scratchoff', 0),
(2662, 'steam:11000010c2ebf86', 'bolcacahuetes', 0),
(2663, 'steam:11000010c2ebf86', 'carotool', 0),
(2664, 'steam:11000010c2ebf86', 'whiskycoca', 0),
(2665, 'steam:11000010c2ebf86', 'drugtest', 0),
(2666, 'steam:11000010c2ebf86', 'pastacarbonara', 0),
(2667, 'steam:11000010c2ebf86', 'vegetables', 0),
(2668, 'steam:11000010c2ebf86', 'scratchoff_used', 0),
(2669, 'steam:11000010c2ebf86', 'cutted_wood', 0),
(2670, 'steam:11000010c2ebf86', 'nitrocannister', 0),
(2671, 'steam:11000010c2ebf86', 'teqpaf', 0),
(2672, 'steam:11000010c2ebf86', 'coke_pooch', 0),
(2673, 'steam:11000010c2ebf86', 'limonade', 0),
(2674, 'steam:11000010c2ebf86', 'bandage', 0),
(2675, 'steam:11000010c2ebf86', 'pizza', 0),
(2676, 'steam:11000010c2ebf86', 'meat', 0),
(2677, 'steam:11000010c2ebf86', 'meth_pooch', 0),
(2678, 'steam:11000010c2ebf86', 'bolchips', 0),
(2679, 'steam:11000010c2ebf86', 'cigarett', 5),
(2680, 'steam:11000010c2ebf86', 'defibrillateur', 0),
(2681, 'steam:11000010c2ebf86', 'saucisson', 0),
(2682, 'steam:11000010c2ebf86', 'grip', 0),
(2683, 'steam:11000010c2ebf86', 'soda', 0),
(2684, 'steam:11000010c2ebf86', 'weed_pooch', 0),
(2685, 'steam:11000010c2ebf86', 'lockpick', 0),
(2686, 'steam:11000010c2ebf86', 'tacos', 0),
(2687, 'steam:11000010c2ebf86', 'rhumfruit', 0),
(2688, 'steam:11000010c2ebf86', 'cocacola', 4),
(2689, 'steam:11000010c2ebf86', 'contrat', 0),
(2690, 'steam:11000010c2ebf86', 'pearl', 0),
(2691, 'steam:11000010c2ebf86', 'dabs', 0),
(2692, 'steam:11000010c2ebf86', 'meth', 0),
(2693, 'steam:11000010c2ebf86', 'lotteryticket', 0),
(2694, 'steam:11000010c2ebf86', 'jagerbomb', 0),
(2695, 'steam:11000010c2ebf86', 'breathalyzer', 0),
(2696, 'steam:11000010c2ebf86', 'vodkafruit', 0),
(2697, 'steam:11000010c2ebf86', 'loka', 0),
(2698, 'steam:11000010c2ebf86', 'whool', 0),
(2699, 'steam:11000010c2ebf86', 'beer', 0),
(2700, 'steam:11000010c2ebf86', 'pcp', 0),
(2701, 'steam:11000010c2ebf86', 'pearl_pooch', 0),
(2702, 'steam:11000010c2ebf86', 'gazbottle', 0),
(2703, 'steam:11000010c2ebf86', 'powerade', 0),
(2704, 'steam:11000010c2ebf86', 'weapons_license3', 0),
(2705, 'steam:11000010c2ebf86', 'diving_license', 0),
(2706, 'steam:11000010c2ebf86', 'vodka', 0),
(2707, 'steam:11000010c2ebf86', 'packaged_plank', 0),
(2708, 'steam:11000010c2ebf86', 'turtle_pooch', 0),
(2709, 'steam:11000010c2ebf86', 'marabou', 0),
(2710, 'steam:11000010c2ebf86', 'macka', 0),
(2711, 'steam:11000010c2ebf86', 'boitier', 0),
(2712, 'steam:11000010c2ebf86', 'clip', 0),
(2713, 'steam:11000010c2ebf86', 'sportlunch', 0),
(2714, 'steam:11000010c2ebf86', 'cdl', 0),
(2715, 'steam:11000010c2ebf86', 'plongee1', 0),
(2716, 'steam:11000010c2ebf86', '9mm_rounds', 0),
(2717, 'steam:11000010c2ebf86', 'marriage_license', 0),
(2718, 'steam:11000010c2ebf86', 'tequila', 0),
(2719, 'steam:11000010c2ebf86', 'menthe', 0),
(2720, 'steam:11000010c2ebf86', 'cocaine', 0),
(2721, 'steam:11000010c2ebf86', 'gold', 0),
(2722, 'steam:11000010c2ebf86', 'wood', 0),
(2723, 'steam:11000010c2ebf86', 'weapons_license1', 0),
(2724, 'steam:11000010c2ebf86', 'fixkit', 0),
(2725, 'steam:11000010c2ebf86', 'iron', 0),
(2726, 'steam:11000010c2ebf86', 'turtle', 0),
(2727, 'steam:11000010c2ebf86', 'clothe', 0),
(2728, 'steam:11000010c2ebf86', 'cola', 0),
(2729, 'steam:11000010c2ebf86', 'lsd', 0),
(2730, 'steam:11000010c2ebf86', 'fixtool', 0),
(2731, 'steam:11000010c2ebf86', 'vodkaenergy', 0),
(2732, 'steam:11000010c2ebf86', 'slaughtered_chicken', 0),
(2733, 'steam:11000010c2ebf86', 'lsd_pooch', 0),
(2734, 'steam:11000010c2ebf86', 'copper', 0),
(2735, 'steam:11000010c2ebf86', 'jusfruit', 0),
(2736, 'steam:11000010c2ebf86', 'golem', 0),
(2737, 'steam:11000010c2ebf86', 'litter_pooch', 0),
(2738, 'steam:11000010c2ebf86', 'sprite', 0),
(2739, 'steam:11000010c2ebf86', 'licenseplate', 1),
(2740, 'steam:11000010c2ebf86', 'shotgun_shells', 0),
(2741, 'steam:11000010c2ebf86', 'ephedrine', 0),
(2742, 'steam:11000010c2ebf86', 'silencieux', 0),
(2743, 'steam:11000010c2ebf86', 'donut', 0),
(2744, 'steam:11000010c2ebf86', 'WEAPON_PUMPSHOTGUN', 0),
(2745, 'steam:11000010c2ebf86', 'WEAPON_PISTOL', 0),
(2746, 'steam:11000010c2ebf86', 'crack', 0),
(2747, 'steam:11000010c2ebf86', 'rhum', 0),
(2748, 'steam:11000010c2ebf86', 'bolpistache', 0),
(2749, 'steam:11000010c2ebf86', 'WEAPON_BAT', 0),
(2750, 'steam:11000010c2ebf86', 'WEAPON_KNIFE', 0),
(2751, 'steam:11000010c2ebf86', 'WEAPON_STUNGUN', 0),
(2752, 'steam:11000010c2ebf86', 'diamond', 0),
(2753, 'steam:11000010c2ebf86', 'ephedra', 0),
(2754, 'steam:11000010c2ebf86', 'WEAPON_FLASHLIGHT', 0),
(2755, 'steam:11000010c2ebf86', 'cheesebows', 4),
(2756, 'steam:11000010c2ebf86', 'chips', 0),
(2757, 'steam:11000010c2ebf86', 'weed', 0),
(2758, 'steam:11000010c2ebf86', 'washed_stone', 247),
(2759, 'steam:11000010c2ebf86', 'radio', 0),
(2760, 'steam:11000010c2ebf86', 'firstaidpass', 0),
(2761, 'steam:11000010c2ebf86', 'boating_license', 0),
(2762, 'steam:11000010c2ebf86', 'dlic', 1),
(2763, 'steam:11000010c2ebf86', 'mlic', 0),
(2764, 'steam:11000010c2ebf86', 'drpepper', 0),
(2765, 'steam:11000010c2ebf86', 'taxi_license', 0),
(2766, 'steam:11000010c2ebf86', 'burger', 0),
(2767, 'steam:11000010c2ebf86', 'plongee2', 0),
(2768, 'steam:11000010c2ebf86', 'pills', 0),
(2769, 'steam:11000010c2ebf86', 'pilot_license', 0),
(2770, 'steam:11000010c2ebf86', 'petrol', 0),
(2771, 'steam:11000010c2ebf86', 'fishing_license', 0),
(2772, 'steam:11000010c2ebf86', 'weapons_license2', 0),
(2773, 'steam:11000010c2ebf86', 'baconburger', 0),
(2774, 'steam:11000010c2ebf86', 'cannabis', 0),
(2775, 'steam:11000010c2ebf86', 'opium', 0),
(2776, 'steam:11000010c2ebf86', 'croquettes', 0),
(2777, 'steam:11000010c2ebf86', 'icetea', 0),
(2778, 'steam:11000010c2ebf86', 'jager', 0),
(2779, 'steam:11000010c2ebf86', 'petrol_raffin', 0),
(2780, 'steam:11000010c2ebf86', 'narcan', 0),
(2781, 'steam:11000010c2ebf86', 'gym_membership', 0),
(2782, 'steam:11000010c2ebf86', 'painkiller', 0),
(2783, 'steam:11000010c2ebf86', 'coffee', 0),
(2784, 'steam:11000010c2ebf86', 'armor', 0),
(2785, 'steam:11000010c2ebf86', 'rhumcoca', 0),
(2786, 'steam:11000010c2ebf86', 'fakepee', 0),
(2787, 'steam:11000010c2ebf86', 'whiskey', 0),
(2788, 'steam:11000010c2ebf86', 'alive_chicken', 0),
(2789, 'steam:11000010c2ebf86', 'poppy', 0),
(2790, 'steam:11000010c2ebf86', 'fish', 0),
(2791, 'steam:11000010c2ebf86', 'mixapero', 0),
(2792, 'steam:11000010c2ebf86', 'gasoline', 0),
(2793, 'steam:11000010c2ebf86', 'coke', 0),
(2794, 'steam:11000010c2ebf86', 'packaged_chicken', 0),
(2795, 'steam:11000010c2ebf86', 'mojito', 0),
(2796, 'steam:11000010c2ebf86', 'medikit', 0),
(2797, 'steam:11000010c2ebf86', 'carokit', 0),
(2798, 'steam:11000010c2ebf86', 'bread', 0),
(2799, 'steam:11000010c2ebf86', 'energy', 0),
(2800, 'steam:11000010c2ebf86', 'binoculars', 0),
(2801, 'steam:11000010c2ebf86', 'coca', 0),
(2802, 'steam:11000010c2ebf86', 'metreshooter', 0),
(2803, 'steam:11000010c2ebf86', 'ice', 0),
(2804, 'steam:11000010c2ebf86', 'lighter', 1),
(2805, 'steam:11000010c2ebf86', 'opium_pooch', 0),
(2806, 'steam:11000010c2ebf86', 'bolnoixcajou', 0),
(2807, 'steam:11000010c2ebf86', 'water', 0),
(2808, 'steam:11000010c2ebf86', 'blackberry', 0),
(2809, 'steam:11000010c2ebf86', 'litter', 0),
(2810, 'steam:11000010c2ebf86', 'blowpipe', 0),
(2811, 'steam:11000010a078bc7', 'hunting_license', 0),
(2812, 'steam:11000010a078bc7', 'scratchoff', 0),
(2813, 'steam:11000010a078bc7', 'vegetables', 0),
(2814, 'steam:11000010a078bc7', 'nitrocannister', 0),
(2815, 'steam:11000010a078bc7', 'contrat', 0),
(2816, 'steam:11000010a078bc7', 'pearl', 0),
(2817, 'steam:11000010a078bc7', 'beer', 0),
(2818, 'steam:11000010a078bc7', 'pearl_pooch', 0),
(2819, 'steam:11000010a078bc7', 'weapons_license3', 0),
(2820, 'steam:11000010a078bc7', 'turtle_pooch', 0),
(2821, 'steam:11000010a078bc7', 'iron', 0),
(2822, 'steam:11000010a078bc7', 'golem', 0),
(2823, 'steam:11000010a078bc7', 'alive_chicken', 0),
(2824, 'steam:11000010a078bc7', 'metreshooter', 0),
(2825, 'steam:11000010a078bc7', 'blackberry', 0),
(2826, 'steam:11000010ddba29f', 'leather', 0),
(2827, 'steam:11000010ddba29f', 'martini', 0),
(2828, 'steam:11000010ddba29f', 'firstaidkit', 0),
(2829, 'steam:11000010ddba29f', 'wrench', 0),
(2830, 'steam:11000010ddba29f', 'heroine', 0),
(2831, 'steam:11000010ddba29f', 'grapperaisin', 0),
(2832, 'steam:11000010ddba29f', 'flashlight', 0),
(2833, 'steam:11000010ddba29f', 'marijuana', 0),
(2834, 'steam:11000010ddba29f', 'yusuf', 0),
(2835, 'steam:11000010ddba29f', 'hunting_license', 0),
(2836, 'steam:11000010ddba29f', 'whisky', 0),
(2837, 'steam:11000010ddba29f', 'fabric', 0),
(2838, 'steam:11000010ddba29f', 'receipt', 0),
(2839, 'steam:11000010ddba29f', 'stone', 0),
(2840, 'steam:11000010ddba29f', 'protein_shake', 0),
(2841, 'steam:11000010ddba29f', 'fanta', 0),
(2842, 'steam:11000010ddba29f', 'scratchoff', 0),
(2843, 'steam:11000010ddba29f', 'bolcacahuetes', 0),
(2844, 'steam:11000010ddba29f', 'carotool', 0),
(2845, 'steam:11000010ddba29f', 'whiskycoca', 0),
(2846, 'steam:11000010ddba29f', 'drugtest', 0),
(2847, 'steam:11000010ddba29f', 'pastacarbonara', 0),
(2848, 'steam:11000010ddba29f', 'vegetables', 0),
(2849, 'steam:11000010ddba29f', 'scratchoff_used', 0),
(2850, 'steam:11000010ddba29f', 'cutted_wood', 0),
(2851, 'steam:11000010ddba29f', 'nitrocannister', 0),
(2852, 'steam:11000010ddba29f', 'teqpaf', 0),
(2853, 'steam:11000010ddba29f', 'coke_pooch', 0),
(2854, 'steam:11000010ddba29f', 'limonade', 0),
(2855, 'steam:11000010ddba29f', 'bandage', 0),
(2856, 'steam:11000010ddba29f', 'pizza', 0),
(2857, 'steam:11000010ddba29f', 'meat', 0),
(2858, 'steam:11000010ddba29f', 'meth_pooch', 0),
(2859, 'steam:11000010ddba29f', 'bolchips', 0),
(2860, 'steam:11000010ddba29f', 'cigarett', 0),
(2861, 'steam:11000010ddba29f', 'defibrillateur', 0),
(2862, 'steam:11000010ddba29f', 'saucisson', 0),
(2863, 'steam:11000010ddba29f', 'grip', 0),
(2864, 'steam:11000010ddba29f', 'soda', 0),
(2865, 'steam:11000010ddba29f', 'weed_pooch', 0),
(2866, 'steam:11000010ddba29f', 'lockpick', 0),
(2867, 'steam:11000010ddba29f', 'tacos', 0),
(2868, 'steam:11000010ddba29f', 'rhumfruit', 0),
(2869, 'steam:11000010ddba29f', 'cocacola', 0),
(2870, 'steam:11000010ddba29f', 'contrat', 0),
(2871, 'steam:11000010ddba29f', 'pearl', 0),
(2872, 'steam:11000010ddba29f', 'dabs', 0),
(2873, 'steam:11000010ddba29f', 'meth', 0),
(2874, 'steam:11000010ddba29f', 'lotteryticket', 0),
(2875, 'steam:11000010ddba29f', 'jagerbomb', 0),
(2876, 'steam:11000010ddba29f', 'breathalyzer', 0),
(2877, 'steam:11000010ddba29f', 'vodkafruit', 0),
(2878, 'steam:11000010ddba29f', 'loka', 0),
(2879, 'steam:11000010ddba29f', 'whool', 0),
(2880, 'steam:11000010ddba29f', 'beer', 0),
(2881, 'steam:11000010ddba29f', 'pcp', 0),
(2882, 'steam:11000010ddba29f', 'pearl_pooch', 0),
(2883, 'steam:11000010ddba29f', 'gazbottle', 0),
(2884, 'steam:11000010ddba29f', 'powerade', 0),
(2885, 'steam:11000010ddba29f', 'weapons_license3', 0),
(2886, 'steam:11000010ddba29f', 'diving_license', 0),
(2887, 'steam:11000010ddba29f', 'vodka', 0),
(2888, 'steam:11000010ddba29f', 'packaged_plank', 0),
(2889, 'steam:11000010ddba29f', 'turtle_pooch', 0),
(2890, 'steam:11000010ddba29f', 'marabou', 0),
(2891, 'steam:11000010ddba29f', 'macka', 0),
(2892, 'steam:11000010ddba29f', 'boitier', 0),
(2893, 'steam:11000010ddba29f', 'clip', 0),
(2894, 'steam:11000010ddba29f', 'sportlunch', 0),
(2895, 'steam:11000010ddba29f', 'cdl', 0),
(2896, 'steam:11000010ddba29f', 'plongee1', 0),
(2897, 'steam:11000010ddba29f', '9mm_rounds', 0),
(2898, 'steam:11000010ddba29f', 'marriage_license', 0),
(2899, 'steam:11000010ddba29f', 'tequila', 0),
(2900, 'steam:11000010ddba29f', 'menthe', 0),
(2901, 'steam:11000010ddba29f', 'cocaine', 0),
(2902, 'steam:11000010ddba29f', 'gold', 0),
(2903, 'steam:11000010ddba29f', 'wood', 0),
(2904, 'steam:11000010ddba29f', 'weapons_license1', 0),
(2905, 'steam:11000010ddba29f', 'fixkit', 0),
(2906, 'steam:11000010ddba29f', 'iron', 0),
(2907, 'steam:11000010ddba29f', 'turtle', 0),
(2908, 'steam:11000010ddba29f', 'clothe', 0),
(2909, 'steam:11000010ddba29f', 'cola', 0),
(2910, 'steam:11000010ddba29f', 'lsd', 0),
(2911, 'steam:11000010ddba29f', 'fixtool', 0),
(2912, 'steam:11000010ddba29f', 'vodkaenergy', 0),
(2913, 'steam:11000010ddba29f', 'slaughtered_chicken', 0),
(2914, 'steam:11000010ddba29f', 'lsd_pooch', 0),
(2915, 'steam:11000010ddba29f', 'copper', 0),
(2916, 'steam:11000010ddba29f', 'jusfruit', 0),
(2917, 'steam:11000010ddba29f', 'golem', 0),
(2918, 'steam:11000010ddba29f', 'litter_pooch', 0),
(2919, 'steam:11000010ddba29f', 'sprite', 0),
(2920, 'steam:11000010ddba29f', 'licenseplate', 0),
(2921, 'steam:11000010ddba29f', 'shotgun_shells', 0),
(2922, 'steam:11000010ddba29f', 'ephedrine', 0),
(2923, 'steam:11000010ddba29f', 'silencieux', 0),
(2924, 'steam:11000010ddba29f', 'donut', 5),
(2925, 'steam:11000010ddba29f', 'WEAPON_PUMPSHOTGUN', 0),
(2926, 'steam:11000010ddba29f', 'WEAPON_PISTOL', 0),
(2927, 'steam:11000010ddba29f', 'crack', 0),
(2928, 'steam:11000010ddba29f', 'rhum', 0),
(2929, 'steam:11000010ddba29f', 'bolpistache', 0),
(2930, 'steam:11000010ddba29f', 'WEAPON_BAT', 0),
(2931, 'steam:11000010ddba29f', 'WEAPON_KNIFE', 0),
(2932, 'steam:11000010ddba29f', 'WEAPON_STUNGUN', 0),
(2933, 'steam:11000010ddba29f', 'diamond', 0),
(2934, 'steam:11000010ddba29f', 'ephedra', 0),
(2935, 'steam:11000010ddba29f', 'WEAPON_FLASHLIGHT', 0),
(2936, 'steam:11000010ddba29f', 'cheesebows', 0),
(2937, 'steam:11000010ddba29f', 'chips', 0),
(2938, 'steam:11000010ddba29f', 'weed', 0),
(2939, 'steam:11000010ddba29f', 'washed_stone', 0),
(2940, 'steam:11000010ddba29f', 'radio', 1),
(2941, 'steam:11000010ddba29f', 'firstaidpass', 0),
(2942, 'steam:11000010ddba29f', 'boating_license', 0),
(2943, 'steam:11000010ddba29f', 'dlic', 0),
(2944, 'steam:11000010ddba29f', 'mlic', 0),
(2945, 'steam:11000010ddba29f', 'drpepper', 0),
(2946, 'steam:11000010ddba29f', 'taxi_license', 0),
(2947, 'steam:11000010ddba29f', 'plongee2', 0),
(2948, 'steam:11000010ddba29f', 'burger', 0),
(2949, 'steam:11000010ddba29f', 'pills', 0),
(2950, 'steam:11000010ddba29f', 'pilot_license', 0),
(2951, 'steam:11000010ddba29f', 'petrol', 0),
(2952, 'steam:11000010ddba29f', 'fishing_license', 0),
(2953, 'steam:11000010ddba29f', 'weapons_license2', 0),
(2954, 'steam:11000010ddba29f', 'baconburger', 0),
(2955, 'steam:11000010ddba29f', 'cannabis', 0),
(2956, 'steam:11000010ddba29f', 'opium', 0),
(2957, 'steam:11000010ddba29f', 'croquettes', 0),
(2958, 'steam:11000010ddba29f', 'icetea', 0),
(2959, 'steam:11000010ddba29f', 'jager', 0),
(2960, 'steam:11000010ddba29f', 'petrol_raffin', 0),
(2961, 'steam:11000010ddba29f', 'narcan', 0),
(2962, 'steam:11000010ddba29f', 'gym_membership', 0),
(2963, 'steam:11000010ddba29f', 'painkiller', 0),
(2964, 'steam:11000010ddba29f', 'coffee', 3),
(2965, 'steam:11000010ddba29f', 'armor', 0),
(2966, 'steam:11000010ddba29f', 'rhumcoca', 0),
(2967, 'steam:11000010ddba29f', 'fakepee', 0),
(2968, 'steam:11000010ddba29f', 'whiskey', 0),
(2969, 'steam:11000010ddba29f', 'alive_chicken', 0),
(2970, 'steam:11000010ddba29f', 'poppy', 0),
(2971, 'steam:11000010ddba29f', 'fish', 0),
(2972, 'steam:11000010ddba29f', 'mixapero', 0),
(2973, 'steam:11000010ddba29f', 'gasoline', 0),
(2974, 'steam:11000010ddba29f', 'coke', 0),
(2975, 'steam:11000010ddba29f', 'packaged_chicken', 0),
(2976, 'steam:11000010ddba29f', 'mojito', 0),
(2977, 'steam:11000010ddba29f', 'medikit', 1),
(2978, 'steam:11000010ddba29f', 'carokit', 0),
(2979, 'steam:11000010ddba29f', 'bread', 0),
(2980, 'steam:11000010ddba29f', 'energy', 0),
(2981, 'steam:11000010ddba29f', 'binoculars', 0),
(2982, 'steam:11000010ddba29f', 'coca', 0),
(2983, 'steam:11000010ddba29f', 'metreshooter', 0),
(2984, 'steam:11000010ddba29f', 'ice', 0),
(2985, 'steam:11000010ddba29f', 'lighter', 0),
(2986, 'steam:11000010ddba29f', 'opium_pooch', 0),
(2987, 'steam:11000010ddba29f', 'bolnoixcajou', 0),
(2988, 'steam:11000010ddba29f', 'water', 0),
(2989, 'steam:11000010ddba29f', 'blackberry', 0),
(2990, 'steam:11000010ddba29f', 'litter', 0),
(2991, 'steam:11000010ddba29f', 'blowpipe', 0),
(2992, 'steam:11000013b85ca5f', 'leather', 0),
(2993, 'steam:11000013b85ca5f', 'martini', 0),
(2994, 'steam:11000013b85ca5f', 'firstaidkit', 0),
(2995, 'steam:11000013b85ca5f', 'wrench', 0),
(2996, 'steam:11000013b85ca5f', 'heroine', 0),
(2997, 'steam:11000013b85ca5f', 'grapperaisin', 0),
(2998, 'steam:11000013b85ca5f', 'flashlight', 0),
(2999, 'steam:11000013b85ca5f', 'marijuana', 0),
(3000, 'steam:11000013b85ca5f', 'yusuf', 0),
(3001, 'steam:11000013b85ca5f', 'hunting_license', 0),
(3002, 'steam:11000013b85ca5f', 'whisky', 0),
(3003, 'steam:11000013b85ca5f', 'fabric', 0),
(3004, 'steam:11000013b85ca5f', 'receipt', 0),
(3005, 'steam:11000013b85ca5f', 'stone', 0),
(3006, 'steam:11000013b85ca5f', 'protein_shake', 0),
(3007, 'steam:11000013b85ca5f', 'fanta', 0),
(3008, 'steam:11000013b85ca5f', 'scratchoff', 0),
(3009, 'steam:11000013b85ca5f', 'bolcacahuetes', 0),
(3010, 'steam:11000013b85ca5f', 'carotool', 0),
(3011, 'steam:11000013b85ca5f', 'whiskycoca', 0),
(3012, 'steam:11000013b85ca5f', 'drugtest', 0),
(3013, 'steam:11000013b85ca5f', 'pastacarbonara', 0),
(3014, 'steam:11000013b85ca5f', 'vegetables', 0),
(3015, 'steam:11000013b85ca5f', 'scratchoff_used', 0),
(3016, 'steam:11000013b85ca5f', 'cutted_wood', 0),
(3017, 'steam:11000013b85ca5f', 'nitrocannister', 0),
(3018, 'steam:11000013b85ca5f', 'teqpaf', 0),
(3019, 'steam:11000013b85ca5f', 'coke_pooch', 0),
(3020, 'steam:11000013b85ca5f', 'limonade', 0),
(3021, 'steam:11000013b85ca5f', 'bandage', 0),
(3022, 'steam:11000013b85ca5f', 'pizza', 0),
(3023, 'steam:11000013b85ca5f', 'meat', 0),
(3024, 'steam:11000013b85ca5f', 'meth_pooch', 0),
(3025, 'steam:11000013b85ca5f', 'bolchips', 0),
(3026, 'steam:11000013b85ca5f', 'cigarett', 0),
(3027, 'steam:11000013b85ca5f', 'defibrillateur', 0),
(3028, 'steam:11000013b85ca5f', 'saucisson', 0),
(3029, 'steam:11000013b85ca5f', 'grip', 0),
(3030, 'steam:11000013b85ca5f', 'soda', 0),
(3031, 'steam:11000013b85ca5f', 'weed_pooch', 0),
(3032, 'steam:11000013b85ca5f', 'lockpick', 0),
(3033, 'steam:11000013b85ca5f', 'tacos', 0),
(3034, 'steam:11000013b85ca5f', 'rhumfruit', 0),
(3035, 'steam:11000013b85ca5f', 'cocacola', 0),
(3036, 'steam:11000013b85ca5f', 'contrat', 0),
(3037, 'steam:11000013b85ca5f', 'pearl', 0),
(3038, 'steam:11000013b85ca5f', 'dabs', 0),
(3039, 'steam:11000013b85ca5f', 'meth', 0),
(3040, 'steam:11000013b85ca5f', 'lotteryticket', 0),
(3041, 'steam:11000013b85ca5f', 'jagerbomb', 0),
(3042, 'steam:11000013b85ca5f', 'breathalyzer', 0),
(3043, 'steam:11000013b85ca5f', 'vodkafruit', 0),
(3044, 'steam:11000013b85ca5f', 'loka', 0),
(3045, 'steam:11000013b85ca5f', 'whool', 0),
(3046, 'steam:11000013b85ca5f', 'beer', 0),
(3047, 'steam:11000013b85ca5f', 'pcp', 0),
(3048, 'steam:11000013b85ca5f', 'pearl_pooch', 0),
(3049, 'steam:11000013b85ca5f', 'gazbottle', 0),
(3050, 'steam:11000013b85ca5f', 'powerade', 0),
(3051, 'steam:11000013b85ca5f', 'weapons_license3', 0),
(3052, 'steam:11000013b85ca5f', 'diving_license', 0),
(3053, 'steam:11000013b85ca5f', 'vodka', 0),
(3054, 'steam:11000013b85ca5f', 'packaged_plank', 0),
(3055, 'steam:11000013b85ca5f', 'turtle_pooch', 0),
(3056, 'steam:11000013b85ca5f', 'marabou', 0),
(3057, 'steam:11000013b85ca5f', 'macka', 0),
(3058, 'steam:11000013b85ca5f', 'boitier', 0),
(3059, 'steam:11000013b85ca5f', 'clip', 0),
(3060, 'steam:11000013b85ca5f', 'sportlunch', 0),
(3061, 'steam:11000013b85ca5f', 'cdl', 0),
(3062, 'steam:11000013b85ca5f', 'plongee1', 0),
(3063, 'steam:11000013b85ca5f', '9mm_rounds', 0),
(3064, 'steam:11000013b85ca5f', 'marriage_license', 0),
(3065, 'steam:11000013b85ca5f', 'tequila', 0),
(3066, 'steam:11000013b85ca5f', 'menthe', 0),
(3067, 'steam:11000013b85ca5f', 'cocaine', 0),
(3068, 'steam:11000013b85ca5f', 'gold', 0),
(3069, 'steam:11000013b85ca5f', 'wood', 0),
(3070, 'steam:11000013b85ca5f', 'weapons_license1', 0),
(3071, 'steam:11000013b85ca5f', 'fixkit', 0),
(3072, 'steam:11000013b85ca5f', 'iron', 0),
(3073, 'steam:11000013b85ca5f', 'turtle', 0),
(3074, 'steam:11000013b85ca5f', 'clothe', 0),
(3075, 'steam:11000013b85ca5f', 'cola', 0),
(3076, 'steam:11000013b85ca5f', 'lsd', 0),
(3077, 'steam:11000013b85ca5f', 'fixtool', 0),
(3078, 'steam:11000013b85ca5f', 'vodkaenergy', 0),
(3079, 'steam:11000013b85ca5f', 'slaughtered_chicken', 0),
(3080, 'steam:11000013b85ca5f', 'lsd_pooch', 0),
(3081, 'steam:11000013b85ca5f', 'copper', 0),
(3082, 'steam:11000013b85ca5f', 'jusfruit', 0),
(3083, 'steam:11000013b85ca5f', 'golem', 0),
(3084, 'steam:11000013b85ca5f', 'litter_pooch', 0),
(3085, 'steam:11000013b85ca5f', 'sprite', 0),
(3086, 'steam:11000013b85ca5f', 'licenseplate', 0),
(3087, 'steam:11000013b85ca5f', 'shotgun_shells', 0),
(3088, 'steam:11000013b85ca5f', 'ephedrine', 0),
(3089, 'steam:11000013b85ca5f', 'silencieux', 0),
(3090, 'steam:11000013b85ca5f', 'donut', 0),
(3091, 'steam:11000013b85ca5f', 'WEAPON_PUMPSHOTGUN', 0),
(3092, 'steam:11000013b85ca5f', 'WEAPON_PISTOL', 0),
(3093, 'steam:11000013b85ca5f', 'crack', 0),
(3094, 'steam:11000013b85ca5f', 'rhum', 0),
(3095, 'steam:11000013b85ca5f', 'bolpistache', 0),
(3096, 'steam:11000013b85ca5f', 'WEAPON_BAT', 0),
(3097, 'steam:11000013b85ca5f', 'WEAPON_KNIFE', 0),
(3098, 'steam:11000013b85ca5f', 'WEAPON_STUNGUN', 0),
(3099, 'steam:11000013b85ca5f', 'diamond', 0),
(3100, 'steam:11000013b85ca5f', 'ephedra', 0),
(3101, 'steam:11000013b85ca5f', 'WEAPON_FLASHLIGHT', 0),
(3102, 'steam:11000013b85ca5f', 'cheesebows', 0),
(3103, 'steam:11000013b85ca5f', 'chips', 0),
(3104, 'steam:11000013b85ca5f', 'weed', 0),
(3105, 'steam:11000013b85ca5f', 'washed_stone', 0),
(3106, 'steam:11000013b85ca5f', 'radio', 0),
(3107, 'steam:11000013b85ca5f', 'firstaidpass', 0),
(3108, 'steam:11000013b85ca5f', 'boating_license', 0),
(3109, 'steam:11000013b85ca5f', 'dlic', 0),
(3110, 'steam:11000013b85ca5f', 'mlic', 0),
(3111, 'steam:11000013b85ca5f', 'drpepper', 0),
(3112, 'steam:11000013b85ca5f', 'taxi_license', 0),
(3113, 'steam:11000013b85ca5f', 'burger', 0),
(3114, 'steam:11000013b85ca5f', 'plongee2', 0),
(3115, 'steam:11000013b85ca5f', 'pills', 0),
(3116, 'steam:11000013b85ca5f', 'pilot_license', 0),
(3117, 'steam:11000013b85ca5f', 'petrol', 0),
(3118, 'steam:11000013b85ca5f', 'fishing_license', 0),
(3119, 'steam:11000013b85ca5f', 'weapons_license2', 0),
(3120, 'steam:11000013b85ca5f', 'baconburger', 0),
(3121, 'steam:11000013b85ca5f', 'cannabis', 0),
(3122, 'steam:11000013b85ca5f', 'opium', 0),
(3123, 'steam:11000013b85ca5f', 'croquettes', 0),
(3124, 'steam:11000013b85ca5f', 'icetea', 0),
(3125, 'steam:11000013b85ca5f', 'jager', 0),
(3126, 'steam:11000013b85ca5f', 'petrol_raffin', 0),
(3127, 'steam:11000013b85ca5f', 'narcan', 0),
(3128, 'steam:11000013b85ca5f', 'gym_membership', 0),
(3129, 'steam:11000013b85ca5f', 'painkiller', 0),
(3130, 'steam:11000013b85ca5f', 'coffee', 0),
(3131, 'steam:11000013b85ca5f', 'armor', 0),
(3132, 'steam:11000013b85ca5f', 'rhumcoca', 0),
(3133, 'steam:11000013b85ca5f', 'fakepee', 0),
(3134, 'steam:11000013b85ca5f', 'whiskey', 0),
(3135, 'steam:11000013b85ca5f', 'alive_chicken', 0),
(3136, 'steam:11000013b85ca5f', 'poppy', 0),
(3137, 'steam:11000013b85ca5f', 'fish', 0),
(3138, 'steam:11000013b85ca5f', 'mixapero', 0),
(3139, 'steam:11000013b85ca5f', 'gasoline', 0),
(3140, 'steam:11000013b85ca5f', 'coke', 0),
(3141, 'steam:11000013b85ca5f', 'packaged_chicken', 0),
(3142, 'steam:11000013b85ca5f', 'mojito', 0),
(3143, 'steam:11000013b85ca5f', 'medikit', 0),
(3144, 'steam:11000013b85ca5f', 'carokit', 0),
(3145, 'steam:11000013b85ca5f', 'bread', 0),
(3146, 'steam:11000013b85ca5f', 'energy', 0),
(3147, 'steam:11000013b85ca5f', 'binoculars', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(3148, 'steam:11000013b85ca5f', 'coca', 0),
(3149, 'steam:11000013b85ca5f', 'metreshooter', 0),
(3150, 'steam:11000013b85ca5f', 'ice', 0),
(3151, 'steam:11000013b85ca5f', 'lighter', 0),
(3152, 'steam:11000013b85ca5f', 'opium_pooch', 0),
(3153, 'steam:11000013b85ca5f', 'bolnoixcajou', 0),
(3154, 'steam:11000013b85ca5f', 'water', 0),
(3155, 'steam:11000013b85ca5f', 'blackberry', 0),
(3156, 'steam:11000013b85ca5f', 'litter', 0),
(3157, 'steam:11000013b85ca5f', 'blowpipe', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(11) NOT NULL,
  `reported_by` varchar(80) DEFAULT NULL,
  `report_type` varchar(255) DEFAULT NULL,
  `report_comment` varchar(255) DEFAULT NULL,
  `report_admin` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_warnings`
--

CREATE TABLE `user_warnings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `warning` longtext DEFAULT NULL,
  `time_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `inshop` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `inshop`) VALUES
('Peterbuilt', '337flatbed', 120000, 'utility', 1),
('3500flatbed', '3500 Flatbed', 78000, 'imports', 1),
('Adder', 'adder', 900000, 'super', 1),
('Akuma', 'AKUMA', 7500, 'motorcycles', 1),
('Alpha', 'alpha', 60000, 'sports', 1),
('Ardent', 'ardent', 1150000, 'sportsclassics', 1),
('Asea', 'asea', 5500, 'sedans', 1),
('Autarch', 'autarch', 1955000, 'super', 1),
('Avarus', 'avarus', 18000, 'motorcycles', 1),
('Bagger', 'bagger', 13500, 'motorcycles', 1),
('Baller', 'baller2', 40000, 'suvs', 1),
('Baller Sport', 'baller3', 60000, 'suvs', 1),
('Banshee', 'banshee', 70000, 'sports', 1),
('Banshee 900R', 'banshee2', 255000, 'sports', 1),
('Bati 801', 'bati', 12000, 'motorcycles', 1),
('Bati 801RR', 'bati2', 19000, 'motorcycles', 1),
('Bestia GTS', 'bestiagts', 55000, 'sports', 1),
('BF400', 'bf400', 6500, 'motorcycles', 1),
('Bf Injection', 'bfinjection', 16000, 'offroad', 1),
('Bifta', 'bifta', 12000, 'offroad', 1),
('Bison', 'bison', 45000, 'vans', 1),
('Blade', 'blade', 15000, 'muscle', 1),
('Blazer', 'blazer', 6500, 'offroad', 1),
('Blazer Sport', 'blazer4', 8500, 'offroad', 1),
('blazer5', 'blazer5', 1755600, 'offroad', 1),
('Blista', 'blista', 8000, 'compacts', 1),
('BMX (velo)', 'bmx', 160, 'motorcycles', 1),
('Bobcat XL', 'bobcatxl', 32000, 'vans', 1),
('Brawler', 'brawler', 45000, 'offroad', 1),
('Brioso R/A', 'brioso', 18000, 'compacts', 1),
('Btype', 'btype', 62000, 'sportsclassics', 1),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 1),
('Btype Luxe', 'btype3', 85000, 'sportsclassics', 1),
('Buccaneer', 'buccaneer', 18000, 'muscle', 1),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 1),
('Buffalo', 'buffalo', 12000, 'sports', 1),
('Buffalo S', 'buffalo2', 20000, 'sports', 1),
('Bullet', 'bullet', 90000, 'super', 1),
('Burrito', 'burrito3', 19000, 'vans', 1),
('Camper', 'camper', 42000, 'vans', 1),
('Carbonizzare', 'carbonizzare', 75000, 'sports', 1),
('Carbon RS', 'carbonrs', 18000, 'motorcycles', 1),
('Casco', 'casco', 30000, 'sportsclassics', 1),
('Cavalcade', 'cavalcade2', 55000, 'suvs', 1),
('Cheetah', 'cheetah', 375000, 'super', 1),
('Chimera', 'chimera', 38000, 'motorcycles', 1),
('Chino', 'chino', 15000, 'muscle', 1),
('Chino Luxe', 'chino2', 19000, 'muscle', 1),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 1),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 1),
('Cognoscenti', 'cognoscenti', 55000, 'sedans', 1),
('Comet', 'comet2', 65000, 'sports', 1),
('Comet 5', 'comet5', 1145000, 'sports', 1),
('Contender', 'contender', 70000, 'suvs', 1),
('Coquette', 'coquette', 65000, 'sports', 1),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 1),
('Coquette BlackFin', 'coquette3', 55000, 'muscle', 1),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles', 1),
('Cyclone', 'cyclone', 1890000, 'super', 1),
('Daemon', 'daemon', 11500, 'motorcycles', 1),
('Daemon High', 'daemon2', 13500, 'motorcycles', 1),
('Defiler', 'defiler', 9800, 'motorcycles', 1),
('Deluxo', 'deluxo', 4721500, 'sportsclassics', 1),
('Dominator', 'dominator', 35000, 'muscle', 1),
('Double T', 'double', 28000, 'motorcycles', 1),
('Dubsta', 'dubsta', 45000, 'suvs', 1),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 1),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 1),
('Dukes', 'dukes', 28000, 'muscle', 1),
('Dune Buggy', 'dune', 8000, 'offroad', 1),
('Elegy', 'elegy2', 38500, 'sports', 1),
('Emperor', 'emperor', 8500, 'sedans', 1),
('Enduro', 'enduro', 5500, 'motorcycles', 1),
('Entity XF', 'entityxf', 425000, 'importcars', 0),
('Esskey', 'esskey', 4200, 'motorcycles', 1),
('Exemplar', 'exemplar', 32000, 'coupes', 1),
('F620', 'f620', 40000, 'coupes', 1),
('Faction', 'faction', 20000, 'muscle', 1),
('Faction Rider', 'faction2', 30000, 'muscle', 1),
('Faction XL', 'faction3', 40000, 'muscle', 1),
('Faggio', 'faggio', 1900, 'motorcycles', 1),
('Vespa', 'faggio2', 2800, 'motorcycles', 1),
('Felon', 'felon', 42000, 'coupes', 1),
('Felon GT', 'felon2', 55000, 'coupes', 1),
('Feltzer', 'feltzer2', 55000, 'sports', 1),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 1),
('Freightliner', 'fhauler', 70000, 'imports', 1),
('Fixter (velo)', 'fixter', 225, 'motorcycles', 1),
('Flat Bed', 'flatbed', 90000, 'utility', 1),
('FMJ', 'fmj', 185000, 'super', 1),
('Road King', 'foxharley1', 23000, 'importbikes', 0),
('Road Glide', 'foxharley2', 30000, 'importbikes', 0),
('Fhantom', 'fq2', 17000, 'suvs', 1),
('Fugitive', 'fugitive', 12000, 'sedans', 1),
('Furore GT', 'furoregt', 45000, 'sports', 1),
('Fusilade', 'fusilade', 40000, 'sports', 1),
('Gargoyle', 'gargoyle', 16500, 'motorcycles', 1),
('Gauntlet', 'gauntlet', 30000, 'muscle', 1),
('Gang Burrito', 'gburrito', 45000, 'vans', 1),
('Burrito', 'gburrito2', 29000, 'vans', 1),
('Glendale', 'glendale', 6500, 'sedans', 1),
('Grabger', 'granger', 50000, 'suvs', 1),
('Gresley', 'gresley', 47500, 'suvs', 1),
('GT 500', 'gt500', 785000, 'sportsclassics', 1),
('Guardian', 'guardian', 45000, 'offroad', 1),
('Hakuchou', 'hakuchou', 31000, 'motorcycles', 1),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 1),
('Heavy Wrecker', 'hdwrecker', 500000, 'utility', 1),
('Hellcat', 'hellcatlb', 65000, 'importcars', 0),
('Hermes', 'hermes', 535000, 'muscle', 1),
('Hexer', 'hexer', 12000, 'motorcycles', 1),
('Hotknife', 'hotknife', 125000, 'muscle', 1),
('Huntley S', 'huntley', 40000, 'suvs', 1),
('Hustler', 'hustler', 625000, 'muscle', 1),
('Street Glide', 'hvrod', 25000, 'imports', 1),
('Infernus', 'infernus', 180000, 'super', 1),
('Innovation', 'innovation', 23500, 'motorcycles', 1),
('Intruder', 'intruder', 7500, 'sedans', 1),
('Issi', 'issi2', 10000, 'compacts', 1),
('Jackal', 'jackal', 38000, 'coupes', 1),
('Jester', 'jester', 65000, 'sports', 1),
('Jester(Racecar)', 'jester2', 135000, 'sports', 1),
('Journey', 'journey', 6500, 'vans', 1),
('k9', 'k9', 1, 'emergency', 1),
('Kamacho', 'kamacho', 345000, 'offroad', 1),
('Khamelion', 'khamelion', 38000, 'sports', 1),
('Kuruma', 'kuruma', 30000, 'sports', 1),
('Landstalker', 'landstalker', 35000, 'suvs', 1),
('RE-7B', 'le7b', 325000, 'super', 1),
('Lynx', 'lynx', 40000, 'sports', 1),
('Mamba', 'mamba', 70000, 'sports', 1),
('Manana', 'manana', 12800, 'sportsclassics', 1),
('Manchez', 'manchez', 5300, 'motorcycles', 1),
('Massacro', 'massacro', 65000, 'sports', 1),
('Massacro(Racecar)', 'massacro2', 130000, 'sports', 1),
('Mesa', 'mesa', 16000, 'suvs', 1),
('Mesa Trail', 'mesa3', 40000, 'suvs', 1),
('Minivan', 'minivan', 13000, 'vans', 1),
('Monroe', 'monroe', 55000, 'sportsclassics', 1),
('The Liberator', 'monster', 210000, 'offroad', 1),
('Moonbeam', 'moonbeam', 18000, 'vans', 1),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 1),
('Nemesis', 'nemesis', 5800, 'motorcycles', 1),
('Neon', 'neon', 1500000, 'sports', 1),
('Nightblade', 'nightblade', 35000, 'motorcycles', 1),
('Nightshade', 'nightshade', 65000, 'muscle', 1),
('9F', 'ninef', 65000, 'sports', 1),
('9F Cabrio', 'ninef2', 80000, 'sports', 1),
('Omnis', 'omnis', 35000, 'sports', 1),
('Oppressor', 'oppressor', 3524500, 'super', 1),
('Oracle XS', 'oracle2', 35000, 'coupes', 1),
('Osiris', 'osiris', 160000, 'super', 1),
('Panto', 'panto', 10000, 'compacts', 1),
('Paradise', 'paradise', 19000, 'vans', 1),
('Pariah', 'pariah', 1420000, 'sports', 1),
('Patriot', 'patriot', 55000, 'suvs', 1),
('PCJ-600', 'pcj', 6200, 'motorcycles', 1),
('Penumbra', 'penumbra', 28000, 'sports', 1),
('Pfister', 'pfister811', 85000, 'super', 1),
('Phoenix', 'phoenix', 12500, 'muscle', 1),
('Picador', 'picador', 18000, 'muscle', 1),
('Pigalle', 'pigalle', 20000, 'sportsclassics', 1),
('Prairie', 'prairie', 12000, 'compacts', 1),
('Premier', 'premier', 8000, 'sedans', 1),
('Primo Custom', 'primo2', 14000, 'sedans', 1),
('X80 Proto', 'prototipo', 2500000, 'super', 1),
('qrv', 'qrv', 1, 'emergency', 0),
('Radius', 'radi', 29000, 'suvs', 1),
('raiden', 'raiden', 1375000, 'sports', 1),
('Rapid GT', 'rapidgt', 35000, 'sports', 1),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 1),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 1),
('Reaper', 'reaper', 150000, 'importcars', 0),
('Rebel', 'rebel2', 35000, 'offroad', 1),
('Regina', 'regina', 5000, 'sedans', 1),
('Retinue', 'retinue', 615000, 'sportsclassics', 1),
('Revolter', 'revolter', 1610000, 'sports', 1),
('riata', 'riata', 380000, 'offroad', 1),
('Rocoto', 'rocoto', 45000, 'suvs', 1),
('Ruffian', 'ruffian', 6800, 'motorcycles', 1),
('Ruiner 2', 'ruiner2', 5745600, 'muscle', 1),
('Rumpo', 'rumpo', 15000, 'vans', 1),
('Rumpo Trail', 'rumpo3', 19500, 'vans', 1),
('Sabre Turbo', 'sabregt', 20000, 'muscle', 1),
('Sabre GT', 'sabregt2', 25000, 'muscle', 1),
('Sanchez', 'sanchez', 5300, 'motorcycles', 1),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 1),
('Sanctus', 'sanctus', 25000, 'motorcycles', 1),
('Sandking', 'sandking', 55000, 'offroad', 1),
('Savestra', 'savestra', 990000, 'sportsclassics', 1),
('SC 1', 'sc1', 1603000, 'super', 1),
('Schafter', 'schafter2', 25000, 'sedans', 1),
('Schafter V12', 'schafter3', 50000, 'sports', 1),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles', 1),
('Seminole', 'seminole', 25000, 'suvs', 1),
('Sentinel', 'sentinel', 32000, 'coupes', 1),
('Sentinel XS', 'sentinel2', 40000, 'coupes', 1),
('Sentinel3', 'sentinel3', 650000, 'sports', 1),
('Seven 70', 'seven70', 39500, 'sports', 1),
('ETR1', 'sheava', 220000, 'importcars', 0),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 1),
('Slam Van', 'slamvan3', 11500, 'muscle', 1),
('Indian Chief', 'slave', 46000, 'importbikes', 0),
('Sovereign', 'sovereign', 22000, 'motorcycles', 1),
('Spirit', 'spirit', 15000, 'imports', 1),
('Stinger', 'stinger', 80000, 'sportsclassics', 1),
('Stinger GT', 'stingergt', 75000, 'sportsclassics', 1),
('Streiter', 'streiter', 500000, 'sports', 1),
('Stretch', 'stretch', 90000, 'sedans', 1),
('Stromberg', 'stromberg', 3185350, 'sports', 1),
('Sultan', 'sultan', 15000, 'sports', 1),
('Sultan RS', 'sultanrs', 65000, 'importcars', 0),
('Super Diamond', 'superd', 130000, 'sedans', 1),
('Surano', 'surano', 50000, 'sports', 1),
('Surfer', 'surfer', 12000, 'vans', 1),
('T20', 't20', 300000, 'super', 1),
('Tailgater', 'tailgater', 30000, 'sedans', 1),
('Tampa', 'tampa', 16000, 'muscle', 1),
('Drift Tampa', 'tampa2', 80000, 'importcars', 0),
('Thrust', 'thrust', 24000, 'motorcycles', 1),
('Tow Truck', 'towtruck', 30000, 'utility', 1),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles', 1),
('Trophy Truck', 'trophytruck', 60000, 'offroad', 1),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 1),
('Tropos', 'tropos', 40000, 'sports', 1),
('Turismo R', 'turismor', 350000, 'super', 1),
('Tyrus', 'tyrus', 600000, 'super', 1),
('Vacca', 'vacca', 120000, 'super', 1),
('Vader', 'vader', 7200, 'motorcycles', 1),
('Verlierer', 'verlierer2', 70000, 'sports', 1),
('Vigero', 'vigero', 12500, 'muscle', 1),
('Virgo', 'virgo', 14000, 'muscle', 1),
('Viseris', 'viseris', 875000, 'sportsclassics', 1),
('Visione', 'visione', 2250000, 'super', 1),
('Voltic', 'voltic', 90000, 'super', 1),
('Voltic 2', 'voltic2', 3830400, 'super', 1),
('Voodoo', 'voodoo', 7200, 'muscle', 1),
('Vortex', 'vortex', 9800, 'motorcycles', 1),
('Warrener', 'warrener', 4000, 'sedans', 1),
('Washington', 'washington', 9000, 'sedans', 1),
('Windsor', 'windsor', 95000, 'coupes', 1),
('Windsor Drop', 'windsor2', 125000, 'importcars', 0),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 1),
('XLS', 'xls', 32000, 'suvs', 1),
('Yosemite', 'yosemite', 485000, 'muscle', 1),
('Youga', 'youga', 10800, 'vans', 1),
('Youga Luxuary', 'youga2', 14500, 'vans', 1),
('Z190', 'z190', 900000, 'sportsclassics', 1),
('Zentorno', 'zentorno', 1500000, 'super', 1),
('Zion', 'zion', 36000, 'coupes', 1),
('Zion Cabrio', 'zion2', 45000, 'coupes', 1),
('Zombie', 'zombiea', 9500, 'motorcycles', 1),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 1),
('Z-Type', 'ztype', 220000, 'sportsclassics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_display`
--

CREATE TABLE `vehicles_display` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT 10,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles_display`
--

INSERT INTO `vehicles_display` (`ID`, `name`, `model`, `profit`, `price`) VALUES
(1, 'Windsor Drop', 'windsor2', 10, 125000),
(2, 'Reaper', 'reaper', 10, 150000),
(3, 'Hell Cat', 'hellcatlb', 10, 65000),
(4, 'Road Glide', 'foxharley2', 10, 30000),
(5, 'Road King', 'foxharley1', 10, 23000),
(6, 'Indian Chief', 'slave', 10, 46000);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('emergency', 'emergency'),
('importbikes', 'Import Bikes'),
('importcars', 'Import Cars'),
('imports', 'imports'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('utility', 'Utility'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `fullname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `crimes` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 1000),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 200),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 150),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 60),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 35),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 75),
(9, 'GunShop', 'WEAPON_BAT', 30),
(10, 'BlackWeashop', 'WEAPON_BAT', 5),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 10000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 18000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 24000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 26000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 50),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 5),
(25, 'GunShop', 'WEAPON_BALL', 15),
(26, 'BlackWeashop', 'WEAPON_BALL', 2),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 46),
(28, 'GunShop', 'WEAPON_PISTOL50', 5000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `baninfo`
--
ALTER TABLE `baninfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banlist`
--
ALTER TABLE `banlist`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commend`
--
ALTER TABLE `commend`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `gsr`
--
ALTER TABLE `gsr`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kicks`
--
ALTER TABLE `kicks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nitro_vehicles`
--
ALTER TABLE `nitro_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `vehsowned` (`owner`);

--
-- Indexes for table `owned_vehicles_old`
--
ALTER TABLE `owned_vehicles_old`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `index_owned_vehicles_owner` (`owner`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `license` (`license`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_bans`
--
ALTER TABLE `received_bans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `server_actions`
--
ALTER TABLE `server_actions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin_notes`
--
ALTER TABLE `user_admin_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_warnings`
--
ALTER TABLE `user_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `baninfo`
--
ALTER TABLE `baninfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `banlisthistory`
--
ALTER TABLE `banlisthistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commend`
--
ALTER TABLE `commend`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2089;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=399;

--
-- AUTO_INCREMENT for table `kicks`
--
ALTER TABLE `kicks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;

--
-- AUTO_INCREMENT for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `received_bans`
--
ALTER TABLE `received_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_actions`
--
ALTER TABLE `server_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3158;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_warnings`
--
ALTER TABLE `user_warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
