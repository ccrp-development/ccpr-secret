-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2019 at 03:44 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`) VALUES
(1, 'bread', 'Bread', -1, 0, 1),
(2, 'water', 'Water', -1, 0, 1),
(3, 'weed', 'Weed', 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1),
(14, 'fish', 'Fish', -1, 0, 1),
(15, 'stone', 'Stone', -1, 0, 1),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1),
(17, 'copper', 'Copper', -1, 0, 1),
(18, 'iron', 'Iron', -1, 0, 1),
(19, 'gold', 'Gold', -1, 0, 1),
(20, 'diamond', 'Diamond', -1, 0, 1),
(21, 'wood', 'Wood', -1, 0, 1),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1),
(24, 'petrol', 'Gas', -1, 0, 1),
(25, 'petrol_raffin', 'Refined Oil', -1, 0, 1),
(26, 'essence', 'Essence', -1, 0, 1),
(27, 'whool', 'Whool', -1, 0, 1),
(28, 'fabric', 'Fabric', -1, 0, 1),
(29, 'clothe', 'Clothe', -1, 0, 1),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1),
(31, 'fixtool', 'Repair Tools', -1, 0, 1),
(32, 'carotool', 'Tools', -1, 0, 1),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1),
(34, 'fixkit', 'Repair Kit', -1, 0, 1),
(35, 'carokit', 'Body Kit', -1, 0, 1),
(36, 'beer', 'Beer', -1, 0, 1),
(37, 'bandage', 'Bandage', 20, 0, 1),
(38, 'medikit', 'Medikit', 100, 0, 1),
(39, 'pills', 'Pills', 10, 0, 1),
(40, 'lockpick', 'Lockpick', -1, 0, 1),
(41, 'vodka', 'Vodka', -1, 0, 1),
(42, 'coffee', 'Coffee', -1, 0, 1),
(49, 'clip', 'Chargeur', -1, 0, 1),
(50, 'jager', 'Jägermeister', 5, 0, 1),
(51, 'vodka', 'Vodka', 5, 0, 1),
(52, 'rhum', 'Rhum', 5, 0, 1),
(53, 'whisky', 'Whisky', 5, 0, 1),
(54, 'tequila', 'Tequila', 5, 0, 1),
(55, 'martini', 'Martini blanc', 5, 0, 1),
(56, 'soda', 'Soda', 5, 0, 1),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1),
(58, 'icetea', 'Ice Tea', 5, 0, 1),
(59, 'energy', 'Energy Drink', 5, 0, 1),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1),
(61, 'limonade', 'Limonade', 5, 0, 1),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1),
(63, 'bolnoixcajou', 'Bowl of Cashews', 5, 0, 1),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1),
(66, 'saucisson', 'Sausage', 5, 0, 1),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1),
(69, 'golem', 'Golem', 5, 0, 1),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1),
(76, 'mojito', 'Mojito', 5, 0, 1),
(77, 'ice', 'Glaçon', 5, 0, 1),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1),
(81, 'menthe', 'Mint leaf', 10, 0, 1),
(82, 'cola', 'Coke', -1, 0, 1),
(105, 'vegetables', 'Vegetables', 20, 0, 1),
(117, 'turtle', 'Turtle', -1, 0, 1),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1),
(119, 'lsd', 'Lsd', -1, 0, 1),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1),
(121, 'pearl', 'Pearl', -1, 0, 1),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1),
(123, 'litter', 'Litter', -1, 0, 1),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1),
(128, 'meat', 'Meat', 20, 0, 1),
(129, 'tacos', 'Tacos', 20, 0, 1),
(130, 'burger', 'Burger', 20, 0, 1),
(131, 'silencieux', 'Siliencer', -1, 0, 1),
(132, 'flashlight', 'Flashlight', -1, 0, 1),
(133, 'grip', 'Grip', -1, 0, 1),
(134, 'yusuf', 'Skin', -1, 0, 1),
(135, 'binoculars', 'Binoculars', 1, 0, 1),
(136, 'croquettes', 'Croquettes', -1, 0, 1),
(137, 'blackberry', 'blackberry', -1, 0, 1),
(138, 'lighter', 'Bic', -1, 0, 1),
(139, 'cigarett', 'Cigarette', -1, 0, 1),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1),
(2010, 'armor', 'Armor', -1, 0, 1),
(2011, 'contrat', '📃 Facture', 100, 0, 1),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1),
(2013, 'powerade', 'Powerade', -1, 0, 1),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1),
(2016, 'plongee1', 'Short Dive', -1, 0, 1),
(2017, 'plongee2', 'Long Dive', -1, 0, 1),
(2018, 'contrat', 'Salvage', 15, 0, 1),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1),
(2021, 'meat', 'Meat', -1, 0, 1),
(2022, 'leather', 'Leather', -1, 0, 1),
(2023, 'cannabis', 'Cannabis', 50, 0, 1),
(2024, 'marijuana', 'Marijuana', 250, 0, 1),
(2025, 'coca', 'CocaPlant', 150, 0, 1),
(2026, 'cocaine', 'Coke', 50, 0, 1),
(2027, 'ephedra', 'Ephedra', 100, 0, 1),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1),
(2029, 'poppy', 'Poppy', 100, 0, 1),
(2030, 'opium', 'Opium', 50, 0, 1),
(2031, 'meth', 'Meth', 25, 0, 1),
(2032, 'heroine', 'Heroine', 10, 0, 1),
(2033, 'beer', 'Beer', 30, 0, 1),
(2034, 'tequila', 'Tequila', 10, 0, 1),
(2035, 'vodka', 'Vodka', 10, 0, 1),
(2036, 'whiskey', 'Whiskey', 10, 0, 1),
(2037, 'crack', 'Crack', 25, 0, 1),
(2038, 'drugtest', 'DrugTest', 10, 0, 1),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1),
(2041, 'pcp', 'PCP', 25, 0, 1),
(2042, 'dabs', 'Dabs', 50, 0, 1),
(2043, 'painkiller', 'Painkiller', 10, 0, 1),
(2044, 'narcan', 'Narcan', 10, 0, 1),
(2045, 'boitier', 'Darknet', -1, 0, 1),
(2046, 'cocacola', 'Coca Cola', -1, 0, 1),
(2047, 'fanta', 'Fanta Exotic', -1, 0, 1),
(2048, 'sprite', 'Sprite', -1, 0, 1),
(2049, 'loka', 'Loka Crush', -1, 0, 1),
(2050, 'cheesebows', 'Cheese Doodles', -1, 0, 1),
(2051, 'chips', 'Chips', -1, 0, 1),
(2052, 'marabou', 'Milk Chocolate ', -1, 0, 1),
(2053, 'pizza', 'Kebab Pizza', -1, 0, 1),
(2054, 'baconburger', 'Bacon Burger', -1, 0, 1),
(2055, 'pastacarbonara', 'Pasta Carbonara', -1, 0, 1),
(2056, 'macka', 'Ham sammy', -1, 0, 1),
(2059, 'lotteryticket', 'Trisslott', -1, 0, 1),
(2060, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1),
(2061, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1),
(2062, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1),
(2063, 'hunting_license', 'Hunting License', -1, 0, 1),
(2064, 'fishing_license', 'Fishing License', -1, 0, 1),
(2065, 'diving_license', 'Diving License', -1, 0, 1),
(2066, 'marriage_license', 'Marriage License', -1, 0, 1),
(2067, 'pilot_license', 'Pilot License', -1, 0, 1),
(2068, 'taxi_license', 'Taxi License', -1, 0, 1),
(2069, 'commercial_license', 'Commerical Drivers License', -1, 0, 1),
(2070, 'motorcycle_license', 'Motorcycle License', -1, 0, 1),
(2071, 'drivers_license', 'Drivers License', -1, 0, 1),
(2072, 'boating_license', 'Boating License', -1, 0, 1),
(2073, 'firstaidpass', 'First Aid Pass', 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2074;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
