-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2019 at 09:43 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 15),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 15),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 15),
(6, 'LTDgasoline', 'water', 15),
(7, 'TwentyFourSeven', 'scratchoff', 20),
(16, 'LTDgasoline', 'fixkit', 50),
(19, 'RobsLiquor', 'fixkit', 50),
(22, 'TwentyFourSeven', 'fixkit', 50),
(24, 'RobsLiquor', 'beer', 45),
(25, 'TwentyFourSeven', 'lockpick', 100),
(26, 'LTDgasoline', 'lockpick', 100),
(27, 'RobsLiquor', 'vodka', 50),
(28, 'LTDgasoline', 'vodka', 50),
(29, 'TwentyFourSeven', 'coffee', 30),
(30, 'LTDgasoline', 'coffee', 30),
(31, 'RobsLiquor', 'coffee', 30),
(32, 'RobsLiquor', 'nitro', 500),
(33, 'RobsLiquor', 'cola', 100),
(34, 'RobsLiquor', 'vegetables', 100),
(35, 'RobsLiquor', 'meat', 100),
(36, 'RobsLiquor', 'silencieux', 500),
(37, 'RobsLiquor', 'flashlight', 500),
(38, 'RobsLiquor', 'grip', 500),
(39, 'RobsLiquor', 'yusuf', 500),
(40, 'TwentyFourSeven', 'binoculars', 1000),
(41, 'RobsLiquor', 'binoculars', 1000),
(42, 'LTDgasoline', 'binoculars', 1000),
(43, 'LTDgasoline', 'binoculars', 1000),
(44, 'RobsLiquor', 'blackberry', 50),
(45, 'LTDgasoline', 'lighter', 10),
(46, 'LTDgasoline', 'cigarett', 35),
(47, 'RobsLiquor', 'armor', 500),
(48, 'LTDgasoline', 'plongee1', 250),
(49, 'RobsLiquor', 'plongee1', 250),
(50, 'TwentyFourSeven', 'plongee1', 250),
(51, 'LTDgasoline', 'plongee2', 350),
(52, 'RobsLiquor', 'plongee2', 350),
(53, 'TwentyFourSeven', 'plongee2', 350),
(54, 'LTDgasoline', 'scratchoff', 20),
(55, 'PDShop', 'coffee', 1),
(56, 'PDShop', 'donut', 1),
(57, 'PDShop', 'clip', 1),
(58, 'PDShop', 'armor', 1),
(59, 'PDShop', 'medikit', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
