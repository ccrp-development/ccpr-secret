-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2019 at 03:53 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `essentialmode`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
