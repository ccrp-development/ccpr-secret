-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2019 at 01:27 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_ambulance', 'Ambulance', 1),
(2, 'society_police', 'Police', 1),
(3, 'caution', 'Caution', 0),
(4, 'society_mecano', 'Mechanic', 1),
(5, 'society_taxi', 'Taxi', 1),
(7, 'property_black_money', 'Silver Sale Property', 0),
(9, 'society_fire', 'fire', 1),
(10, 'society_airlines', 'Airlines', 1),
(11, 'society_ambulance', 'Ambulance', 1),
(12, 'society_mafia', 'Mafia', 1),
(14, 'society_rebel', 'Rebel', 1),
(15, 'society_unicorn', 'Unicorn', 1),
(16, 'society_unicorn', 'Unicorn', 1),
(17, 'society_dock', 'Marina', 1),
(18, 'society_avocat', 'Avocat', 1),
(19, 'society_irish', 'Irish', 1),
(20, 'society_rodriguez', 'Rodriguez', 1),
(21, 'society_avocat', 'Avocat', 1),
(22, 'society_bishops', 'Bishops', 1),
(23, 'society_irish', 'Irish', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_dismay', 'Dismay', 1),
(26, 'society_bountyhunter', 'Bountyhunter', 1),
(27, 'society_grove', 'Grove', 1),
(28, 'society_foodtruck', 'Foodtruck', 1),
(29, 'society_vagos', 'Vagos', 1),
(30, 'society_ballas', 'Ballas', 1),
(31, 'society_carthief', 'Car Thief', 1),
(32, 'society_realestateagent', 'Real Estae Agent', 1),
(33, 'society_admin', 'admin', 1),
(34, 'society_biker', 'Biker', 1),
(35, 'society_cardealer', 'Car Dealer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` double NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(1, 'society_ambulance', 1200, NULL),
(2, 'society_police', 0, NULL),
(3, 'society_mecano', 0, NULL),
(4, 'society_taxi', 0, NULL),
(5, 'society_fire', 0, NULL),
(6, 'society_airlines', 0, NULL),
(7, 'society_mafia', 0, NULL),
(8, 'society_rebel', 0, NULL),
(9, 'society_unicorn', 0, NULL),
(10, 'society_dock', 0, NULL),
(11, 'society_avocat', 0, NULL),
(12, 'society_irish', 0, NULL),
(13, 'society_rodriguez', 0, NULL),
(14, 'society_bishops', 0, NULL),
(15, 'society_bountyhunter', 0, NULL),
(16, 'society_dismay', 0, NULL),
(17, 'society_grove', 0, NULL),
(18, 'society_foodtruck', 0, NULL),
(19, 'society_vagos', 0, NULL),
(20, 'society_ballas', 0, NULL),
(21, 'society_carthief', 0, NULL),
(22, 'society_realestateagent', 0, NULL),
(23, 'society_admin', 0, NULL),
(24, 'society_biker', 0, NULL),
(25, 'society_cardealer', 0, NULL),
(40, 'caution', 0, 'steam:110000132580eb0'),
(41, 'property_black_money', 0, 'steam:110000132580eb0'),
(42, 'property_black_money', 0, 'steam:110000107e03e6e'),
(43, 'caution', 0, 'steam:110000107e03e6e'),
(52, 'property_black_money', 0, 'steam:11000011a9b2c0c'),
(53, 'caution', 0, 'steam:11000011a9b2c0c'),
(54, 'caution', 0, 'Char1:11000010a01bdb9'),
(55, 'property_black_money', 0, 'Char1:11000010a01bdb9'),
(56, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(57, 'caution', 0, 'steam:11000010a01bdb9'),
(58, 'property_black_money', 0, 'steam:1100001068ef13c'),
(59, 'caution', 0, 'steam:1100001068ef13c'),
(60, 'caution', 0, 'steam:11000010bf93966'),
(61, 'property_black_money', 0, 'steam:11000010bf93966'),
(62, 'property_black_money', 0, 'steam:110000112969e8f'),
(63, 'caution', 0, 'steam:110000112969e8f'),
(64, 'property_black_money', 0, 'steam:110000117b74e38'),
(65, 'caution', 0, 'steam:110000117b74e38');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'society_mecano', 'Mechanic', 1),
(3, 'society_taxi', 'Taxi', 1),
(5, 'property', 'Property', 0),
(6, 'society_fire', 'fire', 1),
(7, 'society_airlines', 'Airlines', 1),
(8, 'society_mafia', 'Mafia', 1),
(9, 'society_citizen', 'Mafia', 1),
(10, 'society_rebel', 'Rebel', 1),
(11, 'society_unicorn', 'Unicorn', 1),
(12, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(13, 'society_unicorn', 'Unicorn', 1),
(14, 'society_unicorn_fridge', 'Unicorn (frigo)', 1),
(15, 'society_dock', 'Marina', 1),
(16, 'society_avocat', 'Avocat', 1),
(17, 'society_irish', 'Irish', 1),
(18, 'society_rodriguez', 'Rodriguez', 1),
(19, 'society_avocat', 'Avocat', 1),
(20, 'society_bishops', 'Bishops', 1),
(21, 'society_irish', 'Irish', 1),
(22, 'society_bountyhunter', 'Bountyhunter', 1),
(23, 'society_dismay', 'Dismay', 1),
(24, 'society_bountyhunter', 'Bountyhunter', 1),
(25, 'society_grove', 'Grove', 1),
(26, 'society_vagos', 'Vagos', 1),
(27, 'society_ballas', 'Ballas', 1),
(28, 'society_carthief', 'Car Thief', 1),
(29, 'society_admin', 'admin', 1),
(30, 'society_biker', 'Biker', 1),
(31, 'society_cardealer', 'Car Dealer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `ban_issued` varchar(50) NOT NULL,
  `banned_until` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `Column 9` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'f',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `ems_rank` int(11) DEFAULT '-1',
  `leo_rank` int(11) DEFAULT '-1',
  `tow_rank` int(11) DEFAULT '-1',
  `admin_rank` int(11) DEFAULT '-1',
  `biker_rank` int(11) DEFAULT '-1',
  `offdutyleo_rank` int(11) DEFAULT '-1',
  `offdutyems_rank` int(11) DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `ems_rank`, `leo_rank`, `tow_rank`, `admin_rank`, `biker_rank`, `offdutyleo_rank`, `offdutyems_rank`) VALUES
('steam:11000010a01bdb9', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', -1, -1, -1, -1, -1, -1, -1);

-- --------------------------------------------------------

--
-- Table structure for table `coffees`
--

CREATE TABLE `coffees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `commend`
--

CREATE TABLE `commend` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `ID` int(11) NOT NULL,
  `community_name` varchar(50) NOT NULL,
  `discord_webhook` varchar(50) NOT NULL,
  `joinmessage` enum('T','F') NOT NULL,
  `chatcommands` enum('T','F') NOT NULL,
  `checktimeout` int(11) NOT NULL,
  `trustscore` int(11) NOT NULL,
  `tswarn` int(11) NOT NULL,
  `tskick` int(11) NOT NULL,
  `tsban` int(11) NOT NULL,
  `tscommend` int(11) NOT NULL,
  `tstime` int(11) NOT NULL,
  `recent_time` int(11) NOT NULL,
  `permissions` varchar(50) NOT NULL,
  `serveractions` varchar(50) NOT NULL,
  `debug` enum('T','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(1, 'society_police', 'Police', 1),
(2, 'user_mask', 'Masque', 0),
(3, 'property', 'Property', 0),
(4, 'society_fire', 'fire', 1),
(5, 'society_mafia', 'Mafia', 1),
(7, 'society_rebel', 'Rebel', 1),
(8, 'society_unicorn', 'Unicorn', 1),
(9, 'society_unicorn', 'Unicorn', 1),
(10, 'society_avocat', 'Avocat', 1),
(11, 'society_irish', 'Irish', 1),
(12, 'society_rodriguez', 'Rodriguez', 1),
(13, 'society_avocat', 'Avocat', 1),
(14, 'society_bishops', 'Bishops', 1),
(15, 'society_irish', 'Irish', 1),
(16, 'society_bountyhunter', 'Bountyhunter', 1),
(17, 'society_dismay', 'Dismay', 1),
(18, 'society_bountyhunter', 'Bountyhunter', 1),
(19, 'society_grove', 'Grove', 1),
(20, 'society_vagos', 'Vagos', 1),
(21, 'society_ballas', 'Ballas', 1),
(22, 'society_carthief', 'Car Thief', 1),
(23, 'society_admin', 'admin', 1),
(24, 'society_biker', 'Biker', 1);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(1, 'society_police', NULL, '{}'),
(2, 'society_fire', NULL, '{}'),
(3, 'society_mafia', NULL, '{}'),
(4, 'society_rebel', NULL, '{}'),
(5, 'society_unicorn', NULL, '{}'),
(6, 'society_avocat', NULL, '{}'),
(7, 'society_irish', NULL, '{}'),
(8, 'society_rodriguez', NULL, '{}'),
(9, 'society_bishops', NULL, '{}'),
(10, 'society_bountyhunter', NULL, '{}'),
(11, 'society_dismay', NULL, '{}'),
(12, 'society_grove', NULL, '{}'),
(13, 'society_vagos', NULL, '{}'),
(14, 'society_ballas', NULL, '{}'),
(15, 'society_carthief', NULL, '{}'),
(16, 'society_admin', NULL, '{}'),
(17, 'society_biker', NULL, '{}'),
(32, 'user_mask', 'steam:110000132580eb0', '{}'),
(33, 'property', 'steam:110000132580eb0', '{}'),
(34, 'user_mask', 'steam:110000107e03e6e', '{}'),
(35, 'property', 'steam:110000107e03e6e', '{}'),
(44, 'user_mask', 'steam:11000011a9b2c0c', '{}'),
(45, 'property', 'steam:11000011a9b2c0c', '{}'),
(46, 'user_mask', 'Char1:11000010a01bdb9', '{}'),
(47, 'property', 'Char1:11000010a01bdb9', '{}'),
(48, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(49, 'property', 'steam:11000010a01bdb9', '{}'),
(50, 'property', 'steam:1100001068ef13c', '{}'),
(51, 'user_mask', 'steam:1100001068ef13c', '{}'),
(52, 'user_mask', 'steam:11000010bf93966', '{}'),
(53, 'property', 'steam:11000010bf93966', '{}'),
(54, 'property', 'steam:110000112969e8f', '{}'),
(55, 'user_mask', 'steam:110000112969e8f', '{}'),
(56, 'property', 'steam:110000117b74e38', '{}'),
(57, 'user_mask', 'steam:110000117b74e38', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dock`
--

CREATE TABLE `dock` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock`
--

INSERT INTO `dock` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Toro', 'toro', 2000, 'dock'),
(3, 'Dinghy3', 'dinghy3', 2000, 'dock'),
(4, 'Seashark', 'seashark', 1000, 'dock'),
(5, 'Submarine', 'submersible2', 4000, 'dock');

-- --------------------------------------------------------

--
-- Table structure for table `dock_categories`
--

CREATE TABLE `dock_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `dock_categories`
--

INSERT INTO `dock_categories` (`id`, `name`, `label`) VALUES
(1, 'dock', 'Bateaux');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Distracted Driving', 500, 0),
(2, 'Fleeing and Eluding', 3000, 0),
(3, 'Grand Theft Auto', 2000, 0),
(4, 'Illegal U-Turn', 500, 0),
(5, 'Jaywalking', 150, 0),
(6, 'Leaving the Scene of an Accident/Hit and Run', 1000, 0),
(7, 'Reckless Driving', 1500, 0),
(8, 'Reckless Driving causing Death', 3000, 0),
(9, 'Running a Red Light / Stop Sign', 500, 0),
(10, 'Undue Care and Attention', 700, 0),
(11, 'Unlawful Vehicle Modifications', 700, 0),
(12, 'Illegal Window Tint', 250, 0),
(13, 'Speeding', 750, 0),
(14, 'Speeding in the 2nd Degree', 1000, 0),
(15, 'Speeding in the 3rd Degree', 1500, 0),
(16, 'Disorderly Conduct', 800, 1),
(17, 'Disturbing the Peace', 1000, 1),
(18, 'Public Intoxication', 800, 1),
(19, 'Driving Without Drivers License / Permit', 3500, 1),
(20, 'Domestic Violence', 1000, 1),
(21, 'Harassment', 1000, 1),
(22, 'Hate Crimes', 3000, 1),
(23, 'Bribery', 1500, 1),
(24, 'Fraud', 2000, 1),
(25, 'Stalking', 3000, 1),
(26, 'Threaten to Harm', 1500, 1),
(27, 'Arson', 1500, 1),
(28, 'Loitering', 800, 1),
(29, 'Conspiracy', 2500, 1),
(30, 'Obstruction of Justice', 1000, 1),
(31, 'Cop Baiting', 10000, 1),
(32, 'Trolling', 15000, 1),
(33, 'Murder of an LEO', 30000, 3),
(34, 'Murder of a Civilian', 15000, 3),
(35, 'Att. Murder LEO', 15000, 3),
(36, 'Att. Murder Civillian', 10000, 3),
(37, 'Bank Robbery', 7000, 3),
(38, 'Attempted Manslaughter', 5000, 3),
(39, 'Attempted Vehicular Manslaughter', 4500, 3),
(40, 'Possession of a Class 2 Firearm', 5000, 3),
(41, 'Felon in Possession of a Class 2 Firearm', 7500, 3),
(42, 'Class 2 Weapon trafficking', 7500, 3),
(43, 'Burglary', 2000, 2),
(44, 'Larceny', 1500, 2),
(45, 'Robbery', 2000, 2),
(46, 'Theft', 1500, 2),
(47, 'Vandalism', 1300, 2),
(48, 'Espionage', 1000, 2),
(49, 'Aggravated Assault / Battery', 5000, 2),
(50, 'Assault / Battery', 3500, 2),
(51, 'Threaten to Harm with a Deadly Weapon', 3000, 2),
(52, 'Rioting and Inciting Riots', 5000, 2),
(53, 'Sedition', 2500, 2),
(54, 'Terrorism and Terroristic Threats', 10000, 2),
(55, 'Treason', 5500, 2),
(56, 'DUI/DWI', 4500, 2),
(57, 'Money Laundering', 5000, 2),
(58, 'Possession', 1500, 2),
(59, 'Manufacturing and Cultivation', 2500, 2),
(60, 'Trafficking/Distribution', 4500, 2),
(61, 'Dealing', 5000, 2),
(62, 'Accessory', 3500, 2),
(63, 'Brandishing a Lethal Weapon', 1500, 2),
(64, 'Destruction of Police Property', 1500, 2);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ambulance`
--

CREATE TABLE `fine_types_ambulance` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fine_types_ambulance`
--

INSERT INTO `fine_types_ambulance` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Soin pour membre de la police', 400, 0),
(2, ' Soin de base', 500, 0),
(3, 'Soin longue distance', 750, 0),
(4, 'Soin patient inconscient', 800, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_ballas`
--

CREATE TABLE `fine_types_ballas` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_ballas`
--

INSERT INTO `fine_types_ballas` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_biker`
--

CREATE TABLE `fine_types_biker` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_biker`
--

INSERT INTO `fine_types_biker` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bishops`
--

CREATE TABLE `fine_types_bishops` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bishops`
--

INSERT INTO `fine_types_bishops` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_bountyhunter`
--

CREATE TABLE `fine_types_bountyhunter` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_bountyhunter`
--

INSERT INTO `fine_types_bountyhunter` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_dismay`
--

CREATE TABLE `fine_types_dismay` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_dismay`
--

INSERT INTO `fine_types_dismay` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_grove`
--

CREATE TABLE `fine_types_grove` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_grove`
--

INSERT INTO `fine_types_grove` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_irish`
--

CREATE TABLE `fine_types_irish` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_irish`
--

INSERT INTO `fine_types_irish` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_mafia`
--

CREATE TABLE `fine_types_mafia` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_mafia`
--

INSERT INTO `fine_types_mafia` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rebel`
--

CREATE TABLE `fine_types_rebel` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rebel`
--

INSERT INTO `fine_types_rebel` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_rodriguez`
--

CREATE TABLE `fine_types_rodriguez` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types_rodriguez`
--

INSERT INTO `fine_types_rodriguez` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fine_types_vagos`
--

CREATE TABLE `fine_types_vagos` (
  `id` int(11) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fine_types_vagos`
--

INSERT INTO `fine_types_vagos` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Raket', 3000, 0),
(2, 'Raket', 5000, 0),
(3, 'Raket', 10000, 1),
(4, 'Raket', 20000, 1),
(5, 'Raket', 50000, 2),
(6, 'Raket', 150000, 3),
(7, 'Raket', 350000, 3);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gsr`
--

CREATE TABLE `gsr` (
  `identifier` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `limit` int(11) NOT NULL DEFAULT '-1',
  `rare` int(11) NOT NULL DEFAULT '0',
  `can_remove` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `label`, `limit`, `rare`, `can_remove`, `price`) VALUES
(1, 'bread', 'Bread', -1, 0, 1, 0),
(2, 'water', 'Water', -1, 0, 1, 0),
(3, 'weed', 'Weed', 0, 0, 0, 0),
(4, 'weed_pooch', 'Pouch of weed', -1, 0, 0, 0),
(5, 'coke', 'Coke', 0, 0, 0, 0),
(6, 'coke_pooch', 'Pouch of coke', 0, 0, 0, 0),
(7, 'meth', 'Meth', 0, 0, 0, 0),
(8, 'meth_pooch', 'Pouch of meth', 0, 0, 0, 0),
(9, 'opium', 'Opium', 0, 0, 0, 0),
(10, 'opium_pooch', 'Pouch of opium', 0, 0, 0, 0),
(11, 'alive_chicken', 'Alive Chicken', -1, 0, 1, 0),
(12, 'slaughtered_chicken', 'Slaughtered Chicken', -1, 0, 1, 0),
(13, 'packaged_chicken', 'Packaged Chicken', -1, 0, 1, 0),
(14, 'fish', 'Fish', -1, 0, 1, 0),
(15, 'stone', 'Stone', -1, 0, 1, 0),
(16, 'washed_stone', 'Washed Stone', -1, 0, 1, 0),
(17, 'copper', 'Copper', -1, 0, 1, 0),
(18, 'iron', 'Iron', -1, 0, 1, 0),
(19, 'gold', 'Gold', -1, 0, 1, 0),
(20, 'diamond', 'Diamond', -1, 0, 1, 0),
(21, 'wood', 'Wood', -1, 0, 1, 0),
(22, 'cutted_wood', 'Cut Wood', -1, 0, 1, 0),
(23, 'packaged_plank', 'Packaged Plank', -1, 0, 1, 0),
(24, 'petrol', 'Gas', -1, 0, 1, 0),
(25, 'petrol_raffin', 'Refined Oil', -1, 0, 1, 0),
(26, 'essence', 'Essence', -1, 0, 1, 0),
(27, 'whool', 'Whool', -1, 0, 1, 0),
(28, 'fabric', 'Fabric', -1, 0, 1, 0),
(29, 'clothe', 'Clothe', -1, 0, 1, 0),
(30, 'gazbottle', 'Gas Bottle', -1, 0, 1, 0),
(31, 'fixtool', 'Repair Tools', -1, 0, 1, 0),
(32, 'carotool', 'Tools', -1, 0, 1, 0),
(33, 'blowpipe', 'Blowtorch', -1, 0, 1, 0),
(34, 'fixkit', 'Repair Kit', -1, 0, 1, 0),
(35, 'carokit', 'Body Kit', -1, 0, 1, 0),
(36, 'beer', 'Beer', -1, 0, 1, 0),
(37, 'bandage', 'Bandage', 20, 0, 1, 0),
(38, 'medikit', 'Medikit', 100, 0, 1, 0),
(39, 'pills', 'Pills', 10, 0, 1, 0),
(40, 'lockpick', 'Lockpick', -1, 0, 1, 0),
(41, 'vodka', 'Vodka', -1, 0, 1, 0),
(42, 'coffee', 'Coffee', -1, 0, 1, 0),
(49, 'clip', 'Chargeur', -1, 0, 1, 0),
(50, 'jager', 'Jägermeister', 5, 0, 1, 0),
(51, 'vodka', 'Vodka', 5, 0, 1, 0),
(52, 'rhum', 'Rhum', 5, 0, 1, 0),
(53, 'whisky', 'Whisky', 5, 0, 1, 0),
(54, 'tequila', 'Tequila', 5, 0, 1, 0),
(55, 'martini', 'Martini blanc', 5, 0, 1, 0),
(56, 'soda', 'Soda', 5, 0, 1, 0),
(57, 'jusfruit', 'Fruit Juice', 5, 0, 1, 0),
(58, 'icetea', 'Ice Tea', 5, 0, 1, 0),
(59, 'energy', 'Energy Drink', 5, 0, 1, 0),
(60, 'drpepper', 'Dr. Pepper', 5, 0, 1, 0),
(61, 'limonade', 'Limonade', 5, 0, 1, 0),
(62, 'bolcacahuetes', 'Bowl of Peanuts', 5, 0, 1, 0),
(63, 'bolnoixcajou', 'Bowl of Cashews', 5, 0, 1, 0),
(64, 'bolpistache', 'Bowl of Pistachios', 5, 0, 1, 0),
(65, 'bolchips', 'Bowl of Chips', 5, 0, 1, 0),
(66, 'saucisson', 'Sausage', 5, 0, 1, 0),
(67, 'grapperaisin', 'Bunch of Grapes', 5, 0, 1, 0),
(68, 'jagerbomb', 'Jägerbomb', 5, 0, 1, 0),
(69, 'golem', 'Golem', 5, 0, 1, 0),
(70, 'whiskycoca', 'Whisky-coca', 5, 0, 1, 0),
(71, 'vodkaenergy', 'Vodka-energy', 5, 0, 1, 0),
(72, 'vodkafruit', 'Vodka with Fruit', 5, 0, 1, 0),
(73, 'rhumfruit', 'Rum with Fruit', 5, 0, 1, 0),
(74, 'teqpaf', 'Tequila Sunrise', 5, 0, 1, 0),
(75, 'rhumcoca', 'Rhum-coca', 5, 0, 1, 0),
(76, 'mojito', 'Mojito', 5, 0, 1, 0),
(77, 'ice', 'Glaçon', 5, 0, 1, 0),
(78, 'mixapero', 'Mixed Nuts', 3, 0, 1, 0),
(79, 'metreshooter', 'Vodka Shooter', 3, 0, 1, 0),
(81, 'menthe', 'Mint leaf', 10, 0, 1, 0),
(82, 'cola', 'Coke', -1, 0, 1, 0),
(105, 'vegetables', 'Vegetables', 20, 0, 1, 0),
(117, 'turtle', 'Turtle', -1, 0, 1, 0),
(118, 'turtle_pooch', 'Pouch of turtle', -1, 0, 1, 0),
(119, 'lsd', 'Lsd', -1, 0, 1, 0),
(120, 'lsd_pooch', 'Pochon de LSD', -1, 0, 1, 0),
(121, 'pearl', 'Pearl', -1, 0, 1, 0),
(122, 'pearl_pooch', 'Pochon de Pearl', -1, 0, 1, 0),
(123, 'litter', 'Litter', -1, 0, 1, 0),
(124, 'litter_pooch', 'Pochon de LITTER', -1, 0, 1, 0),
(128, 'meat', 'Meat', 20, 0, 1, 0),
(129, 'tacos', 'Tacos', 20, 0, 1, 0),
(130, 'burger', 'Burger', 20, 0, 1, 0),
(131, 'silencieux', 'Siliencer', -1, 0, 1, 0),
(132, 'flashlight', 'Flashlight', -1, 0, 1, 0),
(133, 'grip', 'Grip', -1, 0, 1, 0),
(134, 'yusuf', 'Skin', -1, 0, 1, 0),
(135, 'binoculars', 'Binoculars', 1, 0, 1, 0),
(136, 'croquettes', 'Croquettes', -1, 0, 1, 0),
(137, 'blackberry', 'blackberry', -1, 0, 1, 0),
(138, 'lighter', 'Bic', -1, 0, 1, 0),
(139, 'cigarett', 'Cigarette', -1, 0, 1, 0),
(140, 'donut', 'Policeman\'s Best Friend', -1, 0, 1, 0),
(2010, 'armor', 'Armor', -1, 0, 1, 0),
(2011, 'contrat', '📃 Facture', 100, 0, 1, 0),
(2012, 'gym_membership', 'Gym Membership', -1, 0, 1, 0),
(2013, 'powerade', 'Powerade', -1, 0, 1, 0),
(2014, 'sportlunch', 'Sportlunch', -1, 0, 1, 0),
(2015, 'protein_shake', 'Protein Shake', -1, 0, 1, 0),
(2016, 'plongee1', 'Short Dive', -1, 0, 1, 0),
(2017, 'plongee2', 'Long Dive', -1, 0, 1, 0),
(2018, 'contrat', 'Salvage', 15, 0, 1, 0),
(2019, 'scratchoff', 'Scratchoff Ticket', -1, 0, 1, 0),
(2020, 'scratchoff_used', 'Used Scratchoff Ticket', -1, 0, 1, 0),
(2021, 'meat', 'Meat', -1, 0, 1, 0),
(2022, 'leather', 'Leather', -1, 0, 1, 0),
(2023, 'cannabis', 'Cannabis', 50, 0, 1, 0),
(2024, 'marijuana', 'Marijuana', 250, 0, 1, 0),
(2025, 'coca', 'CocaPlant', 150, 0, 1, 0),
(2026, 'cocaine', 'Coke', 50, 0, 1, 0),
(2027, 'ephedra', 'Ephedra', 100, 0, 1, 0),
(2028, 'ephedrine', 'Ephedrine', 100, 0, 1, 0),
(2029, 'poppy', 'Poppy', 100, 0, 1, 0),
(2030, 'opium', 'Opium', 50, 0, 1, 0),
(2031, 'meth', 'Meth', 25, 0, 1, 0),
(2032, 'heroine', 'Heroine', 10, 0, 1, 0),
(2033, 'beer', 'Beer', 30, 0, 1, 0),
(2034, 'tequila', 'Tequila', 10, 0, 1, 0),
(2035, 'vodka', 'Vodka', 10, 0, 1, 0),
(2036, 'whiskey', 'Whiskey', 10, 0, 1, 0),
(2037, 'crack', 'Crack', 25, 0, 1, 0),
(2038, 'drugtest', 'DrugTest', 10, 0, 1, 0),
(2039, 'breathalyzer', 'Breathalyzer', 10, 0, 1, 0),
(2040, 'fakepee', 'Fake Pee', 5, 0, 1, 0),
(2041, 'pcp', 'PCP', 25, 0, 1, 0),
(2042, 'dabs', 'Dabs', 50, 0, 1, 0),
(2043, 'painkiller', 'Painkiller', 10, 0, 1, 0),
(2044, 'narcan', 'Narcan', 10, 0, 1, 0),
(2045, 'boitier', 'Darknet', -1, 0, 1, 0),
(2046, 'cocacola', 'Coca Cola', -1, 0, 1, 0),
(2047, 'fanta', 'Fanta Exotic', -1, 0, 1, 0),
(2048, 'sprite', 'Sprite', -1, 0, 1, 0),
(2049, 'loka', 'Loka Crush', -1, 0, 1, 0),
(2050, 'cheesebows', 'Cheese Doodles', -1, 0, 1, 0),
(2051, 'chips', 'Chips', -1, 0, 1, 0),
(2052, 'marabou', 'Milk Chocolate ', -1, 0, 1, 0),
(2053, 'pizza', 'Kebab Pizza', -1, 0, 1, 0),
(2054, 'baconburger', 'Bacon Burger', -1, 0, 1, 0),
(2055, 'pastacarbonara', 'Pasta Carbonara', -1, 0, 1, 0),
(2056, 'macka', 'Ham sammy', -1, 0, 1, 0),
(2059, 'lotteryticket', 'Trisslott', -1, 0, 1, 0),
(2060, 'weapons_license1', 'Class 1 Weapons License', -1, 0, 1, 0),
(2061, 'weapons_license2', 'Class 2 Weapons License', -1, 0, 1, 0),
(2062, 'weapons_license3', 'Class 3 Weapons License', -1, 0, 1, 0),
(2063, 'hunting_license', 'Hunting License', -1, 0, 1, 0),
(2064, 'fishing_license', 'Fishing License', -1, 0, 1, 0),
(2065, 'diving_license', 'Diving License', -1, 0, 1, 0),
(2066, 'marriage_license', 'Marriage License', -1, 0, 1, 0),
(2067, 'pilot_license', 'Pilot License', -1, 0, 1, 0),
(2068, 'taxi_license', 'Taxi License', -1, 0, 1, 0),
(2069, 'commercial_license', 'Commerical Drivers License', -1, 0, 1, 0),
(2070, 'motorcycle_license', 'Motorcycle License', -1, 0, 1, 0),
(2071, 'drivers_license', 'Drivers License', -1, 0, 1, 0),
(2072, 'boating_license', 'Boating License', -1, 0, 1, 0),
(2073, 'firstaidpass', 'First Aid Pass', 1, 0, 0, 0),
(2074, 'nitrocannister', 'NOS', 1, 0, 1, 0),
(2075, 'wrench', 'Wrench', 1, 0, 1, 0),
(2076, 'firstaidkit', 'First Aid Kit', 1, 0, 1, 0),
(2077, 'defibrillateur', 'AED', 1, 0, 1, 0),
(2078, 'radio', 'Radio', 1, 0, 0, 0),
(2079, 'WEAPON_FLASHLIGHT', 'Flashlight', 1, 0, 1, 0),
(2080, 'WEAPON_STUNGUN', 'Taser', 100, 1, 1, 0),
(2081, 'WEAPON_KNIFE', 'Knife', 100, 1, 1, 0),
(2082, 'WEAPON_BAT', 'Baseball Bat', 1, 0, 1, 0),
(2083, 'WEAPON_PISTOL', 'Pistol', 100, 1, 1, 0),
(2084, 'WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 0, 1, 0),
(2085, '9mm_rounds', '9mm Rounds', 20, 0, 1, 0),
(2086, 'shotgun_shells', 'Shotgun Shells', 20, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jail`
--

CREATE TABLE `jail` (
  `identifier` varchar(100) NOT NULL,
  `jail_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `label`, `whitelisted`) VALUES
(1, 'unemployed', 'Unemployed', 0),
(3, 'police', 'LEO', 1),
(4, 'slaughterer', 'Slaughterer', 0),
(5, 'fisherman', 'Fisherman', 0),
(6, 'miner', 'Miner', 0),
(7, 'lumberjack', 'Woodcutter', 0),
(8, 'fuel', 'Refiner', 0),
(9, 'reporter', 'Journalist', 0),
(10, 'textil', 'Couturier', 0),
(11, 'mechanic', 'Mechanic', 0),
(12, 'taxi', 'Taxi', 0),
(14, 'trucker', 'Trucker', 0),
(16, 'fire', 'LFD', 1),
(17, 'deliverer', 'Deliverer', 0),
(18, 'brinks', 'Transport', 0),
(19, 'gopostal', 'GoPostal', 0),
(20, 'airlines', 'Airlines', 0),
(21, 'ambulance', 'AMR', 1),
(22, 'mafia', 'Mafia', 1),
(24, 'rebel', 'Rebel', 1),
(25, 'security', 'Security', 0),
(28, 'garbage', 'Garbage Driver', 0),
(29, 'pizza', 'Pizzadelivery', 0),
(30, 'ranger', 'parkranger', 0),
(33, 'unicorn', 'Unicorn', 0),
(34, 'bus', 'busdriver', 0),
(36, 'coastguard', 'CoastGuard', 0),
(38, 'irish', 'Irish', 1),
(39, 'rodriguez', 'Rodriguez', 1),
(41, 'bishops', 'Bishops', 1),
(42, 'irish', 'Irish', 1),
(43, 'lawyer', 'lawyer', 1),
(45, 'dismay', 'Dismay', 1),
(46, 'bountyhunter', 'Bountyhunter', 1),
(47, 'grove', 'Grove Street Family', 1),
(49, 'vagos', 'Vagos', 1),
(50, 'ballas', 'Ballas', 1),
(51, 'traffic', 'trafficofficer', 0),
(52, 'poolcleaner', 'PoolCleaner', 0),
(53, 'carthief', 'Car Thief', 0),
(54, 'Salvage', 'Salvage', 0),
(55, 'realestateagent', 'Real Estate Agent', 1),
(56, 'admin', 'Admin', 1),
(57, 'biker', 'HHMC', 1),
(58, 'cardealer', 'Car Dealer', 1),
(59, 'offpolice', 'Off-Duty LEO', 1),
(60, 'offambulance', 'Off-Duty EMS', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'lumberjack', 0, 'interim', 'Employee', 250, '{}', '{}'),
(2, 'fisherman', 0, 'interim', 'Employee', 250, '{}', '{}'),
(3, 'fuel', 0, 'interim', 'Employee', 250, '{}', '{}'),
(4, 'reporter', 0, 'employee', 'Employee', 250, '{}', '{}'),
(5, 'textil', 0, 'interim', 'Employee', 250, '{}', '{}'),
(6, 'miner', 0, 'interim', 'Employee', 250, '{}', '{}'),
(7, 'slaughterer', 0, 'interim', 'Employee', 250, '{}', '{}'),
(8, 'mechanic', 0, 'recrue', 'Recruit', 250, '{}', '{}'),
(9, 'mechanic', 1, 'mech', 'Mechanic', 300, '{}', '{}'),
(10, 'mechanic', 2, 'experimente', 'Experienced Mechanic', 350, '{}', '{}'),
(11, 'mechanic', 3, 'chief', 'Lead Mechanic', 400, '{}', '{}'),
(12, 'mechanic', 4, 'boss', 'Mechanic Manager', 400, '{}', '{}'),
(13, 'taxi', 0, 'uber', 'Recruit', 250, '{}', '{}'),
(14, 'taxi', 1, 'uber', 'Novice', 300, '{}', '{}'),
(15, 'taxi', 2, 'uber', 'Experimente', 350, '{}', '{}'),
(16, 'taxi', 3, 'uber', 'Uber', 400, '{}', '{}'),
(17, 'taxi', 4, 'boss', 'Boss', 400, '{}', '{}'),
(18, 'trucker', 0, 'employee', 'Employee', 250, '{}', '{}'),
(19, 'deliverer', 0, 'employee', 'Employee', 250, '{}', '{}'),
(20, 'brinks', 0, 'employee', 'Employee', 200, '{}', '{}'),
(21, 'gopostal', 0, 'employee', 'Employee', 200, '{}', '{}'),
(22, 'airlines', 0, 'recruit', 'Recruit', 800, '{}', '{}'),
(23, 'airlines', 1, 'firstofficer', 'Firstofficer', 800, '{}', '{}'),
(24, 'airlines', 0, 'pilote', 'Pilote', 800, '{}', '{}'),
(25, 'airlines', 0, 'gerant', 'Gerant', 800, '{}', '{}'),
(26, 'airlines', 0, 'boss', 'Patron', 800, '{}', '{}'),
(27, 'mafia', 0, 'soldato', 'Ptite-Frappe', 700, '{}', '{}'),
(28, 'mafia', 1, 'capo', 'Capo', 800, '{}', '{}'),
(29, 'mafia', 2, 'consigliere', 'Consigliere', 900, '{}', '{}'),
(30, 'mafia', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(31, 'rebel', 0, 'gangster', 'Gangster', 400, '{}', '{}'),
(32, 'rebel', 1, 'capo', 'Capo', 400, '{}', '{}'),
(33, 'rebel', 2, 'consigliere', 'Consigliere', 400, '{}', '{}'),
(34, 'rebel', 3, 'boss', 'OG', 400, '{}', '{}'),
(35, 'security', 0, 'employee', 'Security', 200, '{}', '{}'),
(36, 'garbage', 0, 'employee', 'Employee', 750, '{}', '{}'),
(37, 'pizza', 0, 'employee', 'driver', 200, '{}', '{}'),
(38, 'ranger', 0, 'employee', 'ranger', 400, '{}', '{}'),
(39, 'traffic', 0, 'employee', 'Officer', 200, '{}', '{}'),
(40, 'unicorn', 0, 'barman', 'Barman', 400, '{}', '{}'),
(41, 'unicorn', 1, 'dancer', 'Danseur', 600, '{}', '{}'),
(42, 'unicorn', 2, 'viceboss', 'Co-gerant', 800, '{}', '{}'),
(43, 'unicorn', 3, 'boss', 'Gerant', 1000, '{}', '{}'),
(44, 'bus', 0, 'employee', 'Driver', 200, '{}', '{}'),
(45, 'coastguard', 0, 'employee', 'CoastGuard', 200, '{}', '{}'),
(46, 'irish', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(47, 'irish', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(48, 'irish', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(49, 'irish', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(50, 'bishops', 0, 'soldato', 'Ptite-Frappe', 900, '{}', '{}'),
(51, 'bishops', 1, 'capo', 'Capo', 1100, '{}', '{}'),
(52, 'bishops', 2, 'consigliere', 'Consigliere', 1300, '{}', '{}'),
(53, 'bishops', 3, 'boss', 'Parain', 1500, '{}', '{}'),
(54, 'lawyer', 0, 'employee', 'Employee', 800, '{}', '{}'),
(55, 'dismay', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(56, 'dismay', 1, 'capo', 'Capo', 600, '{}', '{}'),
(57, 'dismay', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(58, 'dismay', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(59, 'grove', 0, 'soldato', 'Ptite-Frappe', 500, '{}', '{}'),
(60, 'grove', 1, 'capo', 'Capo', 600, '{}', '{}'),
(61, 'grove', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(62, 'grove', 3, 'boss', 'Parain', 800, '{}', '{}'),
(63, 'vagos', 1, 'soldato', 'Perite-Frappe', 150, '{}', '{}'),
(64, 'vagos', 2, 'capo', 'Capo', 500, '{}', '{}'),
(65, 'vagos', 3, 'consigliere', 'Consigliere', 750, '{}', '{}'),
(66, 'vagos', 0, 'boss', 'Parain', 1000, '{}', '{}'),
(67, 'ballas', 0, 'soldato', 'Ptite-Frappe', 400, '{}', '{}'),
(68, 'ballas', 1, 'capo', 'Capo', 600, '{}', '{}'),
(69, 'ballas', 2, 'consigliere', 'Consigliere', 800, '{}', '{}'),
(70, 'ballas', 3, 'boss', 'Parain', 1000, '{}', '{}'),
(71, 'traffic', 0, 'employee', 'Valores', 200, '{}', '{}'),
(72, 'poolcleaner', 0, 'interim', 'Interimaire', 400, '{}', '{}'),
(73, 'carthief', 0, 'thief', 'Thief', 50, '{}', '{}'),
(74, 'carthief', 1, 'bodyguard', 'Bodyguard', 70, '{}', '{}'),
(75, 'carthief', 2, 'boss', 'Boss', 100, '{}', '{}'),
(76, 'salvage', 0, 'interim', 'Diver', 80, '{}', '{}'),
(77, 'realestateagent', 0, 'agnt', 'Agent', 550, '{}', '{}'),
(78, 'realestateagent', 1, 'seller', 'Senior Agent', 965, '{}', '{}'),
(79, 'realestateagent', 2, 'mgmtn', 'Management', 1750, '{}', '{}'),
(80, 'realestateagent', 3, 'boss', 'Boss', 3000, '{}', '{}'),
(81, 'police', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(82, 'police', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(83, 'police', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(84, 'police', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(85, 'police', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(86, 'police', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(87, 'police', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(88, 'police', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(89, 'police', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(90, 'police', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(91, 'police', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(92, 'police', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(93, 'police', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(94, 'police', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(95, 'police', 14, 'reserve', '(ACSO) Reserve Deputy', 150, '{}', '{}'),
(96, 'police', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(97, 'police', 16, 'corporal', '(ACSO) Sargent', 250, '{}', '{}'),
(98, 'police', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(99, 'police', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(100, 'police', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(101, 'police', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(102, 'police', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(103, 'police', 22, 'chief', '(ACSO) Asst Chief', 400, '{}', '{}'),
(104, 'police', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(105, 'police', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(106, 'police', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(107, 'police', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(108, 'police', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(109, 'police', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(110, 'police', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(111, 'police', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(112, 'police', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(113, 'police', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(114, 'police', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(115, 'police', 34, 'chief', '(CSP) Asst Chief', 400, '{}', '{}'),
(116, 'police', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(117, 'police', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(118, 'police', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(119, 'police', 38, 'staff', 'Admin', 0, '{}', '{}'),
(120, 'police', 39, 'owner', 'Owner', 0, '{}', '{}'),
(121, 'fire', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(122, 'fire', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(123, 'fire', 2, 'reservefirefighter', 'Reserve Firefighter', 150, '{}', '{}'),
(124, 'fire', 3, 'firefighter', 'Firefighter', 200, '{}', '{}'),
(125, 'fire', 4, 'lieutenant', 'Lieutenant', 250, '{}', '{}'),
(126, 'fire', 5, 'captain', 'Captain', 300, '{}', '{}'),
(127, 'fire', 6, 'sharkone', 'Shark One', 300, '{}', '{}'),
(128, 'fire', 7, 'firemarshall', 'Fire Marshall', 300, '{}', '{}'),
(129, 'fire', 8, 'fto', 'FTO', 300, '{}', '{}'),
(130, 'fire', 9, 'hr', 'HR', 300, '{}', '{}'),
(131, 'fire', 10, 'battalionchief', 'Battalion Chief', 350, '{}', '{}'),
(132, 'fire', 11, 'deputyfirechief', 'Deputy Fire Chief', 2000, '{}', '{}'),
(133, 'fire', 12, 'boss', 'Fire Chief', 2000, '{}', '{}'),
(134, 'ambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(135, 'ambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(136, 'ambulance', 2, 'emr', 'Emergency Medical Responder', 150, '{}', '{}'),
(137, 'ambulance', 3, 'emt', 'Emergency Medical Tech', 200, '{}', '{}'),
(138, 'ambulance', 4, 'aemt', 'Advanced Emergency Medical Tech', 250, '{}', '{}'),
(139, 'ambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(140, 'ambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(141, 'ambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(142, 'ambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(143, 'ambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(144, 'ambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(145, 'ambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(146, 'ambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}'),
(147, 'staff', 0, 'tester', 'Tester', 0, '{}', '{}'),
(148, 'staff', 1, 'admin', 'Admin', 0, '{}', '{}'),
(149, 'staff', 2, 'developer', 'Developer', 0, '{}', '{}'),
(150, 'staff', 3, 'owner', 'Owner', 0, '{}', '{}'),
(151, 'fork', 0, 'employee', 'Operator', 20, '{}', '{}'),
(152, 'journaliste', 0, 'stagiaire', 'Journalist', 250, '{}', '{}'),
(153, 'journaliste', 1, 'reporter', 'Reporter', 350, '{}', '{}'),
(154, 'journaliste', 2, 'investigator', 'Investigator', 400, '{}', '{}'),
(155, 'journaliste', 3, 'boss', 'News Anchor', 450, '{}', '{}'),
(156, 'parking', 0, 'meter_maid', 'Meter Maid', 650, '{}', '{}'),
(157, 'parking', 1, 'parking_enforcement', 'Parking Enforcement', 650, '{}', '{}'),
(158, 'parking', 2, 'boss', 'CEO', 1000, '{}', '{}'),
(159, 'admin', 0, 'tester', 'Tester', 250, '{}', '{}'),
(160, 'admin', 1, 'admin', 'Admin', 500, '{}', '{}'),
(161, 'admin', 2, 'developer', 'developer', 750, '{}', '{}'),
(162, 'admin', 3, 'boss', 'Owner', 1000, '{}', '{}'),
(163, 'biker', 0, 'soldato', 'Prospect', 1500, '{}', '{}'),
(164, 'biker', 1, 'capo', 'Chapter member', 1800, '{}', '{}'),
(165, 'biker', 2, 'consigliere', 'Nomad', 2100, '{}', '{}'),
(166, 'biker', 3, 'boss', 'President', 10000, '{}', '{}'),
(167, 'unemployed', 0, 'rsa', 'Welfare', 100, '{}', '{}'),
(168, 'cardealer', 0, 'recruit', 'Recruit', 100, '{}', '{}'),
(169, 'cardealer', 1, 'novice', 'Novice', 250, '{}', '{}'),
(170, 'cardealer', 2, 'experienced', 'Experienced', 350, '{}', '{}'),
(171, 'cardealer', 3, 'boss', 'Boss', 500, '{}', '{}'),
(338, 'offpolice', 0, 'cadet', '(CCPD) Cadet', 100, '{}', '{}'),
(339, 'offambulance', 0, 'cadet', 'Cadet', 100, '{}', '{}'),
(340, 'offpolice', 1, 'probationary', '(CCPD) Probationary', 150, '{}', '{}'),
(341, 'offpolice', 2, 'reserve', '(CCPD) Reserve Officer', 150, '{}', '{}'),
(342, 'offpolice', 3, 'officer', '(CCPD) Officer', 200, '{}', '{}'),
(343, 'offpolice', 4, 'sergent', '(CCPD) Sargent', 250, '{}', '{}'),
(344, 'offpolice', 5, 'dui', '(CCPD) DUI', 300, '{}', '{}'),
(345, 'offpolice', 6, 'sru', '(CCPD) SRU', 300, '{}', '{}'),
(346, 'offpolice', 7, 'fto', '(CCPD) FTO', 300, '{}', '{}'),
(347, 'offpolice', 8, 'hr', '(CCPD) HR', 300, '{}', '{}'),
(348, 'offpolice', 9, 'captain', '(CCPD) Captain', 350, '{}', '{}'),
(349, 'offpolice', 10, 'asstchief', '(CCPD) Asst Chief', 400, '{}', '{}'),
(350, 'offpolice', 11, 'chief', '(CCPD) Chief', 500, '{}', '{}'),
(351, 'offpolice', 12, 'cadet', '(ACSO) Cadet', 100, '{}', '{}'),
(352, 'offpolice', 13, 'probationary', '(ACSO) Probationary', 150, '{}', '{}'),
(353, 'offpolice', 14, 'reserve', '(ACSO) Reserve Officer', 150, '{}', '{}'),
(354, 'offpolice', 15, 'deputy', '(ACSO) Deputy', 200, '{}', '{}'),
(355, 'offpolice', 16, 'corporal', '(ACSO) Corporal', 250, '{}', '{}'),
(356, 'offpolice', 17, 'dui', '(ACSO) DUI', 300, '{}', '{}'),
(357, 'offpolice', 18, 'sru', '(ACSO) SRU', 300, '{}', '{}'),
(358, 'offpolice', 19, 'fto', '(ACSO) FTO', 300, '{}', '{}'),
(359, 'offpolice', 20, 'hr', '(ACSO) HR', 300, '{}', '{}'),
(360, 'offpolice', 21, 'captain', '(ACSO) Captain', 350, '{}', '{}'),
(361, 'offpolice', 22, 'chief', '(ACSO) Chief', 400, '{}', '{}'),
(362, 'offpolice', 23, 'sheriff', '(ACSO) Sheriff', 500, '{}', '{}'),
(363, 'offpolice', 24, 'cadet', '(CSP) Cadet', 100, '{}', '{}'),
(364, 'offpolice', 25, 'probationary', '(CSP) Probationary', 150, '{}', '{}'),
(365, 'offpolice', 26, 'reserve', '(CSP) Reserve Trooper', 150, '{}', '{}'),
(366, 'offpolice', 27, 'trooper', '(CSP) Trooper', 200, '{}', '{}'),
(367, 'offpolice', 28, 'sergent', '(CSP) Sargent', 250, '{}', '{}'),
(368, 'offpolice', 29, 'dui', '(CSP) DUI', 300, '{}', '{}'),
(369, 'offpolice', 30, 'sru', '(CSP) SRU', 300, '{}', '{}'),
(370, 'offpolice', 31, 'fto', '(CSP) FTO', 300, '{}', '{}'),
(371, 'offpolice', 32, 'hr', '(CSP) HR', 300, '{}', '{}'),
(372, 'offpolice', 33, 'captain', '(CSP) Captain', 350, '{}', '{}'),
(373, 'offpolice', 34, 'chief', '(CSP) Chief', 400, '{}', '{}'),
(374, 'offpolice', 35, 'colonel', '(CSP) Colonel', 500, '{}', '{}'),
(375, 'offpolice', 36, 'boss', 'Deputy Commissioner', 10000, '{}', '{}'),
(376, 'offpolice', 37, 'boss', 'Commissioner', 10000, '{}', '{}'),
(377, 'offambulance', 1, 'probationary', 'Probationary', 150, '{}', '{}'),
(378, 'offambulance', 2, 'emr', 'Emergency Medical Response', 150, '{}', '{}'),
(379, 'offambulance', 3, 'emt', 'Emergency Medical Technician', 200, '{}', '{}'),
(380, 'offambulance', 4, 'aemt', 'Advanced Emergency Medical Technician', 250, '{}', '{}'),
(381, 'offambulance', 5, 'paramedic', 'Paramedic', 300, '{}', '{}'),
(382, 'offambulance', 6, 'medone', 'Med One', 0, '{}', '{}'),
(383, 'offambulance', 7, 'supervisor', 'Supervisor', 300, '{}', '{}'),
(384, 'offambulance', 8, 'fto', 'FTO', 300, '{}', '{}'),
(385, 'offambulance', 9, 'medicalchief', 'Medical Chief', 400, '{}', '{}'),
(386, 'offambulance', 10, 'deputymedicaldirector', 'Deputy Medical Director', 2000, '{}', '{}'),
(387, 'offambulance', 11, 'boss', 'Medical Director', 2000, '{}', '{}'),
(388, 'offambulance', 12, 'boss', 'Medical Examiner', 0, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kicks`
--

CREATE TABLE `kicks` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`id`, `type`, `label`) VALUES
(1, 'dmv', 'Driving Permit'),
(2, 'drive', 'Drivers License'),
(3, 'drive_bike', 'Motorcycle License'),
(4, 'drive_truck', 'Commercial Drivers License'),
(5, 'weapon', 'Permis de port d\'arme'),
(6, 'weapon', 'Permis de port d\'arme'),
(7, 'weapon', 'Permis de port d\'arme'),
(8, 'weapon', 'Permis de port d\'arme');

-- --------------------------------------------------------

--
-- Table structure for table `nitro_vehicles`
--

CREATE TABLE `nitro_vehicles` (
  `plate` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicles`
--

CREATE TABLE `old_vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicles`
--

INSERT INTO `old_vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 15000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 18000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
(4, 'Chino', 'chino', 15000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 19000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 250000, 'muscle'),
(7, 'Dominator', 'dominator', 60000, 'muscle'),
(8, 'Dukes', 'dukes', 28000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 30000, 'muscle'),
(10, 'Hotknife', 'hotknife', 125000, 'muscle'),
(11, 'Faction', 'faction', 20000, 'muscle'),
(12, 'Faction Rider', 'faction2', 30000, 'muscle'),
(13, 'Faction XL', 'faction3', 40000, 'muscle'),
(14, 'Nightshade', 'nightshade', 65000, 'muscle'),
(15, 'Phoenix', 'phoenix', 12500, 'muscle'),
(16, 'Picador', 'picador', 18000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 20000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 25000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 11500, 'muscle'),
(20, 'Tampa', 'tampa', 16000, 'muscle'),
(21, 'Virgo', 'virgo', 14000, 'muscle'),
(22, 'Vigero', 'vigero', 12500, 'muscle'),
(23, 'Voodoo', 'voodoo', 7200, 'muscle'),
(24, 'Blista', 'blista', 42958, 'compacts'),
(25, 'Brioso R/A', 'brioso', 18000, 'compacts'),
(26, 'Issi', 'issi2', 10000, 'compacts'),
(27, 'Panto', 'panto', 10000, 'compacts'),
(28, 'Prairie', 'prairie', 12000, 'compacts'),
(29, 'Bison', 'bison', 45000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 32000, 'vans'),
(31, 'Burrito', 'burrito3', 19000, 'work'),
(32, 'Burrito', 'gburrito2', 29000, 'vans'),
(33, 'Camper', 'camper', 42000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 45000, 'vans'),
(35, 'Journey', 'journey', 6500, 'vans'),
(36, 'Minivan', 'minivan', 13000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 18000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
(39, 'Paradise', 'paradise', 19000, 'vans'),
(40, 'Rumpo', 'rumpo', 30000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 19500, 'vans'),
(42, 'Surfer', 'surfer', 12000, 'vans'),
(43, 'Youga', 'youga', 10800, 'vans'),
(44, 'Youga Luxuary', 'youga2', 14500, 'vans'),
(45, 'Asea', 'asea', 5500, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 55000, 'sedans'),
(47, 'Emperor', 'emperor', 8500, 'sedans'),
(48, 'Fugitive', 'fugitive', 12000, 'sedans'),
(49, 'Glendale', 'glendale', 6500, 'sedans'),
(50, 'Intruder', 'intruder', 7500, 'sedans'),
(51, 'Premier', 'premier', 8000, 'sedans'),
(52, 'Primo Custom', 'primo2', 14000, 'sedans'),
(53, 'Regina', 'regina', 5000, 'sedans'),
(54, 'Schafter', 'schafter2', 25000, 'sedans'),
(55, 'Stretch', 'stretch', 90000, 'sedans'),
(56, 'Phantom', 'superd', 250000, 'sedans'),
(57, 'MercedezAMG', 'tailgater', 130000, 'modded'),
(58, 'Warrener', 'warrener', 120000, 'sedans'),
(59, 'Washington', 'washington', 90000, 'sedans'),
(60, 'Baller', 'baller2', 40000, 'suvs'),
(61, 'Baller Sport', 'baller3', 60000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 55000, 'suvs'),
(63, 'Contender', 'contender', 70000, 'suvs'),
(64, 'Dubsta', 'dubsta', 45000, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
(66, 'Fhantom', 'fq2', 17000, 'suvs'),
(67, 'Grabger', 'granger', 50000, 'suvs'),
(68, 'Gresley', 'gresley', 47500, 'suvs'),
(69, 'Huntley S', 'huntley', 40000, 'suvs'),
(70, 'Landstalker', 'landstalker', 35000, 'suvs'),
(71, 'Mesa', 'mesa', 16000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 40000, 'suvs'),
(73, 'Patriot', 'patriot', 55000, 'suvs'),
(74, 'Radius', 'radi', 29000, 'suvs'),
(75, 'Rocoto', 'rocoto', 45000, 'suvs'),
(76, 'Seminole', 'seminole', 25000, 'suvs'),
(77, 'XLS', 'xls', 70000, 'suvs'),
(78, 'Btype', 'btype', 62000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 85000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
(81, 'Casco', 'casco', 350000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
(83, 'Manana', 'manana', 12800, 'sportsclassics'),
(84, 'Monroe', 'monroe', 55000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 20000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 80000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 220000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
(89, 'Porsche Classic', 'ztype', 160000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 12000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 16000, 'offroad'),
(92, 'Blazer', 'blazer', 6500, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 8500, 'offroad'),
(94, 'Brawler', 'brawler', 75000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
(96, 'Dune Buggy', 'dune', 8000, 'offroad'),
(97, 'Guardian', 'guardian', 45000, 'offroad'),
(98, 'Rebel', 'rebel2', 35000, 'offroad'),
(99, 'Sandking', 'sandking', 55000, 'offroad'),
(100, 'The Liberator', 'monster', 210000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 60000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
(104, 'Exemplar', 'exemplar', 132000, 'modded'),
(105, 'Silvia', 'f620', 80000, 'modded'),
(106, 'Felon', 'felon', 42000, 'coupes'),
(107, 'Felon GT', 'felon2', 55000, 'coupes'),
(108, 'Jackal', 'jackal', 38000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 35000, 'coupes'),
(110, 'Sentinel', 'sentinel', 32000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 40000, 'coupes'),
(112, 'Windsor', 'windsor', 350000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 180000, 'coupes'),
(114, 'Zion', 'zion', 40000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 70000, 'coupes'),
(116, '9F', 'ninef', 65000, 'sports'),
(117, '9F Cabrio', 'ninef2', 80000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 125000, 'modded'),
(120, 'Bestia GTS', 'bestiagts', 400000, 'sports'),
(121, 'Buffalo', 'buffalo', 120000, 'modded'),
(122, 'wide body charger', 'buffalo2', 175000, 'modded'),
(123, 'Carbonizzare', 'carbonizzare', 110000, 'modded'),
(124, 'Comet', 'comet2', 200000, 'modded'),
(125, 'Coquette', 'coquette', 65000, 'sports'),
(126, 'Vantage', 'tampa2', 230000, 'sports'),
(127, 'Elegy', 'elegy2', 175000, 'sports'),
(128, 'Feltzer', 'feltzer2', 125000, 'sports'),
(129, 'Furore GT', 'furoregt', 45000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 135000, 'sports'),
(133, 'Khamelion', 'khamelion', 38000, 'sports'),
(134, 'Kuruma', 'kuruma', 75000, 'sports'),
(135, 'Lynx', 'lynx', 40000, 'sports'),
(136, 'Mamba', 'mamba', 70000, 'sports'),
(137, 'Massacro', 'massacro', 65000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 130000, 'sports'),
(139, 'Omnis', 'omnis', 35000, 'sports'),
(140, 'Penumbra', 'penumbra', 28000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 35000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
(143, 'Schafter V12', 'schafter3', 130000, 'sports'),
(144, 'Seven 70', 'seven70', 290000, 'sports'),
(145, 'Sultan', 'sultan', 55000, 'sports'),
(146, 'Surano', 'surano', 50000, 'sports'),
(147, 'Tropos', 'tropos', 95000, 'sports'),
(148, 'Verlierer', 'verlierer2', 70000, 'sports'),
(149, 'Adder', 'adder', 1000000, 'modded'),
(150, 'Banshee 900R', 'banshee2', 350000, 'super'),
(151, 'Bullet', 'bullet', 250000, 'super'),
(152, 'Cheetah', 'cheetah', 500000, 'modded'),
(153, 'Entity XF', 'entityxf', 425000, 'super'),
(154, 'Maserati ET1', 'sheava', 500000, 'super'),
(155, 'FMJ', 'fmj', 185000, 'super'),
(156, 'Infernus', 'infernus', 240000, 'modded'),
(157, 'Osiris', 'osiris', 160000, 'super'),
(158, 'Pfister', 'pfister811', 85000, 'super'),
(159, 'RE-7B', 'le7b', 325000, 'super'),
(160, 'Reaper', 'reaper', 150000, 'super'),
(161, 'Sultan RS', 'sultanrs', 130000, 'super'),
(162, 'T20', 't20', 300000, 'super'),
(163, 'Turismo R', 'turismor', 350000, 'super'),
(164, 'Tyrus', 'tyrus', 600000, 'super'),
(165, 'Vacca', 'vacca', 120000, 'super'),
(166, 'Voltic', 'voltic', 900000, 'super'),
(167, 'X80 Proto', 'prototipo', 950000, 'super'),
(168, 'Zentorno', 'zentorno', 700000, 'super'),
(169, 'Akuma', 'AKUMA', 7500, 'motorcycles'),
(170, 'Avarus', 'avarus', 18000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13500, 'motorcycles'),
(172, 'Bati 801', 'bati', 12000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 19000, 'motorcycles'),
(174, 'BF400', 'bf400', 6500, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 160, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 18000, 'motorcycles'),
(177, 'Chimera', 'chimera', 38000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
(180, 'Daemon', 'daemon', 11500, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 13500, 'motorcycles'),
(182, 'Defiler', 'defiler', 9800, 'motorcycles'),
(183, 'Double T', 'double', 28000, 'motorcycles'),
(184, 'Enduro', 'enduro', 5500, 'motorcycles'),
(185, 'Esskey', 'esskey', 4200, 'motorcycles'),
(186, 'Faggio', 'faggio', 1900, 'motorcycles'),
(187, 'Vespa', 'faggio2', 2800, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 225, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 16500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 31000, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
(192, 'Hexer', 'hexer', 12000, 'motorcycles'),
(193, 'Innovation', 'innovation', 23500, 'motorcycles'),
(194, 'Manchez', 'manchez', 5300, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 5800, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 35000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 6200, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 6800, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 7500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 7500, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 25000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 22000, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 80000, 'motorcycles'),
(205, 'Thrust', 'thrust', 24000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
(207, 'Vader', 'vader', 7200, 'motorcycles'),
(208, 'Vortex', 'vortex', 9800, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 9500, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
(213, 'Ruiner', 'ruiner', 90000, 'muscle'),
(214, 'TunedComet', 'comet3', 300000, 'super'),
(215, 'TunedslamVan', 'slamvan3', 270000, 'muscle'),
(216, 'TunedVirgo', 'virgo3', 220000, 'muscle'),
(217, 'Nero', 'nero', 400000, 'super'),
(218, 'TunedBucaneer', 'buccaneer2', 200000, 'muscle'),
(219, 'TunedChino', 'chino2', 190000, 'muscle'),
(220, 'TunedElegy', 'elegy', 220000, 'sports'),
(221, 'TunedFaction', 'faction2', 190000, 'muscle'),
(222, 'PegassiFCR', 'fcr', 8000, 'motorcycles'),
(223, 'TunedPegassi', 'fcr2', 15000, 'motorcycles'),
(224, 'ProgenItaliGTB', 'italigtb', 260000, 'super'),
(225, 'TunedItaliGTB', 'italigtb2', 320000, 'super'),
(226, 'Tunedminivan', 'minivan2', 160000, 'suvs'),
(227, 'TunedNero', 'nero2', 500000, 'super'),
(228, 'Primo', 'primo', 35000, 'coupes'),
(229, 'DewbaucheeSpecter', 'specter', 220000, 'sports'),
(230, 'Tunedspecter', 'specter2', 290000, 'sports'),
(231, 'TunedVan', 'slamvan2', 25000, 'muscle'),
(232, 'EMSCar', 'emscar', 1000, 'work'),
(233, 'EMSCar2', 'emscar2', 1000, 'work'),
(234, 'EMSVan', 'emsvan', 1000, 'work'),
(235, 'EMSSuv', 'emssuv', 1000, 'work'),
(236, 'Rat Loader', 'ratloader', 10000, 'work'),
(237, 'Sand King2', 'sandking2', 60000, 'offroad'),
(238, 'Sadler', 'sadler', 75000, 'offroad'),
(239, 'Taxi', 'taxi', 15000, 'work'),
(240, 'Rubble', 'rubble', 100000, 'work'),
(241, 'Tour Bus', 'tourbus', 30000, 'work'),
(242, 'Tow Truck', 'towtruck', 25000, 'work'),
(243, 'Flat Bed', 'flatbed', 27000, 'work'),
(244, 'Clown', 'speedo2', 16000, 'work'),
(246, 'Stratum', 'stratum', 80000, 'sports'),
(249, 'Mazda', 'blista3', 40000, 'compacts'),
(250, 'Honda Civic', 'blista2', 30000, 'compacts'),
(251, 'Caddilac', 'buffalo3', 50000, 'sedans'),
(252, 'Golf Green', 'surge', 70000, 'compacts'),
(253, 'Stalion', 'stalion', 60000, 'muscle'),
(254, 'SRT', 'stalion2', 160000, 'muscle'),
(255, 'Mercedez RI', 'serrano', 90000, 'suvs'),
(256, 'Mercedes AMG', 'schafter4', 140000, 'sedans'),
(257, 'BMW M3', 'schafter5', 135000, 'sedans'),
(258, 'BMW M3', 'schwarzer', 150000, 'sedans'),
(259, 'gt500', 'gt500', 45000, 'doomsday'),
(260, 'comet4', 'comet4', 180000, 'doomsday'),
(261, 'comet5', 'comet5', 200000, 'doomsday'),
(263, 'hermes', 'hermes', 60000, 'doomsday'),
(264, 'hustler', 'hustler', 60000, 'doomsday'),
(265, 'kamacho', 'kamacho', 50000, 'doomsday'),
(266, 'neon', 'neon', 100000, 'doomsday'),
(267, 'pariah', 'pariah', 100000, 'doomsday'),
(268, 'raiden', 'raiden', 75000, 'doomsday'),
(269, 'revolter', 'revolter', 75000, 'doomsday'),
(270, 'riata', 'riata', 75000, 'doomsday'),
(271, 'savestra', 'savestra', 75000, 'doomsday'),
(272, 'sc1', 'sc1', 75000, 'doomsday'),
(273, 'streiter', 'streiter', 75000, 'doomsday'),
(274, 'stromberg', 'stromberg', 75000, 'doomsday'),
(275, 'sentinel3', 'sentinel3', 75000, 'doomsday'),
(276, 'viseris', 'viseris', 75000, 'doomsday'),
(277, 'yosemite', 'yosemite', 75000, 'doomsday'),
(278, 'z190', 'z190', 75000, 'doomsday'),
(279, 'Mustang', 'musty5', 90000, 'modded'),
(280, 'Infiniti G37', 'g37cs', 50000, 'modded'),
(281, 'Peugeot 107', 'p107', 30000, 'modded'),
(282, 'Renault Megane', 'renmeg', 70000, 'modded'),
(283, 'Lamborghini Hurricane', 'lh610', 230000, 'modded'),
(284, 'Aston Cygnet', 'cygnet11', 40000, 'modded'),
(285, 'Cadillac CTS', 'cadicts', 55000, 'modded'),
(286, 'Mini John Cooper', 'miniub', 45000, 'modded'),
(287, 'Lotus Espirit V8', 'lev8', 130000, 'modded'),
(288, 'Lambo Veneno', 'lamven', 500000, 'modded'),
(289, 'Nissan GTR SpecV', 'gtrublu', 230000, 'modded'),
(290, 'Genesis', 'genublu', 60000, 'modded'),
(291, 'Porsche Cayman R', 'caymanub', 190000, 'modded'),
(292, 'Porsche 911 GT', '911ublu', 700000, 'modded'),
(293, 'Ferrari Laferrari', 'laferublu', 450000, 'modded'),
(294, 'Mclaren 12c', 'mcublu', 400000, 'modded'),
(295, 'Merc SLR', 'slrublu', 200000, 'modded'),
(297, 'Merc SLS AMG Electric', 'slsublue', 150000, 'modded'),
(298, 'Dodge Charger', 'charublu', 90000, 'modded'),
(299, 'subaru 22b', '22bbublu', 80000, 'modded'),
(300, 'Focus RS', 'focusublu', 70000, 'modded'),
(301, 'Mazda furai', 'furaiub', 350000, 'modded'),
(302, 'Ferrari F50', 'f50ub', 350000, 'modded'),
(303, 'Porsche 550a', 'p550a', 250000, 'modded'),
(304, 'Porsche 959', 'p959', 250000, 'modded'),
(305, 'Porsche 944', 'p944', 180000, 'modded'),
(306, 'dodge Viper', 'vip99', 450000, 'modded'),
(307, 'Mazda Rx8', 'rx8', 50000, 'modded'),
(308, 'Ferrari 599', 'gtbf', 290000, 'modded'),
(309, 'Tesla Roadster', 'tesla11', 55000, 'modded'),
(310, 'Mazda Mx5a', 'mx5a', 45000, 'modded'),
(311, 'toyota Celica', 'celicassi', 48000, 'modded'),
(312, 'Toyota celica T', 'celicassi2', 55000, 'modded'),
(313, 'Aston Martin Vanquish', 'amv12', 280000, 'modded'),
(314, 'Subari WRX STI', 'sti05', 80000, 'modded'),
(315, 'Porsche Panamera', 'panamera', 200000, 'modded'),
(316, 'Ferrari 360', 'f360', 250000, 'modded'),
(317, 'Lambo Mura', 'miura', 230000, 'modded'),
(318, 'chevrolet Corvette', 'zr1c3', 180000, 'modded'),
(319, 'Lambo Gallardo', 'gallardo', 300000, 'modded'),
(320, 'Corvette Stingray', 'vc7', 230000, 'modded'),
(321, 'Ferrari Cali', '2fiftygt', 260000, 'modded'),
(322, 'Mercedz Gullwing', '300gsl', 200000, 'modded'),
(323, 'Aston Martin vantage', 'db700', 140000, 'modded'),
(324, 'shelby cobra', 'cobra', 130000, 'modded'),
(325, 'BMW Z4i', 'z4i', 100000, 'modded'),
(326, 'Lambo Huracan', 'huracan', 240000, 'modded'),
(327, 'Ferrari 812', 'ferrari812', 250000, 'modded'),
(328, 'Lambo Veneno', 'veneno', 350000, 'modded'),
(329, 'Ferrari XXK', 'fxxk16', 300000, 'modded'),
(330, 'LaFerrari 15', 'laferrari15', 400000, 'modded'),
(331, 'Italia 458 LW', 'lw458s', 320000, 'modded'),
(332, 'Lykan', 'lykan', 350000, 'modded'),
(333, 'iTalia 458', 'italia458', 290000, 'modded'),
(334, 'Diablous', 'Diablous', 15000, 'motorcycles'),
(335, 'Diablous 2', 'Diablous2', 17000, 'motorcycles'),
(336, 'Raptor', 'Raptor', 20000, 'motorcycles'),
(337, 'Ratbike', 'Ratbike', 16000, 'motorcycles'),
(338, 'Xa21', 'XA21', 340000, 'super'),
(339, 'Penetrator', 'Penetrator', 300000, 'super'),
(340, 'Gp1', 'GP1', 270000, 'super'),
(341, 'Tempestra', 'Tempesta', 260000, 'super'),
(342, 'Toreo', 'Torero', 250000, 'sportsclassics'),
(343, 'Infernus2', 'Infernus2', 240000, 'sportsclassics'),
(344, 'Savestra', 'Savestra', 240000, 'sportsclassics'),
(345, 'Cheetah2', 'Cheetah2', 200000, 'sportsclassics'),
(346, 'Turismo2', 'Turismo2', 180000, 'sportsclassics'),
(347, 'Viceris', 'Viceris', 190000, 'sportsclassics'),
(348, 'JB700', 'JB700', 190000, 'sportsclassics'),
(349, 'Peyote', 'Peyote', 175000, 'sportsclassics'),
(350, 'Ruston', 'Ruston', 150000, 'sports'),
(351, 'Surge', 'Surge', 45000, 'sedans'),
(353, 'Voltic', 'Voltic2', 4000000, 'super'),
(354, 'Dilettante', 'Dilettante', 50000, 'compacts'),
(355, 'Tornado6', 'Tornado6', 150000, 'muscle'),
(356, 'Gauntlet2', 'Gauntlet2', 100000, 'muscle'),
(357, 'Dominator2', 'Dominator2', 100000, 'muscle'),
(358, 'Hurse', 'Lurcher', 60000, 'muscle'),
(359, 'Vagner', 'Vagner', 250000, 'super'),
(360, 'Austarch', 'Autarch', 230000, 'super'),
(361, 'Tornado5', 'tornado5', 75000, 'muscle'),
(362, 'audi a4', 'asterope', 110000, 'sports'),
(363, 'Merc AMG', 'rmodamgc63', 165000, 'sports'),
(364, 'dodgeCharger', '69charger', 140000, 'muscle'),
(365, 'R35', 'r35', 200000, 'modded'),
(367, 'Mustang', 'mgt', 180000, 'modded'),
(368, 'Cheburek', 'cheburek', 20000, 'assault'),
(369, 'Ellie', 'ellie', 100000, 'assault'),
(370, 'Dommy3', 'dominator3', 110000, 'assault'),
(371, 'Enity2', 'entity2', 200000, 'assault'),
(372, 'Fagi', 'fagaloa', 22000, 'assault'),
(373, 'Flash', 'flashgt', 105000, 'assault'),
(374, 'RS200', 'gb200', 135000, 'assault'),
(375, 'Hotring', 'hotring', 140000, 'assault'),
(376, 'Mini', 'issi3', 18000, 'assault'),
(377, 'Jester3', 'jester3', 85000, 'assault'),
(378, 'Michelle', 'michelli', 41000, 'assault'),
(379, 'Tai', 'taipan', 220000, 'assault'),
(380, 'Tezeract', 'tezeract', 180000, 'assault'),
(381, 'Tyrant', 'tyrant', 220000, 'assault'),
(382, 'Impreza', 'ySbrImpS11', 100000, 'modded'),
(383, 'R35 Skyline', 'r35', 200000, 'sports'),
(384, 'Insurgent', 'insurgent3', 200000, 'modded'),
(385, 'Halftrack', 'halftrack', 200000, 'modded'),
(386, 'Tempa', 'tampa3', 200000, 'modded'),
(387, 'Technical', 'technical3', 200000, 'modded'),
(388, 'Tech2', 'technical2', 200000, 'modded'),
(389, 'Barrage', 'barrage', 200000, 'modded'),
(390, 'Boxville', 'boxville5', 200000, 'modded'),
(391, 'Road Glide', 'foxharley2', 3000, 'motorcycles'),
(392, 'Harley Bobber', 'foxharley1', 3000, 'motorcycles');

-- --------------------------------------------------------

--
-- Table structure for table `old_vehicle_categories`
--

CREATE TABLE `old_vehicle_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_vehicle_categories`
--

INSERT INTO `old_vehicle_categories` (`id`, `name`, `label`) VALUES
(1, 'compacts', 'Compacts'),
(2, 'coupes', 'Coupés'),
(3, 'sedans', 'Sedans'),
(4, 'sports', 'Sports'),
(5, 'sportsclassics', 'Sports Classics'),
(6, 'super', 'Super'),
(7, 'muscle', 'Muscle'),
(8, 'offroad', 'Off Road'),
(9, 'suvs', 'SUVs'),
(10, 'vans', 'Vans'),
(11, 'motorcycles', 'Motos'),
(12, 'work', 'Work'),
(13, 'doomsday', 'doomsday'),
(14, 'modded', 'Modded'),
(15, 'assault', 'assault');

-- --------------------------------------------------------

--
-- Table structure for table `outfits`
--

CREATE TABLE `outfits` (
  `identifier` varchar(30) NOT NULL,
  `skin` varchar(30) NOT NULL COMMENT 'mp_m_freemode_01',
  `face` int(11) NOT NULL COMMENT '0',
  `face_text` int(11) NOT NULL COMMENT '0',
  `hair` int(11) NOT NULL COMMENT '0',
  `pants` int(11) NOT NULL COMMENT '0',
  `pants_text` int(11) NOT NULL COMMENT '0',
  `shoes` int(11) NOT NULL COMMENT '0',
  `shoes_text` int(11) NOT NULL COMMENT '0',
  `torso` int(11) NOT NULL COMMENT '0',
  `torso_text` int(11) NOT NULL COMMENT '0',
  `shirt` int(11) NOT NULL COMMENT '0',
  `shirt_text` int(11) NOT NULL COMMENT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `owned_dock`
--

CREATE TABLE `owned_dock` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `state of the car` tinyint(1) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'State of the car',
  `finance` int(32) NOT NULL DEFAULT '0',
  `financetimer` int(32) NOT NULL DEFAULT '0',
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `stored` tinyint(1) NOT NULL DEFAULT '0',
  `jamstate` tinyint(11) NOT NULL DEFAULT '0',
  `fourrieremecano` tinyint(1) NOT NULL DEFAULT '0',
  `vehiclename` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT 'voiture'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `owner_vehicles`
--

CREATE TABLE `owner_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Num tel proprio',
  `num` varchar(10) NOT NULL COMMENT 'Num reférence du contact',
  `incoming` int(11) NOT NULL COMMENT 'Défini si on est à l''origine de l''appels',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accepts` int(11) NOT NULL COMMENT 'Appels accepter ou pas'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(123, '626-4798', '7395135', 1, '2019-08-15 03:19:08', 0),
(124, '739-5135', '626-4798', 0, '2019-08-15 03:20:46', 1),
(125, '626-4798', '739-5135', 1, '2019-08-15 03:20:46', 1),
(126, '739-5135', '626-4798', 0, '2019-08-15 03:21:38', 0),
(127, '626-4798', '739-5135', 1, '2019-08-15 03:21:38', 0),
(128, '739-5135', '626-4798', 0, '2019-08-15 03:21:44', 1),
(129, '626-4798', '739-5135', 1, '2019-08-15 03:21:44', 1),
(130, '626-4798', '739-5135', 1, '2019-08-15 03:23:34', 1),
(131, '739-5135', '626-4798', 0, '2019-08-15 03:23:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isRead` int(11) NOT NULL DEFAULT '0',
  `owner` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `license` varchar(50) NOT NULL,
  `steam` varchar(50) NOT NULL,
  `playtime` int(11) NOT NULL,
  `firstjoined` varchar(50) NOT NULL,
  `lastplayed` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `playerstattoos`
--

CREATE TABLE `playerstattoos` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tattoos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Apartment de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modyrn1Apartment', 'Apartment Modern 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modyrn2Apartment', 'Apartment Modern 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modyrn3Apartment', 'Apartment Modern 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Apartment Mody 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Apartment Mody 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Apartment Mody 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Apartment Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Apartment Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Apartment Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Apartment Persian 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Apartment Persian 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Apartment Persian 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Apartment Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Apartment Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Apartment Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Apartment Seductive 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Apartment Seductive 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Apartment Seductive 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Apartment Regal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Apartment Regal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Apartment Regal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Apartment Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Apartment Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Apartment Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000),
(43, 'MazeBankBuilding', 'Maze Bank Building', '{\"x\":-79.18,\"y\":-795.92,\"z\":43.35}', NULL, NULL, '{\"x\":-72.50,\"y\":-786.92,\"z\":43.40}', '[]', NULL, 0, 0, 1, NULL, 0),
(44, 'OldSpiceWarm', 'Old Spice Warm', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(45, 'OldSpiceClassical', 'Old Spice Classical', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(46, 'OldSpiceVintage', 'Old Spice Vintage', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_01c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(47, 'ExecutiveRich', 'Executive Rich', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(48, 'ExecutiveCool', 'Executive Cool', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(49, 'ExecutiveContrast', 'Executive Contrast', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_02a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(50, 'PowerBrokerIce', 'Power Broker Ice', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03a\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(51, 'PowerBrokerConservative', 'Power Broker Conservative', NULL, '', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03b\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(52, 'PowerBrokerPolished', 'Power Broker Polished', NULL, '{\"x\":-75.69,\"y\":-827.08,\"z\":242.43}', '{\"x\":-75.51,\"y\":-823.90,\"z\":242.43}', NULL, '[\"ex_dt1_11_office_03c\"]', 'MazeBankBuilding', 0, 1, 0, '{\"x\":-76.49,\"y\":-826.80,\"z\":243.38}', 5000000),
(53, 'LomBank', 'Lom Bank', '{\"x\":-1581.36,\"y\":-558.23,\"z\":34.07}', NULL, NULL, '{\"x\":-1583.60,\"y\":-555.12,\"z\":34.07}', '[]', NULL, 0, 0, 1, NULL, 0),
(54, 'LBOldSpiceWarm', 'LB Old Spice Warm', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(55, 'LBOldSpiceClassical', 'LB Old Spice Classical', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(56, 'LBOldSpiceVintage', 'LB Old Spice Vintage', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_01c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(57, 'LBExecutiveRich', 'LB Executive Rich', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(58, 'LBExecutiveCool', 'LB Executive Cool', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(59, 'LBExecutiveContrast', 'LB Executive Contrast', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_02a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(60, 'LBPowerBrokerIce', 'LB Power Broker Ice', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03a\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(61, 'LBPowerBrokerConservative', 'LB Power Broker Conservative', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03b\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(62, 'LBPowerBrokerPolished', 'LB Power Broker Polished', NULL, '{\"x\":-1579.53,\"y\":-564.89,\"z\":107.62}', '{\"x\":-1576.42,\"y\":-567.57,\"z\":107.62}', NULL, '[\"ex_sm_13_office_03c\"]', 'LomBank', 0, 1, 0, '{\"x\":-1577.68,\"y\":-565.91,\"z\":108.52}', 3500000),
(63, 'MazeBankWest', 'Maze Bank West', '{\"x\":-1379.58,\"y\":-499.63,\"z\":32.22}', NULL, NULL, '{\"x\":-1378.95,\"y\":-502.82,\"z\":32.22}', '[]', NULL, 0, 0, 1, NULL, 0),
(64, 'MBWOldSpiceWarm', 'MBW Old Spice Warm', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(65, 'MBWOldSpiceClassical', 'MBW Old Spice Classical', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(66, 'MBWOldSpiceVintage', 'MBW Old Spice Vintage', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_01c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(67, 'MBWExecutiveRich', 'MBW Executive Rich', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(68, 'MBWExecutiveCool', 'MBW Executive Cool', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(69, 'MBWExecutive Contrast', 'MBW Executive Contrast', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_02a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(70, 'MBWPowerBrokerIce', 'MBW Power Broker Ice', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03a\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(71, 'MBWPowerBrokerConvservative', 'MBW Power Broker Convservative', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03b\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000),
(72, 'MBWPowerBrokerPolished', 'MBW Power Broker Polished', NULL, '{\"x\":-1392.74,\"y\":-480.18,\"z\":71.14}', '{\"x\":-1389.43,\"y\":-479.01,\"z\":71.14}', NULL, '[\"ex_sm_15_office_03c\"]', 'MazeBankWest', 0, 1, 0, '{\"x\":-1390.76,\"y\":-479.22,\"z\":72.04}', 2700000);

-- --------------------------------------------------------

--
-- Table structure for table `qalle_brottsregister`
--

CREATE TABLE `qalle_brottsregister` (
  `id` int(255) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `dateofcrime` varchar(255) NOT NULL,
  `crime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `received_bans`
--

CREATE TABLE `received_bans` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `banned_by` varchar(255) DEFAULT NULL,
  `banned_on` varchar(255) DEFAULT NULL,
  `ban_expires` varchar(255) DEFAULT NULL,
  `userid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rented_dock`
--

CREATE TABLE `rented_dock` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(10) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `ID` int(11) NOT NULL,
  `name` int(11) NOT NULL,
  `connection` int(11) NOT NULL,
  `rcon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `server_actions`
--

CREATE TABLE `server_actions` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `action_do` varchar(255) DEFAULT NULL,
  `action_ammount` varchar(255) DEFAULT NULL,
  `byadmin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'PDShop', 'coffee', 1),
(2, 'PDShop', 'donut', 1),
(3, 'PDShop', 'clip', 1),
(4, 'PDShop', 'armor', 1),
(5, 'PDShop', 'medikit', 0),
(6, 'CityHall', 'weapons_license1', 100),
(7, 'CityHall', 'weapons_license2', 200),
(8, 'CityHall', 'weapons_license3', 300),
(62, 'CityHall', 'hunting_license', 100),
(63, 'CityHall', 'fishing_license', 100),
(64, 'CityHall', 'diving_license', 50),
(65, 'CityHall', 'marriage_license', 5000),
(66, 'DMV', 'drivers_license', 80),
(67, 'DMV', 'motorcycle_license', 80),
(68, 'DMV', 'commercial_license', 175),
(69, 'DMV', 'boating_license', 40),
(70, 'DMV', 'taxi_license', 175),
(71, 'DMV', 'pilot_license', 500);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `society` varchar(60) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `truck_inventory`
--

CREATE TABLE `truck_inventory` (
  `id` int(11) NOT NULL,
  `item` varchar(100) NOT NULL,
  `count` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `itemt` varchar(50) DEFAULT NULL,
  `owned` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(38, 'Jak Fulton', 'fuck4tick', NULL),
(39, 'tpickles', 'stoner420', 'kermit.png');

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 39, 'steam:11000010a01bdb9', 'New city who dis?', '2019-08-09 03:24:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `ID` int(11) NOT NULL,
  `rank` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `steamid` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `job` varchar(255) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT '0',
  `loadout` longtext COLLATE utf8mb4_bin,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` longtext COLLATE utf8mb4_bin,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `animal` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `timeplayed` varchar(255) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `online` int(10) NOT NULL DEFAULT '0',
  `server` int(10) NOT NULL DEFAULT '1',
  `is_dead` tinyint(1) DEFAULT '0',
  `lastPosition` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `ID`, `rank`, `steamid`, `license`, `money`, `name`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `status`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `phone_number`, `last_property`, `animal`, `timeplayed`, `online`, `server`, `is_dead`, `lastPosition`) VALUES
('steam:11000010a01bdb9', 3, '', '', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 0, 'stickybombz', 'unemployed', 0, '[]', '{\"y\":0.0,\"z\":0.0,\"x\":0.0}', 300, 0, 'user', '[{\"val\":785000,\"percent\":78.5,\"name\":\"hunger\"},{\"val\":731250,\"percent\":73.125,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', 'Tommie', 'Pickles', '12/28/1988', 'M', '72', '101-5635', NULL, NULL, '0', 0, 1, 0, '[420.95666503906,-1019.1330566406,29.036413192749]');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `money` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(8, 'steam:110000132580eb0', 'black_money', 0),
(9, 'steam:110000107e03e6e', 'black_money', 0),
(14, 'Char1:11000010a01bdb9', 'black_money', 0),
(15, 'steam:11000011a9b2c0c', 'black_money', 0),
(16, 'steam:11000010a01bdb9', 'black_money', 0),
(17, 'steam:1100001068ef13c', 'black_money', 0),
(18, 'steam:11000010bf93966', 'black_money', 0),
(19, 'steam:110000112969e8f', 'black_money', 0),
(20, 'steam:110000117b74e38', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_admin_notes`
--

CREATE TABLE `user_admin_notes` (
  `id` int(11) NOT NULL,
  `note` longblob,
  `admin` varchar(255) DEFAULT NULL,
  `note_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_contacts`
--

CREATE TABLE `user_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_documents`
--

CREATE TABLE `user_documents` (
  `id` int(11) NOT NULL,
  `owner` varchar(45) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1, 'steam:11000010a01bdb9', 'jagerbomb', 0),
(2, 'steam:11000010a01bdb9', 'sportlunch', 0),
(3, 'steam:11000010a01bdb9', 'vegetables', 0),
(4, 'steam:11000010a01bdb9', 'pills', 0),
(5, 'steam:11000010a01bdb9', 'weapons_license2', 0),
(6, 'steam:11000010a01bdb9', 'litter', 0),
(7, 'steam:11000010a01bdb9', 'boitier', 0),
(8, 'steam:11000010a01bdb9', 'coke', 0),
(9, 'steam:11000010a01bdb9', 'carokit', 0),
(10, 'steam:11000010a01bdb9', 'pilot_license', 0),
(11, 'steam:11000010a01bdb9', 'macka', 0),
(12, 'steam:11000010a01bdb9', 'WEAPON_FLASHLIGHT', 0),
(13, 'steam:11000010a01bdb9', 'lotteryticket', 0),
(14, 'steam:11000010a01bdb9', 'opium', 0),
(15, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(16, 'steam:11000010a01bdb9', 'boating_license', 0),
(17, 'steam:11000010a01bdb9', 'carotool', 0),
(18, 'steam:11000010a01bdb9', 'icetea', 0),
(19, 'steam:11000010a01bdb9', 'rhum', 0),
(20, 'steam:11000010a01bdb9', 'croquettes', 0),
(21, 'steam:11000010a01bdb9', 'scratchoff_used', 0),
(22, 'steam:11000010a01bdb9', 'painkiller', 0),
(23, 'steam:11000010a01bdb9', 'marabou', 0),
(24, 'steam:11000010a01bdb9', 'pizza', 0),
(25, 'steam:11000010a01bdb9', 'lsd', 0),
(26, 'steam:11000010a01bdb9', 'water', 0),
(27, 'steam:11000010a01bdb9', 'cocaine', 0),
(28, 'steam:11000010a01bdb9', 'taxi_license', 0),
(29, 'steam:11000010a01bdb9', 'saucisson', 0),
(30, 'steam:11000010a01bdb9', 'marriage_license', 0),
(31, 'steam:11000010a01bdb9', 'medikit', 0),
(32, 'steam:11000010a01bdb9', 'shotgun_shells', 0),
(33, 'steam:11000010a01bdb9', 'coffee', 0),
(34, 'steam:11000010a01bdb9', 'loka', 0),
(35, 'steam:11000010a01bdb9', '9mm_rounds', 0),
(36, 'steam:11000010a01bdb9', 'tacos', 0),
(37, 'steam:11000010a01bdb9', 'turtle_pooch', 0),
(38, 'steam:11000010a01bdb9', 'fanta', 0),
(39, 'steam:11000010a01bdb9', 'fabric', 0),
(40, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(41, 'steam:11000010a01bdb9', 'rhumcoca', 0),
(42, 'steam:11000010a01bdb9', 'coca', 0),
(43, 'steam:11000010a01bdb9', 'baconburger', 0),
(44, 'steam:11000010a01bdb9', 'pearl', 0),
(45, 'steam:11000010a01bdb9', 'firstaidpass', 0),
(46, 'steam:11000010a01bdb9', 'energy', 0),
(47, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(48, 'steam:11000010a01bdb9', 'fixkit', 0),
(49, 'steam:11000010a01bdb9', 'rhumfruit', 0),
(50, 'steam:11000010a01bdb9', 'iron', 0),
(51, 'steam:11000010a01bdb9', 'weed_pooch', 0),
(52, 'steam:11000010a01bdb9', 'blowpipe', 0),
(53, 'steam:11000010a01bdb9', 'whool', 0),
(54, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(55, 'steam:11000010a01bdb9', 'copper', 0),
(56, 'steam:11000010a01bdb9', 'cocacola', 0),
(57, 'steam:11000010a01bdb9', 'defibrillateur', 0),
(58, 'steam:11000010a01bdb9', 'petrol', 0),
(59, 'steam:11000010a01bdb9', 'WEAPON_STUNGUN', 0),
(60, 'steam:11000010a01bdb9', 'grip', 0),
(61, 'steam:11000010a01bdb9', 'protein_shake', 0),
(62, 'steam:11000010a01bdb9', 'fixtool', 0),
(63, 'steam:11000010a01bdb9', 'whiskycoca', 0),
(64, 'steam:11000010a01bdb9', 'limonade', 0),
(65, 'steam:11000010a01bdb9', 'soda', 0),
(66, 'steam:11000010a01bdb9', 'diamond', 0),
(67, 'steam:11000010a01bdb9', 'silencieux', 0),
(68, 'steam:11000010a01bdb9', 'powerade', 0),
(69, 'steam:11000010a01bdb9', 'blackberry', 0),
(70, 'steam:11000010a01bdb9', 'turtle', 0),
(71, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(72, 'steam:11000010a01bdb9', 'ice', 0),
(73, 'steam:11000010a01bdb9', 'diving_license', 0),
(74, 'steam:11000010a01bdb9', 'bolcacahuetes', 0),
(75, 'steam:11000010a01bdb9', 'jusfruit', 0),
(76, 'steam:11000010a01bdb9', 'grapperaisin', 0),
(77, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(78, 'steam:11000010a01bdb9', 'crack', 0),
(79, 'steam:11000010a01bdb9', 'donut', 0),
(80, 'steam:11000010a01bdb9', 'pearl_pooch', 0),
(81, 'steam:11000010a01bdb9', 'vodkafruit', 0),
(82, 'steam:11000010a01bdb9', 'plongee2', 0),
(83, 'steam:11000010a01bdb9', 'scratchoff', 0),
(84, 'steam:11000010a01bdb9', 'stone', 0),
(85, 'steam:11000010a01bdb9', 'teqpaf', 0),
(86, 'steam:11000010a01bdb9', 'sprite', 0),
(87, 'steam:11000010a01bdb9', 'bolpistache', 0),
(88, 'steam:11000010a01bdb9', 'cigarett', 0),
(89, 'steam:11000010a01bdb9', 'jager', 0),
(90, 'steam:11000010a01bdb9', 'ephedra', 0),
(91, 'steam:11000010a01bdb9', 'flashlight', 0),
(92, 'steam:11000010a01bdb9', 'WEAPON_PUMPSHOTGUN', 0),
(93, 'steam:11000010a01bdb9', 'WEAPON_PISTOL', 0),
(94, 'steam:11000010a01bdb9', 'WEAPON_BAT', 0),
(95, 'steam:11000010a01bdb9', 'fishing_license', 0),
(96, 'steam:11000010a01bdb9', 'fish', 0),
(97, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(98, 'steam:11000010a01bdb9', 'poppy', 0),
(99, 'steam:11000010a01bdb9', 'whiskey', 0),
(100, 'steam:11000010a01bdb9', 'WEAPON_KNIFE', 0),
(101, 'steam:11000010a01bdb9', 'radio', 0),
(102, 'steam:11000010a01bdb9', 'lighter', 0),
(103, 'steam:11000010a01bdb9', 'marijuana', 0),
(104, 'steam:11000010a01bdb9', 'firstaidkit', 0),
(105, 'steam:11000010a01bdb9', 'martini', 0),
(106, 'steam:11000010a01bdb9', 'nitrocannister', 0),
(107, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(108, 'steam:11000010a01bdb9', 'drivers_license', 0),
(109, 'steam:11000010a01bdb9', 'motorcycle_license', 0),
(110, 'steam:11000010a01bdb9', 'commercial_license', 0),
(111, 'steam:11000010a01bdb9', 'dabs', 0),
(112, 'steam:11000010a01bdb9', 'weapons_license3', 0),
(113, 'steam:11000010a01bdb9', 'weapons_license1', 0),
(114, 'steam:11000010a01bdb9', 'gazbottle', 0),
(115, 'steam:11000010a01bdb9', 'vodkaenergy', 0),
(116, 'steam:11000010a01bdb9', 'wood', 0),
(117, 'steam:11000010a01bdb9', 'pastacarbonara', 0),
(118, 'steam:11000010a01bdb9', 'chips', 0),
(119, 'steam:11000010a01bdb9', 'wrench', 0),
(120, 'steam:11000010a01bdb9', 'cheesebows', 0),
(121, 'steam:11000010a01bdb9', 'bolchips', 0),
(122, 'steam:11000010a01bdb9', 'washed_stone', 0),
(123, 'steam:11000010a01bdb9', 'narcan', 0),
(124, 'steam:11000010a01bdb9', 'hunting_license', 0),
(125, 'steam:11000010a01bdb9', 'lockpick', 0),
(126, 'steam:11000010a01bdb9', 'metreshooter', 0),
(127, 'steam:11000010a01bdb9', 'fakepee', 0),
(128, 'steam:11000010a01bdb9', 'golem', 0),
(129, 'steam:11000010a01bdb9', 'bread', 0),
(130, 'steam:11000010a01bdb9', 'essence', 0),
(131, 'steam:11000010a01bdb9', 'drugtest', 0),
(132, 'steam:11000010a01bdb9', 'bolnoixcajou', 0),
(133, 'steam:11000010a01bdb9', 'beer', 0),
(134, 'steam:11000010a01bdb9', 'ephedrine', 0),
(135, 'steam:11000010a01bdb9', 'tequila', 0),
(136, 'steam:11000010a01bdb9', 'lsd_pooch', 0),
(137, 'steam:11000010a01bdb9', 'heroine', 0),
(138, 'steam:11000010a01bdb9', 'binoculars', 0),
(139, 'steam:11000010a01bdb9', 'weed', 0),
(140, 'steam:11000010a01bdb9', 'cannabis', 0),
(141, 'steam:11000010a01bdb9', 'vodka', 0),
(142, 'steam:11000010a01bdb9', 'pcp', 0),
(143, 'steam:11000010a01bdb9', 'clothe', 0),
(144, 'steam:11000010a01bdb9', 'mixapero', 0),
(145, 'steam:11000010a01bdb9', 'menthe', 0),
(146, 'steam:11000010a01bdb9', 'meat', 0),
(147, 'steam:11000010a01bdb9', 'contrat', 0),
(148, 'steam:11000010a01bdb9', 'burger', 0),
(149, 'steam:11000010a01bdb9', 'clip', 0),
(150, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(151, 'steam:11000010a01bdb9', 'meth', 0),
(152, 'steam:11000010a01bdb9', 'drpepper', 0),
(153, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(154, 'steam:11000010a01bdb9', 'whisky', 0),
(155, 'steam:11000010a01bdb9', 'plongee1', 0),
(156, 'steam:11000010a01bdb9', 'cola', 0),
(157, 'steam:11000010a01bdb9', 'leather', 0),
(158, 'steam:11000010a01bdb9', 'gold', 0),
(159, 'steam:11000010a01bdb9', 'gym_membership', 0),
(160, 'steam:11000010a01bdb9', 'mojito', 0),
(161, 'steam:11000010a01bdb9', 'litter_pooch', 0),
(162, 'steam:11000010a01bdb9', 'yusuf', 0),
(163, 'steam:11000010a01bdb9', 'armor', 0),
(164, 'steam:11000010a01bdb9', 'bandage', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_lastcharacter`
--

CREATE TABLE `user_lastcharacter` (
  `steamid` varchar(255) NOT NULL,
  `charid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_lastcharacter`
--

INSERT INTO `user_lastcharacter` (`steamid`, `charid`) VALUES
('steam:11000010a01bdb9', 1),
('steam:110000132580eb0', 1),
('steam:110000107e03e6e', 1),
('steam:11000011a9b2c0c', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_parkings`
--

CREATE TABLE `user_parkings` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `garage` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `zone` int(11) NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user_reports`
--

CREATE TABLE `user_reports` (
  `id` int(11) NOT NULL,
  `reported_by` varchar(80) DEFAULT NULL,
  `report_type` varchar(255) DEFAULT NULL,
  `report_comment` varchar(255) DEFAULT NULL,
  `report_admin` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reports`
--

INSERT INTO `user_reports` (`id`, `reported_by`, `report_type`, `report_comment`, `report_admin`, `report_time`, `userid`) VALUES
(1, 'stickybombz', 'cheating', 'Player', NULL, NULL, '38');

-- --------------------------------------------------------

--
-- Table structure for table `user_warnings`
--

CREATE TABLE `user_warnings` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `admin` varchar(255) DEFAULT NULL,
  `warning` longtext,
  `time_added` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL,
  `inshop` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`, `inshop`) VALUES
('Adder', 'adder', 900000, 'super', 1),
('Akuma', 'AKUMA', 7500, 'motorcycles', 1),
('Alpha', 'alpha', 60000, 'sports', 1),
('Ardent', 'ardent', 1150000, 'sportsclassics', 1),
('Asea', 'asea', 5500, 'sedans', 1),
('Autarch', 'autarch', 1955000, 'super', 1),
('Avarus', 'avarus', 18000, 'motorcycles', 1),
('Bagger', 'bagger', 13500, 'motorcycles', 1),
('Baller', 'baller2', 40000, 'suvs', 1),
('Baller Sport', 'baller3', 60000, 'suvs', 1),
('Banshee', 'banshee', 70000, 'sports', 1),
('Banshee 900R', 'banshee2', 255000, 'importcars', 0),
('Bati 801', 'bati', 12000, 'motorcycles', 1),
('Bati 801RR', 'bati2', 19000, 'motorcycles', 1),
('Bestia GTS', 'bestiagts', 55000, 'sports', 1),
('BF400', 'bf400', 6500, 'motorcycles', 1),
('Bf Injection', 'bfinjection', 16000, 'offroad', 1),
('Bifta', 'bifta', 12000, 'offroad', 1),
('Bison', 'bison', 45000, 'vans', 1),
('Blade', 'blade', 15000, 'muscle', 1),
('Blazer', 'blazer', 6500, 'offroad', 1),
('Blazer Sport', 'blazer4', 8500, 'offroad', 1),
('blazer5', 'blazer5', 1755600, 'offroad', 1),
('Blista', 'blista', 8000, 'compacts', 1),
('BMX (velo)', 'bmx', 160, 'motorcycles', 1),
('Bobcat XL', 'bobcatxl', 32000, 'vans', 1),
('Brawler', 'brawler', 45000, 'offroad', 1),
('Brioso R/A', 'brioso', 18000, 'compacts', 1),
('Btype', 'btype', 62000, 'sportsclassics', 1),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics', 1),
('Btype Luxe', 'btype3', 85000, 'sportsclassics', 1),
('Buccaneer', 'buccaneer', 18000, 'muscle', 1),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle', 1),
('Buffalo', 'buffalo', 12000, 'sports', 1),
('Buffalo S', 'buffalo2', 20000, 'sports', 1),
('Bullet', 'bullet', 90000, 'super', 1),
('Burrito', 'burrito3', 19000, 'vans', 1),
('Camper', 'camper', 42000, 'vans', 1),
('Carbonizzare', 'carbonizzare', 75000, 'sports', 1),
('Carbon RS', 'carbonrs', 18000, 'motorcycles', 1),
('Casco', 'casco', 30000, 'sportsclassics', 1),
('Cavalcade', 'cavalcade2', 55000, 'suvs', 1),
('Cheetah', 'cheetah', 375000, 'super', 1),
('Chimera', 'chimera', 38000, 'motorcycles', 1),
('Chino', 'chino', 15000, 'muscle', 1),
('Chino Luxe', 'chino2', 19000, 'muscle', 1),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles', 1),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes', 1),
('Cognoscenti', 'cognoscenti', 55000, 'sedans', 1),
('Comet', 'comet2', 65000, 'sports', 1),
('Comet 5', 'comet5', 1145000, 'sports', 1),
('Contender', 'contender', 70000, 'suvs', 1),
('Coquette', 'coquette', 65000, 'sports', 1),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics', 1),
('Coquette BlackFin', 'coquette3', 55000, 'muscle', 1),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles', 1),
('Cyclone', 'cyclone', 1890000, 'super', 1),
('Daemon', 'daemon', 11500, 'motorcycles', 1),
('Daemon High', 'daemon2', 13500, 'motorcycles', 1),
('Defiler', 'defiler', 9800, 'motorcycles', 1),
('Deluxo', 'deluxo', 4721500, 'sportsclassics', 1),
('Dominator', 'dominator', 35000, 'muscle', 1),
('Double T', 'double', 28000, 'motorcycles', 1),
('Dubsta', 'dubsta', 45000, 'suvs', 1),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs', 1),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad', 1),
('Dukes', 'dukes', 28000, 'muscle', 1),
('Dune Buggy', 'dune', 8000, 'offroad', 1),
('Elegy', 'elegy2', 38500, 'sports', 1),
('Emperor', 'emperor', 8500, 'sedans', 1),
('Enduro', 'enduro', 5500, 'motorcycles', 1),
('Entity XF', 'entityxf', 425000, 'importcars', 0),
('Esskey', 'esskey', 4200, 'motorcycles', 1),
('Exemplar', 'exemplar', 32000, 'coupes', 1),
('F620', 'f620', 40000, 'coupes', 1),
('Faction', 'faction', 20000, 'muscle', 1),
('Faction Rider', 'faction2', 30000, 'muscle', 1),
('Faction XL', 'faction3', 40000, 'muscle', 1),
('Faggio', 'faggio', 1900, 'motorcycles', 1),
('Vespa', 'faggio2', 2800, 'motorcycles', 1),
('Felon', 'felon', 42000, 'coupes', 1),
('Felon GT', 'felon2', 55000, 'coupes', 1),
('Feltzer', 'feltzer2', 55000, 'sports', 1),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics', 1),
('Fixter (velo)', 'fixter', 225, 'motorcycles', 1),
('Flat Bed', 'flatbed', 15000, 'utility', 1),
('FMJ', 'fmj', 185000, 'super', 1),
('Road King', 'foxharley1', 23000, 'importbikes', 0),
('Road Glide', 'foxharley2', 30000, 'importbikes', 0),
('Fhantom', 'fq2', 17000, 'suvs', 1),
('Fugitive', 'fugitive', 12000, 'sedans', 1),
('Furore GT', 'furoregt', 45000, 'sports', 1),
('Fusilade', 'fusilade', 40000, 'sports', 1),
('Gargoyle', 'gargoyle', 16500, 'motorcycles', 1),
('Gauntlet', 'gauntlet', 30000, 'muscle', 1),
('Gang Burrito', 'gburrito', 45000, 'vans', 1),
('Burrito', 'gburrito2', 29000, 'vans', 1),
('Glendale', 'glendale', 6500, 'sedans', 1),
('Grabger', 'granger', 50000, 'suvs', 1),
('Gresley', 'gresley', 47500, 'suvs', 1),
('GT 500', 'gt500', 785000, 'sportsclassics', 1),
('Guardian', 'guardian', 45000, 'offroad', 1),
('Hakuchou', 'hakuchou', 31000, 'motorcycles', 1),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles', 1),
('Hermes', 'hermes', 535000, 'muscle', 1),
('Hexer', 'hexer', 12000, 'motorcycles', 1),
('Hotknife', 'hotknife', 125000, 'muscle', 1),
('Huntley S', 'huntley', 40000, 'suvs', 1),
('Hustler', 'hustler', 625000, 'muscle', 1),
('Infernus', 'infernus', 180000, 'super', 1),
('Innovation', 'innovation', 23500, 'motorcycles', 1),
('Intruder', 'intruder', 7500, 'sedans', 1),
('Issi', 'issi2', 10000, 'compacts', 1),
('Jackal', 'jackal', 38000, 'coupes', 1),
('Jester', 'jester', 65000, 'sports', 1),
('Jester(Racecar)', 'jester2', 135000, 'sports', 1),
('Journey', 'journey', 6500, 'vans', 1),
('Kamacho', 'kamacho', 345000, 'offroad', 1),
('Khamelion', 'khamelion', 38000, 'sports', 1),
('Kuruma', 'kuruma', 30000, 'sports', 1),
('Landstalker', 'landstalker', 35000, 'suvs', 1),
('RE-7B', 'le7b', 325000, 'super', 1),
('Lynx', 'lynx', 40000, 'sports', 1),
('Mamba', 'mamba', 70000, 'sports', 1),
('Manana', 'manana', 12800, 'sportsclassics', 1),
('Manchez', 'manchez', 5300, 'motorcycles', 1),
('Massacro', 'massacro', 65000, 'sports', 1),
('Massacro(Racecar)', 'massacro2', 130000, 'sports', 1),
('Mesa', 'mesa', 16000, 'suvs', 1),
('Mesa Trail', 'mesa3', 40000, 'suvs', 1),
('Minivan', 'minivan', 13000, 'vans', 1),
('Monroe', 'monroe', 55000, 'sportsclassics', 1),
('The Liberator', 'monster', 210000, 'offroad', 1),
('Moonbeam', 'moonbeam', 18000, 'vans', 1),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans', 1),
('Nemesis', 'nemesis', 5800, 'motorcycles', 1),
('Neon', 'neon', 1500000, 'sports', 1),
('Nightblade', 'nightblade', 35000, 'importbikes', 0),
('Nightshade', 'nightshade', 65000, 'muscle', 1),
('9F', 'ninef', 65000, 'sports', 1),
('9F Cabrio', 'ninef2', 80000, 'sports', 1),
('Omnis', 'omnis', 35000, 'sports', 1),
('Oppressor', 'oppressor', 3524500, 'super', 1),
('Oracle XS', 'oracle2', 35000, 'coupes', 1),
('Osiris', 'osiris', 160000, 'super', 1),
('Panto', 'panto', 10000, 'compacts', 1),
('Paradise', 'paradise', 19000, 'vans', 1),
('Pariah', 'pariah', 1420000, 'sports', 1),
('Patriot', 'patriot', 55000, 'suvs', 1),
('PCJ-600', 'pcj', 6200, 'motorcycles', 1),
('Penumbra', 'penumbra', 28000, 'sports', 1),
('Pfister', 'pfister811', 85000, 'super', 1),
('Phoenix', 'phoenix', 12500, 'muscle', 1),
('Picador', 'picador', 18000, 'muscle', 1),
('Pigalle', 'pigalle', 20000, 'sportsclassics', 1),
('Prairie', 'prairie', 12000, 'compacts', 1),
('Premier', 'premier', 8000, 'sedans', 1),
('Primo Custom', 'primo2', 14000, 'sedans', 1),
('X80 Proto', 'prototipo', 2500000, 'super', 1),
('Radius', 'radi', 29000, 'suvs', 1),
('raiden', 'raiden', 1375000, 'sports', 1),
('Rapid GT', 'rapidgt', 35000, 'sports', 1),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports', 1),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics', 1),
('Reaper', 'reaper', 150000, 'importcars', 0),
('Rebel', 'rebel2', 35000, 'offroad', 1),
('Regina', 'regina', 5000, 'sedans', 1),
('Retinue', 'retinue', 615000, 'sportsclassics', 1),
('Revolter', 'revolter', 1610000, 'sports', 1),
('riata', 'riata', 380000, 'offroad', 1),
('Rocoto', 'rocoto', 45000, 'suvs', 1),
('Ruffian', 'ruffian', 6800, 'motorcycles', 1),
('Ruiner 2', 'ruiner2', 5745600, 'muscle', 1),
('Rumpo', 'rumpo', 15000, 'vans', 1),
('Rumpo Trail', 'rumpo3', 19500, 'vans', 1),
('Sabre Turbo', 'sabregt', 20000, 'muscle', 1),
('Sabre GT', 'sabregt2', 25000, 'muscle', 1),
('Sanchez', 'sanchez', 5300, 'motorcycles', 1),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles', 1),
('Sanctus', 'sanctus', 25000, 'motorcycles', 1),
('Sandking', 'sandking', 55000, 'offroad', 1),
('Savestra', 'savestra', 990000, 'sportsclassics', 1),
('SC 1', 'sc1', 1603000, 'super', 1),
('Schafter', 'schafter2', 25000, 'sedans', 1),
('Schafter V12', 'schafter3', 50000, 'sports', 1),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles', 1),
('Seminole', 'seminole', 25000, 'suvs', 1),
('Sentinel', 'sentinel', 32000, 'coupes', 1),
('Sentinel XS', 'sentinel2', 40000, 'coupes', 1),
('Sentinel3', 'sentinel3', 650000, 'sports', 1),
('Seven 70', 'seven70', 39500, 'sports', 1),
('ETR1', 'sheava', 220000, 'importcars', 0),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles', 1),
('Slam Van', 'slamvan3', 11500, 'muscle', 1),
('Sovereign', 'sovereign', 22000, 'motorcycles', 1),
('Stinger', 'stinger', 80000, 'sportsclassics', 1),
('Stinger GT', 'stingergt', 75000, 'sportsclassics', 1),
('Streiter', 'streiter', 500000, 'sports', 1),
('Stretch', 'stretch', 90000, 'sedans', 1),
('Stromberg', 'stromberg', 3185350, 'sports', 1),
('Sultan', 'sultan', 15000, 'sports', 1),
('Sultan RS', 'sultanrs', 65000, 'importcars', 0),
('Super Diamond', 'superd', 130000, 'sedans', 1),
('Surano', 'surano', 50000, 'sports', 1),
('Surfer', 'surfer', 12000, 'vans', 1),
('T20', 't20', 300000, 'super', 1),
('Tailgater', 'tailgater', 30000, 'sedans', 1),
('Tampa', 'tampa', 16000, 'muscle', 1),
('Drift Tampa', 'tampa2', 80000, 'sports', 1),
('Thrust', 'thrust', 24000, 'motorcycles', 1),
('Tow Truck', 'towtruck', 15000, 'utility', 1),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles', 1),
('Trophy Truck', 'trophytruck', 60000, 'offroad', 1),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad', 1),
('Tropos', 'tropos', 40000, 'sports', 1),
('Turismo R', 'turismor', 350000, 'super', 1),
('Tyrus', 'tyrus', 600000, 'super', 1),
('Vacca', 'vacca', 120000, 'super', 1),
('Vader', 'vader', 7200, 'motorcycles', 1),
('Verlierer', 'verlierer2', 70000, 'sports', 1),
('Vigero', 'vigero', 12500, 'muscle', 1),
('Virgo', 'virgo', 14000, 'muscle', 1),
('Viseris', 'viseris', 875000, 'sportsclassics', 1),
('Visione', 'visione', 2250000, 'super', 1),
('Voltic', 'voltic', 90000, 'super', 1),
('Voltic 2', 'voltic2', 3830400, 'super', 1),
('Voodoo', 'voodoo', 7200, 'muscle', 1),
('Vortex', 'vortex', 9800, 'motorcycles', 1),
('Warrener', 'warrener', 4000, 'sedans', 1),
('Washington', 'washington', 9000, 'sedans', 1),
('Windsor', 'windsor', 95000, 'coupes', 1),
('Windsor Drop', 'windsor2', 125000, 'importcars', 0),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles', 1),
('XLS', 'xls', 32000, 'suvs', 1),
('Yosemite', 'yosemite', 485000, 'muscle', 1),
('Youga', 'youga', 10800, 'vans', 1),
('Youga Luxuary', 'youga2', 14500, 'vans', 1),
('Z190', 'z190', 900000, 'sportsclassics', 1),
('Zentorno', 'zentorno', 1500000, 'super', 1),
('Zion', 'zion', 36000, 'coupes', 1),
('Zion Cabrio', 'zion2', 45000, 'coupes', 1),
('Zombie', 'zombiea', 9500, 'motorcycles', 1),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles', 1),
('Z-Type', 'ztype', 220000, 'sportsclassics', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles_display`
--

CREATE TABLE `vehicles_display` (
  `ID` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `profit` int(11) NOT NULL DEFAULT '10',
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles_display`
--

INSERT INTO `vehicles_display` (`ID`, `name`, `model`, `profit`, `price`) VALUES
(1, 'Windsor Drop', 'windsor2', 10, 125000),
(2, 'Reaper', 'reaper', 10, 150000),
(3, 'Sultan RS', 'sultanrs', 10, 65000),
(4, 'Road Glide', 'foxharley2', 10, 30000),
(5, 'Road King', 'foxharley1', 10, 23000),
(6, 'Nightblade', 'nightblade', 10, 35000);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupés'),
('importbikes', 'Import Bikes'),
('importcars', 'Import Cars'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('utility', 'Utility'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `warnings`
--

CREATE TABLE `warnings` (
  `ID` int(11) NOT NULL,
  `license` varchar(50) NOT NULL,
  `reason` varchar(50) NOT NULL,
  `staff_name` varchar(50) NOT NULL,
  `staff_steamid` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `warrants`
--

CREATE TABLE `warrants` (
  `fullname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `crimes` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 3500),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(12, 'BlackWeashop', 'WEAPON_MICROSMG', 45000),
(14, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 25000),
(16, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 85000),
(18, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 95000),
(20, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 110000),
(22, 'BlackWeashop', 'WEAPON_FIREWORK', 30000),
(23, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(24, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(25, 'GunShop', 'WEAPON_BALL', 50),
(26, 'BlackWeashop', 'WEAPON_BALL', 50),
(27, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(28, 'GunShop', 'WEAPON_PISTOL50', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `whitelist_jobs`
--

CREATE TABLE `whitelist_jobs` (
  `identifier` varchar(255) COLLATE utf8_bin NOT NULL,
  `job` varchar(255) COLLATE utf8_bin NOT NULL,
  `grade` varchar(255) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coffees`
--
ALTER TABLE `coffees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commend`
--
ALTER TABLE `commend`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock`
--
ALTER TABLE `dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dock_categories`
--
ALTER TABLE `dock_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `gsr`
--
ALTER TABLE `gsr`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jail`
--
ALTER TABLE `jail`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kicks`
--
ALTER TABLE `kicks`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nitro_vehicles`
--
ALTER TABLE `nitro_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_dock`
--
ALTER TABLE `owned_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`),
  ADD KEY `index_owned_vehicles_owner` (`owner`);

--
-- Indexes for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `license` (`license`);

--
-- Indexes for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `received_bans`
--
ALTER TABLE `received_bans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_dock`
--
ALTER TABLE `rented_dock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `server_actions`
--
ALTER TABLE `server_actions`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item` (`item`,`plate`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_admin_notes`
--
ALTER TABLE `user_admin_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_contacts`
--
ALTER TABLE `user_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_documents`
--
ALTER TABLE `user_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_parkings`
--
ALTER TABLE `user_parkings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reports`
--
ALTER TABLE `user_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_warnings`
--
ALTER TABLE `user_warnings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `warnings`
--
ALTER TABLE `warnings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account`
--
ALTER TABLE `addon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coffees`
--
ALTER TABLE `coffees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `commend`
--
ALTER TABLE `commend`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datastore`
--
ALTER TABLE `datastore`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `dock`
--
ALTER TABLE `dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `dock_categories`
--
ALTER TABLE `dock_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `fine_types_ballas`
--
ALTER TABLE `fine_types_ballas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_biker`
--
ALTER TABLE `fine_types_biker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bishops`
--
ALTER TABLE `fine_types_bishops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_bountyhunter`
--
ALTER TABLE `fine_types_bountyhunter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_dismay`
--
ALTER TABLE `fine_types_dismay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_grove`
--
ALTER TABLE `fine_types_grove`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_irish`
--
ALTER TABLE `fine_types_irish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_mafia`
--
ALTER TABLE `fine_types_mafia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rebel`
--
ALTER TABLE `fine_types_rebel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_rodriguez`
--
ALTER TABLE `fine_types_rodriguez`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `fine_types_vagos`
--
ALTER TABLE `fine_types_vagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2087;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=389;

--
-- AUTO_INCREMENT for table `kicks`
--
ALTER TABLE `kicks`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `licenses`
--
ALTER TABLE `licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `old_vehicles`
--
ALTER TABLE `old_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;

--
-- AUTO_INCREMENT for table `old_vehicle_categories`
--
ALTER TABLE `old_vehicle_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `owned_dock`
--
ALTER TABLE `owned_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `owner_vehicles`
--
ALTER TABLE `owner_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `playerstattoos`
--
ALTER TABLE `playerstattoos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `qalle_brottsregister`
--
ALTER TABLE `qalle_brottsregister`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `received_bans`
--
ALTER TABLE `received_bans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_dock`
--
ALTER TABLE `rented_dock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_actions`
--
ALTER TABLE `server_actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `truck_inventory`
--
ALTER TABLE `truck_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_contacts`
--
ALTER TABLE `user_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_documents`
--
ALTER TABLE `user_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_parkings`
--
ALTER TABLE `user_parkings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reports`
--
ALTER TABLE `user_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_warnings`
--
ALTER TABLE `user_warnings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles_display`
--
ALTER TABLE `vehicles_display`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warnings`
--
ALTER TABLE `warnings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `whitelist_jobs`
--
ALTER TABLE `whitelist_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
